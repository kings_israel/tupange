class UKTags extends HTMLElement {
	get disabled(){
		return this.hasAttribute("disabled");
	}
	set disabled(val){
		if (val) this.__setAttribute("disabled", "");
		else this.removeAttribute("disabled");
	}
	get value(){
		if (!this.$ready) return "";
		return this.$hidden.value.trim();
	}
	set value(val){
		if (!this.$ready) return "";
		this.__initValue(val);
		return this.value;
	}
	get fetch(){
		let fetch = this.$fetch;
		if (fetch !== null && "function" === typeof fetch) return fetch;
		if (fetch !== null && "string" === typeof fetch && (fetch = fetch.trim()).length) return fetch;
		if (this.$attrs.hasOwnProperty("fetch") && (fetch = this.$attrs["fetch"].trim()).length) return fetch;
		return "";
	}
	set fetch(val){
		if (val !== null && "function" === typeof val) this.$fetch = val;
		else if (val !== null && "string" === typeof val && (val = val.trim()).length) this.$fetch = val;
		return this.fetch;
	}
	__setAttribute(name, value){
		value = value + "";
		if (this.hasAttribute(name) && this.__getAttribute(name) == value) return;
		this.__set_attribute = true;
		return this.setAttribute(name, value);
	}
	__getAttribute(name){
		let value = null;
		if (!(this.hasAttribute(name) && "string" === typeof (value = this.getAttribute(name)) && (value = value.trim()).length)) value = null;
		return value;
	}
	__emitEvent(name, cancelable){
		if (!("string" === typeof name && (name = name.trim()).length)) return false;
		let event = document.createEvent("Event");
		event.initEvent(name, false, !!cancelable);
		return this.dispatchEvent(event);
	}
	__emitReady(){
		if (this.emit_ready) return;
		this.__emitEvent("ready", false);
		this.emit_ready = true;
	}
	__httpGet(url, callback){
		let temp_placeholder = this.$input.getAttribute("placeholder");
		if (this.$ready) this.$input.setAttribute("placeholder", "Please wait...");
		let docall = (data) => {
			this.$input.setAttribute("placeholder", temp_placeholder);
			if ("function" === typeof callback){
				if (data === null) callback(null);
				if (!Array.isArray(data)){
					console.debug("UKTags Error! Unexpected fetch data.", data, this);
					return callback(null);
				}
				return callback(data);
			}
			return false;
		};
		try {
			let xhr = new XMLHttpRequest();
			xhr.onreadystatechange = () => {
				if (xhr.readyState == 4){
					if (xhr.status == 200){
						let res = xhr.responseText;
						try {
							let data = JSON.parse(res);
							docall(data);
						}
						catch (e){
							console.debug("UKTags Exception! Unexpected data in response.", e, res);
							throw ("UKTags Exception! Unexpected data in response.");
						}
					}
					else throw ("UKTags Exception! Unexpected response status: " + xhr.status);
				}
			};
			xhr.open("GET", url, true);
			xhr.send(null);
		}
		catch (e){
			console.debug(e);
			docall(null);
		}
	}
	__fetchData(query, callback){
		let docall = (data) => {
			if ("function" === typeof callback){
				if (!data) return callback(null);
				if (!Array.isArray(data)){
					console.debug("UKTags Error! Unexpected fetch data.", data, this);
					return callback(null);
				}
				this.$last_query = query;
				this.$last_query_items = data;
				return callback(data);
			}
			return false;
		};
		if (query == this.$last_query && Array.isArray(this.$last_query_items)) return docall(this.$last_query_items);
		let fetch = this.fetch;
		if (!fetch){
			console.debug("UKTags Error! No Fetch URL", this);
			return docall(null);
		}
		if ("function" === typeof fetch){
			let result = fetch(query);
			if (result && Object.prototype.toString.call(result) === "[object Promise]"){
				Promise.resolve(result).then((res) => docall(res), (err) => {
					console.debug(err);
					docall(null);
				});
			}
			else docall(result);
		}
		else if ("string" === typeof fetch){
			fetch = fetch.trim();
			if (fetch.match(/\{\{query\}\}/ig)) fetch = fetch.replace(/\{\{query\}\}/ig, query);
			else if (fetch.match(/=\s*$/)) fetch = fetch + query;
			else {
				console.debug("AutoComplete Error! Query value not set in url");
				return docall(null);
			}
			this.__httpGet(fetch, (res) => docall(res));
		}
		else {
			console.debug("AutoComplete Error! Unsupported fetch value");
			return docall(null);
		}
	}
	__initValue(val){
		try {
			val = val.replace(/\'/g, "\"");
			let items = JSON.parse(val);
			for (let i = 0; i < items.length; i ++){
				let item = items[i];
				this.setValue(item, true);
			}
		}
		catch (e){
			this.$tags.innerHTML = "";
			this.$values = [];
			this.setValue(null, true);
		}
		this.__emitReady();
	}
	__template(item, template){
		if (!("string" === typeof template && (template = (template + "").trim()).length)){
			console.debug("UKAutoComplete Error: Template error!", template, item);
			this.__stringify(item);
		}
		let match, template_value = template;
		if ("object" === typeof item && Object.keys(item).length){
			while (match = template_value.match(/\{\{([^\{\}]*)\}\}/)){
				let match_key = match[1];
				let match_value = match[0];
				let replace_value = "";
				if (item.hasOwnProperty(match_key)) replace_value = item[match_key];
				else console.debug("UKAutoComplete Error: Template item object missing key \"" + match_key + "\"", item, template, template_value);
				template_value = template_value.replace(match_value, replace_value);
			}
		}
		return template_value;
	}
	__restoreItem(item){
		let template;
		if (this.$attrs.hasOwnProperty("item-template") && (template = this.$attrs["item-template"].trim()).length) template = this.__template(item, template);
		else template = this.__template(item, "");
		let list_item = this.__list_item(template, false);
		list_item.addEventListener("click", () => {
			this.setValue(item);
			list_item.remove();
		});
		this.$dropdown_list.insertBefore(list_item, this.$dropdown_list.firstChild);
	}
	__inValues(item){
		for (let i = 0; i < this.$values.length; i ++){
			if (this.__stringify(item) == this.__stringify(this.$values[i])) return true;
		}
		return false;
	}
	__removeValuesItem(item){
		for (let i = 0; i < this.$values.length; i ++){
			if (this.__stringify(item) == this.__stringify(this.$values[i])){
				this.$values.splice(i, 1);
				return true;
			}
		}
		return false;
	}
	__populate(items){
		if (!Array.isArray(items)){
			this.$dropdown.innerHTML = "<div style='padding:10px;color:#888;'>Invalid data!</div>";
			return;
		}
		if (!items.length){
			this.$dropdown.innerHTML = "<div style='padding:10px;color:#888;'>No results.</div>";
			return;
		}
		let list = document.createElement("ul");
		list.style.listStyleType = "none";
		list.style.margin = "0";
		list.style.padding = "0";
		list.style.width = "100%";
		for (let i = 0; i < items.length; i ++){
			let item = items[i];
			if (this.__inValues(item)) continue;
			if ("object" === typeof item && item.hasOwnProperty("header") && item.header && item.hasOwnProperty("text")){
				let list_item = this.__list_item(item.text, true);
				list.appendChild(list_item);
			}
			else {
				let template;
				if (this.$attrs.hasOwnProperty("item-template") && (template = this.$attrs["item-template"].trim()).length) template = this.__template(item, template);
				else template = this.__template(item, "");
				let list_item = this.__list_item(template, false);
				list_item.addEventListener("click", () => {
					this.setValue(item);
					list_item.remove();
				});
				list.appendChild(list_item);
			}
		}
		let css = ".uk-ac-itm:focus,.uk-ac-itm:hover{cursor:pointer;background:#39f !important;color:#fff !important;}";
		let style = document.createElement("style");
		if (style.styleSheet) style.styleSheet.cssText = css;
		else style.appendChild(document.createTextNode(css));
		this.$dropdown.innerHTML = "";
		this.$dropdown.appendChild(list);
		this.$dropdown.appendChild(style);
		this.$dropdown_list = list;
	}
	__list_item(text, is_header){
		let list_item = document.createElement("li");
		list_item.innerHTML = text;
		if (is_header){
			list_item.style.pointerEvents = "none";
			list_item.style.padding = "10px 15px 5px";
			list_item.style.borderBottom = "1px solid #ddd";
			list_item.style.fontWeight = "700";
			list_item.style.color = "#888";
		}
		else {
			list_item.className = "uk-ac-itm";
			list_item.style.color = "#444";
			list_item.style.padding = "10px 15px";
			list_item.style.borderBottom = "1px solid #f1f1f1";
		}
		return list_item;
	}
	__stringify(obj){
		let seen = [];
		return JSON.stringify(obj, function(key, val){
			if (val != null && typeof val == "object"){
				if (seen.indexOf(val) >= 0) return;
				seen.push(val);
			}
			return val;
		});
	}
	__create(){
		this.build();
		let value;
		if (this.$attrs.hasOwnProperty("value") && (value = this.$attrs["value"].trim()).length) this.__initValue(value);
		else this.__emitReady();
	}
	build(){
		//set attrs
		this.$attrs = {};
		let attrs = UKTags.observedAttributes;
		for (let i = 0; i < attrs.length; i ++){
			let attr = attrs[i];
			let value = this.__getAttribute(attr);
			if ("string" === typeof value) this.$attrs[attr] = value.trim();
		}

		//destroy
		this.$ready = false;
		while (this.firstChild) this.removeChild(this.firstChild);

		//wrapper
		let input_wrapper = document.createElement("div");
		input_wrapper.className = "uk-tags-input-wrapper";
		input_wrapper.style.display = "inline-block";
		input_wrapper.style.position = "relative";
		input_wrapper.style.width = "calc(100% - 14px)";
		input_wrapper.style.border = "1px solid #ddd";
		input_wrapper.style.cursor = "text";
		input_wrapper.style.padding = "0 6px";
		input_wrapper.style.background = "#fff";

		//tags-wrapper
		let tags_wrapper = document.createElement("div");
		tags_wrapper.style.display = "inline";
		input_wrapper.appendChild(tags_wrapper);

		//input
		let input = document.createElement("input");
		input.type = "text";
		input.style.height = "28px";
		input.style.width = "100%";
		input.style.border = "0";
		input.style.padding = "0";
		input.style.marginTop = "2px";
		input.style.verticalAlign = "middle";
		input.style.font = "inherit";
		input.style.background = "#fff";
		input.style.color = "#444";

		if (this.$attrs.hasOwnProperty("placeholder")) input.setAttribute("placeholder", this.$attrs["placeholder"]);
		input_wrapper.appendChild(input);

		//input icon
		let icon;
		if (this.$attrs.hasOwnProperty("icon") && (icon = this.$attrs["icon"].trim()).length){
			input.style.paddingLeft = "30px";
			input.style.width = "calc(100% - 30px)";
			let input_icon = document.createElement("span");
			input_icon.innerHTML = "<i class='uk-icon-" + icon + "'></i>";
			input_icon.className = "uk-tags-icon";
			input_icon.style.color = "#999";
			input_icon.style.position = "absolute";
			input_icon.style.width = "30px";
			input_icon.style.height = "100%";
			input_icon.style.display = "flex";
			input_icon.style.alignItems = "center";
			input_icon.style.justifyContent = "center";
			input_icon.style.left = "0";
			input_icon.style.textDecoration = "none";
			input_wrapper.appendChild(input_icon);
		}

		//hidden
		let name, hidden = document.createElement("input");
		hidden.type = "hidden";
		if (this.$attrs.hasOwnProperty("name") && (name = this.$attrs["name"].trim()).length) hidden.setAttribute("name", name);
		input_wrapper.appendChild(hidden);

		//action
		let action = document.createElement("a");
		action.href = "javascript:";
		action.innerHTML = "<i class='uk-icon-caret-down'></i>";
		action.className = "uk-tags-action";
		action.style.color = "#555";
		action.style.position = "absolute";
		action.style.width = "30px";
		action.style.height = "30px";
		action.style.display = "flex";
		action.style.alignItems = "center";
		action.style.justifyContent = "center";
		action.style.right = "0";
		action.style.bottom = "0";
		action.style.textDecoration = "none";
		input_wrapper.appendChild(action);

		//dropdown
		let drop = document.createElement("div");
		drop.className = "uk-tags-dropdown";
		drop.style.display = "none";
		drop.style.marginTop = "0";
		drop.style.position = "absolute";
		drop.style.left = "0";
		drop.style.minWidth = "100%";
		drop.style.minHeight = "100px";
		drop.style.background = "#fff";
		//drop.style.border = "1px solid #ddd";
		drop.style.maxHeight = "300px";
		drop.style.overflowY = "auto";
		drop.style.overflowX = "hidden";
		drop.style.boxShadow = "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)";
		drop.style.zIndex = "100";

		//children
		this.style.position = "relative";
		this.style.display = "inline-block";
		this.appendChild(input_wrapper);
		this.appendChild(drop);

		//disabled css
		let css = this.$tag_name + "[disabled]{pointer-events:none;opacity:0.7;}";
		let style = document.createElement("style");
		if (style.styleSheet) style.styleSheet.cssText = css;
		else style.appendChild(document.createTextNode(css));
		this.appendChild(style);

		//set
		this.$tags = tags_wrapper;
		this.$dropdown = drop;
		this.$input = input;
		this.$hidden = hidden;
		this.$action = action;
		this.$wrapper = input_wrapper;

		//listeners
		let isHover = (elem) => {
			return elem.parentElement.querySelector(":hover") === elem;
		};
		this.$wrapper.addEventListener("click", (event) => {
			if (isHover(this.$action) || this.disabled) return;
			this.$input.focus();
		}, {capture: false});
		this.$action.addEventListener("click", (event) => {
			if (this.$open) this.hideDropdown();
			else this.showDropdown();
			event.preventDefault();
		});
		this.$input.addEventListener("focus", (event) => {
			this.showDropdown();
		});
		this.$input.addEventListener("input", (event) => {
			this.$hidden.value = "";
			this.showDropdown();
		});
		document.addEventListener("click", (event) => {
			if (!(isHover(this.$action) || isHover(this.$input) || isHover(this.$dropdown) || isHover(this.$wrapper))) this.hideDropdown();
		});

		//complete
		this.$ready = true;
		this.__emitEvent("create", false);
	}
	populateDropdown(items){}
	showDropdown(){
		if (!this.$ready || this.disabled || !this.__emitEvent("show", true)) return;
		this.$action.innerHTML = "<i class='uk-icon-caret-up'></i>";
		this.$dropdown.innerHTML = "<div style='padding:10px;color:#888;'>Loading...</div>";
		this.$dropdown.style.display = "block";
		this.$open = true;
		this.__fetchData(this.$input.value.trim(), (items) => this.__populate(items));
	}
	hideDropdown(){
		if (!this.$ready) return;
		this.$input.value = "";
		this.$action.innerHTML = "<i class='uk-icon-caret-down'></i>";
		this.$dropdown.style.display = "none";
		this.$open = false;
	}
	setValue(item, no_dropdown){
		this.new_item = item;
		if (!this.__emitEvent("input", true)) return;
		let value = "";
		if ("object" === typeof item && item) value = this.__stringify(item);
		else if (!item) value = "";
		else value = (item + "").trim();
		let template, input_value = value;
		if ("object" === typeof item && item && value != ""){
			if (this.$attrs.hasOwnProperty("template") && (template = this.$attrs["template"].trim()).length) input_value = this.__template(item, template);

			//tag
			let tag = document.createElement("div");
			tag.style.padding = "5px 10px";
			tag.style.borderRadius = "15px";
			tag.style.margin = "5px 5px 5px 0";
			tag.style.background = "#eee";
			tag.style.display = "inline-block";
			tag.style.fontSize = "12px";
			tag.innerHTML = input_value;

			//tag close
			let tag_close = document.createElement("a");
			tag_close.style.marginLeft = "5px";
			tag_close.style.textDecoration = "none";
			tag_close.style.color = "#999";
			tag_close.innerHTML = "<i class='uk-icon-close'></i>";
			tag_close.setAttribute("data-value", value);
			tag_close.setAttribute("data-object", "object" === typeof item && item ? "1" : "0");
			tag_close.addEventListener("click", (event) => {
				tag.remove();
				let data_value = tag_close.getAttribute("data-value");
				let data_object = tag_close.getAttribute("data-object");
				try {
					let item = data_object == "1" ? JSON.parse(data_value) : data_value;
					this.__removeValuesItem(item);
					this.__restoreItem(item);
				}
				catch (e){
					console.error(e);
					return;
				}
				this.$input.value = "";
				this.$hidden.value = this.$values.length ? this.__stringify(this.$values) : "";
			});

			//apppend
			tag.appendChild(tag_close);
			this.$tags.appendChild(tag);
			this.$values.push(item);
			this.$input.value = "";
			this.$hidden.value = this.__stringify(this.$values);
		}
		else {
			this.$input.value = "";
			this.$hidden.value = "";
		}
		if (!no_dropdown) this.showDropdown();

		//on change
		this.__emitEvent("change");
	}
	constructor(){
		super();
		this.emit_ready = false;
		this.new_item = null;
		this.$tag_name = "uk-tags";
		this.__set_attribute = false;
		this.$attrs = {};
		this.$values = [];
		this.$wrapper = null;
		this.$input = null;
		this.$hidden = null;
		this.$action = null;
		this.$tags = null;
		this.$dropdown = null;
		this.$dropdown_list = null;
		this.$ready = false;
		this.$open = false;
		this.$fetch = null;
		this.$last_query = null;
		this.$last_query_items = null;
	}
	connectedCallback(){
		this.__create();
	}
	disconnectedCallback(){}
	static get observedAttributes(){
		return ["placeholder", "name", "icon", "value", "fetch", "item-template", "template"];
	}
	attributeChangedCallback(name, oldValue, newValue){
		if ((newValue = (newValue + "").trim()).length && newValue != oldValue && oldValue !== null && !this.__set_attribute){
			switch (name){
				case "placeholder":
				if (this.$ready) this.$input.setAttribute("placeholder", newValue);
				break;
				case "name":
				if (this.$ready) this.$hidden.setAttribute("name", newValue);
				break;
				case "icon":
				case "value":
				case "fetch":
				case "template":
				case "item-template":
				this.__create();
				break;
			}
		}
		this.__set_attribute = false;
	}
	writeValue(obj){
	    console.log("writeValue", obj);
	}
}
window.customElements.define("uk-tags", UKTags);
