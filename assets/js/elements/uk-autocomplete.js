class UKAutoComplete extends HTMLElement {
	get disabled(){
		return this.hasAttribute("disabled");
	}
	set disabled(val){
		if (val) this.__setAttribute("disabled", "");
		else this.removeAttribute("disabled");
	}
	get value(){
		if (!this.$ready) return this.current_value;
		return this.$hidden.value.trim();
	}
	set value(val){
		if (!this.$ready) return this.current_value;
		if ("object" === typeof val && val){
			if (val.hasOwnProperty("value")) this.__initValue(val.value);
			else if (val.hasOwnProperty("name")) this.__initValue(val.name);
			else if (val.hasOwnProperty("text")) this.__initValue(val.text);
		}
		else if (["string", "number"].indexOf(typeof val) > -1) this.__initValue(val);
		return this.value;
	}
	get fetch(){
		let fetch = this.$fetch;
		if (fetch !== null && "function" === typeof fetch) return fetch;
		if (fetch !== null && "string" === typeof fetch && (fetch = fetch.trim()).length) return fetch;
		if (this.$attrs.hasOwnProperty("fetch") && (fetch = this.$attrs["fetch"].trim()).length) return fetch;
		return "";
	}
	set fetch(val){
		if (val !== null && "function" === typeof val) this.$fetch = val;
		else if (val !== null && "string" === typeof val && (val = val.trim()).length) this.$fetch = val;
		return this.fetch;
	}
	__setAttribute(name, value){
		value = value + "";
		if (this.hasAttribute(name) && this.__getAttribute(name) == value) return;
		this.__set_attribute = true;
		return this.setAttribute(name, value);
	}
	__getAttribute(name){
		let value = null;
		if (!(this.hasAttribute(name) && "string" === typeof (value = this.getAttribute(name)) && (value = value.trim()).length)) value = null;
		return value;
	}
	__emitEvent(name, cancelable){
		if (!("string" === typeof name && (name = name.trim()).length)) return false;
		let event = document.createEvent("Event");
		event.initEvent(name, false, !!cancelable);
		return this.dispatchEvent(event);
	}
	__httpGet(url, callback){
		if (this.$ready) this.$input.setAttribute("placeholder", "Please wait...");
		let docall = (data) => {
			this.$input.setAttribute("placeholder", this.input_placeholder);
			if ("function" === typeof callback){
				if (data === null) callback(null);
				if (!Array.isArray(data)){
					console.debug("UKAutoComplete Error! Unexpected fetch data.", data, this);
					return callback(null);
				}
				return callback(data);
			}
			return false;
		};
		try {
			let xhr = new XMLHttpRequest();
			xhr.onreadystatechange = () => {
				if (xhr.readyState == 4){
					if (xhr.status == 200){
						let res = xhr.responseText;
						try {
							let data = JSON.parse(res);
							docall(data);
						}
						catch (e){
							console.debug("UKAutoComplete Exception! Unexpected data in response.", e, res);
							throw ("UKAutoComplete Exception! Unexpected data in response.");
						}
					}
					else throw ("UKAutoComplete Exception! Unexpected response status: " + xhr.status);
				}
			};
			xhr.open("GET", url, true);
			xhr.send(null);
		}
		catch (e){
			console.debug(e);
			docall(null);
		}
	}
	__fetchData(query, callback){
		let docall = (data) => {
			if ("function" === typeof callback){
				if (!data) return callback(null);
				if (!Array.isArray(data)){
					console.debug("UKAutoComplete Error! Unexpected fetch data.", data, this);
					return callback(null);
				}
				this.$last_query = query;
				this.$last_query_items = data;
				return callback(data);
			}
			return false;
		};
		if (query == this.$last_query && Array.isArray(this.$last_query_items)) return docall(this.$last_query_items);
		let fetch = this.fetch;
		if (!fetch){
			console.debug("UKAutoComplete Error! No Fetch URL", this);
			return docall(null);
		}
		if ("function" === typeof fetch){
			let result = fetch(query);
			if (result && Object.prototype.toString.call(result) === "[object Promise]"){
				Promise.resolve(result).then((res) => docall(res), (err) => {
					console.debug(err);
					docall(null);
				});
			}
			else docall(result);
		}
		else if ("string" === typeof fetch){
			fetch = fetch.trim();
			if (fetch.match(/\{\{query\}\}/ig)) fetch = fetch.replace(/\{\{query\}\}/ig, query);
			else if (fetch.match(/=\s*$/)) fetch = fetch + query;
			else {
				console.debug("AutoComplete Error! Query value not set in url");
				return docall(null);
			}
			this.__httpGet(fetch, (res) => docall(res));
		}
		else {
			console.debug("AutoComplete Error! Unsupported fetch value");
			return docall(null);
		}
	}
	__initValue(val){
		this.__fetchData("", (items) => {
			val = String(val).trim();
			if (val.length && Array.isArray(items)){
				let found = false;
				for (let i = 0; i < items.length; i ++){
					let item = items[i];
					if ("object" === typeof item && item){
						if (this.__stringify(item) == this.__stringify(val)){
							this.setValue(item, true);
							break;
						}
						for (let key in item){
							if (item.hasOwnProperty(key)){
								let item_value = item[key];
								if (item_value == val){
									found = true;
									this.setValue(item, true);
									break;
								}
							}
						}
						if (found) break;
					}
					else if (val == item){
						this.setValue(item, true);
						break;
					}
				}
			}
			if (!val.length) this.setValue(null, true);
			this.__emitReady();
		});
	}
	__emitReady(){
		if (this.emit_ready) return;
		this.__emitEvent("ready", false);
		this.emit_ready = true;
	}
	__template(item, template){
		if (!("string" === typeof template && (template = (template + "").trim()).length)){
			console.debug("UKAutoComplete Error: Template error!", template, item);
			this.__stringify(item);
		}
		let match, template_value = template;
		if ("object" === typeof item && Object.keys(item).length){
			while (match = template_value.match(/\{\{([^\{\}]*)\}\}/)){
				let match_key = match[1];
				let match_value = match[0];
				let replace_value = "";
				if (item.hasOwnProperty(match_key)) replace_value = item[match_key];
				else console.debug("UKAutoComplete Error: Template item object missing key \"" + match_key + "\"", item, template, template_value);
				template_value = template_value.replace(match_value, replace_value);
			}
		}
		return template_value;
	}
	__populate(items){
		if (!Array.isArray(items)){
			this.$dropdown.innerHTML = "<div style='padding:10px;color:#888;'>Invalid data!</div>";
			return;
		}
		if (!items.length){
			this.$dropdown.innerHTML = "<div style='padding:10px;color:#888;'>No results.</div>";
			return;
		}
		let list = document.createElement("ul");
		list.style.listStyleType = "none";
		list.style.margin = "0";
		list.style.padding = "0";
		list.style.width = "100%";
		for (let i = 0; i < items.length; i ++){
			let item = items[i];
			if ("object" === typeof item && item.hasOwnProperty("header") && item.header && item.hasOwnProperty("text")){
				let list_item = this.__list_item(item.text, true);
				list.appendChild(list_item);
			}
			else {
				let template;
				if (this.$attrs.hasOwnProperty("item-template") && (template = this.$attrs["item-template"].trim()).length) template = this.__template(item, template);
				else template = this.__template(item, "");
				let list_item = this.__list_item(template, false);
				list_item.addEventListener("click", () => this.setValue(item));
				list.appendChild(list_item);
			}
		}
		let css = ".uk-ac-itm:focus,.uk-ac-itm:hover{cursor:pointer;background:#39f !important;color:#fff !important;}";
		let style = document.createElement("style");
		if (style.styleSheet) style.styleSheet.cssText = css;
		else style.appendChild(document.createTextNode(css));
		this.$dropdown.innerHTML = "";
		this.$dropdown.appendChild(list);
		this.$dropdown.appendChild(style);
	}
	__list_item(text, is_header){
		let list_item = document.createElement("li");
		list_item.innerHTML = text;
		if (is_header){
			list_item.style.pointerEvents = "none";
			list_item.style.padding = "10px 15px 5px";
			list_item.style.borderBottom = "1px solid #ddd";
			list_item.style.fontWeight = "700";
			list_item.style.color = "#888";
		}
		else {
			list_item.className = "uk-ac-itm";
			list_item.style.color = "#444";
			list_item.style.padding = "10px 15px";
			list_item.style.borderBottom = "1px solid #f1f1f1";
		}
		return list_item;
	}
	__stringify(obj){
		if ("undefined" === typeof obj || obj === null) return "";
		if ("object" === typeof obj && obj){
			let seen = [];
			return JSON.stringify(obj, function(key, val){
				if (val != null && typeof val == "object"){
					if (seen.indexOf(val) >= 0) return;
					seen.push(val);
				}
				return val;
			});
		}
		return obj + "";
	}
	__create(){
		this.build();
		let value;
		if (this.$attrs.hasOwnProperty("value") && (value = this.$attrs["value"].trim()).length) this.__initValue(value);
		else this.__emitReady();
	}
	build(){
		//set attrs
		this.$attrs = {};
		let attrs = UKAutoComplete.observedAttributes;
		for (let i = 0; i < attrs.length; i ++){
			let attr = attrs[i];
			let value = this.__getAttribute(attr);
			if ("string" === typeof value) this.$attrs[attr] = value.trim();
		}

		//destroy
		this.$ready = false;
		while (this.firstChild) this.removeChild(this.firstChild);

		//wrapper
		let input_wrapper = document.createElement("div");
		input_wrapper.className = "uk-autocomplete-input-wrapper";
		input_wrapper.style.display = "inline-block";
		input_wrapper.style.position = "relative";
		input_wrapper.style.width = "100%";

		//input
		let input = document.createElement("input");
		input.type = "text";
		input.style.float = "left";
		input.style.height = "32px";
		input.style.width = "calc(100% - 36px)";
		input.style.margin = "0";
		input.style.verticalAlign = "middle";
		input.style.font = "inherit";
		input.style.padding = "4px 6px";
		input.style.border = "1px solid #ddd";
		input.style.background = "#fff";
		input.style.color = "#444";
		input.style.transition = "all .2s linear";
		input.style.transitionProperty = "border,background,color,box-shadow,padding";
		input.style.borderRadius = "0px";
		input.style.paddingRight = "30px";
		if (this.$attrs.hasOwnProperty("placeholder")) this.input_placeholder = this.$attrs["placeholder"];
		input.setAttribute("placeholder", this.input_placeholder);
		input_wrapper.appendChild(input);

		//input icon
		let icon;
		if (this.$attrs.hasOwnProperty("icon") && (icon = this.$attrs["icon"].trim()).length){
			input.style.paddingLeft = "30px";
			input.style.width = "calc(100% - 60px)";
			let input_icon = document.createElement("span");
			input_icon.innerHTML = "<i class='uk-icon-" + icon + "'></i>";
			input_icon.className = "uk-autocomplete-icon";
			input_icon.style.color = "#999";
			input_icon.style.position = "absolute";
			input_icon.style.width = "30px";
			input_icon.style.height = "100%";
			input_icon.style.display = "flex";
			input_icon.style.alignItems = "center";
			input_icon.style.justifyContent = "center";
			input_icon.style.left = "0";
			input_icon.style.textDecoration = "none";
			input_wrapper.appendChild(input_icon);
		}

		//hidden
		let name, hidden = document.createElement("input");
		hidden.type = "hidden";
		if (this.$attrs.hasOwnProperty("name") && (name = this.$attrs["name"].trim()).length) hidden.setAttribute("name", name);
		input_wrapper.appendChild(hidden);

		//action
		let action = document.createElement("a");
		action.className = "uk-autocomplete-action";
		action.href = "javascript:";
		action.innerHTML = "<i class='uk-icon-caret-down'></i>";
		action.style.color = "#555";
		action.style.position = "absolute";
		action.style.width = "30px";
		action.style.height = "100%";
		action.style.display = "flex";
		action.style.alignItems = "center";
		action.style.justifyContent = "center";
		action.style.right = "0";
		action.style.textDecoration = "none";
		input_wrapper.appendChild(action);

		//dropdown
		let drop = document.createElement("div");
		drop.className = "uk-autocomplete-dropdown";
		drop.style.display = "none";
		drop.style.marginTop = "-6px";
		drop.style.position = "absolute";
		drop.style.left = "0";
		drop.style.minWidth = "calc(100% - 2px)";
		drop.style.minHeight = "100px";
		drop.style.background = "#fff";
		//drop.style.border = "1px solid #ddd";
		drop.style.maxHeight = "300px";
		drop.style.overflowY = "auto";
		drop.style.overflowX = "hidden";
		drop.style.boxShadow = "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)";
		drop.style.zIndex = "100";

		//children
		this.style.position = "relative";
		this.style.display = "inline-block";
		this.appendChild(input_wrapper);
		this.appendChild(drop);

		//disabled css
		let css = this.$tag_name + "[disabled]{pointer-events:none;opacity:0.7;}";
		let style = document.createElement("style");
		if (style.styleSheet) style.styleSheet.cssText = css;
		else style.appendChild(document.createTextNode(css));

		this.appendChild(style);

		//set
		this.$dropdown = drop;
		this.$input = input;
		this.$hidden = hidden;
		this.$action = action;
		this.$open = false;

		//listeners
		let isHover = (elem) => {
			return elem.parentElement.querySelector(":hover") === elem;
		};
		this.$action.addEventListener("click", (event) => {
			if (this.$open) this.hideDropdown();
			else {
				if (this.value.trim().length){
					this.setValue();
					this.showDropdown();
				}
				else this.showDropdown();
			}
		});
		this.$input.addEventListener("focus", (event) => {
			this.showDropdown();
		});
		this.$input.addEventListener("input", (event) => {
			this.setValue(null, false, true);
			this.showDropdown();
		});
		document.addEventListener("click", (event) => {
			if (!(isHover(this.$action) || isHover(this.$input) || isHover(this.$dropdown))) this.hideDropdown();
		});

		//complete
		this.$ready = true;
		this.__emitEvent("created", false);
	}
	populateDropdown(items){}
	showDropdown(){
		if (!this.$ready || this.disabled || !this.__emitEvent("show-dropdown", true)) return;
		this.$action.innerHTML = "<i class='uk-icon-caret-up'></i>";
		this.$dropdown.innerHTML = "<div style='padding:10px;color:#888;'>Loading...</div>";
		this.$dropdown.style.display = "block";
		this.$open = true;
		this.$input.focus();
		this.__fetchData(this.$input.value.trim(), (items) => this.__populate(items));
	}
	hideDropdown(){
		if (!this.$ready) return;
		if (this.value.trim().length) this.$action.innerHTML = "<i class='uk-icon-close'></i>";
		else {
			this.$input.value = "";
			this.$action.innerHTML = "<i class='uk-icon-caret-down'></i>";
		}
		this.$dropdown.style.display = "none";
		this.$open = false;
	}
	setValue(item, init_value, on_input){
		this.new_item = item;
		if (!init_value && !this.__emitEvent("input", true)) return;
		let value = "";
		if ("object" === typeof item && item) value = this.__stringify(item);
		else if (!item) value = "";
		else value = (item + "").trim();
		let template, input_value = value;
		if (item && this.$attrs.hasOwnProperty("template") && (template = this.$attrs["template"].trim()).length) input_value = this.__template(item, template);
		if (!on_input) this.$input.value = input_value;
		this.$hidden.value = value;
		this.current_value = value;
		if (!on_input) this.hideDropdown();
		if (!on_input) this.__fetchData(this.$input.value.trim(), () => {});
		if (!init_value) this.__emitEvent("change");
	}
	constructor(){
		super();
		this.new_item = null;
		this.current_value = null;
		this.emit_ready = false;
		this.$tag_name = "uk-autocomplete";
		this.input_placeholder = "";
		this.__set_attribute = false;
		this.$attrs = {};
		this.$input = null;
		this.$hidden = null;
		this.$action = null;
		this.$dropdown = null;
		this.$ready = false;
		this.$open = false;
		this.$fetch = null;
		this.$last_query = null;
		this.$last_query_items = null;
	}
	connectedCallback(){
		this.__create();
	}
	disconnectedCallback(){}
	static get observedAttributes(){
		return ["placeholder", "name", "icon", "value", "fetch", "item-template", "template"];
	}
	attributeChangedCallback(name, oldValue, newValue){
		if ((newValue = (newValue + "").trim()).length && newValue != oldValue && oldValue !== null && !this.__set_attribute){
			switch (name){
				case "placeholder":
				if (this.$ready) this.$input.setAttribute("placeholder", newValue);
				break;
				case "name":
				if (this.$ready) this.$hidden.setAttribute("name", newValue);
				break;
				case "value":
				this.value = newValue;
				break;
				case "icon":
				case "fetch":
				case "template":
				case "item-template":
				this.__create();
				break;
			}
		}
		this.__set_attribute = false;
	}
}
window.customElements.define("uk-autocomplete", UKAutoComplete);

class UKLocations extends UKAutoComplete {
	__getCurrentLocation(callback){
		if (navigator.geolocation){
			navigator.geolocation.getCurrentPosition(function(pos){
				let data = {
					timestamp: pos.timestamp,
					lat: pos.coords.latitude,
					lon: pos.coords.longitude
				};
				if ("function" === typeof callback) callback(data);
			}, function(error){
				if (error.PERMISSION_DENIED){
					if ("function" === typeof callback) callback(false);
				}
				else {
					console.error(error);
					if ("function" === typeof callback) callback(null);
				}
			}, {enableHighAccuracy: true});
		}
		else if ("function" === typeof callback) callback(null);
	}
	__fetchData(query, callback, init_self){
		let docall = (data) => {
			if ("function" === typeof callback){
				if (!data) return callback(null);
				if (!Array.isArray(data) && data !== true){
					console.debug("UKAutoComplete Error! Unexpected fetch data.", data, this);
					return callback(null);
				}
				this.$last_query = query;
				this.$last_query_items = data;
				return callback(data);
			}
			return false;
		};
		query = (query + "").trim();
		if (!init_self && !query.length) return docall(true);
		if (query == this.$last_query && Array.isArray(this.$last_query_items)) return docall(this.$last_query_items);
		let fetch = this.fetch;
		if (!fetch){
			console.debug("UKAutoComplete Error! No Fetch URL", this);
			return docall(null);
		}
		if ("function" === typeof fetch){
			let result = fetch(query, init_self);
			if (result && Object.prototype.toString.call(result) === "[object Promise]"){
				Promise.resolve(result).then((res) => docall(res), (err) => {
					console.debug(err);
					docall(null);
				});
			}
			else docall(result);
		}
		else if ("string" === typeof fetch){
			fetch = fetch.trim();
			if (fetch.match(/\{\{query\}\}/ig)) fetch = fetch.replace(/\{\{query\}\}/ig, query);
			else if (fetch.match(/=\s*$/)) fetch = fetch + query;
			else {
				console.debug("AutoComplete Error! Query value not set in url");
				return docall(null);
			}
			this.__httpGet(fetch, (res) => docall(res));
		}
		else {
			console.debug("AutoComplete Error! Unsupported fetch value");
			return docall(null);
		}
	}
	__initValue(val){
		this.__fetchData("", (items) => {
			val = String(val).trim();
			if (val.length && Array.isArray(items)){
				let found = false;
				for (let i = 0; i < items.length; i ++){
					let item = items[i];
					if ("object" === typeof item && item){
						if (this.__stringify(item) == this.__stringify(val)){
							this.setValue(item, true);
							break;
						}
						for (let key in item){
							if (item.hasOwnProperty(key)){
								let item_value = item[key];
								if (item_value == val){
									found = true;
									this.setValue(item, true);
									break;
								}
							}
						}
						if (found) break;
					}
					else if (val == item){
						this.setValue(item, true);
						break;
					}
				}
			}
			if (!val.length) this.setValue(null, true);
			this.__emitReady();
		}, true);
	}
	__populateDefault(){
		let list = document.createElement("ul");
		list.style.listStyleType = "none";
		list.style.margin = "0";
		list.style.padding = "0";
		list.style.width = "100%";
		let current_location = this.__list_item("<i class='uk-icon-crosshairs'></i> Current Location");
		current_location.addEventListener("click", (event) => {
			this.__getCurrentLocation((data) => {
				this.setValue({
					type: "CURRENT",
					name: "Current Location",
					lat: data.lat,
					lon: data.lon
				});
			});
		});
		list.appendChild(current_location);
		list.appendChild(this.__list_item("Popular Locations", true));
		let popular_locations = [
			{"county":"","name":"Nairobi","type":"COUNTY","lat":-1.27615,"lon":36.839269},
			{"county":"","name":"Mombasa","type":"COUNTY","lat":-4.035931,"lon":39.662767},
			{"county":"","name":"Kiambu","type":"COUNTY","lat":-1.08135,"lon":36.785095},
			{"county":"","name":"Nakuru","type":"COUNTY","lat":-0.378619,"lon":35.926952},
			{"county":"","name":"Kisumu","type":"COUNTY","lat":-0.170611,"lon":34.852921},
		];
		for (let i = 0; i < popular_locations.length; i ++){
			let place = popular_locations[i];
			let list_item = this.__list_item("<i class='uk-icon-map-marker'></i> " + place.name);
			list_item.addEventListener("click", (event) => {
				event.preventDefault();
				this.setValue(place);
			});
			list.appendChild(list_item);
		}
		let css = ".uk-ac-itm:focus,.uk-ac-itm:hover{cursor:pointer;background:#39f !important;color:#fff !important;}";
		let style = document.createElement("style");
		if (style.styleSheet) style.styleSheet.cssText = css;
		else style.appendChild(document.createTextNode(css));
		this.$dropdown.innerHTML = "";
		this.$dropdown.appendChild(list);
		this.$dropdown.appendChild(style);
	}
	__populate(items){
		if (items === true) return this.__populateDefault();
		if (!Array.isArray(items)){
			this.$dropdown.innerHTML = "<div style='padding:10px;color:#888;'>Invalid data!</div>";
			return;
		}
		if (!items.length){
			this.$dropdown.innerHTML = "<div style='padding:10px;color:#888;'>No results.</div>";
			return;
		}
		let list = document.createElement("ul");
		list.style.listStyleType = "none";
		list.style.margin = "0";
		list.style.padding = "0";
		list.style.width = "100%";
		for (let i = 0; i < items.length; i ++){
			let item = items[i];
			if ("object" === typeof item && item.hasOwnProperty("header") && item.header && item.hasOwnProperty("text")){
				let list_item = this.__list_item(item.text, true);
				list.appendChild(list_item);
			}
			else {
				let template;
				if (this.$attrs.hasOwnProperty("item-template") && (template = this.$attrs["item-template"].trim()).length) template = this.__template(item, template);
				else template = this.__template(item, "");
				let list_item = this.__list_item(template, false);
				list_item.addEventListener("click", (event) => {
					event.preventDefault();
					this.setValue(item);
				});
				list.appendChild(list_item);
			}
		}
		let css = ".uk-ac-itm:focus,.uk-ac-itm:hover{cursor:pointer;background:#39f !important;color:#fff !important;}";
		let style = document.createElement("style");
		if (style.styleSheet) style.styleSheet.cssText = css;
		else style.appendChild(document.createTextNode(css));
		this.$dropdown.innerHTML = "";
		this.$dropdown.appendChild(list);
		this.$dropdown.appendChild(style);
	}
	constructor(){
		super();
		this.$tag_name = "uk-locations";
	}
}
window.customElements.define("uk-locations", UKLocations);
