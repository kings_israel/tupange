//TODO  update value, init value
class MapInput extends HTMLElement {
	get disabled(){
		return this.hasAttribute("disabled");
	}
	set disabled(val){
		if (val) this.__setAttribute("disabled", "");
		else this.removeAttribute("disabled");
	}
	get value(){
		if (!this.$ready) return "";
		return this.$hidden.value;
	}
	set value(val){
		if (!this.$ready) return "";
		try {
			let loc = null;
			if ("object" === typeof val && val) loc = val;
			else {
				val = val.replace(/\'/g, "\"");
				loc = JSON.parse(val);
			}
			if ("object" === typeof loc && loc && loc.hasOwnProperty("lat") && loc.hasOwnProperty("lng")){
				let type = loc.hasOwnProperty("type") ? loc.type : "UPDATE";
				let name = loc.hasOwnProperty("name") ? loc.name : "Custom";
				if (!this.__setMapLocation(loc.lat, loc.lng, false, type, name)) return "";
			}
			else throw ("Invalid value!");
		}
		catch (e){
			console.error("MapInput Exception:", e);
			return "";
		}
		return this.value;
	}
	get markerTitle(){
		if (!this.$ready) return "";
		return this.marker_title;
	}
	set markerTitle(val){
		if (!this.$ready) return "";
		val = ("" + val).trim();
		this.marker_title = title;
		if (this.last_marker) this.last_marker.setTitle(val);
		if (this.hasAttribute("marker-title")) this.__setAttribute("marker-title", val);
		return this.markerTitle;
	}
	get zoom(){
		return this.$ready ? this.$google_map.getZoom() : this.map_zoom;
	}
	set zoom(val){
		if (!isNaN(val = Number(val)) && val >= 0){
			this.map_zoom = val;
			if (this.$ready){
				this.$google_map.setZoom(this.map_zoom);
			}
		}
		return this.zoom;
	}
	get location(){
		try {
			let loc = JSON.parse(this.value);
			return loc;
		}
		catch (e){}
		return null;
	}
	set location(val){
		this.value = val;
		return this.location;
	}
	__setAttribute(name, value){
		value = value + "";
		if (this.hasAttribute(name) && this.__getAttribute(name) == value) return;
		this.__set_attribute = true;
		return this.setAttribute(name, value);
	}
	__getAttribute(name){
		let value = null;
		if (!(this.hasAttribute(name) && "string" === typeof (value = this.getAttribute(name)) && (value = value.trim()).length)) value = null;
		return value;
	}
	__emitEvent(name, cancelable){
		if (!("string" === typeof name && (name = name.trim()).length)) return false;
		let event = document.createEvent("Event");
		event.initEvent(name, false, !!cancelable);
		return this.dispatchEvent(event);
	}
	__emitReady(){
		if (this.emit_ready) return;
		this.__emitEvent("ready", false);
		this.emit_ready = true;
	}
	__removeMarkers(){
		this.$markers.forEach(marker => marker.setMap(null));
		this.$markers = [];
	}
	__addMarker(latlng, title){
		let marker = new google.maps.Marker({
			position: latlng,
			map: this.$google_map,
			title: title,
			draggable: true,
		});
		marker.addListener("dragend", (event) => {
			let self = this;
			let lat = event.latLng.lat();
			let lng = event.latLng.lng();
			let geocoder = new google.maps.Geocoder();
			geocoder.geocode({'latLng': new google.maps.LatLng(lat, lng)}, function(results, status){
				if (status == google.maps.GeocoderStatus.OK && results && results.length) self.__setMapLocation(lat, lng, false, "DRAG", results[0].formatted_address);
				else self.__setMapLocation(lat, lng, false, "DRAG", "Custom");
				if (!self.self_change) self.self_change = true;
			});
		});
		this.last_marker = marker;
		this.$markers.push(marker);
	}
	__setMapLocation(lat, lng, no_center, type, name){
		if (!this.$map_ready) return false;
		if (!this.emit_ready && !this.__emitEvent("change", true)) return false;
		let map_center = new google.maps.LatLng(lat, lng);
		this.__removeMarkers();
		this.__addMarker(map_center, this.marker_title);
		if (!no_center) this.$google_map.setCenter(map_center);
		this.setValue(lat, lng, type, name);
		return true;
	}
	__initMap(){
		if (!this.$ready){
			if (this.$input) this.$input.setAttribute("placeholder", "ERROR LOADING MAP!");
			console.error("MapInput initMap Error: Control not ready!");
			return;
		}
		if (!(window.hasOwnProperty("google") && "object" === typeof google && google.hasOwnProperty("maps"))){
			if (this.$input) this.$input.setAttribute("placeholder", "ERROR LOADING MAP!");
			console.error("MapInput initMap Error: Google maps library is not available!");
			return;
		}
		let default_place = {lat: -1.2835201, lon: 36.8218568};
		let map_center = new google.maps.LatLng(default_place.lat, default_place.lon);
		let map_options = {
			zoom: this.map_zoom,
			disableDefaultUI: true,
			scrollwheel: false,
			zoomControl: true,
			navigationControl: true,
			mapTypeControl: false,
			scaleControl: true,
			draggable: true,
			center: map_center,
			mapTypeId: "roadmap",
			draggableCursor: "default",
			styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
		};
		this.$google_map = new google.maps.Map(this.$map, map_options);
		let search_box = new google.maps.places.SearchBox(this.$input);
		this.$google_map.addListener("bounds_changed", () => search_box.setBounds(this.$google_map.getBounds()));
		search_box.addListener("places_changed", (event) => {
			let places = search_box.getPlaces();
			if (places.length == 0) return;
			let bounds = new google.maps.LatLngBounds();
			if (places.length){
				let place = places[0];
				if (!place.geometry){
					console.debug("place with no geometry", place);
					return;
				}
				let loc = place.geometry.location; //place.name
				if (!this.__setMapLocation(loc.lat(), loc.lng(), false, "PLACE", place.name)) return this.__eventPrevent(event);
				if (!this.self_change) this.self_change = true;
				if (place.geometry.viewport) bounds.union(place.geometry.viewport);
				else bounds.extend(place.geometry.location);
			}
			this.$google_map.fitBounds(bounds);
		});
		this.$map_ready = true;
		this.$google_map.addListener('click', (event) => {
			if (!this.__setMapLocation(event.latLng.lat(), event.latLng.lng(), true, "CLICK", "Custom")) return this.__eventPrevent(event);
			if (!this.self_change) this.self_change = true;
	    });
		this.__setMapLocation(default_place.lat, default_place.lon, false, "DEFAULT", "Nairobi");
		this.__initValue();
		if (this.$input) this.$input.setAttribute("placeholder", this.input_placeholder);
		if (!window.hasOwnProperty("map-inputs")) window["map-inputs"] = [];
		window["map-inputs"].push(this.$google_map);
	}
	__eventPrevent(event){
		try {
			if (!event) return false;
			if (event) event.stop();
			event.cancelBubble = true;
			if (event.stopPropagation) event.stopPropagation();
			if (event.preventDefault) event.preventDefault();
			else event.returnValue = false;
		}
		catch (e){}
		return false;
	}
	__initValue(){
		let value;
		if (this.$attrs.hasOwnProperty("value") && (value = this.$attrs["value"].trim()).length){
			this.value = value;
			if (!this.self_change) this.self_change = true;
		}
		this.__emitReady();
	}
	build(){
		//set attrs
		this.$attrs = {};
		let attrs = MapInput.observedAttributes;
		for (let i = 0; i < attrs.length; i ++){
			let attr = attrs[i];
			let value = this.__getAttribute(attr);
			if ("string" === typeof value) this.$attrs[attr] = value.trim();
		}

		//destroy
		this.$ready = false;
		while (this.firstChild) this.removeChild(this.firstChild);

		//wrapper
		let input_wrapper = document.createElement("div");
		input_wrapper.className = "map-input-wrapper";
		input_wrapper.style.display = "inline-block";
		input_wrapper.style.position = "relative";
		input_wrapper.style.width = "100%";

		//input
		let input = document.createElement("input");
		input.className = "map-input-search";
		input.type = "text";
		input.style.display = "block";
		input.style.float = "left";
		input.style.height = "30px";
		input.style.width = "calc(100% - 30px)";
		input.style.margin = "0";
		input.style.verticalAlign = "middle";
		input.style.font = "inherit";
		input.style.padding = "4px 6px";
		input.style.border = "1px solid #ddd";
		input.style.background = "#fff";
		input.style.color = "#444";
		input.style.transition = "all .2s linear";
		input.style.transitionProperty = "border,background,color,box-shadow,padding";
		input.style.borderRadius = "0px";
		input.style.paddingRight = "30px";
		input.setAttribute("placeholder", "Please wait...");
		if (this.$attrs.hasOwnProperty("placeholder")) this.input_placeholder = this.$attrs["placeholder"];
		if (this.$attrs.hasOwnProperty("marker-title")) this.marker_title = this.$attrs["marker-title"];
		input_wrapper.appendChild(input);

		//input icon
		let icon;
		if (this.$attrs.hasOwnProperty("icon") && (icon = this.$attrs["icon"].trim()).length){
			input.style.paddingLeft = "30px";
			input.style.width = "calc(100% - 60px)";
			let input_icon = document.createElement("span");
			input_icon.innerHTML = "<i class='uk-icon-" + icon + "'></i>";
			input_icon.style.color = "#999";
			input_icon.style.position = "absolute";
			input_icon.style.width = "30px";
			input_icon.style.height = "30px";
			input_icon.style.display = "flex";
			input_icon.style.alignItems = "center";
			input_icon.style.justifyContent = "center";
			input_icon.style.left = "0";
			input_icon.style.textDecoration = "none";
			input_wrapper.appendChild(input_icon);
		}

		//hidden
		let name, hidden = document.createElement("input");
		hidden.type = "hidden";
		if (this.$attrs.hasOwnProperty("name") && (name = this.$attrs["name"].trim()).length) hidden.setAttribute("name", name);
		input_wrapper.appendChild(hidden);

		//map
		let map = document.createElement("div");
		map.className = "map-input-map";
		map.style.display = "block";
		map.style.width = "100%";
		map.style.minHeight = "250px";
		map.style.background = "#eee";

		//children
		this.style.position = "relative";
		this.style.display = "inline-block";
		this.appendChild(input_wrapper);
		this.appendChild(map);

		//set
		this.$input = input;
		this.$hidden = hidden;
		this.$map = map;
		let map_zoom; if (this.$attrs.hasOwnProperty("zoom") && !isNaN(map_zoom = Number(this.$attrs["zoom"])) && map_zoom > 0) this.map_zoom = map_zoom;

		//listeners
		document.addEventListener("DOMContentLoaded", (event) => {
			this.__initMap();
		});

		//complete
		this.$ready = true;
		this.__emitEvent("create", false);
	}
	setValue(lat, lng, type, name){
		if (!type) type = "";
		if (!name) name = "";
		let value = JSON.stringify({lat: lat, lng: lng, type: type, name: name, title: this.marker_title});
		this.$hidden.value = value;
	}
	__create(){
		this.build();
	}
	constructor(){
		super();
		this.self_change = false;
		this.input_placeholder = "";
		this.emit_ready = false;
		this.map_zoom = 17;
		this.new_item = null;
		this.last_marker = null;
		this.marker_title = "My Location";
		this.$tag_name = "map-input";
		this.__set_attribute = false;
		this.$attrs = {};
		this.$input = null;
		this.$hidden = null;
		this.$map = null;
		this.$google_map = null;
		this.$markers = [];
		this.$ready = false;
		this.$map_ready = false;
	}
	connectedCallback(){
		this.__create();
	}
	disconnectedCallback(){}
	static get observedAttributes(){
		return ["placeholder", "name", "icon", "value", "zoom", "marker-title"];
	}
	attributeChangedCallback(name, oldValue, newValue){
		if ((newValue = (newValue + "").trim()).length && newValue != oldValue && oldValue !== null && !this.__set_attribute){
			switch (name){
				case "placeholder":
				if (this.$ready) this.$input.setAttribute("placeholder", newValue);
				break;
				case "name":
				if (this.$ready) this.$hidden.setAttribute("name", newValue);
				break;
				case "value":
				this.value = newValue;
				break;
				case "icon":
				this.__create();
				break;
				case "zoom":
				this.zoom = newValue;
				break;
				case "marker-title":
				this.markerTitle = newValue;
				break;
			}
		}
		this.__set_attribute = false;
	}
}
window.customElements.define("map-input", MapInput);
