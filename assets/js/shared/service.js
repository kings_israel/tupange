$(function(){
	//helpers
	let action_login_dialog = (msg) => {
		UKPromptConfirm('<h2>LOGIN REQUIRED</h2><p class="uk-text-danger">' + str(msg, "Default", true) + '</p>', function(){
			window.location.href = ROOT_URL + "/login?redirect=" + encodeURIComponent(ROOT_URL + "/search");
		}, function(){
			window.location.href = ROOT_URL + "/signup?redirect=" + encodeURIComponent(ROOT_URL + "/search");
		}, '<i class="uk-icon-lock"></i> Login', "uk-button-primary", '<i class="uk-icon-user-plus"></i> Get Started', "uk-button-success", null, true);
	};

	//service favorite
	window["favorite_service_toggle"] = function(id, loading_callback){
		return new Promise((resolve, reject) => {
			id = str(id, "", true);
			if (!id.length) return reject("Invalid item reference");
			let post_url = relativeRoot() + "/favorite";
			let post_data = "toggle_like=" + encodeURIComponent(id);
			serverPOST(post_url, post_data, loading_callback).then((res) => resolve(!!res.data), function(err){
				if (SERVER_RESPONSE_DATA === 0) action_login_dialog(err);
				else notify(err, "danger");
				reject(err);
			});
		});
	};
	$(document.body).on("click", "[data-service-favorite]", function(event){
		let $link = $(this);
		if ($link[0].hasAttribute("disabled")) return event.preventDefault();
		let service_id = str($link.attr("data-service-favorite"), "", true);

		//toggle like
		let toggle_like = (liked) => {
			if (liked){
				$link.addClass("favorite");
				$link.attr("title", "Unlike");
				$link.html('<i class="uk-icon-heart"></i>');
			}
			else {
				$link.removeClass("favorite");
				$link.attr("title", "Like");
				$link.html('<i class="uk-icon-heart-o"></i>');
			}
		};

		//loading callback
		let loading_callback = (toggle) => {
			if (toggle){
				$link.addClass("loading");
				$link.attr("disabled", "disabled");
				$link.attr("title", "Please wait...");
				$link.html('<i class="uk-icon-spinner uk-icon-spin"></i>');
			}
			else {
				$link.removeClass("loading");
				$link.removeAttr("disabled");
				toggle_like($link[0].className.match(/([^-]|^)favorite/));
			}
		};

		//favorite toggle request
		favorite_service_toggle(service_id, loading_callback).then((liked) => toggle_like(liked), () => {});
	});

	//service cart ~ add
	window["service_cart_toggle_item"] = function(toggle, id, loading_callback){
		return new Promise((resolve, reject) => {
			id = str(id, "", true);
			if (!id.length) return reject("Invalid item reference");
			let post_url = relativeRoot() + "/cart";
			let post_data = (toggle ? "cart_add" : "cart_delete") + "=" + encodeURIComponent(id);
			serverPOST(post_url, post_data, loading_callback).then(function(res){
				if (toggle) badgeSet("cart", badgeGet("cart") + 1);
				else badgeSet("cart", badgeGet("cart") - 1);
				resolve(res);
			}, function(err){
				if (SERVER_RESPONSE_DATA === 0) action_login_dialog(err);
				else notify(err, "danger");
				reject(err);
			});
		});
	};
	$(document.body).on("click", "[data-service-cart]", function(event){
		let service_id = str($(this).attr("data-service-cart"), "", true);
		$add = $(".service-cart-add." + service_id);
		$added = $(".service-cart-added." + service_id);
		$loading = $(".service-cart-loading." + service_id);
		if (!$add.length || !$added.length || !$loading.length){
			console.error("Unable to find service cart controls: " + service_id, $add, $added, $loading);
			return event.preventDefault();
		}
		if ($add[0].hasAttribute("disabled")) return event.preventDefault();

		//loading callback
		let loading_callback = (toggle) => {
			if (toggle){
				$add.addClass("x-hidden");
				$added.addClass("x-hidden");
				$loading.html('<i class="uk-icon-spinner uk-icon-spin"></i> Adding');
				$loading.removeClass("x-hidden");
			}
			else $loading.addClass("x-hidden");
		};

		//add to cart request
		service_cart_toggle_item(true, service_id, loading_callback).then((res) => {
			$added.removeClass("x-hidden");
			$added.attr("data-cart-delete", res.data.id);
		}, () => {
			$add.removeClass("x-hidden");
		});
	});

	//service cart ~ delete
	$(document.body).on("click", "[data-cart-delete]", function(event){
		let $added = $(this);
		let cart_id = str($added.attr("data-cart-delete"), "", true);
		if (!cart_id.length) return event.preventDefault();
		$parent = $added.parent();
		$add = $parent.find(".service-cart-add");
		$loading = $parent.find(".service-cart-loading");
		if (!$add.length || !$added.length || !$loading.length){
			console.error("Unable to find service cart controls: " + cart_id, $add, $added, $loading);
			return event.preventDefault();
		}
		if ($added[0].hasAttribute("disabled")) return event.preventDefault();

		//loading callback
		let loading_callback = (toggle) => {
			if (toggle){
				$add.addClass("x-hidden");
				$added.addClass("x-hidden");
				$loading.html('<i class="uk-icon-spinner uk-icon-spin"></i> Removing');
				$loading.removeClass("x-hidden");
			}
			else $loading.addClass("x-hidden");
		};

		//confirm remove from cart
		UKPromptConfirm('<p class="uk-text-danger">Do you want to remove this item from your cart?</p>', function(){
			//delete cart item request
			service_cart_toggle_item(false, cart_id, loading_callback).then(() => {
				$add.removeClass("x-hidden");
				$added.removeAttr("data-cart-delete");
			}, () => {
				$added.removeClass("x-hidden");
			});
		}, null, "Remove", "uk-text-danger");
	});
});
