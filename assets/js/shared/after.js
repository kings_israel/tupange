$(function(){
	//shared functions loaded by jquery
	//show on load
	$(".onload-show").removeClass("x-display-none");
	$(".onload-show").removeClass("x-hidden");
	$(".onload-show").show();

	//watermarked
	Array.from(document.querySelectorAll(".x-watermarked")).forEach(function(elem){
		elem.dataset.watermark = (elem.dataset.watermark + " ").repeat(300);
	});

	//trigger resize
	emitResize();
});
