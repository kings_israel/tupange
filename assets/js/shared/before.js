const REFRESH_TIMEOUT = (15 * 60 * 1000); //refresh every 15 minutes

$(function(){
	//jquery custom extensions
	$.fn.extend({
		//check checked status
		isChecked: function(){
			if (["radio", "checkbox"].indexOf($(this)[0].type.toLowerCase()) < 0) return false;
			let checked = $(this)[0].checked;
			if ($(this).attr("checked") && !checked) setTimeout(() => {
				if (!$(this)[0].checked) $(this).removeAttr("checked");
			}, 500);
			return checked;
		},

		//set input checked status
		setChecked: function(val, trigger_click){
			let is_checked = !!val;
			if (trigger_click) $(this).trigger("click");
			$(this)[0].checked = is_checked;
			$(this).prop("checked", is_checked);
			if (is_checked) $(this).attr("checked", "checked");
			else $(this).removeAttr("checked");
			return is_checked;
		},

		//radio input.groupVal() returns checked value
		groupVal: function(val, trigger_click){
			try {
				//radio controls only
				let ctrl = $(this);
				if (ctrl[0].type.toLowerCase() != "radio") throw "groupVal is used on radio controls";

				//return current value
				if (["string", "number"].indexOf(typeof val) < 0) return ctrl.filter(':checked').val();

				//clear current selection
				ctrl.each(function(){
					$(this).setChecked();
				});

				//check value
				let input = $(this).filter("[value='" + val + "']");
				if (input.length){
					input.setChecked(true, trigger_click);
					return val;
				}
			}
			catch (e){
				return null;
			}
		},

		//scroll to element
		scrollToMe: function($wrapper, scroll_pad, callback){
			let $self = $(this);
			let is_win_visible = isWinVisible($self[0]);
			if (is_win_visible === true) return $self;
			$wrapper = $($wrapper);
			if (!$wrapper.length){
				let scroll_parent = getScrollParent($self[0]);
				if (scroll_parent !== null){
					if (isWinVisible(scroll_parent) !== true){
						scroll_parent.scrollTop = 0;
						scroll_parent.scrollLeft = 0;
					}
					$wrapper = $(scroll_parent);
				}
				else $wrapper = $('html, body');
			}
			let offset = $self.offset();
			console.log($wrapper, $wrapper.offset());
			$wrapper.animate({scrollTop: offset.top - num(scroll_pad), scrollLeft: offset.left - num(scroll_pad)}, {
				complete: function(){
					if ('function' === typeof callback) callback();
				}
			});
			return $self;
		},

		//flash green
		flashGreen: function(seconds, delay){
			delay = num(delay, 100);
			let flash_classes = ["x-flash-green-infinite", "x-flash-green-1", "x-flash-green-2", "x-flash-green-3"];
			seconds = num(seconds, 3);
			if (seconds < 0 || seconds >= flash_classes.length) seconds = flash_classes.length - 1;
			let flash_clear = () => {
				for (let i = 0; i < flash_classes.length; i ++) $(this).removeClass(flash_classes[i]);
			};
			flash_clear();
			setTimeout(() => {
				$(this).addClass(flash_classes[seconds]);
				if (seconds) setTimeout(flash_clear, seconds * 1000);
			}, delay);
			return $(this);
		},

		//get input control type
		getInputType: function(){
			let $self = $(this), node_name = $self[0].nodeName.toLowerCase();
			if (node_name == "input"){
				let type = str($self.attr("type"), "", true).toLowerCase();
				let input_types = ["radio", "checkbox", "number", "text", "hidden"];
				if (input_types.indexOf(type) > -1) return type;
				else return "input";
			}
			return node_name;
		},

		//get input control on change/input event
		getInputEvent: function(){
			let input_type = $(this).getInputType();
			if (["input", "textarea", "number", "text"].indexOf(input_type) > -1) return "input";
			return "change";
		},

		//get input control value
		getValue: function(){
			let $self = $(this), input_type = $self.getInputType(), val = "";
			if (input_type == "radio") val = $self.groupVal();
			else if (input_type == "checkbox") val = $self.isChecked();
			else if (input_type == "number") val = String(num($self.val(), ""));
			else val = $self.val();
			return val;
		},

		//set input control error
		inputError: function(err, scroll_to_me){
			err = str(err, "", true);
			let $self = $(this), $parent = $self.parent();
			if ($parent[0].className.indexOf("uk-form-icon") > -1) $parent = $parent.parent();
			$parent.find(".x-input-error").remove();
			$self.removeClass("uk-form-danger");
			if (err.length){
				$self.addClass("uk-form-danger");
				$parent.append($('<div class="x-input-error"><small class="uk-text-danger">' + err + '</small></div>'));
				let updated = false;
				$self.one("input", () => {
					if (updated) return;
					updated = true;
					$self.inputError();
				});
				$self.one("change", () => {
					if (updated) return;
					updated = true;
					$self.inputError();
				});
				if (scroll_to_me) $self.scrollToMe();
			}
			return $self;
		},

		//scroll to bottom
		scrollBottom: function(){
			$(this).scrollTop($(this)[0].scrollHeight);
			return $(this);
		},

		//disable toggle
		toggleDisable: function(toggle){
			let $self = $(this);
			if (toggle){
				$self.addClass('uk-disabled');
				$self.attr('disabled', 'disabled');
				$self[0].disabled = true;
			}
			else {
				$self.removeClass('uk-disabled');
				$self.removeAttr('disabled');
				$self[0].disabled = false;
			}
			return $self;
		}
	});

	//jquery ajax helper
	window['objectParams'] = function(obj, parent_key){
		let params = '';
		parent_key = str(parent_key, '', true);
		if (isObj(obj, true)){
			for (let key in obj){
				if (obj.hasOwnProperty(key)){
					let param_key = parent_key.length ? parent_key + '[' + key + ']' : key;
					let value = obj[key];
					let param_pair = '';
					if (isObj(value, true)) param_pair = objectParams(value, param_key);
					else param_pair = param_key + '=' + encodeURIComponent(value);
					params += params.length ? '&' + param_pair : param_pair;
				}
			}
		}
		return params;
	};
	window["serverRequest"] = function(url, data, loading_callback, custom_options, raw_response){
		return new Promise((resolve, reject) => {
			if (!window.jQuery) return reject("jQuery library is required!");
			let toggle_loading = (toggle) => {
				if (isFunc(loading_callback)) loading_callback(!!toggle);
			};
			let ajax_callback = (server_response, text_status, request, failed) => {
				toggle_loading();
				if (failed) return reject("Internal Error! Request failed.");
				if (raw_response) return resolve({response: server_response, text_status: text_status, request: request});
				let response = serverResponse(server_response, (res) => {});
				if (response === false) return reject(SERVER_RESPONSE_ERROR);
				return resolve(response);
			};
			toggle_loading(true);
			let ajax_options = {
				type: "GET",
				url: str(url, "", true),
				data: "undefined" === typeof data ? null : data,
				dataType: "text",
			};
			if (isObj(custom_options, true, true).length){
				for (let key in custom_options){
					if (custom_options.hasOwnProperty(key)){
						if (key == "raw_response") raw_response = custom_options[key];
						else ajax_options[key] = custom_options[key];
					}
				}
			}
			ajax_options.success = (data, textStatus, request) => ajax_callback(data, textStatus, request);
			ajax_options.error = (request, textStatus, errorThrown) => ajax_callback(errorThrown, textStatus, request, true);
			$.ajax(ajax_options);
		});
	};
	window["serverGET"] = function(url, loading_callback){
		return serverRequest(url, null, loading_callback);
	};
	window["serverPOST"] = function(url, data, loading_callback){
		return serverRequest(url, data, loading_callback, {
			type: "POST",
		});
	};
	window["selfPOST"] = function(data, loading_callback){
		return serverPOST("", data, loading_callback);
	};
	window["selfGET"] = function(loading_callback){
		return serverGET("", data, loading_callback);
	};
	window["notify"] = function(prompt, status, pos){
		pos = str(pos, "top-center", true);
		status = str(status, "primary", true);
		UIkit.notify(prompt, {pos: pos, status: status});
	};
	window["badgeGet"] = function(type){
		type = str(type, "", true);
		$badge = $(".x-badge-count." + type);
		if ($badge.length) return num($($badge[0]).html());
		else console.error("Badge " + type + "not found!");
		return 0;
	};
	window["badgeSet"] = function(type, value){
		value = num(value);
		type = str(type, "", true);
		$badge = $(".x-badge-count." + type);
		if ($badge.length){
			if (value > 0) $badge.removeClass("x-hidden");
			else $badge.addClass("x-hidden");
			$badge.html(String(value));
		}
		else console.error("Badge " + type + "not found!");
		return String(value);
	};
	window["loading_block_modal"] = null;
	window["loading_block"] = (toggle, message) => {
		if (!(window.hasOwnProperty("UKBlock") && isFunc(window.UKBlock))){
			console.error("UKBlock is not defined!");
			return false;
		}
		if (toggle){
			message = str(message, '<i class="uk-icon-spinner uk-icon-spin"></i> Please wait...', true);
			if (!loading_block_modal) loading_block_modal = UKBlock(message, null, function(){
				loading_block_modal = null;
			});
			loading_block_modal.show();
		}
		else if (loading_block_modal) loading_block_modal.close();
	};

	//uk responsive class toggle
	window.onresize = function(event){
		$('[uk-width-xxs]').each(function(index){
			let width_class = str($(this).attr('uk-width-xxs'), '', true);
			if (width_class.length){
				if (displayWidth() >= 375) $(this).removeClass(width_class);
				else $(this).addClass(width_class);
			}
		});
		$('[uk-width-xs]').each(function(index){
			let width_class = str($(this).attr('uk-width-xs'), '', true);
			if (width_class.length){
				if (displayWidth() >= 425) $(this).removeClass(width_class);
				else $(this).addClass(width_class);
			}
		});
		$('[uk-width-small]').each(function(index){
			let width_class = str($(this).attr('uk-width-small'), '', true);
			if (width_class.length){
				if (!isUKSmall()) $(this).removeClass(width_class);
				else $(this).addClass(width_class);
			}
		});
		$('[uk-width-medium]').each(function(index){
			let width_class = str($(this).attr('uk-width-medium'), '', true);
			if (width_class.length){
				if (!isUKMedium()) $(this).removeClass(width_class);
				else $(this).addClass(width_class);
			}
		});
		$('[uk-width-large]').each(function(index){
			let width_class = str($(this).attr('uk-width-large'), '', true);
			if (width_class.length){
				if (!isUKLarge()) $(this).removeClass(width_class);
				else $(this).addClass(width_class);
			}
		});
		$('[uk-width-xl]').each(function(index){
			let width_class = str($(this).attr('uk-width-xl'), '', true);
			if (width_class.length){
				if (displayWidth() >= 1024) $(this).removeClass(width_class);
				else $(this).addClass(width_class);
			}
		});
	};

	//refresh messages badge
	let getUnreadCount = () => {
		let post_url = relativeRoot() + '/messages';
		let post_data = 'get_unread_count=1';
		serverPOST(post_url, post_data, null).then((res) => {
			let current_count = num(badgeGet('messages'));
			let new_count = num(res.data.count);
			if (current_count != new_count) $(document).trigger("new_messages");
			badgeSet('messages', new_count);
		}, (err) => {});
	};
	getUnreadCount();
	window.get_unread_count_timer = setInterval(getUnreadCount, REFRESH_TIMEOUT);
});

//notifications
$(function(){
	//notifications toggle
	let $notifications_drop = $('.x-notifications-drop');
	let $notifications_toggle = $('.x-notifications-toggle');
	$notifications_toggle.click(function(event){
		let $dropdown = $(this).parent().find('.x-notifications-drop');
		if (!$dropdown.length) return event.preventDefault();
		let is_hidden = $dropdown[0].className.indexOf('x-hidden') > -1;
		$notifications_drop.addClass('x-hidden');
		if (is_hidden) $dropdown.removeClass('x-hidden');
		else $dropdown.addClass('x-hidden');
	});

	//get notifications
	let notification_template = (data) => {
		if (!hasKeys(data, 'id', 'text', 'icon', 'time', 'status')) return;
		let html = '';
		html += '<li class="' + (data.status == 0 ? 'unread' : '') + '" data-id="' + data.id + '">';
		html += '<a class="notification-item" href="javascript:" data-id="' + data.id + '">';
		if (tstr(data.icon)) html += '<i class="uk-icon-' + data.icon + '"></i> ';
		html += data.text;
		html += '<small>' + data.time + '</small>';
		html += '</a>';
		html += '<div class="loading x-display-none"><i class="uk-icon-spinner uk-icon-spin"></i></div>';
		html += '</li>';
		return html;
	};
	let $notifications_list = $(".x-notifications-list");
	let $notifications_empty = $(".x-notifications-empty");
	let $notifications_badge = $('.x-badge-count.notifications');
	let $notifications_actions = $('.x-notifications-actions');
	let $notifications_mark_read = $('.x-notifications-mark-read');
	let getNotifications = (no_loading) => {
		let post_url = relativeRoot() + '/notifications';
		let post_data = 'get_notifications=1';
		if (!no_loading){
			$notifications_list.html('');
			$notifications_actions.addClass('x-hidden');
			$notifications_mark_read.addClass('x-hidden');
			$notifications_drop.removeClass('unread');
			$notifications_badge.addClass('x-hidden');
			$notifications_list.addClass('x-hidden');
			$notifications_empty.find('p').html('Loading notifications...');
			$notifications_empty.removeClass('x-hidden');
		}
		serverPOST(post_url, post_data, null).then((res) => {
			let items = res.data;
			let unread_count = 0;
			if (!(Array.isArray(items) && items.length)) items = [];
			for (let i = 0; i < items.length; i ++){
				let item = items[i];
				let html = notification_template(item);
				if (!html){
					console.error('Invalid notification item object', item);
					continue;
				}
				if (!item.status) unread_count ++;
				let $item = $(html);
				$notifications_list.append($item);
			}
			if ($notifications_list.children().length){
				$notifications_empty.addClass('x-hidden');
				$notifications_list.removeClass('x-hidden');
				$notifications_actions.removeClass('x-hidden');
				if (unread_count){
					badgeSet('notifications', unread_count);
					$notifications_badge.removeClass('x-hidden');
					$notifications_drop.addClass('unread');
					$notifications_mark_read.removeClass('x-hidden');
				}
				else {
					badgeSet('notifications', unread_count);
					$notifications_badge.addClass('x-hidden');
					$notifications_drop.removeClass('unread');
					$notifications_mark_read.addClass('x-hidden');
				}
			}
			else {
				$notifications_actions.addClass('x-hidden');
				$notifications_drop.removeClass('unread');
				$notifications_list.addClass('x-hidden');
				$notifications_empty.removeClass('x-hidden');
				$notifications_empty.find('p').html('You have no new notifications.<br>Enjoy your day!<br><br><button class="x-notifications-refresh uk-button uk-button-small uk-button-default">Refresh</button>');
			}
		}, (err) => {
			$notifications_actions.addClass('x-hidden');
			$notifications_drop.removeClass('unread');
			$notifications_list.addClass('x-hidden');
			$notifications_empty.removeClass('x-hidden');
			$notifications_empty.find('p').html(err);
			$notifications_empty.find('p').addClass('uk-text-danger');
		});
	};

	//mark all read
	$notifications_mark_read.click(function(event){
		let $self = $(this);
		let $dropdown = $self.parent().parent();
		let html_bak = $self.html();
		let loading_callback = (toggle) => {
			if (toggle){
				$self.html('<i class="uk-icon-spinner uk-icon-spin"></i> Please wait...');
				$dropdown.addClass('x-block');
			}
			else {
				$self.html(html_bak);
				$dropdown.removeClass('x-block');
			}
		};
		let post_url = relativeRoot() + '/notifications';
		let post_data = 'mark_notifications_read=1';
		serverPOST(post_url, post_data, loading_callback).then((res) => {
			console.log('res', res);
			clearInterval(window.get_notifications_timer);
			getNotifications();
			window.get_notifications_timer = setInterval(() => getNotifications(true), REFRESH_TIMEOUT);
		}, (err) => UIkit.modal.alert('<p class="uk-text-danger">' + err + '</p>'));
	});

	//on notification click
	$(document.body).on("click", "a.notification-item", function(event){
		let $self = $(this);
		let id = tstr($self.attr('data-id'));
		let $parent = $('.x-notifications-list li[data-id="' + id + '"]');
		if (!id || !$parent.length) return event.preventDefault();
		let $loading = $parent.find('.loading');
		if (!$loading.length) return event.preventDefault();
		$loading.removeClass('x-display-none');
		let doAction = (action, successful) => {
			action = tstr(action);
			$loading.addClass('x-display-none');
			if (successful){
				$parent.removeClass('unread');
				$parent.parent().removeClass('unread');
			}
			if (action.match(/^https?:\/\//)) window.location.href = action;
		};
		let post_url = relativeRoot() + '/notifications';
		let post_data = 'get_notification_action=' + encodeURIComponent(id);
		serverPOST(post_url, post_data, null).then((res) => doAction(res.data, true), (err) => {
			doAction();
			UIkit.modal.alert('<p class="uk-text-danger">' + err + '</p>');
		});
	});

	//on notifications refresh
	$(document.body).on("click", "button.x-notifications-refresh", function(event){
		clearInterval(window.get_notifications_timer);
		getNotifications();
		window.get_notifications_timer = setInterval(() => getNotifications(true), REFRESH_TIMEOUT);
	});

	//on outside click
	$(document.body).on("click", function(event){
		let $target = $(event.target);
		let is_child = $target[0].className.indexOf('x-notifications-') > -1 || $target.parents('[class*=x-notifications-wrapper]').length;
		if (!is_child) $notifications_drop.addClass('x-hidden');
	});

	//refresh notifications count
	getNotifications();
	window.get_notifications_timer = setInterval(() => getNotifications(true), REFRESH_TIMEOUT);
});

//page error alert
$(function(){
	let params = {};
	params['page-error'] = true;
	getQueryParams(params, function(replace, new_url, data){
		if (!!replace) history.replaceState({}, document.title, new_url);
		if ('object' === typeof data && data && data.hasOwnProperty('page-error')) UIkit.modal.alert('<p class="uk-text-danger">' + data['page-error'] + '</p>');
	});
});

$(function() {
	$(document.body).on("click", ".switch-profile", function(event) {
		event.preventDefault()
		let $btn = $(this);
		let userid = str($btn.attr("data-id"), "", true);
		let userlevel = str($btn.attr("data-user"), "", true);
		let post_url = relativeRoot() + '/service/profile'
		let post_data = objectParams({
			user_id: userid,
			user_level: userlevel
		});
		
		serverPOST(post_url, post_data, loading_block).then(() => {
			loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
			window.location.reload();
		}, (err) => {
			UKPrompt('<p class="uk-text-warning">' + err + '</p>');
		});

	})
})
