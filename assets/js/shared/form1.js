//shared form controls
let $location = document.getElementById("location");
let $location_map = document.getElementById("location_map");

//jQuery init
$(function(){
	//location map change
	if (isElement($location) && isElement($location_map)){
		$location.addEventListener("change", function(){
			let item = this.new_item;
			if ("object" === typeof item && item && item.hasOwnProperty("lat") && item.hasOwnProperty("lon") && item.hasOwnProperty("type")){
				let latlng = {lat: item.lat, lng: item.lon, type: item.type, name: item.name};
				$location_map.location = latlng;
				$(".map-input-wrapper input.map-input-search").val("");
				if (item.type == "COUNTY") $location_map.zoom = 12;
				else if (item.type == "CITY") $location_map.zoom = 15;
				else if (item.type == "CURRENT") $location_map.zoom = 16;
			}
		});
	}
	//display image
	if ($("#display-image").length && $("#display-img").length) $("#display-image").change(function(event){
		let files = event.target.files;
		if (FileReader && files && files.length){
			let file = files[0];
			let fsize = num(file.size / (1024 ** 2)).round(2);
			if (!fsize){
				UIkit.modal.alert('<p class="uk-text-warning">Invalid image size!</p>');
				event.preventDefault();
				$("#display-image")[0].value = "";
				return;
			}
			if (fsize > 2){
				UIkit.modal.alert('<p class="uk-text-warning">Image size is larger than the allowed image size of 2MB! Current size ' + fsize + 'MB.</p>');
				event.preventDefault();
				$("#display-image")[0].value = "";
				return;
			}
			let fr = new FileReader();
			fr.onload  = () => {
				//let image_data = fr.result;
				$("#display-img").attr("src", fr.result);
				if ($("#display-img-btn").length) $("#display-img-btn").html("Change Image");
				if ($("#display-img-keep").length) $("#display-img-keep").val("1");
				if ($("#display-img-remove-btn").length) $("#display-img-remove-btn").removeClass("x-hidden");
			};
			fr.readAsDataURL(files[0]);
		}
		else console.error("Preview not supported!");
	});
	if ($("#display-image").length && $("#display-img-link").length) $("#display-img-link").click(function(event){
		event.preventDefault();
		$("#display-image").click();
	});
	if ($("#display-img-remove-btn").length && $("#display-img-keep").length && $("#display-img").length && $("#display-image").length && $("input.placeholder_image").length) $("#display-img-remove-btn").click(function(event){
		UIkit.modal.confirm("Are you sure you want to remove current image?<br><span class='uk-text-danger x-fw-400'>Changes will be applied permanently upon saving.</span>", function(){
			$("#display-image").val("");
			$("#display-img-keep").val("0");
			$("#display-img").attr("src", $("input.placeholder_image").val());
			if ($("#display-img-btn").length) $("#display-img-btn").html("Select Image");
			$("#display-img-remove-btn").addClass("x-hidden");
		});
	});
});
