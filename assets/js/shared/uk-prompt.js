$(function(){
	window["UKPromptConfirmTest"] = function(prompt_html, input_test, on_confirm, on_cancel, button_confirm_text, button_confirm_class, button_cancel_text, button_cancel_class){
		let prompt_id = uid();
		let prompt_selector = "modal-" + prompt_id;

		//defaults
		prompt_html = str(prompt_html, "<p>Comfirm test prompt.</p>", true);
		input_test = str(input_test, "Test", true);
		button_confirm_text = str(button_confirm_text, "Confirm", true);
		button_confirm_class = str(button_confirm_class, "uk-button-primary", true);
		button_confirm_class = "uk-button " + button_confirm_class.trim();
		button_cancel_text = str(button_cancel_text, "Cancel", true);
		button_cancel_class = str(button_cancel_class, "", true);
		button_cancel_class = "uk-modal-close uk-button " + button_cancel_class.trim();

		//build modal
		let html = '<div id="' + prompt_selector + '" class="uk-modal">';
		html += '<div class="uk-modal-dialog">';
		html += '<a href="" class="' + prompt_selector + ' modal-cancel uk-modal-close uk-close uk-close-alt x-noselect"></a>';
		html += '<div class="x-noselect">' + prompt_html + "</div>";
		html += '<p class="x-noselect" style="margin:10px 0 5px;font-size:14px;">Type <span class="x-fw-400">&quot;' + input_test + '&quot;</span> (without quotes) to confirm:</p>';
		html += '<div class="uk-form">';
		html += '<input type="text" class="' + prompt_selector + ' modal-input uk-width-1-1" placeholder="Input Required">';
		html += '<small class="' + prompt_selector + ' uk-text-danger x-noselect" style="display:block;font-size:12px;"></small>';
		html += '<small class="' + prompt_selector + ' modal-assist x-noselect" style="display:block;font-size:12px;">Required input is case sensitive</small>';
		html += '</div>';
		html += '<div class="uk-modal-footer uk-text-right">';
        html += '<button type="button" class="' + prompt_selector + ' modal-cancel ' + button_cancel_class + '">' + button_cancel_text + '</button> ';
        html += '<button type="button" class="' + prompt_selector + ' modal-confirm ' + button_confirm_class + '">' + button_confirm_text + '</button>';
        html += '</div>';
        html += '</div>';
        html += '</div>';

		//modal elements
		let uk_modal = null;
		let $modal = $(html).appendTo("body");
		let $modal_input = $modal.find(".uk-form input." + prompt_selector + ".modal-input");
		let $modal_input_error = $modal.find(".uk-form ." + prompt_selector + ".uk-text-danger");
		let $modal_input_assist = $modal.find(".uk-form ." + prompt_selector + ".modal-assist");
		let $modal_cancel = $modal.find("." + prompt_selector + ".modal-cancel");
		let $modal_confirm = $modal.find("." + prompt_selector + ".modal-confirm");

		//timeout check
		let last_timeout = null;

		//on input
		$modal_input.on("input", function(event){
			$(this).removeClass("uk-form-danger");
			last_timeout = null;
			$modal_input_error.html("");
			$modal_input_assist.show();
		});

		//on confirm
		$modal_confirm.click(function(event){
			let input_value = str($modal_input.val(), "", true);
			if (input_value != input_test){
				$modal_input.addClass("uk-form-danger");
				$modal_input_assist.hide();
				$modal_input_error.html("Please type \"" + input_test + "\" (without quotes) on the input provided. Required input is case sensitive.");
				let timeout = uid();
				last_timeout = timeout;
				setTimeout(() => {
					if (timeout != last_timeout) return;
					$modal_input_error.html("");
					$modal_input_assist.show();
				}, 3000);
				event.preventDefault();
				return false;
			}
			if (uk_modal && uk_modal.isActive()) uk_modal.hide();
			if ("function" === typeof on_confirm) setTimeout(() => on_confirm(input_value), 50);
		});

		//on cancel
		$modal_cancel.click(function(event){
			let cancel_result = null;
			if ("function" === typeof on_cancel) cancel_result = on_cancel();
			if (cancel_result === false){
				event.preventDefault();
				return false;
			}
		});

		//modal listeners
		$modal.on({
			"show.uk.modal": function(){
				$modal_input.focus();
			},
			"hide.uk.modal": function(){
				if (!$modal.is(":visible")){
					$modal.remove();
					let modal_wrapper = document.querySelector("#" + prompt_selector);
					if (modal_wrapper) modal_wrapper.remove();
				}
			},
		});

		//init UIkit modal
		uk_modal = UIkit.modal($modal, {
			keyboard: false,
			bgclose: false,
			center: true,
			modal: false,
		});

		//show modal
		if (uk_modal.isActive()) uk_modal.hide();
		else uk_modal.show();
	};
	window["UKPromptConfirm"] = function(prompt, on_confirm, on_cancel, button_confirm_text, button_confirm_class, button_cancel_text, button_cancel_class, other_buttons, dismiss_link){
		let prompt_id = uid();
		let prompt_selector = "modal-" + prompt_id;

		//defaults
		let button_class = (classes) => {
			return (Array.from(new Set(classes.split(" ")))).join(" ").trim();
		};
		prompt = str(prompt, "<p>Comfirm test prompt.</p>", true);
		button_confirm_text = str(button_confirm_text, "Confirm", true);
		button_confirm_class = button_class("uk-button " + str(button_confirm_class, "uk-button-primary", true));
		button_cancel_text = str(button_cancel_text, "Cancel", true);
		button_cancel_class = button_class("uk-button " + str(button_cancel_class, "", true));

		let other_buttons_htmls = [];
		let other_buttons_callbacks = {};
		if (Array.isArray(other_buttons) && other_buttons.length){
			for (let i = 0; i < other_buttons.length; i ++){
				let btn = other_buttons[i];
				if (hasKeys(btn, "text", "class", "callback")){
					let btn_text = str(btn.text, "Button " + i, true);
					let btn_class = button_class("uk-button " + str(btn.class, "uk-button", true));
					let btn_callback = btn.callback;
					if ("function" === typeof btn_callback) other_buttons_callbacks[i] = btn_callback;
					let btn_html = '<button type="button" class="other-button ' + prompt_selector + ' ' + btn_class + '" data-index="' + i + '">' + btn_text + '</button>';
					other_buttons_htmls.push(btn_html);
				}
			}
		}

		//build modal
		let html = '<div id="' + prompt_selector + '" class="uk-modal">';
		html += '<div class="uk-modal-dialog">';
		if (dismiss_link) html += '<a href="" title="Dismiss" class="uk-modal-close uk-close uk-close-alt"></a>';
		html += '<div class="x-noselect">' + prompt + "</div>";
		html += '<div class="uk-modal-footer uk-text-right">';
        html += '<button type="button" class="' + prompt_selector + ' modal-cancel ' + button_cancel_class + '">' + button_cancel_text + '</button> ';
        html += '<button type="button" class="' + prompt_selector + ' modal-confirm ' + button_confirm_class + '">' + button_confirm_text + '</button>';
		if (other_buttons_htmls.length) html += ' ' + other_buttons_htmls.join(" ");
		html += '</div>';
        html += '</div>';
        html += '</div>';

		//modal elements
		let uk_modal = null;
		let $modal = $(html).appendTo("body");
		let $modal_cancel = $modal.find("." + prompt_selector + ".modal-cancel");
		let $modal_confirm = $modal.find("." + prompt_selector + ".modal-confirm");

		//on confirm
		$modal_confirm.click(function(event){
			if (uk_modal && uk_modal.isActive()) uk_modal.hide();
			if ("function" === typeof on_confirm) setTimeout(() => on_confirm(), 50);
		});

		//on cancel
		$modal_cancel.click(function(event){
			let cancel_result = null;
			if ("function" === typeof on_cancel) cancel_result = on_cancel();
			if (cancel_result === false){
				event.preventDefault();
				return false;
			}
			uk_modal.hide();
		});

		//on other buttons
		$(document.body).on("click", ".other-button." + prompt_selector + "[data-index]", function(event){
			let index = num($(this).attr("data-index"));
			console.log("other-button click", index);
			if (other_buttons_callbacks.hasOwnProperty(index) && isFunc(other_buttons_callbacks[index])) other_buttons_callbacks[index](event);
			else return event.preventDefault();
		});

		//modal listeners
		$modal.on({
			"show.uk.modal": function(){},
			"hide.uk.modal": function(){
				if (!$modal.is(":visible")){
					$modal.remove();
					let modal_wrapper = document.querySelector("#" + prompt_selector);
					if (modal_wrapper) modal_wrapper.remove();
				}
			},
		});

		//init UIkit modal
		uk_modal = UIkit.modal($modal, {
			keyboard: true,
			bgclose: true,
			center: true,
			modal: false,
		});

		//show modal
		if (uk_modal.isActive()) uk_modal.hide();
		else uk_modal.show();
	};
	window["UKPrompt"] = function(prompt, on_close, button_text, button_class){
		let prompt_id = uid();
		let prompt_selector = "modal-" + prompt_id;

		//defaults
		prompt = str(prompt, "<p>Dialog prompt message.</p>", true);
		button_text = str(button_text, "OK", true);
		button_class = str(button_class, "uk-button-primary", true);
		button_class = "uk-button " + button_class.trim();

		//build modal
		let html = '<div id="' + prompt_selector + '" class="uk-modal">';
		html += '<div class="uk-modal-dialog">';
		html += '<div class="x-noselect">' + prompt + "</div>";
		html += '<div class="uk-modal-footer uk-text-right">';
        html += '<button type="button" class="' + prompt_selector + ' modal-ok ' + button_class + '">' + button_text + '</button>';
        html += '</div>';
        html += '</div>';
        html += '</div>';

		//modal elements
		let uk_modal = null;
		let $modal = $(html).appendTo("body");
		let $modal_ok = $modal.find("." + prompt_selector + ".modal-ok");

		//on ok
		$modal_ok.click(function(event){
			if (uk_modal && uk_modal.isActive()) uk_modal.hide();
		});

		//modal listeners
		$modal.on({
			"show.uk.modal": function(){},
			"hide.uk.modal": function(){
				if (!$modal.is(":visible")){
					$modal.remove();
					let modal_wrapper = document.querySelector("#" + prompt_selector);
					if (modal_wrapper) modal_wrapper.remove();
					if ("function" === typeof on_close) setTimeout(() => on_close(), 50);
				}
			},
		});

		//init UIkit modal
		uk_modal = UIkit.modal($modal, {
			keyboard: true,
			bgclose: true,
			center: true,
			modal: false,
		});

		//show modal
		if (uk_modal.isActive()) uk_modal.hide();
		else uk_modal.show();
	};
	window["UKModal"] = function(dialog_html, modal_class, on_show, on_close, on_data_event, keyboard, bgclose, center, modal){
		let prompt_id = uid();
		let prompt_selector = "modal-" + prompt_id;

		//defaults
		if ("undefined" === typeof keyboard) keyboard = true;
		if ("undefined" === typeof bgclose) bgclose = true;
		if ("undefined" === typeof center) center = true;
		if ("undefined" === typeof modal) modal = false;
		dialog_html = str(dialog_html, "<p>Dialog contents.</p>", true);
		modal_class = str(modal_class, "x-modal", true);

		//build modal
		let html = '<div id="' + prompt_selector + '" class="uk-modal">';
		html += '<div class="uk-modal-dialog ' + modal_class + '"> ' + dialog_html + ' </div>';
		html += '</div>';

		//modal elements
		let uk_modal = null;
		let $modal = $(html).appendTo("body");

		//modal listeners
		$(document.body).on("click", "#" + prompt_selector + " [data-event]", function(event){
			let $button = $(this);
			let data_event = str($button.attr("data-event"), "", true);
			if (data_event.length && "function" === typeof on_data_event) return on_data_event(data_event, event, $modal);
			return false;
		});
		$modal.on({
			"show.uk.modal": function(){
				if ("function" === typeof on_show) setTimeout(() => on_show($modal, uk_modal), 50);
			},
			"hide.uk.modal": function(){
				if (!$modal.is(":visible")){
					$modal.remove();
					let modal_wrapper = document.querySelector("#" + prompt_selector);
					if (modal_wrapper) modal_wrapper.remove();
					if ("function" === typeof on_close) setTimeout(() => on_close(), 50);
				}
			},
		});

		//init UIkit modal
		uk_modal = UIkit.modal($modal, {
			keyboard: keyboard, //default: true
			bgclose: bgclose, //default: true
			center: !!center, //default: false
			modal: !!modal, //default: true
			minScrollHeight: 200, //default: 150
		});

		//show modal
		if (uk_modal.isActive()) uk_modal.hide();
		else uk_modal.show();
	};
	window["UKBlock"] = function(message, on_show, on_hide, center, keyboard, bgclose, modal){
		let prompt_id = uid();
		let prompt_selector = "modal-" + prompt_id;

		//defaults
		if ("undefined" === typeof center) center = true;
		if ("undefined" === typeof keyboard) keyboard = false;
		if ("undefined" === typeof bgclose) bgclose = false;
		if ("undefined" === typeof modal) modal = false;
		message = str(message, '<i class="uk-icon-spinner uk-icon-spin"></i> Please wait...', true);

		//build modal
		let html = '<div id="' + prompt_selector + '" class="uk-modal"><div class="uk-modal-dialog x-modal-small"><div class="uk-modal-content">' + message + '</div></div></div>';
		let $modal = $(html).appendTo("body"), uk_modal = UIkit.modal($modal, {
			keyboard: keyboard, //default: true
			bgclose: bgclose, //default: true
			center: center, //default: false
			modal: !!modal, //default: true
			minScrollHeight: 150, //default: 150
		}), hidden_destroy = false, self = {
			modal: uk_modal,
			$modal: $modal,
			show: function(){
				if (!uk_modal) return;
				if (uk_modal.isActive()) uk_modal.hide();
				else uk_modal.show();
			},
			hide: function(){
				if (!uk_modal) return;
				hidden_destroy = false;
				if (uk_modal.isActive()) uk_modal.hide();
			},
			close: function(){
				if (!uk_modal) return;
				hidden_destroy = true;
				if (uk_modal.isActive()) uk_modal.hide();
			}
		};

		//listeners
		$modal.on({
			"show.uk.modal": function(){
				if ("function" === typeof on_show) setTimeout(() => on_show($modal, uk_modal), 50);
			},
			"hide.uk.modal": function(){
				if (hidden_destroy && !$modal.is(":visible")){
					$modal.remove();
					let modal_wrapper = document.querySelector("#" + prompt_selector);
					if (modal_wrapper) modal_wrapper.remove();
					$modal = null;
					uk_modal = null;
				}
				if ("function" === typeof on_hide) setTimeout(() => on_hide($modal, uk_modal), 50);
			},
		});

		//return self
		return self;
	};
});
