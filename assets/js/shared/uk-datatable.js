$(function(){
	//uk-datatable elements
	$uk_datatable = $(".uk-datatable");
	$uk_datatable_search = $(".uk-datatable .uk-datatable-search");
	$uk_datatable_pagination = $(".uk-datatable .uk-datatable-pagination");
	$uk_datatable_toggle = $(".uk-datatable input[name='uk-datatable-toggle']");
	$uk_datatable_wrapper = $(".uk-datatable .uk-datatable-wrapper");
	$uk_datatable_table_th = $(".uk-datatable .uk-datatable-wrapper table thead th:not(.x-ignore):not(.x-block)");
	$uk_datatable_table_tbody = $(".uk-datatable .uk-datatable-wrapper table tbody");
	$uk_datatable_table_tr = $(".uk-datatable .uk-datatable-wrapper table tbody tr");
	$uk_datatable_no_content = $(".uk-datatable .uk-datatable-wrapper .uk-datatable-no-contents");
	$uk_datatable_no_results = $(".uk-datatable .uk-datatable-wrapper .uk-datatable-no-results");
	$uk_datatable_table_th_row_index = $(".uk-datatable .uk-datatable-wrapper table thead th.uk-datatable-row-index:not(.x-ignore)");

	//datatable init
	window["uk_datatable_rows"] = [];
	window["init_uk_datatable_listeners"] = false;
	window["init_uk_datatable"] = () => {
		let toggle_no_results = (show) => {
			if ($uk_datatable_no_results.length){
				if (show) $uk_datatable_no_results.removeClass("x-hidden");
				else $uk_datatable_no_results.addClass("x-hidden");
			}
		};
		let toggle_no_content = (show) => {
			if ($uk_datatable_no_content.length){
				if (show) $uk_datatable_no_content.removeClass("x-hidden");
				else $uk_datatable_no_content.addClass("x-hidden");
			}
		};
		//get all rows array
		uk_datatable_rows = Array.from($uk_datatable_table_tr);

		//set row indexes
		let row_index = num($uk_datatable.attr("data-row-index"), 1);
		let setRowIndexes = () => {
			let row_num = 1;
			let row_num_asc = true;
			if (row_index == -1){
				row_num = uk_datatable_rows.length;
				row_num_asc = false;
			}
			for (let i = 0; i < uk_datatable_rows.length; i ++){
				let $row = $(uk_datatable_rows[i]);
				let cell = $row.find("td.uk-datatable-row-index");
				if (cell.length){
					cell.html(row_num + "");
					cell.attr("data-value", row_num + "");
					cell.attr("title", "Row #" + row_num);
				}
				row_num += row_num_asc ? 1 : -1;
				uk_datatable_rows[i] = $row[0];
			}
			//row index column icon
			if (!$uk_datatable_table_th_row_index.find("[class*=uk-icon-]").length){
				$uk_datatable_table_th_row_index.append(!row_num_asc ? '<i class="uk-icon-arrow-up"></i>' : '<i class="uk-icon-arrow-down"></i>');
			}
		};
		window["uk_datatable_set_row_indexes"] = setRowIndexes;
		setRowIndexes();

		//get page limit
		let page_limit = num($uk_datatable.attr("data-limit"), 5);

		//page variables
		let current_page = 1;
		let pages_count = 0;
		let sort_column = {};
		let filtered_rows = [];
		let listeners_initialized = false;

		//set pagination
		let setPagination = (rows_count, total_rows_count) => {
			//check rows count
			if (isNaN(rows_count = Number(rows_count))) rows_count = 0;
			if (isNaN(total_rows_count = Number(total_rows_count))) total_rows_count = 0;

			//remove existing pagination
			$uk_datatable_pagination.html("");

			//create new pagination
			if (!total_rows_count) return false;
			let links = [];
			if (pages_count > 1){
				if (current_page > 1) links.push('<li><a class="prev" href="javascript:"><i class="uk-icon-angle-double-left"></i></a></li>');
				else links.push('<li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>');
				let appendLinks = (start, end) => {
					if (start > 5){
						links.push('<li><a href="javascript:" class="page" data-page="1">1</a></li>');
						links.push('<li><span>...</span></li>');
					}
					for (let i = start; i <= end && i <= pages_count; i ++){
						if (current_page == i) links.push('<li class="uk-active"><span>' + i + '</span></li>');
						else links.push('<li><a href="javascript:" class="page" data-page="' + i + '">' + i + '</a></li>');
					}
					if (pages_count > 5 && end <= pages_count){
						if ((pages_count - end) > 1) links.push('<li><span>...</span></li>');
						if (current_page == pages_count) links.push('<li class="uk-active"><span>' + pages_count + '</span></li>');
						else links.push('<li><a href="javascript:" class="page" data-page="' + pages_count + '">' + pages_count + '</a></li>');
					}
				};
				if (current_page > 5){
					let diff = pages_count - current_page;
					let start = diff < 4 ? current_page - (4 - diff) : current_page;
					if (start < 1) start = 1;
					let end = start + 3;
					end = start + 3;
					if (end > pages_count) end = pages_count;
					appendLinks(start, end);
				}
				else appendLinks(1, 5);
				if (current_page < pages_count) links.push('<li><a class="next" href="javascript:"><i class="uk-icon-angle-double-right"></i></a></li>');
				else links.push('<li class="uk-disabled"><span><i class="uk-icon-angle-double-right"></i></span></li>');
			}
			let showing = "<strong>Page " + current_page + "</strong> | Showing " + rows_count + "/" + total_rows_count;
			if (total_rows_count != uk_datatable_rows.length) showing += " of " + uk_datatable_rows.length;
			showing += " results";
			let html = '<div class="uk-margin-top uk-text-center">';
			html += '<small>' + showing + '</small>';
			if (links.length) html += '<ul class="uk-pagination">' + links.join("") + '</ul>';
			html += '</div>';
			$uk_datatable_pagination.append(html);
			return true;
		};

		//pagination set page
		let setPage = (page_number) => {
			//check page number & pages_count
			if (isNaN(page_number = Number(page_number)) || page_number < 1) return false;

			//page rows
			let rows_count = 0;
			let total_rows_count = filtered_rows.length;

			//empty tbody rows
			$uk_datatable_table_tbody.html("");

			//create tbody rows
			if (total_rows_count){
				let p = (page_number - 1) * page_limit;
				let x = p + page_limit;
				for (let i = p; i < filtered_rows.length && i < x; i ++){
					rows_count ++;
					$uk_datatable_table_tbody.append(filtered_rows[i]);
				}
				toggle_no_results();
			}
			else toggle_no_results(true);

			//show tbody
			$uk_datatable_table_tbody.show();

			//set pagination
			current_page = page_number;
			return setPagination(rows_count, total_rows_count);
		};
		window["uk_datatable_set_page"] = setPage;

		//pagination filters
		let filterRows = () => {
			//check rows
			toggle_no_content();
			let rows = Array.from(uk_datatable_rows);
			if (!rows.length){
				toggle_no_content(true);
				return false;
			}

			//sort rows by column
			let sort_column_keys = Object.keys(sort_column);
			for (let i = 0; i < sort_column_keys.length; i ++){
				let key = sort_column_keys[i];
				let column_index = Number(key);
				let column_asc = sort_column[key] ? true : false;
				let temp = Array.from(rows);
				rows.sort(function(a, b){
					let tdA = column_index >= 0 && column_index < a.children.length ? a.children[column_index] : null;
					let tdB = column_index >= 0 && column_index < b.children.length ? b.children[column_index] : null;
					let textA = tdA && tdA.hasAttribute("data-value") ? tdA.getAttribute("data-value").trim() : "";
					let textB = tdB && tdB.hasAttribute("data-value") ? tdB.getAttribute("data-value").trim() : "";
					if (column_asc){
						let temp = textB;
						textB = textA;
						textA = temp;
					}
					let numA = num(textA);
					let numB = num(textB);
					if (String(numA) == textA && String(numB) == textB) return numA - numB;
					return textA.localeCompare(textB);
				});
			}

			//filter rows
			filtered_rows = [];
			let toggle_value = $uk_datatable_toggle.length ? $uk_datatable_toggle.groupVal().trim() : "";
			let search_value = $uk_datatable_search.length ? $uk_datatable_search.val().trim() : "";
			for (let i = 0; i < rows.length; i ++){
				let row = rows[i];
				let row_search = row.hasAttribute("data-search") ? row.getAttribute("data-search").trim() : "";
				let row_toggle = row.hasAttribute("data-toggle") ? row.getAttribute("data-toggle").trim() : "";
				let filter = true;
				if (search_value.length && row_search.toLowerCase().indexOf(search_value.toLowerCase()) < 0) filter = false;
				if (toggle_value.length && row_toggle.toLowerCase() != toggle_value.toLowerCase()) filter = false;
				if (filter) filtered_rows.push(row);
			}

			//set pages count
			pages_count = Math.ceil(filtered_rows.length / page_limit);

			//return on done
			return filtered_rows;
		};
		window["uk_datatable_filter_rows"] = filterRows;

		//pagination show page
		let showPage = (page_number) => {
			if (isNaN(page_number) || page_number <= 0) page_number = current_page;
			if (!filterRows()) return false;
			return setPage(page_number);
		};
		window["uk_datatable_show_page"] = showPage;

		//uk-datatable refresh page
		let refreshPage = () => {
			setRowIndexes();
			//clear rows
			$uk_datatable_table_tbody.html("");
			$uk_datatable_table_tbody.hide();

			if (!filterRows()) return false;
			if (pages_count >= 1){
				while (current_page > pages_count) current_page --;
				setPage(current_page);
			}
		};
		window["uk_datatable_refresh"] = refreshPage;

		//init listeners
		if (!init_uk_datatable_listeners){
			init_uk_datatable_listeners = true;

			//on search
			if ($uk_datatable_search.length) $uk_datatable_search.on("input", () => showPage(1));

			//on toggle
			if ($uk_datatable_toggle.length) $uk_datatable_toggle.on("change", () => showPage(1));

			//on sort column
			$uk_datatable_table_th.click(function(){
				let column_icon = true;
				let column_index = $(this).index();
				$(this).find("[class*=uk-icon-]").remove();
				if (!sort_column.hasOwnProperty(column_index)) sort_column[column_index] = false;
				else {
					if ($(this)[0].className.indexOf("uk-datatable-row-index") < 0 && sort_column[column_index]){
						column_icon = false;
						delete sort_column[column_index];
					}
					else sort_column[column_index] = !sort_column[column_index];
				}
				if (column_icon) $(this).append(sort_column[column_index] ? '<i class="uk-icon-arrow-up"></i>' : '<i class="uk-icon-arrow-down"></i>');
				showPage(1);
			});

			//on pagination link next
			$(document.body).on("click", ".uk-datatable-pagination li > a.next", function(event){
				if (pages_count && current_page < pages_count) return setPage(current_page + 1);
				event.preventDefault();
			});

			//on pagination link prev
			$(document.body).on("click", ".uk-datatable-pagination li > a.prev", function(event){
				if (current_page > 1) return setPage(current_page - 1);
				event.preventDefault();
			});

			//on pagination link page
			$(document.body).on("click", ".uk-datatable-pagination li > a.page", function(event){
				let page_number = Number($(this).attr("data-page"));
				if (!isNaN(page_number) && pages_count && page_number >= 1 && page_number <= pages_count) return setPage(page_number);
				event.preventDefault();
			});
		}

		//init pagination page
		showPage(current_page);
	};
	window["uk_datatable_raw"] = (test_callback_column, test_callback_row) => {
		let columns = {}, column_indexes = [], rows = [];
		let test_column = ($column) => {
			if ("function" === typeof test_callback_column && test_callback_column($column) === false) return false;
			return true;
		};
		let test_row = ($row) => {
			if ("function" === typeof test_callback_row && test_callback_row($row) === false) return false;
			return true;
		};
		for (let i = 0; i < $uk_datatable_table_th.length; i ++){
			$column = $($uk_datatable_table_th[i]);
			if (!test_column($column)) return;
			let column_index = $column.index();
			let column_text = $column[0].innerText;
			columns[column_index] = column_text;
			column_indexes.push(column_index);
		}
		let row = [], row_text = "";
		for (let i = 0; i < column_indexes.length; i ++) row.push(columns[column_indexes[i]]);
		rows.push(row);
		let filtered_rows = window.uk_datatable_filter_rows ? uk_datatable_filter_rows() : Array.from(uk_datatable_rows);
		for (let i = 0; i < filtered_rows.length; i ++){
			let $row = $(filtered_rows[i]);
			if (!test_row($row)) continue;
			let cells = $row.children();
			row = [];
			for (let c = 0; c < column_indexes.length; c ++){
				let col = column_indexes[c];
				let cell_value = "";
				if (col >= 0 && col < cells.length) cell_value = cells[col].innerText;
				row.push(cell_value);
			}
			rows.push(row);
		}
		return rows;
	};
	window["uk_datatable_pdf"] = (options, loading_callback, test_callback_column, test_callback_row) => {
		return new Promise((resolve, reject) => {
			let raw = uk_datatable_raw(test_callback_column, test_callback_row);
			if (raw.length <= 1) return reject("No records in table for this action");
			let post_url = relativeRoot() + "/document";
			let post_data = "uk_datatable_pdf=" + encodeURIComponent(stringify(raw));
			if (isObj(options, true, true).length){
				for (let key in options){
					if (options.hasOwnProperty(key) && key != "uk_datatable_pdf"){
						let value = options[key];
						if ("object" === typeof value) value = stringify(value);
						post_data += "&" + encodeURIComponent(key) + "=" + encodeURIComponent(value);
					}
				}
			}
			serverPOST(post_url, post_data, loading_callback).then((res) => {
				let pdf = base64Blob(res.data, "application/pdf", 512);
				return resolve(pdf);
			}, (err) => reject(err));
		});
	};
	window["uk_datatable_print"] = (options, loading_callback, test_callback_column, test_callback_row) => {
		return new Promise((resolve, reject) => {
			uk_datatable_pdf(options, loading_callback, test_callback_column, test_callback_row).then((pdf) => {
				if (window.hasOwnProperty("printJS")){
					printJS(blobURL(pdf));
					return resolve();
				}
				let error = "Exception (uk_datatable_pdf_print): \"printJS\" missing";
				console.error(error);
				return reject(error);
			}, reject);
		});
	};
	window["uk_datatable_download"] = (filename, loading_callback, test_callback_column, test_callback_row) => {
		return new Promise((resolve, reject) => {
			let raw = uk_datatable_raw(test_callback_column, test_callback_row);
			console.log('uk_datatable_raw', raw);
			if (raw.length <= 1) return reject("No records in table for this action");
			let post_url = relativeRoot() + "/document";
			let post_data = "uk_datatable_download=" + encodeURIComponent(stringify(raw)) + "&filename=" + encodeURIComponent(str(filename, "download", true));
			serverPOST(post_url, post_data, loading_callback).then((res) => {
				let chunk_size = res.data.chunk_size;
				let content_type = res.data.type;
				let filename = res.data.filename;
				let content = res.data.content;
				let file = base64Blob(content, content_type, chunk_size);
				if (downloadBlob(file, filename)) resolve();
				else reject("Unable to download exported file");
			}, reject);
		});
	};

	//check datatable elements
	window["uk_datatable_ready"] = () => {
		return $uk_datatable.length &&
		$uk_datatable_wrapper.length &&
		$uk_datatable_pagination.length &&
		$uk_datatable_table_th.length &&
		$uk_datatable_table_tbody.length &&
		$uk_datatable_no_results.length;
	};

	//init datatable
	if (uk_datatable_ready()) init_uk_datatable();
});
