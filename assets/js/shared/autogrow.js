;(function($){
	$.fn.autogrow = function(opts){
		var that = $(this).css({overflow: 'hidden', resize: 'none'}) //no scrollies
		, selector = that.selector
		, defaults = {
			context: $(document),
			animate: true,
			speed: 200,
			fixMinHeight: true,
			cloneClass: 'autogrow_clone',
			onInitialize: false,
		};
		opts = $.isPlainObject(opts) ? opts : {context: opts ? opts : $(document)};
		opts = $.extend({}, defaults, opts);
		that.each(function(i, elem){
			var min, clone;
			elem = $(elem);
			if (elem.is(':visible') || parseInt(elem.css('height'), 10) > 0){
				min = parseInt(elem.css('height'), 10) || elem.innerHeight();
			}
			else {
				clone = elem.clone()
				.addClass(opts.cloneClass)
				.val(elem.val())
				.css({
					position: 'absolute',
					visibility: 'hidden',
					display: 'block'
				});
				$('body').append(clone);
				min = clone.innerHeight();
				clone.remove();
			}
			if (opts.fixMinHeight){
				elem.data('autogrow-start-height', min); //set min height
			}
			elem.css('height', min);
			if (opts.onInitialize && elem.length){
				resize.call(elem[0]);
				setTimeout(() => resize.call(elem[0]), 0); //resize hack
			}
		});
		opts.context.on('keyup paste', selector, resize);
		function resize(e){
			let box = $(this)
			, oldHeight = box.innerHeight()
			, newHeight = this.scrollHeight
			, minHeight = box.data('autogrow-start-height') || 0
			, clone;
			if (oldHeight < newHeight){ //typing
				this.scrollTop = 0;
				opts.animate ? box.stop().animate({height: newHeight}, opts.speed) : box.innerHeight(newHeight);
			}
			else if (!e || e.which == 8 || e.which == 46 || (e.ctrlKey && e.which == 88)){ //deleting/backspace/cutting
				if (oldHeight > minHeight){ //shrink
					clone = box.clone()
					.addClass(opts.cloneClass)
					.css({position: 'absolute', zIndex: -10, height: ''})
					.val(box.val());
					box.after(clone);
					do {
						newHeight = clone[0].scrollHeight - 1;
						clone.innerHeight(newHeight);
					}
					while (newHeight === clone[0].scrollHeight);
					newHeight ++;
					clone.remove();
					box.focus();
					newHeight < minHeight && (newHeight = minHeight);
					oldHeight > newHeight && opts.animate ? box.stop().animate({height: newHeight}, opts.speed) : box.innerHeight(newHeight);
				}
				else {
					box.innerHeight(minHeight); //set to min height
				}
			}
		}
		return that;
	}
})(jQuery);
