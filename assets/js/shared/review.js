$(function(){
	window.reviewPrompt = (title, review_data, submit_callback) => {
		title = str(title, 'Review', true);
		let rating = 0;
		let comment = '';
		if (hasKeys(review_data, 'rating', 'comment')){
			rating = review_data.rating;
			comment = review_data.comment;
		}
		let html = '';
		html += '<a class="uk-modal-close uk-close"></a>';
		html += '<div class="uk-modal-header">' + title + '</div>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<div class="x-pad-20">'; //2
		if (SESSION_USER.name !== ''){
			html += '<div class="uk-grid uk-grid-small" data-uk-grid-margin>'; //g1
			html += '<div class="uk-width-medium-1-4 uk-text-center uk-hidden-small">'; //g11
			html += '<div class="x-avatar" style="width:80px"><img draggable="false" src="' + SESSION_USER.avatar + '" data-onerror="' + DEFAULT_AVATAR + '" onerror="onImageError(this);" /></div>';
			html += '<p class="x-fw-400">' + SESSION_USER.name + '</p>';
			html += '</div>'; //g11
			html += '<div class="uk-width-medium-3-4">'; //g12
		}
		html += '<form id="review-form" action="javascript:" method="post" class="uk-form">';
		if (SESSION_USER.name !== '') html += '<h2 class="uk-visible-small x-fw-400">' + SESSION_USER.name + '</h2>';
		html += '<div class="x-review-rating-stars">'; //c
		html += '<p class="x-nomargin x-fw-400">My Rating <span class="uk-text-danger">*</span></p>';
		html += '<h1 id="review-rating" class="x-iblock x-nomargin uk-rating-stars editable" data-rate="' + rating + '" data-stars="5"></h1>';
		html += '</div>'; //c
		html += '<div class="uk-form-row uk-margin-top">'; //c
		html += '<label class="uk-form-label x-fw-400" for="review-comment">My Review <span class="uk-text-danger">*</span></label>';
		html += '<div class="uk-form-controls">';
		html += '<textarea id="review-comment" class="uk-width-1-1" placeholder="Review Comment" onkeyup="this.value = sentenseCase(this.value)">' + comment + '</textarea>';
		html += '</div>';
		html += '</div>'; //c
		html += '</form>';
		if (SESSION_USER.name !== ''){
			html += '</div>'; //g12
			html += '</div>'; //g1
		}
		html += '</div>'; //2
		html += '</div>'; //1
		html += '<div class="uk-modal-footer uk-text-right">'; //f
		html += '<button type="button" class="uk-button uk-modal-close">Cancel</button> ';
		html += '<button type="button" id="rating-submit" class="uk-button uk-button-success">Submit Review</button> ';
		html += '</div>'; //f
		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			initRatingStars();
			let $form = $modal.find("#review-form");
			let $review_rating = $form.find("#review-rating");
			let $review_comment = $form.find("#review-comment");
			let $form_submit = $modal.find("#rating-submit");
			$form.submit(event => event.preventDefault());
			$form_submit.click(function(event){
				let review_rating = parseInt($review_rating.attr('data-rate'));
				let review_comment = str($review_comment.val(), '', true);
				let pass = true;
				if (review_rating <= 0){
					let $err = $('<p class="x-nomargin uk-text-danger">Kindly select your rating</p>');
					$review_rating.parent().append($err);
					$review_rating.one('input', () => $err.remove());
					pass = false;
				}
				if (!review_comment.length){
					$review_comment.inputError('Kindly enter your review');
					pass = false;
				}
				if (!pass) return event.preventDefault();
				if ('function' === typeof submit_callback) submit_callback({rating: review_rating, comment: review_comment}, uk_modal);
			});
		}, null, true, false, false, false);
	};
});
