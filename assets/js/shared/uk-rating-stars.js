$(function(){
	window.initRatingStars = () => $(".uk-rating-stars").each(function(){
		let $parent = $(this);
		let rate = num($parent.attr("data-rate"));
		let stars = num($parent.attr("data-stars"), 5);
		let is_editable = $parent[0].className.indexOf("editable") > -1;
		if (stars < 1) stars = 5;
		$parent.html("");
		for (let s = 1; s <= stars; s ++){
			let icon = rate >= s ? "uk-icon-star fill" : (rate.round(0) >= s ? "uk-icon-star-half-o fill" : "uk-icon-star-o");
			let $item = $('<a draggable="false"><i class="' + icon + '"></i></a>').appendTo($parent);
			if (is_editable){
				$item.mouseenter(function(){
					let children = $(this).parent().children(), c = $(this).index();
					for (let i = 0; i < children.length; i ++) $(children[i]).find("i").attr("class", i <= c ? "uk-icon-star fill" : "uk-icon-star-o");
				}).mouseleave(function(){
					let $parent = $(this).parent(), children = $parent.children(), rate = num($parent.attr("data-rate"));
					for (let s = 1; s <= children.length; s ++){
						let icon = rate >= s ? "uk-icon-star fill" : (rate.round(0) >= s ? "uk-icon-star-half-o fill" : "uk-icon-star-o");
						$(children[s - 1]).find("i").attr("class", icon);
					}
				}).click(function(event){
					event.preventDefault();
					let $parent = $(this).parent(), children = $parent.children(), c = $(this).index(), rate = num($parent.attr("data-rate")), r = c + 1;
					if (r == 1){
						if (r == rate) r = 0;
						$(this).find("i").attr("class", r ? "uk-icon-star fill" : "uk-icon-star-o");
					}
					if (emitEvent($parent[0], 'input', true)) $parent.attr("data-rate", String(r));
					else console.log('input prevented');
				});
			}
		}
	});
	initRatingStars();
});
