//main shared script
Number.prototype.round = function(places){
	let num = this.valueOf();
	places = !isNaN(places = Number(places)) ? places : 2;
	num = +num.toFixed(places);
	return num;
};
Number.prototype.commas = function(places){
	let num = this.valueOf();
	places = !isNaN(places = Number(places)) ? places : 2;
	return num.toFixed(places).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
function onLoad(callback, args){
	let ready_fired = false;
	let ready = function(){
		if (ready_fired) return;
		ready_fired = true;
		if ("function" === typeof callback){
			if ("undefined" === typeof args) callback();
			if (Array.isArray(args)) callback.apply(args);
			else callback.apply([args]);
		}
	};
	if (document.readyState === "complete") setTimeout(ready, 1);
	else {
		if (document.addEventListener){
			document.addEventListener("DOMContentLoaded", ready, false);
			window.addEventListener("load", ready, false);
		}
		else {
			let readyStateChange = function(){
				if (document.readyState === "complete") ready();
			};
			document.attachEvent("onreadystatechange", readyStateChange);
			window.attachEvent("onload", ready);
		}
	}
}
function onImageError(img){
	if (img && img.hasAttribute("data-onerror")){
		img.onerror = "";
		img.src = img.getAttribute("data-onerror");
		return true;
	}
	else console.debug("onImageError Invalid img!", img);
}
function errorMessage(message){
	let html = "<div class=\"uk-alert uk-alert-danger\" data-uk-alert>";
	html += "<a href=\"\" class=\"uk-alert-close uk-close\"></a>";
	html += "<p>" + message + "</p>";
	html += "</div>";
	return html;
}
function scrollTop(){
	document.body.scrollTop = 0;
	document.documentElement.scrollTop = 0;
}
function stringify(obj){
	if ("undefined" === typeof obj || obj === null) return "";
	if ("object" === typeof obj && obj){
		let seen = [];
		return JSON.stringify(obj, function(key, val){
			if (val != null && typeof val == "object"){
				if (seen.indexOf(val) >= 0) return;
				seen.push(val);
			}
			return val;
		});
	}
	return obj + "";
}
function jsonParse(val, _default, test_callback){
	if ("undefined" === typeof _default) _default = null;
	try {
		if (!(val = str(val, "", true)).length) return _default;
		let temp = JSON.parse(val);
		if ("function" === typeof test_callback && !test_callback(temp)) return _default;
		return temp;
	}
	catch (e){
		return _default;
	}
}
function join(items, glue){
	if (!(Array.isArray(items) && items.length)) return "";
	if ("boolean" === typeof glue) glue = !!glue ? 1 : 0;
	if ("object" === typeof glue && !(Array.isArray(glue) && glue.length)) glue = ",";
	if (!Array.isArray(glue)) glue = [glue];
	let buffer = "", s = 0;
	for (let i = 0; i < items.length; i ++){
		let item = items[i];
		if ("string" !== typeof item) item = stringify(item);
		if (i == 0) buffer += item;
		else {
			let separator = glue[s];
			if ((s + 1) < glue.length) s ++;
			else s = 0;
			buffer += separator + item;
		}
	}
	return buffer;
}
function camelCase(val, regex){
	if ("string" !== typeof val) val = "" + val;
	let _regex = regex instanceof RegExp ? regex : " ";
	regex = new RegExp(_regex, "g");
	try {
		let separators = val.match(regex);
		let parts = val.split(regex);
		let items = [];
		for (let i = 0; i < parts.length; i ++){
			let part = parts[i];
			let part_space = part.match(/[^\s]/);
			if (part_space){
				let x = Number(part_space["index"]);
				if (!isNaN(x)){
					x += 1; part = part.substr(0, x).toUpperCase() + part.substr(x);
				}
			}
			else part = part = part.substr(0, 1).toUpperCase() + part.substr(1);
			items.push(part);
		}
		val = join(items, separators);
	}
	catch (e){
		console.error(e);
	}
	return val;
}
function sentenseCase(val){
	return camelCase(val, /[\.!?]/);
}
function onKeyPressNumber(event, prevent_negatives, prevent_decimals){
	let curval = event.srcElement.value;
	let newchar = String.fromCharCode(event.charCode || event.keyCode);
	let curval_arr = curval.split("");
	curval_arr.splice(event.target.selectionStart, (event.target.selectionEnd - event.target.selectionStart), newchar);
	let newval = curval_arr.join("");
	if (!prevent_negatives && newval == "-") return true;
	if (newval.match(/\.$/)) newval += "0";
	let num = Number(newval);
	if (newchar == ' ' || isNaN(num) || (prevent_negatives && num < 0) || (prevent_decimals && newchar == '.')){
		event.preventDefault();
		return false;
	}
	return true;
}
function num(val, _default){
	if ("undefined" === typeof _default) _default = 0;
	let num = !val && val !== 0 || isNaN(val = Number(val)) ? _default : val;
	return num;
}
function str(val, _default, trim){
	if ("string" === typeof val){
		if (trim) val = val.trim();
		if (val.length) return val;
	}
	if ("undefined" === typeof _default){
		if ("object" === typeof val && val) _default = stringify(val);
		if ("number" === typeof val) _default = val + "";
		if ("boolean" === typeof val) _default = val ? "1" : "";
		else _default = "";
	}
	return _default;
}
function tstr(val){
	return str(val, '', true);
}
function toStr(val, trim){
	if (!val) val = "";
	else if ("number" === typeof val) val = String(val);
	else if ("boolean" === typeof val) val = val ? "true" : "";
	else if ("object" === typeof val) val = stringify(val);
	if (trim) val = val.trim();
	return val;
}
function uid(){
	return Math.random().toString(36).substring(2) + (new Date()).getTime().toString(36);
}
function isObj(obj, not_null, return_keys){
	if ("object" === typeof obj && (not_null ? !!obj : true)) return (return_keys ? (obj ? Object.keys(obj) : []) : true);
	return return_keys ? [] : false;
}
function hasKeys(obj, ...keys){
	let obj_keys = isObj(obj, true, true);
	if (obj_keys.length && Array.isArray(keys) && keys.length){
		for (let i = 0; i < keys.length; i ++) if (obj_keys.indexOf(keys[i]) < 0) return false;
		return true;
	}
	return false;
}
function propval(obj, ...props){
	let result = null, temp = obj;
	if (Array.isArray(props) && props.length){
		let last_index = props.length - 1;
		for (let i = 0; i < props.length; i ++){
			let prop = props[i];
			if (!("object" === typeof temp && temp && temp.hasOwnProperty(prop))) break;
			temp = temp[prop];
			if (i == last_index) result = temp;
		}
	}
	return result;
}
function isElement(obj){
	try {
		return obj instanceof HTMLElement;
	}
	catch (e){
		return hasKeys(obj, "nodeType", "style", "ownerDocument") && obj.nodeType === 1 && isObj(obj.style) && isObj(obj.ownerDocument);
	}
}
function isNode(obj){
	return isElement(obj) && obj.nodeType === Node.ELEMENT_NODE;
}
function emitEvent(element, event_name, cancelable){
	if (!str(event_name, "", true).length || !isElement(element)) return false;
	let event = document.createEvent("Event");
	event.initEvent(event_name, false, !!cancelable);
	return element.dispatchEvent(event);
}
function validateEmail(email){
	let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}
function emitResize(){
	window.dispatchEvent(new Event('resize'));
}
window["SERVER_RESPONSE"] = "";
window["SERVER_RESPONSE_DATA"] = "";
window["SERVER_RESPONSE_ERROR"] = "";
function isFunc(obj){
	return "function" === typeof obj;
}
function serverResponse(response, callback){
	let res;
	SERVER_RESPONSE = null;
	SERVER_RESPONSE_DATA = null;
	SERVER_RESPONSE_ERROR = "";
	if ("object" !== typeof response) res = jsonParse(response, false);
	let doCallback = (data, is_success) => {
		if ("function" === typeof callback) callback(data, is_success);
		return is_success ? data : false;
	};
	if (!hasKeys(res, "response")){
		SERVER_RESPONSE = response;
		SERVER_RESPONSE_ERROR = "Unexpected server response";
		console.debug(SERVER_RESPONSE_ERROR, response, res);
		return doCallback(response);
	}
	response = res.response;
	SERVER_RESPONSE = response;
	if (!hasKeys(response, "type")){
		SERVER_RESPONSE_ERROR = "Unexpected server object response";
		console.debug(SERVER_RESPONSE_ERROR, response);
		return doCallback(response);
	}
	if (response.type == "error"){
		let message = response.hasOwnProperty("message") ? str(response.message, "", true) : "";
		let data = response.hasOwnProperty("data") ? response.data : null;
		if (!message.length){
			if ("string" === typeof data && (data = str(data, "", true)).length) message = data;
			else message = "Unspecified error in response";
		}
		SERVER_RESPONSE_DATA = data;
		SERVER_RESPONSE_ERROR = message;
		return doCallback(data);
	}
	if (response.type == "success"){
		let message = response.hasOwnProperty("message") ? str(response.message, "", true) : "";
		let data = response.hasOwnProperty("data") ? response.data : null;
		SERVER_RESPONSE_DATA = data;
		return doCallback({message: message, data: data}, true);
	}
	SERVER_RESPONSE_ERROR = "Unsuppported server object response";
	return doCallback(response);
}
function escapeHTML(text, trim){
	return toStr(text, trim).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
}
function printContent(content, landscape, isReceipt){
	try {
		let html = "";
		if (content){
			if ("string" === typeof content) html = content.trim();
			else if (isElement(content)) html = content.outerHTML;
		}
		let print_size = "A4";
		if (landscape) print_size = "A4 landscape";
		html = "<style type='text/css'>@media print{@page{ size: " + print_size + "; margin: 0 auto; }body{ -webkit-print-color-adjust: exact; }}</style>" + html;
		let olf_frame = document.querySelector("#xx-printing-frame");
		if (olf_frame) olf_frame.remove();
		let frame = document.createElement("iframe");
		frame.id = "xx-printing-frame";
		frame.style.width = 0;
		frame.style.height = 0;
		document.body.appendChild(frame);
		frame.contentWindow.document.open();
		frame.contentWindow.document.write(html);
		frame.contentWindow.document.close();
		if (isReceipt){
			let div = frame.contentWindow.document.querySelector("body > div");
			let styles = frame.contentWindow.document.getElementsByTagName("style");
			let style = styles.length ? styles[0] : null;
			if (style) style.innerHTML = "@media print{@page{ size: 350px " + div.offsetHeight + "px; margin: 0 auto; }body{ -webkit-print-color-adjust: exact; margin: 0; }}";
		}
		frame.contentWindow.focus();
		frame.contentWindow.print();
		return true;
	}
	catch(e){
		console.error("printContent Exception:", e);
		return false;
	}
}
function strBlob(val, content_type, slice_size){
	slice_size = num(slice_size, 512);
	content_type = str(content_type, "", true);
	let byte_characters = val;
	let byte_arrays = [];
	for (let offset = 0; offset < byte_characters.length; offset += slice_size){
		let slice = byte_characters.slice(offset, offset + slice_size);
		let byte_numbers = new Array(slice.length);
		for (let i = 0; i < slice.length; i ++) byte_numbers[i] = slice.charCodeAt(i);
		let byte_array = new Uint8Array(byte_numbers);
		byte_arrays.push(byte_array);
	}
	let blob = new Blob(byte_arrays, {type: content_type});
	return blob;
}
function base64Blob(base64_data, content_type, slice_size){
	return strBlob(atob(base64_data), content_type, slice_size);
}
function blobURL(blob){
	return URL.createObjectURL(blob);
}
function downloadBlob(blob, filename){
	if (!navigator.msSaveBlob) return downloadURL(blobURL(blob), filename);
	navigator.msSaveBlob(blob, filename);
	return true;
}
function downloadURL(url, filename){
	let link = document.createElement("a");
	if (link.download !== undefined){
		link.setAttribute("href", url);
		link.setAttribute("download", filename);
		link.style.visibility = "hidden";
		document.body.appendChild(link);
		link.click();
		setTimeout(() => link.remove(), 1000);
		return true;
	}
	return false;
}
function relativeRoot(){
	let temp = window.location.href.toLowerCase().replace(ROOT_URL.toLowerCase(), "").trim().replace(/^\/|\/$/, "");
	let paths = temp.split("/").length - 1, root = "";
	if (paths) for (let i = 0; i < paths; i ++) root += (i != (paths - 1) ? "../" : "..");
	else root = ".";
	return root;
}
function objectMerge(old_object, new_object, replace_old, ){
	replace_old = !!replace_old;
	if ("object" === typeof old_object && old_object){
		if ("object" === typeof new_object && new_object){
			let new_object_keys = Object.keys(new_object);
			for (let i = 0; i < new_object_keys.length; i ++){
				let key = new_object_keys[i];
				if (old_object.hasOwnProperty(key)) old_object[key] = replace_old ? objectMerge(old_object[key], new_object[key], replace_old) : old_object[key];
				else old_object[key] = new_object[key];
			}
		}
		else if (replace_old) old_object = new_object;
	}
	else if (replace_old) old_object = new_object;
	return old_object;
}
function clone(val){
	if ("object" === typeof val && val) val = Array.isArray(val) ? Array.from(val) : jsonParse(stringify(val));
	return val;
}
function displayWidth(){
	return (window.innerWidth > 0) ? window.innerWidth : screen.width;
}
function displayHeight(){
	return (window.innerHeight > 0) ? window.innerHeight : screen.height;
}
function isUKSmall(){
	return displayWidth() < 768;
}
function isUKMedium(){
	let width = displayWidth();
	return width >= 768 && width < 960;
}
function isUKLarge(){
	return displayWidth() >= 960;
}
function changeStateURL(url){
	let current_url = window.location.href;
	if ((url = str(url, '', true)) != '' && current_url != url){
		window.history.pushState({}, null, url);
		console.debug('changeStateURL', current_url, url);
	}
}
function fileBase64(file){
	return new Promise((resolve, reject) => {
		if (FileReader){
			let reader = new FileReader();
			reader.onload = () => resolve(reader.result);
			reader.onerror = error => reject(error);
			reader.readAsDataURL(file);
		}
		else reject('FileReader not defined!');
	});
}
function createImage(src){
	return new Promise((resolve, reject) => {
		let img = new Image();
		img.onload = () => resolve(img);
		img.onerror = error => reject(error);
		img.src = src;
	});
}
function base64ImageResize(src, max_width, max_height){
	return new Promise((resolve, reject) => {
		try {
			createImage(src).then((img) => {
				let canvas = document.createElement('canvas');
				let context = canvas.getContext('2d');
				let copy_canvas = document.createElement("canvas");
				let copy_context = copy_canvas.getContext("2d");
				copy_canvas.width = img.width;
				copy_canvas.height = img.height;
				copy_context.drawImage(img, 0, 0);
				let aspect_ratio_fit = (src_width, src_height) => {
					let ratio = Math.min(max_width / src_width, max_height / src_height);
					return {
						width: src_width * ratio,
						height: src_height * ratio,
					};
				};
				let ratio_size = aspect_ratio_fit(img.width, img.height);
				canvas.width = ratio_size.width;
				canvas.height = ratio_size.height;
				context.drawImage(copy_canvas, 0, 0, copy_canvas.width, copy_canvas.height, 0, 0, canvas.width, canvas.height);
				resolve(canvas.toDataURL("image/png"));
			}, (err) => reject(err));
		}
		catch (e){
			console.error('base64_image_resize error', e);
			reject(e.message);
		}
	});
}
function objectPromise(obj, onObjectItem, ignoreCatch){
	//onObjectItem(obj[key], key, index, keys, obj)
	return new Promise((resolve, reject) => {
		if ("object" === typeof obj && obj){
			let keys = Object.keys(obj);
			let resolvedItems = undefined;
			let addResolvedItem = (key, item) => {
				if (item == undefined) return;
				if (!resolvedItems) resolvedItems = Array.isArray(obj) ? [] : {};
				if (Array.isArray(obj)) resolvedItems.push(item);
				else resolvedItems[key] = item;
			};
			let process = (index) => {
				return Promise.resolve((() => {
					if (!(index < keys.length && "function" === typeof onObjectItem)) return;
					let key = keys[index];
					return (new Promise((res, rej) => {
						Promise.resolve(onObjectItem(obj[key], key, index, keys, obj)).then(res, (err) => {
							if (ignoreCatch) res();
							else rej(err);
						});
					})).then((item) => {
						addResolvedItem(key, item);
						return process(index + 1);
					});
				})());
			};
			process(0).then(() => resolve(resolvedItems), reject);
		}
		else reject("Invalid object!");
	});
}
function openTab(url){
	const link = document.createElement('a');
	link.href = url;
	link.target = '_blank';
	document.body.appendChild(link);
	link.click();
	link.remove();
}
function openDownload(url, filename){
	filename = str(filename, '', true);
	if (!filename) filename = url.substr(url.lastIndexOf('/') + 1);
	let a = document.createElement('A');
	a.href = url;
	a.download = filename;
	document.body.appendChild(a);
	a.click();
	document.body.removeChild(a);
}
function isWinVisible(element, fully){
	if (!isElement(element)) return false;
	var position = element.getBoundingClientRect();
	if (position.top >= 0 && position.bottom <= window.innerHeight) return true;
	if (position.top < window.innerHeight && position.bottom >= 0) return fully ? 0 : 1;
	return false;
}
function getScrollParent(node){
	if (!isElement(node)) return null;
	if (node.scrollHeight > node.clientHeight) return node;
	return getScrollParent(node.parentNode);
}
function pad(num){
	return (num < 10 ? "0" : "") + num;
}
function isDate(val){
	return val instanceof Date && !isNaN(val.getTime());
}
function getQueryParams(obj, callback){
	let pk = isObj(obj, true, true), temp = null;
	if (pk.length){
		let replace_url = false;
		let urlParams = new URLSearchParams(window.location.search);
		let new_params = [];
		for (const [key, val] of urlParams){
			let remove = false;
			if (pk.indexOf(key) !== -1){
				remove = !!obj[key];
				if (!('object' === typeof temp && temp)) temp = {};
				temp[key] = val;
			}
			if (!remove) new_params.push(key + '=' + encodeURIComponent(val));
			else replace_url = true;
		}
		let new_url = window.location.protocol + '//' + window.location.host + window.location.pathname;
		if (new_params.length) new_url += '?' + new_params.join('&');
		if ('function' === typeof callback) callback(replace_url, new_url, temp);
		else if (replace_url) history.replaceState({}, document.title, new_url);
	}
	else if ('function' === typeof callback) callback();
	return temp;
}
