$(function(){
	//on event delete
	$(document.body).on("click", ".x-table td .event-delete", function(event){
		let event_id = $(this).attr("data-id");
		let event_name= $(this).attr("data-name");
		event_id = str(event_id, "", true);
		event_name = str(event_name, "", true);
		if (!event_id.length || !event_name.length) return event.preventDefault();
		if (!window.hasOwnProperty("UKPromptConfirmTest")) return event.preventDefault();
		let prompt = '<h2 class="uk-text-danger uk-margin-small-bottom">' + event_name + '</h2><p class="uk-margin-small-top uk-margin-small-bottom uk-text-danger">Are you sure you want to PERMANENTLY DELETE this event and related records? <strong>This action cannot be undone.</strong></p>';
		UKPromptConfirmTest(prompt, "DELETE", function(value){
			let redirect = ROOT_URL + "/dashboard?delete-event=" + event_id;
			window.location.href = redirect;
		}, null, "Delete Event", "uk-button-danger");
	});
});
