$(function(){
	let IMAGE_SIZE_LIMIT = 5;

	//service pricing
	let pricing_prompt = (item) => {
		let html = '';
		html += '<a class="uk-modal-close uk-close"></a>';
		html += '<div class="uk-modal-header">ADD PRICING ITEM</div>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<div class="x-pad-20">'; //2
		html += '<form id="pricing-form" action="javascript:" method="post" class="uk-form">';
		html += '<input type="hidden" name="pricing-form">';
		html += '<p class="x-nomargin x-pad-10c x-border-bottom"><strong>NOTE:</strong> Fields marked <span class="uk-text-danger">*</span> are required.</p>';
		html += '<div class="uk-margin-top uk-grid uk-grid-small" data-uk-grid-margin>'; //g1
		html += '<div class="uk-width-medium-1-2">'; //g11
		html += '<div class="uk-form-row">'; //f1
		html += '<label class="uk-form-label x-fw-400" for="pricing-title">Pricing Title <span class="uk-text-danger">*</span></label>';
		html += '<div class="uk-form-controls">'; //f12
		html += '<input type="text" id="pricing-title" name="pricing-title" class="uk-width-1-1" placeholder="Pricing Title" onkeyup="this.value = sentenseCase(this.value)">';
		html += '</div>'; //f12
		html += '</div>'; //f1
		html += '</div>'; //g11
		html += '<div class="uk-width-medium-1-2">'; //g12
		html += '<div class="uk-form-row">'; //f1
		html += '<label class="uk-form-label x-fw-400" for="pricing-price">Price <span class="uk-text-danger">*</span></label>';
		html += '<div class="uk-form-controls">'; //f12
		html += '<input id="pricing-price" name="pricing-price" class="uk-width-1-1" type="number" placeholder="KES 0" onkeypress="onKeyPressNumber(event, true)" onblur="this.value = num(this.value, \'\')">';
		html += '</div>'; //f12
		html += '</div>'; //f1
		html += '</div>'; //g12
		html += '</div>'; //g1

		html += '<div class="uk-form-row uk-margin-top">'; //f1
		html += '<label class="uk-form-label x-fw-400" for="pricing-description">Pricing Description <span class="uk-text-danger">*</span></label>';
		html += '<div class="uk-form-controls">'; //f12
		html += '<textarea id="pricing-description" name="pricing-description" class="uk-width-1-1" placeholder="Pricing Description" onkeyup="this.value = sentenseCase(this.value)"></textarea>';
		html += '</div>'; //f12
		html += '</div>'; //f1

		html += '<div class="pricing-packages uk-margin-top">'; //p1
		html += '<h3 class="x-nomargin x-fw-400">PRICING PACKAGE</h3>';
		html += '<p class="x-nomargin x-pad-10c x-border-bottom">Enter package deals for this item.</p>';

		html += '<div class="uk-margin-top pricing-package-items">'; //p2

		html += '<div class="uk-grid uk-grid-small">'; //g1
		html += '<div class="uk-width-4-5">'; //g11
		html += '<div class="uk-form-row">'; //f1
		html += '<label class="uk-form-label">Package Item</label>';
		html += '<div class="uk-form-controls">'; //f12
		html += '<textarea name="pricing-package[]" class="uk-width-1-1" placeholder="Package Item" onkeyup="this.value = sentenseCase(this.value)"></textarea>';
		html += '</div>'; //f12
		html += '</div>'; //f1
		html += '</div>'; //g11
		html += '<div class="uk-width-1-5">'; //g12
		html += '<div class="uk-height-1-1 uk-vertical-align">'; //b1
		html += '<button type="button" class="pricing-package-item-delete uk-vertical-align-bottom uk-button uk-button-white"><i class="uk-icon-trash"></i></button>';
		html += '</div>'; //b1
		html += '</div>'; //g12
		html += '</div>'; //g1

		html += '</div>'; //p2

		html += '<div class="uk-margin-top">'; //b1
		html += '<button type="button" id="pricing-package-item-add" class="uk-button uk-button-white">Add Package Item</button>';
		html += '</div>'; //b1
		html += '</div>'; //p1

		html += '</form>';
		html += '</div>'; //2
		html += '</div>'; //1

		html += '<div class="uk-modal-footer uk-text-right">'; //f
		html += '<button type="button" class="uk-button uk-modal-close">Cancel</button> ';
		html += '<button type="button" id="pricing-form-submit" class="uk-button uk-button-success">Save Details</button> ';
		html += '</div>'; //f

		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			let $form = $modal.find("#pricing-form");
			let $pricing_package_items = $modal.find(".pricing-package-items");
			let $pricing_package_item_delete = $modal.find(".pricing-package-item-delete");
			let $pricing_package_item_add = $modal.find("#pricing-package-item-add");
			let $form_submit = $modal.find("#pricing-form-submit");

			if (!($form.length && $pricing_package_items.length && $pricing_package_item_delete.length && $pricing_package_item_add.length && $form_submit.length)){
				console.error("Unable to find pricing form controls", $form, $pricing_package_items, $pricing_package_item_delete, $pricing_package_item_add, $form_submit);
				event.preventDefault();
				return false;
			}
			$form.submit(function(event){
				event.preventDefault();
				let post_url = "";
				let post_data = $(this).serialize();
				serverPOST(post_url, post_data, loading_block).then((res) => {
					uk_modal.hide();
					window.location.reload();
				}, function(err){
					UKPrompt('<p class="uk-text-warning">' + err + '</p>');
				});
			});
			$form_submit.click(event => $form.submit());
			let package_item_add = function(){
				let html = '<div class="uk-grid uk-grid-small">'; //g1
				html += '<div class="uk-width-4-5">'; //g11
				html += '<div class="uk-form-row">'; //f1
				html += '<label class="uk-form-label">Package Item</label>';
				html += '<div class="uk-form-controls">'; //f12
				html += '<textarea name="pricing-package[]" class="uk-width-1-1" placeholder="Package Item" onkeyup="this.value = sentenseCase(this.value)"></textarea>';
				html += '</div>'; //f12
				html += '</div>'; //f1
				html += '</div>'; //g11
				html += '<div class="uk-width-1-5">'; //g12
				html += '<div class="uk-height-1-1 uk-vertical-align">'; //b1
				html += '<button type="button" class="pricing-package-item-delete uk-vertical-align-bottom uk-button uk-button-white"><i class="uk-icon-trash"></i></button>';
				html += '</div>'; //b1
				html += '</div>'; //g12
				html += '</div>'; //g1
				$(html).appendTo($pricing_package_items);
			};
			$pricing_package_item_add.click(() => package_item_add());
			$(document.body).on("click", ".pricing-package-item-delete", function(event){
				let $btn = $(this);
				let $item = $btn.parent().parent().parent();
				$item.remove();
				if (!$pricing_package_items.children().length) package_item_add();
			});
		});
	};

	let pricing_edit_prompt = (item) => {
		let html = '';
		html += '<a class="uk-modal-close uk-close"></a>';
		html += '<div class="uk-modal-header">EDIT PRICING ITEM</div>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<div class="x-pad-20">'; //2
		html += '<form id="pricing-edit-form" action="javascript:" method="post" class="uk-form">';
		html += '<input type="hidden" name="pricing-edit-form">';
		html += '<input type="hidden" name="id" value="'+item.id+'">';
		html += '<p class="x-nomargin x-pad-10c x-border-bottom"><strong>NOTE:</strong> Fields marked <span class="uk-text-danger">*</span> are required.</p>';
		html += '<div class="uk-margin-top uk-grid uk-grid-small" data-uk-grid-margin>'; //g1
		html += '<div class="uk-width-medium-1-2">'; //g11
		html += '<div class="uk-form-row">'; //f1
		html += '<label class="uk-form-label x-fw-400" for="pricing-title">Pricing Title <span class="uk-text-danger">*</span></label>';
		html += '<div class="uk-form-controls">'; //f12
		html += '<input type="text" id="pricing-title" name="pricing-edit-title" value="'+item.title+'" class="uk-width-1-1" placeholder="Pricing Title" onkeyup="this.value = sentenseCase(this.value)">';
		html += '</div>'; //f12
		html += '</div>'; //f1
		html += '</div>'; //g11
		html += '<div class="uk-width-medium-1-2">'; //g12
		html += '<div class="uk-form-row">'; //f1
		html += '<label class="uk-form-label x-fw-400" for="pricing-edit-price">Price <span class="uk-text-danger">*</span></label>';
		html += '<div class="uk-form-controls">'; //f12
		html += '<input id="pricing-price" name="pricing-edit-price" value='+item.price+' class="uk-width-1-1" type="number" placeholder="KES 0" onkeypress="onKeyPressNumber(event, true)" onblur="this.value = num(this.value, \'\')">';
		html += '</div>'; //f12
		html += '</div>'; //f1
		html += '</div>'; //g12
		html += '</div>'; //g1

		html += '<div class="uk-form-row uk-margin-top">'; //f1
		html += '<label class="uk-form-label x-fw-400" for="pricing-description">Pricing Description <span class="uk-text-danger">*</span></label>';
		html += '<div class="uk-form-controls">'; //f12
		html += '<textarea id="pricing-description" name="pricing-edit-description" class="uk-width-1-1" placeholder="Pricing Description" onkeyup="this.value = sentenseCase(this.value)">'+item.description+'</textarea>';
		html += '</div>'; //f12
		html += '</div>'; //f1

		html += '<div class="pricing-packages uk-margin-top">'; //p1
		html += '<h3 class="x-nomargin x-fw-400">PRICING PACKAGE</h3>';
		html += '<p class="x-nomargin x-pad-10c x-border-bottom">Enter package deals for this item.</p>';

		html += '<div class="uk-margin-top pricing-package-items">'; //p2

		item.package.forEach(package => {
			html += '<div class="uk-grid uk-grid-small">'; //g1
			html += '<div class="uk-width-4-5">'; //g11
			html += '<div class="uk-form-row">'; //f1
			html += '<label class="uk-form-label">Package Item</label>';
			html += '<div class="uk-form-controls">'; //f12
			html += '<textarea name="pricing-edit-package['+package+']" class="uk-width-1-1" placeholder="Package Item" onkeyup="this.value = sentenseCase(this.value)">'+package+'</textarea>';
			html += '</div>'; //f12
			html += '</div>'; //f1
			html += '</div>'; //g11
			html += '<div class="uk-width-1-5">'; //g12
			html += '<div class="uk-height-1-1 uk-vertical-align">'; //b1
			html += '<button type="button" class="pricing-package-item-delete uk-vertical-align-bottom uk-button uk-button-white"><i class="uk-icon-trash"></i></button>';
			html += '</div>'; //b1
			html += '</div>'; //g12
			html += '</div>'; //g1
		});

		if(item.package.length <= 0) {
			html += '<div class="uk-grid uk-grid-small">'; //g1
			html += '<div class="uk-width-4-5">'; //g11
			html += '<div class="uk-form-row">'; //f1
			html += '<label class="uk-form-label">Package Item</label>';
			html += '<div class="uk-form-controls">'; //f12
			html += '<textarea name="pricing-edit-package[]" class="uk-width-1-1" placeholder="Package Item" onkeyup="this.value = sentenseCase(this.value)"></textarea>';
			html += '</div>'; //f12
			html += '</div>'; //f1
			html += '</div>'; //g11
			html += '<div class="uk-width-1-5">'; //g12
			html += '<div class="uk-height-1-1 uk-vertical-align">'; //b1
			html += '<button type="button" class="pricing-package-item-delete uk-vertical-align-bottom uk-button uk-button-white"><i class="uk-icon-trash"></i></button>';
			html += '</div>'; //b1
			html += '</div>'; //g12
			html += '</div>'; //g1
		}

		html += '</div>'; //p2

		html += '<div class="uk-margin-top">'; //b1
		html += '<button type="button" id="pricing-package-item-add" class="uk-button uk-button-white">Add Package Item</button>';
		html += '</div>'; //b1
		html += '</div>'; //p1

		html += '</form>';
		html += '</div>'; //2
		html += '</div>'; //1

		html += '<div class="uk-modal-footer uk-text-right">'; //f
		html += '<button type="button" class="uk-button uk-modal-close">Cancel</button> ';
		html += '<button type="button" id="pricing-edit-form-submit" class="uk-button uk-button-success">Save Details</button> ';
		html += '</div>'; //f

		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			let $form = $modal.find("#pricing-edit-form");
			let $pricing_package_items = $modal.find(".pricing-package-items");
			let $pricing_package_item_delete = $modal.find(".pricing-package-item-delete");
			let $pricing_package_item_add = $modal.find("#pricing-package-item-add");
			let $form_submit = $modal.find("#pricing-edit-form-submit");

			if (!($form.length && $pricing_package_items.length && $pricing_package_item_delete.length && $pricing_package_item_add.length && $form_submit.length)){
				console.error("Unable to find pricing form controls", $form, $pricing_package_items, $pricing_package_item_delete, $pricing_package_item_add, $form_submit);
				event.preventDefault();
				return false;
			}
			$form.submit(function(event){
				event.preventDefault();
				let post_url = "";
				let post_data = $(this).serialize();
				serverPOST(post_url, post_data, loading_block).then((res) => {
					uk_modal.hide();
					window.location.reload();
				}, function(err){
					UKPrompt('<p class="uk-text-warning">' + err + '</p>');
				});
			});
			$form_submit.click(event => $form.submit());
			let package_item_add = function(){
				let html = '<div class="uk-grid uk-grid-small">'; //g1
				html += '<div class="uk-width-4-5">'; //g11
				html += '<div class="uk-form-row">'; //f1
				html += '<label class="uk-form-label">Package Item</label>';
				html += '<div class="uk-form-controls">'; //f12
				html += '<textarea name="pricing-edit-package[]" class="uk-width-1-1" placeholder="Package Item" onkeyup="this.value = sentenseCase(this.value)"></textarea>';
				html += '</div>'; //f12
				html += '</div>'; //f1
				html += '</div>'; //g11
				html += '<div class="uk-width-1-5">'; //g12
				html += '<div class="uk-height-1-1 uk-vertical-align">'; //b1
				html += '<button type="button" class="pricing-package-item-delete uk-vertical-align-bottom uk-button uk-button-white"><i class="uk-icon-trash"></i></button>';
				html += '</div>'; //b1
				html += '</div>'; //g12
				html += '</div>'; //g1
				$(html).appendTo($pricing_package_items);
			};
			$pricing_package_item_add.click(() => package_item_add());
			$(document.body).on("click", ".pricing-package-item-delete", function(event){
				let $btn = $(this);
				let $item = $btn.parent().parent().parent();
				$item.remove();
				if (!$pricing_package_items.children().length) package_item_add();
			});
		});
	};

	//service pricing add
	$pricing_add = $(".x-service-pricing-add");
	$pricing_add.click(function(){
		pricing_prompt();
	});

	// service pricing edit
	$(document.body).on("click", ".x-pricing-item-edit[data-id]", function(event) {
		let service = JSON.parse($(this).attr('data-service'))
		pricing_edit_prompt(service)
	});

	//service pricing delete
	$(document.body).on("click", ".x-pricing-item-delete[data-id]", function(event){
		let item_id = str($(this).attr("data-id"), "", true);
		let item_title = str($(this).attr("data-title"), "", true);
		let $item_wrapper = $(this).parent().parent();
		let $items_wrapper = $item_wrapper.parent();
		//confirm item delete
		UKPromptConfirm('<p class="uk-text-danger">Do you want to delete pricing <strong>\"' + item_title + '\"</strong> permanently?</p>', function(){
			console.log("delete pricing item", item_id, $item_wrapper);
			let post_url = "";
			let post_data = "pricing-delete=" + encodeURIComponent(item_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				$item_wrapper.remove();
				window.location.reload();
			}, function(err){
				UKPrompt('<p class="uk-text-warning">' + err + '</p>');
			});
		}, null, "Delete", "uk-text-danger");
	});

	//service cart
	$service_cart_add = $(".x-service-cart-add");
	$service_cart_loading = $(".x-service-cart-loading");
	$service_cart_remove = $(".x-service-cart-remove");

	//service cart add listener
	$service_cart_add.click(function(event){
		let service_id = str($(this).attr("data-id"), "", true);
		if (!$service_cart_add.length || !$service_cart_remove.length || !$service_cart_loading.length){
			console.error("Unable to find service cart controls: " + service_id, $service_cart_add, $service_cart_remove, $service_cart_loading);
			return event.preventDefault();
		}
		if ($service_cart_add[0].hasAttribute("disabled")) return event.preventDefault();

		//loading callback
		let loading_html = $service_cart_loading.html();
		let loading_callback = (toggle) => {
			if (toggle){
				$service_cart_add.addClass("x-hidden");
				$service_cart_remove.addClass("x-hidden");
				$service_cart_loading.html('<i class="uk-icon-spinner uk-icon-spin"></i> Adding');
				$service_cart_loading.removeClass("x-hidden");
			}
			else {
				$service_cart_loading.addClass("x-hidden");
				$service_cart_loading.html(loading_html);
			}
		};

		//add to cart request
		service_cart_toggle_item(true, service_id, loading_callback).then((res) => {
			$service_cart_remove.removeClass("x-hidden");
			$service_cart_remove.attr("data-id", res.data.id);
		}, (err) => {
			console.error(err);
			$service_cart_add.removeClass("x-hidden");
		});
	});

	//service cart remove listener
	$service_cart_remove.click(function(event){
		let cart_id = str($(this).attr("data-id"), "", true);
		if (!$service_cart_add.length || !$service_cart_remove.length || !$service_cart_loading.length){
			console.error("Unable to find service cart controls: " + service_id, $service_cart_add, $service_cart_remove, $service_cart_loading);
			return event.preventDefault();
		}
		if ($service_cart_remove[0].hasAttribute("disabled")) return event.preventDefault();

		//loading callback
		let loading_html = $service_cart_loading.html();
		let loading_callback = (toggle) => {
			if (toggle){
				$service_cart_add.addClass("x-hidden");
				$service_cart_remove.addClass("x-hidden");
				$service_cart_loading.html('<i class="uk-icon-spinner uk-icon-spin"></i> Removing');
				$service_cart_loading.removeClass("x-hidden");
			}
			else {
				$service_cart_loading.addClass("x-hidden");
				$service_cart_loading.html(loading_html);
			}
		};

		//confirm remove from cart
		UKPromptConfirm('<p class="uk-text-danger">Do you want to remove this item from your cart?</p>', function(){
			//delete cart item request
			service_cart_toggle_item(false, cart_id, loading_callback).then((res) => {
				$service_cart_add.removeClass("x-hidden");
				$service_cart_remove.attr("data-id", "");
			}, (err) => {
				console.error(err);
				$service_cart_remove.removeClass("x-hidden");
			});
		}, null, "Remove", "uk-text-danger");
	});

	//review comments toggle
	let $review_comments = $(".x-review-comments");
	let $review_comments_toggle = $(".x-review-comments-toggle");
	$review_comments_toggle.click(function(){
		if ($review_comments.length){
			if ($review_comments[0].className.indexOf("expand") > -1){
				$review_comments.removeClass("expand");
				$review_comments_toggle.html("Show All");
			}
			else {
				$review_comments.addClass("expand");
				$review_comments_toggle.html("Show Less");
			}
		}
	});

	//gallery upload
	let upload_image_prompt = () => {
		let placeholder_image = ROOT_URL + "/assets/img/placeholder.jpg";
		let html = '';
		html += '<a href="" class="uk-modal-close uk-close uk-close-alt"></a>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<div class="x-pad-20 uk-text-center x-image-upload-dialog">'; //2
		html += '<img draggable="false" id="upload-image-preview" src="' + placeholder_image + '" alt="Preview" title="Preview" />';
		html += '</div>'; //2
		html += '</div>'; //1
		html += '<div class="uk-modal-footer">'; //1
		html += '<form id="upload-image-form" action="javascript:" method="post" enctype="multipart/form-data" class="uk-form">';
		html += '<div class="uk-form-row">'; //2
		html += '<label class="uk-form-label" for="upload-image-caption">Image Caption <small><i>(Remaining <span class="caption-length">100</span>)</i></small></label>';
		html += '<div class="uk-form-controls">'; //3
		html += '<textarea id="upload-image-caption" name="caption" class="uk-width-1-1" placeholder="Image Caption"></textarea>';
		html += '</div>'; //3
		html += '</div>'; //2';
		html += '<div class="uk-text-right uk-margin-top">'; //4
		html += '<button type="button" class="uk-button uk-modal-close">Cancel</button> ';
		html += '<button type="button" id="upload-image-upload" class="uk-button uk-button-default uk-disabled" disabled><i class="uk-icon-upload"></i> Upload</button> ';
		html += '<button type="button" id="upload-image-select" class="uk-button uk-button-primary">Select</button>';
		html += '<input type="file" class="x-hidden" accept=".jpg,.jpeg,.png,.gif" name="gallery_upload" id="upload-image-file">';
		html += '</div>'; //4
		html += '</form>';
		html += '</div>'; //1
		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			let $image = $modal.find("#upload-image-preview");
			let $form = $modal.find("#upload-image-form");
			let $caption = $modal.find("#upload-image-caption");
			let $caption_len = $modal.find(".caption-length");
			let $select = $modal.find("#upload-image-select");
			let $upload = $modal.find("#upload-image-upload");
			let $file = $modal.find("#upload-image-file");
			if (!($image.length && $form.length && $caption.length && $caption_len.length && $select.length && $upload.length && $file.length)){
				console.error("Unable to find image upload controls", $image, $form, $select, $upload);
				event.preventDefault();
				return false;
			}
			$form.submit(function(event){
				event.preventDefault();
				let size = $file[0].files[0].size, size_mb = size / (1024 ** 2); width = $image[0].naturalWidth, height = $image[0].naturalHeight;
				if (size_mb > IMAGE_SIZE_LIMIT){
					UKPrompt('<p class="uk-text-warning">Image file size is above limit <strong>(' + size_mb.round(2) + 'mb size limit is ' + IMAGE_SIZE_LIMIT + 'mb)</strong>. Select another image.</p>');
					return event.preventDefault();
				}
				if (width < 500){
					UKPrompt('<p class="uk-text-warning">Image width is too small (' + width + 'x' + height + '). Select another image <strong>(Preferably 500x300 and above for better quality)</strong>.</p>');
					return event.preventDefault();
				}
				if (height < ((width / 2) - (width > 700 ? 100 : 50))){
					UKPrompt('<p class="uk-text-warning">Image is too wide compared with height (' + width + 'x' + height + '). Select another image <strong>(Preferably 500x300 and above for better quality)</strong>.</p>');
					return event.preventDefault();
				}
				let image_src = $image.attr('src');
				let caption = $caption.val().trim();
				loading_block(true);
				base64ImageResize(image_src, 600, 600).then((base64) => {
					loading_block();
					let post_url = '';
					let post_data = objectParams({
						gallery_upload: base64,
						caption: caption
					});
					serverPOST(post_url, post_data, loading_block).then((res) => {
						let item = res.data;
						let html = '';
						html += '<li data-id="' + item.id + '">';
						html += '<figure class="uk-overlay x-gallery-item">';
						html += '<a href="javascript:" class="x-gallery-delete" data-id="' + item.id + '"><i class="uk-icon-trash"></i></a>';
						html += '<a draggable="false" href="' + item.image + '" data-uk-lightbox="{group:\'gallery\'}" data-lightbox-type="image" title="' + item.caption + '">';
						html += '<img draggable="false" src="' + item.image + '" alt="' + item.caption + '" data-onerror="' + placeholder_image + '" onerror="onImageError(this);" />';
						html += '</a>';
						if (item.caption.length) html += '<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom">' + item.caption + '</figcaption>';
						html += '</figure>';
						html += '</li>';
						$('.x-gallery-empty').addClass('x-hidden');
						$('.x-gallery-items').removeClass('x-hidden').find('ul').append($(html));
						uk_modal.hide();
					}, (err) => {
						UKPrompt('<p class="uk-text-warning">' + err + '</p>');
					});
				}, (err) => {
					loading_block();
					console.error(err);
				});
			});
			$caption.keydown(function(event){
				if ($(this).val().length >= 100) return event.preventDefault();
				return true;
			});
			$caption.keyup(function(event){
				let remaining = 100 - $(this).val().length;
				if (remaining < 20) $caption_len.addClass("uk-text-danger");
				else $caption_len.removeClass("uk-text-danger");
				$caption_len.html(String(remaining));
			});
			$upload.click(event => $form.submit());
			$select.click(event => $file.click());
			$image.click(event => $file.click());
			$file.change(function(event){
				let files = event.target.files;
				if (FileReader && files && files.length){
					let fr = new FileReader();
					fr.onload  = () => {
						$image.attr("src", fr.result);
						$upload.removeClass("uk-button-default uk-disabled");
						$upload[0].disabled = false;
						$upload.removeAttr("disabled");
						$select.html("Change");
						$upload.addClass("uk-button-success");
					};
					fr.readAsDataURL(files[0]);
				}
				else console.error("Preview not supported!");
			});
		});
	};
	$gallery_upload = $(".x-gallery-upload");
	$gallery_upload.click(function(){
		upload_image_prompt();
	});

	//gallery delete
	$(document.body).on("click", ".x-gallery-delete[data-id]", function(event){
		let item_id = str($(this).attr("data-id"), "", true);
		let $item_wrapper = $("li[data-id='" + item_id + "']");
		if (!item_id.length || !$item_wrapper.length){
			console.error("Unable to find gallery item", item_id, $item_wrapper);
			return event.preventDefault();
		}
		//confirm item delete
		UKPromptConfirm('<p class="uk-text-danger">Do you want to delete this image permanently?</p>', function(){
			let post_url = '';
			let post_data = 'gallery_delete=' + encodeURIComponent(item_id);
			serverPOST(post_url, post_data, loading_block).then(() => {
				let $parent = $item_wrapper.parent();
				$item_wrapper.remove();
				if (!$parent.children().length){
					$('.x-gallery-empty').removeClass('x-hidden');
					$('.x-gallery-items').addClass('x-hidden');
				}
			}, (err) => {
				UKPrompt('<p class="uk-text-warning">' + err + '</p>');
			});
		}, null, "Delete", "uk-text-danger");
	});

	//init map
	let $map_container = $(".x-service-map");
	if ($map_container.length && window.SERVICE_DATA){
		let GOOGLE_MAPS_LOADED = window.hasOwnProperty("google") && "object" === typeof google && google.hasOwnProperty("maps");
		if (!GOOGLE_MAPS_LOADED) return console.error("GOOGLE_MAPS: Google maps library is not available!");
		let $map = null;
		let $map_markers = [];
		let removeMarkers = () => {
			$map_markers.forEach(marker => marker.setMap(null));
			$map_markers = [];
		};
		let addMarker = (latlng, title) => {
			if (!GOOGLE_MAPS_LOADED || !$map){
				console.error("summaryMapAddMarker Error: Google Maps Exception!");
				return false;
			}
			let marker = new google.maps.Marker({
				position: latlng,
				map: $map,
				title: title,
				draggable: false,
			});
			$map_markers.push(marker);
			return true;
		};
		let setLocation = (title, lat, lng) => {
			if (!GOOGLE_MAPS_LOADED || !$map){
				console.error("summaryMapSetLocation Error: Google Maps Exception!");
				return false;
			}
			lat = num(lat, -1.2835201);
			lng = num(lng, 36.8218568);
			let map_center = new google.maps.LatLng(lat, lng);
			removeMarkers();
			addMarker(map_center, title);
			$map.setCenter(map_center);
			return true;
		};
		let initMap = (title, lat, lng) => {
			if (!GOOGLE_MAPS_LOADED) return false;
			if (!($map_container && $map_container.length)){
				console.error("initSummaryMap Error: Unable to find map container!");
				return false;
			}
			let map_container = $map_container[0];
			let map_center = new google.maps.LatLng(lat, lng);
			let map_options = {
				zoom: 16,
				disableDefaultUI: true,
				scrollwheel: false,
				zoomControl: true,
				navigationControl: true,
				mapTypeControl: false,
				scaleControl: true,
				draggable: true,
				center: map_center,
				mapTypeId: "roadmap",
				draggableCursor: "default",
				styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
			};
			$map = new google.maps.Map(map_container, map_options);
			setLocation(str(title, "Event Location", true), lat, lng);
		};
		onLoad(() => initMap(SERVICE_DATA.title, SERVICE_DATA.location_map_lat, SERVICE_DATA.location_map_lon));
	}
});
