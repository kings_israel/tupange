$(function(){
	let DEFAULT_AVATAR = ROOT_URL + '/assets/img/user.png';
	let PLACEHOLDER_IMAGE = ROOT_URL + '/assets/img/placeholder.jpg';
	let SELECTED_THREAD = null;
	let VENDOR_DATA = null;
	if (hasKeys(USER_DATA, 'vendor') && hasKeys(USER_DATA.vendor, 'services', 'id', 'company_name')) VENDOR_DATA = USER_DATA.vendor;

	window.ATTACHMENTS = {};

	//controls
	let $threads_search = $('.x-threads-search');
	let $threads = $('ul.x-threads');
	let $threads_count = $('.x-threads-count');

	let sel_thread = 'ul.x-threads li';
	let sel_thread_mark_read = 'ul.x-threads li a[data-action="mark-read"]';
	let sel_thread_mark_unread = 'ul.x-threads li a[data-action="mark-unread"]';
	let sel_thread_archive = 'ul.x-threads li a[data-action="archive"]';
	let sel_thread_delete = 'ul.x-threads li a[data-action="delete"]';
	let sel_thread_time = 'ul.x-threads li .x-thread-time';
	let sel_thread_preview = 'ul.x-threads li .x-thread-preview';

	let $messages_user_avatar = $('.x-messages-header .x-avatar img');
	let $messages_user_names = $('.x-messages-header .x-info h2');
	let $messages_search = $('.x-messages-header .x-messages-search');
	let $messages = $('.x-messages');
	let $messages_start = $('.x-messages-start');
	let $messages_chat = $('.x-messages-chat');
	let $no_messages = $('.x-no-messages');
	let $messages_wrapper = $('.x-messages-wrapper');

	let sel_attachment_image = '.x-message-attachments a.x-attachment.image';
	let sel_attachment_file = '.x-message-attachments a.x-attachment.file';
	let sel_attachment_order = '.x-message-attachments a.x-attachment.order';
	let sel_attachment_quote = '.x-message-attachments a.x-attachment.quote';

	let $attach_input = $('#attach_input');
	let $chat_new = $('#chat_new');
	let $chat_message = $('#chat_new #chat_message');
	let $chat_file_attachments = $('#chat_new .x-file-attachments');
	let $chat_attach_file = $('#chat_new .x-attach-file');
	let $chat_attach_order = $('#chat_new .x-attach-order');
	let $chat_attach_quote = $('#chat_new .x-attach-quote');
	let $chat_message_send = $('#chat_new .x-message-send');
	let sel_file_attachment_close = '#chat_new .x-file-attachments .x-file-attachment a';

	//helpers
	let fist_display = true;
	let searching_messages = false;
	let template_line_text = (text) => {
		return '<div class="x-line-text uk-text-center"><span>' + text + '</span></div>';
	};
	let template_message_bubble = (data) => {
		if (!hasKeys(data, 'id', 'class', 'message', 'sent', 'status', 'attachments')){
			console.error('Invalid chat message object!', data);
			return false;
		}
		let html = '';
		html += '';
		html += '<div class="x-bubble ' + data.class + '" data-id="' + data.id + '">';
		if (data.message !== '') html += '<div class="x-message">' + data.message + '</div>';
		if (Array.isArray(data.attachments) && data.attachments.length){
			html += '<div class="x-message-attachments">';
			let prev_type = null;
			for (let i = 0; i < data.attachments.length; i ++){
				let attachment = data.attachments[i];
				if (!hasKeys(attachment, 'type', 'name', 'data')){
					console.error('Invalid chat attachment object!', order);
					continue;
				}
				if (!(attachment.type === 'image' && (prev_type === 'image' || prev_type === null)) && i) html += ' <br> ';
				prev_type = attachment.type;
				if (attachment.type === 'image'){
					html += '<a href="javascript:" class="x-attachment image" data-src="' + attachment.data + '" title="' + attachment.name + '">';
					html += '<img uk-width-small="small" draggable="false" src="' + attachment.data + '" data-onerror="' + PLACEHOLDER_IMAGE + '" onerror="onImageError(this);" />';
					html += '</a> ';
				}
				else if (attachment.type === 'file'){
					html += '<a href="javascript:" class="x-attachment file" data-src="' + attachment.data + '" title="Click to download"><i class="uk-icon-paperclip"></i> ' + attachment.name + '</a> ';
				}
				else if (attachment.type === 'order'){
					let order = attachment.data;
					if (!hasKeys(order, 'ref', 'service', 'title', 'category', 'agreement', 'image', 'price')){
						console.error('Invalid chat order attachment object!', order);
						continue;
					}
					let order_base64 = btoa(stringify(order));
					html += '<a href="javascript:" class="x-attachment order" data-base64="' + order_base64 + '" title="Preview Order">';
					html += '<div class="uk-grid uk-grid-small" data-uk-grid-margin>';
					html += '<div class="uk-width-medium-1-4">';
					html += '<img uk-width-small="small" draggable="false" src="' + order.image + '" data-onerror="' + PLACEHOLDER_IMAGE + '" onerror="onImageError(this);" />';
					html += '</div>';
					html += '<div class="uk-width-medium-3-4">';
					html += '<h3><i class="uk-icon-shopping-cart"></i> Custom Order</h3>';
					html += '<h4>' + order.title + '<br><small>' + order.category + '</small></h4>';
					html += '<p><strong>Agreement:</strong>' + order.agreement + '</p>';
					html += '<h3>' + order.price.commas(2) + '/=<br><small>' + order.ref + '</small></h3>';
					html += '</div>';
					html += '</div>';
					html += '</a> ';
				}
				else if (attachment.type === 'quote'){
					let quote = attachment.data;
					if (!hasKeys(quote, 'ref', 'order', 'title', 'agreement', 'price')){
						console.error('Invalid chat quotation attachment object!', quote);
						continue;
					}
					let quote_base64 = btoa(stringify(quote));
					html += '<a href="javascript:" class="x-attachment quote" data-base64="' + quote_base64 + '" title="Preview Quotation">';
					html += '<h3><i class="uk-icon-shopping-bag"></i> Order Quotation</h3>';
					html += '<h4>' + quote.title + '</h4>';
					html += '<p><strong>Agreement:</strong> ' + quote.agreement + '</p>';
					html += '<h3>' + quote.price.commas(2) + '/= <br><small>' + quote.ref + '</small></h3>';
					html += '</a> ';
				}
				else {
					console.error('Invalid attachment type: ' + attachment.type);
					continue;
				}
			}
			html += '</div>';
		}
		html += '<div class="x-timestamp">' + data.sent;
		if (data.class === 'me' && data.status === 1) html += ' <i class="uk-icon-check"></i>';
		html += '</div>';
		html += '</div>';
		return html;
	};
	let template_thread = (data) => {
		if (!hasKeys(data, 'user', 'names', 'avatar', 'preview', 'unread', 'messages')){
			console.error('Invalid thread object!', data);
			return false;
		}
		if (!hasKeys(data.preview, 'text', 'status', 'time')){
			console.error('Invalid thread preview object!', data.preview);
			return false;
		}
		let html = '';
		html += '<li data-user="' + data.user + '">';
		html += '<div tabindex="0" class="x-thread">';
		html += '<div class="x-thread-right">';
		html += '<div class="x-thread-time">' + data.preview.time + '</div> ';
		html += '<div class="x-thread-unread ' + (!data.unread ? 'x-display-none' : '') + '">' + data.unread + '</div> ';
		html += '<div class="x-thread-options" data-uk-dropdown="{mode:\'click\'}">';
		html += '<a href="javascript:"><i class="uk-icon-angle-down"></i></a> ';
		html += '<div class="uk-dropdown">';
		html += '<ul class="uk-nav uk-nav-navbar">';
		html += '<li><a class="uk-dropdown-close" href="javascript:" data-user="' + data.user + '" data-action="archive">Archive</a></li>';
		html += '<li><a class="uk-dropdown-close" href="javascript:" data-user="' + data.user + '" data-action="delete" class="uk-text-danger">Delete</a></li>';
		html += '</ul>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '<div class="x-thread-user">';
		html += '<div class="x-avatar">';
		html += '<img src="' + data.avatar + '" data-onerror="' + DEFAULT_AVATAR + '" onerror="onImageError(this);" />';
		html += '</div>';
		html += '<div class="x-info">';
		html += '<h3 class="x-thread-names x-ellipsis-text">' + data.names + '</h3>';
		html += '<span class="x-thread-preview x-ellipsis-text">';
		html += (data.preview.status === 1 ? '<i class="uk-icon-check"></i> ' : '') + data.preview.text
		html += '</span>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		html += '</li>';
		return html;
	};
	let template_file_attachment = (id, type, name) => {
		if (type === 'image') return '<div class="x-file-attachment"><span class="x-fname"><i class="uk-icon-file-image-o"></i> ' + name + '</span> <a class="uk-text-danger" data-id="' + id + '" href="javascript:"><i class="uk-icon-close"></i></a></div>';
		if (type === 'file') return '<div class="x-file-attachment"><span class="x-fname"><i class="uk-icon-file-o"></i> ' + name + '</span> <a class="uk-text-danger" data-id="' + id + '" href="javascript:"><i class="uk-icon-close"></i></a></div>';
		if (type === 'order') return '<div class="x-file-attachment"><span class="x-fname"><i class="uk-icon-shopping-bag"></i> ' + name + '</span> <a class="uk-text-danger" data-id="' + id + '" href="javascript:"><i class="uk-icon-close"></i></a></div>';
		if (type === 'quote') return '<div class="x-file-attachment"><span class="x-fname"><i class="uk-icon-shopping-cart"></i> ' + name + '</span> <a class="uk-text-danger" data-id="' + id + '" href="javascript:"><i class="uk-icon-close"></i></a></div>';
		console.error('Unknown file attachment type!', id, type, name);
		return false;
	};
	let update_thread_unread = (user, unread) => {
		user = str(user, '', true);
		unread = num(unread);
		if (user !== ''){
			let $item = $(sel_thread + '[data-user="' + user + '"]');
			if ($item.length){
				let $unread = $item.find('.x-thread-unread');
				if ($unread.length){
					$unread.html(String(unread));
					if (unread > 0) $unread.removeClass('x-hidden');
					else $unread.addClass('x-hidden');
					return true;
				}
			}
		}
		return false;
	};
	let update_thread_time = (user, time) => {
		user = str(user, '', true);
		time = str(time, '', true);
		if (user !== ''){
			let $item = $(sel_thread + '[data-user="' + user + '"]');
			if ($item.length){
				let $time = $item.find('.x-thread-time');
				if ($time.length){
					$time.html(time);
					return true;
				}
			}
		}
		return false;
	};
	let update_thread_preview = (user, text, status) => {
		user = str(user, '', true);
		text = str(text, '', true);
		status = num(status);
		if (user !== ''){
			let $item = $(sel_thread + '[data-user="' + user + '"]');
			if ($item.length){
				let $preview = $item.find('.x-thread-preview');
				if ($preview.length){
					let html = (status === 1 ? '<i class="uk-icon-check"></i> ' : '') + text;
					$preview.html(html);
					return true;
				}
			}
		}
		return false;
	};
	let edit_thread = (user, unread, time, preview_text, preview_status) => {
		user = str(user, '', true);
		unread = num(unread);
		time = str(time, '', true);
		preview_text = str(preview_text, '', true);
		preview_status = num(preview_status);
		for (let i = 0; i < THREADS_DATA; i ++){
			if (THREADS_DATA[i].user === user){
				THREADS_DATA[i].unread = unread;
				THREADS_DATA[i].time = time;
				THREADS_DATA[i].preview.text = preview_text;
				THREADS_DATA[i].preview.status = preview_status;
				update_thread_unread(user, unread);
				update_thread_time(user, time);
				update_thread_preview(user, preview_text, preview_status);
			}
		}
	};
	let get_thread = (user, return_index) => {
		user = str(user, '', true);
		if (Array.isArray(THREADS_DATA) && THREADS_DATA.length){
			for (let i = 0; i < THREADS_DATA.length; i ++) if (hasKeys(THREADS_DATA[i], 'user', 'messages') && THREADS_DATA[i].user === user) return return_index ? i : THREADS_DATA[i];
		}
		return false;
	};
	let update_read = (user) => {
		user = str(user, '', true);
		if (!user) return;
		let post_url = '';
		let post_data = 'update_read=' + encodeURIComponent(user);
		serverPOST(post_url, post_data, null).then((res) => {
			let $item = $(sel_thread + '[data-user="' + user + '"]');
			if ($item.length) $item.find('.x-thread-unread').html('0').addClass('x-display-none');
		}, (err) => {
			console.error('update_read', err);
		});
	};
	let display_thread = (user) => {
		let is_current = false;
		let thread = get_thread(user);
		if (!thread){
			thread = get_thread(SELECTED_THREAD);
			if (thread && !fist_display) is_current = true;
		}
		if (!thread && Array.isArray(THREADS_DATA) && THREADS_DATA.length) thread = THREADS_DATA[0];
		if (!thread){
			console.error('Thread not found!', user, THREADS_DATA);
			return false;
		}
		SELECTED_THREAD = thread.user;
		window.localStorage.setItem('selected', SELECTED_THREAD);
		let $item = $(sel_thread + '[data-user="' + thread.user + '"]');
		$(sel_thread).removeClass('active');
		if ($item.length) $item.addClass('active');
		else console.error('Unable to find thread item element');
		$no_messages.addClass('x-display-none');
		$messages_wrapper.removeClass('x-display-none');
		$messages_wrapper.attr('data-user', thread.user);
		$messages_user_avatar.attr('src', thread.avatar);
		$messages_user_names.html(thread.names);
		$messages_start.addClass('x-display-none');
		let thread_messages = thread.messages;
		let has_new = false;
		let search_query = str($messages_search.val(), '', true);
		if (thread_messages.length){
			for (let i = 0; i < thread_messages.length; i ++){
				let message = thread_messages[i];
				let $message_bubble = $messages_chat.find('[data-id="' + message.id + '"]');
				if (!$message_bubble.length){
					let bubble_html = template_message_bubble(message);
					if (bubble_html === false){
						console.error('Unable to generate message bubble html!', i, bubble_html);
						continue;
					}
					$message_bubble = $(bubble_html);
					$messages_chat.append($message_bubble);
					has_new = true;
				}
				if (search_query.length){
					let bubble_text = $message_bubble.text();
					if (bubble_text.toLowerCase().indexOf(search_query.toLowerCase()) > -1) $message_bubble.removeClass('x-display-none');
					else $message_bubble.addClass('x-display-none');
				}
				else $message_bubble.removeClass('x-display-none');
			}
		}
		else $messages_start.removeClass('x-display-none');
		if (!is_current){
			$messages.scrollBottom();
			changeStateURL(ROOT_URL + '/messages/' + thread.user);
		}
		if (has_new) $messages.scrollBottom();
		update_read(thread.user);
		fist_display = false;
		return true;
	};
	let refresh_threads = (threads_data) => {
		let threads_count = 0;
		$no_messages.addClass('x-display-none');
		$messages_wrapper.addClass('x-display-none');
		let search_query = str($threads_search.val(), '', true);
		if (Array.isArray(threads_data) && (threads_count = threads_data.length)){
			THREADS_DATA = threads_data;
			for (let i = 0; i < threads_count; i ++){
				if (threads_data[i].hasOwnProperty('avatar') && threads_data[i].avatar == '') threads_data[i].avatar = DEFAULT_AVATAR;
				let thread_html = template_thread(threads_data[i]);
				if (thread_html === false) throw 'Error generating thread html';
				let thread_user = threads_data[i].user;
				let $new_item = $(thread_html);
				let $item = $(sel_thread + '[data-user="' + thread_user + '"]');
				if ($item.length){
					$item.find('.x-thread-time').replaceWith($new_item.find('.x-thread-time'));
					$item.find('.x-thread-unread').replaceWith($new_item.find('.x-thread-unread'));
					$item.find('.x-thread-preview').replaceWith($new_item.find('.x-thread-preview'));
				}
				else {
					$item = $new_item;
					$threads.append($item);
				}
				if (search_query.length){
					let thread_text = $item.text();
					if (thread_text.toLowerCase().indexOf(search_query.toLowerCase()) > -1) $item.removeClass('x-display-none');
					else $item.addClass('x-display-none');
				}
				else $item.removeClass('x-display-none');
			}
			$threads_count.html(threads_count + ' Conversation' + ((threads_count > 1 || threads_count === 0) ? 's' : ''));
			display_thread();
			return true;
		}
		$no_messages.removeClass('x-display-none');
		return false;
	};
	let attach_order = () => {
		let html = '<a class="uk-modal-close uk-close"></a>';
		html += '<div class="uk-modal-header">Custom Order</div>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<form action="javascript:" method="post" class="uk-form x-pad-20">'; //f
		html += '<div class="uk-form-row">'; //c
		html += '<label class="uk-form-label" for="order_service">Select Service<span class="uk-text-danger">*</span></label>';
		html += '<div class="uk-form-controls">';
		html += '<select id="order_service" name="order_service" class="uk-width-1-1">';
		html += '<option value="" selected="selected">Select Order Service</option>';
		if (hasKeys(VENDOR_DATA, 'services') && Array.isArray(VENDOR_DATA.services) && VENDOR_DATA.services.length){
			for (let i = 0; i < VENDOR_DATA.services.length; i ++){
				let service = VENDOR_DATA.services[i];
				html += '<option value="' + service.id + '">' + service.title + '</option>';
			}
		}
		html += '</select>';
		html += '</div>';
		html += '</div>'; //c
		html += '<div class="uk-form-row">'; //c
		html += '<label class="uk-form-label" for="order_price">Price</label>';
		html += '<div class="uk-form-controls">';
		html += '<input id="order_price" name="order_price" class="uk-width-1-1" type="number" step="0.01" placeholder="KES 0" onkeypress="onKeyPressNumber(event, true)" onblur="this.value = num(this.value, \'\')">';
		html += '</div>';
		html += '</div>'; //c
		html += '<div class="uk-form-row uk-margin-top">'; //c
		html += '<label class="uk-form-label" for="order_ref">Reference (optional)</label>';
		html += '<div class="uk-form-controls">';
		html += '<input type="text" id="order_ref" name="order_ref" class="uk-width-1-1" placeholder="Enter your order reference">';
		html += '</div>';
		html += '</div>'; //c
		html += '<div class="uk-form-row uk-margin-top">'; //c
		html += '<label class="uk-form-label" for="order_agreement">Agreement (optional)</label>';
		html += '<div class="uk-form-controls">';
		html += '<textarea id="order_agreement" name="order_agreement" class="uk-width-1-1" placeholder="Enter your terms and conditions agreement for this custom order. i.e. I will deliver services at 12PM for 5 hours..."></textarea>';
		html += '</div>';
		html += '</div>'; //c
		html += '</form>'; //f
		html += '</div>'; //1
		html += '<div class="uk-modal-footer uk-text-right">';
		html += '<button type="button" class="uk-button uk-modal-close">Close</button> ';
		html += '<button type="button" class="x-attachment-add uk-button uk-button-success x-min-100">Add Attachment</button> ';
		html += '</div>';
		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			let $attachment_add = $modal.find('.x-attachment-add');
			let $order_service = $modal.find('form #order_service');
			let $order_ref = $modal.find('form #order_ref');
			let $order_agreement = $modal.find('form #order_agreement');
			let $order_price = $modal.find('form #order_price');
			$attachment_add.click(function(event){
				let service_id = str($order_service.val(), '', true);
				if (!service_id.length){
					$order_service.inputError('Kindly select one of your services.', true);
					return event.preventDefault();
				}
				let service = null;
				if (hasKeys(VENDOR_DATA, 'services') && Array.isArray(VENDOR_DATA.services) && VENDOR_DATA.services.length){
					for (let i = 0; i < VENDOR_DATA.services.length; i ++){
						if (VENDOR_DATA.services[i].id == service_id){
							service = VENDOR_DATA.services[i];
							break;
						}
					}
				}
				if (!service){
					console.error('Failed to get order service data!', service_id, VENDOR_DATA);
					$order_service.inputError('Failed to get order service data!', true);
					return event.preventDefault();
				}
				let ref = str($order_ref.val(), '', true);
				let agreement = str($order_agreement.val(), '', true);
				let price = num($order_price.val());
				let id = uid();
				let attachment = {
					type: 'order',
					name: 'Order: ' + service.title + ' ' + price.commas(2) + '/=',
					data: {
						ref: ref,
						service: service.id,
						title: service.title,
						category: service.category_name,
						agreement: agreement,
						image: service.image_src,
						price: price,
					},
				};
				if (uk_modal.isActive()) uk_modal.hide();
				let $file_attachment = $(template_file_attachment(id, 'order', attachment.name));
				$chat_file_attachments.append($file_attachment);
				ATTACHMENTS[id] = attachment;
				$attach_input[0].value = '';
				$chat_file_attachments.flashGreen(1, 200);
			});
		}, null, null, false, false, true);
	};
	let attach_quote_prompt = (user_orders) => {
		let html = '<a class="uk-modal-close uk-close"></a>';
		html += '<div class="uk-modal-header">Order Quotation</div>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<form action="javascript:" method="post" class="uk-form x-pad-20">'; //f
		html += '<div class="uk-form-row">'; //c
		html += '<label class="uk-form-label" for="quote_order">Select Order<span class="uk-text-danger">*</span></label>';
		html += '<div class="uk-form-controls">';
		html += '<select id="quote_order" name="quote_order" class="uk-width-1-1">';
		html += '<option value="" selected="selected">Select Order</option>';
		if (Array.isArray(user_orders) && user_orders.length){
			for (let i = 0; i < user_orders.length; i ++){
				let order = user_orders[i];
				html += '<option value="' + order.id + '">' + order.id + ': ' + order.title + ' (' + order.category + ') ' + order.time_ordered + '</option>';
			}
		}
		html += '</select>';
		html += '</div>';
		html += '</div>'; //c
		html += '<div class="uk-form-row">'; //c
		html += '<label class="uk-form-label" for="quote_price">Price</label>';
		html += '<div class="uk-form-controls">';
		html += '<input id="quote_price" name="quote_price" class="uk-width-1-1" type="number" step="0.01" placeholder="KES 0" onkeypress="onKeyPressNumber(event, true)" onblur="this.value = num(this.value, \'\')">';
		html += '</div>';
		html += '</div>'; //c
		html += '<div class="uk-form-row uk-margin-top">'; //c
		html += '<label class="uk-form-label" for="quote_ref">Reference (optional)</label>';
		html += '<div class="uk-form-controls">';
		html += '<input type="text" id="quote_ref" name="quote_ref" class="uk-width-1-1" placeholder="Enter your quotation reference">';
		html += '</div>';
		html += '</div>'; //c
		html += '<div class="uk-form-row uk-margin-top">'; //c
		html += '<label class="uk-form-label" for="quote_agreement">Agreement (optional)</label>';
		html += '<div class="uk-form-controls">';
		html += '<textarea id="quote_agreement" name="quote_agreement" class="uk-width-1-1" placeholder="Enter your terms and conditions agreement for this quotation. i.e. I will deliver services at 12PM for 5 hours..."></textarea>';
		html += '</div>';
		html += '</div>'; //c
		html += '</form>'; //f
		html += '</div>'; //1
		html += '<div class="uk-modal-footer uk-text-right">';
		html += '<button type="button" class="uk-button uk-modal-close">Close</button> ';
		html += '<button type="button" class="x-attachment-add uk-button uk-button-success x-min-100">Add Attachment</button> ';
		html += '</div>';
		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			let $attachment_add = $modal.find('.x-attachment-add');
			let $quote_order = $modal.find('form #quote_order');
			let $quote_price = $modal.find('form #quote_price');
			let $quote_ref = $modal.find('form #quote_ref');
			let $quote_agreement = $modal.find('form #quote_agreement');
			$attachment_add.click(function(event){
				let order_id = str($quote_order.val(), '', true);
				if (!order_id.length){
					$quote_order.inputError('Kindly select one of the client orders.', true);
					return event.preventDefault();
				}
				let order = null;
				if (Array.isArray(user_orders) && user_orders.length){
					for (let i = 0; i < user_orders.length; i ++){
						if (user_orders[i].id == order_id){
							order = user_orders[i];
							break;
						}
					}
				}
				if (!order){
					console.error('Failed to get order data!', order_id, user_orders);
					$quote_order.inputError('Failed to get order data!', true);
					return event.preventDefault();
				}
				let ref = str($quote_ref.val(), '', true);
				let agreement = str($quote_agreement.val(), '', true);
				let price = num($quote_price.val());
				let id = uid();
				let attachment = {
					type: 'quote',
					name: 'Quote: ' + order.id + ' - ' + order.title + ' ' + price.commas(2) + '/=',
					data: {
						ref: ref,
						order: order.id,
						title: order.title,
						agreement: agreement,
						price: price,
					},
				};
				if (uk_modal.isActive()) uk_modal.hide();
				let $file_attachment = $(template_file_attachment(id, 'quote', attachment.name));
				$chat_file_attachments.append($file_attachment);
				ATTACHMENTS[id] = attachment;
				$attach_input[0].value = '';
				$chat_file_attachments.flashGreen(1, 200);
			});
		}, null, null, false, false, true);
	};
	let attach_quote = () => {
		if (!SELECTED_THREAD){
			console.error('Failed to get selected thread user!', SELECTED_THREAD);
			return false;
		}
		let quote_user = SELECTED_THREAD;
		let post_url = '';
		let post_data = 'get_user_orders=' + encodeURIComponent(quote_user);
		serverPOST(post_url, post_data, loading_block).then((res) => {
			let user_orders = res.data;
			if (!(Array.isArray(user_orders) && user_orders.length)) UKPrompt('<p class="uk-text-warning">Client has not ordered your services. Send a custom order instead</p>');
			else attach_quote_prompt(user_orders);
		}, (err) => {
			UKPrompt('<p class="uk-text-danger">' + err + '</p>');
			console.error(SERVER_RESPONSE);
		});
	}

	//init
	let selected_user;
	if (hasKeys(SELECTED_DATA, 'thread', 'missing') && (selected_user = str(SELECTED_DATA.thread, '', true)).length){
		SELECTED_THREAD = selected_user;
		if (SELECTED_DATA.missing) UKPrompt('<h2>Thread Not Found!</h2><p>REF: <strong>' + selected_user + '</strong><br>Error loading conversation thread. Reload your browser and try messaging the vendor/client again.</p>');
	}
	if (!SELECTED_THREAD){
		let stored_selected = str(window.localStorage.getItem('selected'), '', true);
		if (stored_selected != '') SELECTED_THREAD = stored_selected;
	}
	refresh_threads(THREADS_DATA);
	let update_threads_callback = () => {
		let post_url = '';
		let post_data = 'refresh_threads=1';
		serverPOST(post_url, post_data, null).then((res) => refresh_threads(res.data), (err) => {
			console.error('refresh_threads', err);
		});
	};
	let update_threads_interval = setInterval(update_threads_callback, 5000);
	let update_threads_now = () => {
		clearInterval(update_threads_interval);
		update_threads_callback();
		update_threads_interval = setInterval(update_threads_callback, 5000);
	};

	//on thread
	$(document.body).on('click', sel_thread, function(event){
		let user = str($(this).attr('data-user'), '', true);
		if (user !== '') display_thread(user);
		return false;
	});

	//on threads search
	$threads_search.on('input', function(event){
		let search_query = str($(this).val(), '', true);
		let threads_children = $threads.children();
		for (let i = 0; i < threads_children.length; i ++){
			let $item = $(threads_children[i]);
			if (search_query.length){
				let thread_text = $item.text();
				if (thread_text.toLowerCase().indexOf(search_query.toLowerCase()) > -1) $item.removeClass('x-display-none');
				else $item.addClass('x-display-none');
			}
			else $item.removeClass('x-display-none');
		}
	});

	//on messages search
	$messages_search.on('input', function(event){
		let search_query = str($(this).val(), '', true);
		let chat_children = $messages_chat.children();
		for (let i = 0; i < chat_children.length; i ++){
			let $message_bubble = $(chat_children[i]);
			if (search_query.length){
				let bubble_text = $message_bubble.text();
				if (bubble_text.toLowerCase().indexOf(search_query.toLowerCase()) > -1) $message_bubble.removeClass('x-display-none');
				else $message_bubble.addClass('x-display-none');
			}
			else $message_bubble.removeClass('x-display-none');
		}
	});

	//on attachment
	$chat_attach_file.click(() => setTimeout(() => $attach_input.click(), 200));
	$attach_input.change(function(event){
		let files = event.target.files;
		for (let i = 0; i < files.length; i ++){
			let file = files[i];
			let file_name = file.name;
			let file_type = file.type;
			let id = uid();
			let isImage = file_type.toLowerCase().indexOf('image/') > -1;
			fileBase64(file).then((base64) => {
				let type = isImage ? 'image' : 'file';
				let $file_attachment = $(template_file_attachment(id, type, file_name));
				$chat_file_attachments.append($file_attachment);
				ATTACHMENTS[id] = {
					type: type,
					name: file_name,
					data: base64,
				};
				$attach_input[0].value = '';
				$chat_file_attachments.flashGreen(1, 200);
			}, (err) => console.error(err));
		}
	});
	$(document.body).on('click', sel_file_attachment_close, function(event){
		let id = str($(this).attr('data-id'), '', true);
		if (id !== '' && ATTACHMENTS.hasOwnProperty(id)){
			$(this).parent().remove();
			delete ATTACHMENTS[id];
		}
	});

	//on order
	$chat_attach_order.click(() => attach_order());

	//on quote
	$chat_attach_quote.click(() => attach_quote());

	//send message
	$chat_message_send.click(function(event){
		let message = str($chat_message.val(), '', true);
		let thread_user = str(SELECTED_THREAD, '', true);
		if (!thread_user.length || !message.length && !isObj(ATTACHMENTS, true, true).length) return event.preventDefault();
		let loading_callback = (toggle) => {
			if (toggle){
				$chat_new.addClass('x-block');
				$chat_message_send.toggleDisable(true);
				$chat_message_send.html('<i class="uk-icon-spinner uk-icon-spin"></i><span class="uk-hidden-small"> Sending...</span>');
			}
			else {
				$chat_new.removeClass('x-block');
				$chat_message_send.toggleDisable();
				$chat_message_send.html('<i class="uk-icon-send"></i><span class="uk-hidden-small"> Send</span>');
			}
		};
		let post_url = '';
		let post_data = objectParams({
			send_message: message,
			recipient: thread_user,
			attachments: btoa(stringify(ATTACHMENTS)),
		});
		serverPOST(post_url, post_data, loading_callback).then((res) => {
			let bubble_html = template_message_bubble(res.data);
			if (bubble_html === false) console.error('Unable to generate message bubble html!', bubble_html);
			else $messages_chat.append($(bubble_html));
			$messages.scrollBottom();
			$chat_message.val('');
			$chat_file_attachments.html('');
			ATTACHMENTS = {};
			update_threads_now();
		}, (err) => {
			UKPrompt('<p class="uk-text-danger">' + err + '</p>');
		});
	});

	//on image attachment click
	$(document.body).on('click', sel_attachment_image, function(event){
		let preview = str($(this).attr('data-src'), '', true);
		if (!preview) return event.preventDefault();
		let html = '';
		html += '<a href="#" class="uk-modal-close uk-close uk-close-alt"></a>';
		html += '<img src="' + preview + '" alt="Image Preview">';
		UKModal(html, 'x-modal uk-modal-dialog-lightbox', null, null, null, true, true, false, true);
	});

	//on file attachment click
	$(document.body).on('click', sel_attachment_file, function(event){
		let download = str($(this).attr('data-src'), '', true);
		if (!download) return event.preventDefault();
		let name = $(this).text();
		openDownload(download, name);
	});

	//on order attachment click
	$(document.body).on('click', sel_attachment_order, function(event){
		let $bubble = $(this).parent().parent();
		let data = jsonParse(atob($(this).attr('data-base64')));
		if (!data) return event.preventDefault();
		let prompt = '<h2>Custom Order</h2><p><strong>' + data.title + '</strong> (' + data.category + ')<br><strong>Price: ' + data.price.commas(2) + '</strong></p>';
		if (data.agreement != '') prompt += '<p>' + data.agreement + '</p>';
		if ($bubble[0].className.indexOf('me') > -1){
			UKPrompt(prompt);
			return;
		};
		UKPromptConfirm(prompt, function(){
			let post_url = '';
			let post_data = objectParams({
				accept_order: data.id,
				service: data.service,
				agreement: data.agreement,
				ref: data.ref,
				price: data.price,
			});
			serverPOST(post_url, post_data, loading_block).then((res) => {
				update_threads_now();
				badgeSet('orders', num(badgeGet('orders')) + 1);
				UKPromptConfirm('<h2>Order Accepted!</h2><p>Would you like to view your order details?</p>', function(){
					window.location.href = ROOT_URL + '/orders/?order=' + res.data;
				}, null, 'View Order', 'uk-button-primary');
			}, (err) => {
				UKPrompt('<p class="uk-text-danger">' + err + '</p>');
			});
		}, null, 'Accept Order', 'uk-button-success');
	});

	//on quote attachment click
	$(document.body).on('click', sel_attachment_quote, function(event){
		let $bubble = $(this).parent().parent();
		let data = jsonParse(atob($(this).attr('data-base64')));
		if (!data) return event.preventDefault();
		let prompt = '<h2>Order Quotation</h2><p><strong>ORDER: ' + data.order + ' ' + data.title + '</strong><br><strong>Quote Price: ' + data.price.commas(2) + '</strong></p>';
		if (data.agreement != '') prompt += '<p>' + data.agreement + '</p>';
		if ($bubble[0].className.indexOf('me') > -1){
			UKPrompt(prompt);
			return;
		};
		UKPromptConfirm(prompt, function(){
			let post_url = '';
			let post_data = objectParams({
				accept_quotation: data.id,
				order: data.order,
				agreement: data.agreement,
				ref: data.ref,
				price: data.price,
			});
			serverPOST(post_url, post_data, loading_block).then((res) => {
				update_threads_now();
				UKPromptConfirm('<h2>Quotation Accepted!</h2><p>Would you like to view updated order details?</p>', function(){
					window.location.href = ROOT_URL + '/orders/?order=' + res.data;
				}, null, 'View Order', 'uk-button-primary');
			}, (err) => {
				UKPrompt('<p class="uk-text-danger">' + err + '</p>');
			});
		}, null, 'Accept Quotation', 'uk-button-success');
	});
});
