$(function(){
	//data
	if (!(window.hasOwnProperty("CHECKLIST_DATA") && isObj(CHECKLIST_DATA, true))) window["CHECKLIST_DATA"] = [];
	//checklist map
	let CHECKLIST_MAP = {};
	let update_checklist_map = () => {
		CHECKLIST_MAP = {};
		if (Array.isArray(CHECKLIST_DATA) && CHECKLIST_DATA.length){
			for (let g = 0; g < CHECKLIST_DATA.length; g ++){
				let group = clone(CHECKLIST_DATA[g]);
				if (hasKeys(group, "id", "items")){
					group.__index = g;
					group.__items = {};
					let tmp_complete_items = 0;
					if (Array.isArray(group.items) && group.items.length){
						for (let i = 0; i < group.items.length; i ++){
							let item = group.items[i];
							if (hasKeys(item, "id", "text", "complete")){
								if (item.complete) tmp_complete_items ++;
								item.__index = i;
								item.__group_index = g;
								group.__items[item.id] = item;
							}
						}
					}
					group.complete = tmp_complete_items >= Object.keys(group.__items).length ? 1 : 0;
					CHECKLIST_DATA[g].complete = group.complete;
					delete group.items;
					CHECKLIST_MAP[group.id] = group;
				}
			}
		}
		return CHECKLIST_MAP;
	};
	update_checklist_map();

	//function helpers
	let check_controls = (controls) => {
		if (Array.isArray(controls) && controls.length){
			for (let i = 0; i < controls.length; i ++){
				if (!$(controls[i]).length){
					console.error("Control not found at: " + i, controls[i], controls);
					return false;
				}
			}
			return true;
		}
		console.error("Invalid check controls list!", controls);
		return false;
	};
	let group_data = (group) => {
		if (!hasKeys(group, "id", "title", "description", "timestamp")) return null;
		let group_map = clone(propval(CHECKLIST_MAP, group.id));
		if (group_map){
			let group_index = group_map.__index;
			group_map.items = clone(CHECKLIST_DATA[group_index].items); clone(group_map.__items);
			delete group_map.__items;
			delete group_map.__index;
			group = objectMerge(group_map, group, true);
			CHECKLIST_DATA[group_index] = group;
		}
		else {
			group = objectMerge(group, {
				id: "",
				title: "",
				description: "",
				items: [],
				items_count: 0,
				items_complete: 0,
				progress: 0,
				complete: 0,
				timestamp: "",
			});
			CHECKLIST_DATA.push(group);
			group.is_new = true;
		}
		update_checklist_map();
		return group;
	};
	let group_template = (group) => {
		group = group_data(group);
		if (!group){
			console.error("Invalid group object", group);
			return false;
		}
		let item_classes = [];
		if (group.complete) item_classes.push("complete");
		item_classes = item_classes.join(" ");
		let data_search = [group.title, group.description].join(" ").trim();
		let html = '';
		html += '<li tabindex="0" ' + (item_classes.length ? 'class="' + item_classes + '"' : '') + 'data-id="' + group.id + '" data-search="' + data_search + '">';
		html += '<div class="x-actions">';
		html += '<a href="javascript:" title="Edit" draggable="false" class="group-edit uk-text-primary"><i class="uk-icon-pencil"></i></a> ';
		html += '<a href="javascript:" title="Delete" draggable="false" class="group-delete uk-text-danger"><i class="uk-icon-trash"></i></a> ';
		html += '</div>';
		html += '<h3>' + group.title + '</h3>';
		html += '<small class="x-nowrap">' + group.timestamp + ' | <strong>' + group.items_complete + '/' + group.items_count + ' ' + group.progress + '%</strong></small>';
		html += '</li>';
		return html;
	};
	let item_data = (item) => {
		let is_item = (item) => {
			return hasKeys(item, "id", "group_id", "text", "timestamp", "complete", "overdue");
		};
		if (!is_item(item)){
			console.error("Invalid group item object", item);
			return false;
		}
		let group_map = propval(CHECKLIST_MAP, item.group_id);
		if (!hasKeys(group_map, "id", "__items")){
			console.error("Failed to get item group data", item);
			return false;
		}
		let item_map = null, group_index = group_map.__index;
		let test_group = propval(CHECKLIST_DATA, group_index);
		if (!(hasKeys(test_group, "id") && test_group.id == group_map.id)){
			console.error("Group mapped index mismatch!", group_map, test_group);
			return false;
		}
		if (hasKeys(group_map.__items, item.id) && hasKeys((item_map = group_map.__items[item.id]), "__index", "__group_index")){
			if (item_map.id != item.id){
				console.error("Group mapped item id mismatch!", item_map, item);
				return false;
			}
			let item_index = item_map.__index;
			let test_item = propval(CHECKLIST_DATA, group_index, "items", item_index);
			if (!(is_item(test_item) && test_item.id == item_map.id)){
				console.error("Group mapped item index mismatch!", group_map, item, test_item);
				return false;
			}
			delete item_map.__index;
			delete item_map.__group_index;
			item = objectMerge(item_map, item, true);
			if (!test_item.checked && item.checked){
				CHECKLIST_DATA[group_index].complete_items += 1;
				let tmp_group = CHECKLIST_DATA[group_index];
				let tmp_progress = Math.floor(tmp_group.complete_items / tmp_group.items_count * 100);
				let tmp_complete = tmp_group.items_count > 0 && tmp_group.items_count == tmp_group.complete_items;
				CHECKLIST_DATA[group_index].progress = tmp_progress;
				CHECKLIST_DATA[group_index].complete = tmp_complete;
			}
			CHECKLIST_DATA[group_index].items[item_index] = item;
		}
		else {
			item = objectMerge(item, {
				id: "",
				group_id: group_map.id,
				text: "",
				due_date: "",
				due_date_text: "",
				overdue: 0,
				complete: 0,
				timestamp: "",
				complete_timestamp: "",
			});
			if (!(Array.isArray(CHECKLIST_DATA[group_index].items) && CHECKLIST_DATA[group_index].items.length)) CHECKLIST_DATA[group_index].items = [];
			CHECKLIST_DATA[group_index].items.push(item);
			CHECKLIST_DATA[group_index].items_count += 1;
			if (item.complete) CHECKLIST_DATA[group_index].complete_items += 1;
			let tmp_group = CHECKLIST_DATA[group_index];
			let tmp_progress = Math.floor(tmp_group.complete_items / tmp_group.items_count * 100);
			let tmp_complete = tmp_group.items_count > 0 && tmp_group.items_count == tmp_group.complete_items;
			CHECKLIST_DATA[group_index].progress = tmp_progress;
			CHECKLIST_DATA[group_index].complete = tmp_complete;
		}
		update_checklist_map();
		return item;
	};
	let group_item_template = (item) => {
		if (!hasKeys(item, "id", "text", "due_date", "due_date_text", "complete", "complete_timestamp", "overdue")){
			console.error("Invalid checklist group item object", item);
			return false;
		}
		let html = '';
		let item_data_search = [item.text, item.due_date_text].join(" ").trim();
		let item_classes = [];
		if (item.complete) item_classes.push("complete");
		if (item.overdue) item_classes.push("overdue");
		item_classes = item_classes.join(" ").trim();
		html += '<li tabindex="0" ' + (item_classes.length ? 'class="' + item_classes + '" ' : '') + 'data-search="' + item_data_search + '" data-group="' + item.group_id + '" data-id="' + item.id + '">';
		html += '<div class="x-actions uk-form">'; //2
		html += '<input class="item-check x-block" type="checkbox"' + (item.complete ? ' checked="checked"' : '') + '>';
		html += '<a href="javascript:" title="Edit" draggable="false" class="item-edit uk-text-primary"><i class="uk-icon-pencil"></i></a> ';
		html += '<a href="javascript:" title="Delete" draggable="false" class="item-delete uk-text-danger"><i class="uk-icon-trash"></i></a> ';
		html += '</div>'; //2
		html += '<p>' + item.text + '</p>';
		html += '<small>';
		html += item.timestamp;
		if (item.complete) html += '| Completed: ' + item.complete_timestamp;
		else if (item.due_date.trim().length) html += '| Due Date: ' + item.due_date;
		if (item.due_date_text.trim().length) html += ' <i><strong>[' + item.due_date_text + ']</strong></i>';
		html += '</small>';
		html += '</li>';
		return html;
	};
	let group_items_template = (group) => {
		group = group_data(group);
		if (!group){
			console.error("Invalid group object", group);
			return false;
		}
		let html = '';
		html += '<div data-items-group="' + group.id + '" class="x-checklist-group x-hidden">'; //1
		html += '<div class="x-pad-20 x-group-header">'; //2
		html += '<h2 class="x-group-title">' + group.title + '</h2>' + (group.description.trim().length ? ' <p class="x-group-description">' + group.description.trim() + '</p>' : '');
		html += '<div class="uk-form-icon uk-width-1-1 x-items-search-wrapper">'; //3
		html += '<i class="uk-icon-search"></i>';
		html += '<input class="uk-width-1-1 x-items-search" type="text" placeholder="Search Items">';
		html += '</div>'; //3
		html += '</div>'; //2
		html += '<div class="x-nothing x-items-search-no-results x-border-top x-hidden">'; //2
		html += '<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />';
		html += '<div>'; //3
		html += '<h3 class="x-fw-400">No Results</h3>';
		html += '<p>Refine your search to try again.</p>';
		html += '</div>'; //3
		html += '</div>'; //2
		html += '<ul class="x-group-items x-border-top">';
		if (group.hasOwnProperty("items") && Array.isArray(group.items) && group.items.length){
			for (let i = 0; i < group.items.length; i ++){
				let item_template = group_item_template(group.items[i]);
				if (!item_template) continue;
				html += item_template;
			}
		}
		html += '</ul>';
		html += '</div>'; //1
		return html;
	};

	//controls ~ group items
	let $group_items_get_started = $(".x-get-started");
	let $group_items_wrapper = $(".x-checklist-group-items");
	let $group_item_add_wrapper = $group_items_wrapper.find(".x-item-add");
	let $group_item_add_button = $group_item_add_wrapper.find("button");
	let $group_item_edit_wrapper = $group_items_wrapper.find(".x-item-edit");
	let $edit_item_index = $group_item_edit_wrapper.find("#edit_item_index");
	let $edit_item_id = $group_item_edit_wrapper.find("#edit_item_id");
	let $edit_item_group_id = $group_item_edit_wrapper.find("#edit_item_group_id");
	let $edit_item_text = $group_item_edit_wrapper.find("#edit_item_text");
	let $edit_item_due_date = $group_item_edit_wrapper.find("#edit_item_due_date");
	let $edit_item_cancel = $group_item_edit_wrapper.find(".x-item-edit-cancel");
	let $edit_item_save = $group_item_edit_wrapper.find(".x-item-edit-save");
	let items_controls = [$group_items_get_started, $group_items_wrapper, $group_item_add_wrapper, $group_item_add_button, $group_item_edit_wrapper, $edit_item_index, $edit_item_id, $edit_item_group_id, $edit_item_text, $edit_item_due_date, $edit_item_cancel, $edit_item_save];

	//controls ~ groups
	let $groups_search = $(".x-groups-search");
	let $groups_search_no_results = $(".x-groups-search-no-results");
	let $group_list_wrapper = $("ul.x-checklist-groups");
	let $group_edit_wrapper = $(".x-group-edit");
	let $group_add_wrapper = $(".x-group-add");
	let $group_add_btn = $group_add_wrapper.find("button");
	let $edit_group_index = $group_edit_wrapper.find("#edit_group_index");
	let $edit_group_id = $group_edit_wrapper.find("#edit_group_id");
	let $edit_group_title = $group_edit_wrapper.find("#edit_group_title");
	let $edit_group_description = $group_edit_wrapper.find("#edit_group_description");
	let $edit_group_cancel = $group_edit_wrapper.find(".x-group-edit-cancel");
	let $edit_group_save = $group_edit_wrapper.find(".x-group-edit-save");
	let group_controls = [$groups_search, $groups_search_no_results, $group_list_wrapper, $group_edit_wrapper, $group_add_wrapper, $group_add_btn, $edit_group_index, $edit_group_id, $edit_group_title, $edit_group_description, $edit_group_cancel, $edit_group_save];

	//check controls
	if (!check_controls(items_controls.concat(group_controls))){
		console.error("Missing controls. Please check!");
		return;
	}

	//groups search
	$groups_search.on("input", function(event){
		if (!CHECKLIST_DATA.length) return event.preventDefault();
		$groups_search_no_results.addClass("x-hidden");
		let $group_list_items = $group_list_wrapper.find("li");
		let query = $(this).val();
		if (!query.length){
			$group_list_items.removeClass("x-display-none");
			return;
		}
		let found_results = 0;
		if ($group_list_items.length){
			for (let i = 0; i < $group_list_items.length; i ++){
				//let $group_list_item = $("ul.x-checklist-groups > li[data-id='" + group.id + "']");
				let $group_list_item = $($group_list_items[i]);
				if ($group_list_item.length){
					let data_search = str($group_list_item.attr("data-search"), "", true);
					if (data_search.length && data_search.toLowerCase().indexOf(query.toLowerCase()) > -1){
						$group_list_item.removeClass("x-display-none");
						found_results ++;
					}
					else $group_list_item.addClass("x-display-none");
				}
			}
		}
		if (!found_results) $groups_search_no_results.removeClass("x-hidden");
	});

	//function helpers
	let save_active = (group_id) => {
		if (window.localStorage) localStorage.setItem("ag", group_id);
	};
	let get_saved_active = () => {
		if (window.localStorage){
			let group_id = str(localStorage.getItem("ag"), "", true);
			if (group_id.length){
				let group_map = propval(CHECKLIST_MAP, group_id);
				if (hasKeys(group_map, "__index")) return group_map;
			}
		}
		return null;
	};
	let edit_group_clear = () => {
		$edit_group_index.val("");
		$edit_group_id.val("");
		$edit_group_title.val("");
		$edit_group_description.val("");
	};
	let edit_group_toggle = (show) => {
		if (show){
			$group_add_wrapper.addClass("x-hidden");
			$group_edit_wrapper.removeClass("x-hidden");
			$group_edit_wrapper.scrollToMe();
			$group_edit_wrapper.flashGreen(1, 50);
		}
		else {
			$group_edit_wrapper.addClass("x-hidden");
			$group_add_wrapper.removeClass("x-hidden");
			edit_group_clear();
		}
	};
	let edit_item_clear = () => {
		$edit_item_index.val("");
		$edit_item_id.val("");
		$edit_item_group_id.val("");
		$edit_item_text.val("");
		$edit_item_due_date.val("");
	};
	let edit_item_toggle = (show) => {
		if (show){
			let $active_group_list_item = $group_list_wrapper.find("li.active");
			if ($active_group_list_item.length){
				let group_id = str($active_group_list_item.attr("data-id"), "", true);
				if (group_id.length) $edit_item_group_id.val(group_id);
				else console.error("Failed to get active group list item reference");
			}
			else console.error("Failed to get active group list item");
			$group_item_add_wrapper.addClass("x-hidden");
			$group_item_edit_wrapper.removeClass("x-hidden");
			$group_item_edit_wrapper.scrollToMe();
			$group_item_edit_wrapper.flashGreen(1, 50);
		}
		else {
			$group_item_edit_wrapper.addClass("x-hidden");
			$group_item_add_wrapper.removeClass("x-hidden");
			edit_item_clear();
		}
	};
	let list_item_get_group = ($list_item) => {
		if (!$list_item.length) return -1;
		let group_id = str($list_item.attr("data-id"), "", true);
		if (!group_id.length) return -2;
		let group_map = propval(CHECKLIST_MAP, group_id);
		if (!group_map) return -3;
		return group_map;
	};
	let group_list_item_activate = (group_id) => {
		group_id = str(group_id, "", true);
		$group_list_items = $group_list_wrapper.find("li");
		if (group_id.length && $group_list_items.length){
			$group_list_items.removeClass("active");
			$group_list_item = $group_list_wrapper.find("li[data-id='" + group_id + "']");
			if ($group_list_item.length){
				let group_map = propval(CHECKLIST_MAP, group_id);
				if (group_map.complete) $group_list_item.addClass("complete");
				else $group_list_item.removeClass("complete");
				$group_list_item.addClass("active");
				save_active(group_id);
			}
			else console.error("Group list item not found!", group_id, $group_list_items); //TODO ~ check log items
		}
	};
	let group_items_activate = (group_id) => {
		let $group_items_children = $group_items_wrapper.find(".x-checklist-group[data-items-group]");
		if ((group_id = str(group_id, "", true)).length && $group_items_children.length){
			//hide other groups
			$group_items_children.addClass("x-hidden");

			//validate group id
			let group = propval(CHECKLIST_MAP, group_id);
			if (!group){
				console.error("Failed to get group data!", group);
				return false;
			}

			//display group items
			$group_items_child = $group_items_wrapper.find(".x-checklist-group[data-items-group='" + group.id + "']");
			if ($group_items_child.length) $group_items_child.removeClass("x-hidden");
			else console.error("Group items child not found!", group_id);
		}
	};
	let reset_group_display = (group_id) => {
		let $group_items_children = $group_items_wrapper.find(".x-checklist-group[data-items-group]");
		edit_group_toggle();
		edit_item_toggle();
		if ($group_items_children.length){
			$group_items_wrapper.removeClass("x-hidden");
			$group_items_get_started.addClass("x-hidden");

			//activate group
			if ((group_id = str(group_id, "", true)).length){
				group_list_item_activate(group_id);
				group_items_activate(group_id);
			}
		}
		else {
			$group_items_get_started.removeClass("x-hidden");
			$group_items_wrapper.addClass("x-hidden");
		}
	};
	let group_remove = (group_id) => {
		//validate group id
		if (!(group_id = str(group_id, "", true)).length) return false;
		let group = propval(CHECKLIST_MAP, group_id);
		if (!group){
			console.error("Failed to get group data!", group);
			return false;
		}

		//remove from checklist data
		CHECKLIST_DATA.splice(group.__index, 1);
		update_checklist_map();

		//remove group list item
		$group_list_item = $group_list_wrapper.find("li[data-id='" + group_id + "']");
		if ($group_list_item.length) $group_list_item.remove();
		else console.error("Failed to find list item for group", group_id);

		//remove group items
		$group_items_child = $group_items_wrapper.find(".x-checklist-group[data-items-group='" + group_id + "']");
		if ($group_items_child.length) $group_items_child.remove();
		else console.error("Failed to find items for group", group_id);

		//reset display
		reset_group_display(propval(CHECKLIST_DATA, 0, "id"));
	};
	let group_update = (group) => {
		//validate group data
		group = group_data(group);
		if (!group){
			console.error("Invalid group object", group);
			return false;
		}

		//create templates
		let template_group = group_template(group);
		if (!template_group) return false;
		let template_group_items = group_items_template(group);
		if (!template_group_items) return false;

		//replace/insert template
		let $group_list_item = $group_list_wrapper.find("li[data-id='" + group.id + "']");
		let $group_items_child = $group_items_wrapper.find(".x-checklist-group[data-items-group='" + group.id + "']");
		if ($group_list_item.length) $group_list_item.replaceWith($(template_group));
		else $group_list_wrapper.append($(template_group));
		if ($group_items_child.length) $group_items_child.replaceWith($(template_group_items));
		else $(template_group_items).insertBefore($group_item_add_wrapper);

		//reset group display
		reset_group_display(group.id);

		//flash green and scroll to items
		$group_list_item = $group_list_wrapper.find("li[data-id='" + group.id + "']");
		$group_items_child = $group_items_wrapper.find(".x-checklist-group[data-items-group='" + group.id + "']");
		if ($group_list_item.length) $group_list_item.flashGreen(2, 100);
		else console.error("Unable to find group list item element", group, $group_list_item);
		if ($group_items_child.length){
			$group_items_child.flashGreen(2, 100);
			$group_items_child.scrollToMe();
		}
		else console.error("Unable to find group items element", group, $group_items_child);
	};
	let item_update = (item, scroll_to) => {
		item = item_data(item);
		if (!item) return false;

		//create template
		let template_item = group_item_template(item);
		if (!template_item) return false;

		//group item elements
		let $group_items_child = $group_items_wrapper.find(".x-checklist-group[data-items-group='" + item.group_id + "']");
		if (!$group_items_child.length){
			console.error("Unable to find group items element", item, $group_items_child);
			return false;
		}
		let $group_items_child_list = $group_items_child.find("ul.x-group-items");
		if (!$group_items_child_list.length){
			console.error("Unable to find group items list element", item, $group_items_child_list);
			return false;
		}

		//replace/insert template
		let $group_items_child_list_item = $group_items_child_list.find("li[data-id='" + item.id + "']");
		if ($group_items_child_list_item.length) $group_items_child_list_item.replaceWith($(template_item));
		else $group_items_child_list.append($(template_item));

		//reset group display
		reset_group_display(item.group_id);

		//flash green and scroll to items
		$group_items_child_list_item = $group_items_child_list.find("li[data-id='" + item.id + "']");
		if ($group_items_child_list_item.length){
			$group_items_child_list_item.flashGreen(2, 100);
			if (scroll_to) $group_items_child_list_item.scrollToMe();
		}
		else console.error("Unable to find group items list item element", item, $group_items_child_list_item);
	};

	//init reset display
	let saved_group = get_saved_active();
	if (hasKeys(saved_group, "__index")) reset_group_display(propval(CHECKLIST_DATA, saved_group.__index, "id"));
	else reset_group_display(propval(CHECKLIST_DATA, 0, "id"));

	//on add group
	$group_add_btn.click(() => edit_group_toggle(true));

	//on group edit cancel
	$edit_group_cancel.click(() => edit_group_toggle());

	//on group edit save
	$edit_group_save.click(function(event){
		let edit_group_id = str($edit_group_id.val(), "", true);
		let edit_group_title = str($edit_group_title.val(), "", true);
		let edit_group_description = str($edit_group_description.val(), "", true);
		if (!edit_group_title.length){
			UKPrompt('<p class="uk-text-warning">Kindly enter the checklist group title.</p>');
			return event.preventDefault();
		}
		let post_url = "";
		let post_data = "edit_checklist_group=" + encodeURIComponent(edit_group_id) + "&group_title=" + encodeURIComponent(edit_group_title) + "&group_description=" + encodeURIComponent(edit_group_description);
		serverPOST(post_url, post_data, loading_block).then((res) => {
			group_update(res.data);
			notify(res.message, "success");
		}, function(err){
			UKPrompt('<p class="uk-text-danger">' + String(err) + '</p>');
			console.error(err, SERVER_RESPONSE_DATA);
		});
	});

	//on group item ~ edit
	$(document.body).on("click", ".x-checklist-groups li .x-actions > a.group-edit", function(event){
		let $list_item = $(this).parent().parent();
		let group = list_item_get_group($list_item);
		if (!hasKeys(group, "id", "__index", "__items")){
			console.error("Failed to get group edit data!", group, $list_item);
			event.preventDefault();
			return false;
		}
		$edit_group_index.val(String(group.__index));
		$edit_group_id.val(group.id);
		$edit_group_title.val(group.title);
		$edit_group_description.val(group.description);
		edit_group_toggle(true);
		return false;
	});

	//on group item ~ delete
	$(document.body).on("click", ".x-checklist-groups li .x-actions > a.group-delete", function(event){
		let $list_item = $(this).parent().parent();
		let group = list_item_get_group($list_item);
		if (!hasKeys(group, "id", "__index", "__items")){
			console.error("Failed to get group delete data!", group, $list_item);
			event.preventDefault();
			return false;
		}
		let confirm_prompt = '<p class="uk-text-danger">Delete checklist group <strong>"' + group.title + '"</strong> and all related items <strong>permanently</strong>? This action cannot be undone.</p>';
		UKPromptConfirm(confirm_prompt, function(){
			let post_url = "";
			let post_data = "delete_checklist_group=" + encodeURIComponent(group.id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				group_remove(group.id);
				notify(res.message, "success");
			}, function(err){
				UKPrompt('<p class="uk-text-danger">' + String(err) + '</p>');
				console.error(err, SERVER_RESPONSE_DATA);
			});
		}, null, "Delete Group", "uk-button-danger");
		return false;
	});

	//on group item ~ select
	$(document.body).on("click", ".x-checklist-groups li", function(event){
		let $list_item = $(this);
		let group = list_item_get_group($list_item);
		if (!hasKeys(group, "id", "__index", "__items")){
			console.error("Failed to get selected group data!", group, $list_item);
			event.preventDefault();
			return false;
		}
		reset_group_display(group.id);
		if (!isUKLarge()) toggleGroupsContent();
		return false;
	});

	//on add item
	$group_item_add_button.click(() => edit_item_toggle(true));

	//on item edit cancel
	$edit_item_cancel.click(() => edit_item_toggle());

	//on item edit save
	$edit_item_save.click(function(event){
		let item_index = num($edit_item_index.val(), -1);
		let item_id = str($edit_item_id.val(), "", true);
		let item_group_id = str($edit_item_group_id.val(), "", true);
		let item_text = str($edit_item_text.val(), "", true);
		let item_due_date = str($edit_item_due_date.val(), "", true);
		if (!item_group_id.length){
			UKPrompt('<p class="uk-text-danger">Invalid item group reference. Kindly refresh your browser and try again.</p>');
			return event.preventDefault();
		}
		let group_map = propval(CHECKLIST_MAP, item_group_id);
		if (!hasKeys(group_map, "id", "__items")){
			UKPrompt('<p class="uk-text-danger">Failed to get item group data. Kindly refresh your browser and try again.</p>');
			return event.preventDefault();
		}
		if (item_id.length && !(hasKeys(group_map.__items, item_id) && item_index == propval(group_map, "__items", item_id, "__index"))){
			console.log("save", item_id, group_map, item_index,propval(group_map, "__items", item_id, "__index"));
			UKPrompt('<p class="uk-text-danger">Failed to get item data. Kindly refresh your browser and try again.</p>');
			return event.preventDefault();
		}
		if (!item_text.length){
			UKPrompt('<p class="uk-text-warning">Kindly enter your checklist item text.</p>');
			return event.preventDefault();
		}
		if (item_due_date.length){
			let validate_date_string = (date_string, date_format) => {
				date_format = str(date_format, "DD/MM/YYYY", true);
				let temp_date = moment(date_string, date_format).toDate();
				if (!(temp_date instanceof Date && !isNaN(temp_date.getTime()))) return false;
				temp_date = moment(temp_date).format(date_format);
				if (temp_date != date_string) return false;
				return temp_date;
			};
			if (!validate_date_string(item_due_date)){
				UKPrompt('<p class="uk-text-danger">Invalid due date format. Kindly use the format DD/MM/YYYY.</p>');
				return event.preventDefault();
			}
		}

		//post request
		let post_url = "";
		let post_data = [];
		post_data.push("edit_checklist_item=" + encodeURIComponent(item_id));
		post_data.push("group_id=" + encodeURIComponent(item_group_id));
		post_data.push("text=" + encodeURIComponent(item_text));
		post_data.push("due_date=" + encodeURIComponent(item_due_date));
		post_data = post_data.join("&");
		serverPOST(post_url, post_data, loading_block).then((res) => {
			item_update(res.data, true);
			notify(res.message, "success");
		}, function(err){
			UKPrompt('<p class="uk-text-danger">' + String(err) + '</p>');
			console.error(err, SERVER_RESPONSE_DATA);
		});
	});

	//on item search
	$(document.body).on("input", ".x-checklist-group-items .x-items-search", function(event){
		let $group_items_wrapper = $(this).parent().parent().parent();
		let $search_group_items_no_results = $group_items_wrapper.find(".x-items-search-no-results");
		let $search_group_items = $(this).parent().parent().parent().find(".x-group-items li");
		if (!$search_group_items_no_results.length || !$search_group_items.length) return event.preventDefault();
		edit_item_toggle();
		$search_group_items_no_results.addClass("x-hidden");
		let query = $(this).val();
		if (!query.length){
			$search_group_items.removeClass("x-display-none");
			return;
		}
		let found_results = 0;
		if ($search_group_items.length){
			for (let i = 0; i < $search_group_items.length; i ++){
				let $search_group_item = $($search_group_items[i]);
				if ($search_group_item.length){
					let data_search = str($search_group_item.attr("data-search"), "", true);
					if (data_search.length && data_search.toLowerCase().indexOf(query.toLowerCase()) > -1){
						$search_group_item.removeClass("x-display-none");
						found_results ++;
					}
					else $search_group_item.addClass("x-display-none");
				}
			}
		}
		if (!found_results) $search_group_items_no_results.removeClass("x-hidden");
	});

	//on item ~ edit
	$(document.body).on("click", ".x-checklist-group-items .x-group-items li .x-actions .item-edit", function(event){
		let $list_item = $(this).parent().parent();
		let group_id = str($list_item.attr("data-group"), "", true);
		let item_id = str($list_item.attr("data-id"), "", true);
		let item_map = null;
		if (!(group_id.length && item_id.length && hasKeys((item_map = propval(CHECKLIST_MAP, group_id, "__items", item_id)), "id", "__index", "__group_index"))){
			console.error("Failed to get group item data!", group_id, item_id, $list_item);
			event.preventDefault();
			return false;
		}
		$edit_item_index.val(String(item_map.__index));
		$edit_item_id.val(item_map.id);
		$edit_item_group_id.val(item_map.group_id);
		$edit_item_text.val(item_map.text);
		let tmp_due_date = item_map.due_date.trim();
		$edit_item_due_date.val("");
		if (tmp_due_date.length){
			let temp_date = moment(tmp_due_date, "YYYY-MM-DD").toDate();
			if (temp_date instanceof Date && !isNaN(temp_date.getTime())){
				temp_date = moment(temp_date).format("DD/MM/YYYY");
				$edit_item_due_date.val(temp_date);
				console.log("convert due_date: " + tmp_due_date + " > " + temp_date);
			}
			else console.error("Error converting date", tmp_due_date, temp_date);
		}
		edit_item_toggle(true);
		return false;
	});

	//on item ~ delete
	$(document.body).on("click", ".x-checklist-group-items .x-group-items li .x-actions .item-delete", function(event){
		let $list_item = $(this).parent().parent();
		let group_id = str($list_item.attr("data-group"), "", true);
		let item_id = str($list_item.attr("data-id"), "", true);
		let item_map = null;
		if (!(group_id.length && item_id.length && hasKeys((item_map = propval(CHECKLIST_MAP, group_id, "__items", item_id)), "id", "__index", "__group_index"))){
			console.error("Failed to get group item data!", group_id, item_id, $list_item);
			event.preventDefault();
			return false;
		}
		let confirm_prompt = '<p class="uk-text-danger">Delete checklist item <strong>"' + item_map.text + '"</strong>? This action cannot be undone.</p>';
		UKPromptConfirm(confirm_prompt, function(){
			let post_url = "";
			let post_data = "delete_checklist_item=" + encodeURIComponent(item_map.id) + "&group_id=" + encodeURIComponent(item_map.group_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				//remove entry
				let group_index = item_map.__group_index;
				CHECKLIST_DATA[group_index].items.splice(item_map.__index, 1);
				$list_item.remove();

				//update data
				CHECKLIST_DATA[group_index].items_count -= 1;
				if (item_map.checked) CHECKLIST_DATA[group_index].complete_items -= 1
				let tmp_group = CHECKLIST_DATA[group_index];
				let tmp_progress = Math.floor(tmp_group.complete_items / tmp_group.items_count * 100);
				let tmp_complete = tmp_group.items_count > 0 && tmp_group.items_count == tmp_group.complete_items;
				CHECKLIST_DATA[group_index].progress = tmp_progress;
				CHECKLIST_DATA[group_index].complete = tmp_complete;

				//refresh
				update_checklist_map();
				reset_group_display(item_map.group_id);
				notify(res.message, "success");
			}, function(err){
				UKPrompt('<p class="uk-text-danger">' + String(err) + '</p>');
				console.error(err, SERVER_RESPONSE_DATA);
			});
		}, null, "Delete Item", "uk-button-danger");
		return false;
	});

	//on item ~ select
	$(document.body).on("click", ".x-checklist-group-items .x-group-items li", function(event){
		let $list_item = $(this);
		let $item_check = $list_item.find("input.item-check");
		if (!$item_check.length){
			event.preventDefault();
			return false;
		}
		let group_id = str($list_item.attr("data-group"), "", true);
		let item_id = str($list_item.attr("data-id"), "", true);
		let item_map = null;
		if (!(group_id.length && item_id.length && hasKeys((item_map = propval(CHECKLIST_MAP, group_id, "__items", item_id)), "id", "__index", "__group_index"))){
			console.error("Failed to get group item data!", group_id, item_id, $list_item);
			event.preventDefault();
			return false;
		}
		let check_item = $item_check.isChecked() ? 0 : 1;
		let post_url = "";
		let post_data = "check_checklist_item=" + encodeURIComponent(item_map.id) + "&group_id=" + encodeURIComponent(item_map.group_id) + "&checked=" + encodeURIComponent(check_item);
		serverPOST(post_url, post_data, loading_block).then((res) => {
			item_update(res.data);
		}, function(err){
			UKPrompt('<p class="uk-text-danger">' + String(err) + '</p>');
			console.error(err, SERVER_RESPONSE_DATA);
		});
		return false;
	});
});
