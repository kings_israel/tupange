//event data
if (!(window.hasOwnProperty("EVENT_DATA") && isObj(EVENT_DATA, true))) window["EVENT_DATA"] = {};

//event users
$(function(){

	//controls
	let $cancel_mini = $("#cancel_mini");
	let $save_mini = $("#save_mini");
	let $edit_add_wrapper = $(".x-edit-add");
	let $edit_add = $edit_add_wrapper.find("button");
	let $invite_form = $("#invite_form");
	let $event_user = $("#event_user");
	let $user_email = $("#user_email");
	let $user_username = $("#user_username");
	let $user_names = $("#user_names");
	let $user_role = $("#user_role");
	let $edit_cancel = $("#edit_cancel");
	let $edit_save = $("#edit_save");
	let $edit_saving = $("#edit_saving");

	//toggle form
	let toggle_form = (toggle) => {
		if (toggle){
			$event_user.val("");
			$user_email.val("");
			$user_username.val("");
			$user_names.val("");
			$user_role.val("0");
			$edit_save.html("Send Invite");
			$edit_add_wrapper.addClass('x-hidden');
			$invite_form.removeClass('x-hidden');
		}
		else {
			$invite_form.addClass('x-hidden');
			$edit_add_wrapper.removeClass('x-hidden');
		}
	};

	//form toggle
	$invite_form.submit(event => event.preventDefault());
	$edit_add.click(() => toggle_form(true));
	$edit_cancel.click(() => toggle_form(false));
	$cancel_mini.click(() => toggle_form(false));

	//save
	$edit_save.click(function(event){
		let event_user = str($event_user.val(), '', true);
		let email = str($user_email.val(), '', true);
		let username = str($user_username.val(), '', true);
		let names = str($user_names.val(), '', true);
		let role = num($user_role.val());
		if (!validateEmail(email)){
			$user_email.inputError('Invalid email address');
			return event.preventDefault();
		}
		if (username == ''){
			$user_username.inputError('Kindly enter a unique @username for this user');
			return event.preventDefault();
		}
		if (names == ''){
			$user_names.inputError('Kindly enter user full names');
			return event.preventDefault();
		}
		if ([0,1,2].indexOf(role) < 0){
			$user_role.inputError('Invalid role selection');
			return event.preventDefault();
		}
		let loading_callback = (toggle) => {
			if (toggle){
				$invite_form.addClass('x-block');
				$edit_save.addClass('x-hidden');
				$edit_saving.removeClass('x-hidden');
				$edit_saving.html(event_user != '' ? 'Saving...' : 'Sending...');
			}
			else {
				$invite_form.removeClass('x-block');
				$edit_save.removeClass('x-hidden');
				$edit_saving.addClass('x-hidden');
			}
		};
		let post_url = '';
		let post_data = objectParams({
			event_user: event_user,
			email: email,
			username: username,
			names: names,
			role: role,
		});
		serverPOST(post_url, post_data, loading_callback).then((res) => {
			toggle_form()
			UKPrompt('<p class="uk-text-success">' + res.message + '</p>');
			setTimeout(() => window.location.reload(), 2000);
		}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
	});

	//user edit
	$(document.body).on("click", "td a.user-edit", function(event){
		let $tr = $(this).parent().parent();
		let data = jsonParse(atob(str($tr.attr('data-base64'), '', true)));
		if (!data) return event.preventDefault();
		toggle_form(true);
		$event_user.val(data.id);
		$user_email.val(data.email);
		$user_username.val(data.username);
		$user_names.val(data.names);
		$user_role.val(String(data.role));
		$invite_form.scrollToMe();
		$invite_form.flashGreen(1, 200);
		$edit_save.html("Save Details");
	});

	//user delete
	$(document.body).on("click", "td a.user-delete", function(event){
		let user_id = str($(this).attr('data-id'), '', true);
		if (user_id === '') return event.preventDefault();
		let prompt = '<h2 class="uk-text-danger">Delete User</h2><p class="uk-text-danger">Are you sure you want to delete this user from your event?</p>';
		UKPromptConfirm(prompt, function(){
			let post_url = '';
			let post_data = 'delete_user=' + encodeURIComponent(user_id);
			serverPOST(post_url, post_data, loading_block).then(() => {
				toggle_form()
				UKPrompt('<p class="uk-text-success">' + res.message + '</p>');
				setTimeout(() => window.location.reload(), 2000);
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Delete', 'uk-button-danger');
	});

	//user resend invite
	$(document.body).on("click", "td a.user-resend", function(event){
		let user_id = str($(this).attr('data-id'), '', true);
		if (user_id === '') return event.preventDefault();
		let prompt = '<h2>Resend Invite</h2><p>Are you sure you want to resend invite?</p>';
		UKPromptConfirm(prompt, function(){
			let post_url = '';
			let post_data = 'resend_invite=' + encodeURIComponent(user_id);
			serverPOST(post_url, post_data, loading_block).then(() => {
				toggle_form()
				UKPrompt('<p class="uk-text-success">' + res.message + '</p>');
				setTimeout(() => window.location.reload(), 2000);
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Resend', 'uk-button-primary');
	});

});
