$(function(){
	//service on pause
	$(document.body).on("click", ".x-service-pause", function(event){
		let service_id = str($(this).attr("data-id"), "", true);
		let service_title = str($(this).attr("data-title"), "", true);
		if (!service_id.length || !service_title.length) return event.preventDefault();
		if (!window.hasOwnProperty("UKPromptConfirmTest")) return event.preventDefault();
		let prompt = "<h2>Pause Service</h2><p>Are you sure you want to pause this service? Paused services can be edited but are shown to clients.</p>";
		UKPromptConfirmTest(prompt, service_title, function(value){
			let redirect = ROOT_URL + "/services?view=edit&s=" + service_id + "&pause=" + value;
			window.location.href = redirect;
		}, null, "Pause Service", "uk-button-primary");
	});

	//service on delete
	$(document.body).on("click", ".x-service-delete", function(){
		let service_id = str($(this).attr("data-id"), "", true);
		let service_title = str($(this).attr("data-title"), "", true);
		if (!service_id.length || !service_title.length) return event.preventDefault();
		if (!window.hasOwnProperty("UKPromptConfirmTest")) return event.preventDefault();
		let prompt = '<h2 class="uk-text-danger">Delete Service</h2><p class="uk-text-danger">Are you sure you want to delete this service? Deleted services will be in your deleted list for 30 days after which they are removed permanently.</p>';
		UKPromptConfirmTest(prompt, service_title, function(value){
			let redirect = ROOT_URL + "/services?view=edit&s=" + service_id + "&delete=" + value;
			window.location.href = redirect;
		}, null, "Delete Service", "uk-button-danger");
	});
});
