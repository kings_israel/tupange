$(function(){
	$profile_status = $("#profile_status");
	$profile_status_form = $("#profile_status_form");
	$("#profile_status_edit").click(function(){
		$profile_status.hide();
		$profile_status_form.show();
	});
	$("#profile_status_cancel").click(function(){
		$profile_status_form.hide();
		$("#profile_status_update").val($("#profile_status_current").val());
		$profile_status.show();
	});
});
