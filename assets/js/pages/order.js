$(function(){
	//helpers
	let confirm_reason = (title, message, btn_text, btn_class, on_confirm) => {
		let html = '<a class="uk-modal-close uk-close"></a>';
		html += '<div class="uk-modal-header">' + str(title) + '</div>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<form action="javascript:" method="post" class="uk-form x-pad-20">'; //f
		html += '<p>' + str(message) + '</p>';
		html += '<div class="uk-form-row">'; //c
		html += '<label class="uk-form-label" for="reason">Price</label>';
		html += '<div class="uk-form-controls">';
		html += '<textarea id="reason" name="reason" class="uk-width-1-1" placeholder="Enter the reason for your action here"></textarea>';
		html += '</div>';
		html += '</form>'; //f
		html += '</div>'; //1
		html += '<div class="uk-modal-footer uk-text-right">';
		html += '<button type="button" class="uk-button uk-modal-close">Cancel</button> ';
		html += '<button type="button" class="x-btn-confirm uk-button ' + str(btn_class) + ' x-min-100">' + str(btn_text) + '</button> ';
		html += '</div>';
		UKModal(html, 'x-modal x-modal-nopad', function($modal, uk_modal){
			let $reason = $modal.find('#reason');
			let $confirm = $modal.find('.x-btn-confirm');
			$confirm.click(function(event){
				let reason = str($reason.val(), '', true);
				if (!reason.length){
					$reason.inputError('Kindly enter your reason');
					return event.preventDefault();
				}
				if ('function' === typeof on_confirm) on_confirm(reason);
				if (uk_modal.isActive()) uk_modal.hide();
			});
		}, null, null, false, false, true);
	};

	//my review
	$('.x-my-review').click(function(event){
		let default_avatar = ROOT_URL + "/assets/img/user.png";
		let order = ORDER_DATA;
		let my_review = null;
		if (order.hasOwnProperty('my_review')) my_review = order.my_review;
		reviewPrompt('Review - ' + order.service_title, my_review, function(data, uk_modal){
			let post_url = '';
			let post_data = objectParams({
				my_review: order.id,
				rating: data.rating,
				comment: data.comment,
			});
			serverPOST(post_url, post_data, loading_block).then((res) => {
				uk_modal.hide();
				loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
				window.location.reload();
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		});
	});

	$(document.body).on("click", ".item-remove", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		let item_index = -1;
		for (let i = 0; i < uk_datatable_rows.length; i ++){
			if (str($(uk_datatable_rows[i]).attr("data-id"), "", true) == item_id){
				item_index = i;
				break;
			}
		}
		if (item_index < 0){
			console.error("Unable to find item index", item_id);
			return event.preventDefault();
		}
		let prompt = '<h2>Remove Order</h2><p>This will cancel the order and it will be removed from the list permanently.</p><p>Are you sure you want to remove order <strong>"' + item_name + '"</strong>?</p>';
		UKPromptConfirm(prompt, function(){
			let post_url = "";
			let post_data = 'remove_order=' + encodeURIComponent(item_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				uk_datatable_rows.splice(item_index, 1);
				uk_datatable_refresh();
				view_badge_update(-1);
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Remove Order', 'uk-button-danger');
	});

	//order decline
	$(document.body).on("click", ".item-decline", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		confirm_reason('Decline Order', 'Declined orders will be removed from your client orders list permanently. Are you sure you want to decline order <strong>"' + item_name + '"</strong>?', 'Decline Order', 'uk-button-danger', function(reason){
			let post_url = "";
			let post_data = objectParams({
				decline_order: item_id,
				reason: reason,
			});
			serverPOST(post_url, post_data, loading_block).then((res) => {
				window.location.href = ROOT_URL + '/orders/?client';
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		});
	});

	// order delete
	$(document.body).on("click", ".item-delete", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		let prompt = '<h2>Delete Order</h2><p>Are you sure you want to permanently delete cancelled order <strong>"' + item_name + '"</strong>?</p>';
		UKPromptConfirm(prompt, function(){
			let post_url = "";
			let post_data = 'delete_order=' + encodeURIComponent(item_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				window.location.href = ROOT_URL + '/orders/?client';
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Delete Order', 'uk-button-danger');
	});

	//order receive
	$(document.body).on("click", ".item-receive", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		let post_url = "";
		let post_data = 'receive_order=' + encodeURIComponent(item_id);
		serverPOST(post_url, post_data, loading_block).then((res) => {
			$btn.remove();
			$status_td = $('.client-order-status');
			$status_td.html('RECEIVED');
		}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
	});
	
	//archive order
	$(document.body).on("click", ".item-archive", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		let post_url = "";
		let post_data = 'archive_order=' + encodeURIComponent(item_id);
		serverPOST(post_url, post_data, loading_block).then((res) => {
			$btn.remove();
			$status_td = $('.client-order-status');
			$status_td.html('ARCHIVED');
		}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
	});

	//order delivered
	$('.x-mark-delivered').click(function(){
		let order = ORDER_DATA;
		UKPromptConfirm('<h2>Delivered Order</h2><p>Do you want to mark this order as delivered? The client will be able to dispute or mark delivered orders as complete.<br><strong>Note: This action cannot be undone!</strong></p>', () => {
			let post_url = "";
			let post_data = 'deliver_order=' + encodeURIComponent(order.id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
				window.location.reload();
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Delivered', 'uk-button-success');
	});

	//order overdue
	$('.x-mark-overdue').click(function(){
		let order = ORDER_DATA;
		UKPromptConfirm('<h2 class="uk-text-danger">Overdue Order</h2><p class="uk-text-danger">Do you want to mark this order as overdue? Overdue orders can be disputed for reimbursement.<br><strong>Note: This action cannot be undone!</strong></p>', () => {
			let post_url = "";
			let post_data = 'overdue_order=' + encodeURIComponent(order.id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
				window.location.reload();
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Overdue', 'uk-button-danger');
	});

	//order complete
	$('.x-mark-complete').click(function(){
		let order = ORDER_DATA;
		UKPromptConfirm('<h2>Complete Order</h2><p>Do you want to mark this order as complete? You can rate and review complete orders.<br><strong>Note: This action cannot be undone!</strong></p>', () => {
			let post_url = "";
			let post_data = 'complete_order=' + encodeURIComponent(order.id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
				window.location.reload();
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Complete', 'uk-button-success');
	});

	//order dispute
	$('.x-mark-dispute').click(function(event){
		let order = ORDER_DATA;
		confirm_reason('Dispute Order', 'Are you sure you want to dispute delivered order? Disputed orders allows for order status review and action.', 'Dispute', 'uk-button-danger', function(reason){
			let post_url = "";
			let post_data = objectParams({
				dispute_order: order.id,
				reason: reason,
			});
			serverPOST(post_url, post_data, loading_block).then((res) => {
				loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
				window.location.reload();
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		});
	});

	//order checkout
	let checkoutPrompt = (callback) => {
		let order = ORDER_DATA;
		let pricing_price = num(order.pricing_price);
		let pricing_timestamp = order.pricing_timestamp;
		if (pricing_price <= 0){
			UKPromptConfirm('<h2 class="uk-text-warning">Order Checkout</h2><p class="uk-text-warning">This order has no pricing details. Check your inbox to request/follow-up on a order quotation from the vendor and try again.</p>', () => {
				window.location.href = ROOT_URL + '/messages/' + order.vendor_uid;
			}, null, '<i class="uk-icon-envelope-o"></i> Check Inbox', 'uk-button-primary');
			return false;
		}
		let html = '';
		html += '<a class="uk-modal-close uk-close"></a>';
		html += '<div class="uk-modal-header">CHECKOUT ORDER ' + order.id + '</div>';
		html += '<div class="uk-overflow-container x-payment-wrapper">'; //1
		html += '<div class="x-pad-20">'; //2
		html += '<div class="uk-grid uk-grid-small" data-uk-grid-margin>'; //g1
		html += '<div class="uk-width-medium-1-2 uk-hidden-small">'; //g11
		html += '<div class="x-checkout-details x-card">'; //d1
		html += '<h2>' + order.service_title + '<br><small>' + order.service_category + '</small></h2>';
		if (order.service_description.trim() !== '') html += '<p>' + order.service_description + '</p>';
		html += '<table cellpadding="0" cellspacing="0" border="0">';
		if (order.ref.trim() !== '') html += '<tr><td>Order Ref</td><td><strong>' + order.ref + '</strong></td></tr>';
		if (order.quote_ref.trim() !== '') html += '<tr><td>Quote Ref</td><td><strong>' + order.quote_ref + '</strong></td></tr>';
		html += '<tr><td>Pricing</td><td><strong>' + order.pricing_title + '</strong></td></tr>';
		if (order.pricing_description.trim() !== '') html += '<tr><td>Description</td><td><strong>' + order.pricing_description + '</strong></td></tr>';
		if (Array.isArray(order.pricing_package) && order.pricing_package.length){
			html += '<tr><td>Package</td><td>';
			for (let i = 0; i < order.pricing_package.length; i ++) html += (!i ? '' : '<br>') + '<strong>' + order.pricing_package[i] + '</strong>';
			html += '</td></tr>';
		}
		html += '<tr><td>Timestamp</td><td><strong>' + order.pricing_timestamp + '</strong></td></tr>';
		html += '</table>';
		html += '<h3><small>KES</small> ' + pricing_price.commas(2) + '</h3>';
		html += '</div>'; //d1
		html += '</div>'; //g11
		html += '<div class="uk-width-medium-1-2">'; //g12
		html += '<form id="payment-form" action="javascript:" method="post" class="uk-form">'; //f
		html += '<h1 class="uk-hidden-small">Payment</h1>';
		html += '<h1 class="uk-visible-small">Pay <span><small>KES</small> ' + pricing_price.commas(2) + '</span></h1>';
		html += '<div class="uk-form-row x-padding-bottom-20 x-border-bottom">'; //c
		html += '<input type="radio" id="payment-option-card" name="payment-options" checked="checked" value="card"> <label for="payment-option-card" class="x-radio"><i class="uk-icon-credit-card"></i> Debit/Credit Card</label><br>';
		//html += '<input type="radio" id="payment-option-gw" name="payment-options" value="gw"> <label for="payment-option-gw" class="x-radio"><i class="uk-icon-google-wallet"></i> Google Wallet</label><br>';
		html += '<input type="radio" id="payment-option-mob" name="payment-options" value="mob"> <label for="payment-option-mob" class="x-radio"><i class="uk-icon-mobile"></i> Mobile Money</label><br>';
		html += '<input type="radio" id="payment-option-pp" name="payment-options" value="pp"> <label for="payment-option-pp" class="x-radio"><i class="uk-icon-paypal"></i> Paypal</label> ';
		html += '</div>'; //c

		html += '<div class="payment-option card uk-margin-top">'; //d
		html += '<div class="uk-form-row uk-margin-top">'; //c
		html += '<label class="uk-form-label x-fw-400" for="payment-card-names">Card Holder</label>';
		html += '<div class="uk-form-controls">';
		html += '<input id="payment-card-names" type="text" placeholder="Card Holder Names" class="uk-width-1-1">';
		html += '</div>';
		html += '</div>'; //c
		html += '<div class="uk-form-row uk-margin-top">'; //c
		html += '<label class="uk-form-label x-fw-400" for="payment-card-number">Card Number <span id="payment-card-type" class="x-fw-700"></span></label>';
		html += '<div class="uk-form-controls">';
		html += '<div class="uk-form-icon uk-width-1-1"><i class="uk-icon-credit-card"></i><input id="payment-card-number" type="text" placeholder="Card Number" class="uk-width-1-1" onkeypress="onKeyPressNumber(event, true, true)"></div>';
		html += '</div>';
		html += '</div>'; //c
		html += '<div class="uk-grid uk-grid-small" data-uk-grid-margin>'; //g2
		html += '<div class="uk-width-1-2">'; //g21
		html += '<div class="uk-form-row uk-margin-top">'; //c
		html += '<label class="uk-form-label x-fw-400" for="payment-card-exp">Card Expiration</label>';
		html += '<div class="uk-form-controls">';
		html += '<div class="uk-form-icon uk-width-1-1"><i class="uk-icon-clock-o"></i><input id="payment-card-exp" type="text" placeholder="MM/YY" class="uk-width-1-1"></div>';
		html += '</div>';
		html += '</div>'; //c
		html += '</div>'; //g21
		html += '<div class="uk-width-1-2">'; //g22
		html += '<div class="uk-form-row uk-margin-top">'; //c
		html += '<label class="uk-form-label x-fw-400" for="payment-card-cvc">Card CV Code <span data-uk-tooltip="{pos:\'right\'}" title="This is a 3 digit number on the back of your card"><i class="uk-icon-question-circle"></i></span></label>';
		html += '<div class="uk-form-controls">';
		html += '<div class="uk-form-icon uk-width-1-1"><i class="uk-icon-clock-o"></i><input id="payment-card-cvc" type="text" placeholder="CV Code" class="uk-width-1-1"></div>';
		html += '</div>';
		html += '</div>'; //c
		html += '</div>'; //g22
		html += '</div>'; //g2
		html += '</div>'; //d

		html += '<div class="payment-option missing uk-margin-top x-hidden">'; //d
		html += '<h1><i class="uk-icon-money"></i></h1>';
		html += '<p>This payment option is not available at the moment. Select another option or check again later.</p>';
		html += '</div>'; //d
		html += '</form>'; //f
		html += '</div>'; //g12
		html += '</div>'; //g1
		html += '</div>'; //2
		html += '</div>'; //1
		html += '<div class="uk-modal-footer uk-text-right">'; //f
		html += '<button type="button" class="uk-button uk-modal-close">Cancel</button> ';
		html += '<button type="button" class="uk-button uk-button-success x-proceed-payment"><i class="uk-icon-lock"></i> Proceed Payment</button> ';
		html += '</div>'; //f
		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			let payment_option = 'card';
			let $payment_options = $modal.find('input[name="payment-options"]');
			let $payment_option_card = $modal.find('.payment-option.card');
			let $payment_option_missing = $modal.find('.payment-option.missing');
			$payment_options.on('change', function(){
				$modal.find('.payment-option').addClass('x-hidden');
				let val = str($payment_options.groupVal(), '', true);
				if (val === 'card') $payment_option_card.removeClass('x-hidden');
				else $payment_option_missing.removeClass('x-hidden');
				payment_option = val;
			});
			let $proceed_payment = $modal.find('.x-proceed-payment');
			let $card_names = $modal.find('#payment-card-names');
			let $card_number = $modal.find('#payment-card-number');
			let $card_type = $modal.find('#payment-card-type');
			let $card_exp = $modal.find('#payment-card-exp');
			let $card_cvc = $modal.find('#payment-card-cvc');
			let card_types = [
				{
					mask: '0000 0000 0000 0000',
					regex: '^(?:6011|65\\d{0,2}|64[4-9]\\d?)\\d{0,12}',
					cardtype: 'discover'
				},
				{
					mask: '0000 000000 0000',
					regex: '^3(?:0([0-5]|9)|[689]\\d?)\\d{0,11}',
					cardtype: 'diners'
				},
				{
			    	mask: '0000-0000-0000-0000',
			    	regex: '^(5019|4175|4571)\\d{0,12}',
			    	cardtype: 'dankort'
				},
				{
			    	mask: '0000-0000-0000-0000',
			    	regex: '^63[7-9]\\d{0,13}',
			    	cardtype: 'instapayment'
				},
				{
					mask: '0000 000000 00000',
					regex: '^(?:2131|1800)\\d{0,11}',
					cardtype: 'jcb15'
				},
				{
					mask: '0000 0000 0000 0000',
					regex: '^(?:35\\d{0,2})\\d{0,12}',
					cardtype: 'jcb'
				},
				{
					mask: '0000 0000 0000 0000',
					regex: '^(?:5[0678]\\d{0,2}|6304|67\\d{0,2})\\d{0,12}',
					cardtype: 'maestro'
				},
				{
				    mask: '0000-0000-0000-0000',
				    regex: '^220[0-4]\\d{0,12}',
				    cardtype: 'mir'
				},
				{
					mask: '0000 0000 0000 0000',
					regex: '^62\\d{0,14}',
					cardtype: 'unionpay'
				},
				{
					mask: '0000 000000 00000',
					regex: '^3[47]\\d{0,13}',
					cardtype: 'american express'
				},
				{
					mask: '0000 0000 0000 0000',
					regex: '^(5[1-5]\\d{0,2}|22[2-9]\\d{0,1}|2[3-7]\\d{0,2})\\d{0,12}',
					cardtype: 'mastercard'
				},
				{
					mask: '0000 0000 0000 0000',
					regex: '^4\\d{0,15}',
					cardtype: 'visa'
				},
			];
			let cardType = (val) => {
				val = str(val, '', true).replace(/[^0-9]/g, '').trim();
				let val_type = null;
				for (let i = 0; i < card_types.length; i ++){
					let type = card_types[i];
					let type_mask = type.mask.replace(/[^0-9]/g, '').trim();
					let type_name = camelCase(type.cardtype, /\s+/);
					let type_regex = new RegExp(type.regex);
					if (val.length > type_mask.length) continue;
					let val_mask = val + type_mask.substring(val.length);
					if (val_mask.match(type_regex)) val_type = type_name;
				}
				return val_type;
			};
			$card_number.on('input', function(){
				let val = str($(this).val(), '', true);
				if (val.length){
					let type = cardType(val);
					if (type && type.length) $card_type.html('~ ' + type);
					else $card_type.html('~ Unknown');
				}
				else $card_type.html('');
			});
			$proceed_payment.click(function(event){
				if (payment_option === 'card'){
					let card_names = str($card_names.val(), '', true);
					let card_number = str($card_number.val(), '', true);
					let card_exp = str($card_exp.val(), '', true);
					let card_cvc = str($card_cvc.val(), '', true);
					let pass = true, $err_input = null;
					if (!card_names.length){
						$card_names.inputError('Kindly enter the card names');
						if (!$err_input) $err_input = $card_names;
						pass = false;
					}
					if (!card_number.length){
						$card_number.inputError('Kindly enter the card number');
						if (!$err_input) $err_input = $card_number;
						pass = false;
					}
					if (!cardType(card_number)){
						$card_number.inputError('Unsupported card number');
						if (!$err_input) $err_input = $card_number;
						pass = false;
					}
					let exp_match = card_exp.match(/^([0-9]{2})\/([0-9]{2})$/), exp_month, exp_year;
					if (!(card_exp.length === 5 && exp_match.length === 3)){
						$card_exp.inputError('Invalid card expiration');
						if (!$err_input) $err_input = $card_exp;
						pass = false;
					}
					else {
						exp_month = parseInt(exp_match[1]);
						exp_year = parseInt(exp_match[2]);
						let this_month = (new Date()).getMonth() + 1;
						let this_year = parseInt(String((new Date()).getFullYear()).substr(2));
						if (!(exp_month >= 1 && exp_month <= 12)){
							$card_exp.inputError('Invalid expiration MM');
							if (!$err_input) $err_input = $card_exp;
							pass = false;
						}
						else if (!(exp_year >= this_year && exp_year <= (this_year + 10))){
							$card_exp.inputError('Invalid expiration YY');
							if (!$err_input) $err_input = $card_exp;
							pass = false;
						}
						else if (exp_year === this_year && exp_month <= this_month){
							$card_exp.inputError('Expired expiration');
							if (!$err_input) $err_input = $card_exp;
							pass = false;
						}
					}
					if (!(card_cvc.length === 3 && card_cvc.match(/^[0-9]+$/))){
						$card_cvc.inputError('Invalid security code');
						if (!$err_input) $err_input = $card_cvc;
						pass = false;
					}
					if (!pass){
						if ($err_input.length) $err_input.scrollToMe();
						return event.preventDefault();
					}
					let card = {
						names: card_names,
						number: card_number,
						exp_month: exp_month,
						exp_year: exp_year,
						cvc: card_cvc,
					};
					let prompt = '<h2>Proceed Payment</h2><p>Are you sure you want to process payment of <strong>KES ' + pricing_price.commas(2) + '</strong> using your card?</p>';
					UKPromptConfirm(prompt, () => {
						let post_url = '';
						let post_data = objectParams({
							order_payment: order.id,
							amount: pricing_price,
							option: 'card',
							data: btoa(stringify(card)),
						});
						serverPOST(post_url, post_data, loading_block).then(() => {
							uk_modal.hide();
							loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
							window.location.reload();
						}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
					}, null, 'Continue Payment', 'uk-button-success');
				}
				else {
					UKPrompt('<p class="uk-text-warning">Kindly select a valid payment option.</p>');
					return event.preventDefault();
				}
			});
		}, null, true, false, false, false);
	};
	$('.x-order-checkout').click(function(event){
		checkoutPrompt(() => {});
	});
});
