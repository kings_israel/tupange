//event data
if (!(window.hasOwnProperty("EVENT_DATA") && isObj(EVENT_DATA, true))) window["EVENT_DATA"] = {};

//gifts
$(function(){
	//controls
	let placeholder_image = ROOT_URL + '/assets/img/placeholder.jpg';
	let $stats_count = $('.x-stats-count');
	let $stats_value = $('.x-stats-value');
	let $table_body = $('.uk-datatable .x-table table tbody');
	let $toolbar_print = $('.x-toolbar-print');
	let $toolbar_download = $('.x-toolbar-download');
	let $gifts_add_wrapper = $('.x-edit-gift-add');
	let $gifts_add = $gifts_add_wrapper.find('button');
	let $gift_form = $('#gift_form');
	let $gift_cancel_mini = $('#gift_cancel_mini');
	let $gift_save_mini = $('#gift_save_mini');
	let $gift_image_data = $('#gift_image_data');
	let $gift_image_display = $('#gift_image_display');
	let $gift_image_btn = $('#gift_image_btn');
	let $gift_image_file = $('#gift_image_file');
	let $gift_image_remove = $('#gift_image_remove');
	let $gift_id = $('#gift_id');
	let $gift_title = $('#gift_title');
	let $gift_description = $('#gift_description');
	let $gift_value = $('#gift_value');
	let $gift_received_date = $('#gift_received_date');
	let $gift_received_by = $('#gift_received_by');
	let $gift_received_from = $('#gift_received_from');
	let $gift_phone = $('#gift_phone');
	let $gift_email = $('#gift_email');
	let $gift_notes = $('#gift_notes');
	let $gift_edit_cancel = $('#gift_edit_cancel');
	let $gift_edit_save = $('#gift_edit_save');
	let $gift_edit_saving = $('#gift_edit_saving');

	//helpers
	let gift_row_html = (data) => {
		if (!hasKeys(data, 'id', 'title', 'description', 'value', 'received_date', 'received_by', 'received_from', 'phone', 'email', 'notes', 'image', 'timestamp')) throw 'Invalid gift object';
		let item_search = [data.title, data.description, data.value, data.received_by, data.received_date, data.received_from, data.phone, data.email, data.notes].join(' ');
		let item_base64 = btoa(stringify(data)), item_image;
		let html = '';
		html += '<tr data-id="' + data.id + '" data-value="' + data.value + '" data-search="' + item_search + '" data-base64="' + item_base64 + '">';
		html += '<td class="x-nowrap uk-datatable-row-index" data-value=""></td>';
		html += '<td class="x-nowrap x-min-150">' + ((item_image = data.image.trim()).length ? '<img draggable="false" src="' + item_image + '" class="x-service-image" />' : '') + '</td>';
		html += '<td class="x-nowrap" data-value="' + data.title + '">' + data.title + '</td>';
		html += '<td class="x-min-150" data-value="' + data.description + '">' + data.description + '</td>';
		html += '<td class="x-nowrap" data-value="' + data.value + '">' + data.value.commas(2) + '</td>';
		html += '<td class="x-nowrap" data-value="' + data.received_by + '">' + data.received_by + '</td>';
		html += '<td class="x-nowrap" data-value="' + data.received_date + '">' + data.received_date + '</td>';
		html += '<td class="x-nowrap" data-value="' + data.received_from + '">' + data.received_from + '</td>';
		html += '<td class="x-nowrap" data-value="' + data.phone + '">' + data.phone + '</td>';
		html += '<td class="x-nowrap" data-value="' + data.email + '">' + data.email + '</td>';
		html += '<td class="x-min-200" data-value="' + data.notes + '">' + data.notes + '</td>';
		html += '<td class="x-nowrap" data-value="' + data.timestamp + '">' + data.timestamp + '</td>';
		html += '<td class="x-nowrap uk-text-center">';
		html += '<a title="Edit" draggable="false" href="javascript:" data-id="' + data.id + '" class="gift-edit uk-button uk-button-small uk-button-primary"><i class="uk-icon-pencil"></i></a> ';
		html += '<a title="Delete" draggable="false" href="javascript:" data-id="' + data.id + '" class="gift-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a> ';
		html += '</td>';
		html += '</tr>';
		return html;
	};
	let uk_datatable_update = () => {
		//refresh stats
		let gifts_count = uk_datatable_rows.length;
		let gifts_value = 0;
		for (let i = 0; i < gifts_count; i ++){
			let $row = $(uk_datatable_rows[i]);
			let value = num($row.attr('data-value'));
			gifts_value += value;
		}
		$stats_count.html(String(gifts_count));
		$stats_value.html(String(gifts_value));

		//refresh table
		uk_datatable_refresh();
	};
	let uk_datatable_edit = (data) => {
		let row_html = gift_row_html(data);
		let item_id = str(data.id, '', true);
		let $new_row = $(row_html);
		let updated = false;
		if (uk_datatable_rows.length){
			for (let i = 0; i < uk_datatable_rows.length; i ++){
				let $row = $(uk_datatable_rows[i]);
				if (str($row.attr('data-id'), '', true) == item_id){
					uk_datatable_rows[i] = $new_row[0];
					updated = true;
					break;
				}
			}
		}
		if (!updated) uk_datatable_rows.unshift($new_row[0]);
		uk_datatable_update();
		let $table_row = $table_body.find('tr[data-id="' + item_id + '"]');
		if ($table_row.length){
			$table_row.replaceWith($new_row);
			$new_row.flashGreen(1, 200);
		}
	};
	let uk_datatable_remove = (id) => {
		let updated = false;
		if (uk_datatable_rows.length){
			for (let i = 0; i < uk_datatable_rows.length; i ++){
				let $row = $(uk_datatable_rows[i]);
				if (str($row.attr('data-id'), '', true) == id){
					uk_datatable_rows.splice(i, 1);
					updated = true;
					break;
				}
			}
		}
		if (updated) uk_datatable_update();
	};
	let print_list = (checked_only) => {
		let loading_modal = null;
		let loading_callback = (toggle_loading) => {
			if (toggle_loading){
				if (!loading_modal){
					let modal_options = {keyboard: false, bgclose: false, center: true, modal: false};
					loading_modal = UIkit.modal.blockUI('<i class="uk-icon-spinner uk-icon-spin"></i> Preparing document. Please wait...', modal_options);
				}
				loading_modal.show();
			}
			else if (loading_modal) loading_modal.hide();
		};
		let test_callback_row = ($row) => {
			if (checked_only){
				let $checkbox = $row.find(".x-bulk-action");
				if (!($checkbox.length && $checkbox.isChecked())) return false;
			}
			return true;
		};
		let options = {
			title: EVENT_DATA.name + ' - Gifts',
			subtitle: EVENT_DATA.display_time + " | Showing ROWS_COUNT record(s)",
			logo: ROOT_URL + "/assets/img/logo.png",
			table_options: {
				aligns_row: {0: "C"},
		    }
		};
		uk_datatable_print(options, loading_callback, null, test_callback_row).then(() => console.debug("Print request successful!"), (err) => UKPrompt(err));
	};
	let download_list = (checked_only) => {
		let loading_modal = null;
		let loading_callback = (toggle_loading) => {
			if (toggle_loading){
				if (!loading_modal){
					let modal_options = {keyboard: false, bgclose: false, center: true, modal: false};
					loading_modal = UIkit.modal.blockUI('<i class="uk-icon-spinner uk-icon-spin"></i> Preparing document. Please wait...', modal_options);
				}
				loading_modal.show();
			}
			else if (loading_modal) loading_modal.hide();
		};
		let test_callback_row = ($row) => {
			if (checked_only){
				let $checkbox = $row.find(".x-bulk-action");
				if (!($checkbox.length && $checkbox.isChecked())) return false;
			}
			return true;
		};
		uk_datatable_download(EVENT_DATA.name + ' - Gifts', loading_callback, null, test_callback_row).then(() => console.debug("Download request successful!"), (err) => UKPrompt(err));
	};
	let gift_form_clear = () => {
		$gift_image_data.val('');
		$gift_image_display.attr('src', placeholder_image);
		$gift_image_file[0].value = '';
		$gift_id.val('');
		$gift_title.val('');
		$gift_description.val('');
		$gift_value.val('');
		$gift_received_date.val('');
		$gift_received_by.val('');
		$gift_received_from.val('');
		$gift_phone.val('');
		$gift_email.val('');
		$gift_notes.val('');
	};
	let gift_form_toggle = (show) => {
		gift_form_clear();
		if (show){
			$gifts_add_wrapper.addClass('x-hidden');
			$gift_form.removeClass('x-hidden');
			$gift_form.scrollToMe();
			$gift_form.flashGreen(1, 200);
		}
		else {
			$gift_form.addClass('x-hidden');
			$gifts_add_wrapper.removeClass('x-hidden');
		}
	};
	let gift_form_submit = (event) => {
		let image_data = $gift_image_data.val() == '1' ? str($gift_image_display.attr('src'), '', true) : '';
		if (image_data == placeholder_image) image_data = '';
		let id = str($gift_id.val(), '', true);
		let title = str($gift_title.val(), '', true);
		let description = str($gift_description.val(), '', true);
		let value = num($gift_value.val());
		let received_date = str($gift_received_date.val(), '', true);
		let received_by = str($gift_received_by.val(), '', true);
		let received_from = str($gift_received_from.val(), '', true);
		let phone = str($gift_phone.val(), '', true);
		let email = str($gift_email.val(), '', true);
		let notes = str($gift_notes.val(), '', true);
		if (!title.length){
			$gift_title.inputError('Kindly provide the gift title', true);
			return event.preventDefault();
		}
		let post_url = '';
		let post_data = objectParams({
			edit_gift: id,
			title: title,
			description: description,
			value: value,
			received_date: received_date,
			received_by: received_by,
			received_from: received_from,
			phone: phone,
			email: email,
			notes: notes,
			image: image_data,
		});
		let loading_callback = (toggle) => {
			if (toggle){
				$gift_form.addClass('x-block');
				$gift_cancel_mini.addClass('x-block');
				$gift_save_mini.addClass('x-block');
				$gift_edit_save.addClass('x-hidden');
				$gift_edit_saving.removeClass('x-hidden');
			}
			else {
				$gift_form.removeClass('x-block');
				$gift_cancel_mini.removeClass('x-block');
				$gift_save_mini.removeClass('x-block');
				$gift_edit_save.removeClass('x-hidden');
				$gift_edit_saving.addClass('x-hidden');
			}
		};
		serverPOST(post_url, post_data, loading_callback).then((res) => {
			notify(res.message, 'success');
			gift_form_toggle();
			uk_datatable_edit(res.data);
		}, (err) => {
			UKPrompt('<p class="uk-text-danger">' + err + '</p>');
			console.error(SERVER_RESPONSE);
		});
	};
	let file_base64 = (file) => {
		return fileBase64(file);
	};
	let base64_image_resize = (src, max_width, max_height) => {
		return base64ImageResize(src, max_width, max_height);
	};

	//listeners
	$toolbar_print.click(() => print_list());
	$toolbar_download.click(() => download_list());
	$gifts_add.click(() => gift_form_toggle(true));
	$gift_edit_cancel.click(() => gift_form_toggle());
	$gift_cancel_mini.click(() => gift_form_toggle());
	$gift_edit_save.click((event) => gift_form_submit(event));
	$gift_save_mini.click((event) => gift_form_submit(event));
	$gift_image_file.change(function(event){
		let files = event.target.files;
		if (files && files.length){
			file_base64(files[0]).then((base64) => {
				base64_image_resize(base64, 400, 400).then((resized_base64) => {
					$gift_image_data.val('1');
					$gift_image_display.attr('src', resized_base64);
					$gift_image_file[0].value = '';
					$gift_image_remove.removeClass('x-hidden');
					$gift_image_btn.html('Change Image');
				}, (err) => console.error(err));
			}, (err) => console.error(err));
		}
	});
	$gift_image_remove.click(function(){
		$gift_image_data.val('');
		$gift_image_display.attr('src', placeholder_image);
		$gift_image_file[0].value = '';
		$gift_image_remove.addClass('x-hidden');
		$gift_image_btn.html('Select Image');
	});
	$gift_image_display.click(() => $gift_image_file.click());
	$(document.body).on("click", "td a.gift-edit", function(event){
		let item_id = str($(this).attr('data-id'), '', true);
		let $row = $table_body.find('tr[data-id="' + item_id + '"]');
		if (!$row.length){
			console.error('Unable to find row data!', $row, item_id);
			return event.preventDefault();
		}
		let data_base64 = str($row.attr('data-base64'), '', true);
		let data = jsonParse(atob(data_base64), false);
		if (data === false){
			console.error('Invalid row gift data!', data_base64, data);
			return event.preventDefault();
		}
		gift_form_toggle(true);
		let tmp_date = '';
		if (data.received_date.trim().length){
			let tmp_date_parts = data.received_date.trim().split('-'); //2019-01-31 --> [2019, 01, 31]
			tmp_date = tmp_date_parts.length == 3 ? tmp_date_parts[2] + '/' + tmp_date_parts[1] + '/' + tmp_date_parts[0] : ''; //31/01/2019
		}
		let tmp_image = data.image.trim();
		if (tmp_image.length){
			$gift_image_file[0].value = '';
			$gift_image_data.val('1');
			$gift_image_display.attr('src', tmp_image);
			$gift_image_btn.html('Change Image');
		}
		$gift_id.val(data.id);
		$gift_title.val(data.title);
		$gift_description.val(data.description);
		$gift_value.val(String(data.value));
		$gift_received_date.val(tmp_date);
		$gift_received_by.val(data.received_by);
		$gift_received_from.val(data.received_from);
		$gift_phone.val(data.phone);
		$gift_email.val(data.email);
		$gift_notes.val(data.notes);
	});
	$(document.body).on("click", "td a.gift-delete", function(event){
		let item_id = str($(this).attr('data-id'), '', true);
		let $row = $table_body.find('tr[data-id="' + item_id + '"]');
		if (!$row.length){
			console.error('Unable to find row data!', $row, item_id);
			return event.preventDefault();
		}
		let data_base64 = str($row.attr('data-base64'), '', true);
		let data = jsonParse(atob(data_base64), false);
		if (data === false){
			console.error('Invalid row gift data!', data_base64, data);
			return event.preventDefault();
		}
		let prompt = '<p class="uk-text-danger">Are you sure you want to <strong>permanently delete</strong> gift entry "' + data.title + '"?</p>';
		UKPromptConfirm(prompt, function(){
			let post_url = '';
			let post_data = 'delete_gift=' + encodeURIComponent(item_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				notify(res.message, 'success');
				$('.x-table').scrollLeft(0);
				$row.remove();
				uk_datatable_remove(item_id);
			}, (err) => {
				UKPrompt('<p class="uk-text-danger">' + err + '</p>');
				console.error(SERVER_RESPONSE);
			});
		}, null, 'Delete Gift', 'uk-button-danger');
	});


});
