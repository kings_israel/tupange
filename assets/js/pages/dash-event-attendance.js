$(function(){
	//event data
	if (!(window.hasOwnProperty("EVENT_DATA") && isObj(EVENT_DATA, true))) window["EVENT_DATA"] = {};
	let $toolbar_print = $('.x-toolbar-print');
	let $toolbar_download = $('.x-toolbar-download');
	let $toolbar_search = $('.x-toolbar-search');
	let $table_body = $('.x-table table tbody');
	let $stats_all = $('.x-stats-all');
	let $stats_invite = $('.x-stats-invite');
	let $stats_ticket = $('.x-stats-ticket');

	//uk datatable update
	let attendance_row_html = (data) => {
		if (!hasKeys(data, 'id', 'barcode', 'type', 'direction', 'names', 'phone', 'email', 'timestamp')) throw 'Invalid attendance object';
		let item_search = [data.barcode, data.type, data.direction, data.names, data.phone, data.email].join(' ');
		let html = '';
		html += '<tr data-id="' + data.id + '" data-type="' + data.type + '" data-barcode="' + data.barcode + '" data-search="' + item_search + '" data-toggle="' + data.direction + '>">';
		html += '<td class="x-nowrap x-min-50 uk-datatable-row-index" data-value=""></td>';
		html += '<td class="x-nowrap x-min-100" data-value="' + data.barcode + '>">' + data.barcode + '</td>';
		html += '<td class="x-nowrap x-min-100" data-value="' + data.type + '">' + data.type + '</td>';
		html += '<td class="x-nowrap x-min-100" data-value="' + data.direction + '">CHECK ' + data.direction + '</td>';
		html += '<td class="x-nowrap x-min-100" data-value="' + data.names + '">' + data.names + '</td>';
		html += '<td class="x-nowrap x-min-100" data-value="' + data.phone + '">' + data.phone + '</td>';
		html += '<td class="x-nowrap x-min-100" data-value="' + data.email + '">' + data.email + '</td>';
		html += '<td class="x-nowrap x-min-100" data-value="' + data.timestamp + '">' + data.timestamp + '</td>';
		html += '<td class="x-nowrap uk-text-center">';
		html += '<a title="Delete" draggable="false" href="javascript:" data-id="' + data.id + '" data-names="' + data.names + '" class="attendance-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a> ';
		html += '</td>';
		html += '</tr>';
		return html;
	};
	let uk_datatable_update = () => {
		//refresh stats
		let item_invite = {}, item_ticket = {}, item_all = {};
		for (let i = 0; i < uk_datatable_rows.length; i ++){
			let $row = $(uk_datatable_rows[i]);
			let type = str($row.attr('data-type'), '', true).toUpperCase();
			let barcode = str($row.attr('data-barcode'), '', true);
			if (type == 'INVITE') item_invite[barcode] = 1;
			if (type == 'TICKET') item_ticket[barcode] = 1;
			item_all[barcode] = 1;
		}
		$stats_all.html(String(Object.keys(item_all).length));
		$stats_invite.html(String(Object.keys(item_invite).length));
		$stats_ticket.html(String(Object.keys(item_ticket).length));

		//refresh table
		uk_datatable_refresh();
	};
	let uk_datatable_edit = (data) => {
		let row_html = attendance_row_html(data);
		let item_id = str(data.id, '', true);
		let $new_row = $(row_html);
		let updated = false;
		if (uk_datatable_rows.length){
			for (let i = 0; i < uk_datatable_rows.length; i ++){
				let $row = $(uk_datatable_rows[i]);
				if (str($row.attr('data-id'), '', true) == item_id){
					uk_datatable_rows[i] = $new_row[0];
					updated = true;
					break;
				}
			}
		}
		if (!updated) uk_datatable_rows.unshift($new_row[0]);
		uk_datatable_update();
		let $table_row = $table_body.find('tr[data-id="' + item_id + '"]');
		if ($table_row.length){
			$table_row.replaceWith($new_row);
			$new_row.flashGreen(1, 200);
		}
	};
	let uk_datatable_remove = (id) => {
		let updated = false;
		if (uk_datatable_rows.length){
			for (let i = 0; i < uk_datatable_rows.length; i ++){
				let $row = $(uk_datatable_rows[i]);
				if (str($row.attr('data-id'), '', true) == id){
					uk_datatable_rows.splice(i, 1);
					updated = true;
					break;
				}
			}
		}
		if (updated) uk_datatable_update();
	};

	//code reader init
	let codeReader = new ZXing.BrowserMultiFormatReader();

	//search guest
	let search_modal_state = 0;
	let search_modal = () => {
		if (!search_modal_state) search_modal_state = 1;
		let html = '<a class="uk-modal-close uk-close"></a>';
		html += '<div class="uk-modal-header">Search Guest</div>';
		html += '<div class="uk-overflow-container">'; //1
			html += '<div class="x-search-input uk-form x-pad-20 ' + (search_modal_state != 1 ? 'x-hidden' : '') + '">';
				html += '<div class="uk-form-icon uk-width-1-1">';
					html += '<i class="uk-icon-search"></i>';
					html += '<input class="uk-width-1-1 x-search" type="text" placeholder="Search Guest Barcode, Names, Phone, Email">';
				html += '</div>';
			html += '</div>';
			html += '<div class="x-scan-input ' + (search_modal_state != 2 ? 'x-hidden' : '') + '">';
				html += '<video id="x-scan-video"></video>';
				html += '<div class="uk-text-center x-border-top x-pad-20">';
					html += '<p class="uk-text-danger x-scan-error x-hidden">Error scanning!</p>';
					html += '<button type="button" class="x-scan-start uk-button uk-button-primary x-min-100">Start Scanner</button> ';
					html += '<button type="button" class="x-scan-stop uk-button uk-button-white x-min-100 x-hidden">Stop Scanner</button> ';
				html += '</div>';
			html += '</div>';
			html += '<div class="x-search-results x-border-top x-hidden">';
				html += '<div class="uk-form x-guide">';
					html += '<p>Click on a result to <strong>Check In/Out</strong></p>'
				html += '</div>';
				html += '<ul class="x-border-top"></ul>';
			html += '</div>';
			html += '<div class="x-search-loading x-border-top x-hidden">';
				html += '<i class="uk-icon-spinner uk-icon-spin"></i>';
				html += '<p>Searching...</p>'
			html += '</div>';
			html += '<div class="x-search-nothing x-border-top x-hidden">';
				html += '<img draggable="false" src="' + ROOT_URL +  '/assets/img/dash/search-no.png" />';
				html += '<p>No results. Refine your search and try again</p>'
			html += '</div>';
		html += '</div>'; //1
		html += '<div class="uk-modal-footer uk-text-right">';
		html += '<button type="button" class="uk-button uk-modal-close">Close</button> ';
		html += '<button type="button" class="x-mode-scan uk-button uk-button-primary x-min-100 ' + (search_modal_state != 1 ? 'x-hidden' : '') + '"><i class="uk-icon-qrcode"></i> Scan</button> ';
		html += '<button type="button" class="x-mode-search uk-button uk-button-white x-min-100 ' + (search_modal_state != 2 ? 'x-hidden' : '') + '"><i class="uk-icon-search"></i> Search</button> ';
		html += '</div>';
		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			let $search_input = $modal.find('.x-search-input');
			let $search = $modal.find('.x-search');
			let $scan_input = $modal.find('.x-scan-input');
			let $scan_start = $modal.find('.x-scan-start');
			let $scan_stop = $modal.find('.x-scan-stop');
			let $scan_video = $modal.find('#x-scan-video');
			let $scan_error = $modal.find('.x-scan-error');
			let $search_results = $modal.find('.x-search-results');
			let $search_results_list = $modal.find('.x-search-results ul');
			let $search_loading = $modal.find('.x-search-loading');
			let $search_nothing = $modal.find('.x-search-nothing');
			let $mode_scan = $modal.find('.x-mode-scan');
			let $mode_search = $modal.find('.x-mode-search');

			let is_scanning = false;
			let results_toggle = (show) => {
				$search_results_list.html('');
				if (show) $search_results.removeClass('x-hidden');
				else $search_results.addClass('x-hidden');
			};
			let scan_error = (err) => {
				err = str(err, '', true);
				if (err.length){
					$scan_error.html(err);
					$scan_error.removeClass('x-hidden');
				}
				else {
					$scan_error.html('');
					$scan_error.addClass('x-hidden');
				}
			};
			let scan_toggle = (start) => {
				if (start){
					is_scanning = true;
					$scan_start.addClass('x-hidden');
					$scan_stop.removeClass('x-hidden');
					codeReader.decodeFromVideoDevice(undefined, 'x-scan-video', (result, err) => {
						if (result){
							$mode_search.click();
							$search.val(result);
							emitEvent($search[0], 'input', true);
						}
						if (err && !(err instanceof ZXing.NotFoundException)){
							console.error(err);
							scan_error(err);
						}
					});
				}
				else {
					is_scanning = false;
					$scan_start.removeClass('x-hidden');
					$scan_stop.addClass('x-hidden');
					codeReader.reset();
					scan_error();
				}
			};
			$mode_search.click(function(){
				if (is_scanning) scan_toggle();
				results_toggle();
				$scan_input.addClass('x-hidden');
				$mode_search.addClass('x-hidden');
				$mode_scan.removeClass('x-hidden');
				$search_input.removeClass('x-hidden');
				search_modal_state = 1;
			});
			$mode_scan.click(function(){
				if (is_scanning) scan_toggle();
				results_toggle();
				$search_loading.addClass('x-hidden');
				$search_nothing.addClass('x-hidden');
				$search_input.addClass('x-hidden');
				$mode_scan.addClass('x-hidden');
				$mode_scan.removeClass('x-hidden');
				$scan_input.removeClass('x-hidden');
				search_modal_state = 2;
			});
			$scan_start.click(() => scan_toggle(true));
			$scan_stop.click(() => scan_toggle());

			//helpers
			let search_query = (query, is_scan) => {
				let post_url = '';
				let post_data = 'search_query=' + encodeURIComponent(str(query, '', true)) + '&is_scan=' + (is_scan ? 1 : 0);
				let loading_callback = (toggle) => {
					if (toggle){
						$search_loading.removeClass('x-hidden');
						$search_nothing.addClass('x-hidden');
						$search_results.addClass('x-hidden');
					}
					else {
						$search_loading.addClass('x-hidden');
					}
				};
				results_toggle();
				serverPOST(post_url, post_data, loading_callback).then((res) => {
					let items = res.data;
					if (Array.isArray(items) && items.length){
						for (let i = 0; i < items.length; i ++){
							let item = items[i];
							let item_base64 = btoa(stringify(item));
							let html = '<li tabindex="0" data-base64="' + item_base64 + '">';
							html += '<div class="x-result-actions uk-align-right">';
							html += '<a href="javascript:" class="x-check-in uk-button uk-button-small uk-button-success">CHECK IN</a> ';
							html += '<a href="javascript:" class="x-check-out uk-button uk-button-small uk-button-primary">CHECK OUT</a> ';
							html += '</div>';
							html += '<div class="x-result-text">';
							html += '<h3>' + item.names + '</h3>';
							let item_contacts = [item.phone, item.email].join(' ').trim();
							html += '<p><strong>' + item.type + '</strong> [' + item.barcode + ']' + (item_contacts.length ? ' ' + item_contacts : '') + '</p>';
							html += '</div>';
							html += '</li>';
							$search_results_list.append($(html));
						}
						$search_results.removeClass('x-hidden');
					}
					else {
						$search_nothing.removeClass('x-hidden');
					}
				}, (err) => {
					UKPrompt('<p class="uk-text-danger">' + err + '</p>');
					console.error(SERVER_RESPONSE);
				});
			};

			//on search input
			let search_timeout;
			$search.on('input', function(event){
				window.clearTimeout(search_timeout);
				search_timeout = window.setTimeout(() => search_query($search.val()), 500);
			});

			//on item click
			$(document.body).on("click", ".x-search-results ul li a.x-check-in", function(event){
				let $btn = $(this);
				let $li = $btn.parent().parent();
				let result_data_base64 = str($li.attr('data-base64'), '', true);
				let data = jsonParse(atob(result_data_base64));
				let prompt = '<h2><i class="uk-icon-arrow-down"></i> Check In</h2><p>Are you sure you want to check-in guest: <strong>' + data.barcode + ' ' + data.names + '</strong>?</p>';
				UKPromptConfirm(prompt, function(){
					let post_url = '';
					let post_data = objectParams({
						guest_check: 'IN',
						names: data.names,
						email: data.email,
						phone: data.phone,
						type: data.type,
						barcode: data.barcode,
					});
					serverPOST(post_url, post_data, loading_block).then((res) => {
						console.log('on success', res);
						notify(res.message);
						uk_datatable_edit(res.data);
						$btn.removeClass('uk-button-success');
						$btn.addClass('uk-button-default uk-disabled');
						$btn[0].disabled = true;
						$btn.attr('disabled', 'disabled');
					}, (err) => {
						UKPrompt('<p class="uk-text-danger">' + err + '</p>');
						console.error(SERVER_RESPONSE);
					});
				}, null, 'Check In', 'uk-button-success');
			});
			$(document.body).on("click", ".x-search-results ul li a.x-check-out", function(event){
				let $btn = $(this);
				let $li = $btn.parent().parent();
				let result_data_base64 = str($li.attr('data-base64'), '', true);
				let data = jsonParse(atob(result_data_base64));
				let prompt = '<h2><i class="uk-icon-arrow-up"></i> Check Out</h2><p>Are you sure you want to check-out guest: <strong>' + data.barcode + ' ' + data.names + '</strong>?</p>';
				UKPromptConfirm(prompt, function(){
					let post_url = '';
					let post_data = objectParams({
						guest_check: 'OUT',
						names: data.names,
						email: data.email,
						phone: data.phone,
						type: data.type,
						barcode: data.barcode,
					});
					serverPOST(post_url, post_data, loading_block).then((res) => {
						console.log('on success', res);
						notify(res.message);
						uk_datatable_edit(res.data);
						$btn.removeClass('uk-button-primary');
						$btn.addClass('uk-button-default uk-disabled');
						$btn[0].disabled = true;
						$btn.attr('disabled', 'disabled');
					}, (err) => {
						UKPrompt('<p class="uk-text-danger">' + err + '</p>');
						console.error(SERVER_RESPONSE);
					});
				}, null, 'Check Out', 'uk-button-primary');
			});
		}, function(){
			//on hide
			codeReader.reset();
		}, null, false, false, true);
	};
	$toolbar_search.click(() => search_modal());

	//delete item
	$(document.body).on("click", "td a.attendance-delete", function(event){
		let item_id = str($(this).attr('data-id'), '', true);
		let guest_names = str($(this).attr('data-names'), '', true);
		let $row = $table_body.find('tr[data-id="' + item_id + '"]');
		if (!$row.length) return console.error('Failed to get row data', $row);
		let prompt = '<p class="uk-text-danger">Are you sure you want to <strong>permanently delete</strong> attendance entry for "' + guest_names + '".</p>';
		UKPromptConfirm(prompt, function(){
			let post_url = '';
			let post_data = 'delete_attendance_entry=' + encodeURIComponent(item_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				notify(res.message, 'success');
				$('.x-table').scrollLeft(0);
				$row.remove();
				uk_datatable_remove(item_id);
			}, (err) => {
				UKPrompt('<p class="uk-text-danger">' + err + '</p>');
				console.error(SERVER_RESPONSE);
			});
		}, null, 'Delete Entry', 'uk-button-danger');
	});

	//printing & download
	//on print list
	let print_list = (checked_only) => {
		let loading_modal = null;
		let loading_callback = (toggle_loading) => {
			if (toggle_loading){
				if (!loading_modal){
					let modal_options = {keyboard: false, bgclose: false, center: true, modal: false};
					loading_modal = UIkit.modal.blockUI('<i class="uk-icon-spinner uk-icon-spin"></i> Preparing document. Please wait...', modal_options);
				}
				loading_modal.show();
			}
			else if (loading_modal) loading_modal.hide();
		};
		let test_callback_row = ($row) => {
			if (checked_only){
				let $checkbox = $row.find(".x-bulk-action");
				if (!($checkbox.length && $checkbox.isChecked())) return false;
			}
			return true;
		};
		let options = {
			title: EVENT_DATA.name + ' - Attendance',
			subtitle: EVENT_DATA.display_time + " | Showing ROWS_COUNT record(s)",
			logo: ROOT_URL + "/assets/img/logo.png",
			table_options: {
				aligns_row: {0: "C"},
		    }
		};
		uk_datatable_print(options, loading_callback, null, test_callback_row).then(() => console.debug("Print request successful!"), (err) => UKPrompt(err));
	};

	//on download
	let download_list = (checked_only) => {
		let loading_modal = null;
		let loading_callback = (toggle_loading) => {
			if (toggle_loading){
				if (!loading_modal){
					let modal_options = {keyboard: false, bgclose: false, center: true, modal: false};
					loading_modal = UIkit.modal.blockUI('<i class="uk-icon-spinner uk-icon-spin"></i> Preparing document. Please wait...', modal_options);
				}
				loading_modal.show();
			}
			else if (loading_modal) loading_modal.hide();
		};
		let test_callback_row = ($row) => {
			if (checked_only){
				let $checkbox = $row.find(".x-bulk-action");
				if (!($checkbox.length && $checkbox.isChecked())) return false;
			}
			return true;
		};
		uk_datatable_download(EVENT_DATA.name + ' - Attendance', loading_callback, null, test_callback_row).then(() => console.debug("Download request successful!"), (err) => UKPrompt(err));
	};

	//listeners
	$toolbar_download.click(() => download_list());
	$toolbar_print.click(() => print_list());
});
