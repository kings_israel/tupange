$(function(){
	//controls
	$form_table = $('.x-form-table table');
	$form_table_body = $('.x-form-table table tbody');
	$loading = $('.x-loading');

	window.all_tasks = [];
	window.task_users = [];
	window.tusers = {};
	window.tag_users = [];
	window.current_tuser = null;

	let EVENT_ID = null;
	let STATUS_OPEN = 0;
	let STATUS_IN_PROGRESS = 1;
	let STATUS_COMPLETE = 2;
	let STATUS_CLOSED = 3;
	let DATE_FORMAT = "DD/MM/YYYY";
	let STATUSES = ['Open', 'In Progress', 'Complete', 'Closed'];
	let NOTIFY = ['Never', 'Daily', 'Weekly', 'Monthly'];

	let mask_img = ROOT_URL + '/assets/img/icons/save.png';

	Date.prototype.dateStamp = function(){
		if (!isDate(this)) return null;
		return this.getFullYear() + pad(this.getMonth() + 1) + pad(this.getDate());
	};
	Date.prototype.toDateString = function(){
		return moment(this).format(DATE_FORMAT);
	};

	let dateString = (val, return_date) => {
		if ("string" === typeof val && (val = val.trim()).length){
			let temp_date = moment(val, DATE_FORMAT).toDate();
			if (isDate(temp_date)){
				let temp_string = temp_date.toDateString();
				if (val == temp_string) return return_date ? temp_date : temp_string;
			}
		}
		else if (isDate(val)) return return_date ? val : val.toDateString();
		return null;
	};

	let fetch_users = function(){
		return new Promise((resolve, reject) => {
			let post_url = '';
			let post_data = 'fetch_task_users=1';
			serverPOST(post_url, post_data, null).then((res) => {
				if (res.hasOwnProperty('data') && Array.isArray(res.data)) resolve(res.data);
				else resolve([]);
			}, (err) => reject(err));
		});
	};
	let fetch_tasks = function(){
		return new Promise((resolve, reject) => {
			let post_url = '';
			let post_data = 'fetch_tasks=1';
			serverPOST(post_url, post_data, null).then((res) => {
				if (res.hasOwnProperty('data') && Array.isArray(res.data)) resolve(res.data);
				else resolve([]);
			}, (err) => reject(err));
		});
	};

	let get_tuser_names = (id, append_username, username_only) => {
		id = tstr(id);
		if (id === '') return '';
		if ('object' === typeof tusers && tusers && tusers.hasOwnProperty(id)){
			let tuser = tusers[id];
			if (username_only) return tuser.username;
			return append_username ? tuser.names + ' @' + tuser.username : tuser.names;
		}
		return 'User Not Found';
	};

	let item_template = (item) => {

		let item_id = tstr(item.id);
		let item_comments = item.hasOwnProperty('comments') && Array.isArray(item.comments) ? item.comments : [];
		let item_unread = item.hasOwnProperty('unseen') && num(item.unseen) ? num(item.unseen) : 0;

		let item_comments_count = 0;
		for (let i = 0; i < item_comments.length; i ++){
			if (item_comments[i].status) item_comments_count ++;
		}

		let btn_comments = '<button title="Comments" class="x-comments uk-button uk-button-small uk-button-white ' + (item_unread ? 'unread' : '') + '">';
		btn_comments += '<i class="uk-icon-comments"></i> Comments <strong>(' + item_comments_count + ')</strong>';
		if (item_unread) btn_comments += '<span class="badge" data-id="' + item_id + '"></span>';
		btn_comments += '</button> ';

		let btn_delete = '<button title="Delete" class="x-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></button> ';
		if (current_tuser.role !== 0) btn_delete = '';

		let html = '';
		if (item.status === STATUS_CLOSED){
			html += '<tr class="done" data-id="' + item_id + '">';
			html += '<td></td>';

			//col task
			html += '<td class="x-min-200"><span>' + item.task + '</span></td>';

			//col person
			html += '<td class="x-min-150"><span>' + get_tuser_names(item.person) + '</span></td>';

			//col notify
			if (current_tuser.role === 0) html += '<td class="col-notify"><span>' + NOTIFY[item.notify] + '</span></td>';
			else html += '<td class="col-notify"></td>';

			//col date
			html += '<td class="col-date ' + (item.diff < 0 && item.status < STATUS_COMPLETE ? 'late' : '') + '"><span>' + item.date + '</span></td>';

			//col status
			html += '<td class="col-status">';
			html += '<select class="input-select ' + (item.status === STATUS_CLOSED && current_tuser.role !== 0 ? 'uk-disabled" disabled ' : '"') + ' name="status">';
			for (let i = 0; i < STATUSES.length; i ++){
				let option_text = STATUSES[i];
				let option_selected = item.status === i ? 'selected="selected"' : '';
				let option_value = i;
				html += '<option value="' + option_value + '" ' + option_selected + '>' + option_text + '</option>';
			}
			html += '</select>';
			html += '</td>';

			//col actions
			html += '<td class="col-actions">';
			html += btn_comments;
			html += btn_delete;
			html += '</td>';

			html += '</tr>';
		}
		else {
			html += '<tr class="pending ' + (!item_id ? 'new' : '') + '" data-id="' + item_id + '">';

			//col #
			html += '<td></td>';

			//col task
			html += '<td class="x-min-200"><input class="input-text" type="text" placeholder="Task (i.e. Transport)" name="task" value="' + item.task + '"></td>';

			//col person
			html += '<td class="x-min-200">';
			html += '<select class="input-select" name="person">';
			html += '<option value="" ' + (item.person == '' ? 'selected="selected"' : '') + '>Assign Person (none)</option>';
			for (let i = 0; i < task_users.length; i ++){
				let user = task_users[i];
				let option_text = user.names + ' @' + user.username;
				let option_selected = user.id == item.person ? 'selected="selected"' : '';
				let option_value = user.id;
				if (SESSION_USER && hasKeys(SESSION_USER, 'uid') && user.uid == SESSION_USER['uid']) option_text += ' (you)';
				html += '<option value="' + option_value + '" ' + option_selected + '>' + option_text + '</option>';
			}
			html += '</select>';
			html += '</td>';

			//col person
			if (current_tuser.role === 0){
				//col person
				html += '<td class="col-notify x-min-150">';
				html += '<select class="input-select" name="notify">';
				for (let i = 0; i < NOTIFY.length; i ++){
					let option_text = NOTIFY[i];
					let option_selected = i === num(item.notify, 0) ? 'selected="selected"' : '';
					let option_value = i;
					html += '<option value="' + option_value + '" ' + option_selected + '>' + option_text + '</option>';
				}
				html += '</select>';
				html += '</td>';
			}
			else '<td class="col-notify"></td>';

			//col date
			html += '<td class="col-date ' + (item.diff < 0 && item.status < STATUS_COMPLETE ? 'late' : '') + '">';
			html += '<div class="input-date uk-form-icon uk-form-icon-flip">';
			html += '<i class="uk-icon-calendar"></i>';
			html += '<input type="text" placeholder="DD/MM/YYYY" data-uk-datepicker="{format:\'DD/MM/YYYY\',pos:\'bottom\'}" name="date" value="' + item.date + '">';
			html += '</div>';
			html += '</td>';

			//col status
			html += '<td class="col-status">';
			html += '<select class="input-select ' + (item.status === STATUS_CLOSED && current_tuser.role !== 0 ? 'uk-disabled" disabled ' : '"') + ' name="status">';
			for (let i = 0; i < STATUSES.length; i ++){
				if (current_tuser.role !== 0 && i === STATUS_CLOSED) continue;
				let option_text = STATUSES[i];
				let option_selected = item.status === i ? 'selected="selected"' : '';
				let option_value = i;
				html += '<option value="' + option_value + '" ' + option_selected + '>' + option_text + '</option>';
			}
			html += '</select>';
			html += '</td>';

			//col actions
			html += '<td class="col-actions">';

			html += '<button title="Save" class="x-save uk-button uk-button-small uk-button-success ' + (item_id ? 'x-hidden' : '') + '">';
			html += '<span draggable="false" class="x-mask-image" style="width:10px;height:10px;mask-image:url(' + mask_img + ');-webkit-mask-image:url(' + mask_img + ')"></span>';
			html += '</button> ';

			if (item_id != '') html += btn_comments;
			if (item_id != '') html += btn_delete;

			html += '</td>';

			html += '</tr>';
		}
		return html;
	};
	let auto_number = () => {
		let children = $form_table_body.children();
		for (let i = 0; i < children.length; i ++){
			let $child = $(children[i]);
			let id = str($child.attr('data-id'), '', true);
			$($child.children()[0]).html(!id ? '*' : String(i + 1));
		}
		if (children.length >= 7) $('.x-form-table table tfoot').removeClass('x-hidden');
		else $('.x-form-table table tfoot').addClass('x-hidden');
	};
	let add_new = () => {
		let item = {id: '', task: '', person: '', person_id: '', date: '', status: 0, user_id: '', unseen: 0, comments: [], timestamp: ''};
		let $item = $(item_template(item))
		$form_table_body.append($item);
		$item.find('[name="task"]').focus();
	};

	//listeners
	let get_item = (id) => {
		for (let i = 0; i < all_tasks.length; i ++){
			let item = all_tasks[i];
			if (item.id == id){
				item.__index = i;
				return item;
			}
		}
	};
	let has_changes = ($row, item) => {
		for (let key in item){
			if (item.hasOwnProperty(key)){
				let value = item[key];
				let $input = $row.find('[name="' + key + '"]');
				if ($input.length && key != 'status'){
					let val;
					if ($input.attr('type') == 'checkbox') val = $input.isChecked() ? STATUS_COMPLETE : STATUS_PENDING;
					else val = str($input.val(), '', true);
					if (value != val) return true;
				}
			}
		}
		return false;
	};
	let on_input = function($self, event){
		let $cell = $self.parent();
		if ($cell[0].nodeName.toLowerCase() != 'td') $cell = $cell.parent();
		let $row = $cell.parent();
		let name = str($self.attr('name'), '', true);
		let row_id = str($row.attr('data-id'), '', true);
		$cell.find('.uk-input-danger').removeClass('uk-input-danger');
		$cell.find('small.uk-text-danger').remove();
		if (row_id){
			let item = get_item(row_id);
			if (name == 'status'){
				let new_status = num($self.val(), -1);
				if (current_tuser.role !== 0 && new_status === STATUS_CLOSED){
					console.error('Action reserved for event manager');
					event.preventDefault();
					$self.val(String(item.status));
					return false;
				}
				let loading_callback = (toggle) => {
					if (toggle){
						$self.addClass('x-display-none');
						$cell.append('<i class="uk-icon-spinner uk-icon-spin"></i>');
					}
					else {
						$cell.find('.uk-icon-spinner').remove();
						$self.removeClass('x-display-none');
					}
				};
				let post_url = '';
				let post_data = 'set_status=' + new_status + '&task=' + row_id;
				serverPOST(post_url, post_data, loading_callback).then(() => {
					all_tasks[item.__index].status = new_status;
					let $item = $(item_template(all_tasks[item.__index]));
					$row.replaceWith($item);
					auto_number();
				}, (err) => {
					UKPrompt('<p class="uk-text-danger">' + err + '</p>');
					$self.val(String(item.status));
				});
			}
			else {
				if (has_changes($row, item)) $row.find('.x-save').removeClass('x-hidden');
				else $row.find('.x-save').addClass('x-hidden');
			}
		}
		return row_id;
	};
	let task_save = function($self, event){
		let $cell = $self.parent();
		if ($cell[0].nodeName.toLowerCase() != 'td') $cell = $cell.parent();
		let $row = $cell.parent();
		let row_id = str($row.attr('data-id'), '', true);
		let task, person, date, status, item;
		if (row_id){
			item = get_item(row_id);
			if (!item){
				console.error('Unable to get item: ' + row_id);
				return event.preventDefault();
			}
		}
		task = str($row.find('[name="task"]').val(), '', true);
		person = str($row.find('[name="person"]').val(), '', true);
		notify = num($row.find('[name="notify"]').val(), -1);
		date = str($row.find('[name="date"]').val(), '', true);
		status = num($row.find('[name="status"]').val(), -1);
		if (task == ''){
			let $task = $row.find('[name="task"]');
			if ($task.parent().find('.uk-text-danger').length) return event.preventDefault();
			$task.parent().append('<small class="uk-text-danger">Kindly enter the task</small>');
			$task.addClass('uk-input-danger');
			return event.preventDefault();
		}
		if (date != ''){
			let date_string = dateString(date);
			if (!date_string){
				let $date = $row.find('[name="date"]');
				if ($date.parent().parent().find('.uk-text-danger').length) return event.preventDefault();
				$date.parent().parent().append('<small class="uk-text-danger">Invalid date format</small>');
				$date.addClass('uk-input-danger');
				$row.find('[name="task"]').focus();
				return event.preventDefault();
			}
		}
		let $btn_save = $row.find('button.x-save');
		let btn_save_html = $btn_save.html();
		let loading_callback = (toggle) => {
			if (toggle){
				$row.addClass('x-block');
				$btn_save.removeClass('uk-button-success');
				$btn_save.addClass('uk-button-default uk-disabled');
				$btn_save.html('<i class="uk-icon-spinner uk-icon-spin"></i>');
			}
			else {
				$row.removeClass('x-block');
				$btn_save.removeClass('uk-button-default uk-disabled');
				$btn_save.addClass('uk-button-success');
				$btn_save.html(btn_save_html);
			}
		};
		let save_data = {
			save_task: row_id,
			task: task,
			person: person,
			notify: notify,
			date: date,
			status: status,
		};
		let post_url = '';
		let post_data = objectParams(save_data);
		serverPOST(post_url, post_data, loading_callback).then((res) => {
			let data_item = res.data;
			let $item = $(item_template(data_item));
			if (item){
				all_tasks[item.__index] = data_item;
				$row.replaceWith($item);
			}
			else {
				all_tasks.push(data_item);
				$row.replaceWith($item);
				add_new();
			}
			auto_number();
		}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
	};
	$(document.body).on("input", "[name]", function(event){
		let $self = $(this);
		return on_input($self, event);
	});
	$(document.body).on("change", "[name='date'],[name='person'],[name='notify']", function(event){
		let $self = $(this);
		let row_id = on_input($self, event);
		if (row_id) return task_save($self, event);
	});
	$(document.body).on("click", "button.x-save", function(event){
		let $self = $(this);
		return task_save($self, event);
	});
	$(document.body).on('keypress', 'input.input-text', function(event){
		let $self = $(this);
		if (event.which == 13) return task_save($self, event);
	});
	$(document.body).on("click", "button.x-delete", function(event){
		if (current_tuser.role !== 0){
			console.error('Delete task action reserved for event managers only!');
			return event.preventDefault();
		}
		let $self = $(this);
		let $cell = $self.parent();
		if ($cell[0].nodeName.toLowerCase() != 'td') $cell = $cell.parent();
		let $row = $cell.parent();
		let row_id = str($row.attr('data-id'), '', true);
		if (!row_id){
			console.error('Unable to get row id for delete', $row);
			return event.preventDefault();
		}
		let item = get_item(row_id);
		if (!item){
			console.error('Unable to get item: ' + row_id);
			return event.preventDefault();
		}
		let prompt = '<p class="uk-text-danger">Do you want to delete the task <br>"' + item.task + '"?</p>';
		UKPromptConfirm(prompt, () => {
			let $btn_delete = $row.find('button.x-delete');
			let btn_delete_html = $btn_delete.html();
			let loading_callback = (toggle) => {
				if (toggle){
					$row.addClass('x-block');
					$btn_delete.removeClass('uk-button-white');
					$btn_delete.addClass('uk-button-default uk-disabled');
					$btn_delete.html('<i class="uk-icon-spinner uk-icon-spin"></i>');
				}
				else {
					$row.removeClass('x-block');
					$btn_delete.removeClass('uk-button-default uk-disabled');
					$btn_delete.addClass('uk-button-success');
					$btn_delete.html(btn_delete_html);
				}
			};
			let post_url = '';
			let post_data = 'delete_task=' + encodeURIComponent(row_id);
			serverPOST(post_url, post_data, loading_callback).then((res) => {
				$row.remove();
				auto_number();
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Delete Task', 'uk-button-danger');
	});

	//comment form
	let $tc_form = $('#tc-form');
	let $tc_comment = $('#tc-comment');
	let $tc_attachment = $('#tc-attachment');
	let $tc_attachment_close = $('#tc-attachment > a');
	let $tc_file = $('#tc-file');
	let $tc_attach = $('#tc-attach');
	let $tc_save = $('#tc-save');
	let $comments_list = $('#task_comments .comments ul');
	let $comments_nothing = $('#task_comments .nothing');
	let edit_comment_defaults = {file_name: '', file_data: '', task: null};
	let EDIT_COMMENT = edit_comment_defaults;

	//tc alert
	let $tc_alert = $('#tc-alert');
	let tc_alert = (msg) => {
		msg = tstr(msg);
		if (!msg){
			$tc_alert.find('.uk-close').click();
			return;
		}
		let html = '';
		html += '<div class="uk-alert uk-alert-warning" data-uk-alert>';
		html += '<a href="javascript:" class="uk-alert-close uk-close"></a>';
		html += '<p>' + msg + '</p>';
		html += '</div>';
		$tc_alert.html(html);
	};

	//comment template
	let comment_template = (item) => {
		let task = EDIT_COMMENT.item;
		let item_comment = tstr(item.comment);
		let html = '';
		html += '<li class="' + (!item.status ? 'deleted' : '') + '">';
		if (item.status && current_tuser.role === 0) html += '<a class="uk-align-right uk-text-danger x-comment-delete" href="javascript:" data-id="' + item.id + '"><i class="uk-icon-trash"></i></a>';
		if (!item.status && current_tuser.id === 'owner') html += '<a class="uk-align-right x-view-deleted" href="javascript:"><i class="uk-icon-eye"></i></a>';
		html += '<div class="x-tc-deleted">Comment deleted</div>';
		html += '<div class="x-tc-wrapper">';
		html += item_comment;
		if (item.file){
			let file_url = '?event-checklist=' + encodeURIComponent(EVENT_ID) + '&t=' + encodeURIComponent(task.id) + '&tcf=' + encodeURIComponent(item.id);
			if (item_comment !== '') html += '<br>';
			html += '<a href="' + file_url + '">' + item.file + '</a>';
		}
		html += '<small><strong>' + get_tuser_names(item.user_id, true) + '</strong> <i>' + item.timestamp + '</i></small>';
		html += '</div>';
		html += '</li>';
		return html;
	};

	//set item and comments
	let set_comments = (item) => {
		if (!hasKeys(item, 'id', 'task', 'person')){
			console.error('Invalid comments task item object!');
			return false;
		}

		EDIT_COMMENT = edit_comment_defaults;
		EDIT_COMMENT.item = item;

		$('#tc-task').html(item.task);
		let $tc_person = $('#tc-person');
		if (tstr(item.person) != ''){
			$tc_person.removeClass('x-display-none');
			$tc_person.find('span').html(get_tuser_names(item.person, true));
		}
		else {
			$tc_person.addClass('x-display-none');
			$tc_person.find('span').html('');
		}
		let $tc_date = $('#tc-date');
		if (tstr(item.date) != ''){
			$tc_date.removeClass('x-display-none');
			$tc_date.find('span').html(item.date);
		}
		else {
			$tc_date.addClass('x-display-none');
			$tc_date.find('span').html('');
		}
		let $tc_by = $('#tc-by');
		if (tstr(item.user_id) != ''){
			$tc_by.removeClass('x-display-none');
			$tc_by.html('By @' + get_tuser_names(item.user_id, 0, 1) + ' On ' + item.timestamp);
		}
		else {
			$tc_by.addClass('x-display-none');
			$tc_by.html('');
		}
		let $tc_as = $('#tc-as');
		$tc_as.html(current_tuser ? '@' + current_tuser.username : 'Error');
		let item_comments = item.hasOwnProperty('comments') && Array.isArray(item.comments) ? item.comments : [];
		if (!item_comments.length){
			$comments_list.html('');
			$comments_list.addClass('x-display-none');
			$comments_nothing.removeClass('x-display-none');
		}
		else {
			$comments_list.html('');
			$comments_list.removeClass('x-display-none');
			$comments_nothing.addClass('x-display-none');
			let $ic = null;
			for (let i = 0; i < item_comments.length; i ++){
				let ic = item_comments[i];
				$ic = $(comment_template(ic));
				$comments_list.append($ic);
			}
			if ($ic.length) $ic.scrollToMe();
		}
		if (item.status === STATUS_CLOSED){
			$tc_form.addClass('x-display-none');
			$comments_nothing.html('No comments available.<br>Task Closed.');
		}
		else {
			$tc_form.removeClass('x-display-none');
			$comments_nothing.html('No comments available.<br>Add a comment using the form below.');
		}

		$tc_comment.val('');
		$tc_attachment.find('span').html('');
		$tc_attachment.addClass('x-display-none');
		if ($tc_file.length) $tc_file[0].value = '';

		return true;
	};

	//update seen
	let update_task_seen = (id) => {
		let post_url = '';
		let post_data = 'task_update_seen=' + encodeURIComponent(id);
		serverPOST(post_url, post_data, null).then((res) => {
			if (res.data){
				$('.badge[data-id="' + id + '"]').remove();
			}
		}, (err) => console.error(err));
	};

	//init comment textarea & mentions
	window.mentions = null;
	let tc_comment_init = () => {
		$tc_comment.autogrow();
		mentions = new Mention({
			name: 'username',
			input: document.querySelector('#tc-comment'),
			//reverse: true,
			options: tag_users,
			template: function(option){
				return '@' + option.username + ' (' + option.names + ')';
			}
		});
	};

	//on comment textarea blur
	$(document.body).on("click", function(event){
		let $target = $(event.target);
		let is_child = $target[0].className.indexOf('mention-option') > -1 || $target.parents('[class*=mention-]').length;
		if (!is_child && mentions) mentions.toggleOptionList(false);
	});

	//attachment remove
	$tc_attachment_close.click(() => {
		$tc_attachment.find('span').html('');
		$tc_attachment.addClass('x-display-none');
		EDIT_COMMENT.file_name = '';
		EDIT_COMMENT.file_data = '';
	});

	//on attach
	$tc_attach.click(() => $tc_file.click());

	//on attachment file input change
	$tc_file.change(function(event){
		let files = event.target.files;
		if (files && files.length){
			let file = files[0];
			let file_name = file.name;
			let file_size = num(file.size / (1024 ** 2)).round(2);
			if (file_size > 5){
				tc_alert('File "' + file_name + '" is larger than the upload limit of 5MB. File size: ' + file_size + 'MB');
				return;
			}
			fileBase64(file).then((file_data) => {
				EDIT_COMMENT.file_name = file_name;
				EDIT_COMMENT.file_data = file_data;
				let fs = file.size < (1024 ** 2) ? num(file.size / 1024).round(2) + 'KB' : num(file.size / (1024 ** 2)).round(2) + 'MB';
				$tc_attachment.find('span').html(file_name + ' (' + fs + ')');
				$tc_attachment.removeClass('x-display-none');
				$tc_file[0].value = '';
				tc_alert();
			}, (err) => {
				$tc_file[0].value = '';
				tc_alert(err);
			});
		}
	});

	//on save
	$tc_save.click(function(event){
		if (!(mentions instanceof Mention)){
			console.error('Invalid comment mention object');
			return event.preventDefault();
		}
		if (!EDIT_COMMENT.item){
			console.error('Invalid comment task object');
			return event.preventDefault();
		}
		let item = EDIT_COMMENT.item;
		let comment = tstr($tc_comment.val());
		let file_data = EDIT_COMMENT.file_data;
		let file_name = EDIT_COMMENT.file_name;
		if (!comment.length && !(file_data.length && file_name.length)){
			tc_alert('Kindly add a comment or attachment to save');
			$tc_comment.one('input', () => tc_alert());
			return event.preventDefault();
		}
		let tags = mentions.collect();
		let tagged_track = {}, tagged = [];
		if (tags.length){
			for (let i = 0; i < tags.length; i ++){
				let tag_word = tags[i].word, tag_index = tags[i].index, tag_username = tag_word.replace('@', ''), tag_user;
				if (tagged_track.hasOwnProperty(tag_username)) tag_user = tagged_track[tag_username];
				else {
					let tag_users = mentions.options.filter((option) => option.username == tag_username);
					if (!tag_users.length){
						console.error('Invalid tag user item at index ' + i);
						return event.preventDefault();
					}
					tag_user = tag_users[0];
					tagged.push({id: tag_user.id, word: tag_word, indexes: []});
					tag_user.__index = tagged.length - 1;
					tagged_track[tag_username] = tag_user;
				}
				if (tagged[tag_user.__index].indexes.indexOf(tag_index) < 0) tagged[tag_user.__index].indexes.push(tag_index);
			}
		}
		let comment_data = {
			task_comment: item.id,
			comment: comment,
			file_data: file_data,
			file_name: file_name,
			tagged: Array.isArray(tagged) && tagged.length ? tagged : '',
		};
		let save_btn_html = $tc_save.html();
		let loading_callback = (toggle) => {
			if (toggle){
				$tc_form.addClass('x-block');
				$tc_save.html('<i class="uk-icon-spinner uk-icon-spin"></i> Saving...');
				$tc_save.removeClass('uk-button-primary').addClass('uk-button-default').toggleDisable(true);
			}
			else {
				$tc_form.removeClass('x-block');
				$tc_save.html(save_btn_html);
				$tc_save.toggleDisable(false).removeClass('uk-button-default').addClass('uk-button-primary');
			}
		};
		let post_url = '';
		let post_data = objectParams(comment_data);
		serverPOST(post_url, post_data, loading_callback).then((res) => {
			let comment = res.data;
			if (hasKeys(comment, 'id', 'seen')){
				$comments_list.removeClass('x-display-none');
				$comments_nothing.addClass('x-display-none');
				let $comment = $(comment_template(comment));
				$comments_list.append($comment);
				$comment.scrollToMe();

				if (!Array.isArray(item.comments)) item.comments = [];
				item.comments.push(comment);

				all_tasks[item.__index] = item;

				$('tr[data-id="' + item.id + '"]').replaceWith($(item_template(item)));
			}

			EDIT_COMMENT = edit_comment_defaults;
			EDIT_COMMENT.item = item;

			$tc_comment.val('');
			$tc_attachment.find('span').html('');
			$tc_attachment.addClass('x-display-none');
			if ($tc_file.length) $tc_file[0].value = '';

			tc_alert();

		}, (err) => tc_alert(err));
	});

	//open comments
	let view_comments = (item_id) => {
		let item = get_item(item_id);
		/*
		if (EDIT_COMMENT.item && EDIT_COMMENT.item.id === item.id){
			UIkit.offcanvas.show('#task_comments', {mode: 'slide'});
			return;
		}
		*/
		if (!set_comments(item)) return false;
		UIkit.offcanvas.show('#task_comments', {mode: 'slide'});
		setTimeout(() => tc_comment_init(), 200);
		setTimeout(() => update_task_seen(item_id), 200);
	};

	//on x-comments button click
	$(document.body).on("click", "button.x-comments", function(event){
		let $self = $(this);
		let $cell = $self.parent();
		if ($cell[0].nodeName.toLowerCase() != 'td') $cell = $cell.parent();
		let $row = $cell.parent();
		let row_id = str($row.attr('data-id'), '', true);
		if (!row_id){
			console.error('Unable to get row id to view comments', $row);
			return event.preventDefault();
		}
		view_comments(row_id);
	});

	//on comment view deleted
	$(document.body).on("click", ".x-view-deleted", function(event){
		if (current_tuser.role !== 0){
			console.error('View deleted task comment action is reserved for event managers only!');
			return event.preventDefault();
		}
		let $self = $(this);
		let $parent = $self.parent();
		if ($parent[0].className.indexOf('deleted') > -1){
			$self.html('<i class="uk-icon-eye-slash"></i>');
			$parent.removeClass('deleted');
		}
		else {
			$self.html('<i class="uk-icon-eye"></i>');
			$parent.addClass('deleted');
		}
	});

	//on comment delete
	$(document.body).on("click", ".x-comment-delete", function(event){
		if (current_tuser.role !== 0){
			console.error('Delete task comment action reserved for event managers only!');
			return event.preventDefault();
		}
		let $self = $(this);
		let $parent = $self.parent();
		let id = tstr($self.attr('data-id'));
		let item = EDIT_COMMENT.item;
		if (!hasKeys(item, 'id', 'comments')){
			console.error('Invalid edit comment item object');
			return event.preventDefault();
		}
		let comment = null;
		if (Array.isArray(item.comments) && item.comments.length){
			for (let i = 0; i < item.comments.length; i ++){
				if (item.comments[i].id === id){
					comment = item.comments[i];
					comment.__index = i;
					break;
				}
			}
		}
		if (!hasKeys(comment, 'id', 'comment')){
			console.error('Unable to find comment object: ' + id);
			return event.preventDefault();
		}
		let comment_prev = comment.comment.trim();
		if (comment_prev.length > 30) comment_prev = comment_prev.substr(0, 30).trim() + '...';
		let prompt = '<p class="uk-text-danger">Do you want to delete the comment <br>"' + comment_prev + '"?</p>';
		UKPromptConfirm(prompt, () => {
			let loading_callback = (toggle) => {
				if (toggle) $parent.addClass('x-block');
				else $parent.removeClass('x-block');
			};
			let post_url = '';
			let post_data = 'task_comment_delete=' + encodeURIComponent(comment.id) + '&task=' + encodeURIComponent(item.id);
			serverPOST(post_url, post_data, loading_callback).then((res) => {
				//update object
				comment.status = 0;
				item.comments[comment.__index] = comment;
				all_tasks[item.__index] = item;
				EDIT_COMMENT.item = item;

				//update comment html
				$comment = $(comment_template(comment));
				$parent.replaceWith($comment);

				//update item html
				let $row = $('tr[data-id="' + item.id + '"]');
				if ($row.length){
					let $item = $(item_template(item));
					$row.replaceWith($item);
				}
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Delete Comment', 'uk-button-danger');
	});

	//init
	let init = () => {
		//init url
		let data = getQueryParams({'event-checklist': false});
		EVENT_ID = data['event-checklist'];

		//init tasks
		let init_tasks = (callback) => {
			fetch_tasks().then((items) => {
				all_tasks = items;
				$loading.addClass('x-hidden');
				if (Array.isArray(all_tasks)){
					$form_table_body.html('');
					for (let i = 0; i < all_tasks.length; i ++){
						let item = all_tasks[i];
						let $item = $(item_template(item));
						$form_table_body.append($item);
					}
					add_new();
					auto_number();
				}
				if (isFunc(callback)) callback();
			}, (err) => {
				$loading.html('<p class="uk-text-danger">' + err + '</p>');
				if (isFunc(callback)) callback();
			});
		};

		//init event users
		let init_event_users = (callback) => {
			fetch_users().then((items) => {
				task_users = [];
				tusers = {};
				if (Array.isArray(items)){
					for (let i = 0; i < items.length; i ++){
						let tuser = items[i];
						tusers[tuser.id] = tuser;
						task_users.push(tuser);
						if (tuser.uid === SESSION_USER.uid) current_tuser = tuser;
						else tag_users.push(tuser);
					}
				}
				if (isFunc(callback)) callback();
			}, (err) => {
				console.error(err);
				if (isFunc(callback)) callback();
			});
		};

		//init query params
		let init_query_params = (callback) => {
			let params = {};
			params['task-assigned'] = true;
			let data = getQueryParams(params);
			if (hasKeys(data, 'task-assigned')){
				let row_id = data['task-assigned'];
				let $row = $('tr[data-id="' + row_id + '"]');
				if ($row.length){
					$row.scrollToMe();
					$row.flashGreen(1, 200);
				}
			}
			if (isFunc(callback)) callback();
		};

		//init
		init_event_users(() => {
			if (current_tuser.role !== 0){
				$('.col-notify').addClass('x-display-none');
			}
			init_tasks(() => init_query_params());
		});
	};

	//init script
	init();
});
