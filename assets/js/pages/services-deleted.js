$(function(){
	//handle on service restore
	$(document.body).on("click", ".x-service-restore", function(event){
		let service_id = str($(this).attr("data-id"), "", true);
		let service_title = str($(this).attr("data-title"), "", true);
		if (!service_id.length || !service_title.length) return event.preventDefault();
		if (!window.hasOwnProperty("UKPromptConfirmTest")) return event.preventDefault();
		let prompt = "<h2>Restore Service</h2><p>Are you sure you want to restore this service?</p>";
		UKPromptConfirmTest(prompt, service_title, function(value){
			let redirect = ROOT_URL + "/services?view=edit&s=" + service_id + "&restore=" + value;
			window.location.href = redirect;
		}, null, "Restore Service", "uk-button-primary");
	});

	//handle on service remove
	$(document.body).on("click", ".x-service-remove", function(event){
		let service_id = str($(this).attr("data-id"), "", true);
		let service_title = str($(this).attr("data-title"), "", true);
		if (!service_id.length || !service_title.length) return event.preventDefault();
		if (!window.hasOwnProperty("UKPromptConfirmTest")) return event.preventDefault();
		let prompt = '<h2 class="uk-text-danger">Delete Permanently</h2><p class="uk-text-danger">Are you sure you want to remove this service and all its related information? <strong>This action cannot be undone!</strong></p>';
		UKPromptConfirmTest(prompt, service_title, function(value){
			let redirect = ROOT_URL + "/services?view=edit&s=" + service_id + "&remove=" + value;
			window.location.href = redirect;
		}, null, "Delete Permanently", "uk-button-danger");
	});
});
