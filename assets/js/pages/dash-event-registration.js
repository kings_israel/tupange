$(function(){
	if (!(window.hasOwnProperty("EVENT_DATA") && isObj(EVENT_DATA, true))) window["EVENT_DATA"] = {};

	//event tickets
	let $toolbar_registration = $('.x-toolbar-registration');
	let $toolbar_tickets = $('.x-toolbar-tickets');
	let $toolbar_download = $('.x-toolbar-download');
	let $toolbar_print_list = $('.x-toolbar-print-list');
	let $toolbar_print_tickets = $('.x-toolbar-print-tickets');
	let $manage_guests = $('.x-manage-guests');
	let $manage_tickets = $('.x-manage-tickets');

	//toggle manage
	$toolbar_tickets.click(function(){
		$manage_guests.addClass('x-hidden');
		$toolbar_tickets.addClass('x-hidden');
		$manage_tickets.removeClass('x-hidden');
		$toolbar_registration.removeClass('x-hidden');
	});
	$toolbar_registration.click(function(){
		$manage_tickets.addClass('x-hidden');
		$toolbar_registration.addClass('x-hidden');
		$manage_guests.removeClass('x-hidden');
		$toolbar_tickets.removeClass('x-hidden');
	});

	let $tickets_table_body = $('.x-manage-tickets').find('.x-table table tbody');
	let $edit_ticket_toggle = $('.x-edit-ticket-toggle');
	let $edit_ticket_add = $edit_ticket_toggle.find('button');
	let $ticket_form = $('#ticket_form');
	$ticket_form.submit(event => event.preventDefault()); //ticket edit form disable submit

	//ticket edit input
	let $edit_ticket_id = $('#edit_ticket_id');
	let $edit_ticket_title = $('#edit_ticket_title');
	let $edit_ticket_amount = $('#edit_ticket_amount');
	let $edit_ticket_limit = $('#edit_ticket_limit');
	let $edit_ticket_description = $('#edit_ticket_description');

	//ticket edit actions
	let $edit_ticket_cancel = $('#ticket_cancel');
	let $edit_ticket_save = $('#ticket_save');
	let $edit_ticket_saving = $('#ticket_saving');
	let $ticket_cancel_mini = $('#ticket_cancel_mini');
	let $ticket_save_mini = $('#ticket_save_mini');

	//ticket edit helpers
	let edit_ticket_clear = () => {
		$edit_ticket_id.val('');
		$edit_ticket_title.val('');
		$edit_ticket_amount.val('');
		$edit_ticket_limit.val('');
		$edit_ticket_description.val('');
	};
	let edit_ticket_toggle = (show) => {
		edit_ticket_clear();
		if (show){
			$ticket_form.removeClass('x-hidden');
			$edit_ticket_toggle.addClass('x-hidden');
		}
		else {
			$ticket_form.addClass('x-hidden');
			$edit_ticket_toggle.removeClass('x-hidden');
		}
	};
	let ticket_row_html = (data) => {
		let html = '<tr data-id="' + data.id + '">';
		html += '<td class="x-nowrap x-ticket-index"></td>';
		html += '<td class="x-nowrap x-ticket-title x-min-100">' + data.title + '</td>';
		html += '<td class="x-nowrap x-ticket-price x-min-100"><small>KES</small> ' + data.price.commas(2) + '</td>';
		html += '<td class="x-nowrap x-ticket-limit x-min-100">' + data.guests_limit.commas(0) + '</td>';
		html += '<td class="x-min-200 x-ticket-description">' + data.description + '</td>';
		html += '<td class="x-nowrap x-ticket-timestamp x-min-100">' + data.timestamp + '</td>';
		html += '<td class="x-nowrap uk-text-center">';
		html += '<a title="Edit" draggable="false" href="javascript:" data-id="' + data.id + '" class="ticket-edit uk-button uk-button-small uk-button-primary"><i class="uk-icon-pencil"></i></a> ';
		html += '<a title="Delete" draggable="false" href="javascript:" data-id="' + data.id + '" data-title="' + data.title + '" class="ticket-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a> ';
		html += '</td>';
		html += '</tr>'
		return html;
	};
	let ticket_row_indexes = () => {
		$tickets_table_body.find('tr').each(function(index){
			$(this).find('.x-ticket-index').html(index + 1);
		});
	};
	let ticket_settings_update = () => {
		let $tickets_select = $('select#guest_ticket');
		let html = '<option value="" selected="selected">General Admission</option> ';
		for (let i = 0; i < TICKET_SETTINGS.length; i ++) html += '<option value="' + TICKET_SETTINGS[i].id + '">' + TICKET_SETTINGS[i].title + ' (' + TICKET_SETTINGS[i].price.commas(2) + '/=)</option> ';
		$tickets_select.html(html);
	};
	let ticket_settings_edit = (data) => {
		let updated = false;
		for (let i = 0; i < TICKET_SETTINGS.length; i ++){
			if (TICKET_SETTINGS[i].id == data.id){
				TICKET_SETTINGS[i] = data;
				updated = true;
				break;
			}
		}
		if (!updated) TICKET_SETTINGS.push(data);
		ticket_settings_update();
	};
	let ticket_settings_remove = (id) => {
		for (let i = 0; i < TICKET_SETTINGS.length; i ++){
			if (TICKET_SETTINGS[i].id == id){
				TICKET_SETTINGS.splice(i, 1);
				break;
			}
		}
		ticket_settings_update();
	};
	let edit_ticket_submit = function(event){
		let id = str($edit_ticket_id.val(), '', true);
		let title = str($edit_ticket_title.val(), '', true);
		let amount = num($edit_ticket_amount.val());
		let limit = num($edit_ticket_limit.val());
		let description = str($edit_ticket_description.val(), '', true);
		if (!title.length){
			$edit_ticket_title.inputError('Kindly enter the ticket title.', true);
			return event.preventDefault();
		}
		let loading_callback = (toggle) => {
			if (toggle){
				$ticket_form.addClass('x-block');
				$ticket_save_mini.addClass('x-block');
				$ticket_cancel_mini.addClass('x-block');
				$edit_ticket_save.addClass('x-hidden');
				$edit_ticket_saving.removeClass('x-hidden');
			}
			else {
				$ticket_form.removeClass('x-block');
				$ticket_save_mini.removeClass('x-block');
				$edit_ticket_save.removeClass('x-hidden');
				$edit_ticket_saving.addClass('x-hidden');
			}
		};
		let post_url = '';
		let post_data = objectParams({
			edit_ticket: id,
			title: title,
			amount: amount,
			limit: limit,
			description: description,
		});
		serverPOST(post_url, post_data, loading_callback).then((res) => {
			notify(res.message, 'success');
			edit_ticket_toggle();
			let ticket_data = res.data;
			let $ticket_row = $(ticket_row_html(ticket_data));
			let $row = $tickets_table_body.find('tr[data-id="' + ticket_data.id + '"]');
			if ($row.length) $row.replaceWith($ticket_row);
			else $tickets_table_body.append($ticket_row);
			ticket_settings_edit(ticket_data);
			ticket_row_indexes();
			$('.x-table').scrollLeft(0);
			$ticket_row.scrollToMe();
			$ticket_row.flashGreen();
		}, (err) => {
			UKPrompt('<p class="uk-text-danger">' + err + '</p>');
			console.error(SERVER_RESPONSE);
		});
	};
	window['ticket_settings_get'] = (id) => {
		for (let i = 0; i < TICKET_SETTINGS.length; i ++){
			if (TICKET_SETTINGS[i].id == id) return TICKET_SETTINGS[i];
		}
		return null;
	};
	ticket_settings_update(); //update tickets select
	ticket_row_indexes(); //calculate the row indexes

	//ticket edit actions
	$edit_ticket_add.click(() => edit_ticket_toggle(true));
	$edit_ticket_cancel.click(() => edit_ticket_toggle());
	$ticket_cancel_mini.click(() => edit_ticket_toggle());
	$edit_ticket_save.click(event => edit_ticket_submit(event));
	$ticket_save_mini.click(event => edit_ticket_submit(event));
	$(document.body).on("click", "td a.ticket-edit", function(event){
		let ticket_id = str($(this).attr('data-id'), '', true);
		let $row = $tickets_table_body.find('tr[data-id="' + ticket_id + '"]');
		if (!$row.length) return console.error('Failed to get row data', $row);
		let ticket_title = $row.find('td.x-ticket-title').text();
		let ticket_price = num($row.find('td.x-ticket-price').text().replace(/[^0-9\.]/g, '')) * 1;
		let ticket_limit = num($row.find('td.x-ticket-limit').text().replace(/[^0-9\.]/g, '')) * 1;
		let ticket_description = $row.find('td.x-ticket-description').text();
		edit_ticket_toggle(true);
		$edit_ticket_id.val(ticket_id);
		$edit_ticket_title.val(ticket_title);
		$edit_ticket_amount.val(ticket_price);
		$edit_ticket_limit.val(ticket_limit);
		$edit_ticket_description.val(ticket_description);
		$ticket_form.scrollToMe();
		$ticket_form.flashGreen(1, 200);
	});
	$(document.body).on("click", "td a.ticket-delete", function(event){
		let ticket_id = str($(this).attr('data-id'), '', true);
		let ticket_title = str($(this).attr('data-title'), '', true);
		let $row = $tickets_table_body.find('tr[data-id="' + ticket_id + '"]');
		if (!$row.length) return console.error('Failed to get row data', $row);
		let prompt = '<p class="uk-text-danger">Are you sure you want to <strong>permanently delete</strong> ticket "' + ticket_title + '". <strong>This might affect registered guests with this ticket title.</strong></p>';
		UKPromptConfirm(prompt, function(){
			let post_url = '';
			let post_data = 'delete_ticket=' + encodeURIComponent(ticket_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				notify(res.message, 'success');
				edit_ticket_toggle();
				$('.x-table').scrollLeft(0);
				$tickets_table_body.scrollToMe();
				$row.remove();
				ticket_settings_remove(ticket_id);
				ticket_row_indexes();
			}, (err) => {
				UKPrompt('<p class="uk-text-danger">' + err + '</p>');
				console.error(SERVER_RESPONSE);
			});
		}, null, 'Delete Ticket', 'uk-button-danger');
	});
});

$(function(){
	//register guests
	let $stats_registered = $('.x-stats-registered');
	let $stats_sales = $('.x-stats-sales');
	let $guests_table_body = $('.uk-datatable .x-table table tbody');
	let $edit_guest_toggle = $('.x-edit-guest-toggle');
	let $edit_guest_add = $edit_guest_toggle.find('button');

	//register guest
	let $guest_form = $('#guest_form');
	$guest_form.submit(event => event.preventDefault()); //prevent submit
	let $guest_cancel_mini = $('.guest_cancel_mini');
	let $guest_save_mini = $('.guest_save_mini');

	//form inputs
	let $guest_id = $('#guest_id');
	let $guest_ticket = $('#guest_ticket'); //select
	let $guest_ticket_guests = $('#guest_ticket_guests');
	let $ticket_amount = $('#ticket_amount'); //disabled readonly
	let $guest_names = $('#guest_names');
	let $guest_phone = $('#guest_phone');
	let $guest_email = $('#guest_email');
	let $guest_company = $('#guest_company');
	let $guest_address = $('#guest_address');
	let $guest_notes = $('#guest_notes');

	//form controls
	let $guest_cancel = $('#guest_cancel');
	let $guest_save = $('#guest_save');
	let $guest_saving = $('#guest_saving');

	//form helpers
	let GUEST_TICKET = null;
	let update_guest_ticket = (ticket_id) => {
		ticket_id = str(ticket_id, '', true);
		let ticket_data = null;
		if (ticket_id.length){
			if (!window.hasOwnProperty('ticket_settings_get')){
				console.error('ticket_settings_get not defined!');
				return false;
			}
			ticket_data = ticket_settings_get(ticket_id);
			if (!ticket_data){
				console.error('Unable to find ticket details!', ticket_id, TICKET_SETTINGS);
				return false;
			}
		}
		GUEST_TICKET = ticket_data;
	};
	let calc_ticket_amount = () => {
		let ticket_price = hasKeys(GUEST_TICKET, 'price') ? GUEST_TICKET.price : 0;
		let ticket_guests = $guest_ticket_guests.val();
		let guests_count = num(ticket_guests);
		if (guests_count <= 0) guests_count = 1;
		let ticket_amount = guests_count * ticket_price;
		if (ticket_guests != '' && ticket_guests != String(guests_count)) $guest_ticket_guests.val(String(guests_count));
		if (ticket_amount == 0) $ticket_amount.val('');
		else $ticket_amount.val('KES ' + ticket_amount.commas(2));
	};
	let guest_ticket_select_get = (val) => {
		let select_value = '';
		$guest_ticket.children().each(function(){
			let option_value = $(this).val();
			let option_text = $(this).text().trim().replace(/\([^\(\)]*\)$/g, '').trim();
			if (option_text == val) select_value = option_value;
		});
		return select_value;
	};
	let guest_form_clear = () => {
		$guest_id.val('');
		$guest_ticket.val('');
		$guest_ticket_guests.val('');
		$ticket_amount.val('');
		$guest_names.val('');
		$guest_phone.val('');
		$guest_email.val('');
		$guest_company.val('');
		$guest_address.val('');
		$guest_notes.val('');
	};
	let guest_form_toggle = (show) => {
		guest_form_clear();
		if (show){
			$edit_guest_toggle.addClass('x-hidden');
			$guest_form.removeClass('x-hidden');
		}
		else {
			$edit_guest_toggle.removeClass('x-hidden');
			$guest_form.addClass('x-hidden');
		}
	};
	let guest_row_html = (data) => {
		if (!hasKeys(data, 'id', 'barcode', 'names', 'phone', 'email', 'address', 'company', 'ticket_title', 'ticket_price', 'guests', 'amount', 'notes', 'time_created', 'time_modified')){
			let err = 'Invalid guest data object!';
			console.error(err, data);
			throw err;
		}
		let guest_search = [data.barcode, data.names, data.phone, data.email, data.address, data.company].join(' ');
		let data_json_base64 = btoa(stringify(data));
		let html = '';
		html += '<tr data-id="' + data.id + '" data-search="' + guest_search + '" data-base64="' + data_json_base64 + '">';
		html += '<td class="x-nowrap x-min-50 uk-datatable-row-index" data-value=""></td>';
		html += '<td class="x-nowrap x-min-100" data-name="ticket_title" data-value="' + data.ticket_title + '">' + data.ticket_title + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="ticket_price" data-value="' + data.ticket_price + '">' + data.ticket_price.commas(2) + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="guests" data-value="' + data.guests + '">' + data.guests + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="amount" data-value="' + data.amount + '">' + data.amount.commas(2) + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="barcode" data-value="' + data.barcode + '">' + data.barcode + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="names" data-value="' + data.names + '">' + data.names + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="phone" data-value="' + data.phone + '">' + data.phone + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="email" data-value="' + data.email + '">' + data.email + '</td>';
		html += '<td class="x-min-150" data-name="address" data-value="' + data.address + '">' + data.address + '</td>';
		html += '<td class="x-min-150" data-name="company" data-value="' + data.company + '">' + data.company + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="status_text" data-value="' + data.status_text + '">' + data.status_text + '</td>';
		html += '<td class="x-min-150" data-name="notes" data-value="' + data.notes + '">' + data.notes + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="time_created" data-value="' + data.time_created + '">' + data.time_created + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="time_modified" data-value="' + data.time_modified + '">' + data.time_modified + '</td>';
		html += '<td class="x-nowrap x-min-100" data-name="sent" data-value="' + data.sent + '">' + data.sent + '</td>';
		html += '<td class="x-nowrap x-min-100 x-italics x-color-b">' + data.names + '</td>';
		html += '<td class="uk-text-center"><div class="uk-form"><label class="x-checkbox"><input class="x-bulk-action" data-id="' + data.id + '" type="checkbox"></label></div></td>';
		html += '<td class="x-nowrap uk-text-center">';
		html += '<a title="Preview" draggable="false" href="javascript:" data-id="' + data.id + '" class="guest-preview uk-button uk-button-small uk-button-white"><i class="uk-icon-send"></i> Ticket</a> ';
		html += '<a title="Edit" draggable="false" href="javascript:" data-id="' + data.id + '" class="guest-edit uk-button uk-button-small uk-button-primary"><i class="uk-icon-pencil"></i></a> ';
		html += '<a title="Delete" draggable="false" href="javascript:" data-id="' + data.id + '" data-names="' + data.names + '" class="guest-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a> ';
		html += '</td>';
		html += '</tr>';
		return html;
	};
	let uk_datatable_update = () => {
		//refresh stats
		let sum_amount = 0;
		let registered = uk_datatable_rows.length;
		if (registered){
			for (let i = 0; i < uk_datatable_rows.length; i ++){
				let $row = $(uk_datatable_rows[i]);
				let $cell = $row.find('td[data-name="amount"]');
				if (!$cell.length){
					console.error('Failed to get row cell at ' + i, $row, $cell);
					continue;
				}
				let amount = num($cell.attr('data-value'));
				sum_amount += amount;
			}
		}
		$stats_registered.html(String(registered.commas(0)));
		$stats_sales.html(String(sum_amount.commas(2)));

		//refresh table
		uk_datatable_refresh();
	};
	let uk_datatable_remove = (id) => {
		let updated = false;
		if (uk_datatable_rows.length){
			for (let i = 0; i < uk_datatable_rows.length; i ++){
				let $row = $(uk_datatable_rows[i]);
				if (str($row.attr('data-id'), '', true) == id){
					uk_datatable_rows.splice(i, 1);
					updated = true;
					break;
				}
			}
		}
		if (updated) uk_datatable_update();
	};
	let uk_datatable_edit = (data) => {
		let row_html = guest_row_html(data);
		let guest_id = str(data.id, '', true);
		let $new_row = $(row_html);
		let updated = false;
		if (uk_datatable_rows.length){
			for (let i = 0; i < uk_datatable_rows.length; i ++){
				let $row = $(uk_datatable_rows[i]);
				if (str($row.attr('data-id'), '', true) == guest_id){
					uk_datatable_rows[i] = $new_row[0];
					let $table_row = $guests_table_body.find('tr[data-id="' + guest_id + '"]');
					if ($table_row.length) $table_row.replaceWith($new_row);
					updated = true;
					break;
				}
			}
		}
		if (!updated) uk_datatable_rows.unshift($new_row[0]);
		uk_datatable_update();
	};
	let guest_form_submit = (event) => {
		let id = str($guest_id.val(), '', true);
		let ticket_title = 'General Admission';
		let ticket_price = 0;
		if (hasKeys(GUEST_TICKET, 'title', 'price')){
			ticket_title = GUEST_TICKET.title;
			ticket_price = GUEST_TICKET.price;
		}
		let guests = num($guest_ticket_guests.val());
		if (guests <= 0) guests = 0;
		let names = str($guest_names.val(), '', true);
		let phone = str($guest_phone.val(), '', true);
		let email = str($guest_email.val(), '', true);
		let company = str($guest_company.val(), '', true);
		let address = str($guest_address.val(), '', true);
		let notes = str($guest_notes.val(), '', true);
		if (!names.length){
			$guest_names.inputError('Kindly enter the guest names!', true);
			return event.preventDefault();
		}
		let loading_callback = (toggle) => {
			if (toggle){
				$guest_form.addClass('x-block');
				$guest_cancel_mini.addClass('x-block');
				$guest_save_mini.addClass('x-block');
				$guest_save.addClass('x-hidden');
				$guest_saving.removeClass('x-hidden');
			}
			else {
				$guest_form.removeClass('x-block');
				$guest_cancel_mini.removeClass('x-block');
				$guest_save_mini.removeClass('x-block');
				$guest_save.removeClass('x-hidden');
				$guest_saving.addClass('x-hidden');
			}
		};
		let post_url = '';
		let post_data = objectParams({
			edit_register_guest: id,
			ticket_title: ticket_title,
			ticket_price: ticket_price,
			guests: guests,
			names: names,
			phone: phone,
			email: email,
			address: address,
			company: company,
			address: address,
			notes: notes,
		});
		serverPOST(post_url, post_data, loading_callback).then((res) => {
			let guest_data = res.data;
			notify(res.message, 'success');
			uk_datatable_edit(guest_data);
			$('.x-table').scrollLeft(0);
			guest_form_toggle();
			let $row = $guests_table_body.find('tr[data-id="' + guest_data.id + '"]');
			if ($row.length){
				$row.scrollToMe();
				$row.flashGreen(1, 200);
			}
		}, (err) => {
			UKPrompt('<p class="uk-text-danger">' + err + '</p>');
			console.error(SERVER_RESPONSE);
		});
	};

	//form listeners
	$guest_ticket.change(function(event){
		update_guest_ticket(str($(this).getValue(), '', true));
		calc_ticket_amount();
	});
	$guest_ticket_guests.on('input', () => calc_ticket_amount());
	$edit_guest_add.click(() => guest_form_toggle(true));
	$guest_cancel.click(() => guest_form_toggle());
	$guest_cancel_mini.click(() => guest_form_toggle());
	$guest_save.click((event) => guest_form_submit(event));
	$guest_save_mini.click((event) => guest_form_submit(event));

	//table listeners
	$(document.body).on("click", "td a.guest-edit", function(event){
		let guest_id = str($(this).attr('data-id'), '', true);
		let $row = $guests_table_body.find('tr[data-id="' + guest_id + '"]');
		if (!$row.length){
			console.error('Unable to find row data!', $row, guest_id);
			return event.preventDefault();
		}
		let data_base64 = str($row.attr('data-base64'), '', true);
		let guest_data = jsonParse(atob(data_base64), false);
		if (guest_data === false){
			console.error('Invalid row guest data!', data_base64, guest_data);
			return event.preventDefault();
		}
		guest_form_toggle(true);
		$guest_id.val(guest_data.id);
		$guest_ticket_guests.val(guest_data.guests);

		let guest_ticket_id = guest_ticket_select_get(guest_data.ticket_title);
		$guest_ticket.val(guest_ticket_id);
		if (guest_ticket_id.length) update_guest_ticket(guest_ticket_id);
		calc_ticket_amount();

		$guest_names.val(guest_data.names);
		$guest_phone.val(guest_data.phone);
		$guest_email.val(guest_data.email);
		$guest_company.val(guest_data.company);
		$guest_address.val(guest_data.address);
		$guest_notes.val(guest_data.notes);

		$guest_form.scrollToMe();
		$guest_form.flashGreen(1, 200);
	});
	$(document.body).on("click", "td a.guest-delete", function(event){
		let guest_id = str($(this).attr('data-id'), '', true);
		let guest_names = str($(this).attr('data-names'), '', true);
		let $row = $guests_table_body.find('tr[data-id="' + guest_id + '"]');
		if (!$row.length) return console.error('Failed to get row data', $row);
		let prompt = '<p class="uk-text-danger">Are you sure you want to <strong>permanently delete</strong> registered guest "' + guest_names + '". This renders their ticket obsolete.</p>';
		UKPromptConfirm(prompt, function(){
			let post_url = '';
			let post_data = 'delete_registered_guest=' + encodeURIComponent(guest_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				notify(res.message, 'success');
				guest_form_toggle();
				$('.x-table').scrollLeft(0);
				$guests_table_body.scrollToMe();
				$row.remove();
				uk_datatable_remove(guest_id);
			}, (err) => {
				UKPrompt('<p class="uk-text-danger">' + err + '</p>');
				console.error(SERVER_RESPONSE);
			});
		}, null, 'Delete Guest', 'uk-button-danger');
	});
});

$(function(){
	//download & printing
	let $toolbar_download = $('.x-toolbar-download');
	let $toolbar_print_list = $('.x-toolbar-print-list');
	let $toolbar_print_tickets = $('.x-toolbar-print-tickets');
	let $guests_table_body = $('.uk-datatable .x-table table tbody');

	let sending_tickets;

	//on print list
	let print_list = (checked_only) => {
		let loading_modal = null;
		let loading_callback = (toggle_loading) => {
			if (toggle_loading){
				if (!loading_modal){
					let modal_options = {keyboard: false, bgclose: false, center: true, modal: false};
					loading_modal = UIkit.modal.blockUI('<i class="uk-icon-spinner uk-icon-spin"></i> Preparing document. Please wait...', modal_options);
				}
				loading_modal.show();
			}
			else if (loading_modal) loading_modal.hide();
		};
		let test_callback_row = ($row) => {
			if (checked_only){
				let $checkbox = $row.find(".x-bulk-action");
				if (!($checkbox.length && $checkbox.isChecked())) return false;
			}
			return true;
		};
		let options = {
			title: EVENT_DATA.name + ' - Registered Guests',
			subtitle: EVENT_DATA.display_time + " | Showing ROWS_COUNT record(s)",
			logo: ROOT_URL + "/assets/img/logo.png",
			table_options: {
				aligns_row: {0: "C"},
		    }
		};
		uk_datatable_print(options, loading_callback, null, test_callback_row).then(() => console.debug("Print request successful!"), (err) => UKPrompt(err));
	};

	//on download
	let download_list = (checked_only) => {
		let loading_modal = null;
		let loading_callback = (toggle_loading) => {
			if (toggle_loading){
				if (!loading_modal){
					let modal_options = {keyboard: false, bgclose: false, center: true, modal: false};
					loading_modal = UIkit.modal.blockUI('<i class="uk-icon-spinner uk-icon-spin"></i> Preparing document. Please wait...', modal_options);
				}
				loading_modal.show();
			}
			else if (loading_modal) loading_modal.hide();
		};
		let test_callback_row = ($row) => {
			if (checked_only){
				let $checkbox = $row.find(".x-bulk-action");
				if (!($checkbox.length && $checkbox.isChecked())) return false;
			}
			return true;
		};
		uk_datatable_download(EVENT_DATA.name + ' - Registered Guests', loading_callback, null, test_callback_row).then(() => console.debug("Download request successful!"), (err) => UKPrompt(err));
	};

	//listeners
	$toolbar_download.click(() => download_list());
	$toolbar_print_list.click(() => print_list());

	//on bulk actions dropdown show
	$(".x-bulk-actions").on("show.uk.dropdown", () => $(".x-table").addClass("bulk_actions_show"));

	//on bulk actions dropdown hide
	$(".x-bulk-actions").on("hide.uk.dropdown", () => $(".x-table").removeClass("bulk_actions_show"));

	//bulk actions
	let bulk_checked = (set_checked, is_checked) => {
		let guests_ids = [];
		for (let i = 0; i < uk_datatable_rows.length; i ++){
			let $row = $(uk_datatable_rows[i]);
			if (!$row.length) continue;
			let guest_id = str($row.attr("data-id"), "", true);
			let $row_checkbox = $row.find("input.x-bulk-action");
			if (!guest_id.length || !$row_checkbox.length) continue;
			if ("boolean" === typeof set_checked){
				$row_checkbox.setChecked(set_checked);
				$("[data-id='" + guest_id + "'].x-bulk-action").setChecked(set_checked);
			}
			if ("boolean" === typeof is_checked && (is_checked === set_checked || $row_checkbox.isChecked() === is_checked) || "boolean" != typeof is_checked && $row_checkbox.isChecked()) guests_ids.push(guest_id);
		}
		return guests_ids;
	};
	let bulk_not_checked = (event) => {
		if (event) event.preventDefault();
		notify("Kindly check items for this action.", "warning");
		return false;
	};

	//bulk action listeners
	$(document.body).on("click", "a[data-action='select-all']", () => bulk_checked(true));
	$(document.body).on("click", "a[data-action='select-none']", () => bulk_checked(false));
	$(document.body).on("click", "a[data-action='print-list']", function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		print_list(true);
	});
	$(document.body).on("click", "a[data-action='download-list']", function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		download_list(true);
	});
	$(document.body).on("click", "a[data-action='send-tickets']", function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		sending_tickets(checked_ids);
	});

	//tickets
	let ticket_html = (data, for_email) => {
		let html = '';
		if (for_email) html += '<div style="border-top:10px solid #8BC34A;display:block;font-family:Helvetica;font-size:14px;text-align:center;background:#fff;">';
		else html += '<div style="max-width:350px;border:1px solid #ddd;border-top:10px solid #8BC34A;display:inline-block;font-family:Helvetica;font-size:14px;text-align:center;background:#fff;">';

		html += '<div style="padding:20px;">';
		html += '<h1 style="margin:0;color:#8BC34A;white-space:nowrap;">T I C K E T</h1>';
		if (EVENT_DATA.settings && EVENT_DATA.settings.type) html += '<p style="margin:0 0 5px;color:#8BC34A;text-align:center;"> ~ ' + data.ticket_title + ' ~ </p>';

		html += '<div style="height:1px;width:200px;margin:10px 0;border-top:1px solid #ddd;display:inline-block;"></div>';

		html += '<h2 style="margin:5px 0 0">To: <strong>' + data.names.toUpperCase() + '</strong></h2>';
		if (data.notes.trim().length) html += '<p style="margin:5px 0 0">&quot;' + data.notes + '&quot;</p>';
		if (EVENT_DATA.settings){
			if (EVENT_DATA.settings.scan_mode == 1) html += '<img draggable="false" src="' + ROOT_URL + '/?qr_code=' + data.barcode + '&size=10&margin=1" style="width:100px;height:auto;margin:10px 0 0;" />';
			if (EVENT_DATA.settings.scan_mode == 2) html += '<img draggable="false" src="' + ROOT_URL + '/?barcode=' + data.barcode + '&width=200&height=50" style="width:150px;height:auto;margin:10px 0 0;" />';
		}
		let admit = data.guests;
		html += '<p style="margin:2px 0 0;"><strong>' + data.barcode + '</strong> <i>(admit ' + admit + ' guest' + (admit > 1 ? 's' : '') + ')</i></p>';
		html += '<div style="height:1px;width:200px;margin:10px 0;border-top:1px solid #ddd;display:inline-block;"></div>';

		html += '<h2 style="margin:0;color:#8BC34A;">DETAILS</h2>';
		html += '<h2 style="margin:5px 0 0;"><strong>' + EVENT_DATA.name + '</strong></h2>';
		html += '<p style="margin: 2px 0 0;">(' + EVENT_DATA.type.name + ')</p>'
		if (EVENT_DATA.description.length) html += '<p style="margin:5px 0 0;">' + EVENT_DATA.description + '</p>';
		html += '<ul style="list-style-type:none;margin:5px 0 0;padding:0;">';
		let event_start = EVENT_DATA.start[3];
		let event_end = EVENT_DATA.end[3];
		if (event_start == event_end) html += '<li><strong>Event Date:</strong> ' + event_start + '</li>';
		else {
			html += '<li><strong>Event Starts:</strong> ' + event_start + '</li>';
			html += '<li><strong>Event Ends:</strong> ' + event_end + '</li>';
		}
		if (EVENT_DATA.location) html += '<li><strong>Location:</strong> ' + EVENT_DATA.location.name + '</li>';
		html += '</ul>';
		html += '</div>';

		if (EVENT_DATA.location_map_image) html += '<a href="' + EVENT_DATA.location_map_image + '" target="_blank"><img draggable="false" src="' + EVENT_DATA.location_map_image + '" style="width:100%;height:150px;object-fit:cover;object-position:center;" /></a>';
		html += '<div style="padding:20px;background:#444;">';
		html += '<p style="margin:5px 0;color:#fff;">Powered By</p>';
		html += '<a href="' + ROOT_URL + '" target="_blank"><img draggable="false" src="' + ROOT_URL + '/assets/img/logow.png" style="display: inline-block; width: 100px; height: auto;margin:0;" /></a>';
		html += '</div>';

		html += '</div>';
		return html;
	};
	let ticket_text = (data) => {
		let text = data.barcode + ': ' + data.names + ' ticket for ' + EVENT_DATA.name + ' (' + EVENT_DATA.type.name + ')';
		if (EVENT_DATA.description.length) text += ' - ' + EVENT_DATA.description.replace(/\s*\.\s*$/g, '') + '.';
		text += ' TIME: ' + EVENT_DATA.display_time + '.';
		let admit = data.guests;
		text += ' ADMIT: ' + admit + ' guest' + (admit > 1 || admit == 0 ? 's' : '');
		if (data.notes.trim().length) text += ' NOTE: ' + data.notes.trim();
		return text;
	};
	let ticket_barcode_index = 5;
	let fetch_ticket_pdf = (event_id, barcodes, loading_callback) => {
		return new Promise((resolve, reject) => {
			let post_url = relativeRoot() + "/document";
			let post_data = "tickets_pdf=" + encodeURIComponent(str(event_id, "", true)) + "&barcodes=" + encodeURIComponent(stringify(barcodes));
			serverPOST(post_url, post_data, loading_callback).then((res) => {
				let pdf = base64Blob(res.data, "application/pdf", 512);
				return resolve(pdf);
			}, (err) => reject(err));
		});
	};
	let ticket_print_pdf = (barcodes) => {
		let loading_modal = null;
		let loading_callback = (toggle_loading) => {
			if (toggle_loading){
				if (!loading_modal){
					let modal_options = {keyboard: false, bgclose: false, center: true, modal: false};
					loading_modal = UIkit.modal.blockUI('<i class="uk-icon-spinner uk-icon-spin"></i> Preparing document. Please wait...', modal_options);
				}
				loading_modal.show();
			}
			else if (loading_modal) loading_modal.hide();
		};
		fetch_ticket_pdf(EVENT_DATA.id, barcodes, loading_callback).then((pdf) => {
			if (window.hasOwnProperty("printJS")){
				printJS(blobURL(pdf));
				return true;
			}
			let error = "Exception (ticket_print_pdf): \"printJS\" missing";
			console.error(error);
			UKPrompt(error);
		}, (err) => UKPrompt(err));
	};
	let ticket_print_pdf_bulk = (checked_only) => {
		let test_callback_row = ($row) => {
			if (checked_only){
				let $checkbox = $row.find(".x-bulk-action");
				if (!($checkbox.length && $checkbox.isChecked())) return false;
			}
			return true;
		};
		let table_data = uk_datatable_raw(null, test_callback_row);
		if (Array.isArray(table_data) && table_data.length > 1){
			let barcodes = [];
			for (let r = 1; r < table_data.length; r ++){
				let row = table_data[r], barcode;
				if (Array.isArray(row) && row.length > ticket_barcode_index && (barcode = str(row[ticket_barcode_index], "", true)).length) barcodes.push(barcode);
			}
			ticket_print_pdf(barcodes);
		}
		else UKPrompt("No records available for this action.");
	};

	//on guest preview ticket
	$toolbar_print_tickets.click(() => ticket_print_pdf_bulk());
	$(document.body).on("click", "a[data-action='print-tickets']", function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		ticket_print_pdf_bulk(true);
	});
	$(document.body).on("click", "td a.guest-preview", function(event){
		let guest_id = str($(this).attr('data-id'), '', true);
		let $row = $guests_table_body.find('tr[data-id="' + guest_id + '"]');
		if (!$row.length){
			console.error('Unable to find row data!', $row, guest_id);
			return event.preventDefault();
		}
		let data_base64 = str($row.attr('data-base64'), '', true);
		let guest_data = jsonParse(atob(data_base64), false);
		if (guest_data === false){
			console.error('Invalid row guest data!', data_base64, guest_data);
			return event.preventDefault();
		}
		let html = '<a href="#" style="display:none;">&nbsp;</a>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<div style="background:#eee;display:inline-block;width:100%;text-align:center;padding:20px 0;">'; //2
		html += ticket_html(guest_data);
		html += '</div>'; //2
		html += '</div>'; //1
		html += '<div class="uk-modal-footer uk-text-right">';
		html += '<button type="button" class="uk-button uk-modal-close">Close</button> ';
		html += '<button type="button" data-event="print" class="uk-button uk-button-primary"><i class="uk-icon-print"></i> Print</button> ';
		html += '<button type="button" data-event="send" class="uk-button uk-button-success"><i class="uk-icon-send"></i> Send</button>';
		html += '</div>';
		UKModal(html, "x-modal x-modal-nopad", null, null, (data_event, event, $modal) => {
			if (data_event == "print"){
				ticket_print_pdf([guest_data.barcode]);
				return true;
			}
			if (data_event == "send") sending_tickets([guest_id]);
			return false;
		});
	});

	//sending tickets
	sending_tickets = (guest_ids) => {
		if (!(Array.isArray(guest_ids) && guest_ids.length)){
			console.error('No guest IDs in params');
			return false;
		}
		let html = '<a href="#" style="display:none;">&nbsp;</a>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<div style="background:#eee;display:inline-block;width:100%;">'; //2
		html += '<div class="x-pad-20">'; //3
		html += '<h2 class="x-inv-title"><i class="uk-icon-spinner uk-icon-spin"></i> Sending Ticket</h2>';
		html += '<div class="x-inv-sending-log"></div>'; //l
		html += '</div>'; //3
		html += '</div>'; //2
		html += '</div>'; //1
		html += '<div class="x-inv-sending-footer uk-modal-footer uk-text-right">';
		html += '<div class="uk-progress-bar"><div></div></div>'; //p
		html += '<button type="button" data-event="print" class="uk-button uk-button-danger">Cancel</button> ';
		html += '</div>';
		let refresh_on_close = false;
		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			let $sending_title = $modal.find('.x-inv-title');
			let $sending_log = $modal.find('.x-inv-sending-log');
			let $sending_prog = $modal.find('.x-inv-sending-footer .uk-progress-bar > div');
			let $sending_cancel = $modal.find('.x-inv-sending-footer .uk-button');
			let is_sending = true, close_on_done = false;
			let close_dialog = () => {
				is_sending = false;
				if (uk_modal.isActive()) uk_modal.hide();
			};
			let setProgress = (p) => {
				if (p === 100) $sending_prog.removeClass('animate');
				else $sending_prog.addClass('animate');
				$sending_prog.css({width: p + '%'}).html(p + '%');
			};
			$sending_cancel.click(function(){
				if (is_sending){
					is_sending = false;
					$sending_cancel.html('Cancelling...');
					$sending_cancel.removeClass('uk-button-danger');
					$sending_cancel.addClass('uk-disabled');
					$sending_cancel.attr('disabled', 'disabled');
					$sending_cancel[0].disabled = true;
					close_on_done = true;
				}
				else close_dialog();
			});
			$sending_cancel.html('Cancel');
			setProgress(0);
			$sending_log.append($('<p>Sending ' + guest_ids.length + ' tickets...</p>'));
			let len = guest_ids.length;
			objectPromise(guest_ids, function(guest_id, key, i){
				return new Promise((resolve, reject) => {
					if (!is_sending) return resolve();
					let err;
					let n = i + 1;
					let p = Math.floor(n / len * 100);
					let o = n + '/' + len;
					let $log = $('<p>Preparing ticket ' + o + '...</p>');
					let onError = (err, _class) => {
						$log.html('[' + o + '] ' + err);
						$log.addClass(str(_class, 'uk-text-default', true));
						setProgress(p);
						return reject(err);
					};
					$sending_log.append($log);
					let guest_id = guest_ids[i];
					let $row = $("tr#guest-" + guest_id);
					if (!guest_id.length || !$row.length) return onError('Unable to get guest row data!', 'uk-text-danger');
					let data_base64 = str($row.attr("data-base64"), "", true);
					let data = jsonParse(atob(data_base64), false, (temp) => hasKeys(temp, "id", "names", "barcode"));
					if (!data) return onError('Unable to get guest data!', 'uk-text-danger');

					let names = data.names;
					let tphone = str(data.phone, '', true);
					let temail = str(data.email, '', true);
					if (!tphone.length && !temail.length) return onError('(' + names + ') No ticket contacts.', 'uk-text-warning');
					let thtml = ticket_html(data, true);
					let ttext = ticket_text(data);

					//post ticket
					$log.html('[' + o + '] (' + names + ') Sending ticket...');
					let post_url = relativeRoot() + "/messenger";
					let post_data = objectParams({
						send_ticket: btoa(thtml),
						event: EVENT_DATA.id,
						names: names,
						phone: tphone,
						email: temail,
						text: ttext,
						barcode: data.barcode,
					});
					serverPOST(post_url, post_data, null).then((res) => {
						refresh_on_close = true;
						$log.html('[' + o + '](' + names + ') ' + res.message);
						setProgress(p);
						resolve();
					}, (err) => {
						console.error(SERVER_RESPONSE);
						onError(err);
					});
				});
			}, true).then(() => {
				is_sending = false;
				$sending_title.find('i').addClass('x-hidden');
				if (close_on_done){
					$sending_log.append($('<p>Cancelled!</p>'));
					setTimeout(() => close_dialog(), 1000);
					return;
				}
				$sending_log.append($('<p>Finished!</p>'));
				$sending_cancel.html('Close');
			});
		}, function(){
			if (refresh_on_close){
				loading_block(true, 'Refreshing list...');
				setTimeout(() => window.location.reload(), 500);
			}
		}, null, false, false, true, false);
	};
});
