$(function(){
	//form controls
	$event_is_new = $('#event_is_new');
	$event_poster_keep = $('#event_poster_keep');
	$event_poster = $('#event_poster');
	$event_poster_change = $('#event_poster_change');
	$event_poster_remove = $('#event_poster_remove');
	$event_poster_file = $('#event_poster_file');
	$event_name = $('#event_name');
	$event_type = $('#event_type');
	$event_type_other_wrapper = $('.event_type_other_wrapper');
	$event_type_other = $('#event_type_other');
	$event_start_date = $('#event_start_date');
	$event_start_time = $('#event_start_time');
	$event_end_date = $('#event_end_date');
	$event_end_time = $('#event_end_time');
	$event_description = $('#event_description');
	$event_guests_expected = $('#event_guests_expected');
	$event_guests_max = $('#event_guests_max');
	$location_map = $('#location_map');
	$event_form = $('#event_form');
	$save_details = $('#save_details');

	//listeners
	let IS_NEW = num($event_is_new.val()) === 1;
	let placeholder_image = ROOT_URL + '/assets/img/placeholder.jpg';

	//event poster
	let reset_event_poster_file = (event) => {
		if (event) event.preventDefault();
		$event_poster_file[0].value = "";
		return false;
	};
	$event_poster_file.change(function(event){
		let files = event.target.files;
		if (FileReader && files && files.length){
			let file = files[0];
			let fsize = num(file.size / (1024 ** 2)).round(2);
			let on_error = (msg, e) => {
				UKPrompt('<p class="uk-text-danger">' + msg + '</p>', null, 'Close', 'uk-button-white');
				return reset_event_poster_file(e);
			};
			if (!fsize) return on_error('Image upload failure!', event);
			if (fsize > 2) return on_error('Image size is larger than the allowed image size of 2MB! Current size ' + fsize + 'MB.', event);
			fileBase64(file).then((image_data) => {
				createImage(image_data).then((img) => {
					let w = img.width, h = img.height;
					if (w > h && (w / h) > 1.5) return on_error('Image is too wide. Try another image or ensure the image width/height ratio does not exceed 1.5');
					else if (h > w && (h / w) > 2) return on_error('Image is too long. Try another image or ensure the image height/width ratio does not exceed 2');
					base64ImageResize(image_data, 600, 1200).then((image_data_resized) => {
						$event_poster.attr('src', image_data_resized);
						$event_poster_change.html('Change Image');
						$event_poster_remove.removeClass('x-hidden');
						$event_poster_keep.val('0');
						reset_event_poster_file();
					}, (err) => on_error(err));
				}, (err) => on_error(err));
			}, (err) => on_error(err));
		}
	});
	$event_poster.click(() => $event_poster_file.click());
	$event_poster_change.click(() => $event_poster_file.click());
	$event_poster_remove.click(() => {
		let do_remove = () => {
			$event_poster.attr('src', placeholder_image);
			$event_poster_keep.val('0');
			$event_poster_remove.addClass('x-hidden');
			$event_poster_change.html('Select Image');
		};
		if (IS_NEW) return do_remove();
		else UKPromptConfirm('<p class="uk-text-danger">Are you sure you want to remove this image?</p>', () => do_remove(), null, 'Remove', 'uk-button-danger');
	});

	//event name
	let on_event_name_change = (update) => {
		let temp = str($event_name.val(), '', true);
		if (!temp.length) return $event_name.inputError('Kindly enter your event name');
		if (update) $event_name.val(temp);
	};
	$event_name.on('change', () => on_event_name_change());
	$event_name.on('blur', () => on_event_name_change(true));

	//event type
	$event_type.on('change', function(event){
		let temp = $event_type[0].new_item;
		if (temp && temp.value == 0) $event_type_other_wrapper.removeClass('x-hidden');
		else {
			$event_type_other_wrapper.addClass('x-hidden')
			$event_type_other.inputError();
			$event_type_other.val('');
		}
		if (!temp) return $event_type.inputError('Kindly select your event type');
	});

	//event type other
	let on_event_type_other_change = (update) => {
		let temp = str($event_type_other.val(), '', true);
		if (!temp.length) return $event_type_other.inputError('Kindly specify your "other" event type');
		if (update) $event_type_other.val(temp);
	};
	$event_type_other.on('change', () => on_event_type_other_change());
	$event_type_other.on('blur', () => on_event_type_other_change(true));

	//dates and time
	let DATE_FORMAT = "DD/MM/YYYY";
	let TIME_FORMAT = "hh:mm A";
	Date.prototype.dateStamp = function(){
		if (!isDate(this)) return null;
		return this.getFullYear() + pad(this.getMonth() + 1) + pad(this.getDate());
	};
	Date.prototype.timeStamp = function(){
		if (!isDate(this)) return null;
		return pad(this.getHours()) + pad(this.getMinutes());
	};
	Date.prototype.toDateString = function(){
		return moment(this).format(DATE_FORMAT);
	};
	Date.prototype.toTimeString = function(){
		return moment(this).format(TIME_FORMAT);
	};
	let dateString = (val, return_date) => {
		if ("string" === typeof val && (val = val.trim()).length){
			let temp_date = moment(val, DATE_FORMAT).toDate();
			if (isDate(temp_date)){
				let temp_string = temp_date.toDateString();
				if (val == temp_string) return return_date ? temp_date : temp_string;
			}
		}
		else if (isDate(val)) return return_date ? val : val.toDateString();
		return null;
	};
	let timeString = (val, return_date) => {
		if ("string" === typeof val && (val = val.trim()).length){
			let temp_date = moment(val, TIME_FORMAT).toDate();
			if (isDate(temp_date)){
				let temp_string = temp_date.toTimeString();
				let temp_date2 = moment(temp_string, TIME_FORMAT).toDate();
				if (isDate(temp_date2)){
					let temp_string2 = temp_date2.toTimeString();
					if (temp_string2 == temp_string) return return_date ? temp_date : temp_string;
				}
			}
		}
		else if (isDate(val)) return return_date ? val : val.toTimeString();
		return null;
	};

	//event start date
	let on_start_date_change = () => {
		let val = str($event_start_date.val(), '', true);
		let date_string = dateString(val);
		if (!date_string) return $event_start_date.inputError('Kindly enter/select a valid date (' + DATE_FORMAT + ')');
		$event_end_date.val(date_string);
		$event_end_date.inputError();
		on_start_time_change();
	};
	$event_start_date.on('change', on_start_date_change);
	$event_start_date.on('blur', on_start_date_change);

	//event start time
	let on_start_time_change = () => {
		let val = str($event_start_time.val(), '', true);
		let time_string = timeString(val);
		if (!time_string) return $event_start_time.inputError('Kindly enter/select a valid time (' + TIME_FORMAT + ')');
		$event_end_time.inputError();
		$event_end_time.val(time_string);
		let time_date = timeString(time_string, true);
		$event_end_time.attr('data-uk-timepicker', "{format:'12h',start:" + time_date.getHours() + "}");
	};
	$event_start_time.on('change', on_start_time_change);
	$event_start_time.on('blur', on_start_time_change);

	//date time comparisons
	let compare_start_end_date = () => {
		let start_date = dateString(str($event_start_date.val(), '', true), true);
		let end_date = dateString(str($event_end_date.val(), '', true), true);
		let pass = true;
		if (!isDate(start_date)){
			$event_start_date.inputError('Kindly enter/select a valid date (' + DATE_FORMAT + ')');
			pass = false;
		}
		if (!isDate(end_date)){
			$event_end_date.inputError('Kindly enter/select a valid date (' + DATE_FORMAT + ')');
			pass = false;
		}
		if (pass && (start_date.getTime() > end_date.getTime())){
			$event_end_date.inputError('Event end date is less than the start date');
			pass = false;
		}
		return pass;
	};
	let compare_start_end_time = () => {
		let start_date = dateString(str($event_start_date.val(), '', true), true);
		let end_date = dateString(str($event_end_date.val(), '', true), true);
		if (!compare_start_end_date()) return false;
		let start_time = timeString(str($event_start_time.val(), '', true), true);
		let end_time = timeString(str($event_end_time.val(), '', true), true);
		let start_timestamp = Number(start_date.dateStamp() + start_time.timeStamp());
		let end_timestamp = Number(end_date.dateStamp() + end_time.timeStamp());
		if (start_timestamp > end_timestamp){
			$event_end_time.inputError('End time is less than the start time');
			return false;
		}
		return true;
	}

	//event end date
	let on_end_date_change = (event) => {
		let val = str($event_end_date.val(), '', true);
		let date_string = dateString(val);
		if (!date_string){
			$event_end_date.inputError('Kindly enter/select a valid date (' + DATE_FORMAT + ')');
			event.stopImmediatePropagation();
			return false;
		}
		if (!compare_start_end_date()){
			event.stopImmediatePropagation();
			return false;
		}
	};
	$event_end_date.on('change', on_end_date_change);
	$event_end_date.on('blur', on_end_date_change);

	//event end time
	let on_end_time_change = (event) => {
		let val = str($event_end_time.val(), '', true);
		let time_string = timeString(val);
		if (!time_string){
			$event_end_time.inputError('Kindly enter/select a valid time (' + TIME_FORMAT + ')');
			event.stopImmediatePropagation();
			return false;
		}
		if (!compare_start_end_time()){
			event.stopImmediatePropagation();
			return false;
		}
	};
	$event_end_time.on('change', on_end_time_change);
	$event_end_time.on('blur', on_end_time_change);

	//init date time values for new event
	if (IS_NEW){
		let today = new Date();
		let today_date_string = today.toDateString();
		let today_time_string = today.toTimeString();
		$event_start_date.val(today_date_string);
		$event_start_time.val(today_time_string);
		on_start_date_change();
	}

	//init location map
	let GOOGLE_MAPS_LOADED = window.hasOwnProperty("google") && "object" === typeof google && google.hasOwnProperty("maps");
	if (!GOOGLE_MAPS_LOADED) $location_map.find('.map-input-search').inputError('Google maps library is not available! Kindly refresh to try again.');

	//event form
	$event_form.submit((event) => event.preventDefault());

	//save details
	$save_details.click(function(event){
		let pass = true;
		let $scroll_elem = null;
		let poster_keep = num($event_poster_keep.val());
		let poster_data = str($event_poster.attr('src'), '', true);
		if (poster_keep || poster_data == placeholder_image) poster_data = '';
		let event_name = str($event_name.val(), '', true);
		if (!event_name.length){
			$event_name.inputError('Kindly enter the event name');
			$scroll_elem = $event_name;
			pass = false;
		}
		let event_type = $event_type[0].new_item;
		if (!event_type){
			$event_type.inputError('Kindly select your event type');
			if (!$scroll_elem) $scroll_elem = $event_type;
			pass = false;
		}
		let event_type_other = str($event_type_other.val(), '', true);
		if (event_type && event_type.value == 0 && !event_type_other.length){
			$event_type_other.inputError('Kindly specify "other" event type');
			if (!$scroll_elem) $scroll_elem = $event_type_other;
			pass = false;
		}
		if (event_type && event_type.value != 0) event_type_other = '';
		if (!compare_start_end_time()){
			if (!$scroll_elem) $scroll_elem = $event_start_date;
			pass = false;
		}
		let start_date = str($event_start_date.val(), '', true);
		let start_time = str($event_start_time.val(), '', true);
		let end_date = str($event_end_date.val(), '', true);
		let end_time = str($event_end_time.val(), '', true);
		let description = str($event_description.val(), '', true);
		let guests_expected = str($event_guests_expected.val(), '', true);
		let guests_max = str($event_guests_max.val(), '', true);
		let location_map = jsonParse($location_map.val());
		if (!pass){
			if ($scroll_elem) $scroll_elem.scrollToMe(null, 40);
			event.preventDefault();
			return false;
		}
		let data = {
			poster_keep: poster_keep,
			poster_data: poster_data,
			event_name: event_name,
			event_type: event_type,
			event_type_other: event_type_other,
			start_date: start_date,
			start_time: start_time,
			end_date: end_date,
			end_time: end_time,
			description: description,
			guests_expected: guests_expected,
			guests_max: guests_max,
			location_map: location_map,
		};
		let post_url = '';
		let post_data = objectParams(data);
		serverPOST(post_url, post_data, loading_block).then((res) => {
			UKPrompt('<p class="uk-text-success">Event details saved successfully!</p>');
			setTimeout(() => {
				window.location.href = ROOT_URL + '/dashboard?event=' + res.message;
			}, 100);
		}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
	});

});
