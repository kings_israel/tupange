$(function(){
	let default_avatar = ROOT_URL + '/assets/img/user.png';

	//rsvp confirm
	let confirm = (status) => {
		let status_texts = {2: 'Attending', 3:'Maybe', 4: 'Reject'};
		let button_classes = {2: 'uk-button-success', 3: 'uk-button-primary', 4: 'uk-button-danger'};
		let button_class = button_classes[status];
		let status_text = status_texts[status];
		let prompt = '<p>Are you sure you want to respond to the invitation with: <strong>' + status_text + '</strong>?</p>';
		UKPromptConfirm(prompt, () => {
			let post_url = '';
			let post_data = objectParams({
				confirm: status,
				guests: $('#guests').length ? $('#guests').val().trim() : '',
				diet: $('#diet').length ? $('#diet').val().trim() : '',
			});
			serverPOST(post_url, post_data, loading_block).then(() => {
				loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
				setTimeout(() => window.location.reload(), 100);
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Response: ' + status_text, button_class);
	};
	$('button#confirm').click(() => confirm(2));
	$('button#maybe').click(() => confirm(3));
	$('button#reject').click(() => confirm(4));

	//rsvp review
	$('button#review').click(() => {
		let post_url = '';
		let post_data = 'get_reviews=1';
		serverPOST(post_url, post_data, loading_block).then((res) => {
			let data = res.data;
			let keys = Object.keys(data), e = keys.indexOf('event');
			keys.splice(e, 1);
			keys.unshift('event');
			let html = '';
			html += '<a class="uk-modal-close uk-close"></a>';
			html += '<div class="uk-modal-header">MY REVIEWS</div>';
			html += '<div class="uk-overflow-container x-guest-reviews">'; //1
			for (let i = 0; i < keys.length; i ++){
				let key = keys[i];
				let rev = data[key];
				if (rev.reviewed){
					html += '<div class="x-review-comment">'; //c
					html += '<div class="x-comment ' + key + '">'; //cc
					html += '<strong>' + rev.title + '</strong>';
					html += '<div class="x-meta"><div class="uk-rating-stars" data-rate="' + rev.rating + '"></div> ' + rev.timestamp + '</div>';
					html += '<p>' + rev.comment + '</p>';
					html += '</div>'; //cc
					html += '</div>'; //c
				}
				else {
					html += '<div class="x-review-item" data-id="' + key + '" data-title="' + rev.title + '">'; //d
					html += '<p>' + (key === 'event' ? '<strong>How can you rate the event?</strong>' : '<strong>How can you rate this service:</strong> ' + rev.title) + '</p>';
					html += '<div class="x-review-rating-stars">'; //c
					html += '<h1 class="review-rating x-iblock x-nomargin uk-rating-stars editable" data-rate="' + rev.rating + '" data-stars="5"></h1>';
					html += '</div>'; //c
					html += '<div class="uk-form-row uk-margin-small-top">'; //c
					html += '<label class="uk-form-label" for="review-comment">Comment <span class="uk-text-danger">*</span></label>';
					html += '<div class="uk-form-controls">';
					html += '<textarea class="review-comment uk-width-1-1" placeholder="Review Comment" onkeyup="this.value = sentenseCase(this.value)">' + rev.comment + '</textarea>';
					html += '</div>';
					html += '</div>'; //c
					html += '<div class="uk-text-right"><button class="review-submit uk-button uk-button-success">Submit Review</button></div>';
					html += '</div>'; //d
				}
			}
			html += '</div>'; //1
			html += '<div class="uk-modal-footer uk-text-right">'; //f
			html += '<button type="button" class="uk-button uk-modal-close uk-button-white x-min-100">Close</button> ';
			html += '</div>'; //f
			UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
				initRatingStars();
				$review_submit = $modal.find('.review-submit');
				$review_submit.click(function(event){
					let $self = $(this);
					let $parent = $self.parent().parent();
					let title = str($parent.attr('data-title'), '', true);
					let order_id = str($parent.attr('data-id'), '', true);
					let rating = num($parent.find('.review-rating').attr('data-rate'));
					let comment = str($parent.find('.review-comment').val(), '', true);
					let submit = {save_review: order_id, rating: rating, comment: comment};
					let loading_callback = (toggle) => {
						if (toggle){
							$parent.addClass('x-block');
							$self.removeClass('uk-button-success');
							$self.addClass('uk-disabled');
							$self.html('<i class="uk-icon-spinner uk-icon-spin"></i> Please wait...');
						}
						else {
							$self.addClass('uk-button-success');
							$self.removeClass('uk-disabled');
							$self.html('Submit Review');
							$parent.removeClass('x-block');
						}
					};
					let post_url = '';
					let post_data = objectParams(submit);
					serverPOST(post_url, post_data, loading_callback).then((res) => {
						let rev = res.data, html = '';
						html += '<div class="x-review-comment">'; //c
						html += '<div class="x-comment ' + order_id + '">'; //cc
						html += '<strong>' + title + '</strong>';
						html += '<div class="x-meta"><div class="uk-rating-stars" data-rate="' + rev.rating + '"></div> ' + rev.timestamp + '</div>';
						html += '<p>' + rev.comment + '</p>';
						html += '</div>'; //cc
						html += '</div>'; //c
						$parent.replaceWith($(html));
						initRatingStars();
					}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
				});
			}, null, true, false, false, false);
		}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
	});

});
