$(function(){
	//init search filters
	$search_query = $("#search_query");
	$filter_query = $("#filter_query");
	$filter_query.val($search_query.val());
	$search_query.on("input", () => $filter_query.val($search_query.val()));
	$filter_form = $("#filter_form");
	$search_query_form = $("#search_query_form");

	$('#xx_location').on('change', function(event){
		let val = $('#xx_location').val();
		let $loc = $filter_form.find('[name="location"]');
		if ($loc.length) $loc.val(val);
		$filter_form.submit();
	});
	$('#xx_date').on('change', function(event){
		let val = $(this).val();
		let $date = $filter_form.find('[name="date"]');
		if ($date.length) $date.val(val);

		$filter_form.submit();
	});
	$('#xx_rating').on('change', function(event){
		let val = $(this).val();
		let $rating = $filter_form.find('[name="rating"]');
		if ($rating.length) $rating.val(val);

		$filter_form.submit();
	});
	$(".xx-categories > li > a").click(function(event){
		event.preventDefault();
		let val = num($(this).attr('data-value'));
		let $cat = $filter_form.find('[name="category"]');
		if ($cat.length) $cat.val(val);

		$filter_form.submit();
	});

	$search_query_form.on("submit", function(event){
		event.preventDefault();
		$filter_form.submit();
		return false;
	});

	const urlParams = new URLSearchParams(window.location.search);
	const sd = tstr(urlParams.get('sd'));
	if (sd){
		/*
		let url = window.location.protocol + '//' + window.location.host + window.location.pathname;
		let params = '';
		urlParams.forEach((v,k) => {
			if (k != 'sd') params += (!params ? '' : '&') + k + '=' + encodeURIComponent(v)
		});
		if (params) url += '?' + params;
		history.replaceState({}, document.title, url);
		*/
		$filter_form.find('[name="date"]').val(atob(sd));
		$('#xx_date').val(atob(sd));
	}
});
