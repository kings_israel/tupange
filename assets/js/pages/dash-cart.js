$(function(){
	//on cart item remove
	$(document.body).on("click", ".x-table td .item-remove", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		if (!window.hasOwnProperty("UKPromptConfirm")) return event.preventDefault();
		UKPromptConfirm("Are you sure you want to delete <strong>\"" + item_name + "\"</strong> from cart?", function(){
			let redirect = ROOT_URL + "/dashboard?cart&delete-item=" + item_id;
			window.location.href = redirect;
		}, null, "Remove Item", "uk-button-danger");
	});

	//checkout
	let checkout_prompt = () => {
		let html = '';
		html += '<a class="uk-modal-close uk-close"></a>';
		html += '<div class="uk-modal-header">CHECKOUT <small>(' + CART_ITEMS.length + ' Orders)</small></div>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<form class="uk-form checkout-form" action="javascript:" method="post">'; //f
		html += '<div class="x-table checkout-table">'; //xt
		html += '<table class="uk-table">';
		html += '<thead><tr>';
		html += '<th class="x-nowrap">#</th>';
		html += '<th class="x-nowrap x-min-100">Service</th>';
		html += '<th class="x-nowrap x-min-150">Pricing</th>';
		html += '<th class="x-nowrap x-min-200">Message</th>';
		html += '</tr></thead>';
		html += '<tbody>';
		for (let i = 0; i < CART_ITEMS.length; i ++){
			let item_service = CART_ITEMS[i].data.service;
			let item_pricing = CART_ITEMS[i].data.pricing;
			let item_vendor = CART_ITEMS[i].data.vendor;
			let item_service_id = item_service.id.trim();
			html += '<tr>';
			html += '<td>' + (i + 1) + '</td>';

			html += '<td>';
			html += item_service.title
			html += '<br><small><i><strong>Category: </strong>' + item_service.category_name + '</i></small>';
			html += '<br><small><i><strong>Company: </strong>' + item_vendor.company_name + '</i></small>';
			html += '</td>';

			html += '<td>';
			html += '<div class="uk-form-controls">'; //c
			html += '<select data-service="' + item_service_id + '" class="item-pricing uk-width-1-1" data-default="0">';
			html += '<option value="" selected="selected">Get Quote</option>';
			for (let p = 0; p < item_pricing.length; p ++){
				let pricing = item_pricing[p];
				html += '<option value="' + pricing.id + '">' + pricing.title + ' (' + pricing.price.commas(2) + ')' + '</option>';
			}
			html += '</select>';
			html += '</div>'; //c
			html += '</td>';

			html += '<td>';
			html += '<div class="uk-form-controls">'; //c
			html += '<textarea data-service="' + item_service_id + '" class="item-message uk-width-1-1" placeholder="Custom Order Message"></textarea>';
			html += '</div>'; //c
			html += '</td>';

			html += '</tr>';
		}
		html += '</tbody>';
		html += '</table>';
		html += '</div>'; //xt
		html += '</form>';
		html += '</div>'; //1

		html += '<div class="uk-modal-footer uk-form">'; //1
		html += '<div class="uk-form-row">'; //fr
		html += '<label class="uk-form-label x-fw-400" for="link-event">Link Orders To Event</label>';
		html += '<div class="uk-form-controls uk-margin-small-top">'; //fc
		html += '<select class="linked-event uk-width-1-1" data-default="0">';
		if (!USER_EVENTS.length) html += '<option value="" selected="selected">No Events Available</option>';
		else {
			html += '<option value="" selected="selected">Select Event...</option>';
			for (let i = 0; i < USER_EVENTS.length; i ++){
				let event = USER_EVENTS[i];
				html += '<option value="' + event.id + '">' + event.text + '</option>';
			}
		}
		html += '</select>';
		html += '</div>'; //fc
		html += '</div>'; //fr
		html += '<div class="uk-grid uk-grid-small uk-margin-top" data-uk-grid-margin>'; //g
		html += '<div class="uk-width-medium-1-2 x-color-b">'; //g1
		html += '<strong>Pricing Total:</strong> <span id="pricing-total">0</span>';
		html += '</div>'; //g1
		html += '<div class="uk-width-medium-1-2">'; //g2
		html += '<div class="uk-text-right">'; //b
		html += '<span class="uk-align-left"></span>';
		html += '<button type="button" class="uk-button uk-modal-close">Cancel</button> ';
		html += '<button type="button" id="checkout-form-submit" class="uk-button uk-button-primary">Finish</button> ';
		html += '</div>'; //b
		html += '</div>'; //g2
		html += '</div>'; //g
		html += '</div>'; //1

		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			//pricing data
			let pricing_data = {};
			for (let i = 0; i < CART_ITEMS.length; i ++){
				let item_service = CART_ITEMS[i].data.service;
				let item_pricing = CART_ITEMS[i].data.pricing;
				let pricing_items = {};
				for (let p = 0; p < item_pricing.length; p ++) pricing_items[item_pricing[p].id] = item_pricing[p];
				pricing_items.title = item_service.title;
				pricing_data[item_service.id] = pricing_items;
			}

			//controls
			let $pricings = $modal.find("form.checkout-form select.item-pricing");
			let $pricing_total = $modal.find("#pricing-total");
			let $linked_event = $modal.find("select.linked-event");
			let $checkout_submit = $modal.find("#checkout-form-submit");

			//check controls
			let controls = [$pricings, $pricing_total, $linked_event, $checkout_submit];
			for (let i = 0; i < controls.length; i ++){
				if (!controls[i].length){
					console.error("Unable to locate control", controls[i]);
					return false;
				};
			}

			//pricing total
			let calc_pricing_total = () => {
				let total = 0;
				for (let i = 0; i < CART_ITEMS.length; i ++){
					let service_id = CART_ITEMS[i].data.service.id;
					let pricing_id = str($modal.find("select[data-service='" + service_id + "']").val(), "", true);
					let price = pricing_id.length ? num(propval(pricing_data, service_id, pricing_id, "price")) : 0;
					total += price;
				}
				$pricing_total.html(total.commas(2));
			};
			$pricings.change(() => calc_pricing_total());
			calc_pricing_total();

			//checkout orders
			$checkout_submit.click(function(event){
				//checkout items
				let checkout_items = [];
				for (let i = 0; i < CART_ITEMS.length; i ++){
					let item_id = CART_ITEMS[i].id;
					let item_service = CART_ITEMS[i].data.service;
					let item_pricing = CART_ITEMS[i].data.pricing;
					let item_vendor = CART_ITEMS[i].data.vendor;
					let service_id = item_service.id;
					let pricing_id = str($modal.find("select[data-service='" + service_id + "']").val(), "", true);
					let pricing = pricing_id.length ? propval(pricing_data, service_id, pricing_id) : null;
					let message = str($modal.find("textarea[data-service='" + service_id + "']").val(), "", true);
					let checkout_item = {
						id: item_id,
						service_id: service_id,
						title: item_service.title,
						category: item_service.category_name,
						vendor: item_vendor.company_name,
						pricing: pricing,
						message: message,
					};
					checkout_items.push(checkout_item);
				}

				//linked event
				let linked_event = null, linked_event_id = str($linked_event.val(), "", true);
				if (linked_event_id.length){
					for (let i = 0; i < USER_EVENTS.length; i ++){
						if (USER_EVENTS[i].id == linked_event_id){
							linked_event = USER_EVENTS[i];
							break;
						}
					}
				}

				//confirmation
				let html = '<h2 class="x-color-g">Confirm Orders</h2>', post_items = [], pricing_total = 0;
				html += '<p class="x-color-g x-nomargin"><strong>Event:</strong> ' + (linked_event ? linked_event.text.trim() : " None") + '</h3>';
				post_items.push("cart-checkout=" + encodeURIComponent(linked_event ? linked_event.id : ""));
				for (let i = 0; i < checkout_items.length; i ++){
					let item = checkout_items[i];
					html += '<div class="x-margin-top-10 x-padding-top-10 x-border-top">';
					html += '<strong>' + item.title + '</strong>';
					html += '<br><small><strong>Pricing: </strong>' + (item.pricing ? item.pricing.title + ' (' + item.pricing.price.commas(2) + ')' : 'Get Quote') + '</small>';
					html += '<br><small><strong>Message: </strong>' + (item.message.length ? item.message : 'None') + '</small>';
					html += '</div>';
					let post_item = "items[" + i + "][id]=" + encodeURIComponent(item.id);
					post_item += "&items[" + i + "][service_id]=" + encodeURIComponent(item.service_id);
					post_item += "&items[" + i + "][pricing][id]=" + encodeURIComponent(item.pricing ? item.pricing.id : "");
					post_item += "&items[" + i + "][pricing][price]=" + encodeURIComponent(item.pricing ? item.pricing.price : "");
					post_item += "&items[" + i + "][message]=" + encodeURIComponent(item.message);
					post_items.push(post_item);
					pricing_total += (item.pricing ? item.pricing.price : 0);
				}
				html += '<div class="x-margin-top-10 x-padding-top-10 x-border-top x-color-b"><strong>Pricing Total: </strong>' + pricing_total.commas(2) + '</div>';

				//confirm dialog
				UKPromptConfirm(html, function(){
					let post_url = "";
					let post_data = post_items.join("&");
					serverPOST(post_url, post_data, loading_block).then((res) => {
						//hide dialog
						uk_modal.hide();

						//reset table
						uk_datatable_rows = [];
						uk_datatable_refresh();

						//hide checkout button
						$(".x-cart-checkout-wrapper").addClass("x-hidden");

						//update badge counts
						badgeSet("orders", badgeGet("orders") + checkout_items.length);
						badgeSet("cart", badgeGet("cart") - checkout_items.length);

						//success message
						UKPromptConfirm(res.message, function(){
							let orders_url = ROOT_URL + "/orders?open";
							window.location.href = orders_url;
						}, null, "View Open Orders", "uk-button-primary");
					}, function(err){
						UKPrompt('<p class="uk-text-danger">' + err + '</p>');
					});
				}, null, '<i class="uk-icon-cart-arrow-down"></i> Checkout', "uk-button-success");
			});

		}, null, null, false, false, true, true);
	};
	$(".x-cart-checkout").click(function(event){
		if (!(Array.isArray(CART_ITEMS) && CART_ITEMS.length)) return event.preventDefault();
		checkout_prompt();
	});
});
