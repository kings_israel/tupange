$(function(){
	//creates a popup window
	let popupCenter = function(url, title, w, h){
		let dual_left = window.screenLeft != undefined ? window.screenLeft : window.screenX;
		let dual_top = window.screenTop != undefined ? window.screenTop : window.screenY;
		let width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		let height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
		let zoom = width / window.screen.availWidth;
		let left = (width - w) / 2 / zoom + dual_left;
		let win = window.open(url, title, "scrollbars=yes,width=" + (w / zoom) + ',height=' + (h / zoom) + ',top=100,left=' + left);
		if (window.focus) win.focus();
		return win;
	};

	//starts an OAuth popup
	let signUpOAuth = function(account, provider, error_wrapper){
		let url = "./oauth?provider=" + provider + "&action=signup&account=" + account;
		window["HandleOAuth"] = function(response){
			let error_message = "Unexpected OAuth response!";
			if (hasKeys(response, "pass", "message")){
				if (response.pass){
					let redirect_url = window.location.href = ROOT_URL + "/dashboard";
					if (window.hasOwnProperty("redirect") && (redirect = str(redirect, "", true)).length) redirect_url = decodeURIComponent(redirect);
					window.location.href = redirect_url;
					return;
				}
				else error_message = response.message;
			}
			else console.error(error_message, response);
			error_wrapper.html(errorMessage(error_message));
		};
		let win = popupCenter(url, "Sign Up " + provider, 500, 400);
	};

	//listen for social media sign up
	$social_error = $("#x-social-error");
	let signup_account = Number($("#register_account").val());
	if ([1,2].indexOf(signup_account) > -1){
		let signup_type = signup_account == 1 ? "client" : "vendor";
		$(".x-btn-fb").click(() => signUpOAuth(signup_type, "Facebook", $social_error));
		$(".x-btn-tw").click(() => signUpOAuth(signup_type, "Twitter", $social_error));
		$(".x-btn-go").click(() => signUpOAuth(signup_type, "Google", $social_error));
	}
});
