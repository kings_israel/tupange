$(function(){
	//mark as seen
	let update_user_seen = () => {
		let update_seen = [];
		let $unseen = $("[data-seen='']");
		$unseen.each(function(){
			$row = $(this);
			let order_seen = str($row.attr("data-seen"), "", true);
			let order_id = str($row.attr("data-id"), "", true);
			if (order_id.length && !order_seen.length) update_seen.push(order_id);
		});
		$unseen.flashGreen();
		let post_url = "";
		let post_data = "update_seen_user=" + encodeURIComponent(update_seen.join(","));
		serverPOST(post_url, post_data, null).then((res) => {
			badgeSet("orders", badgeGet("orders") - update_seen.length);
		}, function(err){
			console.error(err, SERVER_RESPONSE);
			notify(err, "warning");
		});
	};
	setTimeout(() => update_user_seen(), 1000);

	//order delete
	$(document.body).on("click", ".x-table td .item-remove", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		let item_index = -1;
		for (let i = 0; i < uk_datatable_rows.length; i ++){
			if (str($(uk_datatable_rows[i]).attr("data-id"), "", true) == item_id){
				item_index = i;
				break;
			}
		}
		if (item_index < 0){
			console.error("Unable to find item index", item_id);
			return event.preventDefault();
		}
		if (!window.hasOwnProperty("UKPromptConfirm")) return event.preventDefault();
		UKPromptConfirm("<h2 class='uk-text-danger'>Cancel Order</h2><p class='uk-text-danger'>Cancel order <strong>" + item_name + "</strong>?<br>This action cannot be undone. <strong>Cancelled orders are automatically deleted after 7 days</strong></p>", function(){
			let post_url = "";
			let post_data = "cancel_order=" + encodeURIComponent(item_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				badgeSet("orders", badgeGet("orders") - 1);
				uk_datatable_rows.splice(item_index, 1);
				uk_datatable_refresh();
			}, function(err){
				console.error(err, SERVER_RESPONSE);
				notify(err, "warning");
			});
		}, null, "Cancel Order", "uk-button-danger");
	});

});
