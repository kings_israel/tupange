$(function(){
	//init map
	let $map_container = $(".x-event-map");
	if ($map_container.length){
		let map_data = $map_container.attr("data-value");
		map_data = map_data.replace(/\'/g, '"');
		if (!(map_data = jsonParse(map_data))) return console.error("Invalid map data!");
		let GOOGLE_MAPS_LOADED = window.hasOwnProperty("google") && "object" === typeof google && google.hasOwnProperty("maps");
		if (!GOOGLE_MAPS_LOADED) return console.error("GOOGLE_MAPS: Google maps library is not available!");
		let $map = null;
		let $map_markers = [];
		let removeMarkers = () => {
			$map_markers.forEach(marker => marker.setMap(null));
			$map_markers = [];
		};
		let addMarker = (latlng, title) => {
			if (!GOOGLE_MAPS_LOADED || !$map){
				console.error("summaryMapAddMarker Error: Google Maps Exception!");
				return false;
			}
			let marker = new google.maps.Marker({
				position: latlng,
				map: $map,
				title: title,
				draggable: false,
			});
			$map_markers.push(marker);
			return true;
		};
		let setLocation = (title, lat, lng) => {
			if (!GOOGLE_MAPS_LOADED || !$map){
				console.error("summaryMapSetLocation Error: Google Maps Exception!");
				return false;
			}
			lat = num(lat, -1.2835201);
			lng = num(lng, 36.8218568);
			let map_center = new google.maps.LatLng(lat, lng);
			removeMarkers();
			addMarker(map_center, title);
			$map.setCenter(map_center);
			return true;
		};
		let initMap = (title, lat, lng) => {
			if (!GOOGLE_MAPS_LOADED) return false;
			if (!($map_container && $map_container.length)){
				console.error("initSummaryMap Error: Unable to find map container!");
				return false;
			}
			let map_container = $map_container[0];
			let map_center = new google.maps.LatLng(lat, lng);
			let map_options = {
				zoom: 16,
				disableDefaultUI: true,
				scrollwheel: false,
				zoomControl: true,
				navigationControl: true,
				mapTypeControl: false,
				scaleControl: true,
				draggable: true,
				center: map_center,
				mapTypeId: "roadmap",
				draggableCursor: "default",
				styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
			};
			$map = new google.maps.Map(map_container, map_options);
			setLocation(str(title, "Event Location", true), lat, lng);
		};

		let title = map_data.name == "Custom" ? "Event Location" : map_data.name;
		onLoad(() => initMap(title, map_data.lat, map_data.lng));
	}
});
