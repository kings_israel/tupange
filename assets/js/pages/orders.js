$(function(){
	//helpers
	//mark as seen ~ user
	let update_user_seen = () => {
		let update_seen = [];
		let $unseen = $("[data-seen='']");
		$unseen.each(function(){
			$row = $(this);
			let order_seen = str($row.attr("data-seen"), "", true);
			let order_id = str($row.attr("data-id"), "", true);
			if (order_id.length && !order_seen.length) update_seen.push(order_id);
		});
		if (!update_seen.length) return;
		let post_url = "";
		let post_data = "update_seen_user=" + encodeURIComponent(update_seen.join(","));
		serverPOST(post_url, post_data, null).then((res) => {
			badgeSet("orders", badgeGet("orders") - update_seen.length);
			$unseen.flashGreen();
		}, (err) => {});
	};

	//mark as seen ~ vendor
	let update_vendor_seen = () => {
		let update_seen = [];
		let $unseen = $("[data-seen-vendor='']");
		$unseen.each(function(){
			$row = $(this);
			let order_seen = str($row.attr("data-seen-vendor"), "", true);
			let order_id = str($row.attr("data-id"), "", true);
			if (order_id.length && !order_seen.length) update_seen.push(order_id);
		});
		if (!update_seen.length) return;
		let post_url = "";
		let post_data = "update_seen_vendor=" + encodeURIComponent(update_seen.join(","));
		serverPOST(post_url, post_data, null).then((res) => {
			badgeSet("orders", badgeGet("orders") - update_seen.length);
			$unseen.flashGreen();
		}, (err) => {});
	};

	//helpers
	let confirm_reason = (title, message, btn_text, btn_class, on_confirm) => {
		let html = '<a class="uk-modal-close uk-close"></a>';
		html += '<div class="uk-modal-header">' + str(title) + '</div>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<form action="javascript:" method="post" class="uk-form x-pad-20">'; //f
		html += '<p>' + str(message) + '</p>';
		html += '<div class="uk-form-row">'; //c
		html += '<label class="uk-form-label" for="reason">Price</label>';
		html += '<div class="uk-form-controls">';
		html += '<textarea id="reason" name="reason" class="uk-width-1-1" placeholder="Enter the reason for your action here"></textarea>';
		html += '</div>';
		html += '</form>'; //f
		html += '</div>'; //1
		html += '<div class="uk-modal-footer uk-text-right">';
		html += '<button type="button" class="uk-button uk-modal-close">Cancel</button> ';
		html += '<button type="button" class="x-btn-confirm uk-button ' + str(btn_class) + ' x-min-100">' + str(btn_text) + '</button> ';
		html += '</div>';
		UKModal(html, 'x-modal x-modal-nopad', function($modal, uk_modal){
			let $reason = $modal.find('#reason');
			let $confirm = $modal.find('.x-btn-confirm');
			$confirm.click(function(event){
				let reason = str($reason.val(), '', true);
				if (!reason.length){
					$reason.inputError('Kindly enter your reason');
					return event.preventDefault();
				}
				if ('function' === typeof on_confirm) on_confirm(reason);
				if (uk_modal.isActive()) uk_modal.hide();
			});
		}, null, null, false, false, true);
	};

	//view badge update
	let view_badge_update = (count) => {
		let badge_type = 'orders-' + ORDERS_VIEW;
		badgeSet(badge_type, badgeGet(badge_type) + num(count));
	};

	//init
	setTimeout(() => {
		update_user_seen();
		update_vendor_seen();
	}, 1000);

	//listeners
	//order remove
	$(document.body).on("click", ".x-table td .item-remove", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		let item_index = -1;
		for (let i = 0; i < uk_datatable_rows.length; i ++){
			if (str($(uk_datatable_rows[i]).attr("data-id"), "", true) == item_id){
				item_index = i;
				break;
			}
		}
		if (item_index < 0){
			console.error("Unable to find item index", item_id);
			return event.preventDefault();
		}
		let prompt = '<h2>Remove Order</h2><p>This will cancel the order and it will be removed from the list permanently.</p><p>Are you sure you want to remove order <strong>"' + item_name + '"</strong>?</p>';
		UKPromptConfirm(prompt, function(){
			let post_url = "";
			let post_data = 'remove_order=' + encodeURIComponent(item_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				uk_datatable_rows.splice(item_index, 1);
				uk_datatable_refresh();
				view_badge_update(-1);
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Remove Order', 'uk-button-danger');
	});

	//order delete
	$(document.body).on("click", ".x-table td .item-delete", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		let item_index = -1;
		for (let i = 0; i < uk_datatable_rows.length; i ++){
			if (str($(uk_datatable_rows[i]).attr("data-id"), "", true) == item_id){
				item_index = i;
				break;
			}
		}
		if (item_index < 0){
			console.error("Unable to find item index", item_id);
			return event.preventDefault();
		}
		let prompt = '<h2>Delete Order</h2><p>Are you sure you want to permanently delete cancelled order <strong>"' + item_name + '"</strong>?</p>';
		UKPromptConfirm(prompt, function(){
			let post_url = "";
			let post_data = 'delete_order=' + encodeURIComponent(item_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				uk_datatable_rows.splice(item_index, 1);
				uk_datatable_refresh();
				view_badge_update(-1);
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Delete Order', 'uk-button-danger');
	});

	//order receive
	$(document.body).on("click", ".x-table td .item-receive", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		let item_index = -1;
		for (let i = 0; i < uk_datatable_rows.length; i ++){
			if (str($(uk_datatable_rows[i]).attr("data-id"), "", true) == item_id){
				item_index = i;
				break;
			}
		}
		if (item_index < 0){
			console.error("Unable to find item index", item_id);
			return event.preventDefault();
		}
		let post_url = "";
		let post_data = 'receive_order=' + encodeURIComponent(item_id);
		serverPOST(post_url, post_data, loading_block).then((res) => {
			$btn.remove();
			$status_td = $('.x-table td[data-status="' + item_id + '"]');
			$status_td.attr('data-value', String(1));
			$status_td.html('Received');
		}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
	});

	//order cancel
	$(document.body).on("click", ".x-table td .item-cancel", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		let item_index = -1;
		for (let i = 0; i < uk_datatable_rows.length; i ++){
			if (str($(uk_datatable_rows[i]).attr("data-id"), "", true) == item_id){
				item_index = i;
				break;
			}
		}
		if (item_index < 0){
			console.error("Unable to find item index", item_id);
			return event.preventDefault();
		}
		confirm_reason('Cancel Order', 'Cancelled orders will be removed from your open orders list permanently. Are you sure you want to cancel order <strong>"' + item_name + '"</strong>?', 'Cancel Order', 'uk-button-danger', function(reason){
			let post_url = "";
			let post_data = objectParams({
				cancel_order: item_id,
				reason: reason,
			});
			serverPOST(post_url, post_data, loading_block).then((res) => {
				uk_datatable_rows.splice(item_index, 1);
				uk_datatable_refresh();
				view_badge_update(-1);
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		});
	});

	//order decline
	$(document.body).on("click", ".x-table td .item-decline", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr("data-id"), "", true);
		let item_name = str($btn.attr("data-name"), "", true);
		if (!item_id.length || !item_name.length) return event.preventDefault();
		let item_index = -1;
		for (let i = 0; i < uk_datatable_rows.length; i ++){
			if (str($(uk_datatable_rows[i]).attr("data-id"), "", true) == item_id){
				item_index = i;
				break;
			}
		}
		if (item_index < 0){
			console.error("Unable to find item index", item_id);
			return event.preventDefault();
		}
		confirm_reason('Decline Order', 'Declined orders will be removed from your client orders list permanently. Are you sure you want to decline order <strong>"' + item_name + '"</strong>?', 'Decline Order', 'uk-button-danger', function(reason){
			let post_url = "";
			let post_data = objectParams({
				decline_order: item_id,
				reason: reason,
			});
			serverPOST(post_url, post_data, loading_block).then((res) => {
				uk_datatable_rows.splice(item_index, 1);
				uk_datatable_refresh();
				view_badge_update(-1);
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		});
	});

	//order delivered
	$('.x-mark-delivered').click(function(){
		let order = ORDER_DATA;
		UKPromptConfirm('<h2>Delivered Order</h2><p>Do you want to mark this order as delivered? The client will be able to dispute or mark delivered orders as complete.<br><strong>Note: This action cannot be undone!</strong></p>', () => {
			let post_url = "";
			let post_data = 'deliver_order=' + encodeURIComponent(order.id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
				window.location.reload();
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Delivered', 'uk-button-success');
	});

	//order overdue
	$('.x-mark-overdue').click(function(){
		let order = ORDER_DATA;
		UKPromptConfirm('<h2 class="uk-text-danger">Overdue Order</h2><p class="uk-text-danger">Do you want to mark this order as overdue? Overdue orders can be disputed for reimbursement.<br><strong>Note: This action cannot be undone!</strong></p>', () => {
			let post_url = "";
			let post_data = 'overdue_order=' + encodeURIComponent(order.id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
				window.location.reload();
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Overdue', 'uk-button-danger');
	});

	//order complete
	$('.x-mark-complete').click(function(){
		let order = ORDER_DATA;
		UKPromptConfirm('<h2>Complete Order</h2><p>Do you want to mark this order as complete? You can rate and review complete orders.<br><strong>Note: This action cannot be undone!</strong></p>', () => {
			let post_url = "";
			let post_data = 'complete_order=' + encodeURIComponent(order.id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
				window.location.reload();
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		}, null, 'Complete', 'uk-button-success');
	});

	//order dispute
	$('.x-mark-dispute').click(function(event){
		let order = ORDER_DATA;
		confirm_reason('Dispute Order', 'Are you sure you want to dispute delivered order? Disputed orders allows for order status review and action.', 'Dispute', 'uk-button-danger', function(reason){
			let post_url = "";
			let post_data = objectParams({
				dispute_order: order.id,
				reason: reason,
			});
			serverPOST(post_url, post_data, loading_block).then((res) => {
				loading_block(true, '<i class="uk-icon-spinner uk-icon-spin"></i> Refreshing...');
				window.location.reload();
			}, (err) => UKPrompt('<p class="uk-text-danger">' + err + '</p>'));
		});
	});

});
$(function(){
	let $review_comments = $(".x-review-comments");
	let $review_comments_toggle = $(".x-review-comments-toggle");
	$review_comments_toggle.click(function(){
		if ($review_comments.length){
			if ($review_comments[0].className.indexOf("expand") > -1){
				$review_comments.removeClass("expand");
				$review_comments_toggle.html("Show All");
			}
			else {
				$review_comments.addClass("expand");
				$review_comments_toggle.html("Show Less");
			}
		}
	});
});
