if (!(window.hasOwnProperty("EVENT_DATA") && isObj(EVENT_DATA, true))) window["EVENT_DATA"] = {};
if (!(window.hasOwnProperty("BUDGET_DATA") && isObj(BUDGET_DATA, true))) window["BUDGET_DATA"] = {};
let get_budget = (id, return_index) => {
	for (let i = 0; i < BUDGET_DATA.length; i ++){
		let budget = BUDGET_DATA[i];
		if (budget.id === id) return return_index ? i : budget;
	}
	return null;
};
let get_budget_transaction = (budget_id, transaction_id, return_index) => {
	let budget_index = get_budget(budget_id, true);
	if (budget_index !== null){
		let budget = BUDGET_DATA[budget_index];
		if (hasKeys(budget, 'transactions') && Array.isArray(budget.transactions)){
			for (let i = 0; i < budget.transactions.length; i ++){
				let transaction = budget.transactions[i];
				if (transaction.id == transaction_id) return return_index ? [budget_index, i] : transaction;
			}
		}
	}
	return null;
};

//budget groups
$(function(){
	//controls ~ budget
	let $budget_groups_content = $('#groups-toggle-content');
	let $groups_search = $budget_groups_content.find('.x-groups-search');
	let $groups_list = $budget_groups_content.find('.x-budget-groups');
	let $groups_add_wrapper = $budget_groups_content.find('.x-group-add');
	let $groups_add = $groups_add_wrapper.find('button');
	let $budget_form = $budget_groups_content.find('.x-group-edit');
	let $budget_id = $budget_form.find("#budget_id");
	let $budget_title = $budget_form.find("#budget_title");
	let $budget_description = $budget_form.find("#budget_description");
	let $budget_cancel = $budget_form.find(".x-group-edit-cancel");
	let $budget_save = $budget_form.find(".x-group-edit-save");

	//contols ~ display
	let $get_started = $('.x-get-started');
	let $budget_display = $('.x-budget-display');
	let $display_info = $budget_display.find('.x-display-info');
	let $summary_total = $budget_display.find('.x-summary-total');
	let $summary_spent = $budget_display.find('.x-summary-spent');
	let $summary_balance = $budget_display.find('.x-summary-balance');
	let $transactions_search = $budget_display.find('.x-search-transactions');
	let $transactions_toggle = $budget_display.find('.x-transactions-toggle');
	let $transactions_table = $budget_display.find('.x-transactions-table');
	let $transactions_table_body = $transactions_table.find('table tbody');
	let $transactions_add_wrapper = $budget_display.find('.x-transactions-add');
	let $transactions_add = $transactions_add_wrapper.find('button');
	let $transaction_form = $budget_display.find('#transaction_form');
	let $transaction_budget_id = $transaction_form.find('#transaction_budget_id');
	let $transaction_id = $transaction_form.find('#transaction_id');
	let $transaction_date = $transaction_form.find('#transaction_date');
	let $transaction_type = $transaction_form.find('#transaction_type');
	let $transaction_title = $transaction_form.find('#transaction_title');
	let $transaction_description = $transaction_form.find('#transaction_description');
	let $transaction_amount = $transaction_form.find('#transaction_amount');
	let $transaction_reference = $transaction_form.find('#transaction_reference');
	let $transaction_edit_cancel = $transaction_form.find('.x-transaction-edit-cancel');
	let $transaction_edit_save = $transaction_form.find('.x-transaction-edit-save');

	//helpers ~ display
	let DISPLAY_BUDGET = null;
	let DISPLAY_TRANSACTIONS_QUERY = '';
	let DISPLAY_TRANSACTIONS_TOGGLE = '';
	let budget_group_edit;
	let transaction_row_html = (no, budget_id, data) => {
		if (!(budget_id = str(budget_id, '', true)).length) throw 'Invalid budget reference';
		if (!hasKeys(data, 'id', 'date', 'title', 'debit', 'credit', 'balance', 'type_name', 'reference', 'description', 'timestamp')) throw 'Invalid budget transaction object';
		let html = '';
		html += '<tr data-id="' + data.id + '" data-budget="' + budget_id + '">';
		html += '<td class="x-nowrap">' + no + '</td>';
		html += '<td class="x-nowrap">' + data.date + '</td>';
		html += '<td class="x-nowrap">' + data.title + '</td>';
		html += '<td class="x-nowrap">' + data.debit + '</td>';
		html += '<td class="x-nowrap">' + data.credit + '</td>';
		html += '<td class="x-nowrap">' + data.balance + '</td>';
		html += '<td class="x-nowrap">' + data.type_name + '</td>';
		html += '<td class="x-nowrap">' + data.reference + '</td>';
		html += '<td class="x-min-150">' + data.description + '</td>';
		html += '<td class="x-nowrap">' + data.timestamp + '</td>';
		html += '<td class="x-nowrap uk-text-center">';
		html += '<a title="Edit" draggable="false" href="javascript:" data-id="' + data.id + '" data-budget="' + budget_id + '" class="transaction-edit uk-button uk-button-small uk-button-white"><i class="uk-icon-pencil"></i></a> ';
		html += '<a title="Delete" draggable="false" href="javascript:" data-id="' + data.id + '" data-budget="' + budget_id + '" class="transaction-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a> ';
		html += '</td>';
		html += '</tr>';
		return html;
	};
	let display_transactions = () => {
		let budget = DISPLAY_BUDGET;
		let query = str(DISPLAY_TRANSACTIONS_QUERY, '', true);
		let toggle = str(DISPLAY_TRANSACTIONS_TOGGLE, '', true);
		if (!hasKeys(budget, 'id', 'transactions')){
			console.error('Invalid display transactions budget object!', budget);
			return false;
		}
		$transactions_table_body.html('');
		if (Array.isArray(budget.transactions)){
			for (let i = 0; i < budget.transactions.length; i ++){
				let item = budget.transactions[i];
				let show_item = true;
				if (query.length && (item.title + ' ' + item.description).toLowerCase().indexOf(query.toLowerCase()) < 0) show_item = false;
				else if (toggle.length && String(item.type) != toggle) show_item = false;
				if (!show_item) continue;
				let item_html = transaction_row_html(i + 1, budget.id, item);
				let $item = $(item_html);
				$transactions_table_body.append($item);
				if (item.type == 1) $item.addClass('credit');
			}
		}
		return true;
	};
	let transaction_form_clear = () => {
		$transaction_budget_id.val(propval(DISPLAY_BUDGET, 'id'));
		$transaction_id.val('');
		$transaction_date.val('');
		$transaction_type.val('0');
		$transaction_title.val('');
		$transaction_description.val('');
		$transaction_amount.val('');
		$transaction_reference.val('');
	};
	let transaction_form_toggle = (show) => {
		transaction_form_clear();
		if (show){
			$transactions_add_wrapper.addClass('x-hidden');
			$transaction_form.removeClass('x-hidden');
			$transaction_form.scrollToMe();
			$transaction_form.flashGreen(1, 200);
		}
		else {
			$transactions_add_wrapper.removeClass('x-hidden');
			$transaction_form.addClass('x-hidden');
		}
	};
	let display_budget = (item_id) => {
		item_id = str(item_id, '', true);
		if (item_id.length) DISPLAY_BUDGET = get_budget(item_id);
		else if (!DISPLAY_BUDGET && BUDGET_DATA.length) DISPLAY_BUDGET = BUDGET_DATA[0];
		let budget = DISPLAY_BUDGET;
		if (!hasKeys(budget, 'id', 'title', 'description')){
			$get_started.removeClass('x-hidden');
			$budget_display.addClass('x-hidden');
			return false;
		}
		let info_html = '<h2>' + budget.title + '</h2>';
		if (budget.description.length) info_html += '<p>' + budget.description + '</p>';
		$display_info.html(info_html);
		$summary_total.html(budget.total.commas(2));
		$summary_spent.html(budget.spent.commas(2));
		$summary_balance.html(budget.balance.commas(2));
		$transactions_search.val('');
		$transactions_toggle.find('input[type="radio"]').groupVal('');
		transaction_form_toggle();
		DISPLAY_TRANSACTIONS_QUERY = '';
		DISPLAY_TRANSACTIONS_TYPE = '';
		display_transactions();
		$get_started.addClass('x-hidden');
		$budget_display.removeClass('x-hidden');
		$groups_list.find('li').removeClass('active');
		let $list_item = $groups_list.find('li[data-id="' + budget.id + '"]');
		if ($list_item.length) $list_item.addClass('active');
	};
	let transaction_form_submit = (event) => {
		let budget_id = str($transaction_budget_id.val(), '', true);
		let id = str($transaction_id.val(), '', true);
		let date = str($transaction_date.val(), '', true);
		let type = num($transaction_type.val());
		let title = str($transaction_title.val(), '', true);
		let description = str($transaction_description.val(), '', true);
		let amount = num($transaction_amount.val());
		let reference = str($transaction_reference.val(), '', true);
		if (!title.length){
			$transaction_title.inputError('Kindly enter the transaction title', true);
			return event.preventDefault();
		}
		let post_url = '';
		let post_data = objectParams({
			edit_budget_transaction: id,
			budget_id: budget_id,
			date: date,
			type: type,
			title: title,
			description: description,
			amount: amount,
			reference: reference,
		});
		serverPOST(post_url, post_data, loading_block).then((res) => {
			notify(res.message, 'success');
			let res_budget = res.data.budget;
			let res_id = res.data.id;
			let budget_index = get_budget(res_budget.id, true);
			if (budget_index === null){
				console.error('Unable to find edit budget index');
				return;
			}
			else BUDGET_DATA[budget_index] = res_budget;
			budget_group_edit(res_budget);
			let $item_row = $transactions_table_body.find('tr[data-id="' + res_id + '"]');
			if (!$item_row.length){
				console.error('Unable to find edit transaction row');
				return;
			}
			$item_row.scrollToMe();
			$item_row.flashGreen(1, 200);
		}, (err) => {
			UKPrompt('<p class="uk-text-danger">' + err + '</p>');
			console.error(SERVER_RESPONSE);
		});
	};

	//listeners ~ display
	$transactions_search.on('input', function(event){
		DISPLAY_TRANSACTIONS_QUERY = str($(this).val(), '', true);
		display_transactions();
	});
	$transactions_toggle.find('input[type="radio"]').on('change', function(event){
		DISPLAY_TRANSACTIONS_TOGGLE = str($(this).groupVal(), '', true);
		display_transactions();
	});
	$transaction_form.submit((event) => event.preventDefault());
	$transactions_add.click(() => transaction_form_toggle(true));
	$transaction_edit_cancel.click(() => transaction_form_toggle());
	$transaction_edit_save.click((event) => transaction_form_submit(event));
	$(document.body).on('click', '.x-budget-display table tbody td a.transaction-edit', function(event){
		let $btn = $(this);
		let $row = $btn.parent().parent();
		let id = str($btn.attr('data-id'), '', true);
		let budget_id = str($btn.attr('data-budget'), '', true);
		let transaction = get_budget_transaction(budget_id, id);
		if (!hasKeys(transaction, 'id', 'title', 'description')){
			console.error('Unable to get transaction data');
			event.preventDefault();
			return false;
		}
		let tmp_date_parts = transaction.date.trim().split('-'); //2019-01-31 --> [2019, 01, 31]
		let tmp_date = tmp_date_parts.length == 3 ? tmp_date_parts[2] + '/' + tmp_date_parts[1] + '/' + tmp_date_parts[0] : ''; //31/01/2019
		transaction_form_toggle(true);
		$transaction_budget_id.val(budget_id);
		$transaction_id.val(id);
		$transaction_date.val(tmp_date);
		$transaction_type.val(String(transaction.type));
		$transaction_title.val(transaction.title);
		$transaction_description.val(transaction.description);
		$transaction_amount.val(String(transaction.amount));
		$transaction_reference.val(transaction.reference);
		return false;
	});
	$(document.body).on('click', '.x-budget-display table tbody td a.transaction-delete', function(event){
		let $btn = $(this);
		let $row = $btn.parent().parent();
		let id = str($btn.attr('data-id'), '', true);
		let budget_id = str($btn.attr('data-budget'), '', true);
		let transaction_indexes = get_budget_transaction(budget_id, id, true);
		if (!(Array.isArray(transaction_indexes) && transaction_indexes.length == 2)){
			console.error('Unable to get transaction indexes');
			event.preventDefault();
			return false;
		}
		let budget_index = transaction_indexes[0];
		let transaction_index = transaction_indexes[1];
		let transaction = propval(BUDGET_DATA, budget_index, 'transactions', transaction_index);
		if (!hasKeys(transaction, 'id', 'title', 'description')){
			console.error('Unable to get transaction data');
			event.preventDefault();
			return false;
		}
		let prompt = '<h2 class="uk-text-danger">Delete Transaction</h2><p class="uk-text-danger">Are you sure you want to <strong>permanently delete</strong> transaction "' + transaction.title + '"?</p>';
		UKPromptConfirm(prompt, function(){
			let post_url = '';
			let post_data = 'delete_budget_transaction=' + encodeURIComponent(id) + '&budget_id=' + encodeURIComponent(budget_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				notify(res.message, 'success');
				transaction_form_toggle();
				let budget_index = get_budget(res.data.id, true);
				if (budget_index === null){
					console.error('Unable to find edit budget index');
					return;
				}
				else BUDGET_DATA[budget_index] = res.data;
				budget_group_edit(res.data);
			}, (err) => {
				UKPrompt('<p class="uk-text-danger">' + err + '</p>');
				console.error(SERVER_RESPONSE);
			});
		}, null, 'Delete Transaction', 'uk-button-danger');
		return false;
	});

	//helpers ~ budget
	let budget_item_html = (data) => {
		if (!hasKeys(data, 'id', 'title', 'description', 'balance', 'timestamp')) throw 'Invalid budget object!';
		let item_search = [data.title, data.description].join(' ').trim();
		let html = '';
		html += '<li tabindex="0" data-id="' + data.id + '" data-search="' + item_search + '">';
		html += '<div class="x-actions">';
		html += '<a href="javascript:" title="Edit" draggable="false" data-id="' + data.id + '" class="group-edit uk-text-primary"><i class="uk-icon-pencil"></i></a> ';
		html += '<a href="javascript:" title="Delete" draggable="false" data-id="' + data.id + '" class="group-delete uk-text-danger"><i class="uk-icon-trash"></i></a> ';
		html += '</div>';
		html += '<h3>' + data.title + '</h3>';
		html += '<small class="x-nowrap">' + data.timestamp + ' | <strong>Bal:</strong> ' + data.balance.commas(2) + '</small>';
		html += '</li>';
		return html;
	};
	budget_group_edit = (data, scroll_to_me) => {
		if (!hasKeys(data, 'id', 'title', 'description', 'balance', 'timestamp')) throw 'Invalid budget object!';
		let item_id = data.id;
		let budget_index = get_budget(item_id, true);
		if (budget_index !== null) BUDGET_DATA[budget_index] = data;
		else BUDGET_DATA.push(data);
		let item_html = budget_item_html(data);
		let $list_item = $groups_list.find('li[data-id="' + item_id + '"]');
		let $item = $(item_html);
		if ($list_item.length){
			let item_class = $list_item[0].className;
			$list_item.replaceWith($item);
			$item.addClass(item_class);
		}
		else $groups_list.append($item);
		if (scroll_to_me){
			$item.scrollToMe();
			$item.flashGreen(1, 200);
		}
		display_budget(item_id);
	};
	let budget_group_remove = (item_id) => {
		let budget_index = get_budget(item_id, true);
		if (budget_index !== null) BUDGET_DATA.splice(budget_index, 1);
		let $list_item = $groups_list.find('li[data-id="' + item_id + '"]');
		if ($list_item.length) $list_item.remove();
		if (DISPLAY_BUDGET && DISPLAY_BUDGET.id == item_id) DISPLAY_BUDGET = null;
		display_budget();
	};
	let budget_form_clear = () => {
		$budget_id.val('');
		$budget_title.val('');
		$budget_description.val('');
	};
	let budget_form_toggle = (show) => {
		budget_form_clear();
		if (show){
			$groups_add_wrapper.addClass('x-hidden');
			$budget_form.removeClass('x-hidden');
			$budget_form.scrollToMe();
			$budget_form.flashGreen(1, 200);
		}
		else {
			$budget_form.addClass('x-hidden');
			$groups_add_wrapper.removeClass('x-hidden');
		}
	};
	let budget_form_submit = (event) => {
		let id = str($budget_id.val(), '', true);
		let title = str($budget_title.val(), '', true);
		let description = str($budget_description.val(), '', true);
		if (!title.length){
			$budget_title.inputError('Kindly provide the budget title.', true);
			return event.preventDefault();
		}
		let post_url = '';
		let post_data = objectParams({
			edit_budget: id,
			title: title,
			description: description,
		});
		serverPOST(post_url, post_data, loading_block).then((res) => {
			budget_form_toggle();
			notify(res.message, 'success');
			budget_group_edit(res.data, true);
		}, (err) => {
			UKPrompt('<p class="uk-text-danger">' + err + '</p>');
			console.error(SERVER_RESPONSE);
		});
	};

	//listeners ~ budget
	$groups_search.on('input', function(event){
		let query = str($(this).val(), '', true);
		$groups_list.children().each(function(index){
			let $li = $(this);
			let item_search = str($li.attr('data-search'), '', true);
			let show_item = true;
			if (query.length && item_search.toLowerCase().indexOf(query.toLowerCase()) < 0) show_item = false;
			if (show_item) $li.removeClass('x-display-none');
			else $li.addClass('x-display-none');
		});
	});
	$budget_form.submit((event) => event.preventDefault());
	$groups_add.click(() => budget_form_toggle(true));
	$budget_cancel.click(() => budget_form_toggle());
	$budget_save.click((event) => budget_form_submit(event));
	$(document.body).on("click", "#groups-toggle-content .x-budget-groups li .x-actions a.group-edit", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr('data-id'), '', true);
		let budget = get_budget(item_id);
		if (!hasKeys(budget, 'id', 'title', 'description')){
			console.error('Unable to get budget data');
			event.preventDefault();
			return false;
		}
		budget_form_toggle(true);
		$budget_id.val(budget.id);
		$budget_title.val(budget.title);
		$budget_description.val(budget.description);
		return false;
	});
	$(document.body).on("click", "#groups-toggle-content .x-budget-groups li .x-actions a.group-delete", function(event){
		let $btn = $(this);
		let item_id = str($btn.attr('data-id'), '', true);
		let budget = get_budget(item_id);
		if (!hasKeys(budget, 'id', 'title', 'description')){
			console.error('Unable to get budget data');
			event.preventDefault();
			return false;
		}
		let prompt = '<h2 class="uk-text-danger">Delete Budget</h2><p class="uk-text-danger">Are you sure you want to <strong>permanently delete</strong> budget "' + budget.title + '"? <strong>This will also delete related transactions.</strong></p>';
		UKPromptConfirm(prompt, function(){
			let post_url = '';
			let post_data = 'delete_budget=' + encodeURIComponent(item_id);
			serverPOST(post_url, post_data, loading_block).then((res) => {
				notify(res.message, 'success');
				budget_form_toggle();
				budget_group_remove(item_id);
			}, (err) => {
				UKPrompt('<p class="uk-text-danger">' + err + '</p>');
				console.error(SERVER_RESPONSE);
			});
		}, null, 'Delete Budget', 'uk-button-danger');
		return false;
	});
	$(document.body).on("click", "#groups-toggle-content .x-budget-groups li", function(event){
		let $li = $(this);
		let item_id = str($li.attr('data-id'), '', true);
		let budget = get_budget(item_id);
		if (!hasKeys(budget, 'id', 'title', 'description')){
			console.error('Unable to get budget data');
			event.preventDefault();
			return false;
		}
		display_budget(item_id);
		if (!isUKLarge()) toggleGroupsContent();
		return false;
	});

	//show first budget
	if (BUDGET_DATA.length) display_budget(propval(BUDGET_DATA, 0, 'id'));
});
