$(function(){
	//event settings (pre-loaded before script)
	if (!(window.hasOwnProperty("EVENT_DATA") && isObj(EVENT_DATA, true))) window["EVENT_DATA"] = {};
	if (!(window.hasOwnProperty("EVENT_SETTINGS") && isObj(EVENT_SETTINGS, true))) window["EVENT_SETTINGS"] = {};
	if (!(window.hasOwnProperty("EVENT_GUEST_GROUPS") && isObj(EVENT_GUEST_GROUPS, true))) window["EVENT_GUEST_GROUPS"] = {};
	if (!(window.hasOwnProperty("EVENT_INVITE_MODES") && isObj(EVENT_INVITE_MODES, true))) window["EVENT_INVITE_MODES"] = [];

	let sending_invitations;

	//form elements
	$add_guest_wrapper = $(".x-edit-guest-toggle");
	$add_guest = $(".x-edit-guest-toggle button");
	$guest_form = $("#guest_form");
	$guest_cancel = $("#guest_cancel");
	$guest_cancel_mini = $("#guest_cancel_mini");
	$guest_save = $("#guest_save");
	$guest_save_mini = $("#guest_save_mini");
	$guest_saving = $("#guest_saving");
	$guest_error = $("#guest_error");
	$extend_inv = $("[name='guest_extend_inv']");
	$extend_inv_toggle = $(".x-extend_inv-toggle");

	//guest form map
	let GUEST_FORM_MAP = {
		id: "guest_id",
		first_name: "guest_first_name",
		last_name: "guest_last_name",
		type: "guest_type",
		group_id: "guest_group",
		phone: "guest_phone",
		email: "guest_email",
		company: "guest_company",
		address: "guest_address",
		diet: "guest_diet",
		table_no: "guest_table_number",
		extend_inv: "guest_extend_inv",
		expected_guests: "guest_expected",
		inv_phone: "guest_inv_phone",
		inv_email: "guest_inv_email",
		inv_message: "guest_inv_message",
		inv_question: "guest_inv_question",
		inv_answer: "guest_inv_answer",
		inv_edits: "guest_inv_edits",
	};

	//helper functions ~ form
	function toggleForm(show, guest_data){
		let focus_input = $("[name='guest_first_name']");
		setFormData(guest_data);
		if (show){
			$guest_form.find("small.uk-text-danger").remove();
			$guest_form.find(".uk-form-danger").removeClass("uk-form-danger");
			$add_guest_wrapper.hide();
			$guest_form.removeClass("x-hidden");
			if ($extend_inv.length && $extend_inv[0].checked) $extend_inv_toggle.show();
			else $extend_inv_toggle.hide();
			$guest_form.scrollToMe();
			emitResize();
			focus_input.focus();
		}
		else {
			$guest_form.addClass("x-hidden");
			$add_guest_wrapper.show();
		}
		return true;
	}
	function setFormData(guest_data){
		let guest_data_valid = guest_data && hasKeys.apply(null, [guest_data].concat(Object.keys(GUEST_FORM_MAP)));
		for (let key in GUEST_FORM_MAP){
			if (GUEST_FORM_MAP.hasOwnProperty(key)){
				let name = GUEST_FORM_MAP[key];
				let value = guest_data_valid ? guest_data[key] : "";
				let input = $guest_form.find("[name='" + name + "']");
				if (input.length){
					let input_default = input[0].hasAttribute("data-default") ? str(input.attr("data-default"), "") : "";
					if (!value && input_default) value = input_default;
					let node_name = input[0].nodeName.toLowerCase();
					let node_type = input[0].type.toLowerCase();
					if (["hidden", "text", "number", "password", "textarea"].indexOf(node_type) > -1) input.val(value + "");
					else if (node_name == "select") input.val(value + "");
					else if (node_type == "radio") input.groupVal(value, true);
					else if (node_type == "checkbox") input.setChecked(value && value != "0", true);
				}
			}
		}
		return true;
	}
	function getUKDatatableRowIndex(row_id){
		let index = -1;
		if (Array.isArray(window["uk_datatable_rows"])){
			for (let i = 0; i < uk_datatable_rows.length; i ++){
				let uk_datatable_row = uk_datatable_rows[i];
				if (uk_datatable_row.id == row_id){
					index = i;
					break;
				}
			}
		}
		return index;
	}
	function setTableRow(data, flash_green){
		if (!(data && hasKeys.apply(null, [data].concat(Object.keys(GUEST_FORM_MAP))))){
			console.error("Invalid guest data", data);
			return false;
		}
		//row vars
		let id = escapeHTML(data.id, true);
		let data_search = escapeHTML([data.names, data.phone, data.email, data.address, data.type, data.group_name].join(" "), true);
		let data_base64 = escapeHTML(btoa(stringify(data)), true);
		let $updated_row = null;

		//check existing
		let existing_id = "guest-" + data.id;
		let $existing_row = $("#" + existing_id);
		let uk_datatable_row_index = getUKDatatableRowIndex(existing_id);

		//set row data
		if ($existing_row.length){
			//update existing
			let update_existing_row = (row_td, data_html, data_value) => {
				$row_td = $(row_td);
				if ($row_td.length){
					data_html = escapeHTML(data_html, true);
					data_value = escapeHTML(data_value, true);
					$row_td.attr("data-value", data_value);
					$row_td.html(data_html);
				}
			};
			update_existing_row($existing_row.find("td.uk-datatable-row-index"), "", "");
			update_existing_row($existing_row.find("td[data-name='barcode']"), data.barcode, data.barcode);
			update_existing_row($existing_row.find("td[data-name='type_name']"), data.type_name, data.type_name);
			update_existing_row($existing_row.find("td[data-name='group_name']"), data.group_name, data.group_name);
			update_existing_row($existing_row.find("td[data-name='names']"), data.names, data.names);
			update_existing_row($existing_row.find("td[data-name='phone']"), data.phone, data.phone);
			update_existing_row($existing_row.find("td[data-name='email']"), data.email, data.email);
			update_existing_row($existing_row.find("td[data-name='address']"), data.address, data.address);
			update_existing_row($existing_row.find("td[data-name='company']"), data.company, data.company);
			update_existing_row($existing_row.find("td[data-name='table_no']"), data.table_no, data.table_no);
			update_existing_row($existing_row.find("td[data-name='diet']"), data.diet, data.diet);
			update_existing_row($existing_row.find("td[data-name='expected_guests']"), data.expected_guests, data.expected_guests);
			update_existing_row($existing_row.find("td[data-name='inv_contacts']"), data.inv_contacts, data.inv_contacts);
			update_existing_row($existing_row.find("td[data-name='inv_message']"), data.inv_message, data.inv_message);
			update_existing_row($existing_row.find("td[data-name='inv_qa']"), data.inv_qa, data.inv_qa);
			update_existing_row($existing_row.find("td[data-name='status_sms']"), data.status_sms, data.status_sms);
			update_existing_row($existing_row.find("td[data-name='status_email']"), data.status_email, data.status_email);
			update_existing_row($existing_row.find("td[data-name='status_whatsapp']"), data.status_whatsapp, data.status_whatsapp);
			update_existing_row($existing_row.find("td[data-name='status_fb']"), data.status_fb, data.status_fb);
			update_existing_row($existing_row.find("td[data-name='status_twitter']"), data.status_twitter, data.status_twitter);
			update_existing_row($existing_row.find("td[data-name='status_text']"), data.status_text, data.status);
			update_existing_row($existing_row.find("td[data-name='time_created']"), data.time_created, data.time_created);
			update_existing_row($existing_row.find("td[data-name='time_modified']"), data.time_modified, data.time_modified);
			update_existing_row($existing_row.find("td[data-name='time_invited']"), data.time_invited, data.time_invited);
			update_existing_row($existing_row.find("td[data-name='time_confirmed']"), data.time_confirmed, data.time_confirmed);
			$existing_row.attr("data-search", data_search);
			$existing_row.attr("data-toggle", data.status);
			$existing_row.attr("data-base64", data_base64);

			//flash_green
			if (flash_green) $existing_row.flashGreen();

			//set updated row
			$updated_row = $existing_row[0];
		}
		else {
			//new row data
			let has_setting = (name, value) => {
				return "object" === typeof EVENT_SETTINGS && EVENT_SETTINGS && EVENT_SETTINGS.hasOwnProperty(name) && (EVENT_SETTINGS[name] == value || num(EVENT_SETTINGS[name]) == value);
			};
			let has_mode = (value) => {
				return Array.isArray(EVENT_INVITE_MODES) && EVENT_INVITE_MODES.indexOf(value) >= 0;
			};
			let html_row = (data_html, class_name, data_name, data_value) => {
				data_html = escapeHTML(data_html, true);
				class_name = escapeHTML(class_name, true);
				data_name = escapeHTML(data_name, true);
				data_value = escapeHTML(data_value, true);
				let temp_html = '<td';
				temp_html += class_name.length ? ' class="' + class_name + '"' : '';
				temp_html += data_name.length ? ' data-name="' + data_name + '"' : '';
				temp_html += data_value.length ? ' data-value="' + data_value + '"' : '';
				temp_html += '>' + data_html + "</td>";
				return temp_html;
			};
			let html = '<tr id="guest-' + id + '" data-id="' + id + '" data-search="' + data_search + '" data-toggle="' + data.status + '" data-base64="' + data_base64 + '">';
			html += html_row("", "x-nowrap x-min-50 uk-datatable-row-index");
			html += html_row(data.barcode, "x-nowrap x-min-100", "barcode", data.barcode);
			if (has_setting("type", 1)) html += html_row(data.type_name, "x-nowrap x-min-100", "type_name", data.type_name);
			if (Object.keys(EVENT_GUEST_GROUPS).length) html += html_row(data.group_name, "x-nowrap x-min-100", "group_name", data.group_name);
			html += html_row(data.names, "x-nowrap x-min-100", "names", data.names);
			if (has_setting("phone", 1)) html += html_row(data.phone, "x-nowrap", "phone", data.phone);
			if (has_setting("email", 1)) html += html_row(data.email, "x-nowrap", "email", data.email);
			if (has_setting("address", 1)) html += html_row(data.address, "x-min-150", "address", data.address);
			if (has_setting("company", 1)) html += html_row(data.company, "x-min-100", "company", data.company);
			if (has_setting("table", 1)) html += html_row(data.table_no, "x-nowrap", "table_no", data.table_no);
			if (has_setting("diet", 1)) html += html_row(data.diet, "x-min-150", "diet", data.diet);
			if (has_setting("extend", 1)) html += html_row(data.extend_inv_text, "x-nowrap", "extend_inv_text", data.extend_inv);
			if (has_setting("extend", 1)) html += html_row(data.expected_guests, "x-nowrap", "expected_guests", data.expected_guests);
			if (EVENT_INVITE_MODES.length) html += html_row(data.inv_contacts, "x-min-150", "inv_contacts", data.inv_contacts);
			html += html_row(data.inv_message, "x-min-150", "inv_message", data.inv_message);
			if (has_setting("pass_question", 1)) html += html_row(data.inv_qa, "x-min-150", "inv_qa", data.inv_qa);
			if (has_mode("sms")) html += html_row(data.status_sms, "x-min-100", "status_sms", data.status_sms);
			if (has_mode("email")) html += html_row(data.status_email, "x-min-100", "status_email", data.status_email);
			if (has_mode("whatsapp")) html += html_row(data.status_whatsapp, "x-min-100", "status_whatsapp", data.status_whatsapp);
			if (has_mode("facebook")) html += html_row(data.status_fb, "x-min-100", "status_fb", data.status_fb);
			if (has_mode("twitter")) html += html_row(data.status_twitter, "x-min-100", "status_twitter", data.status_twitter);
			html += html_row(data.time_created, "x-nowrap x-min-100", "time_created", data.time_created);
			html += html_row(data.time_modified, "x-nowrap x-min-100", "time_modified", data.time_modified);
			html += html_row(data.time_invited, "x-nowrap x-min-100", "time_invited", data.time_invited);
			html += html_row(data.time_confirmed, "x-nowrap x-min-100", "time_confirmed", data.time_confirmed);
			html += html_row(data.status_text, "x-nowrap x-min-100", "status_text", data.status);
			html += html_row(data.names, "x-nowrap x-min-100 x-italics x-color-b");
			html += '<td class="uk-text-center"><div class="uk-form">';
			html += '<label class="x-checkbox"><input class="x-bulk-action" data-id="' + id + '" type="checkbox"></label>';
			html += '</div></td>';
			html += '<td class="x-nowrap uk-text-center">';
			html += '<a title="Preview" draggable="false" href="javascript:" data-id="' + id + '" class="guest-preview uk-button uk-button-small uk-button-white"><i class="uk-icon-send"></i> Invitation</a> ';
			html += '<a title="Edit" draggable="false" href="javascript:" data-id="' + id + '" data-status="' + data.status + '" class="guest-edit uk-button uk-button-small uk-button-primary"><i class="uk-icon-pencil"></i></a> ';
			html += '<a title="Delete" draggable="false" href="javascript:" data-id="' + id + '" class="guest-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>';
			html += '</td>';
			html += '</tr>';

			//set updated row
			$updated_row = $(html)[0];
		}

		//update & refresh uk-datatable
		if (!$updated_row){
			console.error("Error updating row!", $updated_row);
			return false;
		}
		if (uk_datatable_row_index >= 0) uk_datatable_rows[uk_datatable_row_index] = $updated_row;
		else uk_datatable_rows.unshift($updated_row);
		if ("function" === typeof uk_datatable_refresh) uk_datatable_refresh();
		return true;
	}

	//on inv toggle
	if ($extend_inv.length) $extend_inv.on("change", function(){
		setTimeout(function(){
			if ($extend_inv[0].checked) $extend_inv_toggle.show();
			else $extend_inv_toggle.hide();
		}, 50);
	});

	//on edit guest
	$(document.body).on("click", "td > .guest-edit", function(event){
		let guest_id = str($(this).attr("data-id"), "", true);
		let $row = $("tr#guest-" + guest_id);
		if (!guest_id.length || !$row.length) return event.preventDefault();
		let data_base64 = str($row.attr("data-base64"), "", true);
		let data = jsonParse(atob(data_base64), false, (temp) => hasKeys(temp, "id", "first_name", "last_name"));
		if (data) toggleForm(true, data);
	});

	//on guest delete
	$(document.body).on("click", "td > .guest-delete", function(event){
		let guest_id = str($(this).attr("data-id"), "", true);
		let $row = $("tr#guest-" + guest_id);
		if (!guest_id.length || !$row.length) return event.preventDefault();
		let guest_names = str($row.find("[data-name='names']").attr("data-value"), "Delete Guest", true);
		let uk_datatable_row_index = getUKDatatableRowIndex("guest-" + guest_id);
		UKPromptConfirm("<p class='uk-text-danger'>Delete guest <strong>" + guest_names + "</strong> permanently?", function(){
			//post guest delete
			selfPOST("guest_delete=" + encodeURIComponent(guest_id), is_loading => {
				//toggle loading
				if (is_loading) $(".x-table").addClass("x-block");
				else $(".x-table").removeClass("x-block");
			}).then(response => {
				//on success
				uk_datatable_rows.splice(uk_datatable_row_index, 1); //remove deleted row
				uk_datatable_refresh(); //refresh table
				toggleForm(); //hide form
			}, error => UKPrompt("<p class='uk-text-danger'>" + error + "</p>"));
		}, null, "Delete Guest", "uk-button-danger");
	});

	//on guest invitation
	let invitation_html = (data, for_email) => {
		let GUEST_TYPES = {0: "General Admission", 1: "VIP", 2: "VVIP"};
		let html = '';
		if (for_email) html += '<div style="border-top:10px solid #8BC34A;display:block;font-family:Helvetica;font-size:14px;text-align:center;background:#fff;">';
		else html += '<div style="max-width:350px;border:1px solid #ddd;border-top:10px solid #8BC34A;display:inline-block;font-family:Helvetica;font-size:14px;text-align:center;background:#fff;">';

		html += '<div style="padding:20px;">';
		html += '<h1 style="margin:0;color:#8BC34A;white-space:nowrap;">I N V I T A T I O N</h1>';
		if (EVENT_DATA.settings && EVENT_DATA.settings.type) html += '<p style="margin:0 0 5px;color:#8BC34A;text-align:center;"> ~ ' + GUEST_TYPES[data.type].toUpperCase() + ' ~ </p>';

		html += '<div style="height:1px;width:200px;margin:10px 0;border-top:1px solid #ddd;display:inline-block;"></div>';

		html += '<h2 style="margin:5px 0 0">To: <strong>' + (data.first_name + ' ' + data.last_name).toUpperCase() + '</strong></h2>';
		html += '<p style="margin:5px 0 0">Congratulations! You are invited!</p>';
		if (data.inv_message.trim().length) html += '<p style="margin:5px 0 0">&quot;' + data.inv_message + '&quot;</p>';
		if (EVENT_DATA.settings){
			if (EVENT_DATA.settings.scan_mode == 1) html += '<img draggable="false" src="' + ROOT_URL + '/?qr_code=' + data.barcode + '&size=10&margin=1" style="width:100px;height:auto;margin:10px 0 0;" />';
			if (EVENT_DATA.settings.scan_mode == 2) html += '<img draggable="false" src="' + ROOT_URL + '/?barcode=' + data.barcode + '&width=200&height=50" style="width:150px;height:auto;margin:10px 0 0;" />';
		}
		let admit = data.extend_inv ? data.expected_guests : 1;
		html += '<p style="margin:2px 0 0;"><strong>' + data.barcode + '</strong> <i>(admit ' + admit + ' guest' + (admit > 1 ? 's' : '') + ')</i></p>';
		html += '<div style="height:1px;width:200px;margin:10px 0;border-top:1px solid #ddd;display:inline-block;"></div>';

		html += '<h2 style="margin:0;color:#8BC34A;">DETAILS</h2>';
		html += '<h2 style="margin:5px 0 0;"><strong>' + EVENT_DATA.name + '</strong></h2>';
		html += '<p style="margin: 2px 0 0;">(' + EVENT_DATA.type.name + ')</p>'
		if (EVENT_DATA.description.length) html += '<p style="margin:5px 0 0;">' + EVENT_DATA.description + '</p>';
		html += '<ul style="list-style-type:none;margin:5px 0 0;padding:0;">';
		let event_start = EVENT_DATA.start[3];
		let event_end = EVENT_DATA.end[3];
		if (event_start == event_end) html += '<li><strong>Event Date:</strong> ' + event_start + '</li>';
		else {
			html += '<li><strong>Event Starts:</strong> ' + event_start + '</li>';
			html += '<li><strong>Event Ends:</strong> ' + event_end + '</li>';
		}
		if (EVENT_DATA.location) html += '<li><strong>Location:</strong> ' + EVENT_DATA.location.name + '</li>';
		html += '</ul>';
		if (EVENT_DATA.settings && EVENT_DATA.settings.rsvp) html += '<a href="' + ROOT_URL + '/e/' + EVENT_DATA.id + '/' + data.barcode + '" target="_blank" style="padding:5px 10px;background:#8BC34A;color:#fff;text-decoration:none;text-align:center;display:inline-block;min-width:100px;margin:10px 0 0;">RSVP</a>';
		html += '</div>';

		if (EVENT_DATA.location_map_image) html += '<a href="' + EVENT_DATA.location_map_image + '" target="_blank"><img draggable="false" src="' + EVENT_DATA.location_map_image + '" style="width:100%;height:150px;object-fit:cover;object-position:center;" /></a>';
		html += '<div style="padding:20px;background:#444;">';
		html += '<p style="margin:5px 0;color:#fff;">Powered By</p>';
		html += '<a href="' + ROOT_URL + '" target="_blank"><img draggable="false" src="' + ROOT_URL + '/assets/img/logow.png" style="display: inline-block; width: 100px; height: auto;margin:0;" /></a>';
		html += '</div>';

		html += '</div>';
		return html;
	};
	let invitation_text = (data) => {
		let text = data.barcode + ': ' + data.names + ' invited to ' + EVENT_DATA.name + ' (' + EVENT_DATA.type.name + ')';
		if (EVENT_DATA.description.length) text += ' - ' + EVENT_DATA.description.replace(/\s*\.\s*$/g, '') + '.';
		text += ' TIME: ' + EVENT_DATA.display_time + '.';
		let admit = data.extend_inv ? data.expected_guests : 1;
		text += ' ADMIT: ' + admit + ' guest' + (admit > 1 || admit == 0 ? 's' : '');
		if (data.inv_message.trim().length) text += ' MSG: ' + data.inv_message.trim();
		if (EVENT_DATA.settings && EVENT_DATA.settings.rsvp) text += ' RSVP: ' +  ROOT_URL + '/e/' + EVENT_DATA.id + '/' + data.barcode;
		return text;
	};
	let fetch_invitation_pdf = (event_id, barcodes, loading_callback) => {
		return new Promise((resolve, reject) => {
			let post_url = relativeRoot() + "/document";
			//console.log('post_url', post_url);
			let post_data = "invitations_pdf=" + encodeURIComponent(str(event_id, "", true)) + "&barcodes=" + encodeURIComponent(stringify(barcodes));
			serverPOST(post_url, post_data, loading_callback).then((res) => {
				let pdf = base64Blob(res.data, "application/pdf", 512);
				return resolve(pdf);
			}, (err) => reject(err));
		});
	};
	let invitation_print_pdf = (barcodes) => {
		let loading_modal = null;
		let loading_callback = (toggle_loading) => {
			if (toggle_loading){
				if (!loading_modal){
					let modal_options = {keyboard: false, bgclose: false, center: true, modal: false};
					loading_modal = UIkit.modal.blockUI('<i class="uk-icon-spinner uk-icon-spin"></i> Preparing document. Please wait...', modal_options);
				}
				loading_modal.show();
			}
			else if (loading_modal) loading_modal.hide();
		};
		fetch_invitation_pdf(EVENT_DATA.id, barcodes, loading_callback).then((pdf) => {
			if (window.hasOwnProperty("printJS")){
				printJS(blobURL(pdf));
				return true;
			}
			let error = "Exception (invitation_print_pdf): \"printJS\" missing";
			console.error(error);
			UKPrompt(error);
		}, (err) => UKPrompt(err));
	};
	let invitation_print_pdf_bulk = (checked_only) => {
		let test_callback_row = ($row) => {
			if (checked_only){
				let $checkbox = $row.find(".x-bulk-action");
				if (!($checkbox.length && $checkbox.isChecked())) return false;
			}
			return true;
		};
		let table_data = uk_datatable_raw(null, test_callback_row);
		if (Array.isArray(table_data) && table_data.length > 1){
			let barcodes = [];
			for (let r = 1; r < table_data.length; r ++){
				let row = table_data[r], barcode;
				if (Array.isArray(row) && row.length > 2 && (barcode = str(row[1], "", true)).length) barcodes.push(barcode);
			}
			invitation_print_pdf(barcodes);
		}
		else UKPrompt("No records available for this action.");
	};
	$(document.body).on("click", "td > .guest-preview", function(event){
		let guest_id = str($(this).attr("data-id"), "", true);
		let $row = $("tr#guest-" + guest_id);
		if (!guest_id.length || !$row.length) return event.preventDefault();
		let data_base64 = str($row.attr("data-base64"), "", true);
		let data = jsonParse(atob(data_base64), false, (temp) => hasKeys(temp, "id", "first_name", "last_name"));
		let html = '<a href="#" style="display:none;">&nbsp;</a>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<div style="background:#eee;display:inline-block;width:100%;text-align:center;padding:20px 0;">'; //2
		html += invitation_html(data);
		html += '</div>'; //2
		html += '</div>'; //1
		html += '<div class="uk-modal-footer uk-text-right">';
		html += '<button type="button" class="uk-button uk-modal-close">Close</button> ';
		html += '<button type="button" data-event="print" class="uk-button uk-button-primary"><i class="uk-icon-print"></i> Print</button> ';
		html += '<button type="button" data-event="send" class="uk-button uk-button-success"><i class="uk-icon-send"></i> Send</button>';
		html += '</div>';
		UKModal(html, "x-modal x-modal-nopad", null, null, (data_event, event, $modal) => {
			if (data_event == "print"){
				invitation_print_pdf([data.barcode]);
				return true;
			}
			if (data_event == "send"){
				sending_invitations([guest_id]);
			}
			return false;
		});
	});
	$(document.body).on("click", ".x-toolbar-print-cards", () => invitation_print_pdf_bulk());
	$(document.body).on("click", "[data-action='print-cards']", function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		invitation_print_pdf_bulk(true);
	});

	//on bulk actions dropdown show
	$(".x-bulk-actions").on("show.uk.dropdown", () => $(".x-table").addClass("bulk_actions_show"));

	//on bulk actions dropdown hide
	$(".x-bulk-actions").on("hide.uk.dropdown", () => $(".x-table").removeClass("bulk_actions_show"));

	//helper functions ~ bulk actions
	let bulk_checked = (set_checked, is_checked) => {
		let guests_ids = [];
		for (let i = 0; i < uk_datatable_rows.length; i ++){
			let $row = $(uk_datatable_rows[i]);
			if (!$row.length) continue;
			let guest_id = str($row.attr("data-id"), "", true);
			let $row_checkbox = $row.find("input.x-bulk-action");
			if (!guest_id.length || !$row_checkbox.length) continue;
			if ("boolean" === typeof set_checked){
				$row_checkbox.setChecked(set_checked);
				$("[data-id='" + guest_id + "'].x-bulk-action").setChecked(set_checked);
			}
			if ("boolean" === typeof is_checked && (is_checked === set_checked || $row_checkbox.isChecked() === is_checked) || "boolean" != typeof is_checked && $row_checkbox.isChecked()) guests_ids.push(guest_id);
		}
		return guests_ids;
	};
	let bulk_action_post = (bulk_action, bulk_ids) => {
		bulk_action = str(bulk_action, "", true);
		if (!(bulk_action.length && Array.isArray(bulk_ids) && bulk_ids.length)) return false;
		selfPOST("guest_bulk_action=" + encodeURIComponent(bulk_action) + "&list=" + encodeURIComponent(bulk_ids.join(",")), is_loading => {
			//toggle loading
			if (is_loading) $(".x-table").addClass("x-block");
			else $(".x-table").removeClass("x-block");
		}).then(response => {
			//on success
			if (["invited", "confirmed"].indexOf(bulk_action) > -1){
				if (Array.isArray(response.data)) for (let i = 0; i < response.data.length; i ++) setTableRow(response.data[i], true);
			}
			else if (bulk_action == "delete"){
				if (Array.isArray(response.data)){
					for (let i = 0; i < response.data.length; i ++){
						let guest_id = str(response.data[i], "", true);
						let uk_datatable_row_index = getUKDatatableRowIndex("guest-" + guest_id);
						if (uk_datatable_row_index >= 0) uk_datatable_rows.splice(uk_datatable_row_index, 1); //remove deleted row
					}
				}
			}
			uk_datatable_refresh(); //refresh table
			toggleForm(); //hide form
			UKPrompt('<p class="uk-text-success">' + response.message + "</p>");
		},
		error => UKPrompt('<p class="uk-text-danger">' + error + "</p>")); //on error
	};
	let bulk_not_checked = (event) => {
		if (event) event.preventDefault();
		notify("Kindly check items for this action.", "warning");
		return false;
	};

	//on bulk actions select-all
	$(".x-bulk-actions .uk-dropdown [data-action='select-all']").click(() => bulk_checked(true));

	//on bulk actions select-none
	$(".x-bulk-actions .uk-dropdown [data-action='select-none']").click(() => bulk_checked(false));

	//on bulk actions invited
	$(".x-bulk-actions .uk-dropdown [data-action='invited']").click(function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		let prompt = "<h2>" + checked_ids.length + " Selected</h2><p><strong>Confirm Invitation:</strong> Are you sure you want to set the status of selected guests to <strong>Invited</strong>?</p>";
		prompt += "<small><strong>NOTE:</strong> This does not send invitations, to do that, use \"Send Invitation\" option.</small>";
		UKPromptConfirm(prompt, () => bulk_action_post("invited", checked_ids), null, "Set Invited", "uk-button-primary");
	});

	//on bulk actions confirmed
	$(".x-bulk-actions .uk-dropdown [data-action='confirmed']").click(function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		let prompt = "<h2>" + checked_ids.length + " Selected</h2><p><strong>Confirm Attendance:</strong> Are you sure you want to set the status of selected guests to <strong>Confirmed</strong>?</p>";
		prompt += "<small><strong>NOTE:</strong> This also sets the invitation time of previously uninvited guest records</small>";
		UKPromptConfirm(prompt, () => bulk_action_post("confirmed", checked_ids), null, "Set Confirmed", "uk-button-primary");
	});

	//on bulk actions delete
	$(".x-bulk-actions .uk-dropdown [data-action='delete']").click(function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		let prompt = "<h2 class='uk-text-danger'>" + checked_ids.length + " Selected</h2><p class='uk-text-danger'><strong>Delete:</strong> Are you sure you want to set the status of selected guests? <strong>This action cannot be undone</strong>.</p>";
		UKPromptConfirm(prompt, () => bulk_action_post("delete", checked_ids), null, "Delete Guests", "uk-button-danger");
	});

	//on bulk actions send invitations
	sending_invitations = (guest_ids) => {
		if (!(Array.isArray(guest_ids) && guest_ids.length)){
			console.error('No guest IDs in params');
			return false;
		}
		let html = '<a href="#" style="display:none;">&nbsp;</a>';
		html += '<div class="uk-overflow-container">'; //1
		html += '<div style="background:#eee;display:inline-block;width:100%;">'; //2
		html += '<div class="x-pad-20">'; //3
		html += '<h2 class="x-inv-title"><i class="uk-icon-spinner uk-icon-spin"></i> Sending Invite</h2>';
		html += '<div class="x-inv-sending-log"></div>'; //l
		html += '</div>'; //3
		html += '</div>'; //2
		html += '</div>'; //1
		html += '<div class="x-inv-sending-footer uk-modal-footer uk-text-right">';
		html += '<div class="uk-progress-bar"><div></div></div>'; //p
		html += '<button type="button" data-event="print" class="uk-button uk-button-danger">Cancel</button> ';
		html += '</div>';
		let refresh_on_close = false;
		UKModal(html, "x-modal x-modal-nopad", function($modal, uk_modal){
			let $sending_title = $modal.find('.x-inv-title');
			let $sending_log = $modal.find('.x-inv-sending-log');
			let $sending_prog = $modal.find('.x-inv-sending-footer .uk-progress-bar > div');
			let $sending_cancel = $modal.find('.x-inv-sending-footer .uk-button');
			let is_sending = true, close_on_done = false;
			let close_dialog = () => {
				is_sending = false;
				if (uk_modal.isActive()) uk_modal.hide();
			};
			let setProgress = (p) => {
				if (p === 100) $sending_prog.removeClass('animate');
				else $sending_prog.addClass('animate');
				$sending_prog.css({width: p + '%'}).html(p + '%');
			};
			$sending_cancel.click(function(){
				if (is_sending){
					is_sending = false;
					$sending_cancel.html('Cancelling...');
					$sending_cancel.removeClass('uk-button-danger');
					$sending_cancel.addClass('uk-disabled');
					$sending_cancel.attr('disabled', 'disabled');
					$sending_cancel[0].disabled = true;
					close_on_done = true;
				}
				else close_dialog();
			});
			$sending_cancel.html('Cancel');
			setProgress(0);
			$sending_log.append($('<p>Sending ' + guest_ids.length + ' invitations...</p>'));
			let len = guest_ids.length;
			objectPromise(guest_ids, function(guest_id, key, i){
				return new Promise((resolve, reject) => {
					if (!is_sending) return resolve();
					let err;
					let n = i + 1;
					let p = Math.floor(n / len * 100);
					let o = n + '/' + len;
					let $log = $('<p>Preparing invitation ' + o + '...</p>');
					let onError = (err, _class) => {
						$log.html('[' + o + '] ' + err);
						$log.addClass(str(_class, 'uk-text-default', true));
						setProgress(p);
						return reject(err);
					};
					$sending_log.append($log);
					let guest_id = guest_ids[i];
					let $row = $("tr#guest-" + guest_id);
					if (!guest_id.length || !$row.length) return onError('Unable to get guest row data!', 'uk-text-danger');
					let data_base64 = str($row.attr("data-base64"), "", true);
					let data = jsonParse(atob(data_base64), false, (temp) => hasKeys(temp, "id", "first_name", "last_name"));
					if (!data) return onError('Unable to get guest data!', 'uk-text-danger');
					let names = data.names;
					let inv_phone = str(data.inv_phone, '', true);
					let inv_email = str(data.inv_email, '', true);
					if (!inv_phone.length && !inv_email.length) return onError('(' + names + ') No invitation contacts.', 'uk-text-warning');
					let invite_html = invitation_html(data, true);
					let invite_text = invitation_text(data);

					//post invite
					$log.html('[' + o + '] (' + names + ') Sending invitation...');
					let post_url = relativeRoot() + "/messenger";
					let post_data = objectParams({
						send_invitation: btoa(invite_html),
						event: EVENT_DATA.id,
						names: names,
						phone: inv_phone,
						email: inv_email,
						text: invite_text,
						barcode: data.barcode,
					});
					serverPOST(post_url, post_data, null).then((res) => {
						refresh_on_close = true;
						$log.html('[' + o + '](' + names + ') ' + res.message);
						setProgress(p);
						resolve();
					}, (err) => {
						console.error(SERVER_RESPONSE);
						onError(err);
					});
				});
			}, true).then(() => {
				is_sending = false;
				$sending_title.find('i').addClass('x-hidden');
				if (close_on_done){
					$sending_log.append($('<p>Cancelled!</p>'));
					setTimeout(() => close_dialog(), 1000);
					return;
				}
				$sending_log.append($('<p>Finished!</p>'));
				$sending_cancel.html('Close');
			});
		}, function(){
			if (refresh_on_close){
				loading_block(true, 'Refreshing list...');
				setTimeout(() => window.location.reload(), 500);
			}
		}, null, false, false, true, false);
	};
	$(".x-bulk-actions .uk-dropdown [data-action='send-invites']").click(function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		let prompt = "<h2>" + checked_ids.length + " Selected</h2><p><strong>Send Invitations:</strong> Are you sure you want to send invitations to selected guests?</p>";
		prompt += "<small><strong>NOTE:</strong> Guests without invitation contacts will be ignored. Guests already invited will receive another invitation message.</small>";
		UKPromptConfirm(prompt, () => sending_invitations(checked_ids), null, "Send Invitations", "uk-button-success");
	});

	//on print list
	let print_list = (checked_only) => {
		let loading_modal = null;
		let loading_callback = (toggle_loading) => {
			if (toggle_loading){
				if (!loading_modal){
					let modal_options = {keyboard: false, bgclose: false, center: true, modal: false};
					loading_modal = UIkit.modal.blockUI('<i class="uk-icon-spinner uk-icon-spin"></i> Preparing document. Please wait...', modal_options);
				}
				loading_modal.show();
			}
			else if (loading_modal) loading_modal.hide();
		};
		let test_callback_row = ($row) => {
			if (checked_only){
				let $checkbox = $row.find(".x-bulk-action");
				if (!($checkbox.length && $checkbox.isChecked())) return false;
			}
			return true;
		};
		let options = {
			title: EVENT_DATA.name,
			subtitle: EVENT_DATA.display_time + " | Showing ROWS_COUNT record(s)",
			logo: ROOT_URL + "/assets/img/logo.png",
			table_options: {
				aligns_row: {0: "C"},
		    }
		};
		uk_datatable_print(options, loading_callback, null, test_callback_row).then(() => console.debug("Print request successful!"), (err) => UKPrompt(err));
	};
	$(document.body).on("click", ".x-toolbar-print-list", () => print_list());
	$(document.body).on("click", "[data-action='print-list']", function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		print_list(true);
	});

	//on download
	let download_list = (checked_only) => {
		let loading_modal = null;
		let loading_callback = (toggle_loading) => {
			if (toggle_loading){
				if (!loading_modal){
					let modal_options = {keyboard: false, bgclose: false, center: true, modal: false};
					loading_modal = UIkit.modal.blockUI('<i class="uk-icon-spinner uk-icon-spin"></i> Preparing document. Please wait...', modal_options);
				}
				loading_modal.show();
			}
			else if (loading_modal) loading_modal.hide();
		};
		let test_callback_row = ($row) => {
			if (checked_only){
				let $checkbox = $row.find(".x-bulk-action");
				if (!($checkbox.length && $checkbox.isChecked())) return false;
			}
			return true;
		};
		uk_datatable_download(EVENT_DATA.name, loading_callback, null, test_callback_row).then(() => console.debug("Download request successful!"), (err) => UKPrompt(err));
	};
	$(document.body).on("click", ".x-toolbar-download", () => download_list());
	$(document.body).on("click", "[data-action='download-list']", function(event){
		let checked_ids = bulk_checked();
		if (!checked_ids.length) return bulk_not_checked(event);
		download_list(true);
	});

	//invitation
	let create_invitation = (guest_data) => {};

	//on add guest
	$add_guest.click(() => toggleForm(true, null));

	//on cancel
	$guest_cancel.click(() => toggleForm());
	$guest_cancel_mini.click(() => toggleForm());

	//validation input error set
	let first_input_error = null;
	let inputError = ($input, error_message) => {
		error_message = str(error_message, "", true);
		$input.parent().find("small.uk-text-danger").remove();
		$input.removeClass("uk-form-danger");
		if (error_message.length){
			if (!first_input_error) first_input_error = $input;
			$input.parent().append('<small class="uk-text-danger">' + error_message + '</small>');
			$input.addClass("uk-form-danger");
			return false;
		}
		return true;
	};

	//validate phone number
	let validate_phone_error = "";
	let validate_phone_support = {
		"254$2": /^(\+?254|^0)(7[0-9]{8})$/,
	};
	let validatePhone = (phone, is_supported) => {
		validate_phone_error = "";
		if (!(phone = str(phone, "", true)).length) return null;
		if (!phone.match(/^\+?[0-9\s-\(\)]+$/)){
			validate_phone_error = "Invalid phone number format";
			return false;
		}
		phone = phone.replace(/[^0-9]/g, "");
		if (phone.length < 5 || phone.length > 14){
			validate_phone_error = "Invalid phone number";
			return false;
		}
		if (is_supported && isObj(validate_phone_support, true, true).length){
			let supported = false;
			for (let key in validate_phone_support){
				if (validate_phone_support.hasOwnProperty(key)){
					let test = validate_phone_support[key];
					if (phone.match(test)){
						phone = phone.replace(test, key);
						supported = true;
						break;
					}
				}
			}
			if (!supported){
				validate_phone_error = "Phone number \"" + phone + "\"is not currently supported";
				return false;
			}
		}
		return phone;
	};

	//validation elements
	$validate_items = $("[data-validate]");

	//on validation element
	$validate_items.on("input", function(event){
		inputError($(this), null);
	});

	//validate validation elements
	let validateForm = () => {
		let pass = true;
		first_input_error = null;
		for (let i = 0; i < $validate_items.length; i ++){
			let $item = $($validate_items[i]);
			inputError($item, null);
			let item_validate = str($item.attr("data-validate"), "", true);
			if (item_validate.length){
				let item_value = str($item.val(), "", true);
				if (item_validate == "required" && !item_value.length) pass = inputError($item, "This field requires input.");
				if (item_validate == "phone-any" && item_value.length){
					let phone = validatePhone(item_value);
					if (phone !== null){
						if (!phone) pass = validate_phone_error;
						else $item.val(phone);
					}
				}
				if (item_validate == "phone" && item_value.length){
					let phone = validatePhone(item_value, true);
					if (phone !== null){
						if (!phone) pass = validate_phone_error;
						else $item.val(phone);
					}
				}
				if (item_validate == "email" && item_value.length){
					item_value = item_value.toLowerCase();
					if (!validateEmail(item_value)) pass = inputError($item, "Invalid email address");
					else $item.val(item_value);
				}
				if (item_validate == "number+" && item_value.length){
					item_value = Number(item_value);
					if (isNaN(item_value) || item_value < 0) pass = inputError($item, "Kindly enter a valid number value");
					else $item.val(item_value + "");
				}
				if (item_validate == "qa-a" && item_value.length){
					let $qa_q = $("[data-validate='qa-q']");
					if ($qa_q.length && !str($qa_q.val(), "", true).length){
						pass = inputError($qa_q, "Kindly provide your custom question for the answer.");
					}
					else $item.val(item_value);
				}
			}
		}
		if (!pass){
			if (first_input_error) first_input_error.scrollToMe();
			return false;
		}
		return true;
	};

	//on form submit
	$guest_form.on("submit", function(event){
		event.preventDefault();
		event.stopPropagation();

		//validate form data
		if (!validateForm()) return false;

		//submit form data
		selfPOST($(this).serialize(), is_loading => {
			//toggle loading
			if (is_loading){
				$guest_error.html("");
				$guest_save.hide();
				$guest_saving.removeClass("x-hidden");
				$guest_form.addClass("x-block");
			}
			else {
				$guest_form.removeClass("x-block");
				$guest_saving.addClass("x-hidden");
				$guest_save.show();
			}
		}).then(response => {
			//on success
			let data_id = response.data.id;
			$(".x-table").scrollLeft(0); //scroll table wrapper left
			setTableRow(response.data); //update table
			toggleForm(false); //reset & hide form
			$("tr#guest-" + data_id).scrollToMe();
			$("tr#guest-" + data_id).flashGreen();
		}, error => {
			//on error
			$guest_error.html(errorMessage(error));
			$guest_error.scrollToMe();
		});
	});

	//guest save mini
	$guest_save_mini.click(function(event){
		event.preventDefault();
		$guest_form.submit();
	});
});
