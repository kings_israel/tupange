$(function(){
	//creates a popup window
	let popupCenter = function(url, title, w, h){
		let dual_left = window.screenLeft != undefined ? window.screenLeft : window.screenX;
		let dual_top = window.screenTop != undefined ? window.screenTop : window.screenY;
		let width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		let height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
		let zoom = width / window.screen.availWidth;
		let left = (width - w) / 2 / zoom + dual_left;
		let win = window.open(url, title, "scrollbars=yes,width=" + (w / zoom) + ',height=' + (h / zoom) + ',top=100,left=' + left);
		if (window.focus) win.focus();
		return win;
	};

	//starts an OAuth popup
	let loginOAuth = function(provider, error_wrapper){
		let url = "./oauth?provider=" + provider + "&action=login";
		window["HandleOAuth"] = function(response){
			let error_message = "Unexpected OAuth response!";
			if (hasKeys(response, "pass", "message")){
				if (response.pass){
					let redirect_url = window.location.href = ROOT_URL + "/dashboard";
					if (window.hasOwnProperty("redirect") && (redirect = str(redirect, "", true)).length) redirect_url = decodeURIComponent(redirect);
					window.location.href = redirect_url;
					return;
				}
				else error_message = response.message;
			}
			else console.error(error_message, response);
			error_wrapper.html(errorMessage(error_message));
		};
		let win = popupCenter(url, "Login " + provider, 500, 400);
	};

	//listen for social media login
	$social_error = $("#x-social-error");
	$(".x-btn-fb").click(() => loginOAuth("Facebook", $social_error));
	$(".x-btn-tw").click(() => loginOAuth("Twitter", $social_error));
	$(".x-btn-go").click(() => loginOAuth("Google", $social_error));
});
