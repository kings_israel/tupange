$(function(){
	//load form
	$(".edit_service_form").removeClass("x-display-none");

	//service title
	$title_input = $("#service_title");
	$title_error = $("#service_title_error");
	let check_title = () => {
		let contents_length = $title_input.val().trim().length;
		if (contents_length > 0 && contents_length < 10){
			$title_input.addClass("uk-form-danger");
			$title_error.html("Title requires at least " + (10 - contents_length) + " more characters.");
			$title_error.show();
		}
		else {
			$title_input.removeClass("uk-form-danger");
			$title_error.html("");
			$title_error.hide();
		}
	};
	$title_input.on("input", () => check_title());
	check_title();

	//service description
	$desc_input = $("#service_description");
	$desc_error = $("#service_description_error");
	let check_desc = () => {
		let contents_length = $desc_input.val().trim().length;
		if (contents_length > 0 && contents_length < 40){
			$desc_input.addClass("uk-form-danger");
			$desc_error.html("Description requires at least " + (40 - contents_length) + " more characters.");
			$desc_error.show();
		}
		else {
			$desc_input.removeClass("uk-form-danger");
			$desc_error.html("");
			$desc_error.hide();
		}
	};
	$desc_input.on("input", () => check_desc());
	check_desc();
});
