<?php
//ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;

#shared session variables
$root = NS_SITE;
$placeholder_image = $root . "/assets/img/placeholder.jpg";
$default_avatar = $root . "/assets/img/user.png";
$session_uid = "";
$session_userlevel = 0;
$session_status = 0;
$session_description = "";
$session_avatar = $default_avatar;
$session_name = "";
if (sessionActive()){
	$session_uid = trim($_SESSION["uid"]);
	$session_userlevel = (int) $_SESSION["userlevel"];
	$session_status = (int) $_SESSION["status"];
	$session_description = trim($_SESSION["description"]);
	$session_avatar = isset($_SESSION["photo_url"]) && strlen($src = trim($_SESSION["photo_url"])) ? $src : $default_avatar;
	if (fn1::isFile(getUploads(true) . '/' . preg_replace('/^\/|\/$/s', '', $session_avatar))) $session_avatar = getUploads() . '/' . preg_replace('/^\/|\/$/s', '', $session_avatar);
	$session_name = fn1::strtocamel(fn1::toStrn($_SESSION["display_name"], true));
}

//credits
const SOURCE_CREDIT = "/***"
	. "\r\n" . "Copyright (C) 2019 Tupange Events LTD"
	. "\r\n" . "Developers:"
	. "\r\n" . "~ Martin Thuku (dev@naicode.com)"
	. "\r\n" . "VERSION: " . SITE_VERSION
	. "\r\n***/\r\n\r\n";

//required javascript files
const JS_FILE = [
	//"jquery"			=> __DIR__ . "/jquery-3.4.1.min.js"
	"jquery"			=> __DIR__ . "/jquery-1.8.3.min.js",
	"moment"			=> __DIR__ . "/moment.min.js",
	"uikit"				=> __DIR__ . "/uikit.min.js",
	"print"				=> __DIR__ . "/print.min.js",
	"zxing"				=> __DIR__ . "/zxing.min.js",
	"mention"			=> __DIR__ . "/mention.js",

	"shared-start"		=> __DIR__ . "/shared/start.js",
	"shared-before"		=> __DIR__ . "/shared/before.js",
	"shared-after"		=> __DIR__ . "/shared/after.js",
	"shared-form1"		=> __DIR__ . "/shared/form1.js",
	"shared-datatable"	=> __DIR__ . "/shared/uk-datatable.js",
	"shared-prompt"		=> __DIR__ . "/shared/uk-prompt.js",
	"shared-service"	=> __DIR__ . "/shared/service.js",
	"shared-review"		=> __DIR__ . "/shared/review.js",
	"shared-autogrow"	=> __DIR__ . "/shared/autogrow.js",

	"shared-rating-stars"	=> __DIR__ . "/shared/uk-rating-stars.js",

	"uikit-sticky"		=> __DIR__ . "/components/sticky.min.js",
	"uikit-slider"		=> __DIR__ . "/components/slider.min.js",
	"uikit-slideset"	=> __DIR__ . "/components/slideset.min.js",
	"uikit-datepicker"	=> __DIR__ . "/components/datepicker.min.js",
	"uikit-autocomplete"=> __DIR__ . "/components/autocomplete.min.js",
	"uikit-timepicker"	=> __DIR__ . "/components/timepicker.min.js",
	"uikit-notify"		=> __DIR__ . "/components/notify.min.js",
	"uikit-lightbox"	=> __DIR__ . "/components/lightbox.min.js",
	"uikit-tooltip"		=> __DIR__ . "/components/tooltip.min.js",

	"uk-tags"			=> __DIR__ . "/elements/uk-tags.js",
	"uk-autocomplete"	=> __DIR__ . "/elements/uk-autocomplete.js",
	"map-input"			=> __DIR__ . "/elements/map-input.js",

	"pg-signup"			=> __DIR__ . "/pages/signup.js",
	"pg-login"			=> __DIR__ . "/pages/login.js",
	"pg-profile"		=> __DIR__ . "/pages/profile.js",
	"pg-services-a"		=> __DIR__ . "/pages/services-active.js",
	"pg-services-p"		=> __DIR__ . "/pages/services-paused.js",
	"pg-services-d"		=> __DIR__ . "/pages/services-deleted.js",
	"pg-services-e"		=> __DIR__ . "/pages/services-edit.js",
	"pg-dash-edit-e"	=> __DIR__ . "/pages/dash-edit-event.js",
	"pg-dash-events"	=> __DIR__ . "/pages/dash-events.js",
	"pg-dash-cart"		=> __DIR__ . "/pages/dash-cart.js",
	"pg-dash-event"		=> __DIR__ . "/pages/dash-event.js",
	"pg-service"		=> __DIR__ . "/pages/service.js",
	"pg-search"			=> __DIR__ . "/pages/search.js",
	"pg-orders"			=> __DIR__ . "/pages/orders.js",
	"pg-order"			=> __DIR__ . "/pages/order.js",
	"pg-messages"		=> __DIR__ . "/pages/messages.js",
	"pg-rsvp"			=> __DIR__ . "/pages/rsvp.js",

	"pg-dash-event-guests"			=> __DIR__ . "/pages/dash-event-guests.js",
	"pg-dash-event-attendance"		=> __DIR__ . "/pages/dash-event-attendance.js",
	"pg-dash-event-registration"	=> __DIR__ . "/pages/dash-event-registration.js",
	"pg-dash-event-gifts"			=> __DIR__ . "/pages/dash-event-gifts.js",
	"pg-dash-event-users"			=> __DIR__ . "/pages/dash-event-users.js",
	"pg-dash-event-budget"			=> __DIR__ . "/pages/dash-event-budget.js",
	"pg-dash-event-services"		=> __DIR__ . "/pages/dash-event-services.js",
	"pg-dash-event-checklist"		=> __DIR__ . "/pages/dash-event-checklist.js",
	"pg-dash-event-services"		=> __DIR__ . "/pages/dash-event-services.js",
];

//requested scripts
$files = [];

//pre scripts
if (isset($_GET["pre"])){

	//pre scripts
	switch ($_GET["pre"]){
		case "home":
		$files[] = JS_FILE["uk-tags"];
		$files[] = JS_FILE["uk-autocomplete"];
		break;

		case "complete":
		$files[] = JS_FILE["uk-tags"];
		$files[] = JS_FILE["uk-autocomplete"];
		$files[] = JS_FILE["map-input"];
		break;

		case "services-e":
		$files[] = JS_FILE["uk-tags"];
		$files[] = JS_FILE["uk-autocomplete"];
		$files[] = JS_FILE["map-input"];
		break;

		case "dash-edit-event":
		$files[] = JS_FILE["uk-autocomplete"];
		$files[] = JS_FILE["map-input"];
		break;

		case "search":
		//$files[] = JS_FILE["uk-tags"];
		$files[] = JS_FILE["uk-autocomplete"];
		break;
	}

	//shared start scripts
	$files[] = JS_FILE["shared-start"];
}
else {
	//core inline
	$files["inline-01"] = 'window["ROOT_URL"] = "' . $root . '";';
	$files["inline-02"] = 'window["DEFAULT_AVATAR"] = "' . $default_avatar . '";';
	$files["inline-03"] = 'window["PLACEHOLDER_IMAGE"] = "' . $placeholder_image . '";';
	$files["inline-04"] = 'window["SESSION_USER"] = ' . json_encode([
		'uid' => $session_uid,
		'name' => $session_name,
		'level' => $session_userlevel,
		'avatar' => $session_avatar,
		'description' => $session_description,
		'status' => $session_status
	]) . ';';

	//core scripts
	$files[] = JS_FILE["jquery"];
	$files[] = JS_FILE["uikit"];
	$files[] = JS_FILE["uikit-notify"];

	//shared before scripts
	$files[] = JS_FILE["shared-before"];

	//main scripts
	if (isset($_GET["p"])){
		switch ($_GET["p"]){
			case "home":
			$files[] = JS_FILE["uikit-slideset"];
			$files[] = JS_FILE["uikit-datepicker"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["shared-service"];
			break;

			case "signup":
			$files[] = JS_FILE["pg-signup"];
			break;

			case "login":
			$files[] = JS_FILE["pg-login"];
			break;

			case "complete":
			$files[] = JS_FILE["shared-form1"];
			break;

			case "services-a":
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-services-a"];
			break;

			case "services-p":
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-services-p"];
			break;

			case "services-d":
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-services-d"];
			break;

			case "services-e":
			$files[] = JS_FILE["shared-form1"];
			$files[] = JS_FILE["pg-services-e"];
			break;

			case "dash-edit-event":
			$files[] = JS_FILE["moment"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["uikit-autocomplete"];
			$files[] = JS_FILE["uikit-datepicker"];
			$files[] = JS_FILE["uikit-timepicker"];
			$files[] = JS_FILE["pg-dash-edit-e"];
			break;

			case "dash":
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			break;

			case "dash-events":
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-dash-events"];
			break;

			case "dash-cart":
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-dash-cart"];
			break;

			case "dash-event":
			$files[] = JS_FILE["pg-dash-event"];
			break;

			case "dash-event-guests":
			$files[] = JS_FILE["print"];
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-dash-event-guests"];
			break;

			case "service":
			$files[] = JS_FILE["uikit-lightbox"];
			$files[] = JS_FILE["shared-rating-stars"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["shared-service"];
			$files[] = JS_FILE["pg-service"];
			break;

			case "search":
			$files[] = JS_FILE["uikit-sticky"];
			$files[] = JS_FILE["uikit-datepicker"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["shared-service"];
			$files[] = JS_FILE["pg-search"];
			break;

			case "orders":
			$files[] = JS_FILE["uikit-tooltip"];
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["shared-rating-stars"];
			$files[] = JS_FILE["shared-review"];
			$files[] = JS_FILE["pg-orders"];
			$files[] = JS_FILE["pg-order"];
			break;

			case "messages":
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-messages"];
			break;

			case "dash-event-attendance":
			$files[] = JS_FILE["zxing"];
			$files[] = JS_FILE["print"];
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-dash-event-attendance"];
			break;

			case "dash-event-registration":
			$files[] = JS_FILE["print"];
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-dash-event-registration"];
			break;

			case "dash-event-gifts":
			$files[] = JS_FILE["print"];
			$files[] = JS_FILE["uikit-datepicker"];
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-dash-event-gifts"];
			break;

			case "dash-event-users":
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-dash-event-users"];
			break;

			case "dash-event-budget":
			$files[] = JS_FILE["print"];
			$files[] = JS_FILE["uikit-datepicker"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-dash-event-budget"];
			break;

			case "dash-event-services":
			$files[] = JS_FILE["print"];
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-dash-event-services"];
			break;

			case "dash-event-checklist":
			$files[] = JS_FILE["mention"];
			$files[] = JS_FILE["moment"];
			$files[] = JS_FILE["uikit-datepicker"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["shared-autogrow"];
			$files[] = JS_FILE["pg-dash-event-checklist"];
			break;

			case "dash-event-services":
			$files[] = JS_FILE["shared-datatable"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-orders"];
			break;

			case "rsvp":
			$files[] = JS_FILE["shared-rating-stars"];
			$files[] = JS_FILE["shared-prompt"];
			$files[] = JS_FILE["pg-rsvp"];
			break;

			case "profile":
			$files[] = JS_FILE["shared-form1"];
			break;
		}
	}

	//shared after scripts
	$files[] = JS_FILE["shared-after"];
}

//generate output
$buffer = [];
foreach ($files as $key => $value){
	if (strpos($key, "inline-") !== false) $buffer[] = $value;
	elseif (file_exists($file = fn1::toStrx($value, true)) && is_file($file) && is_readable($file)){
		$script = file_get_contents($file);
		if (isset($_GET["min"])) $script = fn1::compress($script);
		$script = fn1::toStrx($script, true);
		if (strlen($script)){
			$file_info = fn1::pathInfo($file);
			if (array_key_exists("filename", $file_info)) $script = "\r\n/***[~/" . $file_info["filename"] . "]***/\r\n" . $script;
			$buffer[] = $script;
		}
	}
}

//output scripts
header("Content-Type: text/javascript");
$buffer = fn1::toStrx(implode("\r\n", $buffer), true);
echo strlen($buffer) ? SOURCE_CREDIT. $buffer : "";
exit();
