<?php
//ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;

//variables
const COMPRESS_All = false;
const COMPRESS_CSS = true;
const SOURCE_CREDIT = "/***"
	. "\r\n" . "Copyright (C) 2019 Tupange Events LTD"
	. "\r\n" . "Developers:"
	. "\r\n" . "~ Martin Thuku (dev@naicode.com)"
	. "\r\n" . "VERSION: " . SITE_VERSION
	. "\r\n***/\r\n\r\n";

//required css files
const CSS_FILE = [
	"main" 			=> __DIR__ . "/main.css",
	"mention" 		=> __DIR__ . "/mention.css",
	"uikit" 		=> __DIR__ . "/uikit.min.css",
	"form-advanced" => __DIR__ . "/components/form-advanced.almost-flat.min.css",
	"form-file" 	=> __DIR__ . "/components/form-file.almost-flat.min.css",
	"datepicker" 	=> __DIR__ . "/components/datepicker.almost-flat.min.css",
	"slidenav" 		=> __DIR__ . "/components/slidenav.almost-flat.min.css",
	"slider" 		=> __DIR__ . "/components/slider.almost-flat.min.css",
	"dotnav" 		=> __DIR__ . "/components/dotnav.almost-flat.min.css",
	"notify" 		=> __DIR__ . "/components/notify.almost-flat.min.css",
	"tooltip" 		=> __DIR__ . "/components/tooltip.almost-flat.min.css",
	"page"			=> __DIR__ . "/pages/page.css",
	"page-home"		=> __DIR__ . "/pages/home.css",
	"page-signup"	=> __DIR__ . "/pages/signup.css",
	"page-login"	=> __DIR__ . "/pages/login.css",
	"page-forgot"	=> __DIR__ . "/pages/forgot.css",
	"page-complete"	=> __DIR__ . "/pages/complete.css",
	"page-profile"	=> __DIR__ . "/pages/profile.css",
	"page-service"	=> __DIR__ . "/pages/service.css",
	"page-search"	=> __DIR__ . "/pages/search.css",
	"page-orders"	=> __DIR__ . "/pages/orders.css",
	"page-messages"	=> __DIR__ . "/pages/messages.css",
	"page-about"	=> __DIR__ . "/pages/about.css",
	"page-support"	=> __DIR__ . "/pages/support.css",
	"page-rsvp"		=> __DIR__ . "/pages/rsvp.css",

	"page-services"		=> __DIR__ . "/pages/services.css",
	"page-services-e"	=> __DIR__ . "/pages/services-edit.css",

	"page-dash"				=> __DIR__ . "/pages/dash.css",
	"page-dash-edit-event"	=> __DIR__ . "/pages/dash-edit-event.css",
	"page-dash-events"		=> __DIR__ . "/pages/dash-events.css",
	"page-dash-cart"		=> __DIR__ . "/pages/dash-cart.css",
	"page-dash-event"		=> __DIR__ . "/pages/dash-event.css",

	"page-dash-event-guests"		=> __DIR__ . "/pages/dash-event-guests.css",
	"page-dash-event-attendance"	=> __DIR__ . "/pages/dash-event-attendance.css",
	"page-dash-event-registration"	=> __DIR__ . "/pages/dash-event-registration.css",
	"page-dash-event-gifts"			=> __DIR__ . "/pages/dash-event-gifts.css",
	"page-dash-event-users"			=> __DIR__ . "/pages/dash-event-users.css",
	"page-dash-event-budget"		=> __DIR__ . "/pages/dash-event-budget.css",
	"page-dash-event-services"		=> __DIR__ . "/pages/dash-event-services.css",
	"page-dash-event-checklist"		=> __DIR__ . "/pages/dash-event-checklist.css",
];

//requested styles
$files = [];
$files[] = CSS_FILE["uikit"]; //add uikit2
$files[] = CSS_FILE["notify"]; //add notify
$files[] = CSS_FILE["form-advanced"]; //add form advanced

//add page specific library styles
if (isset($_GET["p"])){
	switch ($_GET["p"]){

		case "home":
		$files[] = CSS_FILE["datepicker"];
		$files[] = CSS_FILE["slidenav"];
		$files[] = CSS_FILE["dotnav"];
		break;

		case "services-e":
		$files[] = CSS_FILE["form-file"];
		break;

		case "profile":
		$files[] = CSS_FILE["form-file"];
		break;

		case "dash-edit-event":
		$files[] = CSS_FILE["datepicker"];
		$files[] = CSS_FILE["form-file"];
		break;

		case "service":
		$files[] = CSS_FILE["slidenav"];
		$files[] = CSS_FILE["dotnav"];
		break;

		case "search":
		$files[] = CSS_FILE["datepicker"];
		$files[] = CSS_FILE["slidenav"];
		$files[] = CSS_FILE["dotnav"];
		break;

		case "dash-event-checklist":
		$files[] = CSS_FILE["datepicker"];
		break;

		case "dash-event-budget":
		$files[] = CSS_FILE["datepicker"];
		break;

		case "dash-event-gifts":
		$files[] = CSS_FILE["datepicker"];
		$files[] = CSS_FILE["form-file"];
		break;

		case "orders":
		$files[] = CSS_FILE["tooltip"];
		break;
	}
}

//add main styles
$files[] = CSS_FILE["main"];

//add page specific custom styles
if (isset($_GET["p"])){
	switch ($_GET["p"]){

		case "home":
		$files[] = CSS_FILE["page-home"];
		break;

		case "page":
		$files[] = CSS_FILE["page"];
		break;

		case "signup":
		$files[] = CSS_FILE["page-signup"];
		break;

		case "login":
		$files[] = CSS_FILE["page-login"];
		break;

		case "forgot":
		$files[] = CSS_FILE["page-forgot"];
		break;

		case "complete":
		$files[] = CSS_FILE["page-complete"];
		break;

		case "dash":
		$files[] = CSS_FILE["page-dash"];
		break;

		case "services":
		$files[] = CSS_FILE["page-services"];
		break;

		case "services-e":
		$files[] = CSS_FILE["page-services-e"];
		break;

		case "profile":
		$files[] = CSS_FILE["page-services-e"];
		break;

		case "dash-edit-event":
		$files[] = CSS_FILE["page-dash-edit-event"];
		break;

		case "dash-events":
		$files[] = CSS_FILE["page-dash-events"];
		break;

		case "dash-cart":
		$files[] = CSS_FILE["page-dash-cart"];
		break;

		case "dash-event":
		$files[] = CSS_FILE["page-dash-event"];
		break;

		case "dash-event-guests":
		$files[] = CSS_FILE["page-dash-event-guests"];
		break;

		case "service":
		$files[] = CSS_FILE["page-service"];
		break;

		case "search":
		$files[] = CSS_FILE["page-search"];
		break;

		case "orders":
		$files[] = CSS_FILE["page-orders"];
		break;

		case "messages":
		$files[] = CSS_FILE["page-messages"];
		break;

		case "dash-event-attendance":
		$files[] = CSS_FILE["page-dash-event-attendance"];
		break;

		case "dash-event-registration":
		$files[] = CSS_FILE["page-dash-event-registration"];
		break;

		case "dash-event-gifts":
		$files[] = CSS_FILE["page-dash-event-gifts"];
		break;

		case "dash-event-users":
		$files[] = CSS_FILE["page-dash-event-users"];
		break;

		case "dash-event-budget":
		$files[] = CSS_FILE["page-dash-event-budget"];
		break;

		case "dash-event-services":
		$files[] = CSS_FILE["page-dash-event-services"];
		break;

		case "dash-event-checklist":
		$files[] = CSS_FILE["mention"];
		$files[] = CSS_FILE["page-dash-event-checklist"];
		break;

		case "about":
		$files[] = CSS_FILE["page-about"];
		break;

		case "support":
		$files[] = CSS_FILE["page-support"];
		break;

		case "rsvp":
		$files[] = CSS_FILE["page-rsvp"];
		break;
	}
}

//generate output
$buffer = [];
foreach ($files as $file){
	if (file_exists($file) && is_file($file) && is_readable($file)){
		$script = file_get_contents($file);
		if (!COMPRESS_All && COMPRESS_CSS) $script = minifyCSS($script);
		$script = fn1::toStrx($script, true);
		if (strlen($script)){
			$file_info = fn1::pathInfo($file);
			if (array_key_exists("filename", $file_info)) $script = "\r\n/***[~/" . $file_info["filename"] . "]***/\r\n" . $script;
			$buffer[] = $script;
		}
	}
}

//output styles
header("Content-Type: text/css");
$buffer = fn1::toStrx(implode("\r\n", $buffer), true);
if (COMPRESS_All) $buffer = fn1::toStrx(minifyCSS($buffer), true);
echo strlen($buffer) ? SOURCE_CREDIT. $buffer : "";
exit();
