<?php
//helper functions
function event_countdown($event){
	$event_status = $event["event_status"];
	if ($event_status == "Past") return "Past";
	else if ($event_status == "Live") return "Live";
	else {
		$d = $event["time"]["duration"]["now_start"]["d"];
		$h = $event["time"]["duration"]["now_start"]["hours"];
		$m = $event["time"]["duration"]["now_start"]["minutes"];
		$ns = function($n, $w, $s="s"){return "$n " . ($n == 1 ? $w : $w . $s);};
		if ($d) return $ns($d, "Day");
		else {
			$str = "";
			if ($h) $str = $ns($h, "Hr");
			if ($m != 0) $str .= " " . $ns($m, "Min");
			return trim($str);
		}
	}
}
?>
<div class="x-iblock uk-width-1-1">
	<?php if ($guest_user){ ?>
		<span class="uk-align-right x-nowrap uk-text-right x-guest-user">
			<strong>Guest User:</strong> <?php echo $guest_user['names']; ?> <strong><i>@<?php echo $guest_user['username']; ?></i></strong>
			<br><strong>Roles:</strong> <?php echo ['Manage All', 'Guests & Attendance', 'Budget & Orders'][(int) $guest_user['role']]; ?>
		</span>
	<?php } ?>
	<div class="uk-align-left">
		<h1 class="x-event-title x-nomargin">
			<?php echo $event["name"]; ?>
		</h1>
		<small>(<?php echo $event["type"]["name"]; ?>)</small>
	</div>
</div>
<div class="x-manage-cards">
	<div class="uk-grid uk-grid-small uk-grid-width-medium-1-2 uk-grid-width-large-1-3" data-uk-grid-margin data-uk-grid-match>
		<div>
			<div class="x-manage-card x-box uk-height-1-1">
				<div class="card-header">Countdown</div>
				<div class="card-icon">
					<?php $mask_img = $root . "/assets/img/icons/cal.png"; ?>
					<span draggable="false" class="x-mask-image" style="mask-image:url('<?php echo $mask_img; ?>');-webkit-mask-image:url('<?php echo $mask_img; ?>')"></span>
				</div>
				<div class="card-stat"><?php echo event_countdown($event); ?></div>
				<div class="card-text"><?php echo str_replace(" - ", "<br>", $event["display_time"]); ?></div>
			</div>
		</div>
		<div>
			<div class="x-manage-card x-box uk-height-1-1">
				<div class="card-header">Tasks</div>
				<div class="card-icon">
					<?php $mask_img = $root . "/assets/img/icons/plan.png"; ?>
					<span draggable="false" class="x-mask-image" style="mask-image:url('<?php echo $mask_img; ?>');-webkit-mask-image:url('<?php echo $mask_img; ?>')"></span>
				</div>
				<div class="card-stat"><?php echo $event_stats['checklist_checked'] . '/' . $event_stats['checklist_count']; ?></div>
				<div class="card-text">Items</div>
				<div class="card-action">
					<a href="<?php echo $root; ?>/dashboard?event-checklist=<?php echo $event["id"]; ?>" class="uk-button uk-button-primary">Manage</a>
				</div>
			</div>
		</div>
		<div class="<?php if (!in_array($event['role'], [0, 2])) echo 'x-display-none'; ?>">
			<div class="x-manage-card x-box uk-height-1-1">
				<div class="card-header">Event Orders</div>
				<div class="card-icon">
					<?php $mask_img = $root . "/assets/img/icons/locs.png"; ?>
					<span draggable="false" class="x-mask-image" style="mask-image:url('<?php echo $mask_img; ?>');-webkit-mask-image:url('<?php echo $mask_img; ?>')"></span>
				</div>
				<div class="card-stat"><?php echo $event_stats['orders_count']; ?></div>
				<div class="card-text">Orders</div>
				<div class="card-action">
					<a href="<?php echo $root; ?>/dashboard?event-services=<?php echo $event["id"]; ?>" class="uk-button uk-button-primary">Manage</a>
				</div>
			</div>
		</div>
		<div class="<?php if (!in_array($event['role'], [0, 1])) echo 'x-display-none'; ?>">
			<div class="x-manage-card x-box uk-height-1-1">
				<div class="card-header">Guest List</div>
				<div class="card-icon">
					<?php $mask_img = $root . "/assets/img/icons/guests.png"; ?>
					<span draggable="false" class="x-mask-image" style="mask-image:url('<?php echo $mask_img; ?>');-webkit-mask-image:url('<?php echo $mask_img; ?>')"></span>
				</div>
				<div class="card-stat"><?php echo $event["guest_stats"]["count"]; ?></div>
				<div class="card-text">Guests</div>
				<div class="card-action">
					<a href="<?php echo $root; ?>/dashboard?event-guests=<?php echo $event["id"]; ?>" class="uk-button uk-button-primary">Manage</a>
				</div>
			</div>
		</div>
		<div class="<?php if (!in_array($event['role'], [0, 1])) echo 'x-display-none'; ?>">
			<div class="x-manage-card x-box uk-height-1-1">
				<div class="card-header">Attendance</div>
				<div class="card-icon">
					<?php $mask_img = $root . "/assets/img/icons/qr.png"; ?>
					<span draggable="false" class="x-mask-image" style="mask-image:url('<?php echo $mask_img; ?>');-webkit-mask-image:url('<?php echo $mask_img; ?>')"></span>
				</div>
				<div class="card-stat"><?php echo num_commas($event_stats['unique_checkins'], 0); ?></div>
				<div class="card-text">Unique Check-ins</div>
				<div class="card-action">
					<a href="<?php echo $root; ?>/dashboard?event-attendance=<?php echo $event["id"]; ?>" class="uk-button uk-button-primary">Manage</a>
				</div>
			</div>
		</div>
		<div class="<?php if (!in_array($event['role'], [0, 1])) echo 'x-display-none'; ?>">
			<div class="x-manage-card x-box uk-height-1-1">
				<div class="card-header">Registration</div>
				<div class="card-icon">
					<?php $mask_img = $root . "/assets/img/icons/ticket.png"; ?>
					<span draggable="false" class="x-mask-image" style="mask-image:url('<?php echo $mask_img; ?>');-webkit-mask-image:url('<?php echo $mask_img; ?>')"></span>
				</div>
				<div class="card-stat long x-nowrap"><small>KES</small> <?php echo num_commas($event_stats['ticket_sales']); ?></div>
				<div class="card-text">Sales</div>
				<div class="card-action">
					<a href="<?php echo $root; ?>/dashboard?event-registration=<?php echo $event["id"]; ?>" class="uk-button uk-button-primary">Manage</a>
				</div>
			</div>
		</div>
		<div class="<?php if (!in_array($event['role'], [0, 2])) echo 'x-display-none'; ?>">
			<div class="x-manage-card x-box uk-height-1-1">
				<div class="card-header">Budget &amp; Expenses</div>
				<div class="card-icon">
					<?php $mask_img = $root . "/assets/img/icons/budget.png"; ?>
					<span draggable="false" class="x-mask-image" style="mask-image:url('<?php echo $mask_img; ?>');-webkit-mask-image:url('<?php echo $mask_img; ?>')"></span>
				</div>
				<div class="card-stat long x-nowrap"><small>KES</small> <?php echo num_commas($event_stats['budgets_balance']); ?></div>
				<div class="card-text">Balance</div>
				<div class="card-action">
					<a href="<?php echo $root; ?>/dashboard?event-budget=<?php echo $event["id"]; ?>" class="uk-button uk-button-primary">Manage</a>
				</div>
			</div>
		</div>
		<div class="<?php if (!in_array($event['role'], [0, 1])) echo 'x-display-none'; ?>">
			<div class="x-manage-card x-box uk-height-1-1">
				<div class="card-header">Gifts Registry</div>
				<div class="card-icon">
					<?php $mask_img = $root . "/assets/img/icons/gift.png"; ?>
					<span draggable="false" class="x-mask-image" style="mask-image:url('<?php echo $mask_img; ?>');-webkit-mask-image:url('<?php echo $mask_img; ?>')"></span>
				</div>
				<div class="card-stat"><?php echo num_commas($event_stats['gifts_count'], 0); ?></div>
				<div class="card-text">Items</div>
				<div class="card-action">
					<a href="<?php echo $root; ?>/dashboard?event-gifts=<?php echo $event["id"]; ?>" class="uk-button uk-button-primary">Manage</a>
				</div>
			</div>
		</div>
		<div class="<?php if (!in_array($event['role'], [0])) echo 'x-display-none'; ?>">
			<div class="x-manage-card x-box uk-height-1-1">
				<div class="card-header">Event Users</div>
				<div class="card-icon">
					<?php $mask_img = $root . "/assets/img/icons/users.png"; ?>
					<span draggable="false" class="x-mask-image" style="mask-image:url('<?php echo $mask_img; ?>');-webkit-mask-image:url('<?php echo $mask_img; ?>')"></span>
				</div>
				<div class="card-stat"><?php echo num_commas($event_stats['users_count'], 0); ?></div>
				<div class="card-text">Users</div>
				<div class="card-action">
					<a href="<?php echo $root; ?>/dashboard?event-users=<?php echo $event["id"]; ?>" class="uk-button uk-button-primary">Manage</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="x-event-summary">
	<div class="uk-margin-top uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
		<div class="uk-width-large-1-2">
			<div class="x-pad-20 x-box uk-height-1-1">
				<h2>
					<?php echo $event["name"]; ?>
					<a href="<?php echo $root; ?>/dashboard?edit-event=<?php echo $event["id"]; ?>" class="uk-align-right <?php if (!in_array($event['role'], [0])) echo 'x-display-none'; ?>"><i class="uk-icon-pencil"></i></a>
				</h2>
				<p>
					<?php echo $event["description"] != "" ? $event["description"] : "Event has no description"; ?>
					<br><small>Modified On: <?php echo $event["timestamp"]; ?></small>
				</p>
				<hr/>
				<ul>
					<li>
						<i class="uk-icon-calendar"></i>
						<span class="x-fw-400">Starts</span><span class="x-fw-400 uk-hidden-small">&nbsp;On</span>:
						<span class="right summary_starts"><?php echo $event["start"][3]; ?></span>
					</li>
					<li>
						<i class="uk-icon-calendar"></i>
						<span class="x-fw-400">Ends</span><span class="x-fw-400 uk-hidden-small">&nbsp;On</span>:
						<span class="right summary_ends"><?php echo $event["end"][3]; ?></span>
					</li>
					<li>
						<i class="uk-icon-link"></i>
						<span class="x-fw-400 uk-hidden-small">Online&nbsp;</span><span class="x-fw-400">RSVP</span>:
						<span class="right summary_rsvp"><?php echo $event["settings"]["rsvp"] == 1 ? "Enabled" : "Disabled"; ?></span>
					</li>
					<?php if (is_array($event["location"])){ ?>
					<li>
						<i class="uk-icon-map-marker"></i>
						<span class="x-fw-400">Venue Location</span>:
						<span class="right summary_location"><?php echo $event["location"]["name"]; ?></span>
					</li>
					<?php } ?>
				</ul>
				<hr/>
				<h2>Guest Groups</h2>
				<?php if (is_array($event["guest_groups"]) && count($event["guest_groups"])){ ?>
				<ol>
					<?php foreach ($event["guest_groups"] as $group){ ?>
					<li><?php echo sprintf("<strong>%s</strong>", $group["name"]); if ($group["description"] != "") echo sprintf("<p>%s</p>", $group["description"]); ?></li>
					<?php } ?>
				</ol>
				<?php } else echo "<p>No guest groups created.</p>" ?>
				<p class="x-color-b uk-text-center">more info coming soon...</p>
			</div>
		</div>
		<div class="uk-width-large-1-2">
			<a href="javascript:" class="x-block x-event-poster x-watermarked" data-watermark="Event Poster" title="Event Poster">
				<?php if ($event["poster"] != ""){ ?>
				<img class="x-img-cover uk-height-large-1-1 x-watermarked-content-top" src="<?php echo $event["poster"]; ?>" alt="Event Poster" data-onerror="<?php echo $placeholder_image; ?>" onerror="onImageError(this);" />
				<?php } else { ?>
				<img class="default x-img-cover uk-height-large-1-1 x-watermarked-content-top" src="<?php echo $root; ?>/assets/img/dash/week.jpg" alt="Event Poster" />
				<?php } ?>
			</a>
			<?php if (is_array($event["location_map"])){ $location_map_value = str_replace("\"", "'", json_encode(["lat" => $event["location_map"]["lat"], "lng" => $event["location_map"]["lon"], "type" => $event["location_map"]["type"], "name" => $event["location_map"]["name"]]));?>
			<div class="x-event-map uk-margin-small-top" data-value="<?php echo $location_map_value; ?>"></div>
			<?php } ?>
		</div>
	</div>
</div>
