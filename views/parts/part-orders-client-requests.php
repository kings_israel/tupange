<h1>Client Requests</h1>
<p>View order requests from other clients.</p>
<div class="uk-datatable" data-limit="10" data-row-index="1">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Request">
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th tabindex="0" class="x-nowrap">#</th>
						<th tabindex="0" class="x-nowrap">Request</th>
						<th tabindex="0" class="x-nowrap">Title</th>
						<th tabindex="0" class="x-nowrap">Date</th>
						<th tabindex="0" class="x-nowrap">Category</th>
						<th tabindex="0" class="x-nowrap">Budget</th>
						<th tabindex="0" class="x-nowrap">Bids</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($orders) && is_array($orders) && count($orders)){ foreach($orders as $i => $item){ $item["bids"] = 0; ?>
					<?php $data_search = implode(" ", [$item["id"], $$item["service_title"], $item["service_category"]]); ?>
					<tr data-search="<?php echo $data_search; ?>" data-id="<?php echo $item["id"]; ?>" data-seen="<?php echo $item["seen_user"]; ?>" data-seen-vendor="<?php echo $item["seen_vendor"]; ?>" data-toggle="">
						<td class="x-nowrap x-min-50 uk-datatable-row-index" data-value=""></td>
						<td class="x-nowrap" data-value="<?php echo $item["id"]; ?>">
							<a title="Request Details" href="<?php echo $root; ?>/orders/?request=<?php echo $item["id"]; ?>"><strong><?php echo $item["id"]; ?></strong></a>
						</td>
						<td class="x-nowrap" data-value="<?php echo $item["title"]; ?>"><?php echo $item["title"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["time_ordered"]; ?>"><?php echo $item["time_ordered"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["service_category"] ?>"><?php echo $item["service_category"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["pricing_price"] ?>"><?php echo num_commas($item["pricing_price"]); ?>/=</td>
						<td class="x-nowrap" data-value="<?php echo $item["bids"] ?>"><?php echo num_commas($item["bids"], 0); ?>/=</td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
		</div>
		<div class="uk-datatable-no-contents x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
			<div>
				<h3>No Requests</h3>
				<p>Check again later</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
