<h1>Manage Tasks</h1>
<noscript>
	<div class="uk-alert uk-alert-danger" data-uk-alert>
		<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
	</div>
</noscript>
<div class="x-tasks-table x-box uk-form">
	<div class="x-table x-form-table x-overflow-auto-x x-overflow-hidden-y">
		<table class="uk-table">
			<caption class="x-pad-10">Click an item to edit. Press Enter to save/New line.</caption>
			<thead>
				<tr>
					<th>#</th>
					<th>Task <span class="uk-text-danger">*</span></th>
					<th>Person Responsible</th>
					<th class="col-notify">Notify Due</th>
					<th class="col-date x-min-100">Date Due</th>
					<th>Status</th>
					<th class="col-actions">Actions</th>
				</tr>
			</thead>
			<tbody></tbody>
			<tfoot class="x-hidden">
				<th>#</th>
				<th>Task <span class="uk-text-danger">*</span></th>
				<th>Person Responsible</th>
				<th class="col-notify">Notify Due</th>
				<th class="col-date">Date Due</th>
				<th>Status</th>
				<th class="col-actions">Actions</th>
			</tfoot>
		</table>
	</div>
	<div class="x-loading uk-text-center x-pad-20">
		<i class="uk-icon-spinner uk-icon-spin"></i> Loading...
	</div>
</div>
<div id="task_comments" class="uk-offcanvas">
	<div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
		<div class="toolbar"><a id="tc-close" href="#task_comments" data-uk-offcanvas><i class="uk-icon-angle-left"></i></a> Task Comments</div>
		<div class="comments x-border-top">
			<div class="x-pad-20">
				<h1>
					<span id="tc-task">Task Hello world!</span>
					<small id="tc-person"><strong>Assigned To:</strong> <span>Jane Doe</span></small>
					<small id="tc-date"><strong>Date Due:</strong> <span>01/01/2019</span></small>
				</h1>
				<p id="tc-by">By John Doe <small>2019-01-01 12:00:00</small></p>
			</div>
			<div class="x-pad-20 x-border-top thread">
				<div class="nothing uk-text-center x-color-b">
					No comments available. Add a comment using the form below.
				</div>
				<ul></ul>
			</div>
			<div id="tc-form" class="x-pad-20 x-border-top">
				<div class="uk-form uk-margin-large-bottom">
					<div class="uk-form-row">
						<label class="uk-form-label" for="tc-comment">Add Comment <small><i>(as <span id="tc-as"></span>)</i></small></label>
						<div class="uk-form-controls">
							<textarea id="tc-comment" class="uk-width-1-1" placeholder="Type your new comment. Use @username to tag users."></textarea>
						</div>
					</div>
					<div class="uk-form-row">
						<div id="tc-attachment">
							<span>Attachment File.xlsx</span>
							<a href="javascript:"><i class="uk-icon-close"></i></a>
						</div>
						<div id="tc-alert"></div>
						<div class="uk-text-right">
							<input id="tc-file" class="x-hidden" type="file" accept="<?php echo $file_exts; ?>">
							<button id="tc-attach" class="uk-button uk-button-white"><i class="uk-icon-paperclip"></i> Attachment</button>
							<button id="tc-save" class="uk-button uk-button-primary">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
