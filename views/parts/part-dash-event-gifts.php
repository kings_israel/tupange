<h1>Gift Registry</h1>
<p>Record your gifts here and keep track if its value.</p>
<nav class="uk-navbar x-toolbar">
	<span class="uk-navbar-brand">
		<span class="x-fw-400 x-stats-count"><?php echo num_commas($gifts_stats["count"], 0); ?></span> Gifts
		| <span class="x-fw-400"><small>KES</small> <span class='x-stats-value'><?php echo num_commas($gifts_stats["value"]); ?></span></span> Value
	</span>
	<div class="uk-navbar-content uk-navbar-flip  uk-hidden-small">
		<button title="Download" class="x-toolbar-download uk-button uk-button-primary uk-margin-small-left"><i class="uk-icon-download"></i><span class="uk-hidden-medium"> Download</span></button>
		<button title="Print" class="x-toolbar-print uk-button uk-button-primary uk-margin-small-left"><i class="uk-icon-download"></i><span class="uk-hidden-print"> Print</span></button>
	</div>
	<div class="uk-visible-small">
		<ul class="uk-navbar-nav uk-navbar-flip">
			<li class="uk-parent" data-uk-dropdown="{mode:'click'}">
				<a href="javascript:" class="uk-navbar-toggle"></a>
				<div class="uk-dropdown uk-dropdown-navbar x-min-150">
					<ul class="uk-nav uk-nav-navbar">
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-download"><i class="uk-icon-download"></i> Download List</a></li>
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-print"><i class="uk-icon-print"></i> Print List</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
</nav>
<div class="uk-datatable uk-margin-top" data-limit="10" data-row-index="-1">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Gifts">
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th tabindex="0" class="x-nowrap uk-datatable-row-index">#</th>
						<th class="x-nowrap x-ignore">Image</th>
						<th tabindex="0" class="x-nowrap">Title</th>
						<th tabindex="0" class="x-nowrap">Description</th>
						<th tabindex="0" class="x-nowrap">Value</th>
						<th tabindex="0" class="x-nowrap">Received By</th>
						<th tabindex="0" class="x-nowrap">Received Date</th>
						<th tabindex="0" class="x-nowrap">Received From</th>
						<th tabindex="0" class="x-nowrap">Phone</th>
						<th tabindex="0" class="x-nowrap">Email</th>
						<th tabindex="0" class="x-nowrap">Notes</th>
						<th tabindex="0" class="x-nowrap">Timestamp</th>
						<th class="x-nowrap x-block">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($gifts) && is_array($gifts) && count($gifts)){ foreach ($gifts as $i => $item){ ?>
					<?php $item_search = implode(" ", [$item["title"], $item["description"], $item["value"], $item["received_by"], $item["received_date"], $item["received_from"], $item["phone"], $item["email"], $item["notes"]]); ?>
					<tr data-id="<?php echo $item["id"]; ?>" data-value="<?php echo $item["value"]; ?>" data-search="<?php echo $item_search; ?>" data-base64="<?php echo base64_encode(json_encode($item)); ?>">
						<td class="x-nowrap uk-datatable-row-index" data-value=""></td>
						<td class="x-nowrap x-min-150 x-max-150"><?php if (strlen($gift_image = trim($item["image"]))) echo '<img draggable="false" src="' . $gift_image . '" class="x-service-image" />'; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["title"]; ?>"><?php echo $item["title"]; ?></td>
						<td class="x-min-150" data-value="<?php echo $item["description"]; ?>"><?php echo $item["description"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["value"]; ?>"><?php echo num_commas($item["value"]); ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["received_by"]; ?>"><?php echo $item["received_by"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["received_date"]; ?>"><?php echo $item["received_date"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["received_from"]; ?>"><?php echo $item["received_from"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["phone"]; ?>"><?php echo $item["phone"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["email"]; ?>"><?php echo $item["email"]; ?></td>
						<td class="x-min-200" data-value="<?php echo $item["notes"]; ?>"><?php echo $item["notes"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["timestamp"]; ?>"><?php echo $item["timestamp"]; ?></td>
						<td class="x-nowrap uk-text-center">
							<a title="Edit" draggable="false" href="javascript:" data-id="<?php echo $item["id"]; ?>" class="gift-edit uk-button uk-button-small uk-button-primary"><i class="uk-icon-pencil"></i></a>
							<a title="Delete" draggable="false" href="javascript:" data-id="<?php echo $item["id"]; ?>" class="gift-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>
						</td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
		</div>
		<div class="uk-datatable-no-content x-nothing x-border-bottom x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
			<div>
				<h3>No Gifts</h3>
				<p>Get started by adding an entry to your records.</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-border-bottom x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
		<div class="x-edit-gift-add uk-text-right x-pad-20">
			<button class="uk-button uk-button-success">Register Gift</button>
		</div>
		<form id="gift_form" action="javascript:" class="x-pad-20 uk-form x-border-top x-hidden">
			<h2>
				REGISTER GIFT
				<a href="#" id="gift_cancel_mini" class="uk-align-right x-color-b" draggable="false"><i class="uk-icon-close"></i></a>
				<a href="#" id="gift_save_mini" class="uk-align-right" draggable="false"><i class="uk-icon-save"></i></a>
			</h2>
			<p><strong>NOTE:</strong> Fields marked <span class="uk-text-danger">*</span> must be provided</p>
			<div class="uk-form-row">
				<input name="guest_id" type="hidden">
				<div class="uk-grid" data-uk-grid-margin>
					<div class="uk-width-large-1-3">
						<h2>Gift Image</h2>
						<div class="uk-margin-small-top x-gift-img">
							<input id="gift_image_data" type="hidden" value="">
							<img draggable="false" id="gift_image_display" class="uk-width-1-1" src="<?php echo $placeholder_image; ?>" />
							<div class="uk-margin-top">
								<div class="uk-form-file uk-width-1-1">
									<button id="gift_image_btn" class="uk-button uk-button-white uk-width-1-1">Select Image</button>
									<input id="gift_image_file" type="file" accept=".jpg,.jpeg,.png,.gif">
								</div>
								<button id="gift_image_remove" class="uk-button uk-button-white uk-width-1-1 uk-margin-small-top x-hidden">Remove Image</button>
							</div>
						</div>
					</div>
					<div class="uk-width-large-2-3">
						<h2>Gift Details</h2>
						<div class="uk-grid uk-grid-small" data-uk-grid-margin>
							<div class="uk-width-medium-1-1">
								<input id="gift_id" type="hidden" value="">
								<div class="uk-form-row">
									<label class="uk-form-label" for="gift_title">Title <span class="uk-text-danger">*</span></label>
									<div class="uk-form-controls">
										<input id="gift_title" name="gift_title" class="uk-width-1-1" onkeyup="this.value = camelCase(this.value)" type="text" placeholder="Title">
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-1">
								<div class="uk-form-row">
									<label class="uk-form-label" for="gift_description">Description</label>
									<div class="uk-form-controls">
										<textarea id="gift_description" name="gift_description" class="uk-width-1-1" placeholder="Description" onkeyup="this.value = sentenseCase(this.value)"></textarea>
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-2">
								<div class="uk-form-row">
									<label class="uk-form-label" for="gift_value">Value</label>
									<div class="uk-form-controls">
										<input id="gift_value" name="gift_value" class="uk-width-1-1" type="number" placeholder="KES 0" onkeypress="onKeyPressNumber(event, true)" onblur="this.value = num(this.value, '')">
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-2">
								<div class="uk-form-row">
									<label class="uk-form-label" for="gift_received_date">Received Date</label>
									<div class="uk-form-controls">
										<div class="uk-form-icon uk-form-icon-flip uk-width-1-1">
											<i class="uk-icon-calendar"></i>
											<input type="text" class="uk-width-1-1" id="gift_received_date" name="gift_received_date" placeholder="DD/MM/YYYY" data-uk-datepicker="{format:'DD/MM/YYYY',pos:'bottom'}">
										</div>
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-2">
								<div class="uk-form-row">
									<label class="uk-form-label" for="gift_received_by">Received By</label>
									<div class="uk-form-controls">
										<input id="gift_received_by" name="gift_received_by" class="uk-width-1-1" onkeyup="this.value = camelCase(this.value)" type="text" placeholder="Received By">
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-2">
								<div class="uk-form-row">
									<label class="uk-form-label" for="gift_received_from">Received From</label>
									<div class="uk-form-controls">
										<input id="gift_received_from" name="gift_received_from" class="uk-width-1-1" onkeyup="this.value = camelCase(this.value)" type="text" placeholder="Received From">
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-2">
								<div class="uk-form-row">
									<label class="uk-form-label" for="gift_phone">Phone</label>
									<div class="uk-form-controls">
										<input id="gift_phone" name="gift_phone" class="uk-width-1-1" type="text" placeholder="Phone">
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-2">
								<div class="uk-form-row">
									<label class="uk-form-label" for="gift_email">Email</label>
									<div class="uk-form-controls">
										<input id="gift_email" name="gift_email" class="uk-width-1-1" type="email" placeholder="Email">
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-1">
								<div class="uk-form-row">
									<label class="uk-form-label" for="gift_notes">Notes</label>
									<div class="uk-form-controls">
										<textarea id="gift_notes" name="gift_notes" class="uk-width-1-1" placeholder="Notes" onkeyup="this.value = sentenseCase(this.value)"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-margin-top uk-text-right">
					<button title="Cancel Editing" id="gift_edit_cancel" type="button" class="uk-button uk-button-white">Cancel</button>
					<button title="Save Details" id="gift_edit_save" type="submit" class="uk-button uk-button-success x-min-150">Save Details</button>
					<button title="Please wait..." id="gift_edit_saving" type="button" class="uk-button uk-button-default uk-disabled x-min-150 x-hidden" disabled="disabled"><i class="uk-icon-spinner uk-icon-spin"></i> Saving...</button>
				</div>
			</div>
		</form>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
<script type="text/javascript">
	let EVENT_DATA = <?php echo json_encode($event); ?>;
</script>
