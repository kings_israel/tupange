<h1>My Cart <a href="<?php echo $root; ?>/search?origin=cart" class="uk-button uk-button-success uk-align-right">Shop Services</a></h1>
<p>Check-out cart to request quotations or add selected services to your event's shortlisted services.</p>
<div class="uk-datatable" data-limit="5">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Cart Items">
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th class="x-nowrap x-ignore">Service</th>
						<th tabindex="0" class="x-nowrap">Details</th>
						<th tabindex="0" class="x-nowrap">Time Added</th>
						<th class="x-nowrap x-ignore">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($cart_items) && is_array($cart_items) && count($cart_items)){ foreach($cart_items as $i => $item){ ?>
					<tr data-search="<?php echo $item["title"] . " " . $item["data"]["service"]["description"] . " " . $item["data"]["service"]["category_name"] . " " . $item["data"]["vendor"]["company_name"]; ?>">
						<td class="x-min-150 x-nowrap">
							<a href="<?php echo $root; ?>/service/<?php echo $item["data"]["service"]["id"]; ?>">
								<img class="x-service-image" draggable="false" src="<?php echo $item["data"]["service"]["image_src"]; ?>" />
							</a>
						</td>
						<td class="x-min-200" data-value="<?php echo $item["data"]["service"]["title"] ?>">
							<a href="<?php echo $root; ?>/service/<?php echo $item["data"]["service"]["id"]; ?>"><?php echo $item["data"]["service"]["title"]; ?></a>
							<br><?php echo $item["data"]["service"]["description"]; ?>
							<br><small><i><strong>Category: </strong><?php echo $item["data"]["service"]["category_name"]; ?></i></small>
							<br><small><i><strong>Company: </strong><?php echo $item["data"]["vendor"]["company_name"]; ?></i></small>
						</td>
						<td class="x-min-100 x-nowrap" data-value="<?php echo $item["timestamp"]; ?>"><?php echo $item["timestamp"]; ?></td>
						<td class="x-nowrap uk-text-center">
							<a title="Remove" draggable="false" href="javascript:" data-id="<?php echo $item["id"]; ?>" data-name="<?php echo $item["data"]["service"]["title"]; ?>" class="item-remove uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>
						</td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
		</div>
		<div class="uk-datatable-no-contents x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
			<div>
				<h3>Cart Empty</h3>
				<p>Shop services and add your selections to cart so you can create orders.</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-hidden x-border-bottom">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
		<div class="uk-text-right x-pad-20 x-cart-checkout-wrapper <?php if (!count($cart_items)) echo "x-hidden"; ?>">
			<button class="x-cart-checkout uk-button uk-button-primary x-min-200"><i class="uk-icon-cart-arrow-down"></i> Checkout</button>
		</div>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
<script type="text/javascript">
	let CART_ITEMS = <?php echo json_encode($cart_items); ?>;
	let USER_EVENTS = <?php echo json_encode($user_events); ?>;
</script>
