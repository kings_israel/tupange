<?php
#meta tags / page vars
if (!isset($page_title)) $page_title = "Tupange | Events & Services";
if (!isset($page_type)) $page_type = "page";
if (!isset($page_theme)) $page_theme = "#202020";
if (!isset($page_name)) $page_name = "Tupange Events";
if (!isset($page_description)) $page_description = "Tupange Events & Services";
if (!isset($page_image)) $page_image = $root . "/site.jpg";
if (!isset($page_author)) $page_author = "Tupange Events";
if (!isset($page_url)) $page_url = $root;
if (!isset($page_tags)) $page_tags = "tupange,events,services,event services,occasion,planning,wedding,party,meeting,photography,videography,florist,transport,djs,dj,entertainment,cakes,catering,etc.";
if (!isset($page_published_time)) $page_published_time = "2019-06-05T00:00:00";
if (!isset($page_modified_time)) $page_modified_time = "2019-06-05T00:00:00";
if (!isset($page_twitter_card)) $page_twitter_card = "summary_large_image";
if (!isset($page_twitter_site)) $page_twitter_site = "@tupangeofficial";
if (!isset($page_twitter_author)) $page_twitter_author = "@tupangeofficial";
if (!isset($page_fb_app_id)) $page_fb_app_id = "2037165133048080";
if (!isset($page_favicon)) $page_favicon = $root . "/assets/img/favicon.ico";
if (!isset($page_icon)) $page_icon = $root . "/assets/img/icon.png";
if (!isset($page_cache)) $page_cache = false;
if (!isset($page_twitter_tags)) $page_twitter_tags = true;
if (!isset($page_og_tags)) $page_og_tags = true;
if (!isset($page_article_tags)) $page_article_tags = true;
if (!isset($page_interop_tags)) $page_interop_tags = true;

#create meta tags (array object)
$meta = [];
$meta[] = ["charset" => "utf-8"];
$meta[] = ["name" => "viewport", "content" => "width=device-width, initial-scale=1"];
if (!$page_cache){
	$meta[] = ["http-equiv" => "Cache-Control", "content" => "no-cache, no-store, must-revalidate"];
	$meta[] = ["http-equiv" => "Pragma", "content" => "no-cache"];
	$meta[] = ["http-equiv" => "Expires", "content" => "0"];
}
if (strlen($page_theme)) $meta[] = ["name" => "theme-color", "content" => $page_theme];
if (strlen($page_description)) $meta[] = ["name" => "description", "content" => $page_description];
if (strlen($page_tags)) $meta[] = ["name" => "keywords", "content" => $page_tags];
if ($page_interop_tags){
	if (strlen($page_name)) $meta[] = ["itemprop" => "name", "content" => $page_name];
	if (strlen($page_description)) $meta[] = ["itemprop" => "description", "content" => $page_description];
	if (strlen($page_image)) $meta[] = ["itemprop" => "image", "content" => $page_image];
}
if ($page_twitter_tags){
	if (strlen($page_twitter_card)) $meta[] = ["name" => "twitter:card", "content" => $page_twitter_card];
	if (strlen($page_twitter_site)) $meta[] = ["name" => "twitter:site", "content" => $page_twitter_site];
	if (strlen($page_twitter_author)) $meta[] = ["name" => "twitter:creator", "content" => $page_twitter_author];
	if (strlen($page_title)) $meta[] = ["name" => "twitter:title", "content" => $page_title];
	if (strlen($page_description)) $meta[] = ["name" => "twitter:description", "content" => $page_description];
	if (strlen($page_image)) $meta[] = ["name" => "twitter:image", "content" => $page_image];
}
if ($page_og_tags){
	if (strlen($page_title)) $meta[] = ["property" => "og:title", "content" => $page_title];
	if (strlen($page_type)) $meta[] = ["property" => "og:type", "content" => $page_type];
	if (strlen($page_url)) $meta[] = ["property" => "og:url", "content" => $page_url];
	if (strlen($page_image)) $meta[] = ["property" => "og:image", "content" => $page_image];
	if (strlen($page_description)) $meta[] = ["property" => "og:description", "content" => $page_description];
	if (strlen($page_name)) $meta[] = ["property" => "og:site_name", "content" => $page_name];
}
if ($page_article_tags){
	if (strlen($page_published_time)) $meta[] = ["property" => "article:published_time", "content" => $page_published_time];
	if (strlen($page_modified_time)) $meta[] = ["property" => "article:modified_time", "content" => $page_modified_time];
	if (strlen($page_name)) $meta[] = ["property" => "article:section", "content" => $page_name];
	if (strlen($page_tags)) $meta[] = ["property" => "article:tag", "content" => $page_tags];
}
if (strlen($page_fb_app_id)) $meta[] = ["property" => "fb:app_id", "content" => $page_fb_app_id];

#create tags html
$buffer = "";
foreach ($meta as $meta_tag){
	$tag_content = "";
	foreach ($meta_tag as $key => $value) $tag_content .= sprintf(" %s=\"%s\"", $key, $value);
	$buffer .= sprintf("\t<meta %s>\r\n", trim($tag_content));
}

#add icons and title
$buffer .= sprintf("\t<link rel=\"shortcut icon\" href=\"%s\" type=\"image/x-icon\">\r\n", $page_icon);
$buffer .= sprintf("\t<link rel=\"apple-touch-icon-precomposed\" href=\"%s\">\r\n", $page_icon);
$buffer .= sprintf("\t<title>%s</title>\r\n", $page_title);

#display meta html
echo $buffer;
