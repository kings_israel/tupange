<h1>Event Registration</h1>
<p>Please note that this is a different list from your guest list. During attendance management, the invited guest list and registered guests list is combined so you can track attendance on both lists.</p>
<nav class="uk-navbar x-toolbar">
	<span class="uk-navbar-brand">
		<span class="x-fw-400 x-stats-registered"><?php echo num_commas($registration_stats["registered"], 0); ?></span> Guests
		| <span class="x-fw-400"><small>KES</small> <span class="x-stats-sales"><?php echo num_commas($registration_stats["sales"]); ?></span></span> Sales
	</span>
	<div class="uk-navbar-content uk-navbar-flip  uk-hidden-small">
		<button title="Manage Registration" class="x-toolbar-registration uk-button uk-button-white uk-margin-small-left x-hidden"><i class="uk-icon-edit"></i> Manage Registration</button>
		<button title="Manage Ticket Settings" class="x-toolbar-tickets uk-button uk-button-white uk-margin-small-left"><i class="uk-icon-edit"></i> Manage Tickets</button>
		<button title="Download" class="x-toolbar-download uk-button uk-button-primary uk-margin-small-left"><i class="uk-icon-download"></i></button>
		<div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
            <button title="Print" class="uk-button uk-button-primary uk-margin-small-left"><i class="uk-icon-print"></i></button>
            <div class="uk-dropdown " aria-hidden="true">
                <ul class="uk-nav uk-nav-navbar">
                    <li><a href="javascript:" class="uk-dropdown-close x-toolbar-print-list">Print Guest List</a></li>
                    <li><a href="javascript:" class="uk-dropdown-close x-toolbar-print-tickets">Print Tickets</a></li>
                </ul>
            </div>
        </div>
	</div>
	<div class="uk-visible-small">
		<ul class="uk-navbar-nav uk-navbar-flip">
			<li class="uk-parent" data-uk-dropdown="{mode:'click'}">
				<a href="javascript:" class="uk-navbar-toggle"></a>
				<div class="uk-dropdown uk-dropdown-navbar x-min-150">
					<ul class="uk-nav uk-nav-navbar">
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-tickets"><i class="uk-icon-edit"></i> Tickets</a></li>
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-registration"><i class="uk-icon-edit"></i> Registration</a></li>
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-download"><i class="uk-icon-download"></i> Download List</a></li>
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-print-list"><i class="uk-icon-print"></i> Print Guest List</a></li>
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-print-tickets"><i class="uk-icon-print"></i> Print Tickets</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
</nav>
<div class="x-manage-guests">
	<div class="uk-datatable uk-margin-top" data-limit="10" data-row-index="-1">
		<div class="uk-form-icon uk-width-1-1">
			<i class="uk-icon-search"></i>
			<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Registered Guest">
		</div>
		<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
			<noscript>
				<div class="x-pad-20">
					<div class="uk-alert uk-alert-danger" data-uk-alert>
						<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
					</div>
				</div>
			</noscript>
			<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
				<table class="uk-table">
					<thead>
						<tr>
							<th tabindex="0" class="x-nowrap uk-datatable-row-index">#</th>
							<th tabindex="0" class="x-nowrap">Ticket</th>
							<th tabindex="0" class="x-nowrap">Price</th>
							<th tabindex="0" class="x-nowrap">Guests</th>
							<th tabindex="0" class="x-nowrap">Amount</th>
							<th tabindex="0" class="x-nowrap">Barcode</th>
							<th tabindex="0" class="x-nowrap">Names</th>
							<th tabindex="0" class="x-nowrap">Phone</th>
							<th tabindex="0" class="x-nowrap">Email</th>
							<th tabindex="0" class="x-nowrap">Address</th>
							<th tabindex="0" class="x-nowrap">Company</th>
							<th tabindex="0" class="x-nowrap">Status</th>
							<th tabindex="0" class="x-nowrap">Notes</th>
							<th tabindex="0" class="x-nowrap">Time Created</th>
							<th tabindex="0" class="x-nowrap">Time Modified</th>
							<th tabindex="0" class="x-nowrap">Ticket Sent</th>
							<th class="x-nowrap x-ignore x-italics x-color-b">Names</th>
							<th class="x-nowrap x-ignore uk-text-center">
								<div class="uk-button-dropdown x-bulk-actions" data-uk-dropdown="{mode:'click'}">
	                                <a href="javascript:" class="x-pad-10a"><i class="uk-icon-check-square-o"></i></a>
	                                <div class="uk-dropdown x-drop-mini uk-text-left">
	                                    <ul class="uk-nav uk-nav-dropdown">
	                                        <li><a data-action="select-all" class="uk-dropdown-close"><i class="uk-icon-check-square"></i> Select All</a></li>
	                                        <li><a data-action="select-none" class="uk-dropdown-close"><i class="uk-icon-check-square-o"></i> Select None</a></li>
											<li class="uk-nav-divider"></li>
	                                        <li><a data-action="send-tickets" class="uk-dropdown-close"><i class="uk-icon-send"></i> Send Tickets</a></li>
											<li class="uk-nav-divider"></li>
	                                        <li><a data-action="print-list" class="uk-dropdown-close"><i class="uk-icon-print"></i> Print Guest List</a></li>
	                                        <li><a data-action="print-tickets" class="uk-dropdown-close"><i class="uk-icon-print"></i> Print Tickets</a></li>
											<li><a data-action="download-list" class="uk-dropdown-close"><i class="uk-icon-download"></i> Download Guest List</a></li>
											<li class="uk-nav-divider"></li>
											<li><a data-action="delete" class="uk-dropdown-close uk-text-danger"><i class="uk-icon-trash"></i> Delete</a></li>
	                                    </ul>
	                                </div>
	                            </div>
							</th>
							<th class="x-nowrap x-block">Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($registration_list) && is_array($registration_list) && count($registration_list)){ foreach ($registration_list as $i => $guest){ ?>
						<?php $guest_search = implode(" ", [$guest["barcode"], $guest["names"], $guest["phone"], $guest["email"], $guest["address"], $guest["company"]]); ?>
						<tr id="guest-<?php echo $guest["id"]; ?>" data-id="<?php echo $guest["id"]; ?>" data-search="<?php echo $guest_search; ?>" data-base64="<?php echo base64_encode(json_encode($guest)); ?>">
							<td class="x-nowrap x-min-50 uk-datatable-row-index" data-value=""></td>
							<td class="x-nowrap x-min-100" data-name="ticket_title" data-value="<?php echo $guest["ticket_title"]; ?>"><?php echo $guest["ticket_title"]; ?></td>
							<td class="x-nowrap x-min-100" data-name="ticket_price" data-value="<?php echo $guest["ticket_price"]; ?>"><?php echo num_commas($guest["ticket_price"]); ?></td>
							<td class="x-nowrap x-min-100" data-name="guests" data-value="<?php echo $guest["guests"]; ?>"><?php echo $guest["guests"]; ?></td>
							<td class="x-nowrap x-min-100" data-name="amount" data-value="<?php echo $guest["amount"]; ?>"><?php echo num_commas($guest["amount"]); ?></td>
							<td class="x-nowrap x-min-100" data-name="barcode" data-value="<?php echo $guest["barcode"]; ?>"><?php echo $guest["barcode"]; ?></td>
							<td class="x-nowrap x-min-100" data-name="names" data-value="<?php echo $guest["names"]; ?>"><?php echo $guest["names"]; ?></td>
							<td class="x-nowrap x-min-100" data-name="phone" data-value="<?php echo $guest["phone"]; ?>"><?php echo $guest["phone"]; ?></td>
							<td class="x-nowrap x-min-100" data-name="email" data-value="<?php echo $guest["email"]; ?>"><?php echo $guest["email"]; ?></td>
							<td class="x-min-150" data-name="address" data-value="<?php echo $guest["address"]; ?>"><?php echo $guest["address"]; ?></td>
							<td class="x-min-150" data-name="company" data-value="<?php echo $guest["company"]; ?>"><?php echo $guest["company"]; ?></td>
							<td class="x-nowrap x-min-100" data-name="status_text" data-value="<?php echo $guest["status_text"]; ?>"><?php echo $guest["status_text"]; ?></td>
							<td class="x-min-150" data-name="notes" data-value="<?php echo $guest["notes"]; ?>"><?php echo $guest["notes"]; ?></td>
							<td class="x-nowrap x-min-100" data-name="time_created" data-value="<?php echo $guest["time_created"]; ?>"><?php echo $guest["time_created"]; ?></td>
							<td class="x-nowrap x-min-100" data-name="time_modified" data-value="<?php echo $guest["time_modified"]; ?>"><?php echo $guest["time_modified"]; ?></td>
							<td class="x-nowrap x-min-100" data-name="sent" data-value="<?php echo $guest["sent"]; ?>"><?php echo $guest["sent"]; ?></td>
							<td class="x-nowrap x-min-100 x-italics x-color-b"><?php echo $guest["names"]; ?></td>
							<td class="uk-text-center"><div class="uk-form"><label class="x-checkbox"><input class="x-bulk-action" data-id="<?php echo $guest["id"]; ?>" type="checkbox"></label></div></td>
							<td class="x-nowrap uk-text-center">
								<a title="Preview" draggable="false" href="javascript:" data-id="<?php echo $guest["id"]; ?>" class="guest-preview uk-button uk-button-small uk-button-white"><i class="uk-icon-send"></i> Ticket</a>
								<a title="Edit" draggable="false" href="javascript:" data-id="<?php echo $guest["id"]; ?>" class="guest-edit uk-button uk-button-small uk-button-primary"><i class="uk-icon-pencil"></i></a>
								<a title="Delete" draggable="false" href="javascript:" data-id="<?php echo $guest["id"]; ?>" data-names="<?php echo $guest["names"]; ?>" class="guest-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>
							</td>
						</tr>
						<?php }} ?>
					</tbody>
				</table>
			</div>
			<div class="uk-datatable-no-content x-nothing x-border-bottom x-hidden">
				<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
				<div>
					<h3>No Guests</h3>
					<p>Get started by managing ticket types and continue to add guests.</p>
				</div>
			</div>
			<div class="uk-datatable-no-results x-nothing x-border-bottom x-hidden">
				<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
				<div>
					<h3>No Results Found</h3>
					<p>Refine your search/filters to get different results.</p>
				</div>
			</div>
			<div class="x-edit-guest-toggle uk-text-right x-pad-20">
				<button class="uk-button uk-button-success">Register Guest</button>
			</div>
			<form id="guest_form" action="javascript:" class="x-pad-20 uk-form x-border-top x-hidden">
				<h2>
					Register Guest
					<a href="#" id="guest_cancel_mini" class="uk-align-right x-color-b" draggable="false"><i class="uk-icon-close"></i></a>
					<a href="#" id="guest_save_mini" class="uk-align-right" draggable="false"><i class="uk-icon-save"></i></a>
				</h2>
				<p><strong>NOTE:</strong> Fields marked <span class="uk-text-danger">*</span> must be provided.</p>
				<div id="guest_error"></div>
				<div class="uk-form-row">
					<input id="guest_id" name="guest_id" type="hidden">
					<div class="uk-grid uk-grid-small" data-uk-grid-margin>
						<div class="uk-margin-top uk-width-large-1-2">
							<div class="uk-form-row">
								<label class="uk-form-label" for="guest_ticket">Ticket <span class="uk-text-danger">*</span></label>
								<div class="uk-form-controls">
									<select data-validate="required" id="guest_ticket" name="guest_ticket" class="uk-width-1-1" data-default="0">
										<option value="" selected="selected">Select...</option>
									</select>
								</div>
							</div>
						</div>
						<div class="uk-margin-top uk-width-medium-1-2 uk-width-large-1-4">
							<div class="uk-form-row">
								<label class="uk-form-label" for="guest_ticket_guests">No. Of Guests</label>
								<div class="uk-form-controls">
									<input id="guest_ticket_guests" name="guest_ticket_guests" class="uk-width-1-1" type="number" placeholder="1" onkeypress="onKeyPressNumber(event, true, true)" onblur="this.value = num(this.value, '')" >
								</div>
							</div>
						</div>
						<div class="uk-margin-top uk-width-medium-1-2 uk-width-large-1-4">
							<div class="uk-form-row">
								<label class="uk-form-label" for="ticket_amount">Total Amount</label>
								<div class="uk-form-controls">
									<input id="ticket_amount" class="uk-width-1-1" type="text" placeholder="KES 0" disabled readonly>
								</div>
							</div>
						</div>
						<div class="uk-margin-top uk-width-medium-1-2">
							<div class="uk-form-row">
								<label class="uk-form-label" for="guest_names">Guest Names <span class="uk-text-danger">*</span></label>
								<div class="uk-form-controls">
									<input data-validate="required" id="guest_names" name="guest_names" class="uk-width-1-1" onkeyup="this.value = camelCase(this.value)" type="text" placeholder="Full Names">
								</div>
							</div>
						</div>
						<div class="uk-margin-top uk-width-medium-1-2">
							<div class="uk-form-row">
								<label class="uk-form-label" for="guest_phone">Phone</label>
								<div class="uk-form-controls">
									<input data-validate="required" id="guest_phone" name="guest_phone" class="uk-width-1-1" type="text" placeholder="Phone Number">
								</div>
							</div>
						</div>
						<div class="uk-margin-top uk-width-medium-1-2">
							<div class="uk-form-row">
								<label class="uk-form-label" for="guest_email">Email</label>
								<div class="uk-form-controls">
									<input data-validate="required" id="guest_email" name="guest_email" class="uk-width-1-1" type="text" placeholder="Email Address">
								</div>
							</div>
						</div>
						<div class="uk-margin-top uk-width-medium-1-2">
							<div class="uk-form-row">
								<label class="uk-form-label" for="guest_names">Company</label>
								<div class="uk-form-controls">
									<input id="guest_company" name="guest_company" class="uk-width-1-1" type="text" placeholder="Company/Organization" onkeyup="this.value = sentenseCase(this.value)">
								</div>
							</div>
						</div>
						<div class="uk-margin-top uk-width-medium-1-2">
							<div class="uk-form-row">
								<label class="uk-form-label" for="guest_address">Address</label>
								<div class="uk-form-controls">
									<textarea id="guest_address" name="guest_address" class="uk-width-1-1" placeholder="Postal/Physical Address" onkeyup="this.value = sentenseCase(this.value)"></textarea>
								</div>
							</div>
						</div>
						<div class="uk-margin-top uk-width-medium-1-2">
							<div class="uk-form-row">
								<label class="uk-form-label" for="guest_notes">Notes</label>
								<div class="uk-form-controls">
									<textarea id="guest_notes" name="guest_notes" class="uk-width-1-1" placeholder="Notes" onkeyup="this.value = sentenseCase(this.value)"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-margin-top uk-text-right">
						<button title="Cancel Editing" id="guest_cancel" type="button" class="uk-button uk-button-white">Cancel</button>
						<button title="Save Details" id="guest_save" type="submit" class="uk-button uk-button-success x-min-150">Save Details</button>
						<button title="Please wait..." id="guest_saving" type="button" class="uk-button uk-button-default uk-disabled x-min-150 x-hidden" disabled="disabled"><i class="uk-icon-spinner uk-icon-spin"></i> Saving...</button>
					</div>
				</div>
			</form>
		</div>
		<div class="uk-datatable-pagination"></div>
	</div>
</div>
<div class="x-manage-tickets x-hidden">
	<div class="x-box uk-margin-top">
		<div class="x-pad-20 x-border-bottom">
			<h2 class="x-nomargin">Manage Tickets</h2>
		</div>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table uk-table-striped">
				<thead>
					<tr>
						<th class="x-nowrap">#</th>
						<th class="x-nowrap">Ticket Title</th>
						<th class="x-nowrap">Ticket Price</th>
						<th class="x-nowrap">Guests Limit</th>
						<th class="x-nowrap">Description</th>
						<th class="x-nowrap">Timestamp</th>
						<th class="x-nowrap">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($ticket_settings as $i => $ticket){ ?>
					<tr data-id="<?php echo $ticket["id"]; ?>">
						<td class="x-nowrap x-ticket-index"></td>
						<td class="x-nowrap x-ticket-title x-min-100"><?php echo $ticket["title"]; ?></td>
						<td class="x-nowrap x-ticket-price x-min-100"><small>KES</small> <?php echo num_commas($ticket["price"]); ?></td>
						<td class="x-nowrap x-ticket-limit x-min-100"><?php echo num_commas($ticket["guests_limit"], 0); ?></td>
						<td class="x-min-200 x-ticket-description"><?php echo $ticket["description"]; ?></td>
						<td class="x-nowrap x-min-100 x-ticket-timestamp"><?php echo $ticket["timestamp"]; ?></td>
						<td class="x-nowrap uk-text-center">
							<a title="Edit" draggable="false" href="javascript:" data-id="<?php echo $ticket["id"]; ?>" class="ticket-edit uk-button uk-button-small uk-button-primary"><i class="uk-icon-pencil"></i></a>
							<a title="Delete" draggable="false" href="javascript:" data-id="<?php echo $ticket["id"]; ?>" data-title="<?php echo $ticket["title"]; ?>" class="ticket-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="x-tickets-no-content x-nothing x-border-bottom <?php if (isset($ticket_settings) && is_array($ticket_settings) && count($ticket_settings)) echo 'x-hidden'; ?>">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/icons/ticket.png" />
			<div>
				<h3>No Tickets</h3>
				<p>Create ticket types for your registrations.</p>
			</div>
		</div>
		<div class="x-edit-ticket-toggle uk-text-right x-pad-20">
			<button class="uk-button uk-button-success">Add Ticket</button>
		</div>
		<form id="ticket_form" action="javascript:" class="x-pad-20 uk-form x-hidden">
			<h2>
				Edit Ticket
				<a href="#" id="ticket_cancel_mini" class="uk-align-right x-color-b" draggable="false"><i class="uk-icon-close"></i></a>
				<a href="#" id="ticket_save_mini" class="uk-align-right" draggable="false"><i class="uk-icon-save"></i></a>
			</h2>
			<p><strong>NOTE:</strong> Fields marked <span class="uk-text-danger">*</span> must be provided. Ticket title is automatically transformed to uppercase when saved.</p>
			<div id="guest_error"></div>
			<div class="uk-form-row">
				<input id="edit_ticket_id" name="ticket_id" type="hidden">
				<div class="uk-grid uk-grid-small" data-uk-grid-margin>
					<div class="uk-margin-top uk-width-medium-1-2 uk-width-large-1-2">
						<div class="uk-form-row">
							<label class="uk-form-label" for="edit_ticket_title">Ticket Title <span class="uk-text-danger">*</span></label>
							<div class="uk-form-controls">
								<input id="edit_ticket_title" name="edit_ticket_title" class="uk-width-1-1" type="text" placeholder="Ticket Title">
							</div>
						</div>
					</div>
					<div class="uk-margin-top uk-width-medium-1-2 uk-width-large-1-4">
						<div class="uk-form-row">
							<label class="uk-form-label" for="edit_ticket_amount">Ticket Price</label>
							<div class="uk-form-controls">
								<input id="edit_ticket_amount" class="uk-width-1-1" type="number" step="0.01" placeholder="KES 0" onkeypress="onKeyPressNumber(event, true)" onblur="this.value = num(this.value, '')" >
							</div>
						</div>
					</div>
					<div class="uk-margin-top uk-width-medium-1-2 uk-width-large-1-4">
						<div class="uk-form-row">
							<label class="uk-form-label" for="edit_ticket_limit">Guests Limit</label>
							<div class="uk-form-controls">
								<input id="edit_ticket_limit" class="uk-width-1-1" type="number" placeholder="No Limit" onkeypress="onKeyPressNumber(event, true, true)" onblur="this.value = num(this.value, '')" >
							</div>
						</div>
					</div>
					<div class="uk-margin-top uk-width-1-1">
						<div class="uk-form-row">
							<label class="uk-form-label" for="edit_ticket_description">Description</label>
							<div class="uk-form-controls">
								<textarea id="edit_ticket_description" name="edit_ticket_description" class="uk-width-1-1" placeholder="Ticket Type Description" onkeyup="this.value = sentenseCase(this.value)"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-margin-top uk-text-right">
					<button title="Cancel Editing" id="ticket_cancel" type="button" class="uk-button uk-button-default">Cancel</button>
					<button title="Save Details" id="ticket_save" type="submit" class="uk-button uk-button-success x-min-100">Save</button>
					<button title="Please wait..." id="ticket_saving" type="button" class="uk-button uk-button-default uk-disabled x-min-150 x-hidden" disabled="disabled"><i class="uk-icon-spinner uk-icon-spin"></i> Saving...</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	let EVENT_DATA = <?php echo json_encode($event); ?>;
	let REGISTRATION_LIST = <?php echo json_encode($registration_list); ?>;
	let TICKET_SETTINGS = <?php echo json_encode($ticket_settings); ?>;
</script>
