<div class="x-box" data-id="<?php echo $order["id"]; ?>" data-seen="<?php echo $order["seen_user"]; ?>" data-seen-vendor="<?php echo $order["seen_vendor"]; ?>">
	<div class="x-pad-20">
		<h1>Order <?php echo $order['id']; ?><?php if($is_vendor_order) {?> by <?php echo $order['client_name'] ?><?php } ?> </h1>
		<div class="uk-grid uk-grid-small" data-uk-grid-margin>
			<div class="uk-width-medium-1-3 uk-width-large-1-4">
				<a href="<?php echo $root; ?>/service/<?php echo $order['service_id']; ?>">
					<img class="x-service-image uk-width-1-1" style="height: 150px;" src="<?php echo $order["service_image_src"]; ?>" data-onerror="<?php echo $placeholder_image; ?>" onerror="onImageError(this);" />
				</a>
			</div>
			<div class="uk-width-medium-2-3 uk-width-large-3-4">
				<div class="x-order-service">
					<h2><?php echo $order['service_title']; ?></h2>
					<p><?php echo $order['service_description'] != '' ? $order['service_description'] : 'No service description'; ?></p>
					<p>
						<strong>Category:</strong> <?php echo $order['service_category']; ?>
						<br><strong>Company:</strong> <?php echo $order['vendor_company']; ?>
						<br><strong>Order Time:</strong> <?php echo $order['time_ordered']; ?>
						<br><strong>Time Seen:</strong> <?php echo $order['seen_vendor']; ?>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="x-pad-20 x-order-details x-border-top">
		<div class="uk-grid uk-grid-small uk-grid-divider" data-uk-grid-margin>
			<div class="uk-width-medium-1-2">
				<h3>PRICING <?php echo strlen($order["pricing_title"]) ? '<small>' . $order["pricing_title"] . '</small>' : ''; ?></h3>
				<h1><?php echo $order["pricing_price"] ? '<small>kes</small>' . num_commas($order["pricing_price"]) : 'Get Quote'; ?></h1>
			</div>
			<div class="uk-width-medium-1-2">
				<?php if ($order['event_id'] !== ''){ ?>
				<p><strong>Event:</strong> <a href="<?php echo $root; ?>/dashboard/?event=<?php echo $order['event_id']; ?>"><?php echo $order['event_name']; ?></a></p>
				<?php } ?>
				<p><strong>Status: </strong><span class="client-order-status"><?php echo strtoupper($order_statuses[(int) $order['status']]); ?></span></p>
				<p><strong>Timestamp:</strong> <?php echo $order['timestamp']; ?></p>
				<?php if ($order['payment_ref'] !== ''){ ?>
					<p><strong>Payment Ref:</strong> <?php echo $order['payment_ref']; ?></p>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php if ((int) $order['status'] === 2){ ?>
	<div class="x-status-strip x-order-paid">
		<h2>ORDER PAID</h2>
		<?php if ($is_user_order){ ?>
		<p>The vendor will mark the order as delivered</p>
		<?php } ?>
	</div>
	<?php } ?>
	<?php if ((int) $order['overdue'] === 1){ ?>
	<div class="x-status-strip x-order-overdue">
		<h2>ORDER MARKED OVERDUE ON <?php echo $order['overdue_timestamp']; ?></h2>
	</div>
	<?php } ?>
	<?php if ((int) $order['status'] === 3){ ?>
	<div class="x-status-strip x-order-delivered">
		<h2>ORDER DELIVERED</h2>
		<?php if ($is_vendor_order){ ?>
		<p>The client will mark the order as complete after reviewing delivered service/order.</p>
		<?php } ?>
	</div>
	<?php } ?>
	<?php if ((int) $order['status'] === 5){ ?>
	<div class="x-status-strip x-order-dispute">
		<h2>ORDER DISPUTE</h2>
		<p><strong>REASON:</strong> <?php echo $order['status_reason']; ?></p>
		<?php if ($is_vendor_order){ ?>
			<p>Contact the vendor to resolve any issues.</p>
		<?php } ?>
		<p>Disputed orders will be escallated to management if not resolved in two days.</p>
	</div>
	<?php } ?>
	<?php if ((int) $order['status'] === 4 || (int) $order['status'] === 6){ ?>
	<div class="x-status-strip x-order-complete">
		<h2>ORDER COMPLETED <?php if ($is_vendor_order && $order['status'] === 6) echo ('AND ARCHIVED') ?></h2>
		<?php if ($is_user_order){ ?>
		<p>The order has been moved to your orders history.</p>
		<?php } ?>
	</div>
	<?php } ?>

	<?php if ($is_user_order || $is_vendor_order){ ?>
		<div class="x-pad-20 x-border-top uk-text-center">
			<?php if ($is_vendor_order) { ?>
				<?php if ((int) $order['status'] === -2){ ?>
				<a draggable="false" href="javascript:" data-id="<?php echo $order["id"]; ?>" data-name="<?php echo $order["id"] . " (" . $order["service_title"] . ")"; ?>" class="item-delete uk-button uk-button-white uk-text-danger">Delete</a>
				<?php } ?>
				<?php if ((int) $order['status'] === 0){ ?>
				<a draggable="false" href="javascript:" data-id="<?php echo $order["id"]; ?>" data-name="<?php echo $order["id"] . " (" . $order["service_title"] . ")"; ?>" class="item-receive uk-button uk-button-success">Receive</a>
				<?php } ?>
				<?php if ((int) $order['status'] != -2 && (int) $order['status'] < 2){ ?>
				<a draggable="false" href="javascript:" data-id="<?php echo $order["id"]; ?>" data-name="<?php echo $order["id"] . " (" . $order["service_title"] . ")"; ?>" class="item-decline uk-button uk-button-danger">Decline</a>
				<?php } ?>
				<?php if ((int) $order['status'] === 2){ ?>
				<a draggable="false" href="<?php echo $root; ?>/orders/?order=<?php echo $order["id"]; ?>" class="uk-button uk-button-success">Deliver</a>
				<?php } ?>
				<?php if ((int) $order['status'] === 4){ ?>
				<a draggable="false" href="javascript:" data-id="<?php echo $order["id"]; ?>" data-name="<?php echo $order["id"] . " (" . $order["service_title"] . ")"; ?>" class="item-archive uk-button uk-button-success">Archive</a>
				<?php } ?>
			<?php } ?>

		<?php if ($is_user_order){ ?>
			<a href="<?php echo $root; ?>/messages/<?php echo $order['vendor_uid']; ?>" class="uk-button uk-button-white"><i class="uk-icon-envelope-o"></i> Contact Vendor</a>
		<?php } ?>
		<?php if ($is_user_order && (int) $order['status'] === 1){ ?>
			<button class="uk-button uk-button-success x-order-checkout <?php if ((int) $order['status'] >= 2) echo 'x-hidden'; ?>"><i class="uk-icon-credit-card"></i> Checkout</button>
		<?php } ?>
		<?php if ($is_user_order && (int) $order['status'] === 2 && (int) $order['overdue'] === 0){ ?>
			<button class="uk-button uk-button-danger x-mark-overdue"><i class="uk-icon-clock-o"></i> Order Overdue</button>
		<?php } ?>
		<?php if ($is_user_order && in_array((int) $order['status'], [3, 5])){ ?>
			<button class="uk-button uk-button-success x-mark-complete"><i class="uk-icon-check"></i> Order Complete</button>
		<?php } ?>
		<?php if ($is_user_order && (int) $order['status'] === 3){ ?>
			<button class="uk-button uk-button-danger x-mark-dispute">Order Dispute</button>
		<?php } ?>
		<?php if ($is_vendor_order){ ?>
			<a href="<?php echo $root; ?>/messages/<?php echo $order['uid']; ?>" class="uk-button uk-button-white"><i class="uk-icon-envelope-o"></i> Contact Client</a>
		<?php } ?>
		<?php if ($is_vendor_order && (int) $order['status'] === 2){ ?>
			<button class="uk-button uk-button-success x-mark-delivered"><i class="uk-icon-check"></i> Order Delivered</button>
		<?php } ?>
	</div>
	<?php } ?>
</div>
<?php $order_rating = isset($order) && is_array($order) && array_key_exists('rating', $order) && is_array($order['rating']) && !empty($order['rating']) ? $order['rating'] : null; ?>
<div class="uk-margin-top x-service-reviews x-box">
	<div class="x-pad-20 x-border-bottom">
		<h2 class="x-nomargin">Order Reviews</h2>
		<div class="uk-margin-small-top x-service-rating <?php if (!(isset($order_rating['reviews']) && !empty($order_rating['reviews']))) echo 'x-hidden'; ?>">
			<i class="x-rating-star uk-icon-star"></i> <strong><?php  echo number_format($order_rating["rating"], 1); ?></strong>
			<small>(<?php echo num_commas($order_rating["count"], 0); ?> Reviews)</small>
		</div>
	</div>
	<div class="x-no-reviews <?php if (!empty($order_rating['reviews'])) echo 'x-hidden'; ?>">
		<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
		<p>
			You don't have reviews for this order.
			<?php if ((int) $order['status'] !== 4){ ?>
			<br>An order must be complete to be reviewed.
			<?php } ?>
			<?php if ($is_user_order){ ?>
			<br>Event guests can also review this order through their RSVP link
			<?php } ?>
		</p>
	</div>
	<div class="x-review-comments scrollbox <?php if (empty($order_rating['reviews'])) echo 'x-hidden'; ?> <?php if (count($order_rating['reviews']) > 2) echo 'limit'; ?>">
		<?php foreach ($order_rating['reviews'] as $review){ ?>
		<div class="x-review-comment x-border-top" data-id="<?php echo $review["id"]; ?>">
			<div class="x-comment-avatar">
				<div class="x-avatar"><img draggable="false" src="<?php echo $review["avatar"]; ?>" data-onerror="<?php echo $placeholder_image; ?>" onerror="onImageError(this);" /></div>
			</div>
			<div class="x-comment">
				<strong><?php echo $review["name"]; ?></strong>
				<div class="x-meta"><div class="uk-rating-stars" data-rate="<?php echo $review["rating"]; ?>"></div> <?php echo $review["timestamp"]; ?></div>
				<p><?php echo $review["comment"]; ?></p>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="uk-text-center x-pad-20 x-border-top <?php if (!(count($order_rating['reviews']) > 2 || (int) $order['status'] === 4 && $is_user_order)) echo 'x-hidden'; ?>">
		<button class="x-my-review uk-button uk-button-primary x-min-150 <?php if ((int) $order['status'] !== 4) echo 'x-hidden'; ?>">My Review</button>
		<button class="x-review-comments-toggle uk-button uk-button-white x-min-150 <?php if (count($order_rating['reviews']) <= 2) echo 'x-hidden'; ?>">Show All</button>
	</div>
</div>
