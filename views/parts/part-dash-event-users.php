<h1>Event Users</h1>
<p>Share access and collaborate with other users on your event planning &amp; management.</p>
<div class="uk-datatable uk-margin-top" data-limit="10" data-row-index="-1">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Users">
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th tabindex="0" class="x-nowrap uk-datatable-row-index">#</th>
						<th tabindex="0" class="x-nowrap">Username</th>
						<th tabindex="0" class="x-nowrap">Names</th>
						<th tabindex="0" class="x-nowrap">Email</th>
						<th tabindex="0" class="x-nowrap">Role</th>
						<th tabindex="0" class="x-nowrap">Status</th>
						<th tabindex="0" class="x-nowrap">Time Added</th>
						<th class="x-nowrap x-block">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($users) && is_array($users) && count($users)){ foreach ($users as $i => $item){ ?>
					<?php $item_search = implode(" ", [$item["email"], $item["names"], $item["role_text"], $item["status_text"]]); ?>
					<tr data-id="<?php echo $item["id"]; ?>" data-search="<?php echo $item_search; ?>" data-base64="<?php echo base64_encode(json_encode($item)); ?>">
						<td class="x-nowrap uk-datatable-row-index" data-value=""></td>
						<td class="x-nowrap" data-value="<?php echo $item["username"]; ?>"><?php echo $item["username"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["names"]; ?>"><?php echo $item["names"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["email"]; ?>"><?php echo $item["email"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["role"]; ?>"><?php echo $item["role_text"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["status"]; ?>"><?php echo $item["status_text"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["timestamp"]; ?>"><?php echo $item["timestamp"]; ?></td>
						<td class="x-nowrap uk-text-center">
							<?php if ($item['id'] !== 'owner'){ ?>
							<a title="Resend Invite" draggable="false" href="javascript:" data-id="<?php echo $item["id"]; ?>" class="user-resend uk-button uk-button-small uk-button-white"><i class="uk-icon-send"></i></a>
							<a title="Edit" draggable="false" href="javascript:" class="user-edit uk-button uk-button-small uk-button-primary"><i class="uk-icon-pencil"></i></a>
							<a title="Delete" draggable="false" href="javascript:" data-id="<?php echo $item["id"]; ?>" class="user-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>
							<?php } ?>
						</td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
		</div>
		<div class="uk-datatable-no-content x-nothing x-border-bottom x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
			<div>
				<h3>No Users</h3>
				<p>No shared access users.</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-border-bottom x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
		<div class="x-edit-add uk-text-right x-pad-20">
			<button class="uk-button uk-button-success">Invite User</button>
		</div>
		<form id="invite_form" action="javascript:" class="x-pad-20 uk-form x-border-top x-hidden">
			<h2>
				EVENT USER
				<a href="#" id="cancel_mini" class="uk-align-right x-color-b" draggable="false"><i class="uk-icon-close"></i></a>
				<a href="#" id="save_mini" class="uk-align-right" draggable="false"><i class="uk-icon-save"></i></a>
			</h2>
			<p><strong>NOTE:</strong> Fields marked <span class="uk-text-danger">*</span> must be provided</p>
			<div class="uk-form-row">
				<div class="uk-grid" data-uk-grid-margin>
					<div class="uk-width-large-2-3">
						<div class="uk-grid uk-grid-small" data-uk-grid-margin>
							<div class="uk-width-medium-1-1">
								<input id="event_user" type="hidden" value="">
								<div class="uk-form-row">
									<label class="uk-form-label" for="user_email">Email <span class="uk-text-danger">*</span></label>
									<div class="uk-form-controls">
										<input id="user_email" name="user_email" class="uk-width-1-1" type="email" placeholder="User's email address">
									</div>
								</div>
								<div class="uk-form-row">
									<label class="uk-form-label" for="user_username">Username <span class="uk-text-danger">*</span> <small><i>(Unique @user identifier in event collaboration tools)</i></small></label>
									<div class="uk-form-controls">
										<input id="user_username" name="user_username" class="uk-width-1-1" type="text" placeholder="@username">
									</div>
								</div>
								<div class="uk-form-row">
									<label class="uk-form-label" for="user_names">Names <span class="uk-text-danger">*</span></label>
									<div class="uk-form-controls">
										<input id="user_names" name="user_names" class="uk-width-1-1" type="text" placeholder="Full Names">
									</div>
								</div>
								<div class="uk-form-row">
									<label class="uk-form-label" for="user_role">Role</label>
									<div class="uk-form-controls">
										<select id="user_role" name="user_role" class="uk-width-1-1" data-default="0">
											<option value="0" selected="selected">Manage All</option>
											<option value="1">Guests &amp; Attendance</option>
											<option value="2">Budget &amp; Orders</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-margin-top uk-text-right">
					<button title="Cancel Editing" id="edit_cancel" type="button" class="uk-button uk-button-white">Cancel</button>
					<button title="Save Details" id="edit_save" type="submit" class="uk-button uk-button-success x-min-150">Send Invite</button>
					<button title="Please wait..." id="edit_saving" type="button" class="uk-button uk-button-default uk-disabled x-min-150 x-hidden" disabled="disabled"><i class="uk-icon-spinner uk-icon-spin"></i> Sending...</button>
				</div>
			</div>
		</form>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
