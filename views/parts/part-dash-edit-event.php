<h1><?php echo $event_is_new ? "Create" : "Edit"; ?> Event</h1>
<form id="event_form" class="uk-form x-box uk-margin-top" action="javascript:" method="post">
	<noscript>
		<div class="x-pad-20 x-border-bottom">
			<div class="uk-alert uk-alert-danger x-nomargin" data-uk-alert>
				<h3>You must have JavaScript enabled in order to use this order form. Please enable JavaScript and then reload this page in order to continue.</h3>
			</div>
		</div>
	</noscript>
	<input type="hidden" id="event_is_new" value="<?php echo $event_is_new ? 1 : 0; ?>">
	<div class="x-pad-20">
		<div class="uk-grid" data-uk-grid-margin>
			<div class="uk-width-large-1-3">
				<h2>Event Poster</h2>
				<small>Optional poster image for your event. Visible when guests are viewing your event details.</small>
				<div class="uk-margin-small-top x-poster-img">
					<input id="event_poster_keep" type="hidden" name="event_poster_keep" value="1">
					<img draggable="false" id="event_poster" class="uk-width-1-1" src="<?php echo isset($event_poster) ? $event_poster : $placeholder_image; ?>" alt="Event Poster" />
					<div class="uk-margin-top">
						<div class="uk-form-file uk-width-1-1">
							<button id="event_poster_change" class="uk-button uk-button-white uk-width-1-1"><?php echo isset($event_poster) && strlen($event_poster) ? "Change" : "Select"; ?> Image</button>
							<input type="file" accept=".jpg,.jpeg,.png,.gif" name="event_poster" id="event_poster_file">
						</div>
						<button id="event_poster_remove" class="uk-button uk-button-white uk-width-1-1 uk-margin-small-top uk-text-danger <?php if (!(isset($event_poster) && strlen($event_poster))) echo "x-hidden"; ?>">Remove Image</button>
					</div>
				</div>
			</div>
			<div class="uk-width-large-2-3">
				<h2>Event Details</h2>
				<p>Fill out these basic details about your event. Fields marked with <span class="uk-text-danger">*</span> are required.</p>
				<div class="uk-form-row">
					<div class="uk-grid uk-grid-small" data-uk-grid-margin>
						<div class="uk-width-medium-1-2">
							<label class="uk-form-label x-fw-400" for="event_name">Event Name <span class="uk-text-danger">*</span></label>
							<div class="uk-form-controls">
								<input class="uk-width-1-1" type="text" id="event_name" onkeyup="this.value = sentenseCase(this.value)" name="event_name" placeholder="Enter your event name" value="<?php if (isset($event_name)) echo $event_name; ?>">
							</div>
						</div>
						<div class="uk-width-medium-1-2">
							<div class="uk-width-1-1">
								<label class="uk-form-label x-fw-400">Type <span class="uk-text-danger">*</span></label>
								<div class="uk-form-controls">
									<uk-autocomplete class="uk-width-1-1" id="event_type" name="event_type" fetch="<?php echo $root; ?>/autocomplete.php?type={{query}}&other=1" item-template="{{name}}" template="{{name}}" value="<?php if (isset($event_type_name)) echo $event_type_name; ?>" placeholder="Select an event type"></uk-autocomplete>
								</div>
							</div>
							<div class="uk-width-1-1 event_type_other_wrapper uk-margin-small-top <?php if (!(isset($event_type_value) && !$event_type_value)) echo 'x-hidden'; ?>">
								<label class="uk-form-label x-fw-400">Suggest Event Type <span class="uk-text-danger">*</span></label>
								<div class="uk-form-controls">
									<input type="text" class="uk-width-1-1" id="event_type_other" onkeyup="this.value = sentenseCase(this.value)" name="event_type_other" placeholder="Suggest Other Event Type" value="<?php if (isset($event_type_other)) echo $event_type_other; ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-form-row">
					<div class="uk-grid uk-grid-small" data-uk-grid-margin>
						<div class="uk-width-medium-1-2">
							<div class="uk-width-1-1">
								<label class="uk-form-label x-fw-400" for="event_start_date">Event Starts <span class="uk-text-danger">*</span></label>
								<div class="uk-form-controls">
									<div class="uk-form-icon uk-form-icon-flip uk-width-1-1">
										<i class="uk-icon-calendar"></i>
										<input type="text" class="uk-width-1-1" id="event_start_date" name="event_start_date" placeholder="DD/MM/YYYY" data-uk-datepicker="{format:'DD/MM/YYYY',pos:'bottom'}" value="<?php if (isset($event_start_date)) echo $event_start_date; ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="uk-width-medium-1-2">
							<div class="uk-width-1-1">
								<label class="uk-form-label uk-hidden-small" for="event_start_time">&nbsp;</label>
								<div class="uk-form-controls event_start_time">
									<div class="uk-form-icon uk-form-icon-flip uk-width-1-1">
										<i class="uk-icon-clock-o"></i>
										<input type="text" class="uk-width-1-1" id="event_start_time" name="event_start_time" placeholder="00:00 AM" data-uk-timepicker="{format:'12h'}" value="<?php if (isset($event_start_time)) echo $event_start_time; ?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-form-row">
					<div class="uk-grid uk-grid-small" data-uk-grid-margin>
						<div class="uk-width-medium-1-2">
							<div class="uk-width-1-1">
								<label class="uk-form-label x-fw-400" for="event_end_date">Event Ends <span class="uk-text-danger">*</span></label>
								<div class="uk-form-controls">
									<div class="uk-form-icon uk-form-icon-flip uk-width-1-1">
										<i class="uk-icon-calendar"></i>
										<input type="text" class="uk-width-1-1" id="event_end_date" name="event_end_date" placeholder="DD/MM/YYYY" data-uk-datepicker="{format:'DD/MM/YYYY',pos:'bottom'}" value="<?php if (isset($event_end_date)) echo $event_end_date; ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="uk-width-medium-1-2">
							<div class="uk-width-1-1">
								<label class="uk-form-label uk-hidden-small" for="event_end_time">&nbsp;</label>
								<div class="uk-form-controls event_end_time">
									<div class="uk-form-icon uk-form-icon-flip uk-width-1-1">
										<i class="uk-icon-clock-o"></i>
										<input type="text" class="uk-width-1-1" id="event_end_time" name="event_end_time" placeholder="00:00 AM" data-uk-timepicker="{format:'12h'}" value="<?php if (isset($event_end_time)) echo $event_end_time; ?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-form-row">
					<label class="uk-form-label" for="event_description">Event Description</label>
					<div class="uk-form-controls">
						<textarea class="uk-width-1-1" id="event_description" onkeyup="this.value = sentenseCase(this.value)" name="event_description" placeholder="Describe your event. Guests will be able to see this."><?php if (isset($event_description)) echo $event_description; ?></textarea>
					</div>
				</div>
				<div class="uk-form-row">
					<div class="uk-grid uk-grid-small" data-uk-grid-margin>
						<div class="uk-width-medium-1-2">
							<label class="uk-form-label" for="event_guests_expected">Expected Guests</label>
							<div class="uk-form-controls">
								<input id="event_guests_expected" onkeypress="onKeyPressNumber(event, true)" onblur="this.value = num(this.value, '')" name="event_guests_expected" class="uk-width-1-1" type="number" placeholder="Number Expected Guests" value="<?php if (isset($event_guests_expected)) echo $event_guests_expected; ?>">
								<small class="uk-text-danger"></small>
							</div>
						</div>
						<div class="uk-width-medium-1-2">
							<label class="uk-form-label" for="event_guests_max">Maximum Guests</label>
							<div class="uk-form-controls">
								<input id="event_guests_max" onkeypress="onKeyPressNumber(event, true)" onblur="this.value = num(this.value, '')" name="event_guests_max" class="uk-width-1-1" type="number" placeholder="Limit Number Of Guests" value="<?php if (isset($event_guests_max)) echo $event_guests_max; ?>">
								<small class="uk-text-danger"></small>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-form-row">
					<label class="uk-form-label">Venue Location</label>
					<div class="uk-form-control location_map">
						<map-input class="uk-width-1-1" id="location_map" name="location_map" value="<?php if (isset($location_map_value)) echo $location_map_value; ?>" placeholder="Search Places" zoom="15" icon="map-marker"></map-input>
						<p class="x-location-map-desc">Search/Drag pin. So guests can find this event on their their map.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="uk-margin-top uk-text-right x-border-top x-pad-20">
		<button id="save_details" type="button" class="uk-button uk-button-success x-min-150 uk-button-large">Save Details</button>
	</div>
</form>
