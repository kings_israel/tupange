<h1>Deleted Services</h1>
<p>These services will be automatically removed permanently after a period of 30 days since their delete time.</p>
<div class="uk-datatable" data-limit="5">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Service Title, Description">
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th class="x-nowrap x-block">Service</th>
						<th tabindex="0" class="x-nowrap">Title</th>
						<th tabindex="0" class="x-nowrap">Description</th>
						<th tabindex="0" class="x-nowrap">Delete Time</th>
						<th class="x-nowrap x-block">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($vendor_services) && is_array($vendor_services) && count($vendor_services)){ foreach($vendor_services as $service){ $deleted_days = isset($service["deleted_days"]) ? $service["deleted_days"] : "##"; ?>
					<tr data-search="<?php echo $service["title"] . " " . $service["description"]; ?>" data-toggle="">
						<td class="x-min-150 x-max-150 x-nowrap"><img class="x-service-image" draggable="false" src="<?php echo $service["image_src"]; ?>" /></td>
						<td class="x-min-100 x-nowrap" data-value="<?php echo $service["title"]; ?>">
							<?php echo $service["title"]; if ($service["paused"] == 1){ ?>
							<br><em>Paused Service</em>
							<?php } ?>
						</td>
						<td class="x-min-100" data-value="<?php echo $service["description"]; ?>"><?php echo $service["description"]; ?></td>
						<td class="x-min-100 x-nowrap" data-value="<?php echo $service["timestamp"]; ?>">
							<?php echo $service["timestamp"]; ?>
							<br><small><?php echo $deleted_days; ?></small>
						</td>
						<td class="x-min-100 x-nowrap uk-text-center">
							<a title="Restore Service" draggable="false" href="javascript:" data-id="<?php echo $service["id"]; ?>" data-title="<?php echo $service["title"]; ?>" class="x-service-restore uk-button uk-button-small uk-button-white">Restore</a>
							<a title="Delete Permanently" draggable="false" href="javascript:" data-id="<?php echo $service["id"]; ?>" data-title="<?php echo $service["title"]; ?>" class="x-service-remove uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>
						</td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
		</div>
		<div class="uk-datatable-no-contents x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
			<div>
				<h3>No Services</h3>
				<p>Deleted services will be shown here.</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
