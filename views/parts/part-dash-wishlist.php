<?php
//helper functions
function event_time($event, $break=true){
	try {
		$start = $event["start"][2];
		$end = $event["end"][2];
		if ($start == $end){
			$start = new DateTime(date("Y-m-d H:i:s", strtotime($start)));
			return $start -> format("jS M Y h:i A");
		}
		else {
			$start = new DateTime(date("Y-m-d H:i:s", strtotime($start)));
			$end = new DateTime(date("Y-m-d H:i:s", strtotime($end)));
			return sprintf("%s %s %s", $start -> format("jS M Y h:i A"), ($break ? "-<br>" : "-"), $end -> format("jS M Y h:i A"));
		}
	}
	catch (Exception $e){
		return "ERROR!";
	}
}
?>
<h1>My Wishlist <a href="<?php echo $root; ?>/dashboard?edit-event" class="uk-button uk-button-success uk-align-right">New Event</a></h1>
<p>Wishlist events can be managed but without involving service vendors or sending out invitations.</p>
<div class="uk-datatable" data-limit="5">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Event Name, Description, Type, Status">
	</div>
	<div class="uk-width-1-1 uk-form uk-margin-top x-nowrap">
		<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="" checked> All</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="Active"> Active</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="Live"> Live</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="Past"> Past</label>
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th tabindex="0" class="x-nowrap">Event Name</th>
						<th tabindex="0" class="x-nowrap">Event Type</th>
						<th tabindex="0" class="x-nowrap">Event Time</th>
						<th tabindex="0" class="x-nowrap">Event Status</th>
						<th class="x-nowrap x-block">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($user_events) && is_array($user_events) && count($user_events)){ foreach($user_events as $i => $event){ $event_status = $event["event_status"];?>
					<tr class="<?php echo "x-event-" . strtolower($event_status); ?>" data-search="<?php echo $event["name"] . " " . $event["description"] . " " . $event["type"]["name"] . " " . $event_status; ?>" data-toggle="<?php echo $event_status; ?>">
						<td class="x-min-150" data-value="<?php echo $event["name"]; ?>">
							<a title="Click to view event dashboard" href="<?php echo $root; ?>/dashboard?event=<?php echo $event["id"]; ?>">
								<div class="x-event-name"><?php echo $event["name"]; ?></div>
								<div class="x-event-description"><?php echo $event["description"] != "" ? $event["description"] : "Event has no description"; ?></div>
							</a>
							<div class="x-event-timestamp"><?php echo $event["timestamp"]; ?></div>
						</td>
						<td class="x-nowrap" data-value="<?php echo $event["type"]["name"]; ?>"><?php echo $event["type"]["name"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $event["start"][2]; ?>"><?php echo event_time($event); ?></td>
						<td class="x-nowrap" data-value="<?php echo $event_status; ?>">
							<span class="x-event-status x-uppercase x-fw-400"><?php echo $event_status; ?></span>
						</td>
						<td class="x-nowrap uk-text-center">
							<a title="Event Dashboard" href="<?php echo $root; ?>/dashboard?event=<?php echo $event["id"]; ?>" class="uk-button uk-button-small uk-button-primary"><i class="uk-icon-edit"></i></a>
							<a title="Edit Event" href="<?php echo $root; ?>/dashboard?edit-event=<?php echo $event["id"]; ?>" class="uk-button uk-button-small uk-button-white"><i class="uk-icon-pencil"></i></a>
							<a title="Delete" draggable="false" href="javascript:" data-id="<?php echo $event["id"]; ?>" data-name="<?php echo $event["name"]; ?>" class="event-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>
						</td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
		</div>
		<div class="x-nothing<?php if (isset($user_events) && is_array($user_events) && count($user_events)) echo " x-hidden"; ?>">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/cal.png" />
			<div>
				<h3>No Events</h3>
				<p>Create an event in wishlist mode to add to list</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
