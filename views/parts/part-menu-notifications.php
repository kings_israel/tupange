<a href="javascript:" class="x-notifications-toggle"><i class="uk-icon-bell"></i></a>
<span title="Unread" class="x-badge-count notifications x-hidden">0</span>
<div class="x-notifications-drop x-card x-hidden">
	<ul class="x-notifications-list"></ul>
	<div class="x-notifications-empty">
		<img draggable="false" src="<?php echo $root; ?>/assets/img/icons/notification.png" />
		<p>Loading notifications...</p>
	</div>
	<div class="x-notifications-actions x-hidden">
		<button class="x-notifications-refresh uk-button uk-button-small uk-button-default">Refresh</button>
		<button class="x-notifications-mark-read uk-button uk-button-small uk-button-default">Mark All Read</button>
	</div>
</div>
