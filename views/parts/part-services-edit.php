<h1><?php echo $view_title; ?></h1>
<form class="uk-form onload-show x-display-none x-box uk-margin-top x-pad-20" action="" method="post" enctype="multipart/form-data">
	<p>These are the basic details about the service you are selling. Service pricing, gallery, rating &amp; reviews among other additional details are available on the service profile.</p>
	<?php if (isset($service_error)){ ?>
	<div class="uk-alert uk-alert-danger" data-uk-alert>
		<a href="" class="uk-alert-close uk-close"></a>
		<p><?php echo $service_error; ?></p>
	</div>
	<?php } ?>
	<div class="uk-grid" data-uk-grid-margin>
		<div class="uk-width-large-1-3">
			<h2>Display Image</h2>
			<div class="x-display-img">
				<a href="javascript:" id="display-img-link">
					<img id="display-img" class="uk-width-1-1" src="<?php echo isset($service_image) ? $service_image : $placeholder_image; ?>" alt="Display Image" />
				</a>
				<small class="uk-hidden-large">Image may be trimmed to fit</small>
				<div class="uk-margin-top">
					<div class="uk-form-file uk-width-1-1">
						<button id="display-img-btn" class="uk-button uk-button-white uk-width-1-1">Select Image</button>
						<input type="file" accept=".jpg,.jpeg,.png,.gif" name="display_image" id="display-image">
					</div>
				</div>
			</div>
		</div>
		<div class="uk-width-large-2-3">
			<h2>Service Details</h2>
			<div class="uk-form-row">
				<label class="uk-form-label" for="company_name">Title <span class="uk-text-danger">*</span></label>
				<div class="uk-form-controls">
					<input class="uk-width-1-1" type="text" id="service_title" onkeyup="this.value = sentenseCase(this.value)" name="service_title" placeholder="Service Title" value="<?php if (isset($service_title)) echo $service_title; ?>">
				</div>
				<small id="service_title_error" class="uk-text-danger"></small>
			</div>
			<div class="uk-form-row">
				<label class="uk-form-label" for="company_email">Description <span class="uk-text-danger">*</span></label>
				<div class="uk-form-controls">
					<textarea class="uk-width-1-1" id="service_description" onkeyup="this.value = sentenseCase(this.value)" name="service_description" placeholder="Service Description"><?php if (isset($service_description)) echo $service_description; ?></textarea>
				</div>
				<small id="service_description_error" class="uk-text-danger"></small>
			</div>
			<div class="uk-form-row">
				<label class="uk-form-label" for="combo-category">Category <span class="uk-text-danger">*</span></label>
				<div class="uk-form-controls">
					<uk-autocomplete class="uk-width-1-1" name="service_category" fetch="<?php echo $root; ?>/autocomplete.php?category={{query}}" item-template="<i class='uk-icon-{{icon}}'></i> {{name}}" template="{{name}}" value="<?php if (isset($service_category_value)) echo $service_category_value; ?>" placeholder="Search Category"></uk-autocomplete>
				</div>
			</div>
			<div class="uk-form-row">
				<label class="uk-form-label" for="location">Location <span class="uk-text-danger">*</span></label>
				<div class="uk-form-controls x-drop-wrapper">
					<uk-locations class="uk-width-1-1" id="location" name="location" fetch="<?php echo $root; ?>/autocomplete.php?location={{query}}" item-template="<i class='uk-icon-map-marker'></i> {{name}}" template="{{name}}" value="<?php if (isset($location_value)) echo $location_value; ?>" placeholder="Search City/Area" icon="map-marker"></uk-locations>
				</div>
			</div>
			<div class="uk-form-row">
				<label class="uk-form-label" for="location_map">Location On Map</label>
				<div class="uk-form-controls">
					<map-input class="uk-width-1-1" id="location_map" name="location_map" value="<?php if (isset($location_map_value)) echo $location_map_value; ?>" placeholder="Search Places" icon="map-marker"></map-input>
					<p class="x-location-map-desc">Search/Drag pin. So clients can find this service on their their map.</p>
				</div>
			</div>
			<div class="uk-form-row">
				<label class="uk-form-label" for="company_name">Contact Phone Number </label>
				<div class="uk-form-controls">
					<input class="uk-width-1-1" type="phone_number" id="service_contact_phone_number" name="service_contact_phone_number" placeholder="Contact Phone Number" value="<?php if (isset($service_contact_phone_number)) echo $service_contact_phone_number; ?>">
				</div>
				<small id="service_title_error" class="uk-text-danger"></small>
			</div>
			<div class="uk-form-row">
				<label class="uk-form-label" for="company_name">Contact Email </label>
				<div class="uk-form-controls">
					<input class="uk-width-1-1" type="email" id="service_contact_email" name="service_contact_email" placeholder="Contact Email" value="<?php if (isset($service_contact_email)) echo $service_contact_email; ?>">
				</div>
				<small id="service_title_error" class="uk-text-danger"></small>
			</div>
			<div class="uk-form-row">
				<div class="uk-form-controls">
					<input class="uk-width-1-1" type="checkbox" id="company_contact_details_checkbox" name="company_contact_details_checkbox" value="<?php if (isset($company_contact_details_checkbox)) echo "checked"; ?>">
					<label class="uk-form-label" for="company_name">Use Company Contact Information as contact.</label>
				</div>
				<small id="service_title_error" class="uk-text-danger"></small>
			</div>
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-margin-top uk-text-right uk-width-large-2-3">
					<input draggable="false" type="submit" name="action" class="uk-button uk-button-success" value="Save and Add Service"></input>
				</div>
				<div class="uk-margin-top uk-text-right uk-width-large-1-3">
					<input draggable="false" type="submit" name="action" class="uk-button uk-button-primary" value="Save Details"></input>
				</div>
			</div>
		</div>
	</div>
</form>
<noscript>
	<div class="x-box">
		<div class="x-pad-20">
			<div class="uk-alert uk-alert-danger" data-uk-alert>
				<h3>You must have JavaScript enabled in order to use this form. Please enable JavaScript and then reload this page in order to continue.</h3>
			</div>
		</div>
	</div>
</noscript>
