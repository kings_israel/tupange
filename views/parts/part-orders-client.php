<h1>Client Orders</h1>

<p>
	View and manage orders requested by clients from your services.
	<br><strong>Receive an order to notify the client that you are interested in their order</strong>
</p>
<div class="uk-grid">
	<?php foreach ($order_status_details as $status => $details) { ?>
		<div class="uk-width-medium-1-3">
			<div class="uk-button uk-button-success uk-width-1-1 uk-margin-small-top">
				<?php echo $status ?><br>
				<span style="color: #000;">No. of Orders:</span> <?php echo $details['count'] ?><br>
				<span style="color: #000;">Total Value:</span>  Ksh.<?php echo $details['amount'] ?>
			</div>
		</div>
	<?php } ?>
</div>
<div class="uk-datatable uk-margin-top" data-limit="10" data-row-index="1">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Order">
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th tabindex="0" class="x-nowrap">#</th>
						<th tabindex="0" class="x-nowrap">Order</th>
						<th tabindex="0" class="x-nowrap">Client Name</th>
						<th tabindex="0" class="x-nowrap">Date Due</th>
						<th tabindex="0" class="x-nowrap">Service</th>
						<th tabindex="0" class="x-nowrap">Pricing</th>
						<th tabindex="0" class="x-nowrap">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($orders) && is_array($orders) && count($orders))
						{ foreach($orders as $i => $item)
							if( (int)$item['status'] != -3 && (int)$item['status'] != -2 && (int)$item['status'] != -1 && (int)$item['status'] != 6 && (int)$item['status'] != 4 ) {
								{ $status_text = $order_statuses[(int) $item["status"]];
					?>
					<?php $data_search = implode(" ", [$item["id"], $item["ref"], $item["service_title"], $item["service_category"], $status_text]); ?>
					<tr data-search="<?php echo $data_search; ?>" data-id="<?php echo $item["id"]; ?>" data-seen="<?php echo $item["seen_user"]; ?>" data-seen-vendor="<?php echo $item["seen_vendor"]; ?>" data-toggle="">
						<td class="x-nowrap x-min-50 uk-datatable-row-index" data-value=""></td>
						<td class="x-nowrap" data-value="<?php echo $item["id"]; ?>">
							<a title="Order Details" href="<?php echo $root; ?>/orders/?order=<?php echo $item["id"]; ?>"><strong><?php echo $item["id"]; ?></strong></a>
						</td>
						<td class="x-nowrap" data-value="<?php echo $item['client']['display_name']; ?>"><?php echo $item['client']['display_name']; ?></td>
						<td class="x-nowrap" data-value="<?php if ($item['due_date']) echo $item['due_date']['start_timestamp']; ?>"><?php if ($item['due_date']) echo $item['due_date']['start_timestamp']; ?></td>
						<td class="x-nowrap" data-value="<?php echo $item["service_id"]; ?>">
							<a title="Service Details" href="<?php echo $root; ?>/service/<?php echo $item["service_id"]; ?>"><?php echo $item["service_title"]; ?></a>
						</td>
						<td class="x-nowrap" data-value="<?php echo $item["pricing_title"] ?>"><?php echo strlen($item["pricing_title"]) ? $item["pricing_title"] . " <small><i>" . num_commas($item["pricing_price"]) . "</i></small>": "Get Quote"; ?></td>
						<td class="x-nowrap" data-status="<?php echo $item["id"]; ?>" data-value="<?php echo $item["status"]; ?>"><?php echo $status_text; ?></td>
					</tr>
					<?php }}} ?>
				</tbody>
			</table>
		</div>
		<div class="uk-datatable-no-contents x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
			<div>
				<h3>No Orders</h3>
				<p>Check again later</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
