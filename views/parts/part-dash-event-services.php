<h1>Event Orders</h1>
<p>Review and manage your event orders.</p>
<div class="uk-datatable" data-limit="10" data-row-index="1">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Order Service, Category, Vendor, Status, etc">
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th tabindex="0" class="x-nowrap">#</th>
						<th tabindex="0" class="x-nowrap">Order No.</th>
						<th tabindex="0" class="x-nowrap">Service</th>
						<!-- <th tabindex="0" class="x-nowrap">Category</th> -->
						<th tabindex="0" class="x-nowrap">Pricing</th>
						<th tabindex="0" class="x-nowrap">Order Price</th>
						<th tabindex="0" class="x-nowrap">Order Status</th>
						<th tabindex="0" class="x-nowrap">Order Date</th>
						<th class="x-nowrap x-block">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($orders) && is_array($orders) && count($orders)){ foreach($orders as $i => $item){ $status_text = $order_statuses[$item["status"]]; ?>
					<tr data-search="<?php echo implode(" ", [$item["id"], $item["ref"], $item["service_title"], $item["service_category"], $item["vendor_company"], $status_text, $item["event_name"]]); ?>" data-id="<?php echo $item["id"]; ?>" data-seen="<?php echo $item["seen_user"]; ?>" data-toggle="">
						<td class="x-nowrap x-min-50 uk-datatable-row-index" data-value=""></td>
						<td class="x-nowrap" data-value="<?php echo $item["id"]; ?>">
							<a title="Order Details" href="<?php echo $root; ?>/orders/?order=<?php echo $item["id"]; ?>"><?php echo $item["id"]; ?></a>
						</td>
						<td class="x-min-100" data-value="<?php echo $item["service_id"]; ?>">
							<a title="Service Details" href="<?php echo $root; ?>/service/<?php echo $item["service_id"]; ?>"><?php echo $item["service_title"]; ?></a>
							<small class="x-italics x-color-b"><?php echo $item["service_category"]; ?></small>
						</td>
						<!--<td class="x-min-100 x-nowrap" data-value="<?php echo $item["service_category"] ?>"><?php echo $item["service_category"]; ?></td> -->
						<td class="x-min-100 x-nowrap" data-value="<?php echo $item["pricing_title"] ?>"><?php echo strlen($item["pricing_title"]) ? $item["pricing_title"] . " <small><i>" . num_commas($item["pricing_price"]) . "</i></small>": "Get Quote"; ?></td>
						<td class="x-min-100 x-nowrap" data-value="<?php echo $item["pricing_price"] ?>"><?php echo num_commas($item["pricing_price"]); ?></td>
						<td class="x-min-100 x-nowrap" data-value="<?php echo $item["status"]; ?>"><?php echo $status_text; ?></td>
						<td class="x-min-100 x-nowrap" data-value="<?php echo $item["time_ordered"]; ?>"><?php echo $item["time_ordered"]; ?></td>
						<td class="x-nowrap uk-text-center">
							<a title="Cancel Order" draggable="false" href="javascript:" data-id="<?php echo $item["id"]; ?>" data-name="<?php echo $item["id"] . " (" . $item["service_title"] . ")"; ?>" class="item-remove uk-button uk-button-small uk-button-danger">Cancel</a>
						</td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
		</div>
		<div class="uk-datatable-no-contents x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
			<div>
				<h3>No Orders</h3>
				<p>Create an order to get started</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
