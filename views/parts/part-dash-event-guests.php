<h1>Event Guest List</h1>
<nav class="uk-navbar x-toolbar">
	<span class="uk-navbar-brand">
		<span class="x-fw-400 x-stats-guests"><?php echo $event["guest_stats"]["count"]; ?></span> Guests
		<span class="x-fw-400 x-stats-invited"><?php echo $event["guest_stats"]["invited"]; ?></span> Invited
		<span class="x-fw-400 x-stats-confirmed"><?php echo $event["guest_stats"]["confirmed"]; ?></span> Confirmed
	</span>
	<div class="uk-navbar-content uk-navbar-flip  uk-hidden-small">
		<!-- <button title="Import Guest List" class="x-toolbar-import uk-button uk-button-white"><i class="uk-icon-upload"></i><span class="uk-hidden-medium"> Import</span></button> -->
		<button title="Download Guest List" class="x-toolbar-download uk-button uk-button-success uk-margin-small-left"><i class="uk-icon-download"></i><span class="uk-hidden-medium"> Download</span></button>
		<div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
            <button title="Print Guest List" class="uk-button uk-button-primary uk-margin-small-left"><i class="uk-icon-print"></i><span class="uk-hidden-medium"> Print</span></button>
            <div class="uk-dropdown " aria-hidden="true">
                <ul class="uk-nav uk-nav-navbar">
                    <li><a href="javascript:" class="uk-dropdown-close x-toolbar-print-list">Guest List</a></li>
                    <li><a href="javascript:" class="uk-dropdown-close x-toolbar-print-cards">Invitation Cards</a></li>
                </ul>
            </div>
        </div>
	</div>
	<div class="uk-visible-small">
		<ul class="uk-navbar-nav uk-navbar-flip">
			<li class="uk-parent" data-uk-dropdown="{mode:'click'}">
				<a href="javascript:" class="uk-navbar-toggle"></a>
				<div class="uk-dropdown uk-dropdown-navbar x-min-150">
					<ul class="uk-nav uk-nav-navbar">
						<!-- <li><a href="javascript:" class="uk-dropdown-close x-toolbar-import"><i class="uk-icon-upload"></i> Import List</a></li> -->
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-download"><i class="uk-icon-download"></i> Download List</a></li>
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-print-list"><i class="uk-icon-print"></i> Print Guest List</a></li>
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-print-cards"><i class="uk-icon-print"></i> Print Invitation Cards</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
</nav>
<div class="uk-datatable uk-margin-top" data-limit="10" data-row-index="-1">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Guest Name, Contacts, Type, Group">
	</div>
	<div class="uk-width-1-1 uk-form uk-margin-top x-nowrap">
		<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="" checked> All</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="0"> Uninvited</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="1"> Invited</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="2"> Confirmed</label>
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th tabindex="0" class="x-nowrap uk-datatable-row-index">#</th>
						<th tabindex="0" class="x-nowrap">Barcode</th>

						<?php if (isset($event_settings["type"]) && $event_settings["type"] == 1){ ?><th tabindex="0" class="x-nowrap">Type</th><?php } ?>
						<?php if (isset($guest_groups) && is_array($guest_groups) && count($guest_groups)){ ?><th tabindex="0" class="x-nowrap">Group</th><?php } ?>

						<th tabindex="0" class="x-nowrap">Guest Names</th>

						<?php if (isset($event_settings["phone"]) && $event_settings["phone"] == 1){ ?><th tabindex="0" class="x-nowrap">Phone</th><?php } ?>
						<?php if (isset($event_settings["email"]) && $event_settings["email"] == 1){ ?><th tabindex="0" class="x-nowrap">Email</th><?php } ?>
						<?php if (isset($event_settings["address"]) && $event_settings["address"] == 1){ ?><th tabindex="0" class="x-nowrap">Address</th><?php } ?>
						<?php if (isset($event_settings["company"]) && $event_settings["company"] == 1){ ?><th tabindex="0" class="x-nowrap">Company/Org</th><?php } ?>
						<?php if (isset($event_settings["table"]) && $event_settings["table"] == 1){ ?><th tabindex="0" class="x-nowrap">Table No.</th><?php } ?>
						<?php if (isset($event_settings["diet"]) && $event_settings["diet"] == 1){ ?><th tabindex="0" class="x-nowrap">Diet Restrictions</th><?php } ?>
						<?php if (isset($event_settings["extend"]) && $event_settings["extend"] == 1){ ?><th tabindex="0" class="x-nowrap">Extend Invitation</th><?php } ?>
						<?php if (isset($event_settings["extend"]) && $event_settings["extend"] == 1){ ?><th tabindex="0" class="x-nowrap">Expected Guests</th><?php } ?>
						<?php if (count($event_invite_modes)){ ?><th tabindex="0" class="x-nowrap">Invitation Contacts</th><?php } ?>

						<th tabindex="0" class="x-nowrap">Invitation Custom Message</th>

						<?php if (isset($event_settings["pass_question"]) && $event_settings["pass_question"] == 1){ ?><th tabindex="0" class="x-nowrap">Invitation Custom Question</th><?php } ?>
						<?php if (in_array("sms", $event_invite_modes)){ ?><th tabindex="0" class="x-nowrap">Invitation Status SMS</th><?php } ?>
						<?php if (in_array("email", $event_invite_modes)){ ?><th tabindex="0" class="x-nowrap">Invitation Status Email</th><?php } ?>
						<?php if (in_array("whatsapp", $event_invite_modes)){ ?><th tabindex="0" class="x-nowrap">Invitation Status Whatsapp</th><?php } ?>
						<?php if (in_array("facebook", $event_invite_modes)){ ?><th tabindex="0" class="x-nowrap">Invitation Status Facebook</th><?php } ?>
						<?php if (in_array("twitter", $event_invite_modes)){ ?><th tabindex="0" class="x-nowrap">Invitation Status Twitter</th><?php } ?>

						<th tabindex="0" class="x-nowrap">Time Created</th>
						<th tabindex="0" class="x-nowrap">Time Modified</th>
						<th tabindex="0" class="x-nowrap">Time Invited</th>
						<th tabindex="0" class="x-nowrap">Time Confirmed</th>
						<th tabindex="0" class="x-nowrap">Status</th>
						<th class="x-nowrap x-ignore x-italics x-color-b">Guest Names</th>
						<th class="x-nowrap x-ignore uk-text-center">
							<div class="uk-button-dropdown x-bulk-actions" data-uk-dropdown="{mode:'click'}">
                                <a href="javascript:" class="x-pad-10a"><i class="uk-icon-check-square-o"></i></a>
                                <div class="uk-dropdown x-drop-mini uk-text-left">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a data-action="select-all" class="uk-dropdown-close"><i class="uk-icon-check-square"></i> Select All</a></li>
                                        <li><a data-action="select-none" class="uk-dropdown-close"><i class="uk-icon-check-square-o"></i> Select None</a></li>
										<li class="uk-nav-divider"></li>
										<li><a data-action="send-invites" class="uk-dropdown-close"><i class="uk-icon-send"></i> Send Invitations</a></li>
										<li class="uk-nav-divider"></li>
                                        <li><a data-action="print-list" class="uk-dropdown-close"><i class="uk-icon-print"></i> Print Guest List</a></li>
                                        <li><a data-action="print-cards" class="uk-dropdown-close"><i class="uk-icon-print"></i> Print Invitation Cards</a></li>
										<li><a data-action="download-list" class="uk-dropdown-close"><i class="uk-icon-download"></i> Download Guest List</a></li>
										<li class="uk-nav-divider"></li>
                                        <li><a data-action="invited" class="uk-dropdown-close"><i class="uk-icon-toggle-on"></i> Set Invited</a></li>
                                        <li><a data-action="confirmed" class="uk-dropdown-close"><i class="uk-icon-toggle-on"></i> Set Confirmed</a></li>
										<li class="uk-nav-divider"></li>
										<li><a data-action="delete" class="uk-dropdown-close uk-text-danger"><i class="uk-icon-trash"></i> Delete</a></li>
                                    </ul>
                                </div>
                            </div>
						</th>
						<th class="x-nowrap x-block">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($event_guests) && is_array($event_guests) && count($event_guests)){ foreach ($event_guests as $i => $guest){ ?>
					<?php $guest_search = implode(" ", [$guest["names"], $guest["phone"], $guest["email"], $guest["address"], $guest["type"], $guest["group_name"]]); ?>
					<tr id="guest-<?php echo $guest["id"]; ?>" data-id="<?php echo $guest["id"]; ?>" data-search="<?php echo $guest_search; ?>" data-toggle="<?php echo $guest["status"]; ?>" data-base64="<?php echo base64_encode(json_encode($guest)); ?>">
						<td class="x-nowrap x-min-50 uk-datatable-row-index" data-value=""></td>
						<td class="x-nowrap x-min-100" data-name="barcode" data-value="<?php echo $guest["barcode"]; ?>"><?php echo $guest["barcode"]; ?></td>

						<?php if (isset($event_settings["type"]) && $event_settings["type"] == 1){ ?>
						<td class="x-nowrap x-min-100" data-name="type_name" data-value="<?php echo $guest["type_name"]; ?>"><?php echo $guest["type_name"]; ?></td>
						<?php } if (isset($guest_groups) && is_array($guest_groups) && count($guest_groups)){ ?>
						<td class="x-nowrap x-min-100" data-name="group_name" data-value="<?php echo $guest["group_name"]; ?>"><?php echo $guest["group_name"]; ?></td>
						<?php } ?>

						<td class="x-nowrap x-min-100" data-name="names" data-value="<?php echo $guest["names"]; ?>"><?php echo $guest["names"]; ?></td>

						<?php if (isset($event_settings["phone"]) && $event_settings["phone"] == 1){ ?>
						<td class="x-nowrap" data-name="phone" data-value="<?php echo $guest["phone"]; ?>"><?php echo $guest["phone"]; ?></td>
						<?php } if (isset($event_settings["email"]) && $event_settings["email"] == 1){ ?>
						<td class="x-nowrap" data-name="email" data-value="<?php echo $guest["email"]; ?>"><?php echo $guest["email"]; ?></td>
						<?php } if (isset($event_settings["address"]) && $event_settings["address"] == 1){ ?>
						<td class="x-min-150" data-name="address" data-value="<?php echo $guest["address"]; ?>"><?php echo $guest["address"]; ?></td>
						<?php } if (isset($event_settings["company"]) && $event_settings["company"] == 1){ ?>
						<td class="x-min-100" data-name="company"  data-value="<?php echo $guest["company"]; ?>"><?php echo $guest["company"]; ?></td>
						<?php } if (isset($event_settings["table"]) && $event_settings["table"] == 1){ ?>
						<td class="x-nowrap" data-name="table_no" data-value="<?php echo $guest["table_no"]; ?>"><?php echo $guest["table_no"]; ?></td>
						<?php } if (isset($event_settings["diet"]) && $event_settings["diet"] == 1){ ?>
						<td class="x-min-150" data-name="diet" data-value="<?php echo $guest["diet"]; ?>"><?php echo $guest["diet"]; ?></td>
						<?php } if (isset($event_settings["extend"]) && $event_settings["extend"] == 1){ ?>
						<td class="x-nowrap" data-name="extend_inv_text" data-value="<?php echo $guest["extend_inv"]; ?>"><?php echo $guest["extend_inv_text"]; ?></td>
						<?php } if (isset($event_settings["extend"]) && $event_settings["extend"] == 1){ ?>
						<td class="x-nowrap" data-name="expected_guests" data-value="<?php echo $guest["expected_guests"]; ?>"><?php echo $guest["expected_guests"]; ?></td>
						<?php } if (count($event_invite_modes)){ ?>
						<td class="x-min-150" data-name="inv_contacts" data-value="<?php echo $guest["inv_contacts"]; ?>"><?php echo $guest["inv_contacts"]; ?></td>
						<?php } ?>

						<td class="x-min-150" data-name="inv_message" data-value="<?php echo $guest["inv_message"]; ?>"><?php echo $guest["inv_message"]; ?></td>

						<?php if (isset($event_settings["pass_question"]) && $event_settings["pass_question"] == 1){ ?>
						<td class="x-min-150" data-name="inv_qa" data-value="<?php echo $guest["inv_qa"]; ?>"><?php echo $guest["inv_qa"]; ?></td>
						<?php } if (in_array("sms", $event_invite_modes)){ ?>
						<td class="x-min-100" data-name="status_sms" data-value="<?php echo $guest["status_sms"]; ?>"><?php echo $guest["status_sms"]; ?></td>
						<?php } if (in_array("email", $event_invite_modes)){ ?>
						<td class="x-min-100" data-name="status_email" data-value="<?php echo $guest["status_email"]; ?>"><?php echo $guest["status_email"]; ?></td>
						<?php } if (in_array("whatsapp", $event_invite_modes)){ ?>
						<td class="x-min-100" data-name="status_whatsapp" data-value="<?php echo $guest["status_whatsapp"]; ?>"><?php echo $guest["status_whatsapp"]; ?></td>
						<?php } if (in_array("facebook", $event_invite_modes)){ ?>
						<td class="x-min-100" data-name="status_fb" data-value="<?php echo $guest["status_fb"]; ?>"><?php echo $guest["status_fb"]; ?></td>
						<?php } if (in_array("twitter", $event_invite_modes)){ ?>
						<td class="x-min-100" data-name="status_twitter" data-value="<?php echo $guest["status_twitter"]; ?>"><?php echo $guest["status_twitter"]; ?></td>
						<?php } ?>

						<td class="x-nowrap x-min-100" data-name="time_created" data-value="<?php echo $guest["time_created"]; ?>"><?php echo $guest["time_created"]; ?></td>
						<td class="x-nowrap x-min-100" data-name="time_modified" data-value="<?php echo $guest["time_modified"]; ?>"><?php echo $guest["time_modified"]; ?></td>
						<td class="x-nowrap x-min-100" data-name="time_invited" data-value="<?php echo $guest["time_invited"]; ?>"><?php echo $guest["time_invited"]; ?></td>
						<td class="x-nowrap x-min-100" data-name="time_confirmed" data-value="<?php echo $guest["time_confirmed"]; ?>"><?php echo $guest["time_confirmed"]; ?></td>
						<td class="x-nowrap x-min-100" data-name="status_text" data-value="<?php echo $guest["status"]; ?>"><?php echo $guest["status_text"]; ?></td>
						<td class="x-nowrap x-min-100 x-italics x-color-b"><?php echo $guest["names"]; ?></td>
						<td class="uk-text-center"><div class="uk-form"><label class="x-checkbox"><input class="x-bulk-action" data-id="<?php echo $guest["id"]; ?>" type="checkbox"></label></div></td>
						<td class="x-nowrap uk-text-center">
							<a title="Preview" draggable="false" href="javascript:" data-id="<?php echo $guest["id"]; ?>" class="guest-preview uk-button uk-button-small uk-button-white"><i class="uk-icon-send"></i> Invitation</a>
							<a title="Edit" draggable="false" href="javascript:" data-id="<?php echo $guest["id"]; ?>" data-status="<?php echo $guest["status"]; ?>" class="guest-edit uk-button uk-button-small uk-button-primary"><i class="uk-icon-pencil"></i></a>
							<a title="Delete" draggable="false" href="javascript:" data-id="<?php echo $guest["id"]; ?>" class="guest-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>
						</td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
		</div>
		<div class="uk-datatable-no-content x-nothing x-border-bottom x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
			<div>
				<h3>No Guests</h3>
				<p>Add guests to your list and start managing invitations.</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-border-bottom x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
		<div class="x-edit-guest-toggle uk-text-right x-pad-20">
			<button class="uk-button uk-button-primary">Add Guest</button>
		</div>
		<form id="guest_form" action="javascript:" class="x-pad-20 uk-form x-hidden">
			<div class="x-border-bottom uk-margin-top uk-margin-bottom">
				<h2 class="x-nomargin">
					EDIT GUEST DETAILS
					<a href="#" id="guest_cancel_mini" class="uk-align-right x-color-b" draggable="false"><i class="uk-icon-close"></i></a>
					<a href="#" id="guest_save_mini" class="uk-align-right" draggable="false"><i class="uk-icon-save"></i></a>
				</h2>
				<small><strong>NOTE:</strong> Fields marked <span class="uk-text-danger">*</span> must be provided</small>
			</div>
			<div id="guest_error"></div>
			<div class="uk-form-row">
				<input name="guest_id" type="hidden">
				<div class="uk-grid uk-grid-small" data-uk-grid-margin>
					<div class="uk-width-medium-1-2 uk-width-large-1-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_status">Status</label>
							<div class="uk-form-controls">
								<select id="guest_status" name="guest_status" class="uk-width-1-1" data-default="0">
									<option value="0" selected="selected">Default</option>
									<option value="1">Invited</option>
									<option value="2">Confirmed</option>
								</select>
							</div>
						</div>
					</div>
					<div class="uk-width-medium-1-2 uk-width-large-1-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_first_name">First Name <span class="uk-text-danger">*</span></label>
							<div class="uk-form-controls">
								<input data-validate="required" id="guest_first_name" name="guest_first_name" class="uk-width-1-1" onkeyup="this.value = camelCase(this.value)" type="text" placeholder="First Name">
							</div>
						</div>
					</div>
					<?php if (isset($event_settings["last_name"]) && $event_settings["last_name"] == 1){ ?>
					<div class="uk-width-medium-1-2 uk-width-large-1-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_last_name">Last Name</label>
							<div class="uk-form-controls">
								<input id="guest_last_name" name="guest_last_name" class="uk-width-1-1" onkeyup="this.value = camelCase(this.value)" type="text" placeholder="Last Name">
							</div>
						</div>
					</div>
					<?php } if (isset($event_settings["type"]) && $event_settings["type"] == 1){ ?>
					<div class="uk-width-medium-1-2 uk-width-large-1-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_type">Type</label>
							<div class="uk-form-controls">
								<select id="guest_type" name="guest_type" class="uk-width-1-1" data-default="0">
									<option value="0" selected="selected">General Admission</option>
									<option value="1">VIP</option>
									<option value="2">VVIP</option>
								</select>
							</div>
						</div>
					</div>
					<?php } if (isset($guest_groups) && is_array($guest_groups) && count($guest_groups)){ ?>
					<div class="uk-width-medium-1-2 uk-width-large-1-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_group">Group</label>
							<div class="uk-form-controls">
								<select id="guest_group" name="guest_group" class="uk-width-1-1" data-default="">
									<option class="x-color-b" value="" selected="selected">Select Group</option>
									<?php foreach ($guest_groups as $group){ ?>
									<option value="<?php echo $group["id"]; ?>"><?php echo $group["name"]; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<?php } if (isset($event_settings["phone"]) && $event_settings["phone"] == 1){ ?>
					<div class="uk-width-medium-1-2 uk-width-large-1-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_phone">Phone</label>
							<div class="uk-form-controls">
								<input data-validate="phone-any" id="guest_phone" name="guest_phone" class="uk-width-1-1" type="text" placeholder="Phone Number">
							</div>
						</div>
					</div>
					<?php } if (isset($event_settings["email"]) && $event_settings["email"] == 1){ ?>
					<div class="uk-width-1-1 x-nomargin"></div>
					<div class="uk-width-large-2-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_email">Email</label>
							<div class="uk-form-controls">
								<input data-validate="email" id="guest_email" name="guest_email" class="uk-width-1-1" type="text" placeholder="Email Address">
							</div>
						</div>
					</div>
					<?php } if (isset($event_settings["company"]) && $event_settings["company"] == 1){ ?>
					<div class="uk-width-1-1 x-nomargin"></div>
					<div class="uk-width-large-2-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_company">Company</label>
							<div class="uk-form-controls">
								<input id="guest_company" name="guest_company" class="uk-width-1-1" type="text" placeholder="Company/Organization" onkeyup="this.value = sentenseCase(this.value)">
							</div>
						</div>
					</div>
					<?php } if (isset($event_settings["address"]) && $event_settings["address"] == 1){ ?>
					<div class="uk-width-1-1 x-nomargin"></div>
					<div class="uk-width-large-2-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_address">Address</label>
							<div class="uk-form-controls">
								<textarea id="guest_address" name="guest_address" class="uk-width-1-1" placeholder="Postal/Physical Address" onkeyup="this.value = sentenseCase(this.value)"></textarea>
							</div>
						</div>
					</div>
					<?php } if (isset($event_settings["diet"]) && $event_settings["diet"] == 1){ ?>
					<div class="uk-width-1-1 x-nomargin"></div>
					<div class="uk-width-large-2-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_diet">Diet Restrictions</label>
							<div class="uk-form-controls">
								<textarea id="guest_diet" name="guest_diet" class="uk-width-1-1" placeholder="Diet Restrictions/Instructions" onkeyup="this.value = sentenseCase(this.value)"></textarea>
							</div>
						</div>
					</div>
					<?php } if (isset($event_settings["table"]) && $event_settings["table"] == 1){ ?>
					<div class="uk-width-large-1-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_table_number">Table Number</label>
							<div class="uk-form-controls">
								<input id="guest_table_number" name="guest_table_number" class="uk-width-1-1" type="text" placeholder="Table Number">
							</div>
						</div>
					</div>
					<?php } if (isset($event_settings["extend"]) && $event_settings["extend"] == 1){ ?>
					<div class="uk-width-1-1 x-nomargin"></div>
					<div class="uk-width-large-2-3">
						<div class="uk-form-row">
							<label class="x-checkbox"><input type="checkbox" name="guest_extend_inv"> Extend Invitation</label>
							<br><small>Allow guest to bring company?</small>
						</div>
						<div class="uk-form-row x-extend_inv-toggle">
							<label class="uk-form-label" for="guest_expected">Expected Guests</label>
							<div class="uk-form-controls">
								<input data-validate="number+" data-default="1" id="guest_expected" name="guest_expected" onkeypress="onKeyPressNumber(event, true)" onblur="this.value = num(this.value, '')" class="uk-width-1-1" type="number" value="1" placeholder="Number Of (Guest + Company)">
							</div>
							<small>Number of expected guests (Guest + Company)</small>
						</div>
					</div>
					<?php } ?>
					<div class="uk-margin-top uk-margin-bottom uk-width-1-1">
						<p class="x-nomargin"><strong>INVITATION DETAILS</strong></p>
						<small><strong>NOTE:</strong> When you do not provide invitation contacts, this guest will not receive an online invitation.</small>
						<div class="x-border-bottom uk-width-1-1"></div>
					</div>
					<?php if (in_array("sms", $event_invite_modes)){ ?>
					<div class="uk-width-medium-1-2">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_inv_phone">Invitation Phone Number</label>
							<div class="uk-form-controls">
								<input data-validate="phone" id="guest_inv_phone" name="guest_inv_phone" class="uk-width-1-1" type="text" placeholder="SMS/Whatsapp Phone Contact">
							</div>
						</div>
					</div>
					<?php } if (in_array("email", $event_invite_modes)){ ?>
					<div class="uk-width-medium-1-2">
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_inv_email">Invitation Email Address</label>
							<div class="uk-form-controls">
								<input data-validate="email" id="guest_inv_email" name="guest_inv_email" class="uk-width-1-1" type="text" placeholder="Email Contact">
							</div>
						</div>
					</div>
					<?php } ?>
					<div class="uk-width-1-1">
						<?php if (!count($event_invite_modes)){ ?>
						<div class="uk-margin-bottom">
							<small class="uk-text-warning"><strong>Event has no invitation modes selected.</strong> You will not be able to send invitations (printing is not affected). Edit event settings to change.</small>
						</div>
						<?php } ?>
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_inv_message">Invitation Custom Message</label>
							<div class="uk-form-controls">
								<textarea id="guest_inv_message" name="guest_inv_message" class="uk-width-1-1" placeholder="Custom Accompanying Message" onkeyup="this.value = sentenseCase(this.value)"></textarea>
							</div>
						</div>
						<?php if (isset($event_settings["pass_question"]) && $event_settings["pass_question"] == 1){ ?>
						<div class="uk-form-row">
							<label class="uk-form-label" for="guest_inv_question">Invitation Custom Question</label>
							<div class="uk-form-controls">
								<div class="uk-grid uk-grid-small" data-uk-grid-margin>
									<div class="uk-width-medium-1-2">
										<input data-validate="qa-q" id="guest_inv_question" name="guest_inv_question" class="uk-width-1-1" type="text" placeholder="Custom Question" onkeyup="this.value = sentenseCase(this.value)">
									</div>
									<div class="uk-width-medium-1-2">
										<input data-validate="qa-a" id="guest_inv_answer" name="guest_inv_answer" class="uk-width-1-1" type="text" placeholder="Answer" onkeyup="this.value = sentenseCase(this.value)">
									</div>
								</div>
							</div>
						</div>
						<?php } if (isset($event_settings["rsvp"]) && $event_settings["rsvp"] == 1){ ?>
						<div class="uk-form-row">
							<div class="uk-form-controls">
								<label class="x-checkbox"><input type="checkbox" name="guest_inv_edits"> Invitation RSVP Allow Guest Edits</label>
								<br><small>When enabled guests will be provided with a form to fill/change any information about them that you might have missed when submitting their online RSVP.</small>
							</div>
						</div>
						<?php } else { ?>
						<div class="uk-margin-bottom">
							<small class="uk-text-warning"><strong>Event online RSVP is disabled.</strong> Guests cannot remotely RSVP to your event to confirm attendance (You can manually confirm guest's attendance using the bulk actions). Edit event settings to change.</small>
						</div>
						<?php } ?>
						<p class="uk-text-center uk-margin-top x-color-b">Coming Soon: Whatsapp, Facebook &amp; Twitter Messaging</p>
					</div>
				</div>
				<div class="uk-margin-top uk-text-right">
					<button title="Cancel Editing" id="guest_cancel" type="button" class="uk-button uk-button-white">Cancel</button>
					<button title="Save Details" id="guest_save" type="submit" class="uk-button uk-button-success x-min-150">Save Details</button>
					<button title="Please wait..." id="guest_saving" type="button" class="uk-button uk-button-default uk-disabled x-min-150 x-hidden" disabled="disabled"><i class="uk-icon-spinner uk-icon-spin"></i> Saving...</button>
				</div>
			</div>
		</form>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
<script type="text/javascript">
	let EVENT_DATA = <?php echo json_encode($event); ?>;
	let EVENT_SETTINGS = <?php echo json_encode($event_settings); ?>;
	let EVENT_GUEST_GROUPS = <?php echo json_encode($guest_groups); ?>;
	let EVENT_INVITE_MODES = <?php echo json_encode($event_invite_modes); ?>;
</script>
