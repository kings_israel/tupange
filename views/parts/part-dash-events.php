<h1>My Events <a href="<?php echo $root; ?>/dashboard?edit-event" class="uk-button uk-button-success uk-align-right">New Event</a></h1>
<div class="uk-datatable" data-limit="5">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Event Name, Description, Type, Status">
	</div>
	<div class="uk-width-1-1 uk-form uk-margin-top x-nowrap">
		<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="" checked> All</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="Active"> Active</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="Live"> Live</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="Past"> Past</label>
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th tabindex="0" class="x-nowrap">Event Name</th>
						<th tabindex="0" class="x-nowrap">Event Type</th>
						<th tabindex="0" class="x-nowrap">Event Time</th>
						<th tabindex="0" class="x-nowrap">Event Status</th>
						<th class="x-nowrap x-block">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($user_events) && is_array($user_events) && count($user_events)){ foreach($user_events as $i => $event){ $event_status = $event["event_status"];?>
					<?php $event_search = $event["name"] . " " . $event["description"] . " " . $event["type"]["name"] . " " . $event_status; ?>
					<tr class="<?php echo "x-event-" . strtolower($event_status); ?> <?php if ($event['guest']) echo 'x-guest'; ?>" data-search="<?php echo $event_search; ?>" data-toggle="<?php echo $event_status; ?>">
						<td class="x-min-150" data-value="<?php echo $event["name"]; ?>">
							<a title="Click to view event dashboard" href="<?php echo $root; ?>/dashboard?event=<?php echo $event["id"]; ?>">
								<div class="x-event-name"><?php if ($event['users']) echo '<i class="uk-icon-users"></i> '; ?><?php echo $event["name"]; ?></div>
								<div class="x-event-description"><?php echo $event["description"] != "" ? $event["description"] : "Event has no description"; ?></div>
							</a>
							<div class="x-event-timestamp"><?php echo $event["timestamp"]; ?></div>
						</td>
						<td class="x-nowrap" data-value="<?php echo $event["type"]["name"]; ?>"><?php echo $event["type"]["name"]; ?></td>
						<td class="x-nowrap" data-value="<?php echo $event["start"][2]; ?>"><?php echo str_replace(" - ", " -<br> ", $event["display_time"]); ?></td>
						<td class="x-nowrap" data-value="<?php echo $event_status; ?>">
							<span class="x-event-status x-uppercase x-fw-400"><?php echo $event_status; ?></span>
						</td>
						<td class="x-nowrap uk-text-center">
							<?php if ($event['role'] == 0){ ?>
							<a title="Event Dashboard" href="<?php echo $root; ?>/dashboard?event=<?php echo $event["id"]; ?>" class="uk-button uk-button-small uk-button-primary"><i class="uk-icon-edit"></i></a>
							<a title="Edit Event" href="<?php echo $root; ?>/dashboard?edit-event=<?php echo $event["id"]; ?>" class="uk-button uk-button-small uk-button-white"><i class="uk-icon-pencil"></i></a>
							<?php if (!$event['guest']){ ?>
							<a title="Delete" draggable="false" href="javascript:" data-id="<?php echo $event["id"]; ?>" data-name="<?php echo $event["name"]; ?>" class="event-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>
							<?php } ?>
							<?php } ?>
						</td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
		</div>
		<div class="uk-datatable-no-contents x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/cal.png" />
			<div>
				<h3>No Events</h3>
				<p>Create an event to get started</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
