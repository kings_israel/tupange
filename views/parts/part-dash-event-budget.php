<h1>Budget &amp; Expenses</h1>
<noscript>
	<div class="uk-alert uk-alert-danger" data-uk-alert>
		<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
	</div>
</noscript>
<div class="uk-margin-top uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
	<div class="uk-width-large-1-3">
		<div class="x-box">
			<h2 class="x-box-title">Budgets <a id="groups-toggle" href="javascript:" onclick="toggleGroupsContent()" class="uk-hidden-large uk-navbar-toggle"></a></h2>
			<div id="groups-toggle-content" class="uk-visible-large <?php if (!count($budgets)) echo "x-display-block"; ?>">
				<div class="x-pad-20">
					<div class="uk-form-icon uk-width-1-1">
						<i class="uk-icon-search"></i>
						<input class="uk-width-1-1 x-groups-search" type="text" placeholder="Search Groups">
					</div>
				</div>
				<ul class="x-budget-groups">
					<?php foreach ($budgets as $i => $group){ $item_search = trim(implode(' ', [$group['title'], $group['description']])); ?>
					<li tabindex="0" data-id="<?php echo $group["id"]; ?>" data-search="<?php echo $item_search; ?>">
						<div class="x-actions">
							<a href="javascript:" title="Edit" draggable="false" data-id="<?php echo $group["id"]; ?>" class="group-edit uk-text-primary"><i class="uk-icon-pencil"></i></a>
							<a href="javascript:" title="Delete" draggable="false" data-id="<?php echo $group["id"]; ?>" class="group-delete uk-text-danger"><i class="uk-icon-trash"></i></a>
						</div>
						<h3><?php echo $group["title"]; ?></h3>
						<small class="x-nowrap"><?php echo $group["timestamp"] ?> | <strong>Bal:</strong> <?php echo num_commas($group["balance"]) ?></small>
					</li>
					<?php } ?>
				</ul>
				<div class="x-group-add uk-text-right x-pad-20">
					<button class="uk-button uk-button-primary">Add Budget</button>
				</div>
				<form class="x-group-edit uk-form x-border-top x-hidden" action="javascript:" method="post">
					<div class="x-pad-20">
						<input id="budget_id" type="hidden" name="group_id" value="">
						<div class="uk-form-row">
							<label class="uk-form-label" for="budget_title">Budget Title <span class="uk-text-danger">*</span></label>
							<div class="uk-form-controls">
								<input data-validate="required" id="budget_title" name="budget_title" class="uk-width-1-1" onkeyup="this.value = sentenseCase(this.value)" type="text" placeholder="Budget Title">
							</div>
						</div>
						<div class="uk-form-row">
							<label class="uk-form-label" for="budget_description">Description <i>(Optional)</i></label>
							<div class="uk-form-controls">
								<textarea class="uk-width-1-1" id="budget_description" name="budget_description" placeholder="Description" onkeyup="this.value = sentenseCase(this.value)"></textarea>
							</div>
						</div>
					</div>
					<div class="x-border-top x-pad-20 uk-text-right">
						<button type="button" class="x-group-edit-cancel uk-button">Cancel</button>
						<button type="submit" class="x-group-edit-save uk-button uk-button-success">Save</button>
					</div>
				</form>
			</div>
			<script type="text/javascript">
				function toggleGroupsContent(){
					let groups_content = document.getElementById("groups-toggle-content");
					if (groups_content.className.indexOf("x-display-block") > -1) groups_content.classList.remove("x-display-block");
					else groups_content.classList.add("x-display-block");
				}
			</script>
		</div>
	</div>
	<div class="uk-width-large-2-3">
		<div class="x-box x-pad-20 x-get-started uk-text-center uk-height-1-1 <?php if (count($budgets)) echo 'x-hidden'; ?>">
			<?php $mask_img = $root . "/assets/img/icons/budget.png"; ?>
			<span draggable="false" class="x-mask-image" style="mask-image:url('<?php echo $mask_img; ?>');-webkit-mask-image:url('<?php echo $mask_img; ?>')"></span>
			<h2>Get Started</h2>
			<p>Create a budget, top up and manage your event transactions.</p>
		</div>
		<div class="x-budget-display x-hidden">
			<div class="x-box x-pad-20 x-display-info">
				<h2>Budget Title</h2>
				<p>Budget description</p>
			</div>
			<div class="uk-margin-top uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
				<div class="uk-width-medium-1-3">
					<div class="x-box uk-height-1-1 x-pad-20 x-budget-summary total">
						<p>Budget</p>
						<h2><small>KES</small> <span class="x-summary-total">1,000.00</span></h2>
					</div>
				</div>
				<div class="uk-width-medium-1-3">
					<div class="x-box uk-height-1-1 x-pad-20 x-budget-summary spent">
						<p>Spent</p>
						<h2><small>KES</small> <span class="x-summary-spent">1,000.00</span></h2>
					</div>
				</div>
				<div class="uk-width-medium-1-3">
					<div class="x-box uk-height-1-1 x-pad-20 x-budget-summary balance">
						<p>Balance</p>
						<h2><small>KES</small> <span class="x-summary-balance">1,000.00</span></h2>
					</div>
				</div>
			</div>
			<div class="x-box uk-margin-top">
				<div class="x-pad-20 x-border-bottom">
					<h3 class="x-table-title">Budget Transactions</h3>
					<div class="uk-form-icon uk-width-1-1">
						<i class="uk-icon-search"></i>
						<input class="uk-width-1-1 x-search-transactions" type="text" placeholder="Search Transactions">
					</div>
					<div class="x-transactions-toggle uk-form uk-margin-top x-nowrap">
						<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="" checked> All</label>
						&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="0"> Expenses</label>
						&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="1"> Top Up</label>
					</div>
				</div>
				<div class="x-transactions-table x-table">
					<table class="uk-table uk-table-condensed">
						<thead>
							<tr>
								<th class="x-nowrap">#</th>
								<th class="x-nowrap">Date</th>
								<th class="x-nowrap">Title</th>
								<th class="x-nowrap">Debit</th>
								<th class="x-nowrap">Credit</th>
								<th class="x-nowrap">Balance</th>
								<th class="x-nowrap">Type</th>
								<th class="x-nowrap">Reference</th>
								<th class="x-nowrap">Description</th>
								<th class="x-nowrap">Timestamp</th>
								<th class="x-nowrap">Actions</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="x-transactions-add uk-text-right x-pad-20">
					<button class="uk-button uk-button-primary">New Transaction</button>
				</div>
				<form id="transaction_form" class="uk-form x-border-top x-hidden" action="javascript:" method="post">
					<div class="x-pad-20">
						<input type="hidden" id="transaction_budget_id" value="">
						<input type="hidden" id="transaction_id" value="">
						<div class="uk-grid uk-grid-small" data-uk-grid-margin>
							<div class="uk-width-medium-1-2">
								<div class="uk-form-row">
									<label class="uk-form-label" for="transaction_date">Transaction Date <span class="uk-text-danger">*</span></label>
									<div class="uk-form-controls">
										<div class="uk-form-icon uk-form-icon-flip uk-width-1-1">
											<i class="uk-icon-calendar"></i>
											<input type="text" class="uk-width-1-1" id="transaction_date" name="transaction_date" placeholder="DD/MM/YYYY" data-uk-datepicker="{format:'DD/MM/YYYY',pos:'bottom'}">
										</div>
									</div>
								</div>
							</div>
							<div class="uk-width-medium-1-2">
								<div class="uk-form-row">
									<label class="uk-form-label" for="transaction_type">Transaction Type<span class="uk-text-danger">*</span></label>
									<div class="uk-form-controls">
										<select id="transaction_type" name="transaction_type" class="uk-width-1-1" data-default="0">
											<option value="0" selected="selected">Expense</option>
											<option value="1">Top Up</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="uk-form-row uk-margin-top">
							<label class="uk-form-label" for="transaction_title">Title <span class="uk-text-danger">*</span></label>
							<div class="uk-form-controls">
								<input type="text" id="transaction_title" name="transaction_title" class="uk-width-1-1" onkeyup="this.value = sentenseCase(this.value)" placeholder="Transaction Title">
							</div>
						</div>
						<div class="uk-form-row">
							<label class="uk-form-label" for="transaction_description">Description</label>
							<div class="uk-form-controls">
								<textarea id="transaction_description" name="transaction_description" class="uk-width-1-1" onkeyup="this.value = sentenseCase(this.value)" placeholder="Transaction Description"></textarea>
							</div>
						</div>
						<div class="uk-form-row">
							<label class="uk-form-label" for="transaction_amount">Amount</label>
							<div class="uk-form-controls">
								<input id="transaction_amount" name="transaction_amount" class="uk-width-1-1" type="number" step="0.01" placeholder="KES 0" onkeypress="onKeyPressNumber(event, true)" onblur="this.value = num(this.value, '')">
							</div>
						</div>
						<div class="uk-form-row">
							<label class="uk-form-label" for="transaction_reference">Reference</label>
							<div class="uk-form-controls">
								<input type="text" id="transaction_reference" name="transaction_reference" class="uk-width-1-1" placeholder="Transaction Reference">
							</div>
						</div>
					</div>
					<div class="x-border-top x-pad-20 uk-text-right">
						<button type="button" class="x-transaction-edit-cancel uk-button">Cancel</button>
						<button type="submit" class="x-transaction-edit-save uk-button uk-button-success">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	let EVENT_DATA = <?php echo json_encode($event); ?>;
	let BUDGET_DATA = <?php echo json_encode($budgets); ?>;
</script>
