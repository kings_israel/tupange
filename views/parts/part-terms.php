<div id="modal-terms" class="uk-modal">
	<div class="uk-modal-dialog x-modal x-modal-nopad">
		<a class="uk-modal-close uk-close"></a>
		<div class="uk-modal-header">TERMS OF USE</div>
		<div class="uk-overflow-container">
			<div class="x-pad-20">
				<?php include __DIR__ . "/html-terms.php"; ?>
			</div>
		</div>
	</div>
</div>
<div id="modal-privacy" class="uk-modal">
	<div class="uk-modal-dialog x-modal x-modal-nopad">
		<a class="uk-modal-close uk-close"></a>
		<div class="uk-modal-header">PRIVACY POLICY</div>
		<div class="uk-overflow-container">
			<div class="x-pad-20">
				<?php include __DIR__ . "/html-privacy.php"; ?>
			</div>
		</div>
	</div>
</div>
