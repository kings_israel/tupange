<h1>Manage Attendance</h2>
<nav class="uk-navbar x-toolbar">
	<span class="uk-navbar-brand">
		<span class="x-fw-400 x-stats-all"><?php echo $attendance_stats["all"]; ?></span> All
		<span class="x-fw-400 x-stats-invite"><?php echo $attendance_stats["invite"]; ?></span> Invite
		<span class="x-fw-400 x-stats-ticket"><?php echo $attendance_stats["ticket"]; ?></span> Ticket
	</span>
	<div class="uk-navbar-content uk-navbar-flip  uk-hidden-small">
		<button title="Search Guest" class="x-toolbar-search uk-button uk-button-success uk-margin-small-left"><i class="uk-icon-search"></i><span class="uk-hidden-medium"> Search Guest</span></button>
		<button title="Download List" class="x-toolbar-download uk-button uk-button-primary uk-margin-small-left"><i class="uk-icon-download"></i></button>
		<button title="Print List" class="x-toolbar-print uk-button uk-button-primary uk-margin-small-left"><i class="uk-icon-print"></i></button>
	</div>
	<div class="uk-visible-small">
		<ul class="uk-navbar-nav uk-navbar-flip">
			<li class="uk-parent" data-uk-dropdown="{mode:'click'}">
				<a href="javascript:" class="uk-navbar-toggle"></a>
				<div class="uk-dropdown uk-dropdown-navbar x-min-150">
					<ul class="uk-nav uk-nav-navbar">
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-search"><i class="uk-icon-search"></i> Search Guest</a></li>
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-download"><i class="uk-icon-download"></i> Download List</a></li>
						<li><a href="javascript:" class="uk-dropdown-close x-toolbar-print"><i class="uk-icon-print"></i> Print List</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
</nav>
<div class="uk-datatable uk-margin-top" data-limit="10" data-row-index="-1">
	<div class="uk-form-icon uk-width-1-1">
		<i class="uk-icon-search"></i>
		<input class="uk-width-1-1 uk-datatable-search" type="text" placeholder="Search Attendance Records">
	</div>
	<div class="uk-width-1-1 uk-form uk-margin-top x-nowrap">
		<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="" checked> All</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="IN"> Check In</label>
		&nbsp;<label class="x-radio"><input type="radio" name="uk-datatable-toggle" value="OUT"> Check Out</label>
	</div>
	<div class="x-box x-overflow-hidden-x uk-margin-top uk-datatable-wrapper">
		<noscript>
			<div class="x-pad-20">
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</div>
		</noscript>
		<div class="x-table x-overflow-hidden-y x-overflow-auto-x">
			<table class="uk-table">
				<thead>
					<tr>
						<th tabindex="0" class="x-nowrap uk-datatable-row-index">#</th>
						<th tabindex="0" class="x-nowrap">Barcode</th>
						<th tabindex="0" class="x-nowrap">Type</th>
						<th tabindex="0" class="x-nowrap">Check in/out</th>
						<th tabindex="0" class="x-nowrap">Names</th>
						<th tabindex="0" class="x-nowrap">Phone</th>
						<th tabindex="0" class="x-nowrap">Email</th>
						<th tabindex="0" class="x-nowrap">Timestamp</th>
						<th class="x-nowrap x-block">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($attendance_list) && is_array($attendance_list) && count($attendance_list)){ foreach ($attendance_list as $i => $item){ ?>
					<?php $item_search = implode(" ", [$item["barcode"], $item["type"], $item["direction"], $item["names"], $item["phone"], $item["email"]]); ?>
					<tr data-id="<?php echo $item["id"]; ?>" data-type="<?php echo $item["type"]; ?>" data-barcode="<?php echo $item["barcode"]; ?>" data-search="<?php echo $item_search; ?>" data-toggle="<?php echo $item["direction"]; ?>">
						<td class="x-nowrap x-min-50 uk-datatable-row-index" data-value=""></td>
						<td class="x-nowrap x-min-100" data-value="<?php echo $item["barcode"]; ?>"><?php echo $item["barcode"]; ?></td>
						<td class="x-nowrap x-min-100" data-value="<?php echo $item["type"]; ?>"><?php echo $item["type"]; ?></td>
						<td class="x-nowrap x-min-100" data-value="<?php echo $item["direction"]; ?>">CHECK <?php echo $item["direction"]; ?></td>
						<td class="x-nowrap x-min-100" data-value="<?php echo $item["names"]; ?>"><?php echo $item["names"]; ?></td>
						<td class="x-nowrap x-min-100" data-value="<?php echo $item["phone"]; ?>"><?php echo $item["phone"]; ?></td>
						<td class="x-nowrap x-min-100" data-value="<?php echo $item["email"]; ?>"><?php echo $item["email"]; ?></td>
						<td class="x-nowrap x-min-100" data-value="<?php echo $item["timestamp"]; ?>"><?php echo $item["timestamp"]; ?></td>
						<td class="x-nowrap uk-text-center">
							<a title="Delete" draggable="false" href="javascript:" data-id="<?php echo $item["id"]; ?>" data-names="<?php echo $item["names"]; ?>" class="attendance-delete uk-button uk-button-small uk-button-danger"><i class="uk-icon-trash"></i></a>
						</td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
		</div>
		<div class="uk-datatable-no-content x-nothing x-border-bottom x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
			<div>
				<h3>No Attendance Records</h3>
				<p>Search and add guest to your attendance records.</p>
			</div>
		</div>
		<div class="uk-datatable-no-results x-nothing x-border-bottom x-hidden">
			<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
			<div>
				<h3>No Results Found</h3>
				<p>Refine your search/filters to get different results.</p>
			</div>
		</div>
	</div>
	<div class="uk-datatable-pagination"></div>
</div>
<script type="text/javascript">
	let EVENT_DATA = <?php echo json_encode($event); ?>;
</script>
