<?php
require_once __DIR__ . "/../service/global-data.php";

//counts
if (($session_cart_count = get_cart_badge_count()) === false) die ($get_cart_badge_count_error);
if (($session_unread_orders_count = get_unread_orders_count()) === false) die ($get_unread_orders_count_error);
if (($session_messages_count = get_unread_messages_count()) === false) die ($get_unread_messages_count_error);

//rdr
$rdr = isset($_GET["redirect"]) ? "?redirect=" . urlencode($_GET["redirect"]) : "";

//event start date
$event_start_date = '';
if (isset($event) && array_key_exists('start_timestamp', $event)){
	$temp = $event['start_timestamp'];
	$timestamp = DateTime::createFromFormat('Y-m-d H:i:s', $temp);
	if ($timestamp -> format('Y-m-d H:i:s') == $temp) $event_start_date = $timestamp -> format('d/m/Y');
	unset($temp);
	unset($timestamp);
}
if (!$event_start_date && isset($event) && array_key_exists('start', $event) && is_array($event['start']) && count($event['start']) == 4) $event_start_date = $event['start'][0];
if (!$event_start_date && isset($event_data) && array_key_exists('start_timestamp', $event_data)){
	$temp = $event_data['start_timestamp'];
	$timestamp = DateTime::createFromFormat('Y-m-d H:i:s', $temp);
	if ($timestamp -> format('Y-m-d H:i:s') == $temp) $event_start_date = $timestamp -> format('d/m/Y');
	unset($temp);
	unset($timestamp);
}
if (!$event_start_date && isset($event_data) && array_key_exists('start', $event_data) && is_array($event_data['start']) && count($event_data['start']) == 4) $event_start_date = $event_data['start'][0];
if (!$event_start_date) unset($event_start_date);

//search url
$search_url = $root . '/search' . (isset($event_start_date) ? '?sd=' . urlencode(base64_encode($event_start_date)) : '');

?>
<div id="offcanvas" class="uk-offcanvas">
	<div class="uk-offcanvas-bar">
		<ul data-uk-nav class="uk-nav uk-nav-offcanvas uk-nav-parent-icon">
			<li>&nbsp;</li>
			<?php if (sessionActive()){ ?>
			<li class="<?php if ($route_page == "dashboard") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/dashboard">Events</a></li>
			<?php if ($session_userlevel >= 2){ ?>
			<li class="<?php if ($route_page == "services") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/services">Services</a></li>
			<?php } ?>
			<li class="<?php if ($route_page == "orders") echo "uk-active"; ?>">
				<a href="<?php echo $root; ?>/orders">
					<i class="uk-icon-cart-plus"></i> Orders
					<span title="New Orders" class="x-badge-count orders <?php if (!$session_unread_orders_count) echo "x-hidden"; ?>"><?php echo $session_unread_orders_count; ?></span>
				</a>
			</li>
			<li class="<?php if ($route_page == "messages") echo "uk-active"; ?>">
				<a href="<?php echo $root; ?>/messages">
					<i class="uk-icon-envelope"></i> Messages
					<span title="New Messages" class="x-badge-count messages <?php if (!$session_messages_count) echo "x-hidden"; ?>"><?php echo $session_messages_count; ?></span>
				</a>
			</li>
			<li class="<?php if ($route_page == "search") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/search"><i class="uk-icon-search"></i> Search</a></li>
			<li class="<?php if ($route_page == "dashboard" && isset($_GET["cart"])) echo "uk-active"; ?>">
				<a href="<?php echo $root; ?>/dashboard?cart">
					<i class="uk-icon-shopping-cart"></i> My Cart
					<span title="Cart Items" class="x-badge-count cart <?php if (!$session_cart_count) echo "x-hidden"; ?>"><?php echo $session_cart_count; ?></span>
				</a>
			</li>
			<li class="uk-nav-divider"></li>
			<li class="<?php if ($route_page == "profile") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/profile"><?php echo $session_name; ?></a></li>
			<li class="<?php if ($route_page == "edit-profile") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/edit-profile">Edit Profile</a></li>
			<li class="<?php if ($route_page == "settings") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/settings">Settings</a></li>
			<li class="uk-nav-divider"></li>
			<li><a href="<?php echo $root; ?>/logout">Logout</a></li>
			<?php } else { ?>
			<li class="<?php if ($route_page == "home") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/">Home</a></li>
			<li class="<?php if ($route_page == "about") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/about">About</a></li>
			<li class="<?php if ($route_page == "support") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/support">Support</a></li>
			<li class="<?php if ($route_page == "search") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/search"><i class="uk-icon-search"></i> Search</a></li>
			<li class="uk-nav-divider"></li>
			<li class="<?php if ($route_page == "signup") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/signup<?php echo $rdr; ?>">Sign Up</a></li>
			<li class="<?php if ($route_page == "login") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/login<?php echo $rdr; ?>">Login</a></li>
			<?php } ?>
		</ul>
	</div>
</div>
<div class="x-navbar <?php if ($route_page == "home") echo "x-navbar-transparent "; ?>x-navbar-contrast" data-uk-sticky="{'showup':true,'animation':'uk-animation-slide-top','top':'.uk-sticky-placeholder + *','clsinactive':'<?php if ($route_page == "home") echo "x-navbar-transparent "; ?>x-navbar-contrast'}">
	<nav class="uk-navbar uk-contrast">
		<div class="uk-container uk-container-center">
			<a href="<?php echo $root; ?>/" class="uk-navbar-brand uk-hidden-small">
				<img src="<?php echo $root; ?>/assets/img/logow.png" alt="Tupange Events">
			</a>
			<div class="uk-navbar-flip uk-hidden-small">
				<ul class="uk-navbar-nav">
					<?php if (sessionActive()){ ?>
					<li><a href="<?php echo $root."/switch-profile"; ?>">Switch To <?php echo $session_userlevel >= 2 ? "Client" : "Vendor" ?> Profile</a></li>
					<li class="<?php if ($route_page == "dashboard") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/dashboard">Events</a></li>
					<?php if ($session_userlevel >= 2){ ?>
					<li class="<?php if ($route_page == "services") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/services">Services</a></li>
					<?php } ?>
					<li class="<?php if ($route_page == "orders") echo "uk-active"; ?>">
						<a href="<?php echo $root; ?>/orders">
							<span>Orders</span>
							<span title="New Orders" class="x-badge-count orders <?php if (!$session_unread_orders_count) echo "x-hidden"; ?>"><?php echo $session_unread_orders_count; ?></span>
						</a>
					</li>
					<li class="<?php if ($route_page == "search") echo "uk-active"; ?>">
						<a href="<?php echo $search_url; ?>">
							Search
							<!--
							<i class="uk-icon-search"></i>
							<span class="uk-hidden-medium x-fw-400">&nbsp;Search</span>
							-->
						</a>
					</li>
					<li class="<?php if ($route_page == "messages") echo "uk-active"; ?>">
						<a href="<?php echo $root; ?>/messages">
							<i class="uk-icon-envelope uk-visible-medium"></i>
							<span class="uk-hidden-medium x-fw-400">&nbsp;Messages</span>
							<span title="New Messages" class="x-badge-count messages <?php if (!$session_messages_count) echo "x-hidden"; ?>"><?php echo $session_messages_count; ?></span>
						</a>
					</li>
					<li class="<?php if ($route_page == "dashboard" && isset($_GET["cart"])) echo "uk-active"; ?>">
						<a href="<?php echo $root; ?>/dashboard?cart">
							<i class="uk-icon-shopping-cart"></i>
							<span title="Cart Items" class="x-badge-count cart <?php if (!$session_cart_count) echo "x-hidden"; ?>"><?php echo $session_cart_count; ?></span>
						</a>
					</li>
					<li class="x-notifications-wrapper">
						<?php include __DIR__ . '/part-menu-notifications.php'; ?>
					</li>
					<li>
						<div class="uk-navbar-content">
							<div class="x-navbar-avatar" data-uk-dropdown="{mode:'click'}">
								<a href="javascript:">
									<div class="x-avatar">
										<img src="<?php echo $session_avatar; ?>" data-onerror="<?php echo $default_avatar; ?>" onerror="onImageError(this);" />
									</div>
								</a>
								<div class="uk-dropdown uk-dropdown-navbar uk-dropdown-small x-profile-dropdown">
									<ul class="uk-nav uk-nav-navbar">
										<li class="<?php if ($route_page == "profile") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/profile"><?php echo $session_name; ?></a></li>
										<li class="<?php if ($route_page == "edit-profile") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/edit-profile">Edit Profile</a></li>
										<!-- <li class="switch-profile" data-id="<?php echo $_SESSION['uid'] ?>" data-user="<?php echo $session_userlevel ?>"><a href="javascript:">Switch To <?php echo ($session_userlevel) >= 2 ? "Client" : "Vendor" ?> Profile</a></li> -->
										<!-- <li class="switch-profile" data-id="<?php echo $_SESSION['uid'] ?>"><a href="#" class="switch-profile" onclick="changeProfile('<?php echo $_SESSION['uid'] ?>', '<?php echo $session_userlevel ?>')">Switch To <?php echo ($session_userlevel) >= 2 ? "Client" : "Vendor" ?> Profile</a></li> -->
										<li class="uk-nav-divider"></li>
										<li><a href="<?php echo $root; ?>/logout">Logout</a></li>
									</ul>
								</div>
							</div>
						</div>
					</li>
					<?php } else { ?>
					<li class="<?php if ($route_page == "home") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/">Home</a></li>
					<li class="<?php if ($route_page == "about") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/about">About</a></li>
					<li class="<?php if ($route_page == "support") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/support">Support</a></li>
					<li class="<?php if ($route_page == "search") echo "uk-active"; ?>"><a href="<?php echo $root; ?>/search"><i class="uk-icon-search"></i> Search</a></li>
					<li>
						<div class="uk-navbar-content">
							<a href="<?php echo $root; ?>/signup<?php echo $rdr; ?>" class="uk-button uk-button-success">Sign up</a>
							<a href="<?php echo $root; ?>/login<?php echo $rdr; ?>" class="uk-button uk-button-primary">Login</a>
						</div>
					</li>
					<?php } ?>
				</ul>
			</div>
			<div class="uk-visible-small">
				<a href="#offcanvas" data-uk-offcanvas="{mode: 'push'}" class="uk-navbar-toggle"></a>
			</div>
			<div class="uk-navbar-brand uk-navbar-center uk-visible-small">
				<img src="<?php echo $root; ?>/assets/img/logow.png" alt="Tupange Events">
			</div>
			<div class="uk-visible-small x-notifications-wrapper-small">
				<?php include __DIR__ . '/part-menu-notifications.php'; ?>
			</div>
		</div>
	</nav>
</div>