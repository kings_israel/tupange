<footer class="x-footer uk-block uk-block-default" data-uk-scrollspy="{cls:'uk-animation-fade', delay:10, repeat:false}">
	<div class="uk-container uk-container-center">
		<div data-uk-grid-margin class="uk-grid">
			<div class="uk-width-medium-5-10">
				<h3 class="uk-text-center-small">Tupange Events</h3>
				<p class="uk-text-center-small">
					We want to help you with your event planning or services hunt that is why we provide you this platform.
					We believe you should have peace of mind when hiring service providers for your event so we give you a cash back guarantee on disputed cases.
					Create an account and start using our tools. Contact us if you need assistance.
					<!-- <small><div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div></small> -->
				</p>
			</div>
			<div class="uk-width-medium-2-10">
				<h3 class="uk-text-center-small">Pages</h3>
				<ul class="uk-list uk-text-center-small">
					<li class="uk-margin-small-bottom"><a href="./">Home</a></li>
					<li class="uk-margin-small-bottom"><a href="./about">About</a></li>
					<li class="uk-margin-small-bottom"><a href="./blog">Blog</a></li>
					<li class="uk-margin-small-bottom"><a href="./support">Help &amp; Support</a></li>
				</ul>
			</div>
			<div class="uk-width-medium-3-10">
				<h3 class="uk-text-center-small">Social</h3>
				<ul class="uk-list uk-text-center-small">
					<li>Tupange Offices, Nairobi, Kenya</li>
					<li>
						<ul class="uk-flex-inline uk-padding-remove x-tel">
							<li class="uk-margin-right"><a href="tel:">+254 700 000 000</a></li>
							<li class="uk-margin-right"><a href="tel:">+254 700 000 000</a></li>
						</ul>
						<div class="uk-grid uk-grid-match">
							<div class="uk-width-1-1"></div>
						</div>
					</li>
					<li class="x-mail"><a href="mailto:info@transcademy.com">info@tupange.com</a></li>
					<li>
						<ul class="uk-flex-inline uk-padding-remove x-social">
							<li><a target="_blank" href="http://www.twitter.com/tupangeofficial"><i class="uk-icon-twitter"></i></a></li>
							<li><a target="_blank" href="http://www.facebook.com/tupangeofficial"><i class="uk-icon-facebook"></i></a></li>
							<li><a target="_blank" href="http://www.instagram.com/tupangeofficial"><i class="uk-icon-instagram"></i></a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div class="uk-grid-divider"></div>
		<div data-uk-grid-margin class="uk-grid">
			<div class="uk-width-1-1 uk-text-center-small uk-width-medium-1-2 x-copyright">
				Copyright &copy; 2019 Tupange Events LTD.
				All rights reserved.
			</div>
			<div class="uk-width-1-1 uk-text-center-small uk-width-medium-1-2">
				<ul class="uk-list">
					<li class="uk-text-right uk-text-center-small">
						<a class="uk-margin-right" href="#modal-privacy" data-uk-modal>Privacy Policy</a>
						<a class="uk-margin-right" href="#modal-terms" data-uk-modal>Terms of Use</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>
