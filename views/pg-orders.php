<?php include __DIR__ . "/service/pg-orders.php"; ?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=orders">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-3 uk-width-large-1-4">
					<div class="x-box">
						<h2 class="x-box-title"> Menu <a id="menu-toggle" href="javascript:" onclick="toggleMenu()" class="uk-navbar-toggle uk-visible-small"></a></h2>
						<ul id="menu-toggle-content" class="uk-nav uk-nav-navbar uk-hidden-small">
						    <?php if ($session_userlevel >= 2){ ?>
							<li class="<?php if (isset($_GET["client"])) echo "uk-active"; ?>">
								<a href="<?php echo $root; ?>/orders/?client">
									<i class="uk-icon-shopping-cart"></i> Client Orders
									<?php if (isset($order_stats)) echo '<span class="x-badge-count orders-client">' . $order_stats["client"] . '</span>'; ?>
								</a>
							</li>
							<li class="<?php if (isset($_GET["client-requests"])) echo "uk-active"; ?>">
								<a href="<?php echo $root; ?>/orders/?client-requests">
									<i class="uk-icon-star"></i> Client Requests
									<?php if (isset($order_stats)) echo '<span class="x-badge-count orders-client-requests">' . $order_stats["client-requests"] . '</span>'; ?>
								</a>
							</li>
							<?php } ?>
							<li class="<?php if (isset($_GET["open"])) echo "uk-active"; ?>">
								<a href="<?php echo $root; ?>/orders/?open">
									<i class="uk-icon-shopping-cart"></i> My Orders
									<?php if (isset($order_stats)) echo '<span class="x-badge-count orders-open">' . $order_stats["open"] . '</span>'; ?>
								</a>
							</li>
						   <li class="<?php if (isset($_GET["requests"])) echo "uk-active"; ?>">
							   <a href="<?php echo $root; ?>/orders/?requests">
								   <i class="uk-icon-star"></i> My Requests
								   <?php if (isset($order_stats)) echo '<span class="x-badge-count orders-requests">' . $order_stats["requests"] . '</span>'; ?>
							   </a>
						   </li>
						   <li class="<?php if (isset($_GET["history"])) echo "uk-active"; ?>">
							   <a href="<?php echo $root; ?>/orders/?history">
								   <i class="uk-icon-calendar"></i> History
								   <?php if (isset($order_stats)) echo '<span class="x-badge-count orders-history">' . $order_stats["history"] . '</span>'; ?>
							   </a>
						   </li>
						</ul>
						<script type="text/javascript">
							function toggleMenu(){
								let menu_content = document.getElementById("menu-toggle-content");
								if (menu_content.className.indexOf("x-display-block") > -1) menu_content.classList.remove("x-display-block");
								else menu_content.classList.add("x-display-block");
							}
						</script>
					</div>
					<div class="x-box uk-margin-top x-pad-20 uk-hidden-small">
						<h2>New Event?</h2>
						<div class="uk-contrast">
							<a class="uk-button uk-button-success uk-width-1-1 uk-margin-small-top" href="<?php echo $root; ?>/dashboard?edit-event">Create Event</a>
						</div>
					</div>
					<div class="x-box uk-margin-top x-pad-20 uk-hidden-small">
						<h2>Need Service?</h2>
						<div class="uk-contrast">
							<a class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-top" href="<?php echo $root; ?>/search?origin=request">Request Service</a>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-2-3 uk-width-large-3-4">
					<?php if (isset($include)) include_once $include; ?>
					<script type="text/javascript">
						let ORDERS_VIEW = "<?php echo $orders_view; ?>";
						let ORDERS_DATA = <?php echo json_encode($orders); ?>;
						let ORDER_DATA = <?php echo json_encode($order); ?>;
					</script>
				</div>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=orders"></script>
</body>
</html>
