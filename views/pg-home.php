<?php
include __DIR__ . "/service/pg-home.php";
include __DIR__ . "/service/service-template.php";

//helper function
function get_category_query($value){
	global $root;
	static $categories_data;
	if (!is_array($categories_data)){
		$categories_data = [];
		$categories_file = __DIR__ . "/../data/categories.json";
		$categories_array = @json_decode(@file_get_contents($categories_file), true);
		if ($categories_array && is_array($categories_array)) foreach ($categories_array as $item) $categories_data[$item["value"]] = $item;
	}
	$query = "$root/search";
	if (array_key_exists($value, $categories_data)){
		/*
		$pairs = [];
		foreach ($categories_data[$value] as $key => $value) $pairs[] = "categories[][$key]=" . urlencode($value);
		$query .= '?query=&' . implode("&", $pairs);
		*/
		$c = base64_encode('query=&categories=' . urlencode(json_encode([$categories_data[$value]])));
		$query .= '?c=' . $c;
	}
	return $query;
}
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=home">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre=home&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-hero">
		<div class="uk-container uk-container-center">
			<h1 class="x-hero-text">Plan your event with us!</h1>
			<div class="x-hero-forms">
				<div class="uk-grid">
					<div class="uk-width-medium-1-2 uk-margin-top">
						<form id="form-service" class="uk-form uk-form-stacked x-hero-form" action="./search" method="post">
							<div class="x-hero-form-title">
								<h2 class="x-h20">Looking for services?</h2>
							</div>
							<div class="x-hero-form-content">
								<input type="hidden" name="query">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-medium-1-2">
										<div class="uk-form-row">
											<label class="uk-form-label x-col-dark-green">I Need</label>
											<div class="uk-form-controls">
												<uk-tags class="uk-width-1-1" name="categories" fetch="<?php echo $root; ?>/autocomplete.php?category={{query}}" item-template="<i class='uk-icon-{{icon}}'></i> {{name}}" template="{{name}}" value="" placeholder="Search Category"></uk-tags>
											</div>
										</div>
									</div>
									<div class="uk-width-medium-1-2">
										<div class="uk-form-row">
											<label class="uk-form-label x-col-dark-green">On</label>
											<div class="uk-form-controls">
												<div class="uk-form-icon uk-form-icon-flip uk-width-1-1">
													<i class="uk-icon-calendar"></i>
													<input type="text" name="date" class="uk-width-1-1" placeholder="DD/MM/YYYY" data-uk-datepicker="{format:'DD/MM/YYYY', pos:'bottom'}">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="uk-margin-top uk-width-1-1 uk-text-center uk-contrast">
									<button type="submit" class="uk-button uk-button-success uk-button-large x-min-200">Continue</button>
								</div>
							</div>
						</form>
					</div>
					<div class="uk-width-medium-1-2 uk-margin-top">
						<form id="form-event" class="uk-form uk-form-stacked x-hero-form" action="./search" method="post">
							<div class="x-hero-form-title">
								<h2 class="x-h20">Need help planning?</h2>
							</div>
							<div class="x-hero-form-content">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-medium-1-2">
										<div class="uk-form-row">
											<label class="uk-form-label x-col-dark-green">Am Planning</label>
											<div class="uk-form-controls">
												<uk-autocomplete class="uk-width-1-1" name="event_type" fetch="<?php echo $root; ?>/autocomplete.php?type={{query}}" item-template="{{text}}" template="{{text}}" value="" placeholder="An Event"></uk-autocomplete>
											</div>
										</div>
									</div>
									<div class="uk-width-medium-1-2">
										<div class="uk-form-row">
											<label class="uk-form-label x-col-dark-green">Near</label>
											<div class="uk-form-controls x-drop-wrapper">
												<uk-locations class="uk-width-1-1" name="location" fetch="<?php echo $root; ?>/autocomplete.php?location={{query}}" item-template="<i class='uk-icon-map-marker'></i> {{name}}" template="{{name}}" value="" placeholder="Search City/Area" icon="map-marker"></uk-locations>
											</div>
										</div>
									</div>
								</div>
								<div class="uk-margin-top uk-width-1-1 uk-text-center uk-contrast">
									<button type="submit" class="uk-button uk-button-success uk-button-large x-min-200">Continue</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="x-section-ribbon">
		<div class="uk-container uk-container-center">
			<div class="uk-grid uk-grid-small" data-uk-grid-margin>
				<div class="uk-width-medium-1-3">
					<div class="x-ribbon-item">
						<div class="x-ribbon-icon">
							<img src="<?php echo $root; ?>/assets/img/icons/click-white.png" />
						</div>
						<div class="x-ribbon-text">
							<span>100,000+ vendors</span><br>
							Explore our listing
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-3">
					<div class="x-ribbon-item">
						<div class="x-ribbon-icon">
							<img src="<?php echo $root; ?>/assets/img/icons/check-white.png" />
						</div>
						<div class="x-ribbon-text">
							<span>Expert services</span><br>
							We guarantee quality
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-3">
					<div class="x-ribbon-item">
						<div class="x-ribbon-icon">
							<img src="<?php echo $root; ?>/assets/img/icons/clock-white.png" />
						</div>
						<div class="x-ribbon-text">
							<span>Lifetime access</span><br>
							Get started today
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="x-section-popular" data-uk-scrollspy="{cls:'uk-animation-fade', delay:10, repeat:false}">
		<div class="uk-container uk-container-center">
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-1-2 uk-width-medium-1-4">
					<a href="<?php echo get_category_query(4); ?>">
						<figure class="uk-overlay uk-overlay-hover uk-width-1-1">
							<img class="uk-overlay-scale" src="<?php echo $root; ?>/assets/img/home/cat-wedding.jpg" alt="Weddings">
							<figcaption class="uk-overlay-panel uk-ignore uk-overlay-background uk-flex uk-flex-center uk-flex-middle uk-text-center">Weddings</figcaption>
						</figure>
					</a>
				</div>
				<div class="uk-width-1-2 uk-width-medium-1-4">
					<a href="<?php echo get_category_query(13); ?>">
						<figure class="uk-overlay uk-overlay-hover uk-width-1-1">
							<img class="uk-overlay-scale" src="<?php echo $root; ?>/assets/img/home/cat-wedcake.jpg" alt="Wedding Cakes">
							<figcaption class="uk-overlay-panel uk-ignore uk-overlay-background uk-flex uk-flex-center uk-flex-middle uk-text-center">Wedding Cakes</figcaption>
						</figure>
					</a>
				</div>
				<div class="uk-width-1-2 uk-width-medium-1-4">
					<a href="<?php echo get_category_query(9); ?>">
						<figure class="uk-overlay uk-overlay-hover uk-width-1-1">
							<img class="uk-overlay-scale" src="<?php echo $root; ?>/assets/img/home/cat-venue.jpg" alt="Venues">
							<figcaption class="uk-overlay-panel uk-ignore uk-overlay-background uk-flex uk-flex-center uk-flex-middle uk-text-center">Venues</figcaption>
						</figure>
					</a>
				</div>
				<div class="uk-width-1-2 uk-width-medium-1-4">
					<a href="<?php echo get_category_query(6); ?>">
						<figure class="uk-overlay uk-overlay-hover uk-width-1-1">
							<img class="uk-overlay-scale" src="<?php echo $root; ?>/assets/img/home/cat-deco.jpg" alt="Decorations">
							<figcaption class="uk-overlay-panel uk-ignore uk-overlay-background uk-flex uk-flex-center uk-flex-middle uk-text-center">Decorations</figcaption>
						</figure>
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="x-section x-section-featured" data-uk-scrollspy="{cls:'uk-animation-fade', delay:10, repeat:false}">
		<div class="uk-container uk-container-center">
			<h1 class="uk-text-center">Featured Services</h1>
			<div data-uk-slideset="{small:2,medium:3,large:4,animation:'slide-horizontal',duration:200}">
    			<div class="uk-slidenav-position">
					<ul class="uk-slideset uk-grid uk-grid-small uk-grid-width-medium-1-3 uk-grid-width-large-1-4">
						<?php foreach ($featured_services as $service){ ?>
							<li><?php echo service_template($service); ?></li>
						<?php } ?>
					</ul>
					<a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
        			<a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
				</div>
				<ul class="uk-slideset-nav uk-dotnav uk-flex-center uk-margin-top"></ul>
			</div>
		</div>
	</section>
	<section class="x-section x-section-event">
		<div class="uk-container uk-container-center">
			<div class="uk-text-center">
				<h1>What To Expect</h1>
			</div>
			<div class="uk-grid" data-uk-grid-margin data-uk-grid-match>
				<div class="uk-width-medium-1-3">
					<div class="x-box x-exp">
						<img draggable="false" src="<?php echo $root; ?>/assets/img/home/event.jpg" />
						<div class="x-pad-20">
							<h2>Create &amp; Manage Event</h2>
							<p>Proin condimentum velit tortor, vel aliquet orci accumsan id. Fusce nulla massa, egestas ut lacus eget, porttitor lacinia diam.</p>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-3">
					<div class="x-box x-exp">
						<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/scan.jpg" />
						<div class="x-pad-20">
							<h2>Sell Event Tickets</h2>
							<p>Proin condimentum velit tortor, vel aliquet orci accumsan id. Fusce nulla massa, egestas ut lacus eget, porttitor lacinia diam.</p>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-3">
					<div class="x-box x-exp">
						<img draggable="false" src="<?php echo $root; ?>/assets/img/home/dj.jpg" />
						<div class="x-pad-20">
							<h2>Connect With Vendors</h2>
							<p>Proin condimentum velit tortor, vel aliquet orci accumsan id. Fusce nulla massa, egestas ut lacus eget, porttitor lacinia diam.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--
	<section class="x-section x-section-planners" data-uk-scrollspy="{cls:'uk-animation-fade', delay:10, repeat:false}">
		<div class="uk-container uk-container-center">
			<div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
				<div class="uk-width-medium-1-2">
					<a href="javascript:"><img class="x-img-cover uk-height-1-1 uk-width-1-1" src="<?php echo $root; ?>/assets/img/home/wed.jpg" /></a>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="x-pad-20">
						<h1 class="uk-margin">For Planners</h1>
						<p>
							We know you are busy and you want things to be easy.
							This platform is here for you as you plan that event.
							Be it a wedding party, an official corporate event, a birthday party, a funeral service or that house party, this is the place to be.
						</p>
						<p>
							Get personaliced services and peace of mind with cash back guarantee on selected services (T&Cs apply).
							Waste no time, create an account and start browsing our listings. Make use of our planning tools freely available to make your work even easier.
						</p>
						<div class="uk-text-left uk-contrast">
							<a href="./signup" class="uk-button uk-button-primary x-min-150">Get Started</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="x-section x-section-vendors" data-uk-scrollspy="{cls:'uk-animation-fade', delay:10, repeat:false}">
		<div class="uk-container uk-container-center">
			<div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
				<div class="uk-width-medium-1-2 uk-push-1-2">
					<a href="javascript:"><img class="x-img-cover uk-height-1-1 uk-width-1-1" src="<?php echo $root; ?>/assets/img/home/table.jpg" /></a>
				</div>
				<div class="uk-width-medium-1-2 uk-pull-1-2">
					<div class="x-pad-20">
						<h1 class="uk-margin">For Vendors</h1>
						<p>
							Get your business recognized by connecting with our clients.
							We carefully select our vendors so we guarantee quality service delivery governed by our T&Cs.
							To have your services listed, create a vendor account and start advertising your business.
						</p>
						<p>
							Let clients come to you. If you provide quality services and you are consistent,
							you will have loyal clients who can bookmark your profile or even share with their friends as recommendations.
							Our planning tools also allow clients to narrow down to your specialization so you are guaranteed to close a sale.
						</p>
						<div class="uk-text-right uk-contrast">
							<a href="./signup?vendor" class="uk-button uk-button-primary x-min-150">Vendor Account</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	-->
	<section class="x-section x-section-testimonials">
		<div class="uk-container uk-container-center">
			<h1 class="uk-margin-large uk-text-center">Testimonials</h1>
			<div data-uk-slideset="{medium:2,large:3,animation:'slide-horizontal',duration:200}">
    			<div class="uk-slidenav-position">
					<ul class="uk-slideset uk-grid uk-grid-small uk-grid-width-medium-1-2 uk-grid-width-large-1-3">
					<?php foreach ($testimonials as $testimony){ ?>
						<li>
							<div class="x-testimonial uk-text-center">
								<div class="x-avatar" style="width:100px;height:100px;background:#eee;">
									<img src="<?php echo $testimony["avatar"]; ?>" data-onerror="<?php echo $default_avatar; ?>" onerror="onImageError(this);" />
								</div>
								<div class="x-pad-10">
									<h2><?php echo $testimony["names"]; ?></h2>
									<p><?php echo $testimony["testimony"]; ?></p>
									<small><?php echo $testimony["timestamp"]; ?></small>
								</div>
							</div>
						</li>
					<?php } ?>
					</ul>
					<a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
        			<a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
				</div>
				<ul class="uk-slideset-nav uk-dotnav uk-flex-center uk-margin-top"></ul>
			</div>
		</div>
	</section>
	<section class="x-section x-section-newsletter">
		<div class="uk-container uk-container-center">
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-2 uk-width-large-2-3">
					<h1 class="uk-margin">Our Newsletter</h1>
					<p>
						Get notified about our publications.
						Get offers on services exclusive to our members and you if you subscribe.
						We respect your privacy and we won't spam you.
					</p>
				</div>
				<div class="uk-width-medium-1-2 uk-width-large-1-3">
					<form action="javascript:" class="uk-form uk-form-stacked uk-width-1-1">
						<div class="uk-form-row">
							<label class="uk-form-label x-col-dark-green"><strong>Subscribe To Our Newsletter<strong></label>
							<div class="uk-form-controls">
								<div class="uk-form-icon uk-form-icon-flip uk-width-1-1">
									<i class="uk-icon-envelope-o"></i>
									<input type="text" class="uk-width-1-1" placeholder="Your Email Address">
								</div>
							</div>
						</div>
						<div class="uk-margin-top uk-text-left uk-contrast">
							<button class="uk-button uk-button-primary">Subscribe</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=home"></script>
</body>
</html>
