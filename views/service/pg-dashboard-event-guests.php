<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-db-insert.php";
require_once __DIR__ . "/helper-db-update.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Plugin\Files;
use Naicode\Server\Database;
use Naicode\Server as s;

//some constants
const GUEST_ID_LENGTH = 10;
const GUEST_BARCODE_LENGTH = 4;
const GUEST_TYPES = [0 => "General Admission", 1 => "VIP", 2 => "VVIP"];
const GUEST_STATUS_UNINVITED = 0;
const GUEST_STATUS_INVITED = 1;
const GUEST_STATUS_CONFIRMED = 2;
const GUEST_STATUS_MAYBE = 3;
const GUEST_STATUS_DECLINED = 4;
const GUEST_STATUSES = [
	GUEST_STATUS_UNINVITED => "Uninvited",
	GUEST_STATUS_INVITED => "Invited",
	GUEST_STATUS_CONFIRMED => "Confirmed",
	GUEST_STATUS_MAYBE => "Maybe",
	GUEST_STATUS_DECLINED => "Declined",
];
const VALIDATE_PHONE_SUPPORT = ["254$2" => '/^(\+?254|^0)(7[0-9]{8})$/'];
const MESSAGING_STATUS_NOT_SENT = '';

//fetch user event
if (!$event_data = fetch_event($event_id, $session_uid)){
	//handle event not found
	if (isset($request_params["guest_id"]) && isset($request_params["guest_first_name"])) s::error(null, isset($fetch_event_error) ? $fetch_event_error : "Event not found");
	header("location: $root/dashboard?events");
	exit();
}

//check guest user limit
if ($event_data['guest'] && !in_array($event_data['role'], [0, 1])){
	header("location: $root/dashboard?events");
	exit();
}

//event data
$event = event_from_data($event_data);

//some event data variables
$event_settings = fn1::toArray($event["settings"]);
$guest_groups = fn1::toArray($event["guest_groups"]);
$event_invite_modes = [];
if (isset($event_settings["invite_email"]) && $event_settings["invite_email"] == 1) $event_invite_modes[] = "email";
if (isset($event_settings["invite_sms"]) && $event_settings["invite_sms"] == 1) $event_invite_modes[] = "sms";
if (isset($event_settings["invite_whatsapp"]) && $event_settings["invite_whatsapp"] == 1) $event_invite_modes[] = "whatsapp";
if (isset($event_settings["invite_facebook"]) && $event_settings["invite_facebook"] == 1) $event_invite_modes[] = "facebook";
if (isset($event_settings["invite_twitter"]) && $event_settings["invite_twitter"] == 1) $event_invite_modes[] = "twitter";

//REST POST: Guest form submit
if (isset($request_params["guest_id"]) && isset($request_params["guest_first_name"])){
	//save submitted guest details
	$db = null;
	$conn = null;
	$data = null;
	try {
		//get data
		$guest_id = isset($request_params["guest_id"]) ? fn1::toStrx($request_params["guest_id"], true) : "";
		$guest_status = isset($request_params["guest_status"]) ? (int) $request_params["guest_status"] : 0;
		$guest_first_name = isset($request_params["guest_first_name"]) ? fn1::toStrx($request_params["guest_first_name"], true) : "";
		$guest_last_name = isset($request_params["guest_last_name"]) ? fn1::toStrx($request_params["guest_last_name"], true) : "";
		$guest_type = isset($request_params["guest_type"]) ? (int) $request_params["guest_type"] : 0;
		$guest_group = isset($request_params["guest_group"]) ? fn1::toStrx($request_params["guest_group"], true) : "";
		$guest_phone = isset($request_params["guest_phone"]) ? fn1::toStrx($request_params["guest_phone"], true) : "";
		$guest_email = isset($request_params["guest_email"]) ? strtolower(fn1::toStrx($request_params["guest_email"], true)) : "";
		$guest_company = isset($request_params["guest_company"]) ? fn1::toStrx($request_params["guest_company"], true) : "";
		$guest_address = isset($request_params["guest_address"]) ? fn1::toStrx($request_params["guest_address"], true) : "";
		$guest_diet = isset($request_params["guest_diet"]) ? fn1::toStrx($request_params["guest_diet"], true) : "";
		$guest_table_number = isset($request_params["guest_table_number"]) ? fn1::toStrx($request_params["guest_table_number"], true) : "";
		$guest_extend_inv = isset($request_params["guest_extend_inv"]) && $request_params["guest_extend_inv"] ? 1 : 0;
		$guest_expected = isset($request_params["guest_expected"]) ? (int) $request_params["guest_expected"] : 1;
		$guest_inv_phone = isset($request_params["guest_inv_phone"]) ? fn1::toStrx($request_params["guest_inv_phone"], true) : "";
		$guest_inv_email = isset($request_params["guest_inv_email"]) ? strtolower(fn1::toStrx($request_params["guest_inv_email"], true)) : "";
		$guest_inv_message = isset($request_params["guest_inv_message"]) ? fn1::toStrx($request_params["guest_inv_message"], true) : "";
		$guest_inv_question = isset($request_params["guest_inv_question"]) ? fn1::toStrx($request_params["guest_inv_question"], true) : "";
		$guest_inv_answer = isset($request_params["guest_inv_answer"]) ? fn1::toStrx($request_params["guest_inv_answer"], true) : "";
		$guest_inv_edits = isset($request_params["guest_inv_edits"]) && $request_params["guest_inv_edits"] ? 1 : 0;

		//validate form data
		if (!array_key_exists($guest_status, GUEST_STATUSES)) throw new Exception("Invalid guest status value.");
		$tmp_first_name = validate_name($guest_first_name);
		if ($tmp_first_name === null) throw new Exception("Kindly provide guest first name.");
		if ($tmp_first_name === false) throw new Exception("Invalid guest first name \"$guest_first_name\"");
		$guest_first_name = $tmp_first_name;
		$tmp_last_name = validate_name($guest_last_name);
		if ($tmp_last_name === false) throw new Exception("Invalid guest last name \"$guest_last_name\"");
		$guest_last_name = $tmp_last_name ? $tmp_last_name : "";
		if (!array_key_exists($guest_type, GUEST_TYPES)) throw new Exception("Invalid guest type value.");
		$tmp_phone = validate_phone($guest_phone);
		if ($tmp_phone === false) throw new Exception($validate_phone_error);
		$guest_phone = $tmp_phone ? $tmp_phone : "";
		if ($guest_email != "" && !fn1::isEmail($guest_email)) throw new Exception("Invalid email address \"$guest_email\"");
		if ($guest_expected < 0) throw new Exception("Invalid expected guests value");
		$tmp_inv_phone = validate_phone($guest_inv_phone, true);
		if ($tmp_inv_phone === false) throw new Exception($validate_phone_error);
		$guest_inv_phone = $tmp_inv_phone ? $tmp_inv_phone : "";
		if ($guest_inv_email != "" && !fn1::isEmail($guest_inv_email)) throw new Exception("Invalid invitation email address contact \"$guest_inv_email\"");
		if ($guest_inv_answer != "" &&  !$guest_inv_question) throw new Exception("Kindly provide the custom question for the answer \"$guest_inv_answer\"");

		//timestamp
		$timestamp = fn1::now();

		//prepare save data
		if (!$guest_extend_inv) $guest_expected = 1;
		if (!array_key_exists($guest_group, $guest_groups)) $guest_group = "";
		$barcode = null;
		$existing_guest = null;
		$time_invited = null;
		$time_confirmed = null;

		//check existing
		if ($guest_id != ""){
			if (!$existing_guest = fetch_event_guest($guest_id, $event_id, $session_uid)) throw new Exception($fetch_event_guest_error);
			$existing_guest = xstrip($existing_guest);
			if (!fn1::hasKeys($existing_guest, "id", "time_invited", "time_confirmed")) throw new Exception("Error fetching existing guest details");
			$guest_id = $existing_guest["id"];
			$time_invited = $existing_guest["time_invited"];
			$time_confirmed = $existing_guest["time_confirmed"];
		}

		//set guest status time
		if ($guest_status == GUEST_STATUS_UNINVITED){
			$time_invited = null;
			$time_confirmed = null;
		}
		elseif ($guest_status == GUEST_STATUS_INVITED){
			$time_invited = $timestamp;
			$time_confirmed = null;
		}
		elseif ($guest_status == GUEST_STATUS_CONFIRMED){
			if ($time_invited == "") $time_invited = $timestamp;
			$time_confirmed = $timestamp;
		}

		//guest data
		$data = [
			"first_name" => $guest_first_name,
			"last_name" => $guest_last_name,
			"type" => $guest_type,
			"group_id" => $guest_group,
			"phone" => $guest_phone,
			"email" => $guest_email,
			"company" => $guest_company,
			"address" => $guest_address,
			"diet" => $guest_diet,
			"table_no" => $guest_table_number,
			"extend_inv" => $guest_extend_inv,
			"expected_guests" => $guest_expected,
			"inv_phone" => $guest_inv_phone,
			"inv_email" => $guest_inv_email,
			"inv_message" => $guest_inv_message,
			"inv_question" => $guest_inv_question,
			"inv_answer" => $guest_inv_answer,
			"inv_edits" => $guest_inv_edits,
			"status" => $guest_status,
			"time_invited" => $time_invited,
			"time_confirmed" => $time_confirmed,
			"time_modified" => $timestamp,
		];

		//db connection ~ autocommit disabled
		$db = new Database();
		$conn = $db -> resetValues() -> getConnection(false);

		//duplicate check
		$duplicate_query = "`id` <> ? AND `event_id` = ? AND `uid` = ? AND `first_name` = ? AND `last_name` = ? AND `type` = ? AND `group_id` = ? AND `phone` = ? AND `email` = ?";
		$duplicate_binds = [$guest_id, $event_id, $session_uid, $guest_first_name, $guest_last_name, $guest_type, $guest_group, $guest_phone, $guest_email];
		if (($duplicate_exists = $db -> queryExists(TABLE_EVENT_GUESTS, $duplicate_query, $duplicate_binds)) === false) throw new Exception($db -> getErrorMessage());
		if ($duplicate_exists) throw new Exception("Duplicate guest record found with similar (names, type, group, phone & email). Change these details and try again.");

		//saving guest data
		if ($guest_id == ""){
			//create guest
			$guest_id = $db -> createToken(TABLE_EVENT_GUESTS, "id", GUEST_ID_LENGTH);

			//new barcode
			$barcode = guest_new_barcode($event_id, $session_uid);

			//new guest
			$data["id"] = $guest_id;
			$data["event_id"] = $event_id;
			$data["uid"] = $session_uid;
			$data["time_created"] = $timestamp;
			$data["barcode"] = $barcode;
			$data["status_sms"] = MESSAGING_STATUS_NOT_SENT;
			$data["status_email"] = MESSAGING_STATUS_NOT_SENT;
			$data["status_whatsapp"] = MESSAGING_STATUS_NOT_SENT;
			$data["status_fb"] = MESSAGING_STATUS_NOT_SENT;
			$data["status_twitter"] = MESSAGING_STATUS_NOT_SENT;
			if (!db_insert($db, $conn, TABLE_EVENT_GUESTS, $data)) throw new Exception($db -> err, $db -> errno);
		}
		elseif (!db_update($db, $conn, TABLE_EVENT_GUESTS, $data, "`id` = ? AND `event_id` = ? AND `uid` = ?", [$guest_id, $event_id, $session_uid])) throw new Exception($db -> err, $db -> errno);

		//test
		$query_str = $db -> queryString();

		//commit db changes and close
		if (!$conn -> commit()) throw new Exception($conn -> error, $conn -> errno);
		$db -> close();
		$uploaded_image = null;
		$conn = null;
		$db = null;

		//successful output new guest data
		if ($existing_guest != null) $guest_data = fn1::arrayMerge($existing_guest, $data);
		else $guest_data = $data;
		$guest_data = guest_data($guest_data);

		//output success
		s::success($guest_data, "Guest details saved successfully!");
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($conn !== null && !$conn -> rollback()) $error .= " (Rollback Failed: " . $conn -> error . ")";
		if ($db !== null) $db -> close();
		s::error($data, $error);
	}
}

//REST DELETE POST: Guest delete request
if (isset($request_params["guest_delete"])){
	$db = null;
	try {
		$guest_id = fn1::toStrx($request_params["guest_delete"], true);
		if (fn1::isEmpty($guest_id)) throw new Exception("Invalid delete guest reference");

		//db connection
		$db = new Database();

		//check if guest exists
		if (($guest_exists = $db -> queryExists(TABLE_EVENT_GUESTS, "`id` = ? AND `event_id` = ? AND `uid` = ?", [$guest_id, $event_id, $session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$guest_exists) throw new Exception("Event guest not found");

		//delete existing guest details
		if (!$db -> delete(TABLE_EVENT_GUESTS, "id", "WHERE `id` = ? AND `event_id` = ? AND `uid` = ?", [$guest_id, $event_id, $session_uid])) throw new Exception($db -> getErrorMessage());

		//close connection
		$db -> close();

		//successful delete
		s::success(null, "Guest deleted successfully");
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error($e -> getMessage());
	}
}

//REST BULK ACTION: Guest bulk actions
if (isset($request_params["guest_bulk_action"]) && isset($request_params["list"])){
	$bulk_action = fn1::toStrx($request_params["guest_bulk_action"], true);
	$bulk_list = fn1::toStrx($request_params["list"], true);
	$bulk_list_ids = explode(",", $bulk_list);
	$db = null;
	try {
		//validate
		$bulk_actions = ["invited", "confirmed", "delete"];
		if (!in_array($bulk_action, $bulk_actions)) throw new Exception("Unsupported action \"$bulk_action\"");
		if (!count($bulk_list_ids)) throw new Exception("No items in bulk action list.");

		//db connection
		$db = new Database();

		//guest ids
		$guest_ids = [];
		foreach ($bulk_list_ids as $bulk_list_id) $guest_ids[] = $db -> escapeString($bulk_list_id, true);
		$guest_ids = '"' . implode('","', $guest_ids) . '"';

		//action queries
		$timestamp = fn1::now();
		if ($bulk_action == "invited"){
			//set invited
			$status = GUEST_STATUS_INVITED;
			$query = "UPDATE `%s` SET `time_modified` = '$timestamp', `time_invited`='$timestamp', `time_confirmed` = NULL, `status` = '$status' WHERE `event_id` = '%s' AND `uid` ='%s' AND `id` IN (%s)";
		}
		elseif ($bulk_action == "confirmed"){
			//set confirmed
			$status = GUEST_STATUS_CONFIRMED;
			$query = "UPDATE `%s` SET `time_modified` = '$timestamp', `time_invited`=COALESCE(`time_invited`, '$timestamp'), `time_confirmed` = '$timestamp', `status` = '$status' WHERE `event_id` = '%s' AND `uid` ='%s' AND `id` IN (%s)";
		}
		elseif ($bulk_action == "delete"){
			//delete guests
			$query = "DELETE FROM `%s` WHERE `event_id` = '%s' AND `uid` ='%s' AND `id` IN (%s)";
		}

		//query
		$query = fn1::strbuild($query, $db -> escapeString(TABLE_EVENT_GUESTS, true), $db -> escapeString($event_id, true), $db -> escapeString($session_uid, true), $guest_ids);
		if (!$db -> query($query)) throw new Exception($db -> getErrorMessage());
		$affected_rows = (int) $db -> affected_rows;

		//success
		if (in_array($bulk_action, ["invited", "confirmed"])){
			//fetch updated
			$updated_records = [];
			if ($affected_rows > 0){
				$result = $db -> select(TABLE_EVENT_GUESTS, null, "WHERE `event_id` = ? AND `uid` = ? AND `id` IN ($guest_ids)", [$event_id, $session_uid]);
				if ($result instanceof mysqli_result) while ($row = $result -> fetch_array(MYSQLI_ASSOC)) $updated_records[] = guest_data($row);
			}

			//close connection
			$db -> close();

			//output
			s::success($updated_records, "Update successful! $affected_rows records updated");
		}

		//close connection
		$db -> close();

		//output
		s::success($bulk_list_ids, "Request successful! $affected_rows records affected");
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error($bulk_list_ids, $e -> getMessage());
	}
}

//Fetch guests
$event_guests = [];
$db = null;
try {
	$db = new Database();
	if (!$result = $db -> select(TABLE_EVENT_GUESTS, null, "WHERE `event_id` = ? AND `uid` = ? ORDER BY `time_created` DESC", [$event_id, $session_uid])) throw new Exception($db -> getErrorMessage());
	if ($result -> num_rows){
		while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
			if (!$guest = guest_data($row)) continue;
			$event_guests[] = $guest;
		}
	}
	$db -> close();
}
catch (Exception $e){
	$error = $e -> getMessage();
	if ($db !== null) $db -> close();
	s::error(null, $error);
}

#fn1::printr($event_settings, json_encode($event_settings)); exit(); //test

//helper functions
function guest_data($guest){
	$data = null;
	if (!fn1::isEmpty($guest = fn1::toArray($guest))){
		$guest = xstrip($guest);
		try {
			//guest variables
			$type = (int) fn1::propval($guest, "type");
			$status = (int) fn1::propval($guest, "status");
			$group_id = fn1::toStrx(fn1::propval($guest, "group_id"), true);
			$group = guest_group($group_id);
			$first_name = fn1::toStrx(fn1::propval($guest, "first_name"), true);
			$last_name = fn1::toStrx(fn1::propval($guest, "last_name"), true);
			$names = fn1::strtocamel($first_name . " " . $last_name, true);

			//invite contacts
			$inv_phone = fn1::toStrx(fn1::propval($guest, "inv_phone"), true);
			$inv_email = fn1::toStrx(fn1::propval($guest, "inv_email"), true);
			$inv_contacts = [];
			if ($inv_phone != "") $inv_contacts[] = "Phone: +$inv_phone";
			if ($inv_email != "") $inv_contacts[] = "Email: $inv_email";
			$inv_contacts = implode(", ", $inv_contacts);

			//invite q/a
			$inv_question = fn1::toStrx(fn1::propval($guest, "inv_question"), true);
			$inv_answer = fn1::toStrx(fn1::propval($guest, "inv_answer"), true);
			$inv_qa = [];
			if ($inv_question != "") $inv_qa[] = "Q: $inv_question";
			if ($inv_answer != "") $inv_qa[] = "A: $inv_answer";
			$inv_qa = implode(", ", $inv_qa);
			$extend_inv = (int) fn1::propval($guest, "extend_inv");

			//guest data
			$data = [
				"id" => fn1::toStrx(fn1::propval($guest, "id"), true),
				"event_id" => fn1::toStrx(fn1::propval($guest, "event_id"), true),
				"uid" => fn1::toStrx(fn1::propval($guest, "uid"), true),
				"barcode" => fn1::toStrx(fn1::propval($guest, "barcode"), true),
				"first_name" => fn1::toStrx(fn1::propval($guest, "first_name"), true),
				"last_name" => fn1::toStrx(fn1::propval($guest, "last_name"), true),
				"names" => $names,
				"type" => $type,
				"type_name" => GUEST_TYPES[$type],
				"group_id" => $group_id,
				"group" => $group,
				"group_name" => is_array($group) && array_key_exists("name", $group) ? $group["name"] : "",
				"phone" => fn1::toStrx(fn1::propval($guest, "phone"), true),
				"email" => fn1::toStrx(fn1::propval($guest, "email"), true),
				"company" => fn1::toStrx(fn1::propval($guest, "company"), true),
				"address" => fn1::toStrx(fn1::propval($guest, "address"), true),
				"diet" => fn1::toStrx(fn1::propval($guest, "diet"), true),
				"table_no" => fn1::toStrx(fn1::propval($guest, "table_no"), true),
				"extend_inv" => $extend_inv,
				"extend_inv_text" => $extend_inv ? "Yes" : "No",
				"expected_guests" => (int) fn1::propval($guest, "expected_guests"),

				"inv_phone" => $inv_phone,
				"inv_email" => $inv_email,
				"inv_contacts" => $inv_contacts,

				"inv_message" => fn1::toStrx(fn1::propval($guest, "inv_message"), true),

				"inv_question" => $inv_question,
				"inv_answer" => $inv_answer,
				"inv_qa" => $inv_qa,

				"inv_edits" => (int) fn1::propval($guest, "inv_edits"),
				"status_sms" => fn1::toStrx(fn1::propval($guest, "status_sms"), true),
				"status_email" => fn1::toStrx(fn1::propval($guest, "status_email"), true),
				"status_whatsapp" => fn1::toStrx(fn1::propval($guest, "status_whatsapp"), true),
				"status_fb" => fn1::toStrx(fn1::propval($guest, "status_fb"), true),
				"status_twitter" => fn1::toStrx(fn1::propval($guest, "status_twitter"), true),

				"status" => $status,
				"status_text" => GUEST_STATUSES[$status],

				"time_created" => fn1::toStrx(fn1::propval($guest, "time_created"), true),
				"time_invited" => fn1::toStrx(fn1::propval($guest, "time_invited"), true),
				"time_confirmed" => fn1::toStrx(fn1::propval($guest, "time_confirmed"), true),
				"time_modified" => fn1::toStrx(fn1::propval($guest, "time_modified"), true),
			];
		}
		catch (Exception $e){}
	}
	return $data ? xstrip($data, true, false) : $data;
}
function guest_group($group_id){
	global $guest_groups;
	if (is_array($guest_groups) && array_key_exists($group_id, $guest_groups)){
		$group = $guest_groups[$group_id];
		if (fn1::hasKeys($group, "id", "name", "limit", "description")) return $group;
	}
	return null;
}
function validate_name($name){
	$name = fn1::toStrx($name, true);
	if (!strlen($name)) return null;
	if (preg_match('/[^a-z0-9\']/is', $name)) return false;
	return fn1::strtocamel($name);
}
function validate_phone($phone, $is_supported=false){
	global $validate_phone_error;
	$validate_phone_error = "";
	if (fn1::isEmpty($phone = fn1::toStrn($phone, true))) return null;
	if (!preg_match('/^\+?[0-9\s-\(\)]+$/s', $phone)){
		$validate_phone_error = "Invalid phone number format";
		return false;
	}
	$phone = preg_replace('/[^0-9]/s', "", $phone);
	if (strlen($phone) < 5 || strlen($phone) > 14){
		$validate_phone_error = "Invalid phone number";
		return false;
	}
	if ($is_supported && !fn1::isEmpty(VALIDATE_PHONE_SUPPORT)){
		$supported = false;
		foreach (VALIDATE_PHONE_SUPPORT as $key => $test){
			if (preg_match($test, $phone)){
				$phone = preg_replace($test, $key, $phone);
				$supported = true;
				break;
			}
		}
		if (!$supported){
			$validate_phone_error = "Phone number \"$phone\"is not currently supported";
			return false;
		}
	}
	return $phone;
}
function guest_new_barcode($event_id, $uid){
	global $guest_new_barcode_error;
	$db = null;
	try {
		//new connection
		$db = new Database();

		//count guests
		if (($count_guests = $db -> queryCount(TABLE_EVENT_GUESTS, "WHERE `event_id` = ? AND `uid` = ?", [$event_id, $uid])) === false) throw new Exception($db -> getErrorMessage());

		//set barcode length
		$barcode_length = GUEST_BARCODE_LENGTH;
		if ($count_guests > 40000000) $barcode_length += 2;
		elseif ($count_guests > 1000000) $barcode_length += 1;

		//generate barcode [uppercase + numbers]
		$barcode = fn1::randomString($barcode_length, false);
		while ($db -> queryExists(TABLE_EVENT_GUESTS, "`barcode` = ?", [$barcode])) $barcode = fn1::randomString($barcode_length, false);

		//close connection
		$db -> close();
		return $barcode;
	}
	catch (Exception $e){
		$guest_new_barcode_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}

































/*eof*/
