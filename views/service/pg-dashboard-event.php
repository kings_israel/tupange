<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Plugin\Files;
use Naicode\Server\Database;
use Naicode\Server as s;

//fetch user event
if (!$event_data = fetch_event($event_id, $session_uid)){
	//handle event not found
	header("location: $root/dashboard?events");
	exit();
}

//event data
$event = event_from_data($event_data);
$guest_user = null;
if ($event['guest'] && !($guest_user = get_event_user($event_id, $session_uid))) s::error(null, $get_event_user_error);

//event stats
$db = new Database();
$event_id = $db -> escapeString($event_id);
$table_event_users = $db -> escapeString(TABLE_EVENT_USERS);
$table_checklist = $db -> escapeString(TABLE_CHECKLIST_ITEMS);
$table_tasks = $db -> escapeString(TABLE_TASKS);
$table_orders = $db -> escapeString(TABLE_ORDERS);
$table_register = $db -> escapeString(TABLE_REGISTER);
$table_attendance = $db -> escapeString(TABLE_ATTENDANCE);
$table_budget_transactions = $db -> escapeString(TABLE_BUDGET_TRANSACTIONS);
$table_gifts = $db -> escapeString(TABLE_GIFTS);
$stats_queries = [];
$stats_queries[] = fn1::strbuild("(SELECT COUNT(*) FROM `%s` WHERE `event_id`='%s') as 'checklist_count'", $table_tasks, $event_id);
$stats_queries[] = fn1::strbuild("(SELECT COUNT(*) FROM `%s` WHERE `event_id`='%s' AND `status`='1') as 'checklist_checked'", $table_tasks, $event_id);
$stats_queries[] = fn1::strbuild("(SELECT COUNT(*) FROM `%s` WHERE `event_id`='%s') as 'orders_count'", $table_orders, $event_id);
$stats_queries[] = fn1::strbuild("(SELECT SUM(`amount`) FROM `%s` WHERE `event_id`='%s') as 'ticket_sales'", $table_register, $event_id);
$stats_queries[] = fn1::strbuild("(SELECT COUNT(DISTINCT `barcode`) FROM `%s` WHERE `event_id`='%s' AND `direction`='IN') as 'unique_checkins'", $table_attendance, $event_id);
$stats_queries[] = fn1::strbuild("(SELECT (SUM(IF(`type`='1', `amount`, 0)) - SUM(IF(`type`='0', `amount`, 0))) FROM `%s` WHERE `event_id`='%s') as 'budgets_balance'", $table_budget_transactions, $event_id);
$stats_queries[] = fn1::strbuild("(SELECT COUNT(*) FROM `%s` WHERE `event_id`='%s') as 'gifts_count'", $table_gifts, $event_id);
$stats_queries[] = fn1::strbuild("(SELECT COUNT(*) FROM `%s` WHERE `event_id`='%s') as 'users_count'", $table_event_users, $event_id);
$stats_sql = 'SELECT ' . implode(',', $stats_queries);
$result = $db -> query($stats_sql);
if ($result === false) die ($db -> getErrorMessage());
$event_stats = [];
if ($result instanceof mysqli_result){
	if ($row = $result -> fetch_array(MYSQLI_ASSOC)){
		$event_stats['checklist_count'] = (int) $row['checklist_count'];
		$event_stats['checklist_checked'] = (int) $row['checklist_checked'];
		$event_stats['orders_count'] = (int) $row['orders_count'];
		$event_stats['ticket_sales'] = (float) $row['ticket_sales'];
		$event_stats['unique_checkins'] = (int) $row['unique_checkins'];
		$event_stats['budgets_balance'] = (float) $row['budgets_balance'];
		$event_stats['gifts_count'] = (int) $row['gifts_count'];
		$event_stats['users_count'] = (int) $row['users_count'] + 1;
	}
}


//test
//fn1::printr($event_stats); exit();
