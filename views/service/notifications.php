<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//constants
const NOTIFICATION_CLEANUP_DAYS = 2;
const NOTIFICATION_TYPE_SYSTEM = 0;

//fetch notifications
if (isset($request_params['get_notifications']) && (int) $request_params['get_notifications'] == 1){
	$db = null;
	try {
		//s::success(demo_items(2, 1), 'Demo notifications'); //TODO remove
		if (!(isset($session_uid) && strlen($session_uid))) throw new Exception('Invalid session!');
		if (($cleanup = notifications_cleanup()) !== true) throw new Exception($cleanup); //cleanup before fetch
		$db = new Database();
		if (!$result = $db -> select(TABLE_NOTIFICATIONS, null, 'WHERE `uid` = ? ORDER BY `_index` DESC, `status` ASC', [$session_uid])) throw new Exception($db -> getErrorMessage());
		$items = [];
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				$item = [
					'id' => $row['id'],
					'text' => $row['text'],
					'icon' => $row['icon'],
					'time' => notification_time($row['timestamp']),
					'status' => (int) $row['status'],
				];
				$items[] = $item;
			}
		}
		$db -> close();
		$db = null;
		s::success($items, 'User notifications');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//get notification action
if (isset($request_params['get_notification_action'])){
	$id = fn1::toStrn($request_params['get_notification_action']);
	$db = null;
	try {
		if (!(isset($session_uid) && strlen($session_uid))) throw new Exception('Invalid session!');
		if ($id === '') throw new Exception('Invalid notification action');
		$db = new Database();
		if (($notice = $db -> queryItem(TABLE_NOTIFICATIONS, null, 'WHERE `id` = ? AND `uid` = ?', [$id, $session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$notice) throw new Exception('Notification does not exist');
		$notice = xstrip($notice);
		$action = $notice['action'];
		if (!$db -> update(TABLE_NOTIFICATIONS, ['status' => 1], 'WHERE `id` = ? AND `uid` = ?', [$id, $session_uid])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success($action, 'Notification action');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//mark all read
if (isset($request_params['mark_notifications_read']) && (int) $request_params['mark_notifications_read'] == 1){
	$db = null;
	try {
		if (!(isset($session_uid) && strlen($session_uid))) throw new Exception('Invalid session!');
		$db = new Database();
		if (!$db -> update(TABLE_NOTIFICATIONS, ['status' => 1], 'WHERE `uid` = ? AND `status` = "0"', [$session_uid])) throw new Exception($db -> getErrorMessage());
		$affected_rows = $db -> affected_rows;
		$db -> close();
		$db = null;
		s::success($affected_rows, 'Marked as read');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//notification functions
function notify($uid, $text, $action, $icon='', $type=0, $ref=''){
	global $notify_error;
	$uid = fn1::toStrn($uid, true);
	$text = fn1::toStrn($text, true);
	$action = fn1::toStrn($action, true);
	$icon = fn1::toStrn($icon, true);
	$type = (int) $type;
	$ref = fn1::toStrn($ref, true);
	$db = null;
	try {
		if ($uid === '') throw new Exception('Invalid user reference');
		if ($text === '') throw new Exception('Invalid notification text');
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_USERS, 'WHERE `uid` = ?', [$uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Notification user not found');
		$data = [
			'id' => $db -> createToken(TABLE_NOTIFICATIONS, 'id'),
			'uid' => $uid,
			'type' => $type,
			'text' => $text,
			'icon' => $icon,
			'action' => $action,
			'ref' => $ref,
			'status' => 0,
			'timestamp' => fn1::now(),
		];
		if (!$db -> insert(TABLE_NOTIFICATIONS, $data)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		return $data;
	}
	catch (Exception $e){
		$notify_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}

//check if can notify
function can_notify($uid, $type){}

//notification time
function notification_time($timestamp){
	$time = 'New';
	$temp = fn1::parseDate($timestamp = fn1::toStrx($timestamp, true));
	if (fn1::formatDate($temp) == $timestamp){
		$diff_today = $temp -> time_diff(null);
		if ($diff_today < 1) $time = $temp -> format('h:i A');
		elseif ($diff_today < 2) $time = 'Yesterday';
		elseif ($diff_today < 7) $time = $diff_today . ' days ago';
		else $time = $temp -> format('d/m/Y h:i A');
	}
	return $time;
}

//notifications cleanup
function notifications_cleanup(){
	$db = null;
	try {
		//delete notifications read and past NOTIFICATION_CLEANUP_DAYS
		$db = new Database();
		$today = fn1::now('Y-m-d');
		$days = NOTIFICATION_CLEANUP_DAYS;
		$table = $db -> escapeString(TABLE_NOTIFICATIONS, true);
		if (!$db -> query("DELETE FROM `$table` WHERE `status` = '1' AND ABS(DATEDIFF(`timestamp`, '$today')) > $days")) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		return true;
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return $error;
	}
}

//todo remove
function demo_items($n=5, $unread_max=2){
	$texts = [
		'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus sapien, malesuada non ipsum at, auctor dictum urna.',
		'Donec at ex eu nisi efficitur gravida sit amet tempus eros.',
		'Proin interdum, ipsum aliquet efficitur dapibus, justo velit sagittis mauris, ut congue ipsum arcu eu quam.',
		'Sed tincidunt maximus lacus sed cursus. Vivamus vitae orci vitae erat elementum posuere sed ut purus. Etiam sollicitudin, lectus et malesuada mattis, augue mauris eleifend enim, a eleifend nulla eros eget sem. Nam posuere ac elit eleifend convallis.',
		'Suspendisse eu lectus ligula',
	];
	$icons = [
		'',
		'calendar-check-o',
		'check',
		'at',
		'commenting-o',
	];
	$x = 0;
	$timestamp = new DateTime();
	$items = [];
	for ($i = 0; $i < $n; $i ++){
		$s = (($i + 1) * 24 * 60 * 60) + rand(10, 100);
		$ts = $timestamp -> getTimestamp() + $s;
		$timestamp -> setTimestamp($ts);
		$item = [
			'id' => 'test_id_' . $i,
			'text' => $texts[$x],
			'icon' => $icons[$x],
			'time' => notification_time($timestamp -> format('Y-m-d H:i:s')),
			'status' => ($i + 1) <= $unread_max ? 0 : 1,
		];
		$items[] = $item;
		$x = ($x == 4 ? 0 : $x + 1);
	}
	return $items;
}

//clear unread notification
function notification_clear($ref){
	global $notification_clear_error;
	$db = null;
	try {
		$db = new Database();
		if (!$db -> delete(TABLE_NOTIFICATIONS, 'id', 'WHERE `ref` = ? AND `status` = \'0\'', [$ref])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		return true;
	}
	catch (Exception $e){
		$notification_clear_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
