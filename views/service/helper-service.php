<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//fetch service data
//function get_service_data($service_id, $get_vendor=true, $get_pricing=true, $get_vendor_services=true, $get_related_services=true, $redirect_error=true, $output_error=true, $redirect_location="/search"){
function get_service_data($service_id, $options=null){
	global $root;
	global $get_service_data_error;
	global $get_vendor_data_error;
	global $get_service_pricing_error;
	global $get_service_rating_error;
	global $get_service_gallery_error;

	//options
	$options = fn1::toArray($options);
	$get_vendor = array_key_exists("vendor", $options) ? !!$options["vendor"] : true;
	$get_pricing = array_key_exists("pricing", $options) ? !!$options["pricing"] : true;
	$get_rating = array_key_exists("rating", $options) ? !!$options["rating"] : true;
	$get_gallery = array_key_exists("gallery", $options) ? !!$options["gallery"] : true;
	$get_vendor_services = array_key_exists("services", $options) ? !!$options["services"] : true;
	$get_related_services = array_key_exists("related", $options) ? !!$options["related"] : true;
	$redirect_error = array_key_exists("redirect_error", $options) ? !!$options["redirect_error"] : true;
	$output_error = array_key_exists("output_error", $options) ? !!$options["output_error"] : true;
	$redirect_default = $root . "/search?origin=error";
	$redirect_location = array_key_exists("redirect_location", $options) ? (strlen($temp = fn1::toStrx($options["redirect_location"])) ? $temp : $redirect_default) : $redirect_default;

	$db = null;
	$get_service_data_error = "";
	try {
		$service_id = fn1::toStrx($service_id, true);
		if (!strlen($service_id)) throw new Exception(0);

		//connection
		$db = new Database();

		//fetch service
		$service = $db -> queryItem(TABLE_VENDOR_SERVICES, null, "WHERE `id` = ? AND `deleted` = '0'", [$service_id]);
		if ($service === false) throw new Exception($db -> getErrorMessage());
		if (!$service) throw new Exception("Service not found", 1001);

		//close connection
		$db -> close();
		$db = null;

		//service info
		$service = service_info($service);
		$vendor_id = $service["vendor_id"];

		//fetch vendor
		$vendor = null;
		if ($get_vendor && ($vendor = get_vendor_data($vendor_id, $service_id, $get_vendor_services)) === false) throw new Exception($get_vendor_data_error);

		//fetch pricing
		$pricing = [];
		if ($get_pricing){
			if (($tmp_pricing = get_service_pricing($service_id, $vendor_id)) === false) throw new Exception($get_service_pricing_error);
			$pricing = $tmp_pricing;
		}

		//fetch rating
		$rating = ["count" => 0, "rating" => 0, "reviews" => []];
		if ($get_rating){
			if (($tmp_rating = get_service_rating($service_id)) === false) throw new Exception($get_service_rating_error);
			$rating = $tmp_rating;
		}

		//fetch gallery
		$gallery = [];
		if ($get_gallery){
			if (($tmp_gallery = get_service_gallery($service_id)) === false) throw new Exception($get_service_gallery_error);
			$gallery = $tmp_gallery;
		}

		//fetch related services
		$related_services = [];
		if ($get_related_services){
			$temp = fetch_services("WHERE `vendor_id` <> ? AND `category_value` = ? AND `deleted` = '0' AND `paused` = '0' ORDER BY `timestamp` DESC LIMIT 10", [$vendor_id, $service["category_value"]]);
			if ($temp === false) throw new Exception($GLOBALS['fetch_services_error']);
			$related_services = $temp;
		}

		//return data
		return [
			"service" => $service,
			"vendor" => $vendor,
			"pricing" => $pricing,
			"rating" => $rating,
			"gallery" => $gallery,
			"pricing" => $pricing,
			"related" => $related_services,
		];
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		if ($e -> getCode() == 1001 && $redirect_error){
			header("location: " . $redirect_location);
			exit();
		}
		$get_service_data_error = $e -> getMessage();
		if ($output_error) s::error(null, $get_service_data_error);
		return false;
	}
}

//fetch services
function fetch_services($bind_query, $bind_params=null){
	global $fetch_services_error;
	$fetch_services_error = null;
	$db = null;
	try {
		$db = new Database();
		$result = $db -> select(TABLE_VENDOR_SERVICES, null, $bind_query, $bind_params);
		if ($result === false) throw new Exception($db -> getErrorMessage());
		$services = [];
		if ($result instanceof mysqli_result) while ($row = $result -> fetch_array(MYSQLI_ASSOC)) $services[] = service_info($row);
		$db -> close();
		return $services;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$fetch_services_error = $e -> getMessage();
		return false;
	}
}

//service info
function service_info($service){
	static $tmp_uploads_folder;
	if (!$tmp_uploads_folder) $tmp_uploads_folder = getUploads();
	$row = xstrip(fn1::toArray($service));
	if (!fn1::hasKeys($row, "id", "title", "description")) return false;
	$tmp_location_name = trim($row["location_name"]);
	if (strtolower($tmp_location_name) == "current location") $tmp_location_name = "Custom";
	$tmp_image_src = $tmp_uploads_folder . "/" . $row["image"];
	$service_id = $row["id"];
	$service = [
		"id" => $service_id,
		"vendor_id" => $row["vendor_id"],
		"image" => $row["image"],
		"image_src" => $tmp_image_src,
		"title" => $row["title"],
		"description" => $row["description"],
		"category_name" => $row["category_name"],
		"category_value" => (int) $row["category_value"],
		"location_name" => $tmp_location_name,
		"location_lat" => (float) $row["location_lat"],
		"location_lon" => (float) $row["location_lon"],
		"location_map_lat" => (float) $row["location_map_lat"],
		"location_map_lon" => (float) $row["location_map_lon"],
		"timestamp" => $row["timestamp"],
		"featured" => (int) $row["featured"],
		"sponsored" => (int) $row["sponsored"],
		"paused" => (int) $row["paused"],
		"deleted" => (int) $row["deleted"],
		"demo" => false,
		"in_cart" => service_in_cart($service_id),
		"in_favorites" => service_in_favorites($service_id),
	];
	if (array_key_exists("rating", $row) && array_key_exists("rating_count", $row)){
		$service["rating"] = $row["rating"];
		$service["rating_count"] = $row["rating_count"];
	}
	return xstrip($service, true);
}

//service cart status
function service_in_cart($service_id){
	global $session_uid;
	if (sessionActive() && isset($session_uid) && strlen($session_uid) && strlen($service_id = fn1::toStrx($service_id, true))){
		$db = new Database();
		$existing = $db -> queryItem(TABLE_CART, null, "WHERE `uid` = ? AND `service_id` = ?", [$session_uid, $service_id]);
		$db -> close();
		if ($existing && array_key_exists("id", $existing)){
			$existing = xstrip($existing);
			return $existing["id"];
		}
	}
	return null;
}

//service favorite status
function service_in_favorites($service_id){
	global $session_uid;
	if (sessionActive() && isset($session_uid) && strlen($session_uid) && strlen($service_id = fn1::toStrx($service_id, true))){
		$db = new Database();
		$existing = $db -> queryItem(TABLE_FAVORITES, null, "WHERE `uid` = ? AND `service_id` = ?", [$session_uid, $service_id]);
		$db -> close();
		if ($existing && array_key_exists("id", $existing)){
			$existing = xstrip($existing);
			return $existing["id"];
		}
	}
	return null;
}

//service vendor
function get_vendor_data($vendor_id, $service_id=null, $get_vendor_services=true){
	global $get_vendor_data_error;
	global $fetch_services_error;

	$db = null;
	try {
		if (!strlen($vendor_id = fn1::toStrx($vendor_id, true))) throw new Exception("Invalid vendor reference");

		//db connection
		$db = new Database();

		//fetch vendor
		$vendor = $db -> queryItem(TABLE_VENDORS, null, "WHERE `id` = ?", [$vendor_id]);
		if ($vendor === false) throw new Exception($db -> getErrorMessage());
		if (!$vendor) throw new Exception("Vendor not found");
		$vendor = xstrip($vendor, true);
		unset($vendor["_index"]);

		//fetch vendor categories
		$vendor_categories = [];
		$result = $db -> select(TABLE_VENDOR_CATEGORIES, null, "WHERE `vendor_id` = ?", [$vendor_id]);
		if ($result === false) throw new Exception($db -> getErrorMessage());
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				unset($row["_index"]);
				unset($row["vendor_id"]);
				$vendor_categories[] = $row;
			}
		}

		//fetch vendor services
		$vendor_services = [];
		if ($get_vendor_services){
			$service_id = fn1::toStrx($service_id, true);
			$temp = fetch_services("WHERE `id` <> ? AND `vendor_id` = ? AND `deleted` = '0' AND `paused` = '0'", [$service_id, $vendor_id]);
			if ($temp === false) throw new Exception($GLOBALS['fetch_services_error']);
			$vendor_services = $temp;
		}

		//close connection
		$db -> close();

		//return vendor data
		$vendor["categories"] = $vendor_categories;
		$vendor["services"] = $vendor_services;
		return $vendor;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$get_vendor_data_error = $e -> getMessage();
		return false;
	}
}

//service pricing
function get_service_pricing($service_id, $vendor_id){
	global $get_service_pricing_error;
	$db = null;
	try {
		//db connection
		$db = new Database();

		//fetch pricing details
		$result = $db -> select(TABLE_VENDOR_SERVICE_PRICING, null, "WHERE `service_id` = ? AND `vendor_id` = ? ORDER BY `price` ASC", [$service_id, $vendor_id]);
		if ($result === false) throw new Exception($db -> getErrorMessage());
		$pricing = [];
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row);
				unset($row["_index"]);
				// unset($row["service_id"]);
				unset($row["vendor_id"]);
				$row['service_id'] = fn1::toStr($row['service_id'], true);
				$row["price"] = fn1::toNum($row["price"]);
				$row["package"] = fn1::jsonParse($row["package"], true, []);
				$pricing[] = xstrip($row, true);
			}
		}

		//close connection
		$db -> close();

		//return
		return $pricing;
	} catch (Exception $e){
		$get_service_pricing_error = $e -> getMessage();
		return false;
	}
}

function get_service_rating($service_id, $get_reviews=true){
	global $get_service_rating_error;
	$db = null;
	try {
		$db = new Database();
		if (!$result = $db -> select(TABLE_REVIEWS, null, 'WHERE `service_id` = ? AND `type` = \'1\' ORDER BY `_index` DESC', [$service_id])) throw new Exception($db -> getErrorMessage());
		$items = [];
		$rating_count = 0;
		$rating_sum = 0;
		$my_review = null;
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				$rating = (int) $row['rating'];
				$rating_sum += $rating;
				$rating_count ++;
				if (!$get_reviews) continue;
				$avatar = trim($row['avatar']);
				if (fn1::isFile(getUploads(true) . '/' . preg_replace('/^\/|\/$/s', '', $avatar))) $avatar = getUploads() . '/' . preg_replace('/^\/|\/$/s', '', $avatar);
				$item = [
					'id' => $row['id'],
					'order_id' => $row['order_id'],
					'avatar' => $avatar,
					'name' => trim($row['name']),
					'rating' => $rating,
					'comment' => trim($row['comment']),
					'timestamp' => $row['timestamp'],
				];
				$items[] = $item;
			}
		}
		$db -> close();
		$db = null;
		$count = $rating_count;
		$rating = $count > 0 ? round($rating_sum / $count, 1) : 0;
		return [
			'count' => $count,
			'rating' => $rating,
			'reviews' => $items,
		];
	}
	catch (Exception $e){
		$get_service_rating_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}

function get_service_gallery($service_id){
	global $root;
	global $get_service_gallery_error;
	$db = null;
	try {
		$db = new Database();
		if (!$result = $db -> select(TABLE_SERVICE_GALLERY, ['id', 'image', 'caption', 'timestamp'], 'WHERE `service_id` = ? ORDER BY _index ASC', [$service_id])) throw new Exception($db -> getErrorMessage());
		$gallery = [];
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				$gallery[] = $row;
			}
		}
		$db -> close();
		$db = null;
		return $gallery;
	}
	catch (Exception $e){
		$get_service_gallery_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}

function get_user_profile_data($user_id) {
	global $root;
	global $get_user_data_error;
	$db = null;
	try {
		$db = new Database();
		if(!$result = $db -> select(TABLE_USERS, 
			['photo_url', 'display_name', 'email', 'description', 'first_name', 
			'last_name', 'gender', 'age', 'birth_day', 
			'birth_month', 'birth_year', 'phone', 'address', 'country', 'region', 'city', 'zip'], 
			'WHERE `uid` = ?', $user_id)) throw new Exception($db -> getErrorMessage());
		$user = [];
		if($result instanceof mysqli_result) {
			while($row = $result -> fetch_array(MYSQLI_ASSOC)) {
				$row = xstrip($row, true);
				$user = $row;
			}
		}
		$db -> close();
		$db = null;
		return $user;
	} catch (Exception $e) {
		$get_user_data_error= $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
