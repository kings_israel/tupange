<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;

//db insert data helper function
function db_insert($db, $conn, $table, $data){
	if (!($db instanceof Database && $conn instanceof mysqli)) return false;
	if (($items = $db -> queryDataItems($data)) === false) return false;
	foreach ($items as $i => $item){
		$bind_values = [];
		$query_fields = [];
		$query_values = [];
		foreach ($item as $field => $value){
			$bind_values[] = "?";
			$query_fields[] = "`$field`";
			$query_values[] = $value;
		}
		$bind_values = implode(", ", $bind_values);
		$query_fields = implode(", ", $query_fields);
		$sql = fn1::strbuild("INSERT INTO `%s` (%s) VALUES (%s)", $table, $query_fields, $bind_values);
		if (!$db -> queryPrepared($sql, $query_values, $conn)) return false;
		$db -> closeStatement() -> closeResult();
	}
	return true;
}
