<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;

//db update data helper function
function db_update($db, $conn, $table, $data, $where_bind, $where_bind_values){
	if (!($db instanceof Database && $conn instanceof mysqli)) return false;
	if (($items = $db -> queryDataItems($data)) === false) return false;
	$where = preg_match('/where/is', $where_bind) ? "" : "WHERE ";
	if (($bind = $db -> queryBind($where_bind, $where_bind_values)) === false) return false;
	foreach ($items as $i => $item){
		$bind_set = [];
		$query_values = [];
		foreach ($item as $field => $value){
			$bind_set[] = "`$field`=?";
			$query_values[] = $value;
		}
		$bind_set = implode(", ", $bind_set);
		$sql = fn1::strbuild("UPDATE `%s` SET %s " . $where . "%s", $table, $bind_set, fn1::propval($bind, 0));
		$query_values = array_merge($query_values, $bind[1]);
		if (!$db -> queryPrepared($sql, $query_values, $conn)) return false;
		$db -> closeStatement() -> closeResult();
	}
	return true;
}
