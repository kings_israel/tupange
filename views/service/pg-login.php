<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-local-avatar.php";
use Naicode\Server\Plugin\User;
use Naicode\Server\Funcs as fn1;

//handle session active
if (sessionActive()){
	if (isset($_GET["redirect"])) header("location: " . $_GET["redirect"]);
	else header("location: " . NS_SITE . "/dashboard");
}

//handle email/password login
if (isset($request_params["login_username"]) && isset($request_params["login_password"])){
	$remember_me = isset($request_params["remember_me"]) && $request_params["remember_me"] == "on" ? 1 : 0;
	$login_username = fn1::toStrn($request_params["login_username"], true);
	$login_password = $request_params["login_password"];
	try {
		if (($user = User::getPasswordUser($login_username, $login_password)) === false) throw new Exception(User::$last_error); //login username password
		if (!$user = User::createSessionToken($user -> uid)) throw new Exception(User::$last_error); //create session token
		$user -> photo_url = localAvatar($user -> uid, $user -> photo_url); //update avatar
		$user -> setSession(); //set session
		if (!temp_save($user -> token_fingerprint, "token", $user -> token, 30)) throw new Exception($temp_save_error); //temp token save

		//login successful ~ redirect
		if (isset($_GET["redirect"])) header("location: " . $_GET["redirect"]);
		else header("location: " . NS_SITE);
		exit();
	}
	catch (Exception $e){
		//set login error
		$login_error = $e -> getMessage();
	}
}