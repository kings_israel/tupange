<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//fetch user event
if (!$event_data = fetch_event($event_id, $session_uid)){
	//handle event not found
	if (isset($request_params["edit_gift"]) || isset($request_params["delete_gift"])) s::error(null, isset($fetch_event_error) ? $fetch_event_error : "Event not found");
	header("location: $root/dashboard?events");
	exit();
}

//check guest user limit
if ($event_data['guest'] && !in_array($event_data['role'], [0, 1])){
	header("location: $root/dashboard?events");
	exit();
}

//edit gift
if (isset($request_params['edit_gift'])){
	$id = fn1::toStrx($request_params['edit_gift'], true);
	$title = fn1::toStrn($request_params['title'], true);
	$description = fn1::toStrn($request_params['description'], true);
	$value = (float) $request_params['value'];
	$received_date = fn1::toStrn($request_params['received_date'], true);
	$received_by = fn1::toStrn($request_params['received_by'], true);
	$received_from = fn1::toStrn($request_params['received_from'], true);
	$phone = fn1::toStrn($request_params['phone'], true);
	$email = fn1::toStrn($request_params['email'], true);
	$notes = fn1::toStrn($request_params['notes'], true);
	$image = fn1::toStrn($request_params['image'], true);
	$db = null;
	try {
		if ($title == '') throw new Exception('Kindly provide the gift title');
		if ($value < 0) throw new Exception('Kindly provide a valid gift price value');
		if ($received_date != ''){
			$tmp_date = fn1::parseDate($received_date, 'd/m/Y');
			if ($tmp_date -> format('d/m/Y') != $received_date) throw new Exception('Invalid received date format. Use format DD/MM/YYYY');
			$received_date = $tmp_date -> format('Y-m-d');
		}
		$db = new Database();
		if ($id != ''){
			if (($existing = $db -> queryItem(TABLE_GIFTS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) === false) throw new Exception($db -> getErrorMessage());
			if (!$existing) throw new Exception('Gift reference does not exist!');
		}
		$timestamp = fn1::now();
		$data = [
			 'title' => $title,
			 'description' => $description,
			 'value' => $value,
			 'received_date' => $received_date,
			 'received_by' => $received_by,
			 'received_from' => $received_from,
			 'phone' => $phone,
			 'email' => $email,
			 'notes' => $notes,
			 'image' => $image,
			 'timestamp' => $timestamp
		];
		if ($id == ''){
			$id = $db -> createToken(TABLE_GIFTS, 'id');
			$data['id'] = $id;
			$data['event_id'] = $event_id;
			if (!$db -> insert(TABLE_GIFTS, $data)) throw new Exception($db -> getErrorMessage());
		}
		else {
			if (!$db -> update(TABLE_GIFTS, $data, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) throw new Exception($db -> getErrorMessage());
			$data = array_replace_recursive($existing, $data);
		}
		$db -> close();
		unset($data['_index']);
		unset($data['event_id']);
		s::success($data, 'Gift details have been saved successfully!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//delete gift
if (isset($request_params['delete_gift'])){
	$id = fn1::toStrx($request_params['delete_gift'], true);
	$db = null;
	try {
		if ($id == '') throw new Exception('Invalid gift reference');
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_GIFTS, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Gift reference does not exist!');
		if (!$db -> delete(TABLE_GIFTS, 'id', 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		s::success($id, 'Gift entry has been deleted successfully!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//fetch gifts
$total_value = 0;
if (($gifts = get_gifts($event_id, $total_value)) === false) die ($get_gifts_error);
$gifts_stats = ['count' => count($gifts), 'value' => $total_value];

//event data
$event = event_from_data($event_data);

//get gifts
function get_gifts($event_id, &$total_value=null){
	global $get_gifts_error;
	$db = null;
	try {
		$db = new Database();
		if (($result = $db -> select(TABLE_GIFTS, null, 'WHERE `event_id` = ? ORDER BY `timestamp` DESC', [$event_id])) === false) throw new Exception($db -> getErrorMessage());
		$items = [];
		$total_value = 0;
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				$image = $row['image'];
				$value = (float) $row['value'];
				$total_value += $value;
				$items[] = [
					'id' => $row['id'],
					'title' => $row['title'],
					'description' => $row['description'],
					'value' => $value,
					'received_date' => $row['received_date'],
					'received_by' => $row['received_by'],
					'received_from' => $row['received_from'],
					'phone' => $row['phone'],
					'email' => $row['email'],
					'notes' => $row['notes'],
					'image' => $image,
					'timestamp' => $row['timestamp'],
				];
			}
		}
		$db -> close();
		return $items;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$get_gifts_error = 'Get Gifts Error: ' . $e -> getMessage();
		return false;
	}
}
































/*eof*/
