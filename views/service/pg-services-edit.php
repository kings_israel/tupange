<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-db-insert.php";
require_once __DIR__ . "/helper-db-update.php";
require_once __DIR__ . "/helper-purge-service.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Plugin\Files;
use Naicode\Server\Database;
use Naicode\Server as s;

//redirect incomplete vendor profile
if (!(isset($vendor_data) && array_key_exists("id", $vendor_data))){
	header("location: " . $root . "/complete");
	exit();
}

//set vendor id
$vendor_id = $vendor_data["id"];

//load existing service
if (isset($service_id)){
	try {
		//db connection
		$db = new Database();

		//fetch existing service data
		if (!$result = $db -> select(TABLE_VENDOR_SERVICES, null, "WHERE `vendor_id` = ? AND `id` = ?", [$vendor_id, $service_id])) throw new Exception($db -> getErrorMessage());

		//redirect if service not found
		if (!$row = $result -> fetch_array(MYSQLI_ASSOC)){
			exit();
			$db -> close();
			header("location: " . $root . "/services");
			exit();
		}

		//existing service form variables
		$service_image_src = $row["image"];
		$service_image = getUploads() . "/" . $service_image_src;
		$service_title = fn1::toStrx($row["title"], true);
		$service_description = fn1::toStrx($row["description"], true);
		$service_category_value = fn1::toStrx($row["category_name"], true);
		$location_value = fn1::toStrx($row["location_name"], true);
		$service_contact_phone_number = fn1::toStrx($row['service_contact_phone_number']);
		$service_contact_email = fn1::toStrx($row['service_contact_email']);
		$location_map_value = str_replace("\"", "'", fn1::jsonCreate(["lat" => (float) $row["location_map_lat"], "lng" => (float) $row["location_map_lon"]]));
		$service_paused = (int) $row["paused"];
		$service_deleted = (int) $row["deleted"];

		//handle service delete restore
		if (isset($_GET["restore"])){
			if ($_GET["restore"] == $service_title && !$db -> update(TABLE_VENDOR_SERVICES, ["deleted" => 0, "timestamp" => fn1::now()], "`deleted` = '1' AND `vendor_id` = ? AND `id` = ?", [$vendor_id, $service_id])) throw new Exception($db -> getErrorMessage());
			$db -> close();
			header("location: " . $root . "/services?view=deleted");
			exit();
		}

		//handle service permanent remove
		if (isset($_GET["remove"])){
			$db -> close();
			//purge service
			if ($_GET["remove"] == $service_title && !purge_service($vendor_id, $service_id)) throw new Exception(isset($purge_service_error) ? $purge_service_error : "Error removing service records!");
			header("location: " . $root . "/services?view=deleted");
			exit();
		}

		//deleted service redirect
		if ($service_deleted == 1){
			$db -> close();
			header("location: " . $root . "/services");
			exit();
		}

		//handle service pause
		if (isset($_GET["pause"])){
			if ($_GET["pause"] == $service_title && !$db -> update(TABLE_VENDOR_SERVICES, ["paused" => 1, "timestamp" => fn1::now()], "`deleted` = '0' AND `paused` = '0' AND `vendor_id` = ? AND `id` = ?", [$vendor_id, $service_id])) throw new Exception($db -> getErrorMessage());
			$db -> close();
			header("location: " . $root . "/services");
			exit();
		}

		//handle service activate paused
		if (isset($_GET["activate"])){
			if ($_GET["activate"] == $service_title){
				if (strlen($service_title) < 10){
					$db -> close();
					header("location: " . $root . "/services?view=edit&s=$service_id&fix");
					exit();
				}
				if (strlen($service_description) < 40){
					$db -> close();
					header("location: " . $root . "/services?view=edit&s=$service_id&fix");
					exit();
				}
				if (!$db -> update(TABLE_VENDOR_SERVICES, ["paused" => 0, "timestamp" => fn1::now()], "`deleted` = '0' AND `paused` = '1' AND `vendor_id` = ? AND `id` = ?", [$vendor_id, $service_id])) throw new Exception($db -> getErrorMessage());
			}
			$db -> close();
			header("location: " . $root . "/services?view=paused");
			exit();
		}

		//handle service delete
		if (isset($_GET["delete"])){
			if ($_GET["delete"] == $service_title && !$db -> update(TABLE_VENDOR_SERVICES, ["deleted" => 1, "timestamp" => fn1::now()], "`deleted` = '0' AND `vendor_id` = ? AND `id` = ?", [$vendor_id, $service_id])) throw new Exception($db -> getErrorMessage());
			$db -> close();
			header("location: " . $root . "/services" . ($service_paused == 1 ? "?view=paused" : ""));
			exit();
		}

		//close connection
		if (isset($_GET["fix"])) $service_error = "Some edit changes are required to activate the service.";
		$db -> close();
	}
	catch (Exception $e){
		//error handling
		if ($db !== null) $db -> close();
		s::text($e -> getMessage());
	}
}

//handle form submit
if (isset($request_params["service_title"]) && isset($request_params["service_description"])){
	$db = null;
	$conn = null;
	$uploaded_image = null;
	try {
		//get values
		$service_title = fn1::toStrn($request_params["service_title"], true);
		$service_description = fn1::toStrn($request_params["service_description"], true);
		$service_category = fn1::jsonParse($request_params["service_category"], true, []);
		$service_contact_phone_number = fn1::toStr($request_params['service_contact_phone_number'], true);
		$service_contact_email = fn1::toStr($request_params['service_contact_email'], true);
		
		if (fn1::hasKeys($service_category, "name", "value")){
			$category_value = (int) $service_category["value"];
			$category_name = fn1::toStrx($service_category["name"], true);
			$service_category_value = $category_name;
		}
		$location = fn1::jsonParse($request_params["location"], true, []);
		if (fn1::hasKeys($location, "lat", "lon", "name", "type")){
			$location_lat = (float) $location["lat"];
			$location_lon = (float) $location["lon"];
			$location_name = fn1::strtocamel(fn1::toStrx($location["name"], true));
			$location_type = fn1::toStrx($location["type"], true);
			$location_value = $location_name;
		}
		$location_map = fn1::jsonParse($request_params["location_map"], true, []);
		if (array_key_exists("lat", $location_map) && array_key_exists("lng", $location_map)){
			$location_map_lat = (float) $location_map["lat"];
			$location_map_lon = (float) $location_map["lng"];
			$location_map_value = str_replace("\"", "'", fn1::jsonCreate(["lat" => $location_map_lat, "lng" => $location_map_lon]));
		}

		//validate values
		if (fn1::isEmpty($service_title)) throw new Exception("Kindly enter the service title");
		if (strlen($service_title) < 10) throw new Exception("Your service title is too short. It should have at least 10 characters.");
		if (fn1::isEmpty($service_description)) throw new Exception("Kindly enter the service description");
		if (strlen($service_description) < 40) throw new Exception("Your service description is too short. It should have at least 40 characters.");
		if (!(!fn1::isEmpty($service_category) && fn1::hasKeys($service_category, "name", "value"))) throw new Exception("Kindly select a service category");
		if (!(!fn1::isEmpty($location) && fn1::hasKeys($location, "name", "lat", "lon"))) throw new Exception("Kindly select a location for the service");
		if (!(!fn1::isEmpty($location_map) && fn1::hasKeys($location_map, "lat", "lng"))) throw new Exception("Failed to get map value. Google maps might be unreachable kindly refresh the page and try again");

		if(!isset($request_params['company_contact_details_checkbox'])) {
			if (!fn1::isSafaricomNumber($service_contact_phone_number)) throw new Exception("Please enter a valid Safaricom phone number in the contact information.");
			if (!fn1::isEmail($service_contact_email)) throw new Exception('Please enter a valid email in the contact information.');	
		} else {
			// Get company contact information to save
			//db connection
			$db = new Database();

			//fetch existing service data
			$result = $db -> select(TABLE_VENDORS, null, "WHERE `id` = ?", $vendor_id);
			$row = $result -> fetch_array(MYSQLI_ASSOC);
			$service_contact_phone_number = $row['company_phone_number'];
			$service_contact_email = $row['company_email'];

			$db -> close();
		}

		

		//save values with rollback enabled
		$data = [
			"title" => $service_title,
			"description" => $service_description,
			"category_name" => $category_name,
			"category_value" => $category_value,
			"location_name" => $location_name,
			"location_lat" => $location_lat,
			"location_lon" => $location_lon,
			"location_map_lat" => $location_map_lat,
			"location_map_lon" => $location_map_lon,
			"service_contact_phone_number" => $service_contact_phone_number,
			"service_contact_email" => $service_contact_email,
			"timestamp" => fn1::now(),
		];

		//uploading display image
		if (isset($_FILES["display_image"]) && $_FILES["display_image"]["name"]){

			//check if image is too small
			list($width, $height) = @getimagesize($_FILES["display_image"]['tmp_name']);
			$width = (int) $width;
			$height = (int) $height;
			if ($width < 600 || $height < 300) throw new Exception("Display image is too small (" . $width . "x" . $height . "). Kindly select a larger image with dimensions greater or equal to 600x400 for better quality.");
			if ($width > 600){
				$resized_height = round((((600 / $width) * 100) / 100) * $height, 0);
				if ($resized_height < 300) throw new Exception("Display image is too wide for its height (" . $width . "x" . $height . " will be resized to 600x$resized_height). Kindly select another image with dimensions greater or equal to 600x400 for better quality.");
			}

			//upload new service image
			$files = new Files("display_image");
			$files -> setAllowedExtensions(["gif","jpg", "jpeg", "png"]);
			$files -> setMaxLimit(5);
			if (!$files -> fileUpload()) throw new Exception($files -> getErrorMessage());
			$uploads_folder = getUploads(true);
			$uploaded_image_src = $files -> uploaded[0]["src"];
			$uploaded_image = $uploads_folder . "/" . $uploaded_image_src;

			//resize large image to 600 width
			if ($width > 600){
				if (!$files -> resizeImage($uploaded_image, 600)){
					fn1::fileDelete($uploaded_image);
					throw new Exception($files -> getErrorMessage());
				}
			}

			//remove previous service image
			if (isset($service_image_src)) fn1::fileDelete($uploads_folder . "/" . $service_image_src);

			//add new service image
			$data["image"] = $uploaded_image_src;
		}

		//new service must have image
		if (!isset($service_image_src) && !array_key_exists("image", $data)) throw new Exception("Kindly select the main display image.");

		//db connection without autocommit so we can rollback on error
		$db = new Database();
		$conn = $db -> resetValues() -> getConnection(false);

		//insert/update data
		if (!isset($service_id)){
			$id = $db -> createToken(TABLE_VENDOR_SERVICES, "id");
			$data["id"] = $id;
			$data["vendor_id"] = $vendor_id;
			if (!db_insert($db, $conn, TABLE_VENDOR_SERVICES, $data)) throw new Exception($db -> err, $db -> errno);
		}
		else {
			//fix update
			if (isset($_GET["fix"])) $data["paused"] = "0";
			if (!db_update($db, $conn, TABLE_VENDOR_SERVICES, $data, "`id` = ? AND `vendor_id` = ?", [$service_id, $vendor_id])) throw new Exception($db -> err, $db -> errno);
		}

		//commit db changes and close
		if (!$conn -> commit()) throw new Exception($conn -> error, $conn -> errno);
		$db -> close();
		$conn = null;
		$db = null;

		if ($_POST['action'] == "Save and Add Service") {
			//successful, redirect to add service page
			$redirect_location = $root . "/services?view=edit";
			header("location: $redirect_location");
			exit();
		} else if ($_POST['action'] == "Save Details") {
			//successful, redirect to list
			$redirect_location = $root . "/services?view=active";
			if (isset($service_paused) && $service_paused == 1) $redirect_location = $root . "/services?view=paused";
			header("location: $redirect_location");
			exit();
		}
	}
	catch (Exception $e){
		//error handling
		$service_error = $e -> getMessage();
		if ($uploaded_image !== null && fn1::isFile($uploaded_image)) fn1::fileDelete($uploaded_image);
		if ($conn !== null && !$conn -> rollback()) $service_error .= " (Rollback Failed: " . $conn -> error . ")";
		if ($db !== null) $db -> close();
	}
}
