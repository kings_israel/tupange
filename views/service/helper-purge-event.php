<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;

//helper permanently delete service and related data
function purge_event($uid, $event_id){
	global $purge_event_error;
	$db = null;
	try {
		$db = new Database();
		$event_status = 0;
		$event = $db -> queryItem(TABLE_EVENTS, ["id", "poster", "status"], "`id` = ? AND `uid` = ?", [$event_id, $uid]);
		if ($event === false) throw new Exception($db -> getErrorMessage());
		if (fn1::hasKeys($event, "id", "poster", "status") && $event["id"] == $event_id){
			$event_status = (int) $event["status"];

			//delete event from db
			if (!$db -> delete(TABLE_EVENTS, "id", "WHERE `id` = ? AND `uid` = ?", [$event_id, $uid])) throw new Exception($db -> getErrorMessage());

			//delete event poster image
			if (!fn1::isEmpty($poster = fn1::toStrx($event["poster"], true)) && fn1::isFile($poster_path = getUploads(true) . "/" . $poster)) fn1::fileDelete($poster_path);

			//TODO: delete event related data...
		}
		$db -> close();
		return $event_status;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$purge_event_error = $e -> getMessage();
		return false;
	}
}
