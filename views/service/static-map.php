<?php
$key = "AIzaSyCN73V5VTrs-MzakmhHJ2Pe47lxOc7B6PE";
$test_lat = -1.223017;
$test_lon = 36.89977299999998;
$test_label = "Tupange";

$data = get_map_image_data($test_lat, $test_lon, $test_label);
header("content-type: image/png");
echo $data;
exit();

function get_map_image_data($lat, $lon, $label="C"){
	$params = [
		"center" => $lat . "," . $lon,
		"markers" => "color:red|label:$label|$lat,$lon",
		"zoom" => 16,
		"size" => "600x400",
		"key" => "AIzaSyCN73V5VTrs-MzakmhHJ2Pe47lxOc7B6PE",
	];
	$url_params = "";
	foreach ($params as $key => $value) $url_params .= (strlen($url_params) ? "&" : "") . $key . "=" . urlencode("$value");
	$url = "https://maps.googleapis.com/maps/api/staticmap?" . $url_params;
	$image_type = pathinfo($url, PATHINFO_EXTENSION);
	$image_data = file_get_contents($url);
	return $image_data;
}
