<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Plugin\User;
use Naicode\Server\Funcs as fn1;

//handle password reset
if (isset($request_params["reset_password"]) && isset($request_params["reset_confirm_password"])){
	$reset_password = $request_params["reset_password"];
	$reset_confirm_password = $request_params["reset_confirm_password"];
	if (!strlen($code = $route_child)) $reset_error = "Password recovery link has expired.";
	elseif (strlen($reset_password) < 5) $reset_error = "Password must have 5 or more characters";
	elseif ($reset_password != $reset_confirm_password) $reset_error = "Your passwords do not match!";
	else {
		if (!User::resetPassword($code, $reset_password, $reset_confirm_password)) $reset_error = User::$last_error;
		else {
			if (!isset($_SESSION)) session_start(); //TODO temp redirect
			$_SESSION["reset_success"] = "Your new password is now in effect. You can <a href=\"$root/login\">login</a> now.";
			header("location: " . NS_SITE . "/reset");
			exit();
		}
	}
}

//get success message from session
if (isset($_SESSION["reset_success"])){
	$reset_success = $_SESSION["reset_success"];
	unset($_SESSION["reset_success"]);
}
