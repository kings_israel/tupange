<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
include __DIR__ . "/helper-service.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//constants
const ORDER_STATUS_REMOVED = -3;
const ORDER_STATUS_CANCELLED = -2;
const ORDER_STATUS_DECLINED = -1;
const ORDER_STATUS_SENT = 0;
const ORDER_STATUS_RECEIVED = 1;
const ORDER_STATUS_PAID = 2;
const ORDER_STATUS_DELIVERED = 3;
const ORDER_STATUS_COMPLETE = 4;
const ORDER_STATUS_DISPUTE = 5;
const ORDER_STATUS_ARCHIVED = 6;
const PAYMENT_TYPE_CARD = 0;

//require session
$load_vendor_data = true;
include __DIR__ . "/session.php";

//check if vendor
$is_vendor = false;
if (isset($vendor_data) && array_key_exists("id", $vendor_data) && strlen($vendor_id = fn1::toStrx($vendor_data["id"], true))) $is_vendor = true;
else unset($vendor_id);

//page variables
$page_title = "Tupange | My Orders";
$page_description = "My Orders";
$orders_view = '';
$orders_child = false;
$session_userlevel = (int) $session_userlevel;
$order_types = [0 => 'Order', 1 => 'Request'];
$order_statuses = [1 => "Received", 2 => "Paid", 3 => "Delivered", 4 => "Completed", 5 => "Dispute", 6 => "Archived", 0 => "Sent", -1 => "Declined", -2 => "Cancelled", -3 => "Removed"];
$payment_types = [0 => 'Card'];

//redirect incomplete vendor account
if ($session_userlevel == 2 && $session_status == 0){
	header("location: $root/complete");
	exit();
}

//switch view
if (isset($_GET["client"]) && $session_userlevel >= 2){
	$include = __DIR__ . "/../parts/part-orders-client.php";
	$page_title = "Tupange | Client Orders";
	$page_description = "Client orders";
	$orders_view = 'client';
}
elseif (isset($_GET["client-requests"]) && $session_userlevel >= 2){
	$include = __DIR__ . "/../parts/part-orders-client-requests.php";
	$page_title = "Tupange | Client Requests";
	$page_description = "Client order requests";
	$orders_view = 'client-requests';
}
elseif (isset($_GET["open"])){
	$include = __DIR__ . "/../parts/part-orders-open.php";
	$page_title = "Tupange | Open Orders";
	$page_description = "Open orders";
	$orders_view = 'open';
}
elseif (isset($_GET["requests"])){
	$include = __DIR__ . "/../parts/part-orders-requests.php";
	$page_title = "Tupange | Request Orders";
	$page_description = "Request orders";
	$orders_view = 'requests';
}
elseif (isset($_GET["history"])){
	$include = __DIR__ . "/../parts/part-orders-history.php";
	$page_title = "Tupange | Orders History";
	$page_description = "Orders history";
	$orders_view = 'history';
}
elseif (isset($_GET["request"]) && ($order_id = fn1::toStrx($_GET["request"], true)) != ''){
	$include = __DIR__ . "/../parts/part-orders-request.php";
	$page_title = "Tupange | Order Request";
	$page_description = "Order Request";
	$orders_view = 'request';
	$orders_child = true;
}
elseif (isset($_GET["edit-request"])){
	$include = __DIR__ . "/../parts/part-orders-request-edit.php";
	$page_title = "Tupange | Edit Request";
	$page_description = "Edit Request";
	$orders_view = 'request-edit';
	$orders_child = true;
	$edit_id = fn1::toStrx($_GET["edit-request"], true);
}
elseif (isset($_GET["order"]) && ($order_id = fn1::toStrx($_GET["order"], true)) != ''){
	$include = __DIR__ . "/../parts/part-orders-order.php";
	$page_title = "Tupange | Order Details";
	$page_description = "Order Details";
	$orders_view = 'order';
	$orders_child = true;
}
else {
	$location = $session_userlevel >= 2 ? "$root/orders/?client" : "$root/orders/?open";
	header("location: $location");
	exit();
}

//on update seen ~ user
if (isset($request_params["update_seen_user"])){
	$update_ids = fn1::csvArray($request_params["update_seen_user"]);
	$db = null;
	$updated_count = 0;
	try {
		$update_ids = count($update_ids) ? fn1::trimVal($update_ids[0]) : [];
		if (!(is_array($update_ids) && count($update_ids))) s::success($updated_count, "Updated");
		$db = new Database();
		$id_list = [];
		foreach ($update_ids as $id) if (($id = $db -> escapeString($id, true)) !== false && strlen($id)) $id_list[] = '"' . $id . '"';
		if (!count($id_list)) s::success($updated_count, "Updated");
		$id_list = implode(",", $id_list);
		if (!$db -> update(TABLE_ORDERS, ["seen_user" => fn1::now()], "WHERE `uid` = ? AND `seen_user` = '' AND `id` IN ($id_list)", [$session_uid])) throw new Exception($db -> getErrorMessage());
		$updated_count = (int) $db -> affected_rows;
		$db -> close();
		$db = null;
		s::success($updated_count, "Updated");
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on update seen ~ vendor
if (isset($request_params["update_seen_vendor"])){
	$update_ids = fn1::csvArray($request_params["update_seen_vendor"]);
	$db = null;
	$updated_count = 0;
	try {
		$update_ids = count($update_ids) ? fn1::trimVal($update_ids[0]) : [];
		if (!(is_array($update_ids) && count($update_ids))) s::success($updated_count, "Updated");
		$db = new Database();
		if (($vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `uid` = ?', [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$vendor) throw new Exception('Failed to fetch vendor details!');
		$id_list = [];
		foreach ($update_ids as $id) if (($id = $db -> escapeString($id, true)) !== false && strlen($id)) $id_list[] = '"' . $id . '"';
		if (!count($id_list)) s::success($updated_count, "Updated");
		$id_list = implode(",", $id_list);
		if (!$db -> update(TABLE_ORDERS, ["seen_vendor" => fn1::now()], "WHERE `vendor_id` = ? AND `seen_vendor` = '' AND `id` IN ($id_list)", [$vendor['id']])) throw new Exception($db -> getErrorMessage());
		$updated_count = (int) $db -> affected_rows;
		$db -> close();
		$db = null;
		s::success($updated_count, "Updated");
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order remove
if (isset($request_params["remove_order"])){
	$order_id = fn1::toStrn($request_params["remove_order"], true);
	try {
		if ($order_id == '') throw new Exception('Invalid order reference!');
		$db = new Database();
		$bind_query = "WHERE `id` = ? AND `uid` = ? AND `status`='-1'";
		$bind_params = [$order_id, $session_uid];
		if (($exists = $db -> queryExists(TABLE_ORDERS, $bind_query, $bind_params)) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" was not found!');
		$data = [
			'status' => ORDER_STATUS_REMOVED,
			'status_reason' => '',
			'timestamp' => fn1::now(),
		];
		if (!$db -> update(TABLE_ORDERS, $data, $bind_query, $bind_params)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(1, 'Order "' . $order_id . '" has been updated successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order cancel
if (isset($request_params["cancel_order"])){
	$order_id = fn1::toStrn($request_params["cancel_order"], true);
	$reason = fn1::toStrn($request_params["reason"], true);
	try {
		if ($order_id == '') throw new Exception('Invalid order reference!');
		if ($reason == '') throw new Exception('Kindly provide a cancel reason');
		$db = new Database();
		$bind_query = "WHERE `id` = ? AND `uid` = ? AND `status` IN ('0', '1')";
		$bind_params = [$order_id, $session_uid];
		if (($exists = $db -> queryExists(TABLE_ORDERS, $bind_query, $bind_params)) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" was not found!');
		$data = [
			'status' => ORDER_STATUS_CANCELLED,
			'status_reason' => $reason,
			'timestamp' => fn1::now(),
		];
		if (!$db -> update(TABLE_ORDERS, $data, $bind_query, $bind_params)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(1, 'Order "' . $order_id . '" has been updated successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order decline
if (isset($request_params["decline_order"])){
	$order_id = fn1::toStrn($request_params["decline_order"], true);
	$reason = fn1::toStrn($request_params["reason"], true);
	try {
		if ($order_id == '') throw new Exception('Invalid order reference!');
		if ($reason == '') throw new Exception('Kindly provide a decline reason');
		$db = new Database();
		if ($session_userlevel < 2) throw new Exception('Action reserved for vendor accounts only', -1);
		if (($vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `uid` = ?', [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$vendor) throw new Exception('Failed to fetch vendor details!');
		$bind_query = "WHERE `id` = ? AND `vendor_id` = ? AND `status` IN ('0', '1')";
		$bind_params = [$order_id, $vendor['id']];
		if (($exists = $db -> queryExists(TABLE_ORDERS, $bind_query, $bind_params)) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" was not found!');
		$data = [
			'status' => ORDER_STATUS_DECLINED,
			'status_reason' => $reason,
			'timestamp' => fn1::now(),
		];
		if (!$db -> update(TABLE_ORDERS, $data, $bind_query, $bind_params)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(1, 'Order "' . $order_id . '" has been updated successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order receive
if (isset($request_params["receive_order"])){
	$order_id = fn1::toStrn($request_params["receive_order"], true);
	try {
		if ($order_id == '') throw new Exception('Invalid order reference!');
		$db = new Database();
		if ($session_userlevel < 2) throw new Exception('Action reserved for vendor accounts only', -1);
		if (($vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `uid` = ?', [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$vendor) throw new Exception('Failed to fetch vendor details!');
		$bind_query = "WHERE `id` = ? AND `vendor_id` = ? AND `status` = '0'";
		$bind_params = [$order_id, $vendor['id']];
		if (($exists = $db -> queryExists(TABLE_ORDERS, $bind_query, $bind_params)) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" was not found!');
		$data = [
			'status' => ORDER_STATUS_RECEIVED,
			'status_reason' => '',
			'timestamp' => fn1::now(),
		];
		if (!$db -> update(TABLE_ORDERS, $data, $bind_query, $bind_params)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(1, 'Order "' . $order_id . '" has been updated successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order delivery
if (isset($request_params["deliver_order"])){
	$order_id = fn1::toStrn($request_params["deliver_order"], true);
	try {
		if ($order_id == '') throw new Exception('Invalid order reference!');
		$db = new Database();
		if ($session_userlevel < 2) throw new Exception('Action reserved for vendor accounts only', -1);
		if (($vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `uid` = ?', [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$vendor) throw new Exception('Failed to fetch vendor details!');
		$bind_query = "WHERE `id` = ? AND `vendor_id` = ? AND `status` = '2'";
		$bind_params = [$order_id, $vendor['id']];
		if (($exists = $db -> queryExists(TABLE_ORDERS, $bind_query, $bind_params)) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" was not found!');
		$data = [
			'status' => ORDER_STATUS_DELIVERED,
			'status_reason' => '',
			'timestamp' => fn1::now(),
		];
		if (!$db -> update(TABLE_ORDERS, $data, $bind_query, $bind_params)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(1, 'Order "' . $order_id . '" has been updated successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order overdue
if (isset($request_params["overdue_order"])){
	$order_id = fn1::toStrn($request_params["overdue_order"], true);
	try {
		if ($order_id == '') throw new Exception('Invalid order reference!');
		$db = new Database();
		$bind_query = "WHERE `id` = ? AND `uid` = ? AND `status`='2'";
		$bind_params = [$order_id, $session_uid];
		if (($exists = $db -> queryExists(TABLE_ORDERS, $bind_query, $bind_params)) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" was not found!');
		$data = [
			'overdue' => '1',
			'overdue_timestamp' => fn1::now(),
		];
		if (!$db -> update(TABLE_ORDERS, $data, $bind_query, $bind_params)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(1, 'Order "' . $order_id . '" has been updated successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order complete
if (isset($request_params["complete_order"])){
	$order_id = fn1::toStrn($request_params["complete_order"], true);
	try {
		if ($order_id == '') throw new Exception('Invalid order reference!');
		$db = new Database();
		$bind_query = "WHERE `id` = ? AND `uid` = ? AND `status` IN ('3', '5')";
		$bind_params = [$order_id, $session_uid];
		if (($exists = $db -> queryExists(TABLE_ORDERS, $bind_query, $bind_params)) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" was not found!');
		$data = [
			'status' => ORDER_STATUS_COMPLETE,
			'status_reason' => '',
			'timestamp' => fn1::now(),
		];
		if (!$db -> update(TABLE_ORDERS, $data, $bind_query, $bind_params)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(1, 'Order "' . $order_id . '" has been updated successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order archive
if (isset($request_params["archive_order"])){
	$order_id = fn1::toStrn($request_params["archive_order"], true);
	try {
		if ($order_id == '') throw new Exception('Invalid order reference!');
		$db = new Database();
		if ($session_userlevel < 2) throw new Exception('Action reserved for vendor accounts only', -1);
		if (($vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `uid` = ?', [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$vendor) throw new Exception('Failed to fetch vendor details!');
		$bind_query = "WHERE `id` = ? AND `vendor_id` = ? AND `status` = '4'";
		$bind_params = [$order_id, $vendor['id']];
		if (($exists = $db -> queryExists(TABLE_ORDERS, $bind_query, $bind_params)) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" was not found!');
		$data = [
			'status' => ORDER_STATUS_ARCHIVED,
			'status_reason' => '',
			'timestamp' => fn1::now(),
		];
		if (!$db -> update(TABLE_ORDERS, $data, $bind_query, $bind_params)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(1, 'Order "' . $order_id . '" has been updated successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order dispute
if (isset($request_params["dispute_order"])){
	$order_id = fn1::toStrn($request_params["dispute_order"], true);
	$reason = fn1::toStrn($request_params["reason"], true);
	try {
		if ($order_id == '') throw new Exception('Invalid order reference!');
		if ($reason == '') throw new Exception('Kindly provide a dispute reason');
		$db = new Database();
		$bind_query = "WHERE `id` = ? AND `uid` = ? AND `status` = '3'";
		$bind_params = [$order_id, $session_uid];
		if (($exists = $db -> queryExists(TABLE_ORDERS, $bind_query, $bind_params)) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" was not found!');
		$data = [
			'status' => ORDER_STATUS_DISPUTE,
			'status_reason' => $reason,
			'timestamp' => fn1::now(),
		];
		if (!$db -> update(TABLE_ORDERS, $data, $bind_query, $bind_params)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(1, 'Order "' . $order_id . '" has been updated successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order delete
if (isset($request_params["delete_order"])){
	$order_id = fn1::toStrn($request_params["delete_order"], true);
	$db = null;
	try {
		if ($order_id == '') throw new Exception('Invalid order reference!');
		if ($session_userlevel < 2) throw new Exception('Action reserved for vendor accounts only');
		$db = new Database();
		if (($vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `uid` = ?', [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$vendor) throw new Exception('Failed to fetch vendor details!');
		if (($exists = $db -> queryExists(TABLE_ORDERS, "WHERE `id` = ? AND `vendor_id` = ? AND `status` IN ('-1', '-2')", [$order_id, $vendor['id']])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" ready for delete was not found!');
		if (!$db -> delete(TABLE_ORDERS, 'id', "WHERE `id` = ? AND `vendor_id` = ? AND `status` IN ('-1', '-2')", [$order_id, $vendor['id']])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(1, 'Order "' . $order_id . '" has been deleted permanently!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on my review
if (isset($request_params["my_review"])){
	$order_id = fn1::toStrn($request_params["my_review"], true);
	$rating = (int) $request_params["rating"];
	$comment = fn1::toStrn($request_params["comment"], true);
	$db = null;
	try {
		if ($order_id === '') throw new Exception('Invalid order reference');
		if (!($rating >= 1 && $rating <= 5)) throw new Exception('Invalid review rating');
		if ($comment === '') throw new Exception('Invalid review comment');
		$db = new Database();
		if (($order = $db -> queryItem(TABLE_ORDERS, null, 'WHERE `id` = ? AND `uid` = ?', [$order_id, $session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (($user = $db -> queryItem(TABLE_USERS, null, 'WHERE `uid` = ?', [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$order) throw new Exception('Your order "' . $order_id . '" was not found!');
		if (!$user) throw new Exception('Failed to fetch user data!');
		$order_status = (int) $order['status'];
		if ($order_status !== ORDER_STATUS_COMPLETE) throw new Exception('This order is not complete and cannot be reviewed');
		if (($existing = $db -> queryItem(TABLE_REVIEWS, null, 'WHERE `order_id` = ? AND `uid` = ?', [$order_id, $session_uid])) === false) throw new Exception($db -> getErrorMessage());
		$id = is_array($existing) && array_key_exists('id', $existing) ? $existing['id'] : $db -> createToken(TABLE_REVIEWS, 'id');
		$data = [
			'id' => $id,
			'service_id' => $order['service_id'],
			'order_id' => $order_id,
			'event_id' => $order['event_id'],
			'uid' => $session_uid,
			'avatar' => $user['photo_url'],
			'name' => fn1::strtocamel($user['display_name'], true),
			'type' => 1,
			'rating' => $rating,
			'comment' => $comment,
			'timestamp' => fn1::now()
		];
		if ($existing){
			if (!$db -> update(TABLE_REVIEWS, $data, 'WHERE `id` = ?', [$id])) throw new Exception($db -> getErrorMessage());
		}
		else if (!$db -> insert(TABLE_REVIEWS, $data)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		$avatar = trim($data['avatar']);
		if (fn1::isFile(getUploads(true) . '/' . preg_replace('/^\/|\/$/s', '', $avatar))) $avatar = getUploads() . '/' . preg_replace('/^\/|\/$/s', '', $avatar);
		s::success([
			'id' => $data['id'],
			'avatar' => $avatar,
			'name' => $data['name'],
			'rating' => $data['rating'],
			'comment' => $data['comment'],
			'timestamp' => $data['timestamp'],
		], 'Review submitted successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//on order payment
if (isset($request_params["order_payment"])){
	$order_id = fn1::toStrn($request_params["order_payment"], true);
	$amount = (float) $request_params["amount"];
	$option = fn1::toStrx($request_params["option"], true);
	$data = fn1::toStrx($request_params["data"], true);
	$db = null;
	try {
		if ($order_id === '') throw new Exception('Invalid order reference');
		if ($amount <= 0) throw new Exception('Invalid payment amount');
		$db = new Database();
		if (($existing = $db -> queryItem(TABLE_ORDERS, null, 'WHERE `id` = ? AND `uid` = ?', [$order_id, $session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$existing) throw new Exception('Order does not exist!');
		if ((int) $existing['status'] >= ORDER_STATUS_PAID) throw new Exception('This order has already been paid');
		if ($option === 'card'){
			$data = fn1::jsonParse(base64_decode($data), true, null);
			if (!fn1::hasKeys($data, 'names', 'number', 'exp_month', 'exp_year', 'cvc')) throw new Exception('Invalid payment card data');
			$card_names = fn1::strtocamel($data['names'], true);
			$card_number = fn1::toStrn($data['number'], true);
			$card_exp_month = (int) $data['exp_month'];
			$card_exp_year = (int) $data['exp_year'];
			$card_cvc = fn1::toStrn($data['cvc'], true);
			$payment = process_card_payment($order_id, $amount, $card_names, $card_number, $card_exp_month, $card_exp_year, $card_cvc);
			if ($payment === false) throw new Exception($process_card_payment_error);
		}
		else throw new Exception('Unsupported payment option');
		$data = [
			'payment_id' => $payment['id'],
			'payment_ref' => $payment['ref'],
			'status' => ORDER_STATUS_PAID,
			'status_reason' => '',
			'timestamp' => $payment['timestamp'],
		];
		if (!$db -> update(TABLE_ORDERS, $data, 'WHERE `id` = ? AND `uid` = ?', [$order_id, $session_uid])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success($payment['id'], 'Payment processed successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//db connection
$db = new Database();

//clean up expired orders
$today = fn1::now('Y-m-d');
if (!$db -> query("DELETE FROM `tb_orders` WHERE `status` IN ('-3', '-2') AND ABS(DATEDIFF(`timestamp`, '$today')) > 7")) die ($db -> getErrorMessage());

#fetch details
$order = null;
$orders = [];
foreach($order_statuses as $key => $status) {
	$order_status_details[$status]['count'] = 0;
	$order_status_details[$status]['amount'] = 0;
}

try {
	switch ($orders_view){
		//fetch client orders
		case 'client':
		if (($vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `uid` = ?', [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$vendor) throw new Exception('Failed to fetch vendor details!');
		if (($result = $db -> select(TABLE_ORDERS, null, "WHERE `vendor_id` = ? AND `type` = '0' ORDER BY `_index` DESC", [$vendor['id']])) === false) throw new Exception($db -> getErrorMessage());
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				unset($row['_index']);
				$orders[] = $row;
			}
		}
		for ($i=0; $i < count($orders); $i++) { 
			$orders[$i]['client'] = $db -> queryItem(TABLE_USERS, 'display_name', 'WHERE `uid` = ?', $orders[$i]['uid']);
			$orders[$i]['due_date']['start_timestamp'] = "";
			if ($orders[$i]['event_id'] != "") {
				$orders[$i]['due_date'] = $db -> queryItem(TABLE_EVENTS, 'start_timestamp', 'WHERE `id` = ?', $orders[$i]['event_id']);
			}
			$order_status_details[$order_statuses[$orders[$i]['status']]]['count'] += 1;
			$order_status_details[$order_statuses[$orders[$i]['status']]]['amount'] += (int)$orders[$i]['pricing_price'];
		}
		
		break;

		//fetch client requests
		case 'client-requests':
		if (($result = $db -> select(TABLE_ORDERS, null, "WHERE `type` = '1' AND `status` = '0' ORDER BY `_index` DESC", [])) === false) throw new Exception($db -> getErrorMessage());
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				unset($row['_index']);
				$orders[] = $row;
			}
		}
		break;

		//fetch my orders
		case 'open':
		$tb_orders = $db -> escapeString(TABLE_ORDERS);
		$tb_events = $db -> escapeString(TABLE_EVENTS);
		if (($result = $db -> select(TABLE_ORDERS, ["`$tb_orders`.*", "(`$tb_events`.`name`) AS 'event_name'"], "LEFT JOIN `$tb_events` ON `$tb_events`.`id` = `$tb_orders`.`event_id` WHERE `$tb_orders`.`uid` = ? AND `$tb_orders`.`type` = '0' AND `$tb_orders`.`status` NOT IN ('-3', '-2', '4') ORDER BY `$tb_orders`.`_index` DESC", [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				unset($row['_index']);
				$orders[] = $row;
			}
		}
		break;

		//fetch my requests
		case 'requests':
		$tb_orders = $db -> escapeString(TABLE_ORDERS);
		$tb_events = $db -> escapeString(TABLE_EVENTS);
		if (($result = $db -> select(TABLE_ORDERS, ["`$tb_orders`.*", "(`$tb_events`.`name`) AS 'event_name'"], "LEFT JOIN `$tb_events` ON `$tb_events`.`id` = `$tb_orders`.`event_id` WHERE `$tb_orders`.`uid` = ? AND `$tb_orders`.`type` = '1' AND `$tb_orders`.`status` NOT IN ('-3', '-2', '4') ORDER BY `$tb_orders`.`_index` DESC", [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				unset($row['_index']);
				$orders[] = $row;
			}
		}
		break;

		//fetch orders history
		case 'history':
		$tb_orders = $db -> escapeString(TABLE_ORDERS);
		$tb_events = $db -> escapeString(TABLE_EVENTS);
		if (($result = $db -> select(TABLE_ORDERS, ["`$tb_orders`.*", "(`$tb_events`.`name`) AS 'event_name'"], "LEFT JOIN `$tb_events` ON `$tb_events`.`id` = `$tb_orders`.`event_id` WHERE `$tb_orders`.`uid` = ? AND `$tb_orders`.`type` = '0' AND `$tb_orders`.`status` IN ('4', '6') ORDER BY `$tb_orders`.`_index` DESC", [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				unset($row['_index']);
				$orders[] = $row;
			}
		}
		break;

		//fetch order details
		case 'order':
		$tb_client = $db -> escapeString(TABLE_USERS);
		$tb_orders = $db -> escapeString(TABLE_ORDERS);
		$tb_events = $db -> escapeString(TABLE_EVENTS);
		$tb_services = $db -> escapeString(TABLE_VENDOR_SERVICES);
		$tb_vendors = $db -> escapeString(TABLE_VENDORS);
		if ($order_id == '') throw new Exception('Invalid order reference', -3);
		if (($order = $db -> queryItem(TABLE_ORDERS, [
			"`$tb_orders`.*",
			"(`$tb_events`.`name`) AS 'event_name'",
			"(`$tb_services`.`image`) AS 'service_image'",
			"(`$tb_services`.`description`) AS 'service_description'",
			"(`$tb_vendors`.`uid`) AS 'vendor_uid'",
			"(`$tb_client`.`display_name`) AS 'client_name'",
		],
		"LEFT JOIN `$tb_events` ON `$tb_events`.`id` = `$tb_orders`.`event_id` "
		. "LEFT JOIN `$tb_services` ON `$tb_services`.`id` = `$tb_orders`.`service_id` "
		. "LEFT JOIN `$tb_vendors` ON `$tb_vendors`.`id` = `$tb_orders`.`vendor_id` "
		. "LEFT JOIN `$tb_client` ON `$tb_client`.`uid` = `$tb_orders`.`uid` "
		. "WHERE `$tb_orders`.`id` = ? AND `$tb_orders`.`type` = '0' AND `$tb_orders`.`status` NOT IN ('-3', '-2', '-1')",
		[$order_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$order) throw new Exception('Order not found!', -3);
		$order['pricing_package'] = fn1::jsonParse(xstrip($order['pricing_package']), true, []);
		$order = xstrip($order, true);
		unset($order['_index']);
		$tmp_image = fn1::toStrn($order['service_image'], true);
		$order['service_image_src'] = $tmp_image != '' ? getUploads() . '/' . $tmp_image : '';
		$order_rating = null;
		$is_user_order = $session_uid == $order['uid'];
		$is_vendor_order = $session_uid == $order['vendor_uid'];
		if (($order_rating = get_order_rating($order_id, $my_review)) === false) throw new Exception($get_order_rating_error);
		$order['rating'] = $order_rating;
		$order['my_review'] = $my_review;
		break;

		//fetch order details
		case 'request':
		$tb_orders = $db -> escapeString(TABLE_ORDERS);
		$tb_events = $db -> escapeString(TABLE_EVENTS);
		if ($order_id == '') throw new Exception('Invalid request reference', -3);
		if (($order = $db -> queryItem(TABLE_ORDERS, ["`$tb_orders`.*", "(`$tb_events`.`name`) AS 'event_name'"], "LEFT JOIN `$tb_events` ON `$tb_events`.`id` = `$tb_orders`.`event_id` WHERE `$tb_orders`.`id` = ? AND `$tb_orders`.`type` = '1' AND `$tb_orders`.`status` NOT IN ('-3', '-2', '-1', '4')", [$order_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$order) throw new Exception('Request not found!', -3);
		$order = xstrip($order, true);
		unset($order['_index']);
		break;
	}
}
catch (Exception $e){
	if ($db !== null) $db -> close();
	s::error(null, $e -> getMessage());
	if ($e -> getCode() == -3){
		header("location: $root/orders/?open");
		exit();
	}
}

//close connection
$db -> close();
$db = null;

#order stats
if (($order_stats = order_stats()) === false) s::error(null, $order_stats_error);


//helper functions
function order_set_status($status, $order_id, $reason="", $reason_required=false, $is_vendor=false, $set_data=null){
	global $order_set_status_error, $order_statuses, $session_uid, $session_userlevel;
	try {
		$status = (int) $status;
		$order_id = fn1::toStrn($order_id, true);
		$reason = fn1::toStrn($reason, true);
		if (!array_key_exists($status, $order_statuses)) throw new Exception('Invalid order reference!');
		if ($order_id == '') throw new Exception('Invalid order reference!');
		if ($reason_required && $reason == '') throw new Exception('Kindly provide a reason for your action');
		$db = new Database();
		$bind_query = 'WHERE `id` = ? AND `uid` = ?';
		$bind_params = [$order_id, $session_uid];
		if ($is_vendor){
			if ($session_userlevel < 2) throw new Exception('Action reserved for vendor accounts only', -1);
			if (($vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `uid` = ?', [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
			if (!$vendor) throw new Exception('Failed to fetch vendor details!');
			$bind_query = 'WHERE `id` = ? AND `vendor_id` = ?';
			$bind_params = [$order_id, $vendor['id']];
		}
		if (($exists = $db -> queryExists(TABLE_ORDERS, $bind_query, $bind_params)) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Order "' . $order_id . '" was not found!');
		$data = [
			'status' => $status,
			'status_reason' => $reason,
			'timestamp' => fn1::now(),
		];
		if (is_array($set_data) && !empty($set_data)) $data = array_replace_recursive($data, $set_data);
		if (!$db -> update(TABLE_ORDERS, $data, $bind_query, $bind_params)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		return 'Order "' . $order_id . '" has been updated successfully!';
	}
	catch (Exception $e){
		$order_set_status_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return $e -> getCode() == -1 ? null : false;
	}
}
function order_stats(){
	global $order_stats_error, $session_uid, $session_userlevel;
	$db = null;
	try {
		$db = new Database();
		$tb_orders = $db -> escapeString(TABLE_ORDERS);
		$uid = $db -> escapeString($session_uid);
		$queries = [];
		if ((int) $session_userlevel >= 2){
			if (($vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `uid` = ?', [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
			if (!$vendor) throw new Exception('Failed to fetch vendor details!');
			$vendor_id = $db -> escapeString($vendor['id']);
			$queries[] = "(SELECT COUNT(*) FROM `$tb_orders` WHERE `vendor_id` = '$vendor_id' AND `type` = '0' AND `status` NOT IN ('-3', '-1', '4')) AS 'client'";
			$queries[] = "(SELECT COUNT(*) FROM `$tb_orders` WHERE `type` = '1' AND `status` = '0') AS 'client-requests'";
		}
		else {
			$queries[] = "(SELECT 0) AS 'client'";
			$queries[] = "(SELECT 0) AS 'client-requests'";
		}
		$queries[] = "(SELECT COUNT(*) FROM `$tb_orders` WHERE `uid` = '$uid' AND `type` = '0' AND `status` NOT IN ('-3', '-2', '4')) AS 'open'";
		$queries[] = "(SELECT COUNT(*) FROM `$tb_orders` WHERE `uid` = '$uid' AND `type` = '1' AND `status` NOT IN ('-3', '-2', '4')) AS 'requests'";
		$queries[] = "(SELECT COUNT(*) FROM `$tb_orders` WHERE `uid` = '$uid' AND `type` = '0' AND `status` = '4') AS 'history'";
		$query = "SELECT " . implode(', ', $queries);
		if (!$result = $db -> query($query)) throw new Exception($db -> getErrorMessage());
		$stats = [
			'client' => 0,
			'client-requests' => 0,
			'open' => 0,
			'requests' => 0,
			'history' => 0,
		];
		if ($result instanceof mysqli_result && $row = $result -> fetch_array(MYSQLI_ASSOC)){
			$stats = [
				'client' => (int) $row['client'],
				'client-requests' => (int) $row['client-requests'],
				'open' => (int) $row['open'],
				'requests' => (int) $row['requests'],
				'history' => (int) $row['history'],
			];
		}
		$db -> close();
		$db = null;
		return $stats;
	}
	catch (Exception $e){
		$order_stats_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function get_order_rating($order_id, &$my_review=null){
	global $get_order_rating_error, $session_uid;
	$db = null;
	try {
		$db = new Database();
		if (!$result = $db -> select(TABLE_REVIEWS, null, 'WHERE `order_id` = ? AND `type` = "1" ORDER BY `_index` ASC', [$order_id])) throw new Exception($db -> getErrorMessage());
		$items = [];
		$rating_sum = 0;
		$my_review = null;
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				$avatar = trim($row['avatar']);
				$rating = (int) $row['rating'];
				$rating_sum += $rating;
				if (fn1::isFile(getUploads(true) . '/' . preg_replace('/^\/|\/$/s', '', $avatar))) $avatar = getUploads() . '/' . preg_replace('/^\/|\/$/s', '', $avatar);
				$item = [
					'id' => $row['id'],
					'avatar' => $avatar,
					'name' => trim($row['name']),
					'rating' => $rating,
					'comment' => trim($row['comment']),
					'timestamp' => $row['timestamp'],
				];
				if ($row['uid'] == $session_uid) $my_review = $item;
				$items[] = $item;
			}
		}
		$db -> close();
		$db = null;
		$count = count($items);
		$rating = $count > 0 ? round($rating_sum / $count, 1) : 0;
		return [
			'count' => $count,
			'rating' => $rating,
			'reviews' => $items,
		];
	}
	catch (Exception $e){
		$get_order_rating_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function process_card_payment($order_id, $amount, $card_names, $card_number, $card_exp_month, $card_exp_year, $card_cvc){
	global $save_payment_error, $process_card_payment_error;

	//demo payment process
	if ($card_number === '4500000000000000' && $card_exp_month === 10 && $card_exp_year === 22 && $card_cvc === '123'){
		$ref = 'DEMO_' . fn1::randomString(10, false);
		$data = [
			'names' => $card_names,
			'number' => $card_number,
			'exp_month' => $card_exp_month,
			'exp_year' => $card_exp_year,
			'cvc' => $card_cvc,
		];
		if (($saved = save_payment(PAYMENT_TYPE_CARD, $order_id, $amount, $ref, $data)) === false){
			$process_card_payment_error = $save_payment_error;
			return false;
		}
		return $saved;
	}

	$process_card_payment_error = 'Transaction failed due to internal error';
	return false;
}
function save_payment($type, $order_id, $amount, $ref=null, $data=null){
	global $save_payment_error, $payment_types, $session_uid;
	$db = null;
	try {
		$session_uid = fn1::toStrx($session_uid, true);
		$type = (int) $type;
		$order_id = fn1::toStrx($order_id, true);
		$amount = (float) $amount;
		$ref = fn1::toStrn($ref, true);
		if (!array_key_exists($type, $payment_types)) throw new Exception('Unsupported payment type');
		if ($amount <= 0) throw new Exception('Invalid payment amount');
		if ($order_id === '') throw new Exception('Invalid payment order reference');
		if ($session_uid === '') throw new Exception('Invalid payment user reference');
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_ORDERS, 'WHERE `id` = ? AND `uid` = ?', [$order_id, $session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Payment order not found');
		$data = [
			'id' => $db -> createToken(TABLE_PAYMENTS, 'id'),
			'uid' => $session_uid,
			'ref' => $ref,
			'order_id' => $order_id,
			'amount' => $amount,
			'data' => $data,
			'timestamp' => fn1::now(),
		];
		if (!$db -> insert(TABLE_PAYMENTS, $data)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		return $data;
	}
	catch (Exception $e){
		$save_payment_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}



#eof
