<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Plugin\User;
use Naicode\Server\Funcs as fn1;

//handle password recovery
if (isset($request_params["forgot_email"])){
	$forgot_email = $request_params["forgot_email"];
	if (!User::recoverPassword($forgot_email)) $forgot_error = User::$last_error;
	else {
		if (!isset($_SESSION)) session_start(); //TODO temp redirect
		$_SESSION["forgot_success"] = "An email containing password recovery information has been sent to \"$forgot_email\". Kindly check to continue.";
		header("location: " . NS_SITE . "/forgot");
		exit();
	}
}

//get success message from session
if (isset($_SESSION["forgot_success"])){
	$forgot_success = $_SESSION["forgot_success"];
	unset($_SESSION["forgot_success"]);
}
