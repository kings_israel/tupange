<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//cart add rest
if (isset($request_params["cart_add"])){
	$db = null;
	try {
		if (!sessionActive() || !isset($session_uid)) throw new Exception("Kindly login to add items to your cart", 0);
		if (!strlen($service_id = fn1::toStrx($request_params["cart_add"], true))) throw new Exception("No service reference in action", 1);
		$db = new Database();
		if (!$service = $db -> queryItem(TABLE_VENDOR_SERVICES, null, "WHERE `id` = ?", [$service_id])) throw new Exception("Service not found", 2);
		if (!$service_vendor = $db -> queryItem(TABLE_VENDORS, null, "WHERE `id` = ?", [$service['vendor_id']])) throw new Exception("Service vendor not found", 2);
		if ($session_uid == $service_vendor['uid']) throw new Exception('You cannot order your own services', 2);
		if (($existing = $db -> queryItem(TABLE_CART, null, "WHERE `uid` = ? AND `service_id` = ?", [$session_uid, $service_id])) === false) throw new Exception($db -> getErrorMessage(), 3);
		if ($existing && array_key_exists("id", $existing)){
			$db -> close();
			unset($existing["_index"]);
			unset($existing["uid"]);
			s::success($existing, "Item already exists in cart");
		}
		$data = [
			"id" => $db -> createToken(TABLE_CART, "id"),
			"uid" => $session_uid,
			"service_id" => $service_id,
			"timestamp" => fn1::now(),
		];
		if (!$db -> insert(TABLE_CART, $data)) throw new Exception($db -> getErrorMessage(), 4);
		$db -> close();
		unset($data["uid"]);
		s::success($data, "Item has been added to cart successfully");
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error($e -> getCode(), $e -> getMessage());
	}
}

//cart delete
if (isset($request_params["cart_delete"])){
	$db = null;
	try {
		if (!sessionActive() || !isset($session_uid)) throw new Exception("Kindly login to remove cart items", 0);
		if (!strlen($item_id = fn1::toStrx($request_params["cart_delete"], true))) throw new Exception("No cart item reference in action", 1);
		$db = new Database();
		if (!$db -> delete(TABLE_CART, "id", "WHERE `id` = ? AND `uid` = ?", [$item_id, $session_uid])) throw new Exception($db -> getErrorMessage(), 2);
		$db -> close();
		s::success(null, "Cart item has been deleted successfully");
	} catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error($e -> getCode(), $e -> getMessage());
	}
}

//cart checkout
