<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Plugin\User;
use Naicode\Server\Plugin\Files;
use Naicode\Server\Database;

const AVATAR_NAME_LENGTH = 10;

//update local user avatar image helper functions
function localAvatar($uid, $avatar){
	$uid = fn1::toStrx($uid, true);
	$avatar = fn1::toStrx($avatar, true);
	if (!fn1::isEmpty($uid) && !fn1::isEmpty($avatar)){
		if (preg_match('/^https?:/', $avatar)) $avatar = copyRemoteAvatar($uid, $avatar);
		if (!fn1::isEmpty($avatar)){
			$image_path = getUploads(true) . "/" . $avatar;
			return fn1::isFile($image_path) ? getUploads() . "/" . $avatar : $avatar;
		}
	}
	return "";
}
function copyRemoteAvatar($uid, $avatar){
	$image_copied = false;
	$image_type = exif_imagetype($avatar);
	$image_types = [1 => "gif", 2 => "jpg", 3 => "png"];
	if (array_key_exists($image_type, $image_types)){
		$ext = $image_types[$image_type];
		$uploads_folder = getUploads(true);
		$upload_src = "avatar-" . fn1::randomString(AVATAR_NAME_LENGTH) . "." . $ext;
		while (fn1::isFile($uploads_folder . "/" . $upload_src)) $upload_src = "avatar-" . fn1::randomString(AVATAR_NAME_LENGTH) . "." . $ext;
		$upload_path = $uploads_folder . "/" . $upload_src;
		$image_data = file_get_contents($avatar);
		if (!fn1::isEmpty($image_data)){
			$file_handler = fopen($upload_path, "w");
			$file_upload = fwrite($file_handler, $image_data);
			fclose($file_handler);
			if ($file_upload !== false && fn1::isFile($upload_path)){
				list($width, $height) = @getimagesize($upload_path);
				if ((int) $width > 100){
					if ((int) $width > 512){
						$files = new Files();
						$files -> resizeImage($upload_path, 512); //resize
					}
					$image_copied = true;
				}
				else fn1::fileDelete($upload_path);
			}
		}
	}
	return updatePhotoURL($uid, $image_copied ? $upload_src : "");
}
function updatePhotoURL($uid, $upload_src){
	$upload_src = fn1::toStrx($upload_src, true);
	$data = ["photo_url" => $upload_src];
	$db = new Database();
	$upload_update = $db -> update(TABLE_USERS, $data, "`uid` = ?", [$uid]);
	$db -> close();
	return !$upload_update ? "" : $upload_src;
}
