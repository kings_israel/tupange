<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//get cart badge count
function get_cart_badge_count(){
	global $session_uid;
	global $get_cart_badge_count_error;
	$db = null;
	try {
		$db = new Database();
		if (($count = $db -> queryCount(TABLE_CART, "WHERE `uid` = ?", [$session_uid])) ===  false) throw new Exception($db -> getErrorMessage());
		$db -> close();
		return (int) $count;
	} catch (Exception $e){
		if ($db !== null) $db -> close();
		$get_cart_badge_count_error = $e -> getMessage();
		return false;
	}
}

//get unread orders count
function get_unread_orders_count(){
	global $session_uid;
	global $vendor_id;
	global $get_unread_orders_count_error;
	$db = null;
	try {
		$db = new Database();
		if ($vendor_id && strlen($vendor_id)){
			$bind_query = "WHERE ((`uid` = ? AND `seen_user` = '') OR (`vendor_id` = ? AND `seen_vendor`='')) AND `status`='0'";
			$bind_values = [$session_uid, $vendor_id];
		}
		else {
			$bind_query = "WHERE `uid` = ? AND `seen_user`='' AND `status`='0'";
			$bind_values = [$session_uid];
		}
		if (($count = $db -> queryCount(TABLE_ORDERS, $bind_query, $bind_values)) ===  false) throw new Exception($db -> getErrorMessage());
		$db -> close();
		return (int) $count;
	} catch (Exception $e){
		if ($db !== null) $db -> close();
		$get_unread_orders_count_error = $e -> getMessage();
		return false;
	}
}

//get unread messages count
function get_unread_messages_count(){
	global $session_uid, $get_unread_messages_count_error;
	$db = null;
	try {
		$db = new Database();
		$query_count = $db -> queryCount(TABLE_MESSAGES, "WHERE `recipient` = ? AND `status` = '0'", [$session_uid]);
		if ($query_count === false) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		return (int) $query_count;
	}
	catch (Exception $e){
		$get_unread_messages_count_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
