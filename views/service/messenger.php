<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server\Mailer;
use Naicode\Server\Plugin\EmailBuilder;
use Naicode\Server as s;

//const & vars
const GUEST_STATUS_INVITED = 1;
const URL_SELF = NS_SITE . '/messenger';
const LOG_SENT = false;
const LOG_SENT_FILE = 'sent.log';
const LOG_ERROR = true;
if (!defined("NS_DEBUG_LOG_FILE")) define("NS_DEBUG_LOG_FILE", "debug.log");

//on self_email POST
if (isset($_POST['self_email']) && isset($_POST['from_names']) && isset($_POST['from_email']) && isset($_POST['names']) && isset($_POST['subject']) && isset($_POST['message'])){
	$email = fn1::toStrn($_POST['self_email'], true);
	$from_names = fn1::toStrn($_POST['from_names'], true);
	$from_email = fn1::toStrn($_POST['from_email'], true);
	$names = fn1::toStrn($_POST['names'], true);
	$subject = fn1::toStrn($_POST['subject'], true);
	$message = fn1::toStrn($_POST['message'], true);
	if (!send_email($from_names, $from_email, $names, $email, $subject, $message)){
		log_error('Error [send_email] ' . $send_email_error);
		log_sent('FAILED! send_email: ' . $names . ' <' . $email . '> ' . $subject);
		s::success(null, 'Email send failed!');
	}
	else {
		log_sent('SUCCESS! send_email: ' . $names . ' <' . $email . '> ' . $subject);
		s::success(null, 'Email sent successfully!');
	}
}

//send invitation
if (isset($request_params["send_invitation"])){
	$invite = fn1::toStrx($request_params['send_invitation'], true);
	$event_id = fn1::toStrx($request_params['event'], true);
	$names = fn1::toStrx($request_params['names'], true);
	$phone = fn1::toStrx($request_params['phone'], true);
	$email = fn1::toStrx($request_params['email'], true);
	$text = fn1::toStrx($request_params['text'], true);
	$barcode = fn1::toStrx($request_params['barcode'], true);
	$db = null;
	try {
		if (!(isset($session_uid) && strlen($session_uid))) throw new Exception('Invalid session!');
		if (($invite = base64_decode($invite)) === false || $invite == '') throw new Exception('Invalid invite data!');
		if ($event_id == '') throw new Exception('Invalid invite event reference');
		if ($names == '') throw new Exception('Invalid invite names');
		if ($text == '') throw new Exception('Invalid invite text');
		if ($phone == '' && !fn1::isEmail($email)) throw new Exception('Invalid invite contacts');
		if ($barcode == '') throw new Exception('Invalid invite barcode');

		//db connection
		$db = new Database();
		if (($existing = $db -> queryItem(TABLE_EVENT_GUESTS, null, 'WHERE `barcode` = ? AND `event_id` = ?', [$barcode, $event_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$existing) throw new Exception('Invite event guest not found!');

		$sent_platforms = [];

		//email invite
		if (fn1::isEmail($email)){
			$subject = 'Invitation - ' . $names;
			$message = template_invite($subject, $invite);
			if (!self_email('Invitations', 'no-reply@tupange.com', $names, $email, $subject, $message)) throw new Exception('Error sending email message');
			$data = [
				'status_email' => 'SENT',
				'status' => GUEST_STATUS_INVITED,
				'time_invited' => fn1::now(),
			];
			if (!$db -> update(TABLE_EVENT_GUESTS, $data, 'WHERE `id` = ? AND `event_id` = ?', [$existing['id'], $event_id])) throw new Exception($db -> getErrorMessage());
			$sent_platforms[] = 'email';
		}

		//send sms
		if ($phone != ''){
			//TODO: send sms
			$sent_platforms[] = 'phone (N/A)';
		}

		//TODO: send whatsapp
		//TODO: send facebook
		//TODO: send twitter

		//close connection
		$db -> close();
		$db = null;

		s::success(null, 'Invite sent! (' . implode(', ', $sent_platforms) . ')');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//send ticket
if (isset($request_params["send_ticket"])){
	$ticket = fn1::toStrx($request_params['send_ticket'], true);
	$event_id = fn1::toStrx($request_params['event'], true);
	$names = fn1::toStrx($request_params['names'], true);
	$phone = fn1::toStrx($request_params['phone'], true);
	$email = fn1::toStrx($request_params['email'], true);
	$text = fn1::toStrx($request_params['text'], true);
	$barcode = fn1::toStrx($request_params['barcode'], true);
	$db = null;
	try {
		if (!(isset($session_uid) && strlen($session_uid))) throw new Exception('Invalid session!');
		if (($ticket = base64_decode($ticket)) === false || $ticket == '') throw new Exception('Invalid ticket data!');
		if ($event_id == '') throw new Exception('Invalid ticket event reference');
		if ($names == '') throw new Exception('Invalid ticket names');
		if ($text == '') throw new Exception('Invalid ticket text');
		if ($phone == '' && !fn1::isEmail($email)) throw new Exception('Invalid ticket contacts');
		if ($barcode == '') throw new Exception('Invalid ticket barcode');

		//db connection
		$db = new Database();
		if (($existing = $db -> queryItem(TABLE_REGISTER, null, 'WHERE `barcode` = ? AND `event_id` = ?', [$barcode, $event_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$existing) throw new Exception('Ticket event guest not found!');

		$sent_platforms = [];

		//email ticket
		if (fn1::isEmail($email)){
			$subject = 'Ticket - ' . $names;
			$message = template_invite($subject, $ticket);
			if (!self_email('Tickets', 'no-reply@tupange.com', $names, $email, $subject, $message)) throw new Exception('Error sending email message');
			$data = [
				'sent' => 'SENT',
			];
			if (!$db -> update(TABLE_REGISTER, $data, 'WHERE `id` = ? AND `event_id` = ?', [$existing['id'], $event_id])) throw new Exception($db -> getErrorMessage());
			$sent_platforms[] = 'email';
		}

		//send sms
		if ($phone != ''){
			//TODO: send sms
			$sent_platforms[] = 'phone (N/A)';
		}

		//TODO: send whatsapp
		//TODO: send facebook
		//TODO: send twitter

		//close connection
		$db -> close();
		$db = null;

		s::success(null, 'Ticket sent! (' . implode(', ', $sent_platforms) . ')');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//functions
function template_invite($title, $html){
	$builder = new EmailBuilder($title, "#ddd");
	$builder -> pre(fn1::stripHTML($html));
	$container = $builder -> container();
	$body = $container -> grid(null, "center");
	if (NS_MAIL_TEMPLATE_LOGO != '') $body -> row() -> col_pad_center("background-color:#fff;") -> image_200x50(NS_MAIL_TEMPLATE_LOGO, "Logo", "#fff");
	$body -> row() -> col("font-size: 0px; line-height: 0px;", "", "", "20", "true");
	$content = $body -> row() -> col_white() -> html($html);
	$footer = $container -> grid(null, "center");
	$footer = $footer -> row() -> col_text_12();
	if (NS_MAIL_FOOTER_TEMPLATE != '') $footer -> html(NS_MAIL_FOOTER_TEMPLATE);
	if (NS_MAIL_UNSUBSCRIBE_TEMPLATE != '') $footer -> html(NS_MAIL_UNSUBSCRIBE_TEMPLATE);
	$email_html = $builder -> build(true);
	return $email_html;
}
function template_message($title, $html, $button_link='', $button_text=''){
	$builder = new EmailBuilder($title, "#ddd");
	$builder -> pre(fn1::stripHTML($html));
	$container = $builder -> container();
	$body = $container -> grid(null, "center");
	if (NS_MAIL_TEMPLATE_LOGO != '') $body -> row() -> col_pad_center("background-color:#fff;") -> image_200x50(NS_MAIL_TEMPLATE_LOGO, "Logo", "#fff");
	$body -> row() -> col("font-size: 0px; line-height: 0px;", "", "", "20", "true");
	$content = $body -> row() -> col_white() -> grid();
	$content_body = $content -> row() -> col_text_15();
	$content_body -> html($html);
	if ($button_link != '' && $button_text != '') $content_body -> buttonBlock($button_link, $button_text) -> html("<br>");
	$footer = $container -> grid(null, "center");
	$footer = $footer -> row() -> col_text_12();
	if (NS_MAIL_FOOTER_TEMPLATE != '') $footer -> html(NS_MAIL_FOOTER_TEMPLATE);
	if (NS_MAIL_UNSUBSCRIBE_TEMPLATE != '') $footer -> html(NS_MAIL_UNSUBSCRIBE_TEMPLATE);
	$email_html = $builder -> build(true);
	return $email_html;
}
function email_message($from_names, $from_email, $names, $email, $subject, $message, $button_link='', $button_text=''){
	$message = template_message($subject, $message, $button_link, $button_text);
	return self_email($from_names, $from_email, $names, $email, $subject, $message);
}
function self_email($from_names, $from_email, $names, $email, $subject, $message){
	try {
		$params = 'self_email=' . urlencode($email)
		. '&names=' . urlencode($names)
		. '&from_names=' . urlencode($from_names)
		. '&from_email=' . urlencode($from_email)
		. '&subject=' . urlencode($subject)
		. '&message=' . urlencode($message);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, URL_SELF);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		curl_exec($ch);
		curl_close($ch);
		return true;
	}
	catch (Exception $e){
		log_error('Error [self_email] ' . $e -> getMessage());
		return false;
	}
}
function send_email($from_names, $from_email, $names, $email, $subject, $message){
	global $send_email_error;
	$mailer = new Mailer(true);
	$mailer -> setFrom($from_email, $from_names);
	//$mailer -> addReplyTo($from_email, $from_names);
	$mailer -> addTo($email, $names);
	$mailer -> setSubject($subject);
	$mailer -> setBody($message);
	if (!$mailer -> send()){
		$send_email_error = $mailer -> getErrorMessage();
		return false;
	}
	return true;
}
function send_sms($phone, $text){
	global $send_sms_error;
	//TODO...
	$send_sms_error = 'SMS messaging not yet implemented.';
	return false;
}
function log_error($err){
	if (!LOG_ERROR) return;
	$file = fn1::logFile(NS_DEBUG_LOG_FILE);
	$log = fn1::now() . ': ' . fn1::toStrn($err, true);
	return fn1::fileWrite($file, $log, false, false);
}
function log_sent($msg){
	if (!LOG_SENT) return;
	$file = fn1::logFile(LOG_SENT_FILE);
	$log = fn1::now() . ': ' . fn1::toStrn($msg, true);
	return fn1::fileWrite($file, $log, false, false);
}



































#eof
