<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//add/remove favorite service
if (isset($request_params["toggle_like"])){
	$db = null;
	try {
		if (!sessionActive() || !isset($session_uid)) throw new Exception("Kindly login to add items to your favorites", 0);
		if (!strlen($service_id = fn1::toStrx($request_params["toggle_like"], true))) throw new Exception("No service reference in action", 1);
		//_index, id, uid, service_id, timestamp
		$db = new Database();
		if (!$db -> queryExists(TABLE_VENDOR_SERVICES, "WHERE `id` = ?", [$service_id])) throw new Exception("Service not found", 2);
		if (($exists = $db -> queryExists(TABLE_FAVORITES, "WHERE `uid` = ? AND `service_id` = ?", [$session_uid, $service_id])) === false) throw new Exception($db -> getErrorMessage(), 3);
		if ((int) $exists > 0 ){
			if (!$db -> delete(TABLE_FAVORITES, "id", "WHERE `uid` = ? AND `service_id` = ?", [$session_uid, $service_id])) throw new Exception($db -> getErrorMessage());
			$db -> close();
			s::success(null, "Item removed from favorites successfully");
		}
		$data = [
			"id" => $db -> createToken(TABLE_FAVORITES, "id"),
			"uid" => $session_uid,
			"service_id" => $service_id,
			"timestamp" => fn1::now(),
		];
		if (!$db -> insert(TABLE_FAVORITES, $data)) throw new Exception($db -> getErrorMessage(), 4);
		$db -> close();
		unset($data["uid"]);
		s::success($data, "Item has been added to favorites");
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error($e -> getCode(), $e -> getMessage());
	}
}
