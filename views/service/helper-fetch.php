<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;

//fetch items
function fetch_items($table, $bind_query, $bind_values, $fields=null, $htmlspecialchars=false, $map_fields=null, $omit_fields=null){
	global $fetch_items_error;
	$db = null;
	try {
		//db connection
		$db = new Database();

		//fetch items
		$result = $db -> select($table, $fields, $bind_query, $bind_values);
		if ($result === false) throw new Exception($db -> getErrorMessage());
		$items = [];
		if (!($result instanceof mysqli_result)) throw new Exception("Unexpected database result object. " . $db -> err);
		while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
			$row = xstrip($row, $htmlspecialchars);
			$item = [];
			foreach ($row as $key => $value){
				if (is_array($omit_fields) && in_array($key, $omit_fields)) continue;
				if (is_array($map_fields) && array_key_exists($key, $map_fields) && strlen($temp = fn1::toStrn($map_fields[$key], true))) $item[$temp] = $value;
				else $item[$key] = $value;
			}
			if (!fn1::isEmpty($item)) $items[] = $item;
		}

		//close connection & return items
		$db -> close();
		$db = null;
		return $items;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$fetch_items_error = $e -> getMessage();
		return false;
	}
}
