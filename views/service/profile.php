<?php

require_once __DIR__ . "/../../includes.php";

use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

// Switch profile
$session_uid = $_SESSION['uid'];
$db = new Database();

$user_info = $db->select(TABLE_USERS, ['uid', 'userlevel'], 'WHERE `uid` = ? ', [$session_uid]);

$user = [];

foreach (mysqli_fetch_assoc($user_info) as $item => $value) {
	$user[$item] = $value;
}

$user_id = $user['uid'];
$switch = (int) $user['userlevel'] == 2 ? 1 : 2;

if (!$db -> update(TABLE_USERS, ['userlevel' => $switch], "uid = ?", [$user_id])) throw new Exception($db -> getErrorMessage());
$db -> close();

$redirect_location  = '/dashboard?events';
header("location: $redirect_location");
exit();

// if (isset($request_params['user_id']) && isset($request_params['user_level'])) {
// 	$db = null;
// 	$switch = $request_params['user_level'] == 2 ? 1 : 2;
// 	try {
// 		$db = new Database();

// 		if (!$db -> update(TABLE_USERS, ['userlevel' => $switch], "uid = ?", [$user_id])) throw new Exception($db -> getErrorMessage());

// 		$db->close();
// 	} catch (Exception $e) {
// 		$error = $e -> getMessage();
// 		if ($db !== null) $db -> close();
// 		s::error(null, $error);
// 	}
// }

?>