<?php
#home page pre-process
//import ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-service.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;

//home page data
$featured_services = [];
$testimonials = [];

//connection
$db = null;
try {
	//db connection
	$db = new Database();

	//fetch featured services [TODO join with ratings]
	$tmp_sql = fn1::strbuild("SELECT * FROM `%s` WHERE `deleted` = '0' AND `paused` = '0' ORDER BY `_index` DESC, `sponsored` DESC,`featured` DESC,`timestamp` DESC LIMIT 16", TABLE_VENDOR_SERVICES);
	if (!$result = $db -> query($tmp_sql)) throw new Exception($db -> getErrorMessage());
	if ($result -> num_rows){
		$tmp_uploads_folder = getUploads();
		while ($row = $result -> fetch_array(MYSQLI_ASSOC)) $featured_services[] = service_info($row);
	}

	//fetch testimonials
	if (!$result = $db -> query(fn1::strbuild("SELECT * FROM `%s` ORDER BY `_index` DESC", TABLE_TESTIMONIALS))) throw new Exception($db -> getErrorMessage());
	while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
		$testimonials[] = [
			"names" => fn1::strtocamel($row["names"]),
			"avatar" => $root . "/" . $row["avatar"],
			"testimony" => $row["testimony"],
			"timestamp" => $row["timestamp"],
		];
	}

	//close connection
	$db -> close();
}
catch (Exception $e){
	//error handling
	if ($db !== null) $db -> close();
	s::text($e -> getMessage());
}

//featured services demo content
$featured_services_demo = [];
$featured_services_padding = 8 - count($featured_services);
if ($featured_services_padding > 0){
	$x = 0;
	for ($i = 0; $i < $featured_services_padding; $i ++){
		$x = $x == 5 ? 1 : ($x + 1);
		$featured_services_demo[] = [
			"id" => "demo-service-$i",
			"image" => "demo-$x.jpg",
			"image_src" => $root . "/assets/img/home/demo-$x.jpg",
			"title" => "Demo Service",
			"description" => "Proin vehicula auctor turpis, eget placerat ipsum laoreet ac.",
			"category_name" => "Demo Content",
			"location_name" => "Nairobi",
			"location_lat" => -1.27615,
			"location_lon" => 36.839269,
			"location_map_lat" => -1.27615,
			"location_map_lon" => 36.839269,
			"timestamp" => "2019-06-10 00:00:00",
			"sponsored" => rand(0,1000) % 2 == 0 ? 1 : 0,
			"featured" => 1,
			"demo" => true,
			"rating" => ["5.0","4.5","4.0","3.5","3.0"][rand(0,4)],
			"rating_count" => rand(200, 1500),
			"in_cart" => "",
			"in_favorites" => "",
		];
	}
	array_multisort(array_column($featured_services_demo, "sponsored"), SORT_DESC, $featured_services_demo);
}

//merge with demo
$featured_services = fn1::arraysMerge($featured_services, $featured_services_demo);
