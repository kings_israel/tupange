<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-service.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Plugin\Files;
use Naicode\Server\Database;
use Naicode\Server as s;

//get user profile data
$profile_data = get_user_profile_data($_SESSION['uid']);
// die(print_r($profile_data));
$photo_url = $profile_data['photo_url'];
$display_name = $profile_data['display_name'];
$email = $profile_data['email'];
$phone = $profile_data['phone'];
$description = $profile_data['description'];
$first_name = $profile_data['first_name'];
$last_name = $profile_data['last_name'];
$gender = $profile_data['gender'];
$age = $profile_data['age'];
$birth_day = $profile_data['birth_day'];
$birth_month = $profile_data['birth_month'];
$birth_year = $profile_data['birth_year'];
$address = $profile_data['address'];
$country = $profile_data['country'];
$region = $profile_data['region'];
$city = $profile_data['city'];
$zip = $profile_data['zip'];

//handle form submit
if (isset($request_params['display_name'])){
	$db = null;
	$conn = null;
	$uploaded_image = null;
	try {
		//get values
		$display_name = fn1::toStrn($request_params["display_name"], true);
		$first_name = fn1::toStrn($request_params["first_name"], true);
		$last_name = fn1::toStrn($request_params["last_name"], true);
		$description = fn1::toStrn($request_params["description"], true);
		$email = fn1::toStrn($request_params["email"], true);
		$gender = fn1::toStrn($request_params["gender"], true);
		$age = fn1::toStrn($request_params["age"], true);

		//validate values
		if (fn1::isEmpty($display_name)) throw new Exception("Kindly enter the display name");
		if (fn1::isEmpty($email)) throw new Exception("Kindly enter the display name");

		//save values with rollback enabled
		$data = [
			"display_name" => $display_name,
			"first_name" => $first_name,
			"last_name" => $last_name,
			"email" => $email,
			"phone" => $phone,
			"description" => $description,
			"gender" => $gender,
			"age" => $age,
			"timestamp" => fn1::now(),
		];

		//uploading display image
		if (isset($_FILES["profile_photo"]) && $_FILES["profile_photo"]["name"]){
			
			//check if image is too small
			// list($width, $height) = @getimagesize($_FILES["profile_photo"]['tmp_name']);
			// $width = (int) $width;
			// $height = (int) $height;
			// if ($width < 600 || $height < 300) throw new Exception("Image is too small (" . $width . "x" . $height . "). Kindly select a larger image with dimensions greater or equal to 600x400 for better quality.");
			// if ($width > 600){
			// 	$resized_height = round((((600 / $width) * 100) / 100) * $height, 0);
			// 	if ($resized_height < 300) throw new Exception("Display image is too wide for its height (" . $width . "x" . $height . " will be resized to 600x$resized_height). Kindly select another image with dimensions greater or equal to 600x400 for better quality.");
			// }

			//upload new service image
			$files = new Files("profile_photo");
			$files -> setAllowedExtensions(["gif","jpg", "jpeg", "png"]);
			$files -> setMaxLimit(5);
			if (!$files -> fileUpload()) throw new Exception($files -> getErrorMessage());
			$uploads_folder = getUploads(true);
			$uploaded_image_src = $files -> uploaded[0]["src"];
			$uploaded_image = $uploads_folder . "/" . $uploaded_image_src;

			//resize large image to 600 width
			if ($width > 600){
				if (!$files -> resizeImage($uploaded_image, 600)){
					fn1::fileDelete($uploaded_image);
					throw new Exception($files -> getErrorMessage());
				}
			}

			//remove previous service image
			if (isset($service_image_src)) fn1::fileDelete($uploads_folder . "/" . $service_image_src);

			//add new service image
			$data["photo_url"] = $uploaded_image_src;
		}
		
		//db connection without autocommit so we can rollback on error
		$db = new Database();
		$conn = $db -> resetValues() -> getConnection(false);

        if (!$db->update(TABLE_USERS, $data, "`uid` = ?", [$_SESSION['uid']])) throw new Exception($db -> err, $db -> errno);

		//commit db changes and close
		if (!$conn -> commit()) throw new Exception($conn -> error, $conn -> errno);
		$db -> close();
		$conn = null;
		$db = null;

        $redirect_location = $root . "/edit-profile";
        header("location: $redirect_location");
        exit();
	}
	catch (Exception $e){
		//error handling
		$service_error = $e -> getMessage();
		if ($uploaded_image !== null && fn1::isFile($uploaded_image)) fn1::fileDelete($uploaded_image);
		if ($conn !== null && !$conn -> rollback()) $service_error .= " (Rollback Failed: " . $conn -> error . ")";
		if ($db !== null) $db -> close();
	}
}

?>