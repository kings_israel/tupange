<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

const GUEST_STATUS_INVITED = 1;
const GUEST_STATUS_CONFIRMED = 2;
const GUEST_STATUS_MAYBE = 3;
const GUEST_STATUS_DECLINED = 4;
const GUEST_STATUSES = [
	GUEST_STATUS_CONFIRMED => "Confirmed",
	GUEST_STATUS_MAYBE => "Maybe",
	GUEST_STATUS_DECLINED => "Declined",
];
const ORDER_STATUS_COMPLETE = 4;

//check the invitation details
if (count($route_paths) === 2){
	$event_id = fn1::toStrn($route_paths[0], true);
	$barcode = fn1::toStrn($route_paths[1], true);
	$db = null;
	try {
		$db = new Database();
		if (($guest = $db -> queryItem(TABLE_EVENT_GUESTS, null, 'WHERE `event_id` = ? AND `barcode` = ?', [$event_id, $barcode])) === false) throw new Exception($db -> getErrorMessage());
		if (!$guest) throw new Exception('Invalid invitation link');
		if ((int) $guest['status'] < GUEST_STATUS_INVITED) throw new Exception('You must be invited to be able to RSVP to the event.');
		if (($event_data = $db -> queryItem(TABLE_EVENTS, null, 'WHERE `id` = ?', [$event_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$event_data) throw new Exception('Unable to get event details');
		$event = event_from_data($event_data, true, true, false);
		$guest_names = fn1::strtocamel($guest['first_name'] . ' ' . $guest['last_name'], true);
		$page_title = 'Tupange | RSVP - ' . $event['name'];
		$page_description = 'Tupange invited guest RSVP page for the event: ' . $event['name'] . ' (' . $event['type']['name'] . ')';
		$db -> close();
		$db = null;
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
	}
}
else $error = 'Invalid invitation link';

//handle confirmation
if (isset($request_params['confirm']) && isset($request_params['guests']) && isset($request_params['diet'])){
	$confirm = (int) $request_params['confirm'];
	$guests = (int) $request_params['guests'];
	$diet = fn1::toStrn($request_params['diet'], true);
	$db = null;
	try {
		if (isset($error)) throw new Exception($error);
		if (in_array($guest['status'], [2,3,4])) throw new Exception('You have already responded to the invite with: ' . GUEST_STATUSES[$guest['status']]);
		if (!in_array($confirm, [2,3,4])) throw new Exception('Invalid invitation response');
		if ($guests <= 0) $guests = 1;
		$data = ['status' => $confirm];
		if ($event['settings']['diet']) $data['diet'] = $diet;
		if ($event['settings']['extend']) $data['expected_guests'] = $guests;
		$data['time_confirmed'] = fn1::now();
		$db = new Database();
		if (!$db -> update(TABLE_EVENT_GUESTS, $data, 'WHERE `event_id` = ? AND `barcode` = ?', [$event_id, $barcode])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success(null, 'Invitation response has been updated successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//get reviews
if (isset($request_params['get_reviews'])){
	$db = null;
	try {
		if (isset($error)) throw new Exception($error);
		if (!$event['settings']['reviews']) throw new Exception('This event does not allow guest reviews');
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_ATTENDANCE, 'WHERE `event_id` = ? AND `barcode` = ?', [$event_id, $barcode])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('You must attend the event to be able to review it');
		$reviews = ['event' => [
			'title' => $event['name'] . ' (' . $event['type']['name'] . ')',
			'name' => $guest_names,
			'reviewed' => 0,
			'type' => 3,
			'rating' => 0,
			'comment' => '',
			'timestamp' => '',
		]];
		if (!$result = $db -> select(TABLE_REVIEWS, null, 'WHERE `event_id` = ? AND `barcode` = ? AND `type` IN ("2","3")', [$event_id, $barcode])) throw new Exception($db -> getErrorMessage());
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$type = (int) $row['type'];
				$review = [
					'title' => '',
					'name' => $guest_names,
					'reviewed' => 1,
					'type' => $type,
					'rating' => (int) $row['rating'],
					'comment' => $row['comment'],
					'timestamp' => $row['timestamp'],
				];
				if ($type == 2) $reviews[$row['order_id']] = $review;
				elseif ($type == 3){
					$review['title'] = $event['name'] . ' (' . $event['type']['name'] . ')';
					$reviews['event'] = $review;
				}
			}
		}
		if (!$result = $db -> select(TABLE_ORDERS, null, 'WHERE `event_id` = ? AND `status` = "4"', [$event_id])) throw new Exception($db -> getErrorMessage());
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$order_id = $row['id'];
				$order_name = $row['service_category'] . ' (' . $row['service_title'] . ' - ' . $row['vendor_company'] . ')';
				if (array_key_exists($order_id, $reviews)) $reviews[$order_id]['title'] = $order_name;
				else {
					$review = [
						'title' => $order_name,
						'name' => $guest_names,
						'reviewed' => 0,
						'type' => 2,
						'rating' => 0,
						'comment' => '',
						'timestamp' => '',
					];
					$reviews[$order_id] = $review;
				}
			}
		}
		$db -> close();
		$db = null;
		s::success($reviews, 'Event guest reviews');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//save review
if (isset($request_params['save_review']) && isset($request_params['rating']) && isset($request_params['comment'])){
	$order_id = fn1::toStrn($request_params['save_review'], true);
	$rating = (int) $request_params['rating'];
	$comment = fn1::toStrn($request_params['comment'], true);
	$db = null;
	try {
		if (isset($error)) throw new Exception($error);
		if (!$event['settings']['reviews']) throw new Exception('This event does not allow guest reviews');
		if (!($rating >= 1 && $rating <= 5)) throw new Exception('Invalid review rating: ' . $rating);
		if ($comment === '') throw new Exception('Kindly type your review comment');
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_REVIEWS, 'WHERE `event_id` = ? AND `barcode` = ? AND `order_id` = ?', [$event_id, $barcode, $order_id])) === false) throw new Exception($db -> getErrorMessage());
		if ($exists) throw new Exception('You have already reviewed this');
		if ($order_id !== 'event'){
			if (($order = $db -> queryItem(TABLE_ORDERS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $order_id])) === false) throw new Exception($db -> getErrorMessage());
			if (!$order) throw new Exception('Unable to find review order details');
			$data = [
				'id' => $db -> createToken(TABLE_REVIEWS, 'id'),
				'service_id' => $order['service_id'],
				'order_id' => $order_id,
				'event_id' => $event_id,
				'barcode' => $barcode,
				'uid' => '',
				'avatar' => '',
				'name' => $guest_names,
				'type' => 2,
				'rating' => $rating,
				'comment' => $comment,
				'timestamp' => fn1::now(),
			];
		}
		else {
			$data = [
				'id' => $db -> createToken(TABLE_REVIEWS, 'id'),
				'service_id' => '',
				'order_id' => $order_id,
				'event_id' => $event_id,
				'barcode' => $barcode,
				'uid' => '',
				'avatar' => '',
				'name' => $guest_names,
				'type' => 3,
				'rating' => $rating,
				'comment' => $comment,
				'timestamp' => fn1::now(),
			];
		}
		if (!$db -> insert(TABLE_REVIEWS, $data)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success($data, 'Review has been saved successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}
