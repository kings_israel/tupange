<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-db-insert.php";
require_once __DIR__ . "/helper-db-update.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Plugin\Files;
use Naicode\Server\Database;
use Naicode\Server as s;

//some globals
const EVENT_ID_LEN = 10;
const EVENT_STATUSES = [1, 2];

//redirect invalid session uid
if (!(isset($session_uid) && strlen($session_uid))){
	header("location: $root/dashboard");
	exit();
}

//load existing event
if (isset($event_id)){
	if (strlen($event_id = fn1::toStrx($event_id, true)) < EVENT_ID_LEN){
		//redirect invalid event_id
		header("location: $root/dashboard?events");
		exit();
	}

	//existing event data
	if (!$event_data = fetch_event($event_id, $session_uid)) s::error(null, isset($fetch_event_error) ? $fetch_event_error : "Event not found");

	//check guest user limit
	if ($event_data['guest'] && !in_array($event_data['role'], [0])){
		header("location: $root/dashboard?events");
		exit();
	}

	$event_data = xstrip($event_data); //clean up result
	$event_name = $event_data["name"];
	$event_type_name = $event_data["type_name"];
	$event_type_value = (int) $event_data["type_value"];
	$event_type_other = $event_data["type_other"];
	$temp_start = new DateTime(date("Y-m-d H:i:s", strtotime($event_data["start_timestamp"])));
	$temp_end = new DateTime(date("Y-m-d H:i:s", strtotime($event_data["end_timestamp"])));
	$event_start_date = $temp_start -> format("d/m/Y");
	$event_start_time = $temp_start -> format("h:i A");
	$event_end_date = $temp_end -> format("d/m/Y");
	$event_end_time = $temp_end -> format("h:i A");
	$event_description = $event_data["description"];
	if (!fn1::isEmpty($event_data["location_map_lat"]) && !fn1::isEmpty($event_data["location_map_lon"])){
		$location_map_lat = (float) $event_data["location_map_lat"];
		$location_map_lon = (float) $event_data["location_map_lon"];
		$location_map_type = $event_data["location_map_type"];
		$location_map_name = $event_data["location_map_name"];
		$location_map_value = str_replace("\"", "'", fn1::jsonCreate(["lat" => $location_map_lat, "lng" => $location_map_lon, "type" => $location_map_type, "name" => $location_map_name]));
	}
	$event_settings = fn1::toArray($event_data["settings"]);
	if (array_key_exists('guests_expected', $event_settings)) $event_guests_expected = (int) $event_settings['guests_expected'];
	if (array_key_exists('guests_max', $event_settings)) $event_guests_max = (int) $event_settings['guests_max'];
	if (!fn1::isEmpty(($event_poster_src = fn1::toStrx($event_data["poster"], true))) && fn1::isFile(getUploads(true) . "/" . $event_poster_src)) $event_poster = getUploads() . "/" . $event_poster_src;
	else unset($event_poster_src);
}

//handle event form submission
if (isset($request_params["event_name"]) && isset($request_params["event_type"])){
	//get values
	$event_name = fn1::toStrn($request_params["event_name"], true);
	$event_type = fn1::toArray($request_params["event_type"]);
	if (array_key_exists("name", $event_type) && array_key_exists("value", $event_type)){
		$event_type_name = fn1::toStrx($event_type["name"]);
		$event_type_value = (int) $event_type["value"];
	}
	$event_type_other = fn1::toStrn($request_params["event_type_other"], true);
	$event_start_date = fn1::toStrx($request_params["start_date"], true);
	$event_start_time = fn1::toStrx($request_params["start_time"], true);
	$event_end_date = fn1::toStrx($request_params["end_date"], true);
	$event_end_time = fn1::toStrx($request_params["end_time"], true);
	$event_description = fn1::toStrn($request_params["description"], true);
	$location_map = fn1::toArray($request_params["location_map"]);
	$location_map_lat = 0;
	$location_map_lon = 0;
	$location_map_type = 'Undefined';
	$location_map_name = 'Undefined';
	if (array_key_exists("lat", $location_map) && array_key_exists("lng", $location_map)){
		$location_map_lat = (float) $location_map["lat"];
		$location_map_lon = (float) $location_map["lng"];
		$location_map_type = array_key_exists("type", $location_map) ? fn1::toStrn($location_map["type"], true) : "UNDEFINED";
		$location_map_name = array_key_exists("name", $location_map) ? fn1::toStrn($location_map["name"], true) : "Undefined";
	}
	$event_guests_expected = fn1::toStrn($request_params["guests_expected"], true);
	$event_guests_max = fn1::toStrn($request_params["guests_max"], true);

	//default event settings
	$event_settings = [];
	$event_settings["last_name"] = 1;
	$event_settings["phone"] = 1;
	$event_settings["email"] = 1;
	$event_settings["address"] = 1;
	$event_settings["company"] = 1;
	$event_settings["table"] = 1;
	$event_settings["diet"] = 1;
	$event_settings["type"] = 1;
	$event_settings["rsvp"] = 1;
	$event_settings["reviews"] = 1;
	$event_settings["extend"] = 1;
	$event_settings["invite_email"] = 1;
	$event_settings["invite_sms"] = 1;
	$event_settings["invite_whatsapp"] = 1;
	$event_settings["invite_facebook"] = 1;
	$event_settings["invite_twitter"] = 1;
	$event_settings["pass_question"] = 1;

	//get event groups
	$event_guest_groups = [];

	//validate and save
	$db = null;
	$conn = null;
	$uploaded_image = null;
	try {
		//validation
		if (fn1::isEmpty($event_name)) throw new Exception("Invalid event name!");
		if (!(isset($event_type_name) && isset($event_type_value))) throw new Exception("Invalid event type selection!");
		if (!$event_type_value && $event_type_other == '') throw new Exception("Invalid event type \"other\" suggestion!");

		$temp_start = fn1::strbuild("%s %s", $event_start_date, $event_start_time);
		$start_timestamp = DateTime::createFromFormat("d/m/Y h:i A", $temp_start);
		if (!$start_timestamp || fn1::strbuild("%s %s", $start_timestamp -> format("d/m/Y"), $start_timestamp -> format("h:i A")) != $temp_start) throw new Exception("Unexpected event start date-time format \"$temp_start\"");
		$start_timestamp = $start_timestamp -> format("Y-m-d H:i:s");

		$temp_end = fn1::strbuild("%s %s", $event_end_date, $event_end_time);
		$end_timestamp = DateTime::createFromFormat("d/m/Y h:i A", $temp_end);
		if (!$end_timestamp || fn1::strbuild("%s %s", $end_timestamp -> format("d/m/Y"), $end_timestamp -> format("h:i A")) != $temp_end) throw new Exception("Unexpected event end date-time format \"$temp_end\"");
		$end_timestamp = $end_timestamp -> format("Y-m-d H:i:s");

		if ((int) $event_guests_expected < 0) throw new Exception("Invalid expected guests value");
		if ((int) $event_guests_max < 0) throw new Exception("Invalid maximum guests value");

		//more settings
		$event_settings["guests_expected"] = (int) $event_guests_expected;
		$event_settings["guests_max"] = (int) $event_guests_max;
		$event_settings["scan_mode"] = 1;

		//uploading event poster
		$event_poster_keep = (int) $request_params["poster_keep"];
		$event_poster = fn1::toStrn($request_params['poster_data'], true);
		if (!$event_poster_keep && $event_poster != ''){
			//upload base64 image
			list($type, $data) = explode(';', $event_poster);
			if ($type != 'data:image/png') throw new Exception('Invalid poster image data');
			list(, $data) = explode(',', $data);
			$data = base64_decode($data);
			$uploaded_image_src = "event_poster_" . fn1::randomString(10) . '.png';
			$uploaded_image = getUploads(true) . '/' . $uploaded_image_src;
			file_put_contents($uploaded_image, $data);
			if (!file_exists($uploaded_image)) throw new Exception('Unable to upload event poster');
		}

		//remove event poster
		if (!$event_poster_keep && isset($event_poster_src)) fn1::fileDelete(getUploads(true) . "/" . $event_poster_src);

		//last modified timestamp
		$event_last_modified = fn1::now();

		//prepare event data
		$data = [
			"status" => 1,
			"name" => $event_name,
			"type_name" => $event_type_name,
			"type_value" => (int) $event_type_value,
			'type_other' => $event_type_other,
			"start_timestamp" => $start_timestamp,
			"end_timestamp" => $end_timestamp,
			"description" => $event_description,
			"location" => 0,
			"location_map" => 1,
			"timestamp" => $event_last_modified,
			"settings" => $event_settings,
			"guest_groups" => $event_guest_groups,
			"location_name" => $location_map_name,
			"location_type" => $location_map_type,
			"location_lat" => 0,
			"location_lon" => 0,
			"location_map_lat" => $location_map_lat,
			"location_map_lon" => $location_map_lon,
			"location_map_type" => $location_map_type,
			"location_map_name" => $location_map_name,
		];
		if (!$event_poster_keep && isset($uploaded_image_src)) $data["poster"] = $uploaded_image_src;
		else if (!$event_poster_keep) $data["poster"] = "";

		//delete event map image if it exists
		if (isset($event_id) && strlen($event_id)){
			$event_map_image = getUploads(true) . "/event-map-" . trim($event_id) . ".png";
			if (fn1::isFile($event_map_image)) fn1::fileDelete($event_map_image);
		}

		//db connection
		$db = new Database();
		$conn = $db -> resetValues() -> getConnection(false);

		//save event data
		if (!isset($event_id)){
			//duplicate event check
			$duplicate_event = $db -> queryItem(TABLE_EVENTS, null, "`uid` = ? AND `status` = ? AND `name` = ? AND `type_name` = ? AND `type_other` = ? AND `start_timestamp` = ? AND `end_timestamp` = ?", [$session_uid, 1, $event_name, $event_type_name, $event_type_other, $start_timestamp, $end_timestamp]);
			if ($duplicate_event === false) throw new Exception($db -> getErrorMessage());
			if ($duplicate_event){
				unset($duplicate_event);
				throw new Exception("A duplicate event with the same Name, Type & Time already exists. Kindly change any of these details or edit the existing event.");
			}

			//save new event
			$event_id = $db -> createToken(TABLE_EVENTS, "id", EVENT_ID_LEN);
			$data["id"] = $event_id;
			$data["uid"] = $session_uid;
			if (!db_insert($db, $conn, TABLE_EVENTS, $data)) throw new Exception($db -> err, $db -> errno);
		}
		elseif (!db_update($db, $conn, TABLE_EVENTS, $data, "`id` = ? AND `uid` = ?", [$event_id, $session_uid])) throw new Exception($db -> err, $db -> errno);

		//commit db changes and close
		if (!$conn -> commit()) throw new Exception($conn -> error, $conn -> errno);
		$db -> close();
		$uploaded_image = null;
		$conn = null;
		$db = null;

		//successful redirect to event dashboard
		s::success($data, $event_id);
	}
	catch (Exception $e){
		$event_error = $e -> getMessage();
		if ($uploaded_image !== null && fn1::isFile($uploaded_image)) fn1::fileDelete($uploaded_image);
		if ($conn !== null && !$conn -> rollback()) $event_error .= " (Rollback Failed: " . $conn -> error . ")";
		if ($db !== null) $db -> close();
		s::error(null, $event_error);
	}
}
