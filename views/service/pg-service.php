<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-service.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//get service data
$service_data = get_service_data($route_child);
$service = $service_data["service"];
$vendor = $service_data["vendor"];
$service_pricing = $service_data["pricing"];
$service_rating = $service_data["rating"];
$service_gallery = $service_data["gallery"];
$vendor_categories = fn1::toArray(fn1::propval($service_data, "vendor", "categories"));
$vendor_services = fn1::toArray(fn1::propval($service_data, "vendor", "services"));
$related_services = $service_data["related"];
$other_services = fn1::arraysMerge($vendor_services, $related_services);

//service vars
$service_title = $service["title"];
$service_description = $service["description"];
$service_image = $service["image_src"];

//check if editable
$edit_service = false;
if (sessionActive() && isset($session_userlevel) && $session_userlevel >= 2){
	$load_vendor_data = true;
	include_once __DIR__ . "/session.php";
	if ($vendor["id"] === $vendor_data["id"]) $edit_service = true;
}

//handle pricing item submit
if ($edit_service && isset($request_params["pricing-form"])){
	$service_id = fn1::toStrx($service["id"], true);
	$vendor_id = fn1::toStrx($service["vendor_id"], true);
	$title = fn1::toStrn(fn1::propval($request_params, "pricing-title"), true);
	$price = fn1::toNum(fn1::propval($request_params, "pricing-price"));
	$description = fn1::toStrn(fn1::propval($request_params, "pricing-description"), true);
	$packages = fn1::toArray(fn1::propval($request_params, "pricing-package"));

	$db = null;
	try {
		//validate
		if (!strlen($service_id)) throw new Exception("Invalid service reference.");
		if (!strlen($vendor_id)) throw new Exception("Invalid vendor reference.");
		if (!strlen($title)) throw new Exception("Kindly enter a valid title for your pricing item.");
		if ($price <= 0) throw new Exception("Kindly enter a valid price for your pricing item.");
		if (!strlen($description)) throw new Exception("Kindly enter a valid description for your pricing item.");
		$tmp_packages = [];
		foreach ($packages as $package){
			$package = fn1::toStrn($package, true);
			if (strlen($package)) $tmp_packages[] = $package;
		}

		//save item
		$db = new Database();
		$data = [
			"id" => $db -> createToken(TABLE_VENDOR_SERVICE_PRICING, "id"),
			"service_id" => $service_id,
			"vendor_id" => $vendor_id,
			"title" => $title,
			"price" => $price,
			"description" => $description,
			"package" => $tmp_packages,
		];
		if (!$db -> insert(TABLE_VENDOR_SERVICE_PRICING, $data)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		s::success($data, "Service pricing details saved successfully!");
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//handle edit pricing item submit
if ($edit_service && isset($request_params["pricing-edit-form"])){
	$id = fn1::toStrn($request_params["id"], true);
	$title = fn1::toStrn(fn1::propval($request_params, "pricing-edit-title"), true);
	$price = fn1::toNum(fn1::propval($request_params, "pricing-edit-price"));
	$description = fn1::toStrn(fn1::propval($request_params, "pricing-edit-description"), true);
	$packages = fn1::toArray(fn1::propval($request_params, "pricing-edit-package"));


	$db = null;
	try {
		//validate
		if (!strlen($id)) throw new Exception("Invalid service reference.");
		if (!strlen($title)) throw new Exception("Kindly enter a valid title for your pricing item.");
		if ($price <= 0) throw new Exception("Kindly enter a valid price for your pricing item.");
		if (!strlen($description)) throw new Exception("Kindly enter a valid description for your pricing item.");
		$tmp_packages = [];
		foreach ($packages as $package){
			$package = fn1::toStrn($package, true);
			if (strlen($package)) $tmp_packages[] = $package;
		}

		//save item
		$db = new Database();
		$data = [
			"title" => $title,
			"price" => $price,
			"description" => $description,
			"package" => $tmp_packages,
		];
		if (!$db -> update(TABLE_VENDOR_SERVICE_PRICING, $data, "id = ?", $id)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		s::success($data, "Service pricing details saved successfully!");
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//handle pricing item delete
if ($edit_service && isset($request_params["pricing-delete"])){
	$service_id = fn1::toStrx($service["id"], true);
	$vendor_id = fn1::toStrx($service["vendor_id"], true);
	$item_id = fn1::toStrn(fn1::propval($request_params, "pricing-delete"), true);

	$db = null;
	try {
		//validate
		if (!strlen($service_id)) throw new Exception("Invalid service reference.");
		if (!strlen($vendor_id)) throw new Exception("Invalid vendor reference.");
		if (!strlen($item_id)) throw new Exception("Invalid service pricing item reference.");

		//db connection
		$db = new Database();

		//check existing
		$existing = $db -> queryItem(TABLE_VENDOR_SERVICE_PRICING, null, "WHERE `id` = ? AND `service_id` = ? AND `vendor_id` = ?", [$item_id, $service_id, $vendor_id]);
		if ($existing === false) throw new Exception($db -> getErrorMessage());
		if (!(is_array($existing) && array_key_exists("id", $existing))) throw new Exception("Service pricing item was not found.");

		//delete item
		if (!$db -> delete(TABLE_VENDOR_SERVICE_PRICING, "id", "WHERE `id` = ? AND `service_id` = ? AND `vendor_id` = ?", [$item_id, $service_id, $vendor_id])) throw new Exception($db -> getErrorMessage());

		//close connection
		$db -> close();

		//output
		s::success($item_id, "Service pricing item has been deleted successfully!");
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//handle service gallery image upload
if ($edit_service && isset($request_params['gallery_upload'])){
	$service_id = fn1::toStrx($service["id"], true);
	$image = fn1::toStrn($request_params['gallery_upload'], true);
	$caption = fn1::toStrn($request_params['caption'], true);
	$db = null;
	try {
		if ($image === '') throw new Exception('Invalid image data');
		$db = new Database();
		$data = [
			'id' => $db -> createToken(TABLE_SERVICE_GALLERY, 'id'),
			'service_id' => $service_id,
			'image' => $image,
			'caption' => $caption,
			'timestamp' => fn1::now(),
		];
		if (!$db -> insert(TABLE_SERVICE_GALLERY, $data)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success($data, 'Image uploaded successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}
if ($edit_service && isset($request_params['gallery_delete'])){
	$service_id = fn1::toStrx($service["id"], true);
	$image_id = fn1::toStrn($request_params['gallery_delete'], true);
	$db = null;
	try {
		if ($image_id === '') throw new Exception('Invalid image reference');
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_SERVICE_GALLERY, 'WHERE `service_id` = ? AND `id` = ?', [$service_id, $image_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Image not found!');
		if (!$db -> delete(TABLE_SERVICE_GALLERY, 'id', 'WHERE `service_id` = ? AND `id` = ?', [$service_id, $image_id])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success($image_id, 'Image deleted successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}

}


//page variables
$page_title = "Tupange | $service_title";
$page_description = $service_title . " | " . $service_description;

#fn1::printr([$vendor]); exit(); //test
