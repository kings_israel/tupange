<?php
//include ns server
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-local-avatar.php";
use Naicode\Server as s;
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Plugin\User;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;

//constants
const SERVER_EXCEPTION = 4536;

/*
* callback urls
* Facebook
*	NS_SITE . "/oauth/?provider=Facebook&action=login";
*	NS_SITE . "/oauth/?provider=Facebook&action=signup&account=client";
*	NS_SITE . "/oauth/?provider=Facebook&action=signup&account=vendor";
* Twitter
*	NS_SITE . "/oauth/?provider=Twitter&action=login";
*	NS_SITE . "/oauth/?provider=Twitter&action=signup&account=client";
*	NS_SITE . "/oauth/?provider=Twitter&action=signup&account=vendor";
* Google
*	NS_SITE . "/oauth/?provider=Google&action=login";
*	NS_SITE . "/oauth/?provider=Google&action=signup&account=client";
*	NS_SITE . "/oauth/?provider=Google&action=signup&account=vendor";
*/

//social authentication
if (isset($_GET["provider"]) && isset($_GET["action"])){
	$provider = fn1::strtocamel(fn1::toStrx($_GET["provider"], true));
	$action = strtolower(fn1::toStrx($_GET["action"], true));
	$account = isset($_GET["account"]) ? strtolower(fn1::toStrx($_GET["account"], true)) : "";
	$oauth_success = false;
	$oauth_message = "Default OAuth message";
	try {
		if (!in_array($provider, ["Facebook", "Twitter", "Google"])) throw new Exception("Unsupported authentication provider!", SERVER_EXCEPTION);
		if (!in_array($action, ["login", "signup"])) throw new Exception("Unsupported authentication action!", SERVER_EXCEPTION);
		if ($action == "signup" && !in_array($account, ["client", "vendor"])) throw new Exception("Unsupported signup account type!", SERVER_EXCEPTION);
		if ($action == "signup") $callback = NS_SITE . "/oauth/?provider=$provider&action=$action&account=$account";
		else $callback = NS_SITE . "/oauth/?provider=$provider&action=login";
		$config = [
			"callback" => $callback,
			"providers" => [
				"Facebook" => [
					"enabled" => true,
					"keys" => [
						"id" => "2037165133048080",
						"secret" => "10e10449e42a9c2a6d9ba7e3fe0d6740"
					],
				],
				"Twitter" => [
					"enabled" => true,
					"keys" => [
						"key" => "vTKAk8jqVulResNUbqgz65P9G",
						"secret" => "DkhqQL7joFGOHC4LDlfUxyPXRxfDMtWLBMQlYs0eN9YUsRtaUh"
					],
				],
				"Google" => [
					"enabled" => true,
					"keys" => [
						"id" => "980354434499-abkf1e90vb14ibdmebg8vqrqtqql5rc6.apps.googleusercontent.com",
						"secret" => "UiE23o6WlZQHi47s7KQd7cxY"
					],
				]
			]
		];
		$hybridauth = new HybridAuth($config);
		$adapter = null;
		if ($provider == "Facebook") $adapter = $hybridauth -> authenticate("Facebook");
		if ($provider == "Twitter") $adapter = $hybridauth -> authenticate("Twitter");
		if ($provider == "Google") $adapter = $hybridauth -> authenticate("Google");
		if ($adapter === null) throw new Exception("HybridAuth adapter error!", SERVER_EXCEPTION);
		$tokens = $adapter -> getAccessToken();
		$user_profile = $adapter -> getUserProfile();
		$adapter -> disconnect();
		$identifier = "id_" . fn1::propval($user_profile, "identifier");
		if (($user = User::getIdentifierUser($identifier, $provider)) === false) throw new Exception(User::$last_error, SERVER_EXCEPTION);
		if ($user === null){
			$user = new User([
				"provider" => $provider,
				"identifier" => $identifier,
				"photo_url" => fn1::propval($user_profile, "photoURL"),
				"display_name" => fn1::propval($user_profile, "displayName"),
				"description" => fn1::propval($user_profile, "description"),
				"first_name" => fn1::propval($user_profile, "firstName"),
				"last_name" => fn1::propval($user_profile, "lastName"),
				"gender" => fn1::propval($user_profile, "gender"),
				"language" => fn1::propval($user_profile, "language"),
				"age" => fn1::propval($user_profile, "age"),
				"birth_day" => fn1::propval($user_profile, "birthDay"),
				"birth_month" => fn1::propval($user_profile, "birthMonth"),
				"birth_year" => fn1::propval($user_profile, "birthYear"),
				"email" => fn1::propval($user_profile, "email"),
				"phone" => fn1::propval($user_profile, "phone"),
				"address" => fn1::propval($user_profile, "address"),
				"country" => fn1::propval($user_profile, "country"),
				"region" => fn1::propval($user_profile, "region"),
				"city" => fn1::propval($user_profile, "city"),
				"zip" => fn1::propval($user_profile, "zip"),
				"other" => base64_encode(json_encode($tokens)),
				"userlevel" => $account == "vendor" ? 2 : 1,
				"status" => 0,
			]);
			if (!$user -> save()) throw new Exception($user -> getErrorMessage(), SERVER_EXCEPTION);
		}
		else {
			if ($action == "signup"){
				$user_userlevel = (int) $user -> userlevel;
				$user_account = $user_userlevel == 2 ? "Vendor" : "Client";
				$userlevel = $account == "vendor" ? 2 : 1;
				if ($user_userlevel != $userlevel){
					$error = "Failed to create " . fn1::strtocamel($account) . " account. You already have an existing <strong>$provider $user_account</strong> account. To change your account type, login and go to edit profile.";
					throw new Exception($error, SERVER_EXCEPTION);
				}
			}
		}

		//create session token
		if (!$user = User::createSessionToken($user -> uid)) throw new Exception(User::$last_error, SERVER_EXCEPTION);
		$user -> photo_url = localAvatar($user -> uid, $user -> photo_url); //update avatar
		$user -> setSession(); //set session
		temp_save($user -> token_fingerprint, "token", $user -> token, 30); //temp token save
		temp_save($user -> token_fingerprint, "fingerprint_data", fn1::jsonCreate(["ip" => fn1::getIP(), "useragent" => fn1::getUserAgent()]), 1); //temp fingerprint_data save TODO: remove test data

		//set oauth status
		$oauth_success = true;
		$oauth_message = "OAuth session authenticated!";
	}
	catch (Exception $e){
		$oauth_message = $e -> getMessage();
		if ($e -> getCode() != SERVER_EXCEPTION) $oauth_message = xstrip($oauth_message, true); //escape untrusted exception message
	}
}
else $oauth_message = "Unsupported authentication request";
$oauth_response = fn1::jsonCreate(["pass" => $oauth_success, "message" => $oauth_message]);
?>
<script>
	try { window.opener.HandleOAuth(<?php echo $oauth_response; ?>); } catch (e){}
	window.close();
</script>
