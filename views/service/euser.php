<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

$db = null;
try {
	if (!(is_array($route_paths) && count($route_paths) == 2)) throw new Exception('0x00 Invalid event user invite link');
	$event_id = fn1::toStrn($route_paths[0], true);
	$event_user = fn1::toStrn($route_paths[1], true);
	if (!($event_id !== '' && $event_user !== '')) throw new Exception('0x01 Invalid event user invite link');
	$db = new Database();
	if (($euser = $db -> queryItem(TABLE_EVENT_USERS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $event_user])) === false) throw new Exception($db -> getErrorMessage());
	if (!$euser) throw new Exception('Expired event user invite link');
	$db -> close();
	$db = null;
	if (!sessionActive()){
		$user_fingerprint = userFingerprint();
		temp_delete($user_fingerprint, 'euser');
		temp_save($user_fingerprint, 'euser', $event_id . ',' . $event_user, 30);
		header("location: $root/dashboard");
		exit();
	}
	if (!add_event_user($event_id, $event_user, $session_uid)) throw new Exception($add_event_user_error);
	header("location: $root/dashboard");
	exit();
}
catch (Exception $e){
	$error = $e -> getMessage();
	if ($db !== null) $db -> close();
	s::error(null, $error);
}
