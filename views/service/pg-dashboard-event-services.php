<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
include __DIR__ . "/helper-fetch.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//fetch user event
if (!$event_data = fetch_event($event_id, $session_uid)){
	//handle event not found
	if (isset($request_params["search_guest"]) || isset($request_params["search_guest_barcode"])) s::error(null, isset($fetch_event_error) ? $fetch_event_error : "Event not found");
	header("location: $root/dashboard?events");
	exit();
}

//check guest user limit
if ($event_data['guest'] && !in_array($event_data['role'], [0, 2])){
	header("location: $root/dashboard?events");
	exit();
}

//update seen
if (isset($request_params["update_seen_user"])){
	$updated_count = 0;
	$update_ids = fn1::csvArray($request_params["update_seen_user"]);
	$update_ids = count($update_ids) ? fn1::trimVal($update_ids[0]) : [];
	if (is_array($update_ids) && count($update_ids)){
		$db = new Database();
		$id_list = [];
		foreach ($update_ids as $id) if (($id = $db -> escapeString($id, true)) !== false && strlen($id)) $id_list[] = '"' . $id . '"';
		if (count($id_list)){
			$id_list = implode(",", $id_list);
			if (!$db -> update(TABLE_ORDERS, ["seen_user" => fn1::now()], "WHERE `event_id` = ? AND `seen_user` = '' AND `id` IN ($id_list)", [$event_id])){
				$error = $db -> getErrorMessage();
				$db -> close();
				s::error(null, $error);
			}
			$updated_count = (int) $db -> affected_rows;
		}
		$db -> close();
	}
	s::success($updated_count, "Order seen status updated");
}

//cancel order
if (isset($request_params["cancel_order"])){
	$updated_count = 0;
	$item_id = fn1::toStrn($request_params["cancel_order"], true);
	if (strlen($item_id)){
		$db = new Database();
		$exists = $db -> queryExists(TABLE_ORDERS, "WHERE `event_id` = ? AND `id` = ?", [$event_id, $item_id]);
		if ($exists === false){
			$error = $db -> getErrorMessage();
			$db -> close();
			s::error(null, $error);
		}
		if ($exists){
			if (!$db -> update(TABLE_ORDERS, ["status" => 3, "timestamp" => fn1::now()], "WHERE `event_id` = ? AND `id` = ?", [$event_id, $item_id])){
				$error = $db -> getErrorMessage();
				$db -> close();
				s::error(null, $error);
			}
			$db -> close();
			s::success(null, "Event order has been cancelled successfully!");
		}
		$db -> close();
		s::error(null, "Event order was not found");
	}
	s::error(null, "Invalid cancel order reference");
}

//fetch orders
$order_statuses = [0 => "Sent", 1 => "Received", 2 => "Review", 3 => "Cancelled", 4 => "Accepted", 5 => "Paid", 6 => "Completed"];
$orders = fetch_items(TABLE_ORDERS, "WHERE `event_id` = ? AND `status` <> '3' ORDER BY `_index` ASC", [$event_id], null, true);
if ($orders === false) s::error(null, $fetch_items_error);

//event data
$event = event_from_data($event_data);
































/*eof*/
