<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Plugin\User;
use Naicode\Server\Funcs as fn1;

//handle password recovery
if (!strlen($code = $route_child)) $verify_error = "Email verification link has expired.";
elseif (!User::verifyEmail($code)) $verify_error = User::$last_error;
else {
	if (!isset($_SESSION)) session_start(); //TODO temp redirect
	$_SESSION["verify_success"] = "Your account has been verified successfully! You can <a href=\"$root/login\">login</a> to continue.";
	header("location: " . NS_SITE . "/verify");
	exit();
}

//get success message from session
if (isset($_SESSION["verify_success"])){
	$verify_success = $_SESSION["verify_success"];
	unset($_SESSION["verify_success"]);
	unset($verify_error);
}
