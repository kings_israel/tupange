<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
require_once __DIR__ . "/notifications.php";
require_once __DIR__ . "/messenger.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\NSDateTime;
use Naicode\Server\Database;
use Naicode\Server as s;

//constants
const STATUS_OPEN = 0;
const STATUS_IN_PROGRESS = 1;
const STATUS_COMPLETE = 2;
const STATUS_CLOSED = 3;
if (!defined('STATUSES')) define('STATUSES', [
	0 => 'Open',
	1 => 'In Progress',
	2 => 'Complete',
	3 => 'Closed',
]);
if (!defined('ROLES')) define('ROLES', [
	0 => 'Manage All',
	1 => 'Guests & Attendance',
	2 => 'Budget & Orders',
]);
const ALLOWED_UPLOAD_EXT = ['jpg','jpeg','png','gif','doc','docx','xls','xlsx','csv','txt','ppt','pptx','zip','rar'];
$file_exts = '.' . implode(',.', ALLOWED_UPLOAD_EXT);

//fetch user event
if (!$event_data = fetch_event($event_id, $session_uid)){
	//handle event not found
	if (fn1::hasAnyKeys($request_params, "edit_checklist_group", "edit_checklist_item", "check_checklist_item", "delete_checklist_item", "delete_checklist_group")) s::error(null, isset($fetch_event_error) ? $fetch_event_error : "Event not found");
	header("location: $root/dashboard?events");
	exit();
}

//fn1::printr($event_data); exit();

//handle tasks fetch
if (isset($request_params['fetch_tasks'])){
	$db = null;
	try {
		$db = new Database();
		$today = fn1::now('Y-m-d');
		$select_fields = ['*', "DATEDIFF(`date`, '$today') as 'diff'"];
		if (!$result = $db -> select(TABLE_TASKS, $select_fields, 'WHERE `event_id` = ?', [$event_id])) throw new Exception($db -> getErrorMessage());
		$items = [];
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$id = $row['id'];
				$unseen_count = 0;
				if (($date = format_task_date($row['date'], false)) === false) throw new Exception($format_task_date_error);
				if (($task_comments = fetch_task_comments($event_id, $id, $unseen_count)) === false) throw new Exception($fetch_task_comments_error);
				$item = [
					'id' => $id,
					'task' => $row['task'],
					'person' => $row['person'],
					'notify' => (int) $row['notify'],
					'date' => $date,
					'diff' => (int) $row['diff'],
					'status' => (int) $row['status'],
					'user_id' => $row['user_id'],
					'unseen' => $unseen_count,
					'comments' => $task_comments,
					'timestamp' => $row['timestamp'],
				];
				$items[] = $item;
			}
		}
		$db -> close();
		$db = null;
		s::success($items, 'Tasks list');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//handle task users fetch
if (isset($request_params['fetch_task_users'])){
	$db = null;
	try {
		if (($owner = get_user($event_data['uid'])) === false) throw new Exception($get_user_error);
		$owner_username = fn1::toStrn($owner['username'], true);
		if ($owner_username === '') $owner_username = 'admin';
		$db = new Database();
		if (!$result = $db -> select(TABLE_EVENT_USERS, null, 'WHERE `event_id` = ? AND `status` = \'1\' ORDER BY _index DESC', [$event_id])) throw new Exception($db -> getErrorMessage());
		$items = [];
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$role = (int) $row['role'];
				$item = [
					'id' => $row['id'],
					'uid' => $row['uid'],
					'email' => $row['email'],
					'username' => $row['username'],
					'names' => $row['names'],
					'role' => $role,
					'role_text' => ROLES[$role],
				];
				$items[] = $item;
			}
		}
		$db -> close();
		$db = null;
		$items[] = [
			'id' => 'owner',
			'uid' => $owner['uid'],
			'email' => $owner['email'],
			'username' => $owner_username,
			'names' => $owner['display_name'],
			'role' => 0,
			'role_text' => ROLES[0]
		];
		s::success($items, 'Event users list');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//handle task status change
if (isset($request_params['set_status']) && isset($request_params['task'])){
	$status = (int) $request_params['set_status'];
	$task_id = fn1::toStrn($request_params['task'], true);
	$db = null;
	try {
		if (!in_array($status, [0, 1, 2, 3])) throw new Exception('Invalid status "' . $status . '"');
		if ($status === STATUS_CLOSED && (int) $event_data['role'] !== 0) throw new Exception('Tasks can status closed is reserved for event managers');
		if ($task_id == '') throw new Exception('Invalid task reference');
		$db = new Database();
		if (($existing = $db -> queryItem(TABLE_TASKS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $task_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$existing) throw new Exception('Task was not found!');
		if ((int) $existing['status'] === $status){
			$db -> close();
			$db = null;
			s::success($status, 'Status remains unchanged');
		}
		$data = [
			'status' => $status,
			'timestamp' => fn1::now()
		];
		if (!$db -> update(TABLE_TASKS, $data, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $task_id])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		s::success($status, 'Status updated successfully');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//handle task save
if (isset($request_params['save_task']) && isset($request_params['task']) && isset($request_params['status'])){
	$task_id = fn1::toStrn($request_params['save_task'], true);
	$task = fn1::toStrn($request_params['task'], true);
	$person = fn1::toStrn($request_params['person'], true);
	$notify = (int) $request_params['notify'];
	$date = fn1::toStrn($request_params['date'], true);
	$status = (int) $request_params['status'];
	$db = null;
	try {
		if (!in_array($status, [0, 1, 2, 3])) throw new Exception('Invalid status value "' . $status . '"');
		if (!in_array($notify, [0, 1, 2, 3])) throw new Exception('Invalid notify value "' . $notify . '"');
		if ($status === STATUS_CLOSED && (int) $event_data['role'] !== 0) throw new Exception('Tasks can status closed is reserved for event managers');
		if ($task === '') throw new Exception('Kindly enter the task');
		if ($date !== '' && ($tdate = format_task_date($date)) === false) throw new Exception($format_task_date_error);
		$db = new Database();
		$is_new = true;
		if ($task_id !== ''){
			if (($existing = $db -> queryItem(TABLE_TASKS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $task_id])) === false) throw new Exception($db -> getErrorMessage());
			if (!$existing) throw new Exception('Task does not exist!');
			$is_new = false;
		}
		$tuser = null;
		$assign_emailed = $is_new || !($tuser && isset($existing) && array_key_exists('assign_emailed', $existing) && $existing['assign_emailed'] == $tuser['uid']);
		if ($person !== '' && $person != 'owner'){
			if (($tuser = get_tuser($person, $event_id, $event_data['uid'])) === false) throw new Exception($get_user_error);
		}
		if (($duplicate = $db -> queryExists(TABLE_TASKS, 'WHERE `event_id` = ? AND `id` <> ? AND `task` = ?', [$event_id, $task_id, $task])) === false) throw new Exception($db -> getErrorMessage());
		if ($duplicate) throw new Exception('A duplicate task "' . $task . '" already exists.');
		if (($current_tuser = get_tuser($session_uid, $event_id, $event_data['uid'], true)) === false) throw new Exception($get_user_error);
		$data = [
			'id' => $is_new ? $db -> createToken(TABLE_TASKS, 'id', 10) : $task_id,
			'event_id' => $event_id,
			'task' => $task,
			'person' => $person,
			'notify' => $notify,
			'assign_emailed' => $person,
			'date' => $tdate,
			'status' => $status,
			'user_id' => $current_tuser['id'],
			'timestamp' => fn1::now(),
		];
		if ($is_new){
			if ($tuser && !notify_assigned_task($tuser['uid'], $data['id'], $task, $date, $assign_emailed)) throw new Exception($notify_assigned_error);
			if (!$db -> insert(TABLE_TASKS, $data)) throw new Exception($db -> getErrorMessage());
		}
		else {
			if ($existing['person'] != $data['person']){
				$ref = md5($task_id . '-' . $event_id . '-assign-task');
				if (!notification_clear($ref)) throw new Exception($notification_clear_error);
			}
			if ($tuser && $tuser['id'] != $existing['person'] && !notify_assigned_task($tuser['uid'], $task_id, $task, $date, $assign_emailed)) throw new Exception($notify_assigned_error);
			if (!$db -> update(TABLE_TASKS, $data, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $task_id])) throw new Exception($db -> getErrorMessage());
		}
		$db -> close();
		$db = null;
		$data['date'] = $date;
		$data['diff'] = (fn1::parseDate($tdate, 'Y-m-d') -> time_diff(fn1::now('Y-m-d'), 'd', true));
		s::success($data, 'Task updated successfully');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//handle task delete
if (isset($request_params['delete_task'])){
	$task_id = fn1::toStrn($request_params['delete_task'], true);
	$db = null;
	try {
		if ($task_id === '') throw new Exception('Invalid task reference');
		if ((int) $event_data['role'] !== 0) throw new Exception('Tasks deleted is reserved for event managers');

		$db = new Database();

		if (($existing = $db -> queryItem(TABLE_TASKS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $task_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$existing) throw new Exception('Task was not found!');

		$db -> close();
		$db = null;

		//delete task and related data
		if (($deleted = delete_event_task($event_id, $task_id)) === false) throw new Exception($delete_event_task_error);

		s::success($deleted, 'Task deleted successfully');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//handle item comment save
if (isset($request_params['task_comment']) && isset($request_params['comment']) && isset($request_params['file_name']) && isset($request_params['file_data']) && isset($request_params['tagged'])){
	$task_id = fn1::toStrn($request_params['task_comment'], true);
	$comment = fn1::toStrn($request_params['comment'], true);
	$file_name = fn1::toStrn($request_params['file_name'], true);
	$file_data = fn1::toStrn($request_params['file_data'], true);
	$tagged = fn1::isObject($request_params['tagged']) ? fn1::toArray($request_params['tagged']) : [];

	$db = null;
	$uploaded = null;
	try {
		if ($task_id === '') throw new Exception('Invalid task reference!');
		if ($comment === '' && ($file_name === '' && $file_data === '')) throw new Exception('Kindly provide the task comment or attachment');

		//get current tuser
		if (($current_tuser = get_tuser($session_uid, $event_id, $event_data['uid'], true)) === false) throw new Exception($get_tuser_error);

		//connect db
		$db = new Database();
		$comment_id = $db -> createToken(TABLE_TASK_COMMENTS, 'id');

		//get task
		if (($task = $db -> queryItem(TABLE_TASKS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $task_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$task) throw new Exception('Task does not exist');

		//validate tags
		$temp = [];
		for ($i = 0; $i < count($tagged); $i ++){
			$tag = $tagged[$i];
			if (!fn1::hasKeys($tag, 'id', 'indexes', 'word')) throw new Exception('Invalid tag object at ' . $i);
			if (!($tag_id = fn1::toStrn($tag['id'], true))) throw new Exception('Invalid tag user reference at ' . $i);
			if ($tag_id === $current_tuser['id']) throw new Exception('Self-tagging not allowed at ' . $i);
			if (!count($tag_indexes = fn1::toArray($tag['indexes']))) throw new Exception('Invalid tag indexes at ' . $i);
			if (!($tag_word = fn1::toStrn($tag['word'], true))) throw new Exception('Invalid tag word at ' . $i);
			if (!$tag_user = get_tuser($tag_id, $event_id, $event_data['uid'])) throw new Exception('Tag user error (' . $get_tuser_error . ') at ' . $i);
			$temp[] = [
				'user' => $tag_user,
				'word' => $tag_word,
				'indexes' => $tag_indexes,
			];
		}
		$tagged = $temp; unset($temp);

		//save attachment
		$file_src = '';
		$filename = '';
		if ($file_name !== '' && $file_data !== ''){
			$pinfo = fn1::pathInfo($file_name);
			$ext = $pinfo['ext'];
			if (!$pinfo || !in_array($ext, ALLOWED_UPLOAD_EXT)) throw new Exception('Unsupported upload file ' . $file_name);
			$file_name = $pinfo['filename'];

			//upload base64 image
			list($type, $data) = explode(';', $file_data);
			list(, $data) = explode(',', $data);
			if (!$data = base64_decode($data)) throw new Exception('Invalid upload file data for ' . $file_name);
			unset($file_data);

			$dir_name = 'tcfiles';
			$new_file_dir = getUploads(true) . '/' . $dir_name;
			if (!fn1::dirCreate($new_file_dir)) throw new Exception('Failed to get upload folder ' . $new_file_dir);

			$new_file_name = $event_id  . '-' . $comment_id . '.' . $ext . '.tc';
			$new_file_src = $dir_name . '/' . $new_file_name;
			$new_path = $new_file_dir . '/' . $new_file_name;

			if (!@file_put_contents($new_path, $data)) throw new Exception('File upload failed for ' . $file_name);
			if (!fn1::isFile($new_path)) throw new Exception('Unable to locate uploaded file ~ ' . $new_file_src);
			$uploaded = $new_path;
			$file_src = $new_file_src;
			$filename = $file_name;
		}

		//notify tagged
		$tags = [];
		$task_user_id = fn1::toStrn($task['user_id'], true);
		$task_person = fn1::toStrn($task['person'], true);
		$task_owner_id = $task_person === '' ? $task_user_id : $task_person;
		$task_owner_tagged = false;
		for ($i = 0; $i < count($tagged); $i ++){
			$tag = $tagged[$i];
			$tag_user = $tag['user'];
			if ($tag_user['id'] === $task_owner_id) $task_owner_tagged = true;
			if (!notify_task_comment($tag_user['uid'], $task_id, $task['task'], $comment)) throw new Exception($notify_task_comment_error);
			$tags[] = [
				'id' => $tag_user['id'],
				'indexes' => $tag['indexes'],
				'word' => $tag['word'],
			];
		}

		//save comment data
		$timestamp = fn1::now();
		$data = [
			'id' => $comment_id,
			'task_id' => $task_id,
			'event_id' => $event_id,
			'uid' => $current_tuser['uid'],
			'user_id' => $current_tuser['id'],
			'comment' => $comment,
			'tagged' => $tags,
			'file_name' => $filename,
			'file_src' => $file_src,
			'status' => 1,
			'timestamp' => $timestamp,
			'seen' => $current_tuser['uid'],
		];
		if (!$db -> insert(TABLE_TASK_COMMENTS, $data)) throw new Exception($db -> getErrorMessage());

		$db -> close();
		$db = null;

		//successful return comment
		$comment = task_comment($data);
		s::success($comment, 'Comment added successfully');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($uploaded !== null && fn1::isFile($uploaded)) fn1::fileDelete($uploaded);
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//handle task update seen
if (isset($request_params['task_update_seen'])){
	$task_id = fn1::toStrn($request_params['task_update_seen'], true);
	$db = null;
	try {
		if ($task_id === '') throw new Exception('Invalid task reference');
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_TASKS, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $task_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Task does not exist');

		//update sql
		$eid = $db -> escapeString($event_id);
		$uid = $db -> escapeString($session_uid, true);
		$table = $db -> escapeString(TABLE_TASK_COMMENTS);
		$sql = "UPDATE `$table` SET `seen` = CONCAT(`seen`, ',$uid') WHERE `event_id` = '$eid' AND NOT FIND_IN_SET('$uid', `seen`)";

		if (!$db -> query($sql)) throw new Exception($db -> getErrorMessage());
		$affected_rows = (int) $db -> affected_rows;

		$db -> close();
		$db = null;
		s::success($affected_rows, 'Update complete');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//handle task comment delete
if (isset($request_params['task_comment_delete']) && isset($request_params['task'])){
	$id = fn1::toStrn($request_params['task_comment_delete'], true);
	$task_id = fn1::toStrn($request_params['task'], true);
	$db = null;
	try {
		if ($id === '') throw new Exception('Invalid task comment reference');
		if ($task_id === '') throw new Exception('Invalid task reference');
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_TASK_COMMENTS, 'WHERE `event_id` = ? AND `task_id` = ? AND `id` = ?', [$event_id, $task_id, $id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Task comment does not exist');

		//set status to 0 ~ deleted
		if (!$db -> update(TABLE_TASK_COMMENTS, ['status' => 0], 'WHERE `event_id` = ? AND `task_id` = ? AND `id` = ?', [$event_id, $task_id, $id])) throw new Exception($db -> getErrorMessage());
		$affected_rows = (int) $db -> affected_rows;

		$db -> close();
		$db = null;
		s::success($affected_rows, 'Status updated');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//handle task comment file download
if (isset($request_params['tcf']) && isset($request_params['t'])){
	$id = fn1::toStrn($request_params['tcf'], true);
	$task_id = fn1::toStrn($request_params['t'], true);
	$db = null;
	try {
		if ($id === '') throw new Exception('Invalid task comment reference');
		if ($task_id === '') throw new Exception('Invalid task reference');

		$db = new Database();

		if (($comment = $db -> queryItem(TABLE_TASK_COMMENTS, null, 'WHERE `event_id` = ? AND `task_id` = ? AND `id` = ?', [$event_id, $task_id, $id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$comment) throw new Exception('Task comment does not exist');

		$db -> close();
		$db = null;

		$file_name = fn1::toStrn($comment['file_name'], true);
		$file_src = fn1::toStrn($comment['file_src'], true);
		if ($file_src === '') throw new Exception('No attachment found!');

		$path = getUploads(true) . '/' . $file_src;
		if (!fn1::isFile($path)){
			http_response_code(404);
			die();
		}

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $file_name. '"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($path));
		readfile($path);
		exit();
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//event data
$event = event_from_data($event_data);

//notify assigned task
function notify_assigned_task($uid, $task_id, $task, $date_due='', $email_notification=true){
	global $notify_assigned_error, $get_tuser_error, $notify_error, $session_uid, $event_data, $event_id, $root, $notification_clear_error;
	$db = null;
	try {
		$task_id = fn1::toStrn($task_id, true);
		$event_id = fn1::toStrn($event_id, true);
		if ($task_id === '') throw new Exception('Invalid task reference');
		if (($current_tuser = get_tuser($session_uid, $event_id, $event_data['uid'], true)) === false) throw new Exception($get_tuser_error);
		if (($tuser = get_tuser($uid, $event_id, $event_data['uid'], true)) === false) throw new Exception($get_tuser_error);
		if ($current_tuser['uid'] == $tuser['uid']) return true;

		$event_name = $event_data['name'];

		$text = 'You have been assigned a task by @' . $current_tuser['username'] . ' on the event "' . $event_name . '" - "' . $task . '"';
		if ($date_due != '') $text .= ' due on ' . $date_due;

		$action = $root . '/dashboard?event-checklist=' . urlencode($event_id) . '&task-assigned=' . $task_id;
		$type = 0;
		$ref = md5($task_id . '-' . $event_id . '-assign-task');
		if (!notification_clear($ref)) throw new Exception($notification_clear_error);

		//send notification to user
		if (!notify($uid, $text, $action, 'check', 0, $ref)) throw new Exception($notify_error);

		//email notification
		if ($email_notification){
			$from_names = 'Tupange Event';
			$from_email = 'no-reply@' . NS_DOMAIN;
			$names = $tuser['names'];
			$email = $tuser['email'];
			$subject = 'Assigned Task - ' . $event_name;
			$message = '<h2>Hello ' . $names . ',</h2><p>' . $text . '</p>';
			$button_link = $action;
			$button_text = 'Open Task';
			if (!email_message($from_names, $from_email, $names, $email, $subject, $message, $button_link, $button_text)) throw new Exception('There was an error sending email');
		}

		//on success
		return true;
	}
	catch (Exception $e){
		//on error
		$notify_assigned_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}

//notify task comment mention
function notify_task_comment($uid, $task_id, $task, $comment, $is_mention=true){
	global $notify_task_comment_error, $get_tuser_error, $notify_error, $session_uid, $event_data, $event_id, $root, $notification_clear_error, $current_tuser;
	$db = null;
	try {
		$task_id = fn1::toStrn($task_id, true);
		$event_id = fn1::toStrn($event_id, true);
		if ($task_id === '') throw new Exception('Invalid task reference');
		if (!$current_tuser && ($current_tuser = get_tuser($session_uid, $event_id, $event_data['uid'], true)) === false) throw new Exception($get_tuser_error);
		if (($tuser = get_tuser($uid, $event_id, $event_data['uid'], true)) === false) throw new Exception($get_tuser_error);
		if ($current_tuser['uid'] == $tuser['uid']) return true;

		$event_name = $event_data['name'];

		if ($is_mention) $text = '@' . $current_tuser['username'] . ' mentioned you in a comment "' . $comment . '" on the event "' . $event_name . '" - "' . $task . '"';
		else $text = '@' . $current_tuser['username'] . ' commented on your task "' . $comment . '" on the event "' . $event_name . '" - "' . $task . '"';

		$action = $root . '/dashboard?event-checklist=' . urlencode($event_id) . '&task-assigned=' . $task_id . '&comments=1';
		$type = 0;
		$ref = md5($task_id . '-' . $event_id . '-task-comment');
		if (!notification_clear($ref)) throw new Exception($notification_clear_error);

		//send notification to user
		if (!notify($uid, $text, $action, 'commenting-o', 0, $ref)) throw new Exception($notify_error);

		//on success
		return true;
	}
	catch (Exception $e){
		//on error
		$notify_task_comment_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}

//task comment
function task_comment($comment_data){
	global $session_uid;
	return [
		'id' => $comment_data['id'],
		'comment' => $comment_data['comment'],
		'tags' => @fn1::jsonParse($comment_data['tagged'], true, []),
		'file' => $comment_data['file_name'],
		'status' => (int) $comment_data['status'],
		'seen' => in_array($session_uid, explode(',', $comment_data['seen'])),
		'user_id' => fn1::toStrn($comment_data['user_id'], true),
		'timestamp' => fn1::toStrn($comment_data['timestamp'], true),
	];
}

//fetch task comments
function fetch_task_comments($event_id, $task_id, &$unseen_count=0){
	global $fetch_task_comments_error;
	$db = null;
	try {
		$db = new Database();
		if (!$result = $db -> select(TABLE_TASK_COMMENTS, null, 'WHERE `event_id` = ? AND `task_id` = ? ORDER BY `_index` ASC', [$event_id, $task_id])) throw new Exception($db -> getErrorMessage());
		$items = [];
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$comment = task_comment(xstrip($row));
				if ($comment['status'] && !$comment['seen']) $unseen_count ++;
				$items[] = $comment;
			}
		}
		$db -> close();
		$db = null;
		return $items;
	}
	catch (Exception $e){
		$fetch_task_comments_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}



















/*eof*/
