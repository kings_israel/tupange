<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as dfn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//constants
const REG_STATUS_REGISTERED = 0;
const REG_STATUS_NOTIFIED = 1;
const REGISTRATION_TYPES = [0 => "Registered", 1 => "Registered & Notified"];

//fetch user event
if (!$event_data = fetch_event($event_id, $session_uid)){
	//handle event not found
	if (isset($request_params["search_guest"]) || isset($request_params["search_guest_barcode"])) s::error(null, isset($fetch_event_error) ? $fetch_event_error : "Event not found");
	header("location: $root/dashboard?events");
	exit();
}

//check guest user limit
if ($event_data['guest'] && !in_array($event_data['role'], [0, 1])){
	header("location: $root/dashboard?events");
	exit();
}

//registration ~ edit ticket
if (isset($request_params['edit_ticket'])){
	$ticket_id = dfn1::toStrx($request_params['edit_ticket'], true);
	$ticket_title = strtoupper(dfn1::toStrn($request_params['title'], true));
	$ticket_amount = (float) $request_params['amount'];
	$ticket_limit = (int) $request_params['limit'];
	$ticket_description = dfn1::toStrn($request_params['description'], true);
	$db = null;
	try {
		if ($ticket_title == '') throw new Exception('Kindly enter the ticket title');
		if ($ticket_title == 'GENERAL ADMISSION') throw new Exception('Ticket title "GENERAL ADMISSION" is system reserved. Try another title');
		if ($ticket_amount < 0) throw new Exception('Kindly enter a valid ticket price');
		if ($ticket_limit < 0) throw new Exception('Kindly enter a valid ticket guests limit');
		$db = new Database();
		if ($ticket_id != ''){
			$existing = $db -> queryItem(TABLE_REGISTER_TICKETS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $ticket_id]);
			if ($existing === false) throw new Exception($db -> getErrorMessage());
			if (!$existing) throw new Exception('Ticket reference does not exist');
		}
		$duplicate = $db -> queryExists(TABLE_REGISTER_TICKETS, 'WHERE `event_id` = ? AND `id` <> ? AND `title` = ?', [$event_id, $ticket_id, $ticket_title]);
		if ($duplicate === false) throw new Exception($db -> getErrorMessage());
		if ($duplicate) throw new Exception('A ticket with the title "' . $ticket_title . '" already exists.');

		$timestamp = dfn1::now();
		$data = [
			'title' => $ticket_title,
			'description' => $ticket_description,
			'price' => $ticket_amount,
			'guests_limit' => $ticket_limit,
			'timestamp' => $timestamp,
		];
		if ($ticket_id == ''){
			$data['id'] = $db -> createToken(TABLE_REGISTER_TICKETS, 'id');
			$data['event_id'] = $event_id;
			if (!$db -> insert(TABLE_REGISTER_TICKETS, $data)) throw new Exception($db -> getErrorMessage());
			$db -> close();
			unset($data['event_id']);
			s::success(xstrip($data, true), 'Ticket has been saved successfully!');
		}
		else {
			if (!$db -> update(TABLE_REGISTER_TICKETS, $data, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $ticket_id])) throw new Exception($db -> getErrorMessage());
			$db -> close();
			$data = array_replace_recursive($existing, $data);
			unset($data['_index']);
			unset($data['event_id']);
			s::success(xstrip($data, true), 'Ticket has been updated successfully!');
		}
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}
if (isset($request_params['delete_ticket'])){
	$ticket_id = dfn1::toStrx($request_params['delete_ticket'], true);
	try {
		if ($ticket_id == '') throw new Exception('Invalid delete ticket reference');
		$db = new Database();
		$existing = $db -> queryItem(TABLE_REGISTER_TICKETS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $ticket_id]);
		if ($existing === false) throw new Exception($db -> getErrorMessage());
		if (!$existing) throw new Exception('Ticket reference does not exist');
		if (!$db -> delete(TABLE_REGISTER_TICKETS, 'id', 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $ticket_id])) throw new Exception($db -> getErrorMessage());
		unset($existing['_index']);
		unset($existing['event_id']);
		s::success(xstrip($existing, true), 'Ticket has been deleted successfully!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//registration ~ edit guest
if (isset($request_params['edit_register_guest'])){
	$guest_id = dfn1::toStrx($request_params['edit_register_guest'], true);
	$ticket_title = dfn1::toStrn($request_params['ticket_title'], true);
	$ticket_price = (float) $request_params['ticket_price'];
	$guests = (int) $request_params['guests'];
	$names = dfn1::toStrn($request_params['names'], true);
	$phone = dfn1::toStrn($request_params['phone'], true);
	$email = dfn1::toStrn($request_params['email'], true);
	$company = dfn1::toStrn($request_params['company'], true);
	$address = dfn1::toStrn($request_params['address'], true);
	$notes = dfn1::toStrn($request_params['notes'], true);
	$db = null;
	try {
		if ($ticket_title == '') throw new Exception('Kindly select a valid ticket');
		if ($names == '') throw new Exception('Kindly enter the guest names');
		if ($guests <= 0) $guests = 1;
		$db = new Database();
		if ($guest_id != ''){
			$existing = $db -> queryItem(TABLE_REGISTER, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $guest_id]);
			if ($existing === false) throw new Exception($db -> getErrorMessage());
			if (!$existing) throw new Exception('Registered guest reference does not exist');
		}
		$duplicate = $db -> queryExists(TABLE_REGISTER, 'WHERE `event_id` = ? AND `id` <> ? AND `names` = ? AND `phone` = ? AND `email` = ? AND `company` = ? AND `notes` = ?', [$event_id, $guest_id, $names, $phone, $email, $company, $notes]);
		if ($duplicate === false) throw new Exception($db -> getErrorMessage());
		if ($duplicate) throw new Exception('A guest with the same details already exists. Try changing the guest details to make the entry unique i.e. phone, email, etc.');

		$timestamp = dfn1::now();
		$data = [
			'names' => $names,
			'phone' => $phone,
			'email' => $email,
			'address' => $address,
			'company' => $company,
			'ticket_title' => $ticket_title,
			'ticket_price' => $ticket_price,
			'guests' => $guests,
			'amount' => $ticket_price * $guests,
			'notes' => $notes,
			'time_modified' => $timestamp,
		];
		if ($guest_id == ''){
			$data['id'] = $db -> createToken(TABLE_REGISTER, 'id');
			$data['event_id'] = $event_id;
			$data['barcode'] = $db -> createToken(TABLE_REGISTER, 'barcode', 10, false);
			$data['time_created'] = $timestamp;
			$data['status'] = REG_STATUS_REGISTERED;
			$data['sent'] = '';
			if (!$db -> insert(TABLE_REGISTER, $data)) throw new Exception($db -> getErrorMessage());
			$db -> close();
			unset($data['event_id']);
			$data['status_text'] = REGISTRATION_TYPES[$data['status']];
			s::success(xstrip($data, true), 'Guest has been registered successfully!');
		}
		else {
			if (!$db -> update(TABLE_REGISTER, $data, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $guest_id])) throw new Exception($db -> getErrorMessage());
			$db -> close();
			$data = array_replace_recursive($existing, $data);
			unset($data['_index']);
			unset($data['event_id']);
			$data['status_text'] = REGISTRATION_TYPES[$data['status']];
			s::success(xstrip($data, true), 'Registered guest has been updated successfully!');
		}
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}
if (isset($request_params['delete_registered_guest'])){
	$guest_id = dfn1::toStrx($request_params['delete_registered_guest'], true);
	try {
		if ($guest_id == '') throw new Exception('Invalid delete registered guest reference');
		$db = new Database();
		$existing = $db -> queryItem(TABLE_REGISTER, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $guest_id]);
		if ($existing === false) throw new Exception($db -> getErrorMessage());
		if (!$existing) throw new Exception('Registered guest reference does not exist');
		if (!$db -> delete(TABLE_REGISTER, 'id', 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $guest_id])) throw new Exception($db -> getErrorMessage());
		unset($existing['_index']);
		unset($existing['event_id']);
		s::success(xstrip($existing, true), 'Registered guest has been deleted successfully!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//registration ~ fetch tickets
$db = new Database();
if (!$result = $db -> select(TABLE_REGISTER_TICKETS, null, 'WHERE `event_id` = ? ORDER BY `_index` ASC', [$event_id])) die ($db -> getErrorMessage());
$ticket_settings = [];
if ($result instanceof mysqli_result){
	while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
		$row = xstrip($row, true);
		unset($row['_index']);
		unset($row['event_id']);
		$ticket_settings[] = $row;
	}
}
$db -> close();

//registration ~ fetch guests
$db = new Database();
if (!$result = $db -> select(TABLE_REGISTER, null, 'WHERE `event_id` = ? ORDER BY `time_created` DESC', [$event_id])) die ($db -> getErrorMessage());
$registration_list = [];
if ($result instanceof mysqli_result){
	while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
		$row = xstrip($row, true);
		unset($row['_index']);
		unset($row['event_id']);
		$row['status'] = (int) $row['status'];
		$row['ticket_price'] = (float) $row['ticket_price'];
		$row['amount'] = (float) $row['amount'];
		$row['status_text'] = REGISTRATION_TYPES[(int) $row['status']];
		$registration_list[] = $row;
	}
}
$db -> close();

//registration stats
$db = new Database();
$table_register = $db -> escapeString(TABLE_REGISTER);
$stats_sql = dfn1::strbuild("SELECT COUNT(*) AS 'count_registered', SUM(`amount`) as 'sum_amount' FROM `%s`", $table_register);
if (!$result = $db -> query($stats_sql)) die ($db -> getErrorMessage());
$registration_stats = ['registered' => 0, 'sales' => 0];
if ($result instanceof mysqli_result && $row = $result -> fetch_array(MYSQLI_ASSOC)){
	$registration_stats['registered'] = (int) $row['count_registered'];
	$registration_stats['sales'] = (float) $row['sum_amount'];
}

//event data
$event = event_from_data($event_data);































/*eof*/
