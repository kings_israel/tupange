<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//constants
const TRANSACTION_TYPES = ['Expense', 'Top Up'];
const TRANSACTION_EXPENSE = 0;
const TRANSACTION_TOPUP = 1;

//fetch user event
if (!$event_data = fetch_event($event_id, $session_uid)){
	//handle event not found
	if (isset($request_params["edit_budget"]) || isset($request_params["edit_budget_transaction"])) s::error(null, isset($fetch_event_error) ? $fetch_event_error : "Event not found");
	header("location: $root/dashboard?events");
	exit();
}

//check guest user limit
if ($event_data['guest'] && !in_array($event_data['role'], [0, 2])){
	header("location: $root/dashboard?event=$event_id");
	exit();
}

//edit budget
if (isset($request_params['edit_budget'])){
	$id = fn1::toStrx($request_params['edit_budget'], true);
	$title = fn1::toStrx($request_params['title'], true);
	$description = fn1::toStrx($request_params['description'], true);
	$db = null;
	try {
		if ($title == '') throw new Exception('Kindly provide the budget title');
		$db = new Database();
		if ($id != ''){
			if (($exists = $db -> queryExists(TABLE_BUDGET, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) === false) throw new Exception($db -> getErrorMessage());
			if (!$exists) throw new Exception('Budget reference does not exist!');
		}
		if (($duplicate_exists = $db -> queryExists(TABLE_BUDGET, 'WHERE `event_id` = ? AND `id` <> ? AND `title` = ?', [$event_id, $id, $title])) === false) throw new Exception($db -> getErrorMessage());
		if ($duplicate_exists) throw new Exception('Budget with title "' . $title . '" already exists. Try another title.');
		$data = [
			'title' => $title,
			'description' => $description,
			'timestamp' => fn1::now(),
		];
		if ($id == ''){
			$id = $db -> createToken(TABLE_BUDGET, 'id');
			$data['id'] = $id;
			$data['event_id'] = $event_id;
			if (!$db -> insert(TABLE_BUDGET, $data)) throw new Exception($db -> getErrorMessage());
		}
		else if (!$db -> update(TABLE_BUDGET, $data, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		if (($budget_data = get_budget_data($event_id, $id)) === false) throw new Exception($get_budget_data_error);
		s::success($budget_data, 'Budget details have been saved successfully!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//delete budget
if (isset($request_params['delete_budget'])){
	$id = fn1::toStrx($request_params['delete_budget'], true);
	$db = null;
	try {
		if ($id == '') throw new Exception('Invalid budget reference');
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_BUDGET, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Budget reference does not exist!');
		if (!$db -> delete(TABLE_BUDGET, 'id', 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) throw new Exception($db -> getErrorMessage());
		if (!$db -> delete(TABLE_BUDGET_TRANSACTIONS, 'id', 'WHERE `event_id` = ? AND `budget_id` = ?', [$event_id, $id])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		s::success($id, 'Budget its transactions have been deleted successfully!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//edit transaction
if (isset($request_params['edit_budget_transaction'])){
	$id = fn1::toStrx($request_params['edit_budget_transaction'], true);
	$budget_id = fn1::toStrx($request_params['budget_id'], true);
	$date = fn1::toStrn($request_params['date'], true);
	$type = (int) $request_params['type'];
	$title = fn1::toStrn($request_params['title'], true);
	$description = fn1::toStrn($request_params['description'], true);
	$amount = (float) $request_params['amount'];
	$reference = fn1::toStrn($request_params['reference'], true);
	$db = null;
	try {
		if ($budget_id == '') throw new Exception('Invalid transaction budget reference');
		if ($title == '') throw new Exception('Kindly provide the transaction title');
		if ($amount < 0) throw new Exception('Kindly provide valid transaction amount');
		if (!in_array($type, [0, 1])) throw new Exception('Invalid transaction type');
		$tmp_date = fn1::parseDate($date, 'd/m/Y');
		if ($tmp_date -> format('d/m/Y') != $date) throw new Exception('Kindly provide a valid transaction date (DD/MM/YYYY)');
		$date = $tmp_date -> format('Y-m-d');
		$db = new Database();
		if (($budget_exists = $db -> queryExists(TABLE_BUDGET, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $budget_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$budget_exists) throw new Exception('Transaction budget reference does not exist!');
		if ($id != ''){
			if (($exists = $db -> queryExists(TABLE_BUDGET_TRANSACTIONS, 'WHERE `event_id` = ? AND `budget_id` = ? AND `id` = ?', [$event_id, $budget_id, $id])) === false) throw new Exception($db -> getErrorMessage());
			if (!$exists) throw new Exception('Budget transaction reference does not exist!');
		}
		if (($duplicate_exists = $db -> queryExists(TABLE_BUDGET_TRANSACTIONS, 'WHERE `event_id` = ? AND `budget_id` = ? AND `id` <> ? AND `title` = ? AND `date` = ? AND `amount` = ?', [$event_id, $budget_id, $id, $title, $date, $amount])) === false) throw new Exception($db -> getErrorMessage());
		if ($duplicate_exists) throw new Exception('A duplicate transaction with the same title, date and amount already exists. Try changing these details.');
		$data = [
			'type' => $type,
			'date' => $date,
			'title' => $title,
			'description' => $description,
			'amount' => $amount,
			'reference' => $reference,
			'timestamp' => fn1::now(),
		];
		if ($id == ''){
			$id = $db -> createToken(TABLE_BUDGET_TRANSACTIONS, 'id');
			$data['id'] = $id;
			$data['event_id'] = $event_id;
			$data['budget_id'] = $budget_id;
			if (!$db -> insert(TABLE_BUDGET_TRANSACTIONS, $data)) throw new Exception($db -> getErrorMessage());
		}
		else if (!$db -> update(TABLE_BUDGET_TRANSACTIONS, $data, 'WHERE `event_id` = ? AND `budget_id` = ? AND `id` = ?', [$event_id, $budget_id, $id])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		if (($budget_data = get_budget_data($event_id, $budget_id)) === false) throw new Exception($get_budget_data_error);
		s::success(['id' => $id, 'budget' => $budget_data], 'Budget transaction has been saved successfully!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//delete transaction
if (isset($request_params['delete_budget_transaction'])){
	$id = fn1::toStrx($request_params['delete_budget_transaction'], true);
	$budget_id = fn1::toStrx($request_params['budget_id'], true);
	$db = null;
	try {
		if ($budget_id == '') throw new Exception('Invalid transaction budget reference');
		if ($id == '') throw new Exception('Invalid budget transaction reference');
		$db = new Database();
		if (($budget_exists = $db -> queryExists(TABLE_BUDGET, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $budget_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$budget_exists) throw new Exception('Transaction budget reference does not exist!');
		if (($exists = $db -> queryExists(TABLE_BUDGET_TRANSACTIONS, 'WHERE `event_id` = ? AND `budget_id` = ? AND `id` = ?', [$event_id, $budget_id, $id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Budget transaction reference does not exist!');
		if (!$db -> delete(TABLE_BUDGET_TRANSACTIONS, 'id', 'WHERE `event_id` = ? AND `budget_id` = ? AND `id` = ?', [$event_id, $budget_id, $id])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		if (($budget_data = get_budget_data($event_id, $budget_id)) === false) throw new Exception($get_budget_data_error);
		s::success($budget_data, 'Budget transaction has been deleted successfully!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//fetch budgets
if (($budgets = get_budgets($event_id)) === false) die ($get_budgets_error);

//event data
$event = event_from_data($event_data);

//helper functions
function get_budgets($event_id){
	global $get_budgets_error;
	global $get_budget_transactions_error;
	$db = null;
	try {
		$db = new Database();
		$table_budget = $db -> escapeString(TABLE_BUDGET);
		$table_budget_transactions = $db -> escapeString(TABLE_BUDGET_TRANSACTIONS);
		$event_id = $db -> escapeString($event_id);
		$sql = "SELECT `$table_budget`.*, ";
		$sql .= "(SELECT SUM(`amount`) FROM `$table_budget_transactions` WHERE `event_id` = '$event_id' AND `budget_id` = `$table_budget`.id AND `type` = '0') AS 'total_debit', ";
		$sql .= "(SELECT SUM(`amount`) FROM `$table_budget_transactions` WHERE `event_id` = '$event_id' AND `budget_id` = `$table_budget`.id AND `type` = '1') AS 'total_credit' ";
		$sql .= "FROM `$table_budget` WHERE `event_id` = '$event_id' ORDER BY `_index` ASC";
		if (($result = $db -> query($sql)) === false) throw new Exception($db -> getErrorMessage());
		$items = [];
		if ($result instanceof mysqli_result){
			while ($budget = $result -> fetch_array(MYSQLI_ASSOC)){
				//_index, id, event_id, title, description, timestamp, total_debit, total_credit
				if (($budget_transactions = get_budget_transactions($event_id, $budget['id'])) === false) throw new Exception($get_budget_transactions_error);
				$total_credit = (float) $budget['total_credit'];
				$total_debit = (float) $budget['total_debit'];
				$balance = $total_credit - $total_debit;
				$progress = $total_credit ? (int) ($total_debit/$total_credit * 100) : 0;
				$items[] = xstrip([
					'id' => $budget['id'],
					'title' => $budget['title'],
					'description' => $budget['description'],
					'total' => $total_credit,
					'balance' => $balance,
					'spent' => $total_debit,
					'progress' => $progress,
					'transactions' => $budget_transactions,
					'timestamp' => $budget['timestamp'],
				], true);
			}
		}
		$db -> close();
		return $items;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$get_budgets_error = 'Get Budgets Error: ' . $e -> getMessage();
		return false;
	}
}
function get_budget_data($event_id, $budget_id){
	global $get_budget_data_error;
	global $get_budget_transactions_error;
	$db = null;
	try {
		$db = new Database();
		$table_budget = $db -> escapeString(TABLE_BUDGET);
		$table_budget_transactions = $db -> escapeString(TABLE_BUDGET_TRANSACTIONS);
		$event_id = $db -> escapeString($event_id);
		$budget_id = $db -> escapeString($budget_id);
		$sql = "SELECT `$table_budget`.*, ";
		$sql .= "(SELECT SUM(`amount`) FROM `$table_budget_transactions` WHERE `event_id` = '$event_id' AND `budget_id` = `$table_budget`.id AND `type` = '0') AS 'total_debit', ";
		$sql .= "(SELECT SUM(`amount`) FROM `$table_budget_transactions` WHERE `event_id` = '$event_id' AND `budget_id` = `$table_budget`.id AND `type` = '1') AS 'total_credit' ";
		$sql .= "FROM `$table_budget` WHERE `event_id` = '$event_id' AND `id` = '$budget_id'";
		if (($result = $db -> query($sql)) === false) throw new Exception($db -> getErrorMessage());
		if (!($result instanceof mysqli_result)) throw new Exception('Unable to get query result');
		if (!($budget = $result -> fetch_array(MYSQLI_ASSOC))) throw new Exception('Budget does not exist!');
		$db -> close();
		//_index, id, event_id, title, description, timestamp, total_debit, total_credit
		$total_credit = (float) $budget['total_credit'];
		$total_debit = (float) $budget['total_debit'];
		$balance = $total_credit - $total_debit;
		$progress = $total_credit ? (int) ($total_debit/$total_credit * 100) : 0;
		if (($budget_transactions = get_budget_transactions($event_id, $budget_id)) === false) throw new Exception($get_budget_transactions_error);
		return xstrip([
			'id' => $budget['id'],
			'title' => $budget['title'],
			'description' => $budget['description'],
			'total' => $total_credit,
			'balance' => $balance,
			'spent' => $total_debit,
			'progress' => $progress,
			'transactions' => $budget_transactions,
			'timestamp' => $budget['timestamp'],
		], true);
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$get_budget_data_error = 'Budget Data Error: ' . $e -> getMessage();
		return false;
	}
}
function get_budget_transactions($event_id, $budget_id){
	global $get_budget_transactions_error;
	$db = null;
	try {
		$db = new Database();
		if (($result = $db -> select(TABLE_BUDGET_TRANSACTIONS, null, 'WHERE `event_id` = ? AND `budget_id` = ? ORDER BY `_index` ASC', [$event_id, $budget_id])) === false) throw new Exception($db -> getErrorMessage());
		$items = [];
		$balance = 0;
		if ($result instanceof mysqli_result){
			//_index, id, event_id, budget_id, type, date, title, description, amount, reference, timestamp
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$item = get_budget_transaction($row, $balance);
				$balance = $item['balance'];
				$items[] = $item;
			}
		}
		$db -> close();
		return $items;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$get_budget_transactions_error = 'Budget Transactions Error: ' . $e -> getMessage();
		return false;
	}
}
function get_budget_transaction($data, $balance=0){
	if (!fn1::hasKeys($data, 'id', 'type', 'date', 'title', 'description', 'amount', 'reference', 'timestamp')) throw new Exception('Invalid budget transaction object');
	$type = (int) $data['type'];
	$type_name = TRANSACTION_TYPES[$type];
	$amount = (float) $data['amount'];
	$debit = !$type ? $amount : 0;
	$credit = $type ? $amount : 0;
	$balance += $credit;
	$balance -= $debit;
	return xstrip([
		'id' => $data['id'],
		'date' => $data['date'],
		'type' => $type,
		'type_name' => $type_name,
		'amount' => $amount,
		'debit' => $debit,
		'credit' => $credit,
		'balance' => $balance,
		'reference' => $data['reference'],
		'title' => $data['title'],
		'description' => $data['description'],
		'timestamp' => $data['timestamp'],
	], true);
}
