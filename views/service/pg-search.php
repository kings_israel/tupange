<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-service.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//some const
const SEARCH_TEMP = "SEARCH_TEMP";

//get service data
$search_title = "Search Services";
$search_description = "Browse our service listing";

//fetch featured services
$featured_services = [];
$temp = fetch_services("WHERE `featured` = '1' AND `deleted` = '0' AND `paused` = '0' ORDER BY `sponsored` DESC, `timestamp` DESC LIMIT 24");
if ($temp === false) s::error(null, $fetch_services_error);
$featured_services = $temp;

//fetch recent services
$recent_services = [];
$temp = fetch_services("WHERE `featured` <> '1' AND `deleted` = '0' AND `paused` = '0' ORDER BY `timestamp` DESC LIMIT 24");
if ($temp === false) s::error(null, $fetch_services_error);
$recent_services = $temp;

//search services
$user_fingerprint = userFingerprint();
$is_search = false;
$search_results = [];
$search_result_ids = [];

//unset form variables
unset($search_query);
unset($categories_value);
unset($location_value);
unset($search_date);
unset($search_sql);

/*
//serch redirect
if (isset($request_params["c"])){
	header("location: $root/search?" . base64_decode($request_params["c"]));
	exit();
}

//search reset
if (isset($request_params["reset"])){
	//delete search temp
	if (!temp_delete($user_fingerprint, SEARCH_TEMP)) die ($temp_delete_error);

	//redirect self
	header("location: $root/search?q=0");
	exit();
}
*/

//search request ~ set temp and redirect to prevet resubmission
if (array_key_exists("query", $request_params)){
	$search_query = isset($request_params["query"]) ? fn1::toStrn($request_params["query"], true) : null;
	$search_category = isset($request_params["category"]) ? (int) $request_params["category"] : null;
	$search_rating = isset($request_params["rating"]) ? (float) $request_params["rating"] : null;
	/*
	$search_categories = isset($request_params["categories"]) ? $request_params["categories"] : null;
	if ($search_categories){
		$search_categories = fn1::jsonParse($search_categories, true, []);
		$temp_categories = [];
		if (!fn1::isEmpty($search_categories)){
			foreach ($search_categories as $cat){
				if (!fn1::hasKeys($cat, "name", "value", "icon")) continue;
				$temp_categories[] = ["name" => $cat["name"], "value" => $cat["value"], "icon" => $cat["icon"]];
			}
		}
		if (count($temp_categories)){
			$search_categories = $temp_categories;
			$categories_value = str_replace("\"", "'", fn1::jsonCreate($search_categories));
		}
		else $search_categories = null;
	}
	$search_event_type = isset($request_params["event_type"]) ? $request_params["event_type"] : null;
	if ($search_event_type){
		$search_event_type = fn1::jsonParse($search_event_type, true, []);
		if (fn1::hasKeys($search_event_type, "text", "name", "value")) $event_type_value = $search_event_type["value"];
		else $search_event_type = null;
	}
	*/
	$search_location = isset($request_params["location"]) ? $request_params["location"] : null;
	if ($search_location){
		$search_location = fn1::jsonParse($search_location, true, []);
		if (fn1::hasKeys($search_location, "lat", "lon", "name", "type")){
			$location_lat = (float) $search_location["lat"];
			$location_lon = (float) $search_location["lon"];
			$location_name = $search_location["name"];
			$location_type = $search_location["type"];
			$location_value = $location_name;
		}
	}
	else $location_value = '';
	$search_date = isset($request_params["date"]) ? fn1::toStrx($request_params["date"], true) : null;
	if ($search_date){
		$temp_search_date = DateTime::createFromFormat("d/m/Y", $search_date);
		$temp_search_date = $temp_search_date -> format("d/m/Y");
		if ($temp_search_date != $search_date) $search_date = "";
	}

	//connection
	$db = new Database();

	//queries
	$select_queries = [];
	$bind_queries = [];
	$select_queries[] = "*";
	if (strlen($search_query)){
		$search_query = $db -> escapeString($search_query);
		$bind_query = "(`title` LIKE '%$search_query%'";
		$bind_query .= "OR `description` LIKE '%$search_query%'";
		$bind_query .= "OR `category_name` LIKE '%$search_query%'";
		$bind_query .= "OR `location_name` LIKE '%$search_query%')";
		$bind_queries[] = $bind_query;
	}
	/*
	if (!fn1::isEmpty($search_categories)){
		$category_values = [];
		foreach ($search_categories as $category){
			$category_value = (int) $category["value"];
			if ($category_value) $category_values[] = '"' . $category_value . '"';
		}
		if (count($category_values)) $bind_queries[] = "`category_value` IN (" . implode(",", $category_values) . ")";
	}
	*/
	if ($search_category) $bind_queries[] = "`category_value` = '$search_category'";

	if ($search_rating){
		$bind_queries[] = "(SELECT AVG(`tb_reviews`.`rating`) FROM `tb_reviews` WHERE `service_id` = `tb_vendor_services`.`id` and `tb_reviews`.`type` = '1') >= '$search_rating'";
	}
	$location_distance = false;
	if (isset($location_lat) && isset($location_lon)){
		$select_query = "(111.111 * DEGREES(ACOS(LEAST(COS(RADIANS(`location_map_lat`))";
        $select_query .= " * COS(RADIANS($location_lat))";
        $select_query .= " * COS(RADIANS(`location_map_lon` - $location_lon))";
        $select_query .= " + SIN(RADIANS(`location_map_lat`))";
        $select_query .= " * SIN(RADIANS($location_lat)), 1.0)))) AS 'distance'";
		$select_queries[] = $select_query;
		$location_distance = true;
	}

	//TODO filter [event types, evailability date]

	//build select query
	$table = $db -> escapeString(TABLE_VENDOR_SERVICES, true);
	$query = "SELECT " . implode(",", $select_queries) . " FROM `" . $table . "`";
	$query .= " WHERE `deleted` = '0' AND `paused` = '0'";
	if (count($bind_queries)) $query .= " AND " . implode(" AND ", $bind_queries);
	if ($location_distance) $query .= " ORDER BY `distance` ASC";
	else $query .= " ORDER BY `timestamp` DESC";
	$query .= " LIMIT 24";

	//close connection
	$db -> close();


	if ($query){
		//save search temp
		$search_temp = [
			"tmp_search_sql" => $query,
			"tmp_search_query" => $search_query,
			"tmp_category" => $search_category,
			"tmp_location_value" => $location_value,
			"tmp_search_date" => $search_date,
		];
		$search_temp = fn1::jsonCreate($search_temp);
		//if (!temp_save($user_fingerprint, SEARCH_TEMP, $search_temp)) die ($temp_save_error);

		//redirect self
		//header("location: $root/search?q=1");
		//exit();
	}

	//clear search temp
	//if (!temp_delete($user_fingerprint, SEARCH_TEMP)) die ($temp_delete_error);

	//redirect self
	//header("location: $root/search?q=0");
	//exit();
	//if (!temp_delete($user_fingerprint, SEARCH_TEMP)) die ($temp_delete_error);
}

if (!temp_delete($user_fingerprint, SEARCH_TEMP)) die ($temp_delete_error);

//search temp check
//if (($search_temp = temp_get($user_fingerprint, SEARCH_TEMP)) === false) die ($temp_get_error);
if (isset($search_temp) && $search_temp && !fn1::isEmpty($search_temp = fn1::jsonParse($search_temp, true, [])) && array_key_exists("tmp_search_sql", $search_temp)){
	//get search variables
	$query = fn1::toStrx($search_temp["tmp_search_sql"], true);
	$search_query = $search_temp["tmp_search_query"];
	$search_category = $search_temp["tmp_category"];
	$location_value = $search_temp["tmp_location_value"];
	$search_date = $search_temp["tmp_search_date"];

	//fn1::printr($query); exit();

	//fetch
	if (strlen($query)){
		//connection
		$db = new Database();

		//fetch items
		$result = $db -> query($query);
		if ($result === false){
			$error = $db -> getErrorMessage();
			$db -> close();
			s::error(null, $error);
		}
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$service = service_info($row);
				$search_results[] = $service;
				$search_result_ids[] = $service["id"];
			}
		}

		//close connection
		$db -> close();
		$is_search = true;
	}
	//elseif (!temp_delete($user_fingerprint, SEARCH_TEMP)) die ($temp_delete_error);
}


//set search results title
if ($is_search) $search_title = "Search Results";

//remove results
if (count($search_result_ids) && count($featured_services)){
	$temp = [];
	foreach ($featured_services as $value) if (!in_array($value["id"], $search_result_ids)) $temp[] = $value;
	$featured_services = $temp;
}
if (count($search_result_ids) && count($recent_services)){
	$temp = [];
	foreach ($recent_services as $value) if (!in_array($value["id"], $search_result_ids)) $temp[] = $value;
	$recent_services = $temp;
}


//page variables
$page_title = "Tupange | $search_title";
$page_description = $search_title . " | " . $search_description;

//test
/*
if (count($search_results)) $search_results = fn1::arrayRepeat($search_results, 6);
$search_results[2]["in_favorites"] = 1;
$search_results[5]["in_favorites"] = 1;
if (count($recent_services)) $recent_services = fn1::arrayRepeat($recent_services, 3);
if (count($featured_services)) $featured_services = fn1::arrayRepeat($featured_services, 3);
//$test = ["search_results" => $search_results, "recent_services" => $recent_services, "featured_services" => $featured_services];
//fn1::printr($test); exit(); //test
*/

//all services
$all_services = fn1::arraysMerge($search_results, $recent_services, $featured_services);
