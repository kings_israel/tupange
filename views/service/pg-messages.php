<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-local-avatar.php";
require_once __DIR__ . "/helper-service.php";
require_once __DIR__ . "/notifications.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//constants
const MESSAGE_CLASS_ME = 'me'; //messages session user
const MESSAGE_CLASS_YOU = 'you'; //messages subject user
const MESSAGE_STATUS_SENT = 0;
const MESSAGE_STATUS_READ = 1;
const ATTACHMENT_SIZE_LIMIT = 5;

//requires session
include __DIR__ . "/session.php";

//redirect invalid session uid
if (!(isset($session_uid) && strlen($session_uid))){
	if (isset($request_params['get_unread_count'])) s::error(null, 'Invalid session');
	header("location: $root/dashboard");
	exit();
}

//get unread count
if (isset($request_params['get_unread_count'])){
	$db = new Database();
	$query_count = $db -> queryCount(TABLE_MESSAGES, "WHERE `recipient` = ? AND `status` = '0'", [$session_uid]);
	if ($query_count === false){
		$db -> close();
		s::error(null, $db -> getErrorMessage());
	}
	$db -> close();
	s::success(['count' => $query_count], "Unread messages");
}

//update read
if (isset($request_params['update_read'])){
	$sender = fn1::toStrx($request_params['update_read'], true);
	$db = new Database();
	$data = [
		'status' => 1,
		'timestamp' => fn1::now(),
	];
	if (!$db -> update(TABLE_MESSAGES, $data, "WHERE `recipient` = ? AND `sender` = ? AND `status` = '0'", [$session_uid, $sender])){
		$db -> close();
		s::error(null, $db -> getErrorMessage());
	}
	$affected_rows = $db -> affected_rows;
	$db -> close();
	s::success($affected_rows, "Marked as read");
}

//attachment file
if ($route_child == 'attachment'){
	$name = fn1::toStrn(fn1::propval($request_params, 'name'));
	if (count($route_paths) !== 2){
		http_response_code(404);
		die();
	}
	$file_name = $route_paths[1];
	$path = getUploads(true) . '/' . $file_name . '.attach';
	if (!fn1::isFile($path)){
		http_response_code(404);
		die();
	}
	$file_data = @file_get_contents($path);
	if (($file_data = base64_decode($file_data)) === false){
		http_response_code(404);
		die();
	}
	$ext = strtolower(fn1::toStrn(pathinfo($file_name, PATHINFO_EXTENSION), true));
	$mime_type = fn1::extMimeType($ext);
	if (strpos($mime_type, 'image') !== false){
		header('Content-Disposition: inline');
		header('Content-Type: ' . $mime_type);
	}
	else {
		$filename = $name != '' ? $name : $file_name;
		header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="' . $filename. '"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . strlen($file_data));
	}
	echo $file_data;
	exit();
}

//fetch threads
if (($threads = get_threads($temp_uids)) === false) die ('x1' . $get_threads_error);

//get selected thread
$selected_thread_missing = false;
$selected_thread = fn1::toStrx($route_child, true);
if ($selected_thread !== ''){
	if (!in_array($selected_thread, $temp_uids)){
		if (($temp_user = get_messages_user($selected_thread)) === false) die ($get_messages_user_error);
		if (!$temp_user) $selected_thread_missing = true;
		else array_unshift($threads, $temp_user);
		unset($temp_user);
	}
}
unset($temp_uids);

//get user data
if (!$user_data = get_messages_user_data($session_uid)) die('x2' . $get_messages_user_data_error);
$temp_names = $user_data['user']['display_name'];
if ($user_data['vendor'] !== null && array_key_exists('company_name', $user_data['vendor'])) $temp_names .= ' (' . $user_data['vendor']['company_name'] . ')';

//get_user_orders
if (isset($request_params['get_user_orders'])){
	$temp_uid = fn1::toStrx($request_params['get_user_orders'], true);
	$db = null;
	try {
		if (!(is_array($user_data) && array_key_exists('vendor', $user_data) && fn1::hasKeys($user_data['vendor'], 'id'))) throw new Exception('Failed to get vendor details!');
		if ($temp_uid == '') throw new Exception('Invalid user reference');

		//db connection
		$db = new Database();
		if (($result = $db -> select(TABLE_ORDERS, null, 'WHERE `uid` = ? AND `vendor_id` = ?', [$temp_uid, $user_data['vendor']['id']])) === false) throw new Exception($db -> getErrorMessage());
		$items = [];
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row, true);
				$items[] = [
					'id' => $row['id'],
					'title' => $row['service_title'],
					'category' => $row['service_category'],
					'time_ordered' => $row['time_ordered'],
				];
			}
		}
		//close connection
		$db -> close();
		$db = null;
		s::success($items, 'User orders');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//send message
if (isset($request_params['send_message'])){
	$message = fn1::toStrx($request_params['send_message'], true);
	$recipient = fn1::toStrx($request_params['recipient'], true);
	$attachments = fn1::toStrx($request_params['attachments'], true);
	$db = null;
	$uploaded = [];
	try {
		if ($recipient == '') throw new Exception('Invalid recipient reference!');
		if (($attachments = base64_decode($attachments)) === false) throw new Exception('Invalid attachments data!');
		$attachments = fn1::jsonParse($attachments, true, []);
		if (empty($attachments) && $message == '') throw new Exception('0x1 Invalid send message data!');

		//db connection
		$db = new Database();

		//check attachments
		$temp_attachments = [];
		foreach ($attachments as $attachment){
			if (!fn1::hasKeys($attachment, 'type', 'name', 'data')) continue;
			$name = $attachment['name'];
			$type = $attachment['type'];
			$data = $attachment['data'];
			if ($type === 'file' || $type === 'image'){
				list($file_type, $file_data) = explode(';', $data);
				list(, $file_data) = explode(',', $file_data);
				if (($file_data = base64_decode($file_data)) === false) throw new Exception('Attachment "' . $name . '" has invalid data');
				$file_size = round(strlen($file_data) / (1024 ** 2), 2);
				if ($file_size > ATTACHMENT_SIZE_LIMIT) throw new Exception('Attachment "' . $name . '" size (' . $file_size . 'mb) exceeds maximum upload file size (' . ATTACHMENT_SIZE_LIMIT . 'mb)');
				$ext = strtolower(fn1::toStrn(pathinfo($name, PATHINFO_EXTENSION), true));
				$file_name = fn1::randomString() . ($ext != '' ? '.' . $ext : '') . '.attach';
				$upload_path = getUploads(true) . '/' . $file_name;
				$upload_content = base64_encode($file_data);
				if (!fn1::fileWrite($upload_path, $upload_content, true, false)) continue;
				$uploaded[] = $upload_path;
				$temp_attachments[] = [
					'type' => $type,
					'name' => $name,
					'data' => '/messages/attachment/' . str_replace('.attach', '', $file_name),
				];
			}
			elseif ($type === 'order'){
				if (!fn1::hasKeys($data, 'ref', 'service', 'title', 'category', 'agreement', 'image', 'price')) throw new Exception('Invalid order attachment data: ' . $name);
				$temp_attachments[] = [
					'type' => $type,
					'name' => $name,
					'data' => [
						'id' => $db -> createToken(TABLE_ORDERS, 'ref_id'),
						'ref' => $data['ref'],
						'service' => $data['service'],
						'title' => $data['title'],
						'category' => $data['category'],
						'agreement' => $data['agreement'],
						'image' => $data['image'],
						'price' => (float) $data['price'],
					],
				];
			}
			elseif ($type === 'quote'){
				if (!fn1::hasKeys($data, 'ref', 'order', 'title', 'agreement', 'price')) throw new Exception('Invalid quote attachment data: ' . $name);
				$temp_attachments[] = [
					'type' => $type,
					'name' => $name,
					'data' => [
						'id' => $db -> createToken(TABLE_ORDERS, 'ref_id'),
						'ref' => $data['ref'],
						'order' => $data['order'],
						'title' => $data['title'],
						'agreement' => $data['agreement'],
						'price' => (float) $data['price'],
					],
				];
			}
		}
		if (empty($temp_attachments) && $message == '') throw new Exception('0x2 Invalid send message data!');

		//check if user exists
		if (($recipient_user = $db -> queryItem(TABLE_USERS, null, 'WHERE `uid` = ? AND `uid` <> ?', [$recipient, $session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$recipient_user) throw new Exception('Recipient user was not found!');
		$timestamp = fn1::now();
		$message_id = $db -> createToken(TABLE_MESSAGES, 'id');
		$data = [
			'id' => $message_id,
			'sender' => $session_uid,
			'recipient' => $recipient,
			'message' => $message,
			'attachments' => json_encode($temp_attachments),
			'time_sent' => $timestamp,
			'time_read' => '',
			'status' => 0,
			'timestamp' => $timestamp,
		];
		if (!$db -> insert(TABLE_MESSAGES, $data)) throw new Exception($db -> getErrorMessage());
		$uploaded = [];

		//close connection
		$db -> close();
		$db = null;
		foreach ($temp_attachments as $i => $temp_attachment) {
			if ($temp_attachment['type'] === 'file' || $temp_attachment['type'] === 'image'){
				$temp_attachments[$i]['data'] = NS_SITE . $temp_attachment['data'] . '?name=' . $temp_attachment['name'];
			}
		}
		$message = [
			'id' => $message_id,
			'class' => MESSAGE_CLASS_ME,
			'message' => $data['message'],
			'sent' => get_message_time($data['time_sent']),
			'status' => $data['status'],
			'attachments' => $temp_attachments,
			'time_sent' => $data['time_sent'],
			'time_read' => $data['time_read'],
			'timestamp' => $data['timestamp'],
		];
		s::success($message, 'Message sent successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		if (!empty($uploaded)) foreach ($uploaded as $path) fn1::fileDelete($path);
		s::error(null, $error);
	}
}

//accept custom order
if (isset($request_params['accept_order'])){
	$id = fn1::toStrx($request_params['accept_order'], true);
	$service_id = fn1::toStrx($request_params['service'], true);
	$agreement = fn1::toStrn($request_params['agreement'], true);
	$ref = fn1::toStrn($request_params['ref'], true);
	$price = (float) $request_params['price'];
	$db = null;
	try {
		if ($id === '') throw new Exception('Invalid custom order reference!');
		if ($service_id === '') throw new Exception('Invalid order service reference!');
		$db = new Database();
		if (($existing = $db -> queryItem(TABLE_ORDERS, null, 'WHERE `uid` = ? AND `ref_id` = ?', [$session_uid, $id])) === false) throw new Exception($db -> getErrorMessage());
		if ($existing) throw new Exception('You have already accepted this custom order. Order ID: ' . $existing['id']);
		if (($service = $db -> queryItem(TABLE_VENDOR_SERVICES, null, 'WHERE `id` = ?', [$service_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$service) throw new Exception('Service does not exist!');
		if (($service_vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `id` = ?', [$service['vendor_id']])) === false) throw new Exception($db -> getErrorMessage());
		if (!$service_vendor) throw new Exception('Failed to find service vendor!');
		$service_user = $service_vendor['uid'];
		if ($service_user == $session_uid) throw new Exception('You cannot accept your own orders!');
		$timestamp = fn1::now();
		$order_id = $db -> createToken(TABLE_ORDERS, 'id', 5, false);
		$accept_message = 'ORDER ACCEPTED: ' . $order_id . ' ' . ($ref != '' ? "REF: $ref " : '') . $service['title'] . ' ' . num_commas($price, 2) . '/=';
		$order_data = [
			'id' => $order_id,
			'type' => '0',
			'uid' => $session_uid,
			'ref' => $ref,
			'ref_id' => $id,
			'event_id' => '',
			'time_added' => $timestamp,
			'time_ordered' => $timestamp,
			'pricing_id' => 'CUSTOM',
			'pricing_price' => $price,
			'pricing_title' => 'Custom Order',
			'pricing_description' => $agreement,
			'pricing_package' => '',
			'pricing_timestamp' => $timestamp,
			'service_id' => $service['id'],
			'service_title' => $service['title'],
			'service_category' => $service['category_name'],
			'service_timestamp' => $service['timestamp'],
			'vendor_id' => $service['vendor_id'],
			'vendor_company' => $service_vendor['company_name'],
			'vendor_timestamp' => $service_vendor['timestamp'],
			'message' => $accept_message,
			'notes' => '',
			'status' => 0,
			'timestamp' => $timestamp,
			'seen_user' => '',
			'seen_vendor' => '',
			'quote_id' => '',
			'quote_ref' => '',
			'payment_id' => '',
		];
		$message_data = [
			'id' => $db -> createToken(TABLE_MESSAGES, 'id'),
			'sender' => $session_uid,
			'recipient' => $service_user,
			'message' => $accept_message,
			'attachments' => json_encode([]),
			'time_sent' => $timestamp,
			'time_read' => '',
			'status' => 0,
			'timestamp' => $timestamp,
		];
		if (!$db -> insert(TABLE_ORDERS, $order_data)) throw new Exception($db -> getErrorMessage());
		if (!$db -> insert(TABLE_MESSAGES, $message_data)) throw new Exception($db -> getErrorMessage());

		//notify
		$ref = md5($order_id . '-' . $service['id'] . '-' . $service_vendor['uid'] . '- notify-accept_order');
		if (!notify_vendor_order($ref, $service_vendor['uid'], $order_id, $accept_message)) throw new Exception($notify_vendor_order_error);

		$db -> close();
		$db = null;
		s::success($order_id, 'Custom order accepted successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//accept order quotation
if (isset($request_params['accept_quotation'])){
	$id = fn1::toStrx($request_params['accept_quotation'], true);
	$order_id = fn1::toStrx($request_params['order'], true);
	$agreement = fn1::toStrn($request_params['agreement'], true);
	$ref = fn1::toStrn($request_params['ref'], true);
	$price = (float) $request_params['price'];
	$db = null;
	try {
		if ($id === '') throw new Exception('Invalid quotation reference!');
		if ($order_id === '') throw new Exception('Invalid quotation order reference!');
		$db = new Database();
		if (($existing = $db -> queryItem(TABLE_ORDERS, null, 'WHERE `uid` = ? AND `quote_id` = ?', [$session_uid, $id])) === false) throw new Exception($db -> getErrorMessage());
		if ($existing) throw new Exception('You have already accepted this order quotation. Order ID: ' . $existing['id']);
		if (($order = $db -> queryItem(TABLE_ORDERS, null, 'WHERE `id` = ? AND `uid` = ?', [$order_id, $session_uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$order) throw new Exception('Your order ' . $order_id . ' does not exist!');
		if (($service_vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `id` = ?', [$order['vendor_id']])) === false) throw new Exception($db -> getErrorMessage());
		if (!$service_vendor) throw new Exception('Failed to find order service vendor!');
		$service_user = $service_vendor['uid'];
		if ($service_user == $session_uid) throw new Exception('You cannot accept your own quotations!');
		$timestamp = fn1::now();
		$accept_message = 'QUOTATION ACCEPTED: ' . $order_id . ' ' . ($ref != '' ? "REF: $ref " : '') . $order['service_title'] . ' ' . num_commas($price, 2) . '/=';
		$order_data = [
			'pricing_id' => 'QUOTATION',
			'pricing_price' => $price,
			'pricing_title' => 'Quotation',
			'pricing_description' => $agreement,
			'pricing_package' => '',
			'pricing_timestamp' => $timestamp,
			'quote_id' => $id,
			'quote_ref' => $ref,
		];
		$message_data = [
			'id' => $db -> createToken(TABLE_MESSAGES, 'id'),
			'sender' => $session_uid,
			'recipient' => $service_user,
			'message' => $accept_message,
			'attachments' => json_encode([]),
			'time_sent' => $timestamp,
			'time_read' => '',
			'status' => 0,
			'timestamp' => $timestamp,
		];
		if (!$db -> update(TABLE_ORDERS, $order_data, 'WHERE `id` = ? AND `uid` = ?', [$order_id, $session_uid])) throw new Exception($db -> getErrorMessage());
		if (!$db -> insert(TABLE_MESSAGES, $message_data)) throw new Exception($db -> getErrorMessage());

		//notify
		$ref = md5($order_id . '-' . $order['service_id'] . '-' . $service_vendor['uid'] . '- notify-accept_quotation');
		if (!notify_vendor_order($ref, $service_vendor['uid'], $order_id, $accept_message)) throw new Exception($notify_vendor_order_error);

		$db -> close();
		$db = null;
		s::success($order_id, 'Order quotation accepted successfully!');
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($db !== null) $db -> close();
		s::error(null, $error);
	}
}

//page variables
$page_title = "Tupange | Inbox - " . $temp_names;
$page_description = $page_title . ". User inbox conversations";
unset($temp_names);

//refresh threads
if (isset($request_params['refresh_threads'])){
	s::success($threads, $selected_thread);
}

//helper functions
function get_threads(&$temp_uids=null){
	global $session_uid;
	global $get_threads_error;
	global $get_messages_user_error;
	global $get_service_image_error;
	global $placeholder_image;

	$db = null;
	try {
		//db connection
		$db = new Database();

		//fetch threads
		$tmp_session_uid = $db -> escapeString($session_uid);
		$table_messages = $db -> escapeString(TABLE_MESSAGES);
		$sql = "SELECT * FROM `$table_messages` WHERE `recipient`='$tmp_session_uid' OR `sender`='$tmp_session_uid' ORDER BY `_index` ASC";
		if (!$result = $db -> query($sql)) throw new Exception($db -> getErrorMessage());
		$temp_uids = [];
		$temp_threads = [];
		if ($result instanceof mysqli_result){
			//_index, id, sender, recipient, message, attachments, time_sent, time_read, status, timestamp
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row);

				//message vars
				$message_id = $row['id'];
				$message_uid = $row['recipient'];
				$message_class = MESSAGE_CLASS_ME;
				if ($row['recipient'] == $session_uid){
					$message_uid = $row['sender'];
					$message_class = MESSAGE_CLASS_YOU;
				}
				$message_attachments = fn1::toStrx($row['attachments'], true);
				$message_attachments = fn1::jsonParse($message_attachments, true, []);
				$temp_attachments = [];
				foreach (fn1::toArray($message_attachments) as $attachment){
					if (is_array($attachment) && fn1::hasKeys($attachment, 'type', 'name', 'data')){
						if ($attachment['type'] === 'order'){
							if (!fn1::hasKeys($attachment['data'], 'ref', 'service', 'title', 'category', 'agreement', 'image', 'price')) throw new Exception('Invalid order attachment data');
							if (($image = get_service_image($attachment['data']['service'])) === false) throw new Exception($get_service_image_error);
							if (!$image) $image = $placeholder_image;
							$attachment_data = $attachment['data'];
							$temp_data = [
								'id' => $attachment_data['id'],
								'ref' => $attachment_data['ref'],
								'service' => $attachment_data['service'],
								'title' => $attachment_data['title'],
								'category' => $attachment_data['category'],
								'agreement' => $attachment_data['agreement'],
								'image' => $image,
								'price' => (float) $attachment_data['price'],
							];
							$attachment['data'] = $temp_data;
						}
						elseif ($attachment['type'] === 'quote'){
							if (!fn1::hasKeys($attachment['data'], 'ref', 'order', 'title', 'agreement', 'price')) throw new Exception('Invalid quote attachment data');
							$attachment_data = $attachment['data'];
							$temp_data = [
								'id' => $attachment_data['id'],
								'ref' => $attachment_data['ref'],
								'order' => $attachment_data['order'],
								'title' => $attachment_data['title'],
								'agreement' => $attachment_data['agreement'],
								'price' => (float) $attachment_data['price'],
							];
							$attachment['data'] = $temp_data;
						}
						elseif ($attachment['type'] === 'file' || $attachment['type'] === 'image'){
							$path = getUploads(true) . '/' . str_replace('/messages/attachment/', '', $attachment['data']) . '.attach';
							if (!fn1::isFile($path)) throw new Exception('Unable to find attachment file: ' . $path);
							$attachment['data'] = NS_SITE . $attachment['data'] . '?name=' . $attachment['name'];
						}
						else throw new Exception('Unknown attachment type: ' . $attachment['type']);
						$temp_attachments[] = $attachment;
					}
				}
				$message_attachments = $temp_attachments;
				unset($temp_attachments);
				$message_sent = fn1::toStrx($row['time_sent'], true);
				$message_read = fn1::toStrx($row['time_read'], true);
				$message_status = (int) $row['status'];
				$message_timestamp = fn1::toStrx($row['timestamp'], true);
				$message_time = get_message_time($message_sent);

				//other user
				if (!in_array($message_uid, $temp_uids)){
					if (($message_user = get_messages_user($message_uid)) === false) throw new Exception($get_messages_user_error);
					if (!(is_array($message_user) && fn1::hasKeys($message_user, 'messages', 'unread'))) throw new Exception('Failed to get messages user data!');
					$temp_uids[] = $message_uid;
					$temp_threads[$message_uid] = $message_user;
				}

				//message details
				$message = [
					'id' => $message_id,
					'class' => $message_class,
					'message' => fn1::toStrx($row['message']),
					'sent' => $message_time,
					'status' => $message_status,
					'attachments' => $message_attachments,
					'time_sent' => $message_sent,
					'time_read' => $message_read,
					'timestamp' => $message_timestamp,
				];

				//update message user
				$temp_threads[$message_uid]['messages'][] = $message;
			}
		}

		//close connection
		$db -> close();
		$db = null;

		//set threads
		$threads = [];
		foreach ($temp_uids as $temp_uid) $threads[] = $temp_threads[$temp_uid];

		//return threads
		return $threads;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$get_threads_error = $e -> getMessage();
		return false;
	}
}

//get message user
function get_messages_user_data($uid){
	global $get_messages_user_data_error;
	global $get_vendor_data_error;
	static $tmp_uploads_folder = null;
	if (!$uid = fn1::toStrx($uid, true)) return null;
	$db = null;
	try {
		//db connection
		$db = new Database();

		//get user
		if (($user = $db -> queryItem(TABLE_USERS, null, 'WHERE `uid` = ?', [$uid])) === false) throw new Exception($db -> getErrorMessage());
		if ($user === null) throw new Exception('User not found!', -1);
		$user_unset = ['_index', 'status', 'hash', 'token', 'token_ip', 'token_useragent', 'token_fingerprint', 'provider', 'identifier', 'website', 'profile_url', 'description', 'first_name', 'last_name', 'gender', 'language', 'age', 'birth_day', 'birth_month', 'birth_year', 'username', 'email_verified', 'verification_code', 'reset_code', 'phone', 'address', 'country', 'region', 'city', 'zip', 'other'];
		foreach ($user_unset as $key) unset($user[$key]);

		//get vendor
		$vendor = null;
		if ((int) $user['userlevel'] >= 2 && ($vendor = $db -> queryItem(TABLE_VENDORS, null, 'WHERE `uid` = ?', [$uid])) === false) throw new Exception($db -> getErrorMessage());
		if (fn1::hasKeys($vendor, 'id')){
			$vendor_unset = ['_index', 'uid', 'company_email', 'location_lat', 'location_lon', 'location_name', 'location_type', 'location_map_lat', 'location_map_lon', 'timestamp'];
			foreach ($vendor_unset as $key) unset($vendor[$key]);

			//get vendor services
			if (!$result = $db -> select(TABLE_VENDOR_SERVICES, null, "WHERE `vendor_id` = ? AND `deleted` = '0' AND `paused` = '0'", [$vendor['id']])) throw new Exception($db -> getErrorMessage());
			$vendor_services = [];
			if ($result instanceof mysqli_result){
				while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
					$row = xstrip($row);
					unset($row['_index']);
					if (!$tmp_uploads_folder) $tmp_uploads_folder = getUploads();
					$tmp_image_src = $tmp_uploads_folder . "/" . $row["image"];
					$vendor_services[] = [
						'id' => $row['id'],
						'image' => $row['image'],
						'image_src' => $tmp_image_src,
						'title' => $row['title'],
						'description' => $row['description'],
						'category_name' => $row['category_name'],
						'category_value' => (int) $row['category_value'],
					];
				}
			}
			$vendor['services'] = $vendor_services;
		}
		return ['user' => $user, 'vendor' => $vendor];
	}
	catch (Exception $e){
		$get_messages_user_data_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		if ((int) $e -> getCode() === -1) return null;
		return false;
	}
}

//get message user
function get_messages_user($uid){
	global $session_uid;
	global $get_messages_user_error;
	global $get_messages_user_data_error;
	if (!$uid = fn1::toStrx($uid, true)) return null;
	$db = null;
	try {
		//get user data
		$user_data = get_messages_user_data($uid);
		if ($user_data === false) throw new Exception($get_messages_user_data_error);
		if (!$user_data) throw new Exception($get_messages_user_data_error, -1);
		$user = $user_data['user'];
		$vendor = $user_data['vendor'];

		//db connection
		$db = new Database();

		//get messages preview
		$uid = $db -> escapeString($uid);
		$session_uid = $db -> escapeString($session_uid);
		$table_messages = $db -> escapeString(TABLE_MESSAGES);
		$count_unread = "SELECT COUNT(*) FROM `$table_messages` WHERE `recipient`='$session_uid' AND `sender`='$uid' AND `status`='0'";
		$sql = "SELECT `message`, `time_sent`, `status`, ($count_unread) AS 'count_unread' FROM `$table_messages` WHERE (`recipient`='$session_uid' AND `sender`='$uid') OR (`recipient`='$uid' AND `sender`='$session_uid') ORDER BY `_index` DESC LIMIT 1";
		if (!$result = $db -> query($sql)) throw new Exception($db -> getErrorMessage());
		$preview = ['text' => '', 'status' => MESSAGE_STATUS_SENT, 'time' => 'New'];
		$count_unread = 0;
		if ($result instanceof mysqli_result && $row = $result -> fetch_array(MYSQLI_ASSOC)){
			$count_unread = (int) $row['count_unread'];
			$preview = [
				'text' => fn1::toStrn($row['message'], true),
				'status' => (int) $row['status'],
				'time' => get_message_time($row['time_sent'], true),
			];
		}

		//close connection
		$db -> close();
		$db = null;

		//user item
		$item_names = $user['display_name'];
		if ($vendor !== null && array_key_exists('company_name', $vendor)) $item_names .= ' (' . $vendor['company_name'] . ')';
		$item_avatar = localAvatar($uid, $user['photo_url']);
		return [
			'user' => $uid,
			'names' => $item_names,
			'avatar' => $item_avatar,
			'preview' => $preview,
			'unread' => $count_unread,
			'messages' => [],
		];
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		if ((int) $e -> getCode() === -1) return null;
		$get_messages_user_error = $e -> getMessage();
		return false;
	}
}

//message time
function get_message_time($timestamp, $date_only=false){
	$time = 'New';
	$temp = fn1::parseDate($timestamp = fn1::toStrx($timestamp, true));
	if (fn1::formatDate($temp) == $timestamp){
		$diff_today = $temp -> time_diff(null);
		if ($diff_today < 1) $time = $temp -> format('h:i A');
		elseif ($diff_today < 2) $time = 'Yesterday';
		elseif ($diff_today < 7) $time = $temp -> format('l');
		else $time = $date_only ? $temp -> format('d/m/Y') : $temp -> format('d/m/Y h:i A');
	}
	return $time;
}

//get service image
function get_service_image($service_id){
	global $get_service_image_error;
	$db = null;
	try {
		if (($service_id = fn1::toStrx($service_id, true)) === '') throw new Exception('Invalid service reference!');

		//db connection
		$db = new Database();
		if (($existing = $db -> queryItem(TABLE_VENDOR_SERVICES, null, 'WHERE `id` = ?', [$service_id])) === false) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		if (!$existing) null;
		return getUploads() . '/' . $existing['image'];
	}
	catch (Exception $e){
		$get_service_image_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}

//send vendor notification
function notify_vendor_order($ref, $uid, $order_id, $message, $icon='cart-plus'){
	global $notify_vendor_order_error, $notify_error, $notification_clear_error, $root;
	try {
		if (!($ref = fn1::toStrn($ref, true))) throw new Exception('Invalid notification reference');
		if (!($uid = fn1::toStrn($uid, true))) throw new Exception('Invalid vendor user reference');
		if (!($order_id = fn1::toStrn($order_id, true))) throw new Exception('Invalid order reference');
		if (!($message = fn1::toStrn($message, true))) throw new Exception('Invalid notification message');
		$text = $message;
		$action = $root . '/orders/?order=' . $order_id;
		$type = 0;
		if (!notification_clear($ref)) throw new Exception($notification_clear_error);
		if (!notify($uid, $text, $action, $icon, $type, $ref)) throw new Exception($notify_error);
		return true;
	}
	catch (Exception $e){
		$notify_vendor_order_error = $e -> getMessage();
		return false;
	}
}
