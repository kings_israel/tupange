<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//fetch user events (status = 1) //Normal
if (($user_events = get_all_user_events($session_uid)) === false) s::error(null, $get_all_user_events_error);

/*
//fetch user events (status = 1) //Normal
$db = null;
$user_events = [];
try {
	$db = new Database();
	$uid = $db -> escapeString($session_uid);
	$table_events = $db -> escapeString(TABLE_EVENTS);
	$table_event_users = $db -> escapeString(TABLE_EVENT_USERS);
	$sql = "SELECT ";
	$sql .= "`$table_events`.*, ";
	$sql .= "`$table_event_users`.`role` as 'role', ";
	$sql .= "`$table_event_users`.`uid` as 'euid' ";
	$sql .= "FROM `$table_events` ";
	$sql .= "LEFT JOIN `$table_event_users` ON `$table_event_users`.`event_id` = `$table_events`.`id` ";
	$sql .= "WHERE `$table_events`.`status` = '1' ";
	$sql .= "AND (`$table_events`.`uid` = '$uid' OR `$table_event_users`.`uid` = '$uid') ";
	$sql .= "ORDER BY `$table_events`.`_index` DESC, `$table_event_users`.`role` ASC";
	if (!$result = $db -> query($sql)) throw new Exception($db -> getErrorMessage());
	if ($result instanceof mysqli_result && $result -> num_rows){
		while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
			$role = fn1::toStrn($row['role'], true);
			$euid = fn1::toStrn($row['euid'], true);
			$xuid = fn1::toStrn($row['uid'], true);
			if ($role == "" || $xuid == $uid){
				$row['role'] = 0;
				$row['guest'] = 0;
			}
			else if ($role != "" && $euid != $xuid){
				$row['role'] = (int) $role;
				$row['guest'] = 1;
			}
			unset($row['euid']);
			if ($event = event_from_data($row, false, false, false, false)) $user_events[] = $event;
		}
	}
	$db -> close();
}
catch (Exception $e){
	$error = $e -> getMessage();
	if ($db !== null) $db -> close();
	s::text($error);
}

//set past to last
$live = []; $active = []; $past = [];
foreach ($user_events as $value){
	if ($value["event_status"] == "Live") $live[] = $value;
	if ($value["event_status"] == "Active") $active[] = $value;
	if ($value["event_status"] == "Past") $past[] = $value;
}

//merge sorted
$user_events = fn1::arraysMerge($live, $active, $past);
*/
