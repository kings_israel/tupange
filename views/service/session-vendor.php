<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-purge-service.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//variables
const SESSION_DELETE_LIFETIME_DAYS = 30;

//fetch vendor data
$db = null;
try {
	//db connection
	$db = new Database();

	//vendor data
	if (($vendor_data = $db -> queryItem(TABLE_VENDORS, null, "`uid` = ?", [$session_uid])) === false) throw new Exception($db -> getErrorMessage());
	if ($vendor_data !== null && array_key_exists("id", $vendor_data)){
		$vendor_data = xstrip($vendor_data, true); //clean up result
		//vendor data edits
		unset($vendor_data["_index"]);
		unset($vendor_data["uid"]);
		$vendor_data["company_name"] = fn1::strtocamel($vendor_data["company_name"], true);
		$vendor_data["company_email"] = strtolower(trim($vendor_data["company_email"]));
		$vendor_data["company_description"] = trim($vendor_data["company_description"]);
		$tmp_location_name = fn1::strtocamel($vendor_data["location_name"], true);
		$vendor_data["location_name"] = strtolower($tmp_location_name) == "current location" ? "Custom" : $tmp_location_name;
		$vendor_data["location_lat"] = (float) $vendor_data["location_lat"];
		$vendor_data["location_lon"] = (float) $vendor_data["location_lon"];
		$vendor_data["location_map_lat"] = (float) $vendor_data["location_map_lat"];
		$vendor_data["location_map_lon"] = (float) $vendor_data["location_map_lon"];
		$tmp_vendor_id = fn1::toStrx($vendor_data["id"], true);

		//vendor categories
		$vendor_categories = [];
		if (!$result = $db -> query(fn1::strbuild("SELECT * FROM `%s` WHERE `vendor_id` = '%s' ORDER BY `value` ASC", TABLE_VENDOR_CATEGORIES, $tmp_vendor_id))) throw new Exception($db -> getErrorMessage());
		while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
			$row = xstrip($row, true); //clean up row
			$vendor_categories[] = [
				"name" => trim($row["name"]),
				"value" => (int) $row["value"],
			];
		}

		//fetch vendor services stats
		if (isset($load_vendor_services_stats) && $load_vendor_services_stats){
			$tmp_sql = fn1::strbuild("SELECT (SELECT count(*) FROM `%s` WHERE `vendor_id` = '%s' AND `deleted` = '1') as 'count_deleted',", TABLE_VENDOR_SERVICES, $tmp_vendor_id);
			$tmp_sql .= fn1::strbuild("(SELECT count(*) FROM `%s` WHERE `vendor_id` = '%s' AND `deleted` = '0' AND `paused` = '1') as 'count_paused',", TABLE_VENDOR_SERVICES, $tmp_vendor_id);
			$tmp_sql .= fn1::strbuild("(SELECT count(*) FROM `%s` WHERE `vendor_id` = '%s' AND `deleted` = '0' AND `paused` = '0') as 'count_active'", TABLE_VENDOR_SERVICES, $tmp_vendor_id);
			if (!$result = $db -> query($tmp_sql)) throw new Exception($db -> getErrorMessage());
			if ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$vendor_services_stats = [
					"deleted" => (int) $row["count_deleted"],
					"paused" => (int) $row["count_paused"],
					"active" => (int) $row["count_active"],
				];
			}
		}

		//fetch vendor services
		if (isset($load_vendor_services)){
			$vendor_services = [];
			$load_vendor_services = (int) $load_vendor_services;
			if ($load_vendor_services == 0) $tmp_sql = fn1::strbuild("SELECT * FROM `%s` WHERE `vendor_id` = '%s' AND `deleted` = '0' AND `paused` = '0' ORDER BY `timestamp` DESC", TABLE_VENDOR_SERVICES, $tmp_vendor_id);
			elseif ($load_vendor_services == -1) $tmp_sql = fn1::strbuild("SELECT * FROM `%s` WHERE `vendor_id` = '%s' AND `deleted` = '0' AND `paused` = '1' ORDER BY `timestamp` DESC", TABLE_VENDOR_SERVICES, $tmp_vendor_id);
			elseif ($load_vendor_services == -2) $tmp_sql = fn1::strbuild("SELECT * FROM `%s` WHERE `vendor_id` = '%s' AND `deleted` = '1' ORDER BY `timestamp` DESC", TABLE_VENDOR_SERVICES, $tmp_vendor_id);
			else $tmp_sql = fn1::strbuild("SELECT * FROM `%s` WHERE `vendor_id` = '%s' ORDER BY `timestamp` DESC", TABLE_VENDOR_SERVICES, $tmp_vendor_id);
			if (!$result = $db -> query($tmp_sql)) throw new Exception($db -> getErrorMessage());
			if ($result -> num_rows){
				$tmp_uploads_folder = getUploads();
				while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
					$row = xstrip($row, true); //clean up row
					$tmp_location_name = trim($row["location_name"]);
					if (strtolower($tmp_location_name) == "current location") $tmp_location_name = "Custom";
					$tmp_image = fn1::toStrx($row["image"], true);
					$tmp_image_src = !fn1::isEmpty($tmp_image) ? $tmp_uploads_folder . "/" . $tmp_image : "";
					$tmp_deleted = (int) $row["deleted"];
					$tmp_service_id = fn1::toStrx($row["id"], true);
					$tmp_timestamp = fn1::toStrx($row["timestamp"], true);
					$tmp_service = [
						"id" => $tmp_service_id,
						"image" => $row["image"],
						"image_src" => $tmp_image_src,
						"title" => $row["title"],
						"description" => $row["description"],
						"category_name" => $row["category_name"],
						"category_value" => $row["category_value"],
						"location_name" => $tmp_location_name,
						"location_lat" => (float) $row["location_lat"],
						"location_lon" => (float) $row["location_lon"],
						"location_map_lat" => (float) $row["location_map_lat"],
						"location_map_lon" => (float) $row["location_map_lon"],
						"timestamp" => $tmp_timestamp,
						"featured" => (int) $row["featured"],
						"sponsored" => (int) $row["sponsored"],
						"deleted" => $tmp_deleted,
						"paused" => (int) $row["paused"],
					];
					if ($tmp_deleted == 1){
						$tmp_deleted_days = service_deleted_days($tmp_vendor_id, $tmp_service_id, $tmp_deleted, $tmp_timestamp);
						if ($tmp_deleted_days < 0 && isset($purge_service_error) && strlen($purge_service_error)) throw new Exception($purge_service_error);
						$tmp_service["deleted_days"] = fn1::strbuild("%s Day%s", $tmp_deleted_days, $tmp_deleted_days == 1 ? "" : "s");
					}
					$vendor_services[] = $tmp_service;
				}
			}
		}
	}
	else unset($vendor_data);

	//close connection
	$db -> close();
}
catch (Exception $e){
	//error handling
	if ($db !== null) $db -> close();
	s::text($e -> getMessage());
}

//helper function
//get number of days since delete (purge expired)
function service_deleted_days($vendor_id, $service_id, $deleted, $timestamp){
	static $now;
	if (fn1::isEmpty($vendor_id = fn1::toStrx($vendor_id, true))) return 0;
	if (fn1::isEmpty($service_id = fn1::toStrx($service_id, true))) return 0;
	if (fn1::isEmpty($timestamp = fn1::toStrx($timestamp, true))) return 0;
	if ((int) $deleted != 1) return 0;
	if (!($now instanceof DateTime)) $now = new DateTime();
	$timestamp = new DateTime(date("Y-m-d H:i:s", strtotime($timestamp)));
	if ($timestamp -> getTimestamp() >= $now -> getTimestamp()) return 0;
	$diff = $now -> diff($timestamp);
	$days = (int) $diff -> format("%a");
	//purge expired
	if ($days > SESSION_DELETE_LIFETIME_DAYS && !purge_service($vendor_id, $service_id)) return -1;
	return $days;
}
