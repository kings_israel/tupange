<?php
//helper function
function service_template($service){
	global $root;
	global $placeholder_image;
	$html = '<div class="x-service-item" data-id="' . $service["id"] . '"><div class="x-service-content">';
	$html .= '<a href="javascript:" title="' . ($service["in_favorites"] ? "Unlike" : "Like") . '" class="x-service-favorite ' . ($service["in_favorites"] ? 'favorite' : '') . '" data-service-favorite="' . $service["id"] . '">
		<i class="uk-icon-' . ($service["in_favorites"] ? 'heart' : 'heart-o') . '"></i></a>';
	if ($service["sponsored"]) $html .= '<div class="x-service-ribbon"><div class="rib">Sponsored<div class="shad"></div></div></div>';
	$html .= '<div class="x-service-image">';
	if (!$service["demo"]) $html .= '<a href="' . $root . '/service/' . $service["id"] . '"><img class="uk-width-1-1" src="' . $service["image_src"] . '" data-onerror="' . $placeholder_image . '" onerror="onImageError(this);" /></a>';
	else $html .= '<img draggable="false" class="uk-width-1-1" src="' . $service["image_src"] . '" data-onerror="' . $placeholder_image . '" onerror="onImageError(this);" />';
	$html .= '</div>
			<div class="x-service-info">
				<h2>' . $service["title"] . '</h2>
				<p>' . $service["description"] . '</p>
			</div>
			<div class="x-service-meta">
				<div class="x-service-category">' . $service["category_name"] . '</div>
				<div class="x-service-location"><i class="uk-icon-map-marker"></i> ' . $service["location_name"] . '</div>
			</div>
			<div class="x-service-buttons">
				<div class="x-service-rating">' . (isset($service["rating"]) && isset($service["rating_count"]) ? '<i class="uk-icon-star"></i> <strong>' . $service["rating"] . '</strong> <span>(' . $service["rating_count"] . ')</span>' : '&nbsp;') . '</div>
				<button class="' . $service["id"] . ' service-cart-add uk-button uk-button-small uk-button-success ' . ($service["in_cart"] ? 'x-hidden' : '') . '" data-service-cart="' . $service["id"] . '"><i class="uk-icon-shopping-cart"></i> Add</button>
				<button class="' . $service["id"] . ' service-cart-added uk-button uk-button-small uk-button-white ' . (!$service["in_cart"] ? 'x-hidden' : '') . '" data-cart-delete="' . ((string) $service["in_cart"]) . '"><i class="uk-icon-check"></i> In Cart</button>
				<button class="' . $service["id"] . ' service-cart-loading uk-button uk-button-small uk-button-default uk-disabled x-hidden" disabled><i class="uk-icon-spinner uk-icon-spin"></i> Adding</button>
			</div>
		</div>
	</div>';
	return $html;
}
