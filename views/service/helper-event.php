<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//some constants
if (!defined('ROLES')) define('ROLES', [
	0 => 'Manage All',
	1 => 'Guests & Attendance',
	2 => 'Budget & Orders',
]);

//event helper functions
function get_event_item($event_id){
	global $get_event_item_error;
	$db = null;
	try {
		$db = new Database();
		if (($item = $db -> queryItem(TABLE_EVENTS, null, 'WHERE `id` = ?', [$event_id])) === false) throw new Exception($db -> getErrorMessage());
		//if (!$item) throw new Exception('Event not found!');
		if (!$item){
		    $get_event_item_error = 'Event not found!';
		    return false;
		}
		$db -> close();
		$db = null;
		return $item;
	}
	catch (Exception $e){
		$get_event_item_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function fetch_event($event_id, $uid, $output_db_error=true){
	global $fetch_event_error;
	$fetch_event_error = "";
	if (fn1::isEmpty($event_id = fn1::toStrx($event_id, true))){
		$fetch_event_error = "Invalid event reference";
		return false;
	}
	if (fn1::isEmpty($uid = fn1::toStrx($uid, true))){
		$fetch_event_error = "Invalid user reference";
		return false;
	}
	$db = new Database();
	if (($euser = $db -> queryItem(TABLE_EVENT_USERS, null, 'WHERE `event_id` = ? AND `uid` = ?', [$event_id, $uid])) === false){
		$fetch_event_error = $db -> getErrorMessage();
		$db -> close();
		if ($output_db_error) s::error(null, $fetch_event_error);
		else return false;
	}
	if (is_array($euser) && array_key_exists('event_id', $euser) && $euser['event_id'] == $event_id) $event_data = $db -> queryItem(TABLE_EVENTS, null, "`id` = ?", [$event_id]);
	else {
		$euser = null;
		$event_data = $db -> queryItem(TABLE_EVENTS, null, "`id` = ? AND `uid` = ?", [$event_id, $uid]);
	}
	if ($event_data === false){
		$fetch_event_error = $db -> getErrorMessage();
		$db -> close();
		if ($output_db_error) s::error(null, $fetch_event_error);
		else return false;
	}
	if ($euser && $euser['uid'] == $event_data['uid']) $euser = null;
	$db -> close();
	if (!(is_array($event_data) && array_key_exists('id', $event_data))){
		$fetch_event_error = "Event not found";
		return false;
	}
	$event_data['role'] = $euser && $euser['uid'] != $event_data['uid'] ? (int) $euser['role'] : 0;
	$event_data['guest'] = $euser && $euser['uid'] != $event_data['uid'] ? 1 : 0;
	return $event_data;
}
function fetch_event_guest($guest_id, $event_id, $uid, $output_db_error=false){
	global $fetch_event_guest_error;
	$fetch_event_guest_error = "";
	if (fn1::isEmpty($guest_id = fn1::toStrx($guest_id, true))){
		$fetch_event_guest_error = "Invalid event guest reference";
		return false;
	}
	if (fn1::isEmpty($event_id = fn1::toStrx($event_id, true))){
		$fetch_event_guest_error = "Invalid event reference";
		return false;
	}
	if (fn1::isEmpty($uid = fn1::toStrx($uid, true))){
		$fetch_event_guest_error = "Invalid user reference";
		return false;
	}
	$db = new Database();
	$guest_data = $db -> queryItem(TABLE_EVENT_GUESTS, null, "`id` = ? AND `event_id` = ? AND `uid` = ?", [$guest_id, $event_id, $uid]);
	if ($guest_data === false){
		$fetch_event_guest_error = $db -> getErrorMessage();
		$db -> close();
		if ($output_db_error) s::error(null, $fetch_event_guest_error);
		else return false;
	}
	$db -> close();
	if (fn1::isEmpty($guest_data)){
		$fetch_event_guest_error = "Event guest not found";
		return false;
	}
	return $guest_data;
}
function event_from_data($event_data, $location=true, $settings=true, $guest_groups=true, $event_time=true){
	static $now;
	if (!($now instanceof DateTime)) $now = new DateTime();
	$event_data = fn1::toArray($event_data);
	if (!fn1::hasKeys($event_data, "id", "uid")) return null;
	$event_data = xstrip($event_data);
	$event_id = $event_data["id"];
	$event_uid = $event_data["uid"];
	$start = new DateTime(date("Y-m-d H:i:s", strtotime($event_data["start_timestamp"])));
	$end = new DateTime(date("Y-m-d H:i:s", strtotime($event_data["end_timestamp"])));
	$now_stamp = $now -> getTimestamp();
	$start_stamp = $start -> getTimestamp();
	$end_stamp = $end -> getTimestamp();
	$status = $now_stamp > $start_stamp ? ($now_stamp > $end_stamp ? "Past" : "Live") : "Active";
	$status_value = $status == "Live" ? 1 : ($status == "Active" ? 2 : 3);
	if (!fn1::isEmpty(($event_poster = fn1::toStrx($event_data["poster"], true))) && fn1::isFile(getUploads(true) . "/" . $event_poster)) $event_poster = getUploads() . "/" . $event_poster;
	else $event_poster = "";
	$disp_time_start = $start -> format("d M Y h:i A");
	$disp_time_end = $end -> format("d M Y h:i A");
	if ($disp_time_start == $disp_time_end) $disp_time = $disp_time_start;
	else $disp_time = sprintf("%s - %s", $disp_time_start, $disp_time_end);
	$event_map_image = "";
	$tmp_event_id = fn1::toStrx($event_data["id"], true);
	$tmp_event_name = fn1::toStrx($event_data["name"], true);
	$event_has_users = event_has_users($event_id);
	if ($event_has_users === false) s::error(null, $GLOBALS['event_has_users_error']);
	$event = [
		"id" => $tmp_event_id,
		"role" => (int) $event_data["role"],
		"guest" => (int) $event_data["guest"],
		"users" => (int) $event_has_users,
		"name" => $tmp_event_name,
		"type" => [
			"name" => $event_data["type_name"],
			"value" => (int) $event_data["type_value"]
		],
		"start" => [$start -> format("d/m/Y"), $start -> format("h:i A"), $start -> format("Y-m-d H:i:s"), $start -> format("d M Y h:i A")],
		"end" => [$end -> format("d/m/Y"), $end -> format("h:i A"), $end -> format("Y-m-d H:i:s"), $end -> format("d M Y h:i A")],
		"display_time" => $disp_time,
		"description" => fn1::toStrx($event_data["description"], true),
		"event_status" => $status,
		"event_status_value" => $status_value,
		"timestamp" => fn1::toStrx($event_data["timestamp"], true),
		"status" => (int) $event_data["status"],
		"poster" => $event_poster,
		"guest_stats" => event_guests_stats($event_id, $event_uid),
	];
	if ($event_time){
		$event["time"] = [
			"past" => $status == "Past",
			"now" => $now -> format("Y-m-d H:i:s"),
			"start" => $start -> format("Y-m-d H:i:s"),
			"end" => $end -> format("Y-m-d H:i:s"),
			"duration" => [
				"now_start" => time_difference($now, $start),
				"now_end" => time_difference($now, $end),
				"start_end" => time_difference($start, $end),
			]
		];
	}
	if ($location){
		$event["location"] = null;
		if ((int) $event_data["location"] == 1) $event["location"] = [
			"name" => $event_data["location_name"],
			"type" => $event_data["location_type"],
			"lat" => (float) $event_data["location_lat"],
			"lon" => (float) $event_data["location_lon"]
		];
		$event["location_map"] = null;
		$event["location_map_image"] = "";
		if ((int) $event_data["location_map"] == 1){
			$tmp_lat = (float) $event_data["location_map_lat"];
			$tmp_lon = (float) $event_data["location_map_lon"];
			$event["location_map"] = [
				"name" => $event_data["location_map_name"],
				"type" => $event_data["location_map_type"],
				"lat" => $tmp_lat,
				"lon" => $tmp_lon
			];
			$tmp_event_map_image = event_map_image($tmp_event_id, $tmp_lat, $tmp_lon, substr($tmp_event_name, 0, 1));
			if ($tmp_event_map_image) $event["location_map_image"] = getUploads() . "/" . $tmp_event_map_image;
		}
	}
	if ($settings) $event["settings"] = fn1::toArray($event_data["settings"]);
	if ($guest_groups) $event["guest_groups"] = fn1::toArray($event_data["guest_groups"]);
	return xstrip($event, true, false);
}
function event_has_users($event_id){
	global $event_has_users_error;
	$db = null;
	try {
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_EVENT_USERS, 'WHERE `event_id` = ?', [$event_id])) === false) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		return $exists;
	}
	catch (Exception $e){
		$event_has_users_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function time_difference($a, $b){
	if (!($a instanceof DateTime && $b instanceof DateTime)) return null;
	$d = $a -> diff($b);
	return [
		"years" => (int) $d -> format("%y"),
		"months" => (int) $d -> format("%m"),
		"d" => (int) $d -> format("%a"),
		"days" => (int) $d -> format("%d"),
		"hours" => (int) $d -> format("%h"),
		"minutes" => (int) $d -> format("%i"),
		"seconds" => (int) $d -> format("%s"),
	];
}
function event_guests_stats($event_id, $uid, $output_db_error=true){
	$event_guests_stats_error = "";
	$db = new Database();
	$table = $db -> escapeString(TABLE_EVENT_GUESTS, true);
	$event_id = $db -> escapeString($event_id, true);
	$uid = $db -> escapeString($uid, true);
	$query = [];
	$query[] = fn1::strbuild("(SELECT count(*) FROM `%s` WHERE `event_id` = '%s' AND `uid` = '%s') as 'count'", $table, $event_id, $uid);
	$query[] = fn1::strbuild("(SELECT count(*) FROM `%s` WHERE `event_id` = '%s' AND `uid` = '%s' AND `status` = '0') as 'uninvited'", $table, $event_id, $uid);
	$query[] = fn1::strbuild("(SELECT count(*) FROM `%s` WHERE `event_id` = '%s' AND `uid` = '%s' AND `status` = '1') as 'invited'", $table, $event_id, $uid);
	$query[] = fn1::strbuild("(SELECT count(*) FROM `%s` WHERE `event_id` = '%s' AND `uid` = '%s' AND `status` = '2') as 'confirmed'", $table, $event_id, $uid);
	$query[] = fn1::strbuild("(SELECT count(*) FROM `%s` WHERE `event_id` = '%s' AND `uid` = '%s' AND `status` = '3') as 'attended'", $table, $event_id, $uid);
	$query = "SELECT " . implode(", ", $query);
	$result = $db -> query($query);
	if (!($result instanceof mysqli_result)){
		$event_guests_stats_error = $db -> getErrorMessage();
		$db -> close();
		if ($output_db_error) s::error(null, $event_guests_stats_error);
		else return false;
	}
	$row = $result -> fetch_array(MYSQLI_ASSOC);
	$db -> close();
	if (!fn1::hasKeys($row, "count", "uninvited", "invited", "confirmed", "attended")){
		$event_guests_stats_error = "Error getting event guest stats";
		return false;
	}
	return [
		"count" => (int) $row["count"],
		"uninvited" => (int) $row["uninvited"],
		"invited" => (int) $row["invited"],
		"confirmed" => (int) $row["confirmed"],
		"attended" => (int) $row["attended"],
	];
}
function event_map_image($event_id, $lat, $lon, $label="C"){
	$image_filename = "event-map-" . trim($event_id) . ".png";
	$image_path = getUploads(true) . "/" . $image_filename;
	if (fn1::isFile($image_path)) return $image_filename;
	$params = [
		"center" => $lat . "," . $lon,
		"markers" => "color:red|label:$label|$lat,$lon",
		"zoom" => 16,
		"size" => "600x400",
		"key" => GOOGLE_MAPS_API_KEY,
	];
	$url_params = "";
	foreach ($params as $key => $value) $url_params .= (strlen($url_params) ? "&" : "") . $key . "=" . urlencode("$value");
	$url = "https://maps.googleapis.com/maps/api/staticmap?" . $url_params;
	$image_type = pathinfo($url, PATHINFO_EXTENSION);
	file_put_contents($image_path, file_get_contents($url));
	if (fn1::isFile($image_path)) return $image_filename;
	return null;
}
function get_event_user($event_id, $id){
	global $get_event_user_error;
	$db = null;
	try {
		$db = new Database();
		if (($user = $db -> queryItem(TABLE_EVENT_USERS, null, 'WHERE `event_id` = ? AND (`uid` = ? OR `id` = ?)', [$event_id, $id, $id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$user) throw new Exception('User not found!');
		$db -> close();
		$db = null;
		return $user;
	}
	catch (Exception $e){
		$get_event_user_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function get_event_owner($event_id){
	global $get_event_owner_error, $get_user_error;
	$event_id = fn1::toStrn($event_id, true);
	$db = null;
	try {
		if ($event_id === '') throw new Exception('Invalid event reference');
		$db = new Database();
		if (($event = $db -> queryItem(TABLE_EVENTS, ['uid'], 'WHERE `id` = ?', [$event_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$event) throw new Exception('Event not found!');
		$uid = $event['uid'];
		$db -> close();
		$db = null;
		if (($user = get_user($uid)) === false) throw new Exception($get_user_error);
		return $user;
	}
	catch (Exception $e){
		$get_event_owner_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function get_user($uid){
	global $get_user_error;
	$db = null;
	try {
		$db = new Database();
		if (($user = $db -> queryItem(TABLE_USERS, null, 'WHERE `uid` = ?', [$uid])) === false) throw new Exception($db -> getErrorMessage());
		if (!$user) throw new Exception('User not found!');
		$db -> close();
		$db = null;
		return $user;
	}
	catch (Exception $e){
		$get_user_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function add_event_user($event_id, $id, $uid){
	global $add_event_user_error;
	$event_id = fn1::toStrn($event_id, true);
	$id = fn1::toStrn($id, true);
	$uid = fn1::toStrn($uid, true);
	$db = null;
	try {
		if ($event_id === '') throw new Exception('Invalid event reference');
		if ($id === '') throw new Exception('Invalid event user reference');
		if ($uid === '') throw new Exception('Invalid user reference');
		$db = new Database();
		if (($euser = $db -> queryItem(TABLE_EVENT_USERS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$euser) throw new Exception('Event user not found');
		$euser_uid = fn1::toStrn($euser['uid'], true);
		$euser_names = fn1::toStrn($euser['names'], true);
		if ($euser_uid != '' && $euser_uid != $uid) throw new Exception('Another event user "' . $euser_names . '" has already been added using the same link. Kindly request for another invitation link.');
		if ($euser_uid == $uid && (int) $euser['status'] == 1) return true;
		$data = ['status' => 1, 'uid' => $uid, 'timestamp' => fn1::now()];
		if (!$db -> update(TABLE_EVENT_USERS, $data, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		$db = null;
		return true;
	}
	catch (Exception $e){
		$add_event_user_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function get_all_user_events($uid){
	global $get_all_user_events_error, $get_user_events_error, $get_invited_events_error, $get_invited_event_error;
	try {
		if (($events = get_user_events($uid, $user_event_ids)) === false) throw new Exception($get_user_events_error);
		if (($user_invited_events = get_invited_events($uid, $user_event_ids)) === false) throw new Exception($get_invited_events_error);
		if (is_array($user_invited_events) && ($c = count($user_invited_events))){
			$items = [];
			for ($i = 0; $i < $c; $i ++){
				$inv_user = $user_invited_events[$i];
				if (($inv_event = get_invited_event($inv_user['event_id'])) === false) throw new Exception($get_invited_event_error);
				$inv_event['role'] = (int) $inv_user['role'];
				$inv_event['guest'] = 1;
				if ($event = event_from_data($inv_event, false, false, false, false)) $items[] = $event;
			}
			$events = array_merge($events, $items);
		}
		return $events;
	}
	catch (Exception $e){
		$get_all_user_events_error = $e -> getMessage();
		return false;
	}
}
function get_user_events($uid, &$user_event_ids=[]){
	global $get_user_events_error;
	$db = null;
	try {
		$db = new Database();
		if (!$result = $db -> select(TABLE_EVENTS, null, "WHERE `uid` = ? AND `status` = '1' ORDER BY `_index` ASC", [$uid])) throw new Exception($db -> getErrorMessage());
		$items = [];
		$user_event_ids = [];
		if ($result instanceof mysqli_result && $result -> num_rows){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row['role'] = 0;
				$row['guest'] = 0;
				if ($event = event_from_data($row, false, false, false, false)){
					$user_event_ids[] = $event['id'];
					$items[] = $event;
				}
			}
		}
		$db -> close();
		$db = null;
		return $items;
	}
	catch (Exception $e){
		$get_user_events_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		false;
	}
}
function get_invited_events($uid, $user_event_ids=null){
	global $get_invited_events_error;
	$db = null;
	try {
		$db = new Database();
		if (!$result = $db -> select(TABLE_EVENT_USERS, null, "WHERE `uid` = ? AND `status` = '1' ORDER BY `_index` ASC", [$uid])) throw new Exception($db -> getErrorMessage());
		$items = [];
		if ($result instanceof mysqli_result && $result -> num_rows){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$event_id = $row['event_id'];
				if (is_array($user_event_ids) && in_array($event_id, $user_event_ids)) continue;
				$items[] = [
					'event_id' => $event_id,
					'role' => (int) $row['role'],
				];
			}
		}
		$db -> close();
		$db = null;
		return $items;
	}
	catch (Exception $e){
		$get_invited_events_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function get_invited_event($event_id){
	global $get_invited_event_error;
	$db = null;
	try {
		$db = new Database();
		if (($event_data = $db -> queryItem(TABLE_EVENTS, null, 'WHERE `id` = ?', [$event_id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$event_data) throw new Exception('Event not found!');
		$db -> close();
		$db = null;
		return $event_data;
	}
	catch (Exception $e){
		$get_invited_event_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function delete_event($event_id){
	global $delete_event_error;
}
function delete_event_task($event_id, $task_id){
	global $delete_event_task_error;
	$db = null;
	try {
		$db = new Database();
		$deleted_files = 0;
		$deleted_items = 0;

		//delete task comment files
		if (!$result = $db -> select(TABLE_TASK_COMMENTS, null, 'WHERE `event_id` = ? AND `task_id` = ?', [$event_id, $task_id])) throw new Exception($db -> getErrorMessage());
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row);
				$file_src = fn1::toStrn($row['file_src'], true);
				$file_path = getUploads(true) . '/' . $file_src;
				if (fn1::isFile($file_path)){
					fn1::fileDelete($file_path);
					$deleted_files ++;
				}
			}
		}

		//delete task comment seen
		if (!$db -> delete(TABLE_TASK_COMMENTS_SEEN, '_index', 'WHERE `event_id` = ? AND `task_id` = ?', [$event_id, $task_id])) throw new Exception($db -> getErrorMessage());
		$deleted_items += (int) $db -> affected_rows;

		//delete task comments
		if (!$db -> delete(TABLE_TASK_COMMENTS, 'id', 'WHERE `event_id` = ? AND `task_id` = ?', [$event_id, $task_id])) throw new Exception($db -> getErrorMessage());
		$deleted_items += (int) $db -> affected_rows;

		//delete task
		if (!$db -> delete(TABLE_TASKS, 'id', 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $task_id])) throw new Exception($db -> getErrorMessage());
		$deleted_items += (int) $db -> affected_rows;

		$db -> close();
		$db = null;
		return [
			'files' => $deleted_files,
			'items' => $deleted_items,
		];
	}
	catch (Exception $e){
		$delete_event_task_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function get_tuser($id, $event_id, $event_uid, $is_uid=false){
	global $get_tuser_error, $get_user_error;
	$db = null;
	try {
		$id = fn1::toStrn($id, true);
		$event_id = fn1::toStrn($event_id, true);
		$event_uid = fn1::toStrn($event_uid, true);
		if ($id === '') throw new Exception('Invalid event user reference!');
		if ($event_id === '') throw new Exception('Invalid event reference!');
		if ($event_uid === '') throw new Exception('Invalid event owner reference!');
		if (!$is_uid && $id === 'owner' || $is_uid && $id == $event_uid){
			if (($owner = get_user($event_uid)) === false) throw new Exception($get_user_error);
			$owner_username = fn1::toStrn($owner['username'], true);
			if ($owner_username === '') $owner_username = 'admin';
			return [
				'id' => 'owner',
				'uid' => $owner['uid'],
				'email' => $owner['email'],
				'username' => $owner_username,
				'names' => $owner['display_name'],
				'role' => 0,
				'role_text' => ROLES[0]
			];
		}
		$db = new Database();
		if ($is_uid) $tuser = $db -> queryItem(TABLE_EVENT_USERS, null, 'WHERE `event_id` = ? AND `uid` = ? AND `status` = \'1\'', [$event_id, $id]);
		else $tuser = $db -> queryItem(TABLE_EVENT_USERS, null, 'WHERE `event_id` = ? AND `id` = ? AND `status` = \'1\'', [$event_id, $id]);
		if ($tuser === false) throw new Exception($db -> getErrorMessage());
		if (!$tuser) throw new Exception('Event user does not exist!');
		$db -> close();
		$db = null;
		$role = (int) $tuser['role'];
		return [
			'id' => $tuser['id'],
			'uid' => $tuser['uid'],
			'email' => $tuser['email'],
			'username' => $tuser['username'],
			'names' => $tuser['names'],
			'role' => $role,
			'role_text' => ROLES[$role],
		];
	}
	catch (Exception $e){
		$get_tuser_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function format_task_date($str, $to_mysql_date=true){
	global $format_task_date_error;
	try {
		$str = fn1::toStrn($str, true);
		if (!$str) return '';
		$from_format = $to_mysql_date ? 'd/m/Y' : 'Y-m-d';
		$to_format = $to_mysql_date ? 'Y-m-d' : 'd/m/Y';
		$timestamp = DateTime::createFromFormat($from_format, $str);
		if (!$timestamp) throw new Exception('Unable to format date: ' . $str . ' -> ' . $from_format);
		if ($timestamp -> format($from_format) != $str) throw new Exception('Date format mismatch!');
		return $timestamp -> format($to_format);
	}
	catch (Exception $e){
		$format_task_date_error = $str . ' ' . $e -> getMessage();
		return false;
	}
}
