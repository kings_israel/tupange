<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-local-avatar.php";
use Naicode\Server\Plugin\User;
use Naicode\Server\Funcs as fn1;

//handle email registration
if (isset($request_params["register_account"]) && in_array((int) $request_params["register_account"], [1,2])){
	$register_names = fn1::strtocamel(fn1::toStrn($request_params["register_names"], true));
	$register_email = strtolower(fn1::toStrn($request_params["register_email"], true));
	$register_password = fn1::toStrn($request_params["register_password"]);
	$register_confirm_password = fn1::toStrn($request_params["register_confirm_password"]);
	if (fn1::isEmpty($register_names)) $register_error = "Kindly provide your full names.";
	else if (!fn1::isEmail($register_email)) $register_error = "Kindly provide a valid email address.";
	else if (strlen($register_password) < 5) $register_error = "Password must have 5 or more characters";
	else if ($register_password != $register_confirm_password) $register_error = "Your passwords do not match!";
	else {
		try {
			$user = new User([
				"display_name" => $register_names,
				"email" => $register_email,
				"userlevel" => (int) $request_params["register_account"],
			]);
			if (!$user -> save($register_password, $register_confirm_password)) throw new Exception($user -> getErrorMessage()); //register user
			if (!$user = User::createSessionToken($user -> uid)) throw new Exception(User::$last_error); //create session token
			$user -> photo_url = localAvatar($user -> uid, $user -> photo_url); //update avatar
			$user -> setSession(); //set session
			if (!temp_save($user -> token_fingerprint, "token", $user -> token, 30)) throw new Exception($temp_save_error); //temp token save

			//register successful ~ redirect
			if (isset($_GET["redirect"])) header("location: " . $_GET["redirect"]);
			else header("location: " . NS_SITE . "/dashboard");
			exit();
		}
		catch (Exception $e){
			//set register error
			$register_error = $e -> getMessage();
		}
	}
}
