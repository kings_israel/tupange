<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;

//redirect if session is not active
if (!sessionActive()){
	$redirect = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
	header("location: $root/login?redirect=" . urlencode($redirect));
	exit();
}

//loading vendor data
if ($session_userlevel >= 2 && $session_status > 0 && isset($load_vendor_data) && $load_vendor_data) include_once __DIR__ . "/session-vendor.php";

//other session stats
$session_messages_count = 0;
