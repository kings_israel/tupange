<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
require_once __DIR__ . "/notifications.php";
require_once __DIR__ . "/helper-service.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//redirect invalid session
if (!(sessionActive() && isset($session_uid) && strlen($session_uid))){
	header("location: $root/dashboard");
	exit();
}

//delete cart item
if (isset($request_params["delete-item"])){
	$db = new Database();
	$item_id = fn1::toStrx($request_params["delete-item"], true);
	$db -> delete(TABLE_CART, "id", "WHERE `id` = ? AND `uid` = ?", [$item_id, $session_uid]);
	$db -> close();
	header("location: $root/dashboard?cart");
	exit();
}

//cart submit (rest)
if (isset($request_params["cart-checkout"]) && isset($request_params["items"])){
	$event_id = fn1::toStrx($request_params["cart-checkout"], true);
	$checkout_items = fn1::toArray($request_params["items"]);
	$db = null;
	$conn = null;
	$notify_refs = [];
	try {
		//db connection
		$db = new Database();

		//validate event
		if (strlen($event_id)){
			$event_exists = $db -> queryExists(TABLE_EVENTS, "WHERE `id` = ? AND `uid` = ?", [$event_id, $session_uid]);
			if ($event_exists === false) throw new Exception($db -> getErrorMessage());
			if (!$event_exists) throw new Exception("Linked event not found!");
		}

		//validate items
		$temp_vendors = [];
		if (fn1::isEmpty($checkout_items)) throw new Exception("No items to checkout in request!");
		$tmp_items = [];
		for ($i = 0; $i < count($checkout_items); $i ++){
			$item = $checkout_items[$i];
			$item_id = fn1::toStrx(fn1::propval($item, "id"), true);
			$item_message = fn1::toStrx(fn1::propval($item, "message"), true);
			$item_pricing = fn1::toArray(fn1::propval($item, "pricing"));
			$item_service_id = fn1::toStrx(fn1::propval($item, "service_id"), true);
			if (fn1::isEmpty($item_id)) throw new Exception("Invalid cart item reference at $i");
			if (fn1::isEmpty($item_service_id)) throw new Exception("Invalid cart item service reference at $i");

			//check cart item
			$existing_cart_item = $db -> queryItem(TABLE_CART, null, "WHERE `id` = ?", [$item_id]);
			if ($existing_cart_item === false) throw new Exception($db -> getErrorMessage());
			if (!$existing_cart_item) throw new Exception("Cart item not found for item at $i");
			$existing_cart_item = xstrip($existing_cart_item);

			//check service
			$existing_service = $db -> queryItem(TABLE_VENDOR_SERVICES, null, "WHERE `id` = ?", [$item_service_id]);
			if ($existing_service === false) throw new Exception($db -> getErrorMessage());
			if (!$existing_service) throw new Exception("Service not found for cart item at $i");
			$existing_service = xstrip($existing_service);

			//check service vendor
			if (array_key_exists($existing_service["vendor_id"], $temp_vendors)) $existing_service_vendor = $temp_vendors[$existing_service["vendor_id"]];
			else {
				$existing_service_vendor = $db -> queryItem(TABLE_VENDORS, null, "WHERE `id` = ?", [$existing_service["vendor_id"]]);
				if ($existing_service_vendor === false) throw new Exception($db -> getErrorMessage());
				if (!$existing_service_vendor) throw new Exception("Service vendor not found for cart item at $i");
				$existing_service_vendor = xstrip($existing_service_vendor);
				$temp_vendors[$existing_service["vendor_id"]] = $existing_service_vendor;
			}

			//check pricing
			$tmp_pricing = null;
			if (!fn1::isEmpty($item_pricing)){
				if (!fn1::hasKeys($item_pricing, "id", "price")) throw new Exception("Invalid cart item pricing at $i");
				$pricing_id = fn1::toStrx($item_pricing["id"], true);
				$pricing_price = fn1::toStrx($item_pricing["price"], true);
				if (strlen($pricing_price) && strlen($pricing_price)){
					$pricing_price = fn1::toNum($pricing_price);
					$existing_pricing = $db -> queryItem(TABLE_VENDOR_SERVICE_PRICING, null, "WHERE `id` = ? AND `service_id` = ?", [$pricing_id, $item_service_id]);
					if ($existing_pricing === false) throw new Exception($db -> getErrorMessage());
					if (!$existing_pricing) throw new Exception("Pricing details not found for cart item at $i");
					$existing_pricing = xstrip($existing_pricing);
					$tmp_pricing = [
						"id" => $pricing_id,
						"price" => fn1::toNum($existing_pricing["price"]),
						"title" => $existing_pricing["title"],
						"description" => $existing_pricing["description"],
						"package" => fn1::jsonParse($existing_pricing["package"], true, []),
						"timestamp" => $existing_pricing["timestamp"],
					];
				}
			}
			$tmp_items[] = [
				"item_id" => $item_id,
				"item_time_added" => $existing_cart_item["timestamp"],
				"item_message" => $item_message,
				"pricing_id" => $tmp_pricing ? $tmp_pricing["id"] : "",
				"pricing_price" => $tmp_pricing ? $tmp_pricing["price"] : 0,
				"pricing_title" => $tmp_pricing ? $tmp_pricing["title"] : "",
				"pricing_description" => $tmp_pricing ? $tmp_pricing["description"] : "",
				"pricing_package" => $tmp_pricing ? $tmp_pricing["package"] : "",
				"pricing_time_modified" => $tmp_pricing ? $tmp_pricing["timestamp"] : "",
				"service_id" => $item_service_id,
				"service_title" => $existing_service["title"],
				"service_category" => $existing_service["category_name"],
				"service_time_modified" => $existing_service["timestamp"],
				"vendor_id" => $existing_service_vendor["id"],
				"vendor_uid" => $existing_service_vendor["uid"],
				"vendor_company" => $existing_service_vendor["company_name"],
				"vendor_time_modified" => $existing_service_vendor["timestamp"],
			];
		}

		//new connection
		$conn = $db -> resetValues() -> getConnection(false);

		//create orders (no commit)
		$now = fn1::now();
		$ref = $db -> createToken(TABLE_ORDERS, "ref", 5, false);
		$tmp_orders = [];
		for ($i = 0; $i < count($tmp_items); $i ++){
			$item = $tmp_items[$i];
			$pricing_id = $item["pricing_id"];
			$pricing_price = $item["pricing_price"];
			$service_id = $item["service_id"];
			$service_title = $item["service_title"];
			$service_category = $item["service_category"];
			$vendor_uid = $item["vendor_uid"];
			$order_id = $db -> createToken(TABLE_ORDERS, "id", 5, false);
			$order = [
				"id" => $order_id,
				"type" => '0',
				"uid" => $session_uid,
				"ref" => $ref,
				"event_id" => $event_id,
				"time_added" => $item["item_time_added"],
				"time_ordered" => $now,
				"pricing_id" => $pricing_id,
				"pricing_price" => $pricing_price,
				"pricing_title" => $item["pricing_title"],
				"pricing_description" => $item["pricing_description"],
				"pricing_package" => $item["pricing_package"],
				"pricing_timestamp" => $item["pricing_time_modified"],
				"service_id" => $service_id,
				"service_title" => $service_title,
				"service_category" => $service_category,
				"service_timestamp" => $item["service_time_modified"],
				"vendor_id" => $item["vendor_id"],
				"vendor_company" => $item["vendor_company"],
				"vendor_timestamp" => $item["vendor_time_modified"],
				"message" => $item["item_message"],
				"notes" => "",
				"status" => 0,
				"timestamp" => $now,
				"seen_user" => "",
				"seen_vendor" => "",
				"quote_id" => "",
				"payment_id" => "",
			];

			//save new order
			if (!$db -> insert(TABLE_ORDERS, $order, false, $conn, false)) throw new Exception($db -> getErrorMessage());
			$tmp_orders[] = $order;

			//notify vendor on quotation
			$pricing_price = fn1::toNum($pricing_price, 0, false);
			if (!$pricing_price){
				$ref = md5($order_id . ' - ' . $service_id . ' - ' . $vendor_uid . ' - notify-quote');
				$notify_refs[] = $ref;
				$service_name = $service_title . ' - ' . $service_category;
				if (!notify_vendor_quote($ref, $vendor_uid, $service_title, $order_id)) throw new Exception($notify_vendor_quote_error);
			}

			//delete cart item
			if (!$db -> delete(TABLE_CART, "id", "WHERE `id` = ? AND `uid` = ?", [$item["item_id"], $session_uid], $conn, false)) throw new Exception($db -> getErrorMessage());
		}

		//commit db changes and close
		if (!$conn -> commit()) throw new Exception($conn -> error, $conn -> errno);
		$db -> close();
		$conn = null;
		$db = null;
		$notify_refs = [];

		//success output
		$orders_count = count($tmp_orders);
		$tmp_vendor = $orders_count > 1 ? "vendors" : "vendor <strong><i>(" . $tmp_orders[0]["vendor_company"] . ")</i></strong>";
		$success_message = "<h2 class='uk-text-success'>Checkout Complete!</h2><p class='uk-text-success'><strong>$orders_count New Order" . ($orders_count > 1 ? "s have" : " has") .  " been created.</strong>";
		$success_message .= "<br>The $tmp_vendor will contact you <strong>within 48 hours</strong> with more details about the services you have ordered, quotations, agreements planning, and payments. Check out your <a href='$root/orders?open'>open orders</a> for follow up.</p>";
		s::success($tmp_orders, $success_message);
	}
	catch (Exception $e){
		$error = $e -> getMessage();
		if ($conn !== null && !$conn -> rollback()) $error .= " (Rollback Failed: " . $conn -> error . ")";
		if ($db !== null) $db -> close();
		if (count($notify_refs)){
			foreach ($notify_refs as $ref){
				if (!notification_clear($ref)) $error .= ' (Notification Clear Error: ' . $notification_clear_error . ')';
			}
		}
		s::error(null, $error);
	}
}

//fetch user cart items & events
$db = null;
$cart_items = [];
$user_events = [];
try {
	//db connection
	$db = new Database();

	//fetch cart items
	$result = $db -> select(TABLE_CART, ["id", "service_id", "timestamp"], "WHERE `uid` = ?", [$session_uid]);
	if ($result === false) throw new Exception($db -> getErrorMessage());
	if ($result instanceof mysqli_result && $result -> num_rows){
		while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
			$service_data = get_service_data($row["service_id"], [
				"vendor" => true,
				"pricing" => true,
				"rating" => false,
				"gallery" => false,
				"services" => false,
				"related" => false,
				"redirect_error" => false,
				"output_error" => false,
			]);
			if ($service_data === false) throw new Exception($get_service_data_error);
			$row = xstrip($row);
			$cart_items[] = [
				"id" => $row["id"],
				"timestamp" => $row["timestamp"],
				"data" => $service_data,
			];
		}
	}

	//fetch user events
	$today = fn1::now('Y-m-d');
	$uid = $db -> escapeString($session_uid);
	$table_events = $db -> escapeString(TABLE_EVENTS);
	$table_event_users = $db -> escapeString(TABLE_EVENT_USERS);

	$sql = "SELECT ";
	$sql .= "`$table_events`.id, ";
	$sql .= "`$table_events`.name, ";
	$sql .= "`$table_events`.status, ";
	$sql .= "IF(DATEDIFF(`$table_events`.`start_timestamp`, '$today') > 0, 2, IF (DATEDIFF(`$table_events`.`end_timestamp`, '$today') >= 0, 1, 0)) AS 'active' ";
	$sql .= "FROM `$table_events` ";
	$sql .= "LEFT JOIN `$table_event_users` ON `$table_event_users`.`event_id` = `$table_events`.`id`";
	$sql .= "WHERE `$table_events`.`status` = '1' ";
	$sql .= "AND (`$table_events`.`uid` = '$uid' OR (`$table_event_users`.`uid` = '$uid' AND `$table_event_users`.`role` IN ('0', '2'))) ";
	$sql .= "ORDER BY `$table_events`.`_index` DESC, `$table_event_users`.`role` ASC";


	$result = $db -> query($sql);
	if ($result === false) throw new Exception($db -> getErrorMessage());
	if ($result instanceof mysqli_result && $result -> num_rows){
		while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
			$row = xstrip($row);
			$tmp_active = (int) $row["active"];
			$tmp_status = (int) $row["status"];
			unset($row["status"]);
			unset($row["active"]);
			$temp = ($tmp_status == 2 ? "WISHLIST EVENT" : "");
			$temp .= (strlen($temp) ? " ~ " : "") . ($tmp_active == 2 ? "Active" : ($tmp_active == 0 ? "Past" : "Live"));
			$row["text"] = fn1::toStrn($row["name"], true) . " ($temp)";
			$user_events[] = $row;
		}
	}

	//close connection
	$db -> close();
}
catch (Exception $e){
	if ($db !== null) $db -> close();
	s::error(null, $e -> getMessage());
}

//send vendor notification on order
function notify_vendor_quote($ref, $vendor_uid, $service_name, $order_id){
	global $notify_vendor_quote_error, $notify_error, $notification_clear_error, $root;
	try {
		if (!($ref = fn1::toStrn($ref, true))) throw new Exception('Invalid notification reference');
		if (!($uid = fn1::toStrn($vendor_uid, true))) throw new Exception('Invalid vendor user reference');
		if (!($service_name = fn1::toStrn($service_name, true))) throw new Exception('Invalid service name');
		if (!($order_id = fn1::toStrn($order_id, true))) throw new Exception('Invalid order reference');
		$text = 'A client has requested a quotation for service "' . $service_name . '". Kindly contact the client to send them a custom quotation.';
		$action = $root . '/orders/?order=' . $order_id;
		$type = 0;
		$icon = 'shopping-basket';
		if (!notification_clear($ref)) throw new Exception($notification_clear_error);
		if (!notify($uid, $text, $action, $icon, $type, $ref)) throw new Exception($notify_error);
		return true;
	}
	catch (Exception $e){
		$notify_vendor_quote_error = $e -> getMessage();
		return false;
	}
}
