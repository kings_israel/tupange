<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-db-insert.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Plugin\Files;
use Naicode\Server\Database;

//handle form
if (isset($session_uid) && isset($request_params["company_name"]) && isset($request_params["company_email"])){
	//set uid
	$uid = $session_uid;
	
	//get values
	$company_name = fn1::strtocamel(fn1::toStrn($request_params["company_name"], true));
	$company_email = strtolower(fn1::toStrn($request_params["company_email"], true));
	$company_description = fn1::toStrn($request_params["company_description"], true);
	$company_phone_number = fn1::toStrn($request_params["company_phone_number"], true);
	$company_services = fn1::jsonParse($request_params["company_services"], true, []);
	$company_services_value = str_replace("\"", "'", fn1::jsonCreate($company_services));
	$location = fn1::jsonParse($request_params["location"], true, []);
	if (fn1::hasKeys($location, "lat", "lon", "name", "type")){
		$location_lat = (float) $location["lat"];
		$location_lon = (float) $location["lon"];
		$location_name = $location["name"];
		$location_type = $location["type"];
		$location_value = $location_name;
	}
	$location_map = fn1::jsonParse($request_params["location_map"], true, []);
	if (array_key_exists("lat", $location_map) && array_key_exists("lng", $location_map)){
		$location_map_lat = (float) $location_map["lat"];
		$location_map_lon = (float) $location_map["lng"];
		$location_map_value = str_replace("\"", "'", fn1::jsonCreate(["lat" => $location_map_lat, "lng" => $location_map_lon]));
	}

	$db = null;
	$conn = null;
	
	try {
		//validate values
		if (fn1::isEmpty($company_name)) throw new Exception("Kindly provide your company name");
		if (!fn1::isEmail($company_email)) throw new Exception("Invalid company email address \"$company_email\"");
		if (!fn1::isSafaricomNumber($company_phone_number)) throw new Exception("Invalid phone number. Please enter a Safaricom number.");
		if (fn1::isEmpty($company_services)) throw new Exception("Kindly select services offered");
		$vendor_services = [];
		foreach ($company_services as $service){
			if (!array_key_exists("name", $service) || !array_key_exists("value", $service)) throw new Exception("Invalid service value: " . fn1::jsonCreate($service));
			$vendor_services[] = ["name" => $service["name"], "value" => $service["value"]];
		}
		if (!isset($location_value)) throw new Exception("Kindly select a location");
		if (!isset($location_map_value)) throw new Exception("Kindly select a location on the map so clients can find your business.");
		

		//db connection
		$db = new Database();
		$id = $db -> createToken(TABLE_VENDORS, "id");
		$data = [
			"id" => $id,
			"uid" => $uid,
			"company_name" => $company_name,
			"company_email" => $company_email,
			"company_phone_number" => $company_phone_number,
			"company_description" => $company_description,
			"location_lat" => $location_lat,
			"location_lon" => $location_lon,
			"location_name" => $location_name,
			"location_type" => $location_type,
			"location_map_lat" => $location_map_lat,
			"location_map_lon" => $location_map_lon,
			"timestamp" => fn1::now()
		];

		// Company Logo
		if (isset($_FILES["company_logo"]) && $_FILES["company_logo"]["name"]){

			//check if image is too large
			list($width, $height) = @getimagesize($_FILES["company_logo"]['tmp_name']);
			$width = (int) $width;
			$height = (int) $height;
			if ($width > 350 || $height > 90) throw new Exception("Display image is too large (" . $width . "x" . $height . "). Kindly select a smaller image with dimensions less or equal to 350x90 for better quality.");
			if ($width < 350){
				$resized_height = round((((350 / $width) * 100) / 100) * $height, 0);
				if ($resized_height < 90) throw new Exception("Display image is too wide for its height (" . $width . "x" . $height . " will be resized to 350x$resized_height). Kindly select another image with dimensions greater or equal to 600x400 for better quality.");
			}

			//upload company logo
			$files = new Files("company_logo");
			$files -> setAllowedExtensions(["gif","jpg", "jpeg", "png"]);
			$files -> setMaxLimit(5);
			if (!$files -> fileUpload()) throw new Exception($files -> getErrorMessage());
			$uploads_folder = getUploads(true);
			$uploaded_image_src = $files -> uploaded[0]["src"];
			$uploaded_image = $uploads_folder . "/" . $uploaded_image_src;

			//add new service image
			$data["company_logo"] = $uploaded_image_src;
		}

		if (!isset($service_image_src) && !array_key_exists("company_logo", $data)) throw new Exception("Kindly select the company logo.");

		//db connecction without autocommit so we can rollback on error
		$conn = $db -> resetValues() -> getConnection(false);

		//insert company data
		if (!db_insert($db, $conn, TABLE_VENDORS, $data)) throw new Exception($db -> err, $db -> errno);

		//insert vendor services
		foreach ($vendor_services as $service){
			$data = [
				"vendor_id" => $id,
				"name" => $service["name"],
				"value" => $service["value"]
			];
			if (!db_insert($db, $conn, TABLE_VENDOR_CATEGORIES, $data)) throw new Exception($db -> err, $db -> errno);
		}

		//update user status to 1
		$sql = fn1::strbuild("UPDATE `%s` SET `status`= '1' WHERE `uid` = '%s'", TABLE_USERS, $uid);
		if (!$db -> query($sql, $conn)) throw new Exception($db -> err, $db -> errno);

		//commit db changes and close
		if (!$conn -> commit()) throw new Exception($conn -> error, $conn -> errno);
		$db -> close();
		$conn = null;
		$db = null;

		//successful, update session status and redirect
		$_SESSION["status"] = 1;
		header("location: $root/dashboard");
		exit();
	}
	catch (Exception $e){
		//error handling
		$complete_error = $e -> getMessage();
		if ($conn !== null && !$conn -> rollback()) $complete_error .= " (Rollback Failed: " . $conn -> error . ")";
		if ($db !== null) $db -> close();
	}
}
