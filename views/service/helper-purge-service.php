<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;

//helper permanently delete service and related data
function purge_service($vendor_id, $service_id){
	global $purge_service_error;
	$db = null;
	try {
		$db = new Database();
		$service = $db -> queryItem(TABLE_VENDOR_SERVICES, ["id", "image", "deleted"], "`id` = ? AND `vendor_id` = ? AND `deleted` = '1'", [$service_id, $vendor_id]);
		if ($service === false) throw new Exception($db -> getErrorMessage());
		if (!$service) throw new Exception("Deleted service not found");
		if (fn1::hasKeys($service, "id", "image", "deleted") && $service["id"] == $service_id && (int) $service["deleted"] == 1){

			//delete service from db
			if (!$db -> delete(TABLE_VENDOR_SERVICES, "id", "WHERE `id` = ?", [$service_id])) throw new Exception($db -> getErrorMessage());

			//delete service display image
			if (!fn1::isEmpty($service_image = fn1::toStrx($service["image"], true)) && fn1::isFile($service_image_path = getUploads(true) . "/" . $service_image)) fn1::fileDelete($service_image_path);

			//delete from cart
			if (!$db -> delete(TABLE_CART, "id", "WHERE `service_id` = ?", [$service_id])) throw new Exception($db -> getErrorMessage());

			//delete from favorites
			if (!$db -> delete(TABLE_FAVORITES, "id", "WHERE `service_id` = ?", [$service_id])) throw new Exception($db -> getErrorMessage());

			//TODO: delete service related data...
		}
		$db -> close();
		return true;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$purge_service_error = $e -> getMessage();
		return false;
	}
}
