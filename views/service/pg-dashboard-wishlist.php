<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//fetch user events (status = 2) //Wishlist
$db = null;
$user_events = [];
try {
	$db = new Database();
	if (!$result = $db -> select(TABLE_EVENTS, null, "WHERE `uid` = ? AND `status` = '2' ORDER BY `timestamp` DESC", [$session_uid])) throw new Exception($db -> getErrorMessage());
	if ($result -> num_rows){
		while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
			if ($event = event_from_data($row, false, false, false, false)) $user_events[] = $event;
		}
	}
	$db -> close();
}
catch (Exception $e){
	$error = $e -> getMessage();
	if ($db !== null) $db -> close();
	s::text($error);
}

//set past to last
$live = []; $active = []; $past = [];
foreach ($user_events as $value){
	if ($value["event_status"] == "Live") $live[] = $value;
	if ($value["event_status"] == "Active") $active[] = $value;
	if ($value["event_status"] == "Past") $past[] = $value;
}

//merge sorted
$user_events = fn1::arraysMerge($live, $active, $past);
