<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;
use Naicode\Server\Plugin\PDF;
use Naicode\Server\Plugin\PDFTable;

//invalid session check
if (!(isset($session_uid) && strlen($session_uid))) s::error(null, 'Invalid session!');

//Document: uk-datatable pdf
if (isset($request_params["uk_datatable_pdf"])){
	if (fn1::isEmpty($table_data = fn1::jsonParse($request_params["uk_datatable_pdf"], true, []))) s::error(null, "No table data provided");
	$title = isset($request_params["title"]) ? fn1::toStrn($request_params["title"], true) : null;
	$subtitle = isset($request_params["subtitle"]) ? fn1::toStrn($request_params["subtitle"], true) : null;
	if ($title) $title = str_replace("CURRENT_TIMESTAMP", fn1::now(), $title);
	if ($title) $title = str_replace("ROWS_COUNT", count($table_data) - 1, $title);
	if ($subtitle) $subtitle = str_replace("CURRENT_TIMESTAMP", fn1::now(), $subtitle);
	if ($subtitle) $subtitle = str_replace("ROWS_COUNT", count($table_data) - 1, $subtitle);
	$logo = isset($request_params["logo"]) ? fn1::toStrn($request_params["logo"], true) : null;
	$logo_width = isset($request_params["logo_width"]) && ($logo_width = fn1::toNum($request_params["logo_width"])) > 0 ? $logo_width : 40;
	$logo_page_align = isset($request_params["logo_page_align"]) ? fn1::toStrn($request_params["logo_page_align"], true) : "L";
	$logo_page_margin = isset($request_params["logo_page_margin"]) && ($logo_page_margin = fn1::toNum($request_params["logo_page_margin"])) > 0 ? $logo_page_margin : 10;
	$page_size = isset($request_params["page_size"]) ? fn1::toStrn($request_params["page_size"], true) : "A4";
	$page_orientation = isset($request_params["page_orientation"]) ? fn1::toStrn($request_params["page_orientation"], true) : "L";
	$table_options = isset($request_params["table_options"]) && !fn1::isEmpty($table_options = fn1::jsonParse($request_params["table_options"], true, [])) ? $table_options : [];
	$pdf = new PDF($page_orientation, $page_size);
	$pdf -> setDefaultHeaderItems($title, $subtitle, $logo, $logo_width, $logo_page_align, $logo_page_margin);
	$table = $pdf -> table($table_data);
	foreach ($table_options as $prop => $value) $table -> set($prop, $value);
	$pdf_data = $table -> create() -> pdf() -> getString();
	s::success(base64_encode($pdf_data));
}

//Document: uk-datatable download
if (isset($request_params["uk_datatable_download"])){
	if (fn1::isEmpty($table_data = fn1::jsonParse($request_params["uk_datatable_download"], true, []))) s::error(null, "No table data provided");
	$filename = isset($request_params["filename"]) ? fn1::toStrn($request_params["filename"], true) : null;
	if (!$filename) $filename = "download";
	$csv_data = fn1::arrayCSV($table_data, false, $filename);
	//s::error($csv_data, 'testing $csv_data');
	if (!$csv_data) s::error(null, "Unable to create export document.");
	s::success([
		"chunk_size" => 512,
		"type" => "text/csv;charset=utf-8;",
		"filename" => $csv_data["filename"],
		"content" => base64_encode($csv_data["csv"]),
	]);
}

//Invitations
class InvitationsPDF extends PDF {
	const GUEST_TYPES = [0 => "General Admission", 1 => "VIP", 2 => "VVIP"];
	const cw = [255, 255, 255];
	const cg = [140, 193, 76];
	const c2 = [2, 2, 2];
	const c68 = [68, 68, 68];
	const c85 = [85, 85, 85];
	const c133 = [133, 133, 133];
	const f1 = 20; //31.5;
	const f2 = 15; //18;
	const fp = 9;
	const w100 = 75;
	const w150 = 112.5;
	const wInv = 80;
	const wSep = 50;
	const h5 = 5;
	public $error;
	public $event;
	public $guests;
	public $barcodes;
	public $event_id;
	private $inv_x;
	public $is_ticket = false;
	public function __construct($event_id, $barcodes, $page_orientation="P", $page_size="A4"){
		parent::__construct($page_orientation, $page_size);
		$this -> event_id = fn1::toStrx($event_id, true);
		$this -> barcodes = fn1::trimVal(fn1::toArray($barcodes));
	}
	public function create(){
		global $event_invites_error;
		$items_data = false;
		if ($this -> is_ticket) $items_data = self::eventTickets($this -> event_id, $this -> barcodes);
		else $items_data = self::eventInvites($this -> event_id, $this -> barcodes);
		if ($items_data === false){
			$this -> error = $event_invites_error;
			return false;
		}
		$this -> event = $items_data["event"];
		$this -> guests = $items_data["guests"];
		if (!count($this -> guests)){
			$this -> error = "No guests found";
			return false;
		}
		//create invites per guest
		$n = 0;
		$pw = $this -> GetPageWidth();
		$px = ($pw / 2) - (((self::wInv * 2) + 10) / 2);
		$guests = $this -> guests;
		if (count($guests) == 1) $px = ($pw / 2) - (self::wInv / 2);
		$sx = $px;
		$sy = 10;
		foreach ($guests as $guest){
			if ($n == 0){
				//new page
				$this -> addPage(null, null, null, false);
				$this -> setRGBColor(self::cw, self::c2, [130, 130, 130]);
				$sx = $px;
				$sy = 10;
			}
			$this -> inv_x = $sx;
			if ($this -> drawGuestItem($guest, $sx, $sy) === false){
				return false;
			}
			$sx = $sx + self::wInv + 10;
			$n ++;
			if ($n > 1) $n = 0;
		}
	}
	public function Ln($h=null){
		parent::Ln($h);
		$this -> SetX($this -> inv_x);
	}
	private function drawGuestItem($guest, $sx, $sy, $draw=true){
		$root = NS_SITE;
		$event = $this -> event;
		$is_ticket = $this -> is_ticket;
		if (!(is_array($event) && array_key_exists("name", $event))){
			$this -> error = "Invalid event data";
			return false;
		}
		if (!(is_array($guest) && array_key_exists("id", $guest))){
			$this -> error = "Invalid guest data";
			return false;
		}

		//set xy
		$this -> SetXY($sx, $sy);
		$this -> Ln(5);

		//title
		$temp = $is_ticket ? 'T I C K E T' : 'I N V I T A T I O N';
		$this -> drawTextLine($temp, self::cg, self::f1, self::h5);

		//guest type
		$setting_guest_type = $is_ticket ? 1 : (int) fn1::propval($event, "settings", "type");
		if ($setting_guest_type){
			$guest_type = $is_ticket ? 1 : (int) $guest["type"];
			$guest_type_text = $is_ticket ? $guest['ticket_title']  : strtoupper(self::GUEST_TYPES[$guest_type]);
			$this -> Ln(1);
			$this -> drawTextLine("~ $guest_type_text ~", self::cg, self::fp, self::h5, "B");
			$this -> drawSeparator(1, 5); //separator
		}
		else $this -> drawSeparator(5, 6); //separator

		//to
		$guest_names = $is_ticket ? $guest['names'] : strtoupper($guest["first_name"] . " " . $guest["last_name"]);
		$text = "To: " . $guest_names;
		$this -> drawTextLine($text, self::c2, self::f2);


		//message
		$this -> Ln(2);
		if (!$is_ticket) $this -> drawTextLine("Congratulations! You are invited!", self::c85, self::fp, self::h5, "B");
		if (!$is_ticket && strlen($guest["inv_message"])) $this -> drawTextLine('"' . $guest["inv_message"] . '"', self::c85, self::fp);
		if ($is_ticket && strlen($guest["notes"])) $this -> drawTextLine('"' . $guest["notes"] . '"', self::c85, self::fp);

		//barcode
		$this -> Ln(5);
		$scan_mode = (int) fn1::propval($event, "settings", "scan_mode");
		if ($scan_mode == 1){
			$image_url = $root . "/?qr_code=" . $guest["barcode"] . "&size=10&margin=1";
			$this -> drawImage($image_url, 25, 25);
		}
		else {
			$image_url = $root . "/?barcode=" . $guest["barcode"] . "&width=200&height=50";
			$this -> drawImage($image_url, 35, 10);
		}

		//barcode text
		$admit = $is_ticket ? (int) $guest["guests"] : ((int) $guest["extend_inv"] ? (int) $guest["expected_guests"] : 1);
		if ($admit < 1) $admit = 1;
		$text = "(admit $admit guest" . ($admit > 1 ? "s" : "") . ")";
		$this -> drawTextPair($guest["barcode"], $text, self::c85, self::c85, "B", "I", self::fp, self::h5);

		//separator
		$this -> drawSeparator(5, 3);

		//details title
		$this -> drawTextLine("DETAILS", self::cg, self::f2, self::h5);
		$this -> Ln(3);

		//event title
		$this -> drawTextLine($event["name"], self::c2, self::f2, self::h5);

		//event type
		$this -> drawTextLine("(" . fn1::propval($event, "type", "name") . ")", self::c85, self::fp, self::h5);

		//event description
		if (strlen($event["description"])) $this -> drawTextLine($event["description"], self::c85, self::fp, self::h5);

		//event date
		$event_start = fn1::propval($event, "start", 3);
		$event_end = fn1::propval($event, "end", 3);
		if ($event_start == $event_end) $this -> drawTextPair("Event Date:", $event_start, self::c85, self::c85, "B", "", self::fp, self::h5);
		else {
			$this -> drawTextPair("Event Start: ", $event_start, self::c85, self::c85, "B", "", self::fp, self::h5);
			$this -> drawTextPair("Event End: ", $event_end, self::c85, self::c85, "B", "", self::fp, self::h5);
		}
		$location_name = fn1::toStrx(fn1::propval($event, "location", "name"), true);
		if (strlen($location_name)) $this -> drawTextPair("Location: ", $location_name, self::c85, self::c85, "B", "", self::fp, self::h5);
		$this -> Ln(3);

		//rsvp
		if (!$is_ticket){
			$rsvp_link = $root . "/event/" . $event["id"] . "/?rsvp=" . $guest["barcode"];
			$x = $this -> GetX();
			$y = $this -> GetY();
			$bw = 25;
			$ix = $x + ((self::wInv / 2) - ($bw / 2));
			$this -> SetX($ix);
			$this -> setRGBColor(self::cg, self::cw, self::cg);
			$this -> Cell($bw, 8, "RSVP", "LRTB", 0, "C", true, $rsvp_link);
			$this -> Ln();
			$this -> Ln(5);
		}

		//event map
		$event_map_image = fn1::toStrx(fn1::propval($event, "location_map_image"), true);
		if (strlen($event_map_image)) $this -> drawImage($event_map_image, self::wInv, 50, $event_map_image);

		//powered by
		$x = $this -> GetX();
		$y = $this -> GetY();
		$this -> setRGBColor(self::c68, self::cw, self::c68);
		$this -> Cell(self::wInv, 23, "", "LRB", 0, "", true);
		$this -> SetXY($x, $y);
		$this -> Ln(3);
		$this -> drawTextLine("Powered By", [200, 200, 200], self::fp, self::h5);
		$this -> Ln(2);
		$image_url = $root . "/assets/img/logow.png";
		$this -> drawImage($image_url, 25, 8, $root);

		//draw border
		$x = $this -> GetX();
		$y = $this -> GetY();
		$h = ($y + self::h5) - $sy;
		$this -> SetXY($sx, $sy - 2);
		$this -> setRGBColor(self::cg, null, self::cg);
		$this -> Cell(self::wInv, 2, "", "LR", 0, "", true);
		$this -> Ln();
		$this -> setRGBColor(null, null, [130, 130, 130]);
		$this -> Cell(self::wInv - 0.1, $h, "", "LR", 0, "", false);
		$this -> Ln();
	}
	private function drawSeparator($h1=3, $h2=3){
		$this -> Ln($h1);
		$this -> setRGBColor(null, null, [200, 200, 200]);
		$x = $this -> GetX();
		$y = $this -> GetY();
		$lx = $x + ((self::wInv / 2) - (self::wSep / 2));
		$this -> Line($lx, $y, $lx + self::wSep, $y);
		$this -> SetXY($x, $y);
		$this -> Ln($h2);
	}
	private function drawTextLine($text, $color=self::c2, $font_size=self::fp, $cell_height=self::h5, $font_style=""){
		$this -> setRGBColor(null, $color);
		$this -> setFont($this -> font, $font_style, $font_size);
		$w = self::wInv - 8;
		$x = $this -> GetX();
		$y = $this -> GetY();
		$ix = $x + ((self::wInv / 2) - ($w / 2));
		$this -> SetXY($ix, $y);
		$lines = PDFTable::NbLines($this, $text, $w);
		$h = $this -> mCell($text, $w, $lines, $cell_height, 0, "C");
		$this -> SetXY($x, $y);
		$this -> Ln($h);
		return $h;
	}
	private function drawTextPair($text1, $text2, $color1=self::c2, $color2=self::c2, $font_style1="", $font_style2="", $font_size=self::fp, $cell_height=self::h5){
		$max = self::wInv;
		$wx = $max / 2;
		$x = $this -> GetX();
		$y = $this -> GetY();
		$this -> SetFont("", $font_style1, $font_size);
		$w1 = (int) $this -> GetStringWidth($text1);
		$this -> SetFont("", $font_style2, $font_size);
		$w2 = (int) $this -> GetStringWidth($text2);
		$w1 += 5;
		$w2 += 5;
		$w = $w1 + $w2;
		$ix = $x + ($wx - ($w / 2));
		$this -> SetX($ix + 1);
		$this -> setRGBColor(null, $color1);
		$this -> SetFont("", $font_style1, $font_size);
		$lines = PDFTable::NbLines($this, $text1, $w1);
		$h1 = $this -> mCell($text1, $w1, $lines, $cell_height, 0, "R");
		$this -> SetX($ix + $w1);
		$this -> setRGBColor(null, $color2);
		$this -> SetFont("", $font_style2, $font_size);
		$lines = PDFTable::NbLines($this, $text2, $w2);
		$h2 = $this -> mCell($text2, $w2, $lines, $cell_height, 0, "L");
		$h = $h1;
		if ($h < $h2) $h = $h2;
		$this -> Ln($h);
	}
	private function drawImage($image, $w, $h, $link=null){
		$x = $this -> GetX();
		$y = $this -> GetY();
		$ix = $x + ((self::wInv / 2) - ($w / 2));
		$this -> Image($image, $ix, $y, $w, $h, "PNG", $link);
		$this -> Ln($h);
	}
	public static function eventInvites($event_id, $barcodes){
		global $event_invites_error;
		$db = null;
		try {
			//get params
			$event_id = fn1::toStrx($event_id, true);
			$barcodes = fn1::trimVal(fn1::toArray($barcodes));

			//validate params
			if (fn1::isEmpty($event_id)) throw new Exception("Invalid event reference");
			if (fn1::isEmpty($barcodes)) throw new Exception("No invitation barcodes provided");

			//db connection
			$db = new Database();

			//fetch event data
			$event_data = $db -> queryItem(TABLE_EVENTS, null, "`id` = ?", [$event_id]);
			if ($event_data === false) throw new Exception($db -> getErrorMessage());
			if (!$event_data) throw new Exception("Event not found");

			//prep query barcodes
			$tmp_barcodes = [];
			foreach ($barcodes as $barcode) if (strlen($barcode = fn1::toStrn($barcode, true))) $tmp_barcodes[] = fn1::strbuild('"%s"', $db -> escapeString($barcode));
			if (!count($tmp_barcodes)) throw new Exception("No valid invitation barcodes provided");
			$tmp_barcodes = implode(",", $tmp_barcodes);

			//fetch barcodes' guests
			$result = $db -> select(TABLE_EVENT_GUESTS, null, "WHERE `event_id` = ? AND `barcode` IN ($tmp_barcodes) ORDER BY `time_modified` DESC", [$event_id]);
			if ($result === false) throw new Exception($db -> getErrorMessage());
			$event_guests = [];
			if ($result instanceof mysqli_result) while ($row = $result -> fetch_array(MYSQLI_ASSOC)) $event_guests[] = xstrip($row);

			//close connection
			$db -> close();

			//return invites data
			return ["event" => event_from_data($event_data), "guests" => $event_guests];
		}
		catch (Exception $e){
			//handle errors
			if ($db !== null) $db -> close();
			$event_invites_error = $e -> getMessage();
			return false;
		}
	}
	public static function eventTickets($event_id, $barcodes){
		global $event_invites_error;
		$db = null;
		try {
			//get params
			$event_id = fn1::toStrx($event_id, true);
			$barcodes = fn1::trimVal(fn1::toArray($barcodes));

			//validate params
			if (fn1::isEmpty($event_id)) throw new Exception("Invalid event reference");
			if (fn1::isEmpty($barcodes)) throw new Exception("No ticket barcodes provided");

			//db connection
			$db = new Database();

			//fetch event data
			$event_data = $db -> queryItem(TABLE_EVENTS, null, "`id` = ?", [$event_id]);
			if ($event_data === false) throw new Exception($db -> getErrorMessage());
			if (!$event_data) throw new Exception("Event not found");

			//prep query barcodes
			$tmp_barcodes = [];
			foreach ($barcodes as $barcode) if (strlen($barcode = fn1::toStrn($barcode, true))) $tmp_barcodes[] = fn1::strbuild('"%s"', $db -> escapeString($barcode));
			if (!count($tmp_barcodes)) throw new Exception("No valid ticket barcodes provided");
			$tmp_barcodes = implode(",", $tmp_barcodes);

			//fetch barcodes' guests
			$result = $db -> select(TABLE_REGISTER, null, "WHERE `event_id` = ? AND `barcode` IN ($tmp_barcodes) ORDER BY `time_modified` DESC", [$event_id]);
			if ($result === false) throw new Exception($db -> getErrorMessage());
			$event_guests = [];
			if ($result instanceof mysqli_result) while ($row = $result -> fetch_array(MYSQLI_ASSOC)) $event_guests[] = xstrip($row);

			//close connection
			$db -> close();

			//return invites data
			return ["event" => event_from_data($event_data), "guests" => $event_guests];
		}
		catch (Exception $e){
			//handle errors
			if ($db !== null) $db -> close();
			$event_invites_error = $e -> getMessage();
			return false;
		}
	}
}

//Document: invitation pdf
if (isset($request_params["invitations_pdf"]) && isset($request_params["barcodes"])){
	$event_id = fn1::toStrx($request_params["invitations_pdf"], true);
	$barcodes = fn1::jsonParse($request_params["barcodes"], true, []);
	$pdf = new InvitationsPDF($event_id, $barcodes);
	if ($pdf -> create() === false) s::error($pdf -> error);
	$pdf_data = $pdf -> getString();
	s::success(base64_encode($pdf_data));
}

//Document: ticket pdf
if (isset($request_params["tickets_pdf"]) && isset($request_params["barcodes"])){
	$event_id = fn1::toStrx($request_params["tickets_pdf"], true);
	$barcodes = fn1::jsonParse($request_params["barcodes"], true, []);
	$pdf = new InvitationsPDF($event_id, $barcodes);
	$pdf -> is_ticket = true;
	if ($pdf -> create() === false) s::error($pdf -> error);
	$pdf_data = $pdf -> getString();
	s::success(base64_encode($pdf_data));
}

//Default response
s::error(null, 'Request not supported!');
