<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
require_once __DIR__ . "/messenger.php";
require_once __DIR__ . "/notifications.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

const ROLES = [
	0 => 'Manage All',
	1 => 'Guests & Attendance',
	2 => 'Budget & Orders',
];

const STATUSES = [
	0 => 'Invite Sent',
	1 => 'Active',
];

//fetch user event
if (!$event_data = fetch_event($event_id, $session_uid)){
	//handle event not found
	header("location: $root/dashboard?events");
	exit();
}

//check guest user limit
if ($event_data['guest'] && !in_array($event_data['role'], [0])){
	header("location: $root/dashboard?events");
	exit();
}

//edit user
if (isset($request_params['event_user']) && isset($request_params['email']) && isset($request_params['role'])){
	$id = fn1::toStrx($request_params['event_user'], true);
	$email = strtolower(fn1::toStrx($request_params['email'], true));
	$username = strtolower(fn1::toStrn(str_replace('@', '', $request_params['username']), true));
	$names = fn1::strtocamel(fn1::toStrn($request_params['names'], true));
	$role = (int) $request_params['role'];
	$db = null;
	try {
		if (!fn1::isEmail($email)) throw new Exception('Kindly provide a valid email address');
		if (!array_key_exists($role, ROLES)) throw new Exception('Invalid role selection "' . $role . '"');
		if ($username === '') throw new Exception('Kindly provide a unique username for the user');
		if ($username === 'admin') throw new Exception('Username "admin" is system reserved');
		if ($names === '') throw new Exception('Kindly provide user full names');
		$db = new Database();
		if (($email_user = $db -> queryItem(TABLE_USERS, 'uid', 'WHERE `email` = ?', [$email])) === false) throw new Exception($db -> getErrorMessage());
		if ($email_user && $event_data['uid'] == $email_user['uid']) throw new Exception('Email address "' . $email . '" cannot be used because it is linked to your account');
		if ($id != ''){
			if (($existing = $db -> queryItem(TABLE_EVENT_USERS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) === false) throw new Exception($db -> getErrorMessage());
			if (!$existing) throw new Exception('Event user does not exist!');
		}
		if (($username_exists = $db -> queryExists(TABLE_EVENT_USERS, 'WHERE `event_id` = ? AND `id` <> ? AND `username` = ?', [$event_id, $id, $username])) === false) throw new Exception($db -> getErrorMessage());
		if ($username_exists) throw new Exception('Username "' . $username . '" is already in use by another event user');
		$timestamp = fn1::now();
		$data = [
			 'email' => $email,
			 'username' => $username,
			 'names' => $names,
			 'role' => $role,
			 'timestamp' => $timestamp
		];
		if ($id == ''){
			$id = $db -> createToken(TABLE_EVENT_USERS, 'id');
			$data['id'] = $id;
			$data['event_id'] = $event_id;
			$data['uid'] = '';
			$data['status'] = 0;
			if (($invite_link = send_invite($id, $email, $names, $username)) === false) throw new Exception($send_invite_error);
			if ($email_user){
				$notice = 'You have been invited to collaborate on the event "' . $event_data['name'] . '"';
				if (!notify($email_user['uid'], $notice, $invite_link, 'users', NOTIFICATION_TYPE_SYSTEM)) throw new Exception($notify_error);
			}
			if (!$db -> insert(TABLE_EVENT_USERS, $data)) throw new Exception($db -> getErrorMessage());
			$mes = 'Event user has been created and an invite sent to "' . $email . '"';
		}
		else {
			if (!$db -> update(TABLE_EVENT_USERS, $data, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) throw new Exception($db -> getErrorMessage());
			$data = array_replace_recursive($existing, $data);
			$mes = 'Event user details have been updated successfully';
		}
		$db -> close();
		unset($data['_index']);
		unset($data['event_id']);
		s::success($data, $mes);
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//delete user
if (isset($request_params['delete_user'])){
	$id = fn1::toStrx($request_params['delete_user'], true);
	$db = null;
	try {
		if ($id == '') throw new Exception('Invalid user reference');
		$db = new Database();
		if (($exists = $db -> queryExists(TABLE_EVENT_USERS, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$exists) throw new Exception('Event user does not exist!');
		if (!$db -> delete(TABLE_EVENT_USERS, 'id', 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		s::success($id, 'Event user has been deleted successfully!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//resend invite
if (isset($request_params['resend_invite'])){
	$id = fn1::toStrx($request_params['resend_invite'], true);
	$db = null;
	try {
		if ($id == '') throw new Exception('Invalid user reference');
		$db = new Database();
		if (($existing = $db -> queryItem(TABLE_EVENT_USERS, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$existing) throw new Exception('Event user does not exist!');
		$email = $existing['email'];
		$names = $existing['names'];
		$username = $existing['username'];
		$db -> close();
		if (!send_invite($id, $email, $names, $username)) throw new Exception($send_invite_error);
		s::success($id, 'Invite has been sent to "' . $email . '" successfully!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//fetch users
$users = fetch_event_users();
if (!$users) s::error(null, $fetch_event_users_error);

//event data
$event = event_from_data($event_data);

//send invite
function send_invite($id, $email, $names, $username){
	global $send_invite_error, $event_data, $event_id, $get_user_error, $root;
	try {
		if (($owner = get_user($event_data['uid'])) === false) throw new Exception($get_user_error);
		$event_name = $event_data['name'];
		$owner_names = $owner['display_name'];
		$message = "<h1>User Invite</h1><p>Hello $names, you have been invited to collaborate on a Tupange Event \"$event_name\" by \"$owner_names\". Your event username is <strong>@$username</strong>. To accept, click the link below:</p>";
		$invite_link = $root . '/euser/' . $event_id . '/' . $id;
		if (!email_message('Tupange Events', 'no-reply@' . NS_DOMAIN, $names, $email, "User Invite | $event_name", $message, $invite_link, 'Join Now')) throw new Exception('Error sending invite message');
		return $invite_link;
	}
	catch (Exception $e){
		$send_invite_error = $e -> getMessage();
		return false;
	}
}

//fetch event users
function fetch_event_users(){
	global $fetch_event_users_error, $event_id, $event_data, $get_user_error;
	$db = null;
	try {
		$db = new Database();
		if (($owner = get_user($event_data['uid'])) === false) throw new Exception($get_user_error);
		$owner_username = fn1::toStrn($owner['username'], true);
		if ($owner_username === '') $owner_username = 'admin';
		if (($result = $db -> select(TABLE_EVENT_USERS, null, 'WHERE `event_id` = ? ORDER BY _index DESC', [$event_id])) === false) throw new Exception($db -> getErrorMessage());
		$users = [];
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$status = (int) $row['status'];
				$role = (int) $row['role'];
				$user = [
					'id' => $row['id'],
					'email' => $row['email'],
					'username' => $row['username'],
					'names' => $row['names'],
					'role' => $role,
					'role_text' => ROLES[$role],
					'status' => $row['status'],
					'status_text' => STATUSES[$status],
					'timestamp' => $row['timestamp'],
				];
				$users[] = $user;
			}
		}
		$users[] = [
			'id' => 'owner',
			'email' => $owner['email'],
			'username' => $owner_username,
			'names' => $owner['display_name'],
			'role' => 0,
			'role_text' => ROLES[0],
			'status' => 1,
			'status_text' => STATUSES[1],
			'timestamp' => $event_data['timestamp'],
		];
		$db -> close();
		$db = null;
		return $users;
	}
	catch (Exception $e){
		$fetch_event_users_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
































//eof
