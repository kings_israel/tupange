<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//constants
const REGISTRATION_TYPES = [0 => "Invited Guest", 1 => "Registered Guest"];
const GUEST_TYPES = [0 => "General Admission", 1 => "VIP", 2 => "VVIP"];

//fetch user event
if (!$event_data = fetch_event($event_id, $session_uid)){
	//handle event not found
	if (isset($request_params["search_guest"]) || isset($request_params["search_guest_barcode"])) s::error(null, isset($fetch_event_error) ? $fetch_event_error : "Event not found");
	header("location: $root/dashboard?events");
	exit();
}

//check guest user limit
if ($event_data['guest'] && !in_array($event_data['role'], [0, 1])){
	header("location: $root/dashboard?event=$event_id");
	exit();
}

//search query
if (isset($request_params['search_query'])){
	$search_query = fn1::toStrn($request_params['search_query'], true);
	if (($invite_results = search_query_fetch($event_id, TABLE_EVENT_GUESTS, ['barcode', 'first_name', 'last_name', 'phone', 'email'], $search_query)) === false) s::error(null, $search_query_fetch_error);
	if (($ticket_results = search_query_fetch($event_id, TABLE_REGISTER, ['barcode', 'names', 'phone', 'email'], $search_query, false)) === false) s::error(null, $search_query_fetch_error);
	s::success(array_merge($invite_results, $ticket_results), 'Search results');
}

//guest check in/out
if (isset($request_params['guest_check'])){
	$direction = strtoupper(fn1::toStrx($request_params['guest_check'], true));
	$names = fn1::toStrn($request_params['names'], true);
	$email = fn1::toStrn($request_params['email'], true);
	$phone = fn1::toStrn($request_params['phone'], true);
	$type = strtoupper(fn1::toStrx($request_params['type'], true));
	$barcode = strtoupper(fn1::toStrx($request_params['barcode'], true));
	$db = null;
	try {
		if (!in_array($direction, ['IN', 'OUT'])) $direction = 'IN';
		$direction_lower = strtolower($direction);
		if (!in_array($type, ['INVITE', 'TICKET'])) throw new Exception('Invalid guest type "' . $type . '"');
		if (!strlen($barcode)) throw new Exception('Invalid barcode for check ' . $direction_lower . ' request');
		if (!strlen($names)) throw new Exception('Invalid guest names for check ' . $direction_lower . ' request');
		$db = new Database();
		$timestamp = fn1::now();
		$data = [
			'id' => $db -> createToken(TABLE_ATTENDANCE, 'id'),
			'event_id' => $event_id,
			'type' => $type,
			'barcode' => $barcode,
			'names' => $names,
			'phone' => $phone,
			'email' => $email,
			'direction' => $direction,
			'timestamp' => $timestamp,
		];
		if (!$db -> insert(TABLE_ATTENDANCE, $data)) throw new Exception($db -> getErrorMessage());
		$db -> close();
		s::success($data, 'Guest check ' . $direction_lower . ' successful!');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}

//attendance delete entry
if (isset($request_params['delete_attendance_entry'])){
	$id = fn1::toStrx($request_params['delete_attendance_entry'], true);
	$db = null;
	try {
		if (!strlen($id)) throw new Exception('Invalid entry reference!');
		$db = new Database();
		if (($existing = $db -> queryItem(TABLE_ATTENDANCE, null, 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) === false) throw new Exception($db -> getErrorMessage());
		if (!$existing) throw new Exception('Entry was not found!');
		if (!$db -> delete(TABLE_ATTENDANCE, 'id', 'WHERE `event_id` = ? AND `id` = ?', [$event_id, $id])) throw new Exception($db -> getErrorMessage());
		$db -> close();
		s::success($existing, 'Attendance entry has been deleted');
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		s::error(null, $e -> getMessage());
	}
}



//fetch attendance logs
$db = new Database();
if (!$result = $db -> select(TABLE_ATTENDANCE, null, 'WHERE `event_id` = ? ORDER BY `timestamp` DESC', [$event_id])) die ($db -> getErrorMessage());
$attendance_list = [];
$attendance_all = [];
$attendance_invite = [];
$attendance_ticket = [];
if ($result instanceof mysqli_result){
	while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
		unset($row['_index']);
		$type = strtoupper($row['type']);
		$barcode = strtoupper($row['barcode']);
		$attendance_all[$barcode] = 1;
		if ($type == 'INVITE') $attendance_invite[$barcode] = 1;
		if ($type == 'TICKET') $attendance_ticket[$barcode] = 1;
		$attendance_list[] = xstrip($row, true);
	}
}
$db -> close();
$attendance_stats = ["all" => count($attendance_all), "invite" => count($attendance_invite), "ticket" => count($attendance_ticket)];

//event data
$event = event_from_data($event_data);


//fn1::printr(['$attendance_stats' => $attendance_stats, '$attendance_list' => $attendance_list]); exit(); //TODO remove test

//helper functions
function search_query_fetch($event_id, $table, $fields, $query, $invite=true){
	global $search_query_fetch_error;
	$db = null;
	try {
		$db = new Database();
		$event_id = $db -> escapeString($event_id);
		$table = $db -> escapeString($table);
		$query = $db -> escapeString($query);
		$sql_or = [];
		foreach ($fields as $field) $sql_or[] = "`$field` LIKE '%$query%'";
		$sql = "SELECT * FROM `$table` WHERE `event_id`='$event_id' AND (" . implode(' OR ', $sql_or) . ") ORDER BY `time_modified` DESC LIMIT 10";
		if (!$result = $db -> query($sql)) throw new Exception($db -> getErrorMessage());
		$items = [];
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$item = [
					'type' => $invite ? 'INVITE' : 'TICKET',
					'names' => $invite ? $row['first_name'] . ' ' . $row['last_name'] : $row['names'],
					'phone' => $row['phone'],
					'email' => $row['email'],
					'barcode' => $row['barcode'],
				];
				$items[] = $item;
			}
		}
		$db -> close();
		return $items;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$search_query_fetch_error = $e -> getMessage();
		return false;
	}
}
