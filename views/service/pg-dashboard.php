<?php
#include ns library
require_once __DIR__ . "/../../includes.php";
require_once __DIR__ . "/helper-purge-event.php";
require_once __DIR__ . "/helper-event.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Database;
use Naicode\Server as s;

//load session
include_once __DIR__ . "/session.php";

//redirect invalid session uid
if (!(isset($session_uid) && strlen($session_uid))){
	header("location: $root/dashboard");
	exit();
}

//euser check
$user_fingerprint = userFingerprint();
$temp_euser = fn1::toStrn(temp_get($user_fingerprint, 'euser'), true);
if ($temp_euser !== ''){
	temp_delete($user_fingerprint, 'euser');
	if (count($parts = explode(',', $temp_euser)) == 2){
		$euser_event_id = $parts[0];
		$euser_id = $parts[1];
		if (!add_event_user($euser_event_id, $euser_id, $session_uid)) s::error(null, $add_event_user_error);
		unset($euser_event_id);
		unset($euser_id);
	}
}
unset($user_fingerprint);
unset($temp_euser);

//redirect incomplete vendor account
if ($session_userlevel == 2 && $session_status == 0){
	header("location: $root/complete");
	exit();
}

//page variables
$page_title = "Tupange | Dashboard";
$page_description = "Tupange user dashboard";
$page_scripts_pre = "";
$page_scripts = "dash";
$page_styles = "dash";
$page_parts = [];

//views
if (isset($_GET["events"])){
	$page_title = "Tupange | My Events";
	$page_description = "Tupange user events";
	$page_scripts = "dash-events";
	$page_styles = "dash-events";
	$page_parts[] = __DIR__ . "/../parts/part-dash-events.php";
	include_once __DIR__ . "/pg-dashboard-events.php";
}
elseif (isset($_GET["wishlist"])){
	$page_title = "Tupange | My Wishlist";
	$page_description = "Tupange user events wishlist";
	$page_scripts = "dash-events";
	$page_styles = "dash-events";
	$page_parts[] = __DIR__ . "/../parts/part-dash-wishlist.php";
	include_once __DIR__ . "/pg-dashboard-wishlist.php";
}
elseif (isset($_GET["cart"])){
	$page_title = "Tupange | My Cart";
	$page_description = "Tupange user services shopping cart";
	$page_scripts = "dash-cart";
	$page_styles = "dash-cart";
	$page_parts[] = __DIR__ . "/../parts/part-dash-cart.php";
	include_once __DIR__ . "/pg-dashboard-cart.php";
}
elseif (isset($_GET["edit-event"])){
	$page_title = "Tupange | Create Event";
	$page_description = "Tupange create new event";
	$page_scripts_pre = "dash-edit-event";
	$page_scripts = "dash-edit-event";
	$page_styles = "dash-edit-event";
	$event_id = fn1::toStrx($_GET["edit-event"], true);
	$event_is_new = true;
	if (strlen($event_id)){
		$event_is_new = false;
		$page_title = "Tupange | Edit Event";
		$page_description = "Tupange edit event details";
	}
	else unset($event_id);
	$page_parts[] = __DIR__ . "/../parts/part-dash-edit-event.php";
	include_once __DIR__ . "/pg-dashboard-event-edit.php";
}
elseif (isset($_GET["delete-event"])){
	$event_id = fn1::toStrx($_GET["delete-event"], true);
	$purge_event = purge_event($session_uid, $event_id);
	if ($purge_event === false) s::text(isset($purge_event_error) ? $purge_event_error : "Error removing event records!");
	$redirect_location = $root . "/dashboard?events";
	if ($purge_event == 2) $redirect_location = $root . "/dashboard?wishlist";
	header("location: $redirect_location");
	exit();
}
elseif (isset($_GET["event"])){
	$page_title = "Tupange | Event Dashboard";
	$page_description = "Tupange event management dashboard";
	$page_scripts_pre = "";
	$page_scripts = "dash-event";
	$page_styles = "dash-event";
	$event_id = fn1::toStrx($_GET["event"], true);
	$page_parts[] = __DIR__ . "/../parts/part-dash-event.php";
	include_once __DIR__ . "/pg-dashboard-event.php";
}
elseif (isset($_GET["event-guests"])){
	$event_child_view = true;
	$page_title = "Tupange | Event Guest List & Invitations";
	$page_description = "Tupange event guest list management";
	$page_scripts_pre = "";
	$page_scripts = "dash-event-guests";
	$page_styles = "dash-event-guests";
	$event_id = fn1::toStrx($_GET["event-guests"], true);
	$page_parts[] = __DIR__ . "/../parts/part-dash-event-guests.php";
	include_once __DIR__ . "/pg-dashboard-event-guests.php";
}
elseif (isset($_GET["event-attendance"])){
	$event_child_view = true;
	$page_title = "Tupange | Event Guests Attendance";
	$page_description = "Tupange event guests attendance management";
	$page_scripts_pre = "";
	$page_scripts = "dash-event-attendance";
	$page_styles = "dash-event-attendance";
	$event_id = fn1::toStrx($_GET["event-attendance"], true);
	$page_parts[] = __DIR__ . "/../parts/part-dash-event-attendance.php";
	include_once __DIR__ . "/pg-dashboard-event-attendance.php";
}
elseif (isset($_GET["event-registration"])){
	$event_child_view = true;
	$page_title = "Tupange | Event Registration";
	$page_description = "Tupange event registration & tickets";
	$page_scripts_pre = "";
	$page_scripts = "dash-event-registration";
	$page_styles = "dash-event-registration";
	$event_id = fn1::toStrx($_GET["event-registration"], true);
	$page_parts[] = __DIR__ . "/../parts/part-dash-event-registration.php";
	include_once __DIR__ . "/pg-dashboard-event-registration.php";
}
elseif (isset($_GET["event-gifts"])){
	$event_child_view = true;
	$page_title = "Tupange | Event Gifts Registry";
	$page_description = "Tupange event gifts registry";
	$page_scripts_pre = "";
	$page_scripts = "dash-event-gifts";
	$page_styles = "dash-event-gifts";
	$event_id = fn1::toStrx($_GET["event-gifts"], true);
	$page_parts[] = __DIR__ . "/../parts/part-dash-event-gifts.php";
	include_once __DIR__ . "/pg-dashboard-event-gifts.php";
}
elseif (isset($_GET["event-budget"])){
	$event_child_view = true;
	$page_title = "Tupange | Event Budget & Expenses";
	$page_description = "Tupange event budget & expenses management";
	$page_scripts_pre = "";
	$page_scripts = "dash-event-budget";
	$page_styles = "dash-event-budget";
	$event_id = fn1::toStrx($_GET["event-budget"], true);
	$page_parts[] = __DIR__ . "/../parts/part-dash-event-budget.php";
	include_once __DIR__ . "/pg-dashboard-event-budget.php";
}
elseif (isset($_GET["event-services"])){
	$event_child_view = true;
	$page_title = "Tupange | Event Service Orders";
	$page_description = "Tupange event service orders management";
	$page_scripts_pre = "";
	$page_scripts = "dash-event-services";
	$page_styles = "dash-event-services";
	$event_id = fn1::toStrx($_GET["event-services"], true);
	$page_parts[] = __DIR__ . "/../parts/part-dash-event-services.php";
	include_once __DIR__ . "/pg-dashboard-event-services.php";
}
elseif (isset($_GET["event-checklist"])){
	$event_child_view = true;
	$page_title = "Tupange | Event Checklist";
	$page_description = "Tupange event manage checklist";
	$page_scripts_pre = "";
	$page_scripts = "dash-event-checklist";
	$page_styles = "dash-event-checklist";
	$event_id = fn1::toStrx($_GET["event-checklist"], true);
	$page_parts[] = __DIR__ . "/../parts/part-dash-event-checklist.php";
	include_once __DIR__ . "/pg-dashboard-event-checklist.php";
}
elseif (isset($_GET["event-users"])){
	$event_child_view = true;
	$page_title = "Tupange | Event Users";
	$page_description = "Tupange event shared access";
	$page_scripts_pre = "";
	$page_scripts = "dash-event-users";
	$page_styles = "dash-event-users";
	$event_id = fn1::toStrx($_GET["event-users"], true);
	$page_parts[] = __DIR__ . "/../parts/part-dash-event-users.php";
	include_once __DIR__ . "/pg-dashboard-event-users.php";
}
else {
	//redirect undefined view
	header("location: $root/dashboard?events");
	exit();
}

//dash stats
if (!$dash_stats = dash_stats()) s::text(isset($dash_stats_error) ? $dash_stats_error : "Error loading dashboard stats!");

//helper functions
function dash_stats(){
	global $session_uid;
	global $dash_stats_error;
	$db = null;
	$stats = ["normal" => 0, "wishlist" => 0, "cart" => 0];
	try {
		$db = new Database();
		$selects = [];
		$selects[] = fn1::strbuild("(SELECT count(*) FROM `%s` WHERE `uid` = '%s' AND `status` = '1') as 'count_normal'", TABLE_EVENTS, $session_uid);
		$selects[] = fn1::strbuild("(SELECT count(*) FROM `%s` WHERE `uid` = '%s' AND `status` = '2') as 'count_wishlist'", TABLE_EVENTS, $session_uid);
		$selects[] = "(SELECT 0) as 'count_cart'"; //TODO select cart items count
		$query = "SELECT " . implode(", ", $selects);
		if (!$result = $db -> query($query)) throw new Exception($db -> getErrorMessage());
		if ($row = $result -> fetch_array(MYSQLI_ASSOC)){
			$stats = [
				"normal" => (int) $row["count_normal"],
				"wishlist" => (int) $row["count_wishlist"],
				"cart" => (int) $row["count_cart"],
			];
		}
		$db -> close();
		return $stats;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$dash_stats_error = $e -> getMessage();
		return false;
	}
}
