<?php
//dashboard pre-processor
include __DIR__ . "/service/pg-dashboard.php";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=<?php echo $page_styles; ?>">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre=<?php echo $page_scripts_pre; ?>&min"></script>
</head>
<body>
<!-- menu -->
<?php include __DIR__ . "/parts/part-menu.php"; ?>
<!-- page -->
<section class="x-section-main">
	<div class="uk-container uk-container-center">
		<div class="uk-grid" data-uk-grid-margin>
			<div class="uk-width-medium-1-3 uk-width-large-1-4">
				<div class="x-box">
					<h2 class="x-box-title"> Menu <a id="menu-toggle" href="javascript:" onclick="toggleMenu()" class="uk-navbar-toggle uk-visible-small"></a></h2>
					<ul id="menu-toggle-content" class="uk-nav uk-nav-navbar uk-hidden-small">
					    <li class="<?php if (isset($_GET["events"])) echo "uk-active"; ?>">
							<a href="<?php echo $root; ?>/dashboard/?events">
								<i class="uk-icon-calendar"></i> My Events
								<?php if (isset($dash_stats)) echo '<span class="x-badge-count">' . $dash_stats["normal"] . '</span>'; ?>
							</a>
						</li>
					    <li class="<?php if (isset($_GET["wishlist"])) echo "uk-active"; ?>">
							<a href="<?php echo $root; ?>/dashboard/?wishlist">
								<i class="uk-icon-star"></i> My Wishlist
								<?php if (isset($dash_stats)) echo '<span class="x-badge-count">' . $dash_stats["wishlist"] . '</span>'; ?>
							</a>
						</li>
					    <li class="<?php if (isset($_GET["cart"])) echo "uk-active"; ?>">
							<a href="<?php echo $root; ?>/dashboard/?cart">
								<i class="uk-icon-shopping-cart"></i> My Cart
								<?php if (isset($dash_stats)) echo '<span class="x-badge-count">' . $dash_stats["cart"] . '</span>'; ?>
							</a>
						</li>
						<?php if (isset($event) && is_array($event) && isset($event_child_view) && $event_child_view){ ?>
						<li class="uk-active">
							<a href="<?php echo $root; ?>/dashboard/?event=<?php echo $event_id; ?>">
								<i class="uk-icon-calendar"></i> <?php echo $event["name"]; ?>
							</a>
						</li>
						<?php } ?>
					</ul>
					<script type="text/javascript">
						function toggleMenu(){
							let menu_content = document.getElementById("menu-toggle-content");
							if (menu_content.className.indexOf("x-display-block") > -1) menu_content.classList.remove("x-display-block");
							else menu_content.classList.add("x-display-block");
						}
					</script>
				</div>
				<?php if (!(isset($event_child_view) && $event_child_view || isset($_GET['event']))){ ?>
				<div class="x-box uk-margin-top x-pad-20 uk-hidden-small">
					<h2>New Event?</h2>
					<div class="uk-contrast">
						<a class="uk-button uk-button-success uk-width-1-1 uk-margin-small-top" href="<?php echo $root; ?>/dashboard?edit-event">Create Event</a>
					</div>
				</div>
				<?php } ?>
				<div class="x-box uk-margin-top x-pad-20 uk-hidden-small">
					<h2>Need Service?</h2>
					<div class="uk-contrast">
						<a class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-top" href="<?php echo $search_url; ?>">Request Order</a>
					</div>
				</div>
			</div>
			<div class="uk-width-medium-2-3 uk-width-large-3-4">
				<?php foreach ($page_parts as $page_part) include_once $page_part; ?>
			</div>
		</div>
	</div>
</section>
<!-- terms -->
<?php include __DIR__ . "/parts/part-terms.php"; ?>
<!-- footer -->
<?php include __DIR__ . "/parts/part-footer.php"; ?>
<!-- libraries -->
<?php if (isset($_GET["edit-event"]) || isset($_GET["event"])){ ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCisnVFSnc5QVfU2Jm2W3oRLqMDrKwOEoM&libraries=places"></script>
<?php } ?>
<script src="<?php echo $root; ?>/assets/js/scripts.php?p=<?php echo $page_scripts; ?>"></script>
</body>
</html>
