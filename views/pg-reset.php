<?php
$page_title = "Tupange | Reset Password";
$page_description = "Reset Password. Password recovery.";

//pre-processor
include __DIR__ . "/service/pg-reset.php";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=forgot">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<div class="x-card x-auth-card">
				<h1>Reset Password</h1>
				<p>Set up a new password for your account. This link can only be used once.</p>
				<form action="" method="post" class="uk-form uk-form-stacked">
					<?php if (isset($reset_error)){ ?>
					<div class="uk-alert uk-alert-danger" data-uk-alert>
						<a href="" class="uk-alert-close uk-close"></a>
						<p><?php echo $reset_error; ?></p>
					</div>
					<?php } if (isset($reset_success)){ ?>
					<div class="uk-alert uk-alert-success" data-uk-alert>
						<a href="" class="uk-alert-close uk-close"></a>
						<p><?php echo $reset_success; ?></p>
					</div>
					<?php } ?>
					<div class="uk-form-row">
						<label class="uk-form-label" for="reset_password">New Password</label>
						<div class="uk-form-controls">
							<input class="uk-width-1-1" type="password" id="reset_password" name="reset_password" value="<?php if (isset($reset_password)) echo $reset_password; ?>" autocomplete="new-password">
						</div>
					</div>
					<div class="uk-form-row">
						<label class="uk-form-label" for="reset_confirm_password">Re-Enter Password</label>
						<div class="uk-form-controls">
							<input class="uk-width-1-1" type="password" id="reset_confirm_password" name="reset_confirm_password" value="<?php if (isset($reset_confirm_password)) echo $reset_confirm_password; ?>" autocomplete="new-password">
						</div>
					</div>
					<div class="uk-form-row uk-text-right uk-contrast">
						<button type="submit" class="uk-button uk-button-success uk-width-1-1">Reset Password</button>
					</div>
					<div class="uk-form-row uk-text-center">
						<p><a href="<?php echo $root; ?>/login">Login Here</a></p>
					</div>
				</form>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p="></script>
</body>
</html>
