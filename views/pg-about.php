<?php
$page_title = "Tupange | About Us";
$page_description = "Information about the platform";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=about">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-2">
					<img class="x-img-plan" draggable="false" src="<?php echo $root; ?>/assets/img/about/plan.jpg" />
				</div>
				<div class="uk-width-medium-1-2">
					<h1>About Tupange</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus sapien,
						malesuada non ipsum at, auctor dictum urna. Donec at ex eu nisi efficitur gravida
						sit amet tempus eros. Proin interdum, ipsum aliquet efficitur dapibus, justo velit
						sagittis mauris, ut congue ipsum arcu eu quam. Sed tincidunt maximus lacus sed cursus.
						Vivamus vitae orci vitae erat elementum posuere sed ut purus.
						<div class="uk-text-left uk-margin-top">
							<a href="./search" class="uk-button uk-button-success x-min-200 uk-button-large"><i class="uk-icon-search"></i> Browse Services</a>
							<a href="./dashboard" class="uk-button uk-button-primary x-min-200 uk-button-large"><i class="uk-icon-calendar"></i> Create An Event</a>
						</div>
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="x-section-features">
		<div class="uk-container uk-container-center">
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-3">
					<div class="x-card x-feature">
						<img draggable="false" src="<?php echo $root; ?>/assets/img/about/event.png" />
						<div class="x-pad-20">
							<h2>Create &amp; Manage Events</h2>
							<p>
								Integer scelerisque maximus commodo. Interdum et malesuada fames ac ante
								ipsum primis in faucibus. Donec eleifend enim rutrum dignissim sollicitudin.
							</p>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-3">
					<div class="x-card x-feature">
						<img draggable="false" src="<?php echo $root; ?>/assets/img/about/service.png" />
						<div class="x-pad-20">
							<h2>Offer Your Services</h2>
							<p>
								Integer scelerisque maximus commodo. Interdum et malesuada fames ac ante
								ipsum primis in faucibus. Donec eleifend enim rutrum dignissim sollicitudin.
							</p>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-3">
					<div class="x-card x-feature">
						<img draggable="false" src="<?php echo $root; ?>/assets/img/about/order.png" />
						<div class="x-pad-20">
							<h2>Easy Order Checkout</h2>
							<p>
								Integer scelerisque maximus commodo. Interdum et malesuada fames ac ante
								ipsum primis in faucibus. Donec eleifend enim rutrum dignissim sollicitudin.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="x-section-t">
		<div class="uk-container uk-container-center">
			<div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
				<div class="uk-width-medium-1-2">
					<img draggable="false" class="x-img-cover uk-height-1-1 uk-width-1-1" src="<?php echo $root; ?>/assets/img/about/wed.jpg" />
				</div>
				<div class="uk-width-medium-1-2">
					<div class="x-pad-20">
						<h1 class="uk-margin">For Planners</h1>
						<p>
							We know you are busy and you want things to be easy.
							This platform is here for you as you plan that event.
							Be it a wedding party, an official corporate event, a birthday party, a funeral service or that house party, this is the place to be.
						</p>
						<p>
							Get personaliced services and peace of mind with cash back guarantee on selected services (T&Cs apply).
							Waste no time, create an account and start browsing our listings. Make use of our planning tools freely available to make your work even easier.
						</p>
						<div class="uk-text-left uk-contrast">
							<a href="./signup?client" class="uk-button uk-button-primary x-min-150">Get Started</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="x-section-tb">
		<div class="uk-container uk-container-center">
			<div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
				<div class="uk-width-medium-1-2 uk-push-1-2">
					<img draggable="false" class="x-img-cover uk-height-1-1 uk-width-1-1" src="<?php echo $root; ?>/assets/img/about/table.jpg" />
				</div>
				<div class="uk-width-medium-1-2 uk-pull-1-2">
					<div class="x-pad-20">
						<h1 class="uk-margin">For Vendors</h1>
						<p>
							Get your business recognized by connecting with our clients.
							We carefully select our vendors so we guarantee quality service delivery governed by our T&Cs.
							To have your services listed, create a vendor account and start advertising your business.
						</p>
						<p>
							Let clients come to you. If you provide quality services and you are consistent,
							you will have loyal clients who can bookmark your profile or even share with their friends as recommendations.
							Our planning tools also allow clients to narrow down to your specialization so you are guaranteed to close a sale.
						</p>
						<div class="uk-text-right uk-contrast">
							<a href="./signup?vendor" class="uk-button uk-button-primary x-min-150">Vendor Account</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=about"></script>
</body>
</html>
