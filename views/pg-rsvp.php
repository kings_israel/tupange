<?php
#include ns library
require_once __DIR__ . "/service/pg-rsvp.php";
$page_tags = "";
$page_og_tags = false;
$page_twitter_tags = false;
$page_article_tags = false;
$page_interop_tags = false;
$page_fb_app_id = "";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=rsvp">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<?php if (isset($error)){ ?>
			<div class="uk-text-center x-box x-error">
				<img draggable="false" src="<?php echo $root; ?>/assets/img/icons/close-red.png" />
				<p class="uk-text-danger"><?php echo $error; ?></p>
			</div>
			<?php } else { ?>
			<div class="x-event-summary">
				<div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
					<div class="uk-width-medium-1-2 uk-width-large-3-5">
						<div class="x-pad-20 x-box">
							<h1>RSVP Invitation</h1>
							<hr/>
							<h2><?php echo $event["name"]; ?></h2>
							<p>
								<?php echo $event["description"] != "" ? $event["description"] : "Event has no description"; ?>
								<br><small>Modified On: <?php echo $event["timestamp"]; ?></small>
							</p>
							<hr/>
							<ul>
								<li>
									<i class="uk-icon-calendar"></i>
									<span class="x-fw-400">Starts</span><span class="x-fw-400 uk-hidden-small">&nbsp;On</span>:
									<span class="right summary_starts"><?php echo $event["start"][3]; ?></span>
								</li>
								<li>
									<i class="uk-icon-calendar"></i>
									<span class="x-fw-400">Ends</span><span class="x-fw-400 uk-hidden-small">&nbsp;On</span>:
									<span class="right summary_ends"><?php echo $event["end"][3]; ?></span>
								</li>
								<?php if (is_array($event["location"])){ ?>
								<li>
									<i class="uk-icon-map-marker"></i>
									<span class="x-fw-400">Venue Location</span>:
									<span class="right summary_location"><?php echo $event["location"]["name"]; ?></span>
								</li>
								<?php } ?>
							</ul>
							<hr/>
							<h2><strong>To:</strong> <?php echo $guest['first_name'] . ' ' . $guest['last_name']; ?></h2>
							<?php if ($guest['inv_message']) echo '<p><strong>Message:</strong> ' . $guest['inv_message'] . '</p>'; ?>
							<?php if ($event['settings']['pass_question'] && $guest['inv_question']) echo '<p><strong>Test Quiz:</strong> ' . $guest['inv_question'] . '</p>'; ?>
							<?php if ($guest['table_no']) echo '<p><strong>Table No:</strong> ' . $guest['table_no'] . '</p>'; ?>
							<form action='javascript:' method='post' class='uk-form uk-form-stacked uk-margin-top'>
								<?php if ($event['settings']['diet']){ ?>
								<div class="uk-form-row <?php if (in_array((int) $guest['status'], [2,3,4]) && !$guest['diet']) echo 'x-hidden'; ?>">
                                    <label class="uk-form-label" for="diet">Diet Restrictions</label>
                                    <div class="uk-form-controls">
                                        <textarea class="uk-width-1-1" id="diet" placeholder="Enter your diet restrictions" <?php if (in_array((int) $guest['status'], [2,3,4])) echo 'disabled readonly'; ?>><?php echo $guest['diet']; ?></textarea>
                                    </div>
                                </div>
								<?php } ?>
								<div class="uk-form-row">
                                    <label class="uk-form-label" for="guests">No. Of Expected Guests</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-width-1-1" id="guests" placeholder="You + Your Company" type="number" step="1" <?php if (!$event['settings']['extend'] || in_array((int) $guest['status'], [2,3,4])) echo 'disabled readonly'; ?> value="<?php echo $guest['expected_guests']; ?>">
                                    </div>
                                </div>
								<div class="uk-margin-top">
									<?php if (!in_array((int) $guest['status'], [2,3,4])){ ?>
									<button type="button" id="confirm" class="uk-margin-small-bottom uk-button uk-button-large uk-button-success">Attending</button>
									<button type="button" id="maybe" class="uk-margin-small-bottom uk-button uk-button-large uk-button-primary">Maybe</button>
									<button type="button" id="reject" class="uk-margin-small-bottom uk-button uk-button-large uk-button-danger">Reject</button>
									<?php } else { ?>
									<button type="button" class="uk-margin-small-bottom uk-button uk-button-large uk-disabled">
										<?php if ($guest['status'] == 2) echo '<i class="uk-icon-check"></i> Confirmed'; ?>
										<?php if ($guest['status'] == 3) echo '<i class="uk-icon-check"></i> Maybe'; ?>
										<?php if ($guest['status'] == 4) echo '<i class="uk-icon-close"></i> Declined'; ?>
									</button>
									<?php } ?>
									<?php if ($event['settings']['reviews'] && in_array($guest['status'], [2,3])){ ?>
									<button type="button" id="review" class="uk-margin-small-bottom uk-button uk-button-large uk-button-primary"><i class="uk-icon-star-o"></i> My Reviews</button>
									<?php } ?>
								</div>
							</form>
						</div>
					</div>
					<div class="uk-width-medium-1-2 uk-width-large-2-5">
						<a href="javascript:" class="x-block x-event-poster x-watermarked" data-watermark="Event Poster" title="Event Poster">
							<?php if ($event["poster"] != ""){ ?>
							<img draggable="false" class="x-img-cover uk-height-large-1-1 x-watermarked-content-top" src="<?php echo $event["poster"]; ?>" alt="Event Poster" data-onerror="<?php echo $placeholder_image; ?>" onerror="onImageError(this);" />
							<?php } else { ?>
							<img draggable="false" class="default x-img-cover uk-height-large-1-1 x-watermarked-content-top" src="<?php echo $root; ?>/assets/img/dash/week.jpg" alt="Event Poster" />
							<?php } ?>
						</a>
						<?php if ($event["location_map_image"]){ ?>
						<img draggable="false" class="x-img-map uk-margin-small-top" src="<?php echo $event['location_map_image']; ?>" />
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=rsvp"></script>
</body>
</html>
