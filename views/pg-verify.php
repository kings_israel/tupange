<?php
$page_title = "Tupange | Email Verification";
$page_description = "Email Verification. Account management";

//pre-processor
include __DIR__ . "/service/pg-verify.php";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=forgot">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<div class="x-card x-auth-card uk-text-center">
				<?php if (isset($verify_error)){ ?>
				<a href="#"><img src="<?php echo $root; ?>/assets/img/icons/close-red.png" style="width:90px" /></a>
				<p><?php echo $verify_error; ?></p>
				<?php } if (isset($verify_success)){ ?>
				<a href="#"><img src="<?php echo $root; ?>/assets/img/icons/check-black.png" style="width:90px" /></a>
				<p><?php echo $verify_success; ?></p>
				<?php } ?>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p="></script>
</body>
</html>
