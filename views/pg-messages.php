<?php require_once __DIR__ . '/service/pg-messages.php'; ?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=messages">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<noscript>
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</noscript>
			<div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
				<div class="uk-width-medium-1-3 uk-width-large-1-4">
					<div class="x-box">
						<h2 class="x-box-title">Threads <a id="groups-toggle" href="javascript:" onclick="toggleContent()" class="uk-visible-small uk-navbar-toggle"></a></h2>
						<div id="toggle-content" class="uk-hidden-small">
							<div class="x-pad-20 x-border-bottom">
								<div class="uk-form-icon uk-width-1-1">
									<i class="uk-icon-search"></i>
									<input class="uk-width-1-1 x-threads-search" type="text" placeholder="Search Threads">
								</div>
							</div>
							<ul class="x-threads">
								<?php foreach ($threads as $i => $thread){ ?>
								<li data-user="<?php echo $thread['user']; ?>" class="<?php if ($i === 0) echo 'active'; ?>">
									<div tabindex="0" class="x-thread">
										<div class="x-thread-right">
											<div class="x-thread-time"><?php echo $thread['preview']['time']; ?></div>
											<div class="x-thread-unread <?php if (!$thread['unread']) echo 'x-display-none'; ?>"><?php echo $thread['unread']; ?></div>
											<div class="x-thread-options" data-uk-dropdown="{mode:'click'}">
												<a href="javascript:"><i class="uk-icon-angle-down"></i></a>
												<div class="uk-dropdown">
													<ul class="uk-nav uk-nav-navbar">
														<li><a class="uk-dropdown-close" href="javascript:" data-user="<?php echo $thread['user']; ?>" data-action="archive">Archive</a></li>
														<li><a class="uk-dropdown-close" href="javascript:" data-user="<?php echo $thread['user']; ?>" data-action="delete" class="uk-text-danger">Delete</a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="x-thread-user">
											<div class="x-avatar">
												<img src="<?php echo $thread['avatar']; ?>" data-onerror="<?php echo $default_avatar; ?>" onerror="onImageError(this);" />
											</div>
											<div class="x-info">
												<h3 class="x-thread-names x-ellipsis-text"><?php echo $thread['names']; ?></h3>
												<span class="x-thread-preview x-ellipsis-text">
													<i class="uk-icon-check <?php echo $thread['preview']['status'] !== 1 ? 'x-hidden' : ''; ?>"></i>
													<?php echo $thread['preview']['text']; ?>
												</span>
											</div>
										</div>
									</div>
								</li>
								<?php } ?>
							</ul>
							<div class="x-pad-20 x-threads-count">
								<p class="x-nomargin"><strong><?php echo ($count_threads = count($threads)); ?></strong> Conversation<?php echo $count_threads > 1 || $count_threads === 0 ? 's' : ''; ?></p>
							</div>
						</div>
					</div>
					<script type="text/javascript">
						function toggleContent(){
							let toggle_content = document.getElementById("toggle-content");
							if (toggle_content.className.indexOf("x-display-block") > -1) toggle_content.classList.remove("x-display-block");
							else toggle_content.classList.add("x-display-block");
						}
					</script>
					<div class="x-box uk-margin-top x-pad-20 uk-hidden-small">
						<h2>New Event?</h2>
						<div class="uk-contrast">
							<a class="uk-button uk-button-success uk-width-1-1 uk-margin-small-top" href="<?php echo $root; ?>/dashboard?edit-event">Create Event</a>
						</div>
					</div>
					<div class="x-box uk-margin-top x-pad-20 uk-hidden-small">
						<h2>Need Service?</h2>
						<div class="uk-contrast">
							<a class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-top" href="<?php echo $root; ?>/search?origin=request">Request Service</a>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-2-3 uk-width-large-3-4">
					<div class="x-box">
						<div class="x-no-messages x-display-none">
							<div class="x-info x-pad-20">
								<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/box.png" />
								<h2>No Messages!</h2>
								<p>To start a conversation, click the <strong>Message (Client/Vendor)</strong> option provided in <strong>Service Details</strong> or in <strong>Order Details</strong>.</p>
								<a class="uk-button uk-button-white uk-margin-top" href="<?php echo $root; ?>/search?origin=chat">Browse Services</a>
							</div>
						</div>
						<div class="x-messages-wrapper x-display-none" data-user="">
							<div class="x-messages-header">
								<div class="x-avatar">
									<img src="" data-onerror="<?php echo $default_avatar; ?>" onerror="onImageError(this);" />
								</div>
								<div class="x-info">
									<h2></h2>
									<div class="uk-form-icon uk-width-1-1">
										<i class="uk-icon-search"></i>
										<input class="uk-width-1-1 x-messages-search" type="text" placeholder="Search Messages">
									</div>
								</div>
							</div>
							<div class="x-messages x-border-bottom x-border-top">
								<div class="x-messages-start x-line-text uk-text-center x-display-none"><span>Start a new conversation</span></div>
								<div class="x-messages-chat"></div>
							</div>
							<div class="x-new-message x-pad-20">
							<form id="chat_new" action="" method="post" class="uk-form">
								<div class="uk-form-row">
									<div class="uk-form-controls">
										<textarea id="chat_message" name="chat_message" class="uk-width-1-1" onkeyup="this.value = sentenseCase(this.value)" placeholder="Type a message to send"></textarea>
									</div>
								</div>
								<div class="uk-margin-top x-iblock uk-width-1-1">
									<div class="x-file-attachments uk-align-left x-nomargin" uk-width-small="small" uk-width-xs="xsmall" uk-width-xxs="xxsmall" uk-width-medium="medium"></div>
									<div class="uk-align-right uk-text-right x-nomargin">
										<input id="attach_input" class="x-hidden" type="file" multiple>
										<?php if ($session_userlevel >= 2){ ?>
										<div class="x-attach-options x-iblock" data-uk-dropdown="{mode:'click'}">
											<button type="button" class="uk-button uk-button-white"><i class="uk-icon-paperclip"></i><span class="uk-hidden-small"> Attach</span></button>
											<div class="uk-dropdown">
												<ul class="uk-nav uk-nav-navbar">
													<li><a class="uk-dropdown-close x-attach-file" href="javascript:">Attach File(s)</a></li>
													<li><a class="uk-dropdown-close x-attach-order" href="javascript:">Attach Order</a></li>
													<li><a class="uk-dropdown-close x-attach-quote" href="javascript:">Attach Quotation</a></li>
												</ul>
											</div>
										</div>
										<?php } else { ?>
										<button type="button" class="uk-button uk-button-white x-attach-file"><i class="uk-icon-paperclip"></i><span class="uk-hidden-small"> Attach</span></button>
										<?php } ?>
										<button type="button" class="uk-button uk-button-primary x-message-send"><i class="uk-icon-send"></i><span class="uk-hidden-small"> Send</span></button>
									</div>
								</div>
							</form>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		let SELECTED_DATA = <?php echo json_encode(['thread' => $selected_thread, 'missing' => $selected_thread_missing]); ?>;
		let USER_DATA = <?php echo json_encode($user_data); ?>;
		let THREADS_DATA = <?php echo json_encode($threads); ?>;
	</script>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=messages"></script>
</body>
</html>
