<?php
$page_title = "Tupange | Help & Support";
$page_description = "Help & Support center. Contact us when you need assistance or for feedback";
$page_tags = "tupange,events,help,support,contacts,contact us,feedback,information,phone,numbers,email,address,physical,location,offices";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=support">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<div class="uk-text-center">
				<h1>Tupange Support Center</h1>
				<p>We are here to answer your needs.</p>
			</div>
		</div>
	</section>
	<section class="x-section-s">
		<div class="uk-container uk-container-center">
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-3">
					<div class="x-card x-ss" tabindex="0">
						<div class="x-pad-20 uk-text-center">
							<img draggable="false" src="<?php echo $root; ?>/assets/img/support/phones.png" />
							<h2>Guest Support</h2>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-3">
					<div class="x-card x-ss" tabindex="0">
						<div class="x-pad-20 uk-text-center">
							<img draggable="false" src="<?php echo $root; ?>/assets/img/support/hand.png" />
							<h2>Vendor Support</h2>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-3">
					<div class="x-card x-ss" tabindex="0">
						<div class="x-pad-20 uk-text-center">
							<img draggable="false" src="<?php echo $root; ?>/assets/img/support/info.png" />
							<h2>Help Topics</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="x-section-f">
		<div class="uk-container uk-container-center">
			<div class="uk-grid uk-grid-divider" data-uk-grid-margin>
				<div class="uk-width-medium-1-2">
					<h1>Frequest Questions</h1>
					<ul class="x-faqs">
						<li><a href="javascript:">Praesent placerat risus quis eros.</a></li>
						<li><a href="javascript:">Fusce pellentesque suscipit nibh.</a></li>
						<li><a href="javascript:">Integer vitae libero ac risus egestas placerat.</a></li>
						<li><a href="javascript:">Vestibulum commodo felis quis tortor.</a></li>
						<li><a href="javascript:">Ut aliquam sollicitudin leo.</a></li>
						<li><a href="javascript:">Cras iaculis ultricies nulla.</a></li>
						<li><a href="javascript:">Donec quis dui at dolor tempor interdum.</a></li>
					</ul>
				</div>
				<div class="uk-width-medium-1-2">
					<h1>Contact Us</h1>
					<p>We'd love to hear from you. You can use the form below to send us feedback or inquiries.</p>
					<form action="javascript:" method="post" class="uk-form-stacked">
						<div class="uk-form-row">
							<div class="uk-grid" data-uk-grid-margin>
								<div class="uk-width-medium-1-2">
									<label class="uk-form-label" for="f-names">Names</label>
									<div class="uk-form-controls">
										<input type="text" class="uk-width-1-1" id="f-names" name="names" placeholder="Full Names">
									</div>
								</div>
								<div class="uk-width-medium-1-2">
									<label class="uk-form-label" for="f-email">Email</label>
									<div class="uk-form-controls">
										<input type="text" class="uk-width-1-1" id="f-email" name="email" placeholder="Email Address">
									</div>
								</div>
							</div>
						</div>
						<div class="uk-form-row">
							<label class="uk-form-label" for="f-subject">Subject</label>
							<div class="uk-form-controls">
								<input type="text" class="uk-width-1-1" id="f-subject" name="subject" placeholder="Message Subject">
							</div>
						</div>
						<div class="uk-form-row">
							<label class="uk-form-label" for="f-message">Message</label>
							<div class="uk-form-controls">
								<textarea class="uk-width-1-1" id="f-message" name="message" placeholder="Type your message here"></textarea>
							</div>
						</div>
						<div class="uk-margin-top uk-text-right">
							<button type="reset" class="uk-button uk-button-default">Reset</button>
							<button type="submit" class="uk-button uk-button-success"><i class="uk-icon-send"></i> Send Message</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=support"></script>
</body>
</html>
