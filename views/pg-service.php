<?php
//service pre-processor
include __DIR__ . "/service/pg-service.php";
include __DIR__ . "/service/service-template.php";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=service">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre=service&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<noscript>
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</noscript>
			<div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
				<div class="uk-width-medium-1-3">
					<div class="x-service-image">
						<a href="<?php echo $service_image; ?>" data-uk-lightbox="{group:'gallery'}">
							<img draggable="false" src="<?php echo $service_image; ?>" data-onerror="<?php echo $placeholder_image; ?>" onerror="onImageError(this);" />
						</a>
					</div>
					<div class="uk-margin-top x-service-details x-box x-pad-20">
						<h2><strong><?php echo $service_title; ?></strong></h2>
						<p><?php echo $service_description; ?></p>
						<span class="x-service-location"><i class="uk-icon-map-marker"></i> <?php echo $service["location_name"]; ?></span>
						<div class="uk-margin-small-top">
							<span class="x-pill"><?php echo $service["category_name"]; ?></span>
						</div>
						<?php if ($edit_service) echo '<a href="' . $root . '/services?view=edit&s=' . $service["id"] . '" class="uk-margin-top uk-width-1-1 uk-button uk-button-primary">Edit Service</a>'; ?>
					</div>
					<div class="uk-margin-top x-service-map"></div>
					<div class="uk-margin-top x-box x-pad-20 x-service-vendor">
						<h2>Vendor</h2>
						<p>
							<strong>Company:</strong> <?php echo $vendor["company_name"]; ?>
							<br><?php echo strlen($vendor["company_description"]) ? $vendor["company_description"] : "Company description not available.";?>
						</p>
						<span class="x-service-vendor-location"><i class="uk-icon-map-marker"></i> <?php echo $vendor["location_name"]; ?></span>
						<div class="uk-margin-small-top">
							<?php foreach ($vendor_categories as $category){ ?>
							<span class="x-pill"><?php echo $category["name"]; ?></span>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-2-3">
					<div class="x-service-gallery x-box x-pad-20">
						<h2>Orders &amp; Pricing</h2>
						<ul class="x-pricing-items uk-grid uk-grid-small uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-3 <?php if (!count($service_pricing)) echo "x-hidden"; ?>" data-uk-grid-margin data-uk-grid-match>
							<?php foreach ($service_pricing as $item){ ?>
							<li>
								<div class="x-pricing-item uk-height-1-1">
									<h1><?php echo num_commas($item["price"]); ?></h1>
									<h2><?php echo $item["title"]; ?></h2>
									<p><?php echo $item["description"]; ?></p>

									<?php if (count($item["package"])){ ?>
									<h3>Package</h3>
									<div class="packages">
										<?php foreach ($item["package"] as $package) echo ' <div class="package-div"></div><div class="package">' . $package . '</div>'; ?>
									</div>
									<?php } ?>

									<!-- <button class="x-service-pricing-cart-add uk-button uk-button-small uk-button-success ' <?php echo ($service["in_cart"] ? 'x-hidden' : '') ?> '" data-service-cart="' <?php echo $service["id"] ?> '"><i class="uk-icon-shopping-cart"></i> Add</button> -->
									<?php if ($edit_service) echo '<a href="javascript:" class="x-pricing-item-delete" data-id="' . $item["id"] . '" data-title="' . $item["title"] . '"><i class="uk-icon-trash"></i></a>'; ?>
									
									<?php if ($edit_service) echo '<a href="javascript:" class="x-pricing-item-edit" data-id="' . $item['id'] . '" data-service="' . htmlspecialchars(json_encode($item)) . '"><i class="uk-icon-edit"></i></a>'; ?>
									
								</div>
							</li>
							<?php } ?>
							<?php if (count($service_pricing) < 3) for ($i = 0; $i < (3 - count($service_pricing)); $i ++) echo '<li class="uk-visible-large"><div class="x-pricing-item-placeholder">&nbsp;</div></li>'; ?>
						</ul>
						<div class="x-pricing-no-items x-pad-20 uk-text-center <?php if (count($service_pricing)) echo "x-hidden"; ?>">
							<h3 class="x-fw-400 x-color-b">NO PRICING</h3>
							<p class="x-color-8">Create an order and you will receive a quotation for it.</p>
						</div>
						<div class="uk-margin-top uk-text-center x-nowrap">
							<?php if ($edit_service) echo '<button title="Add Pricing Item" class="x-service-pricing-add uk-button uk-button-primary"><i class="uk-icon-plus"></i> Add Pricing</button>'; ?>
							<button title="Add To Cart" class="x-service-cart-add uk-button uk-button-success <?php if (strlen($service["in_cart"])) echo "x-hidden"; ?>" data-id="<?php echo $service["id"]; ?>"><i class="uk-icon-cart-plus"></i> Get Custom Quote</button>
							<button title="Remove From Cart" class="x-service-cart-remove uk-button uk-button-white <?php if (!strlen($service["in_cart"])) echo "x-hidden"; ?>" data-id="<?php echo $service["in_cart"]; ?>"><i class="uk-icon-check"></i> Added</button>
							<button title="Please wait" class="x-service-cart-loading uk-button uk-button-default uk-disabled x-hidden" disabled><i class="uk-icon-spinner uk-icon-spin"></i> Please wait</button>
							<a href="<?php echo $root; ?>/messages/<?php echo $vendor['uid']; ?>" title="Contact Vendor" class="x-service-message uk-button uk-button-white"><i class="uk-icon-envelope"></i></a>
							<a href="javascript:" title="<?php echo strlen($service["in_favorites"]) ? "Unlike" : "Like"; ?>" class="x-service-like <?php echo strlen($service["in_favorites"]) ? "favorite" : ""; ?>" data-service-favorite="<?php echo $service["id"]; ?>"><i class="uk-icon-<?php echo strlen($service["in_favorites"]) ? "heart" : "heart-o"; ?>"></i></a>
						</div>
					</div>
					<div class="uk-margin-top x-service-reviews x-box">
						<div class="x-pad-20">
							<h2 class="x-nomargin">Buyer Reviews</h2>
							<div class="uk-margin-small-top x-service-rating">
								<i class="x-rating-star uk-icon-star"></i> <strong><?php  echo number_format($service_rating["rating"], 1); ?></strong>
								<small>(<?php echo num_commas($service_rating["count"], 0); ?> Orders)</small>
							</div>
						</div>
						<div class="x-review-comments scrollbox <?php if (count($service_rating["reviews"]) > 2) echo 'limit'; ?>">
							<?php foreach ($service_rating["reviews"] as $review){ ?>
							<div class="x-review-comment x-border-top">
								<div class="x-comment-avatar">
									<div class="x-avatar"><img draggable="false" src="<?php echo $review["avatar"]; ?>" data-onerror="<?php echo $placeholder_image; ?>" onerror="onImageError(this);" /></div>
								</div>
								<div class="x-comment">
									<strong><?php echo $review["name"]; ?></strong>
									<div class="x-meta"><div class="uk-rating-stars" data-rate="<?php echo $review["rating"]; ?>"></div> <?php echo $review["timestamp"]; ?></div>
									<p>
										<?php echo $review["comment"]; ?>
										<?php if ($edit_service) echo '<br><small><strong>ORDER: <a href="' . $root . '/orders/?order=' . $review['order_id'] . '">' . $review['order_id'] . '</a></strong></small>'; ?>
									</p>
								</div>
							</div>
							<?php } ?>
						</div>
						<div class="uk-text-center x-pad-20 x-border-top <?php if (count($service_rating["reviews"]) <= 2) echo 'x-hidden'; ?>">
							<button class="x-review-comments-toggle uk-button uk-button-white x-min-150">Show All</button>
						</div>
					</div>
					<div class="uk-margin-top x-service-gallery x-box x-pad-20">
						<h2>Gallery</h2>
						<div class="x-gallery-empty <?php if (count($service_gallery)) echo 'x-hidden'; ?>">
							<img draggable="false" src="<?php echo $root; ?>/assets/img/icons/picture.png" />
							<p>No pictures in gallery for this service.</p>
						</div>
						<div class="x-gallery-items <?php if (!count($service_gallery)) echo 'x-hidden'; ?>">
							<ul class="uk-margin-top uk-grid uk-grid-small uk-grid-width-medium-1-2 uk-grid-width-large-1-3" data-uk-grid-margin data-uk-grid-match>
								<?php foreach ($service_gallery as $item){ ?>
								<li data-id="<?php echo $item["id"]; ?>">
									<figure class="uk-overlay x-gallery-item">
										<?php if ($edit_service) echo '<a href="javascript:" class="x-gallery-delete" data-id="' . $item["id"] . '"><i class="uk-icon-trash"></i></a>'; ?>
										<a draggable="false" href="<?php echo $item["image"]; ?>" data-uk-lightbox="{group:'gallery'}" data-lightbox-type="image" title="<?php echo $item["caption"]; ?>">
                                        	<img draggable="false" src="<?php echo $item["image"]; ?>" alt="<?php echo $item["caption"]; ?>" data-onerror="<?php echo $placeholder_image; ?>" onerror="onImageError(this);" />
										</a>
										<?php if (strlen($item["caption"])){ ?>
										<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                            <?php echo $item["caption"]; ?>
                                        </figcaption>
										<?php } ?>
                                    </figure>
								</li>
								<?php } ?>
                            </ul>
						</div>
						<?php if ($edit_service){ ?>
						<div class="uk-margin-top uk-text-center">
							<button class="x-gallery-upload uk-button uk-button-primary">Upload Image</button>
						</div>
						<?php } ?>
					</div>
					<?php if (count($other_services)){ ?>
					<div class="uk-margin-top x-box x-pad-20 x-service-related">
						<h2>More By <strong><?php echo $vendor["company_name"]; ?></strong> &amp; Related Services</h2>
						<ul class="uk-margin-top uk-grid uk-grid-small uk-grid-width-medium-1-2 uk-grid-width-large-1-3" data-uk-grid-margin data-uk-grid-match>
							<?php foreach ($other_services as $service){ ?>
							<li><?php echo service_template($service); ?></li>
							<?php } ?>
						</ul>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script type="text/javascript">
	window["SERVICE_DATA"] = <?php echo json_encode($service); ?>;
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCisnVFSnc5QVfU2Jm2W3oRLqMDrKwOEoM&libraries=places"></script>
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=service"></script>
	<?php if ($edit_service){ ?><script src="<?php echo $root; ?>/assets/js/pages/service-edit.js"></script><?php } ?>
</body>
</html>
