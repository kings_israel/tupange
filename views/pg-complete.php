<?php
//include session
include_once __DIR__ . "/service/session.php";

//allow admins or incomplete vendor profiles only
if (!($session_userlevel == 2 && $session_status == 0 || $session_userlevel > 2)){
	header("location: $root/dashboard");
	exit();
}

//pre processor
include __DIR__ . "/service/pg-complete.php";

//page variables
$page_title = "Tupange | Complete Profile";
$page_description = "Vendor account complete profile. Just one more step";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=complete">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=services-e">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre=complete&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<div class="x-forms-banner"></div>
			<div class="x-card x-forms">
				<noscript>
					<div class="uk-alert uk-alert-danger" data-uk-alert>
						<h3>You must have JavaScript enabled in order to use this form. Please enable JavaScript and then reload this page in order to continue.</h3>
					</div>
				</noscript>
				<form class="onload-show x-display-none" action="" method="post" enctype="multipart/form-data">
					<h1>Just one more step</h1>
					<p>Kindly fill out this form to complete your vendor profile</p>
					<?php if (isset($complete_error)){ ?>
					<div class="uk-alert uk-alert-danger" data-uk-alert>
						<a href="" class="uk-alert-close uk-close"></a>
						<p><?php echo $complete_error; ?></p>
					</div>
					<?php } ?>
					<div class="uk-grid" data-uk-grid-margin>
						<div class="uk-width-large-2-3">
						<div class="uk-form-row">
							<label class="uk-form-label" for="company_name">Company Name <span class="uk-text-danger">*</span></label>
							<div class="uk-form-controls">
								<input class="uk-width-1-1" type="text" id="company_name" name="company_name" value="<?php if (isset($company_name)) echo $company_name; ?>">
							</div>
						</div>
						<div class="uk-form-row">
							<label class="uk-form-label" for="company_description">Company Description</label>
							<div class="uk-form-controls">
								<textarea class="uk-width-1-1" type="text" rows="5" id="company_description" name="company_description"><?php if (isset($company_description)) echo $company_description; ?></textarea>
							</div>
						</div>
						</div>
						<div class="uk-width-large-1-3">
							<h4>Company Logo</h4>
							<div class="x-display-img">
								<a href="javascript:" id="display-img-link">
									<img id="display-img" class="uk-width-1-1" src="<?php echo isset($company_logo) ? $company_logo : $placeholder_image; ?>" alt="Display Image" />
								</a>
								<small class="uk-hidden-large">Image may be trimmed to fit</small>
								<div class="uk-margin-top">
									<div class="uk-form-file uk-width-1-1">
										<button id="display-img-btn" class="uk-button uk-button-white uk-width-1-1">Select Image</button>
										<input type="file" accept=".jpg,.jpeg,.png,.gif" name="company_logo" id="display-image">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="uk-form-row">
						<label class="uk-form-label" for="company_email">Company Email <span class="uk-text-danger">*</span></label>
						<div class="uk-form-controls">
							<input class="uk-width-1-1" type="text" id="company_email" name="company_email" value="<?php if (isset($company_email)) echo $company_email; ?>">
						</div>
					</div>
					<div class="uk-form-row">
						<label class="uk-form-label" for="company_phone_number">Company Phone Number <span class="uk-text-danger">*</span></label>
						<div class="uk-form-controls">
							<input class="uk-width-1-1" type="phone_number" id="company_phone_number" name="company_phone_number" value="<?php if (isset($company_phone_number)) echo $company_phone_number; ?>">
						</div>
					</div>
					
					<div class="uk-form-row">
						<label class="uk-form-label" for="category_services_input">Services Offered <span class="uk-text-danger">*</span></label>
						<div class="uk-form-controls x-drop-wrapper">
							<uk-tags class="uk-width-1-1" name="company_services" fetch="<?php echo $root; ?>/autocomplete.php?category={{query}}" item-template="<i class='uk-icon-{{icon}}'></i> {{name}}" template="{{name}}" value="<?php if (isset($company_services_value)) echo $company_services_value; ?>" placeholder="Select"></uk-tags>
						</div>
					</div>
					<div class="uk-form-row">
						<label class="uk-form-label" for="company_location">Location <span class="uk-text-danger">*</span></label>
						<div class="uk-form-controls x-drop-wrapper">
							<uk-locations class="uk-width-1-1" id="location" name="location" fetch="<?php echo $root; ?>/autocomplete.php?location={{query}}" item-template="<i class='uk-icon-map-marker'></i> {{name}}" template="{{name}}" value="<?php if (isset($location_value)) echo $location_value; ?>" placeholder="Search City/Area" icon="map-marker"></uk-locations>
						</div>
					</div>
					<div class="uk-form-row">
						<label class="uk-form-label" for="search_map">Location On Map</label>
						<div class="uk-form-controls">
							<map-input class="uk-width-1-1" id="location_map" name="location_map" value="<?php if (isset($location_map_value)) echo $location_map_value; ?>" placeholder="Search Places" icon="map-marker"></map-input>
							<p class="x-location-map-desc">Search/Drag pin. So clients can find you on their map.</p>
						</div>
					</div>
					<div class="uk-form-row uk-contrast uk-text-left uk-margin-top">
						<button type="submit" class="uk-button uk-button-success x-min-200">Complete Profile</button>
					</div>
				</form>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCN73V5VTrs-MzakmhHJ2Pe47lxOc7B6PE&libraries=places"></script>
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=complete"></script>
</body>
</html>
