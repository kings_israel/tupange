<?php
//service pre-processor
include __DIR__ . "/service/pg-profile.php";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=profile">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre=profile&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<noscript>
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</noscript>
			<div>
                <form class="uk-form onload-show x-display-none x-box x-pad-20" action="" method="post" enctype="multipart/form-data">
                    <?php if (isset($edit_profile_error)){ ?>
                    <div class="uk-alert uk-alert-danger" data-uk-alert>
                        <a href="" class="uk-alert-close uk-close"></a>
                        <p><?php echo $edit_profile_error; ?></p>
                    </div>
                    <?php } ?>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-large-1-3">
                            <h2>Profile Photo</h2>
                            <div class="x-display-img">
                                <a href="javascript:" id="display-img-link">
                                    <img id="display-img" class="uk-width-1-1" src="<?php echo isset($photo_url) ? '../uploads/'.$photo_url : $placeholder_image; ?>" alt="Profile Photo" />
                                </a>
                                <small class="uk-hidden-large">Image may be trimmed to fit</small>
                                <div class="uk-margin-top">
                                    <div class="uk-form-file uk-width-1-1">
                                        <button id="display-img-btn" class="uk-button uk-button-white uk-width-1-1">Select Image</button>
                                        <input type="file" accept=".jpg,.jpeg,.png,.gif" name="profile_photo" id="display-image">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-large-2-3">
                            <h2>User Details</h2>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="company_name">Display Name <span class="uk-text-danger">*</span></label>
                                <div class="uk-form-controls">
                                    <input class="uk-width-1-1" type="text" id="display_name" onkeyup="this.value = sentenseCase(this.value)" name="display_name" placeholder="Display Title" value="<?php if (isset($display_name)) echo $display_name; ?>">
                                </div>
                                <small id="display_name_error" class="uk-text-danger"></small>
                            </div>
                            <div class="uk-grid uk-form-row">
                                <div class="uk-width-large-1-2">
                                    <label class="uk-form-label" for="company_name">First Name <span class="uk-text-danger"></span></label>
                                    <div class="uk-form-controls">
                                        <input class="uk-width-1-1" type="text" id="first_name" onkeyup="this.value = sentenseCase(this.value)" name="first_name" placeholder="First Name" value="<?php if (isset($first_name)) echo $first_name; ?>">
                                    </div>
                                    <small id="first_name_error" class="uk-text-danger"></small>
                                </div>
                                <div class="uk-width-large-1-2">
                                    <label class="uk-form-label" for="company_name">Last Name <span class="uk-text-danger"></span></label>
                                    <div class="uk-form-controls">
                                        <input class="uk-width-1-1" type="text" id="last_name" onkeyup="this.value = sentenseCase(this.value)" name="last_name" placeholder="Last Name" value="<?php if (isset($last_name)) echo $last_name; ?>">
                                    </div>
                                    <small id="last_name_error" class="uk-text-danger"></small>
                                </div>
                            </div>
                            <div class="uk-grid uk-form-row">
                                <div class="uk-width-large-1-2">
                                    <label class="uk-form-label" for="company_name">Email <span class="uk-text-danger">*</span></label>
                                    <div class="uk-form-controls">
                                        <input class="uk-width-1-1" type="email" id="email"  name="email" placeholder="Email" value="<?php if (isset($email)) echo $email; ?>">
                                    </div>
                                    <small id="email_error" class="uk-text-danger"></small>
                                </div>
                                <div class="uk-width-large-1-2">
                                    <label class="uk-form-label" for="company_name">Phone Number <span class="uk-text-danger"></span></label>
                                    <div class="uk-form-controls">
                                        <input class="uk-width-1-1" type="phone number" id="phone_number" onkeyup="this.value = sentenseCase(this.value)" name="phone_number" placeholder="Phone Number" value="<?php if (isset($phone_number)) echo $phone_number; ?>">
                                    </div>
                                    <small id="phone_number_error" class="uk-text-danger"></small>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label" for="company_email">Description <span class="uk-text-danger"></span></label>
                                <div class="uk-form-controls">
                                    <textarea class="uk-width-1-1" id="description" onkeyup="this.value = sentenseCase(this.value)" name="description" placeholder="Service Description"><?php if (isset($description)) echo $description; ?></textarea>
                                </div>
                                <small id="description_error" class="uk-text-danger"></small>
                            </div>
                            <div class="uk-grid uk-form-row">
                                <div class="uk-width-large-1-2">
                                    <label class="uk-form-label" for="company_name">Gender <span class="uk-text-danger"></span></label>
                                    <div class="uk-form-controls">
                                        <select class="uk-width-1-1" id="gender"  name="gender" placeholder="gender">
                                            <option value="">Select Gender</option>
                                            <option value="Male" <?php if (isset($gender) && $gender == "Male") echo 'selected'; ?>>Male</option>
                                            <option value="Male" <?php if (isset($gender) && $gender == "Female") echo 'selected'; ?>>Female</option>
                                        </select>
                                    </div>
                                    <small id="gender_error" class="uk-text-danger"></small>
                                </div>
                                <div class="uk-width-large-1-2">
                                    <label class="uk-form-label" for="company_name">Age <span class="uk-text-danger"></span></label>
                                    <div class="uk-form-controls">
                                        <input class="uk-width-1-1" type="number" id="age" name="age" placeholder="Enter Age" value="<?php if (isset($age)) echo $age; ?>">
                                    </div>
                                    <small id="age_error" class="uk-text-danger"></small>
                                </div>
                            </div>
                            
                            <div class="uk-margin-top uk-text-right">
                                <input draggable="false" type="submit" name="action" class="uk-button uk-button-primary" value="Update Details"></input>
                            </div>
                        </div>
                    </div>
                </form>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=profile"></script>
	<?php if ($edit_profile){ ?><script src="<?php echo $root; ?>/assets/js/pages/profile-edit.js"></script><?php } ?>
</body>
</html>
