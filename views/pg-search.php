<?php
//service pre-processor
include __DIR__ . "/service/pg-search.php";
include __DIR__ . "/service/service-template.php";
$results_grid_class = "uk-grid uk-grid-small uk-grid-width-medium-1-2 uk-grid-width-large-1-3";

//categories
$data_categories = NS_SITE_DIR . "/data/categories.json";
$category_items = json_decode(@file_get_contents($data_categories), true);
if (!is_array($category_items)) $category_items = [];
$category_name = array_column($category_items, 'name');
array_multisort($category_name, SORT_ASC, $category_items);
unset($category_name);

$qs = $_SERVER['QUERY_STRING'];

//if (isset($categories_value)){
//	header("content-type: text/plain"); print_r($categories_value); exit();
//}

?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=search">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre=search&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<noscript>
				<div class="uk-alert uk-alert-danger" data-uk-alert>
					<h3><strong>JAVASCRIPT IS DISABLED</strong><br>Please enable JavaScript and then reload this page to fully enable blocked actions or to show hidden content.</h3>
				</div>
			</noscript>
			<div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match>
				<div class="uk-width-medium-1-3 uk-width-large-1-4">
					<div class="x-search-filters x-box">
						<h2 class="x-box-title">Categories <a href="javascript:" onclick="toggleFilters()" class="uk-visible-small uk-navbar-toggle"></a></h2>
						<form id="filter_form" class="uk-form x-pad-20 uk-hidden-small x-display-none" action="" method="get">
							<input id="filter_query" type="hidden" name="query" value="<?php if (isset($search_query)) echo $search_query; ?>">
							<input type="hidden" name="rating" value="<?php if (isset($search_rating)) echo $search_rating; ?>">
							<input type="hidden" name="category" value="<?php if (isset($search_category)) echo $search_category; ?>">
							<div class="uk-form-row">
								<label class="uk-form-label x-col-dark-green">Near Location</label>
								<div class="uk-form-controls x-drop-wrapper">
									<uk-locations class="uk-width-1-1" name="location" fetch="<?php echo $root; ?>/autocomplete.php?location={{query}}" item-template="<i class='uk-icon-map-marker'></i> {{name}}" template="{{name}}" placeholder="Search City/Area" icon="map-marker" value="<?php if (isset($location_value)) echo $location_value; ?>"></uk-locations>
								</div>
							</div>
							<div class="uk-form-row">
								<label class="uk-form-label x-col-dark-green">Availability On</label>
								<div class="uk-form-controls">
									<div class="uk-form-icon uk-form-icon-flip uk-width-1-1">
										<i class="uk-icon-calendar"></i>
										<input name="date" type="text" class="uk-width-1-1" placeholder="DD/MM/YYYY" data-uk-datepicker="{format:'DD/MM/YYYY', pos:'bottom'}" value="<?php if (isset($search_date)) echo $search_date; ?>">
									</div>
								</div>
							</div>
							<div class="uk-margin-top uk-text-center">
								<button id="filter_apply" type="submit" class="uk-button uk-button-primary x-min-100 uk-width-1-1">Apply Filters</button>
							</div>
						</form>
						<ul class="xx-categories">
							<?php foreach ($category_items as $item) { if (!((int) $item['value'])) continue; $item_url = ($qs ? '?' . $qs . '&' : '?') . 'category=' . $item['value']; ?>
							<?php $item_url = '?category=' . $item['value']; ?>
							<li class="<?php if (isset($search_category) && (int) $search_category == (int) $item['value']) echo 'current'; ?>">
								<a data-value="<?php echo $item['value']; ?>" href="<?php echo $item_url; ?>"><!-- <i class="uk-icon-<?php echo $item['icon']; ?>"></i> --><?php echo $item['name']; ?></a>
							</li>
							<?php } ?>
						</ul>
						<script type="text/javascript">
						function toggleFilters(){
							let filter_form = document.getElementById("filter_form");
							if (filter_form.className.indexOf("x-display-block") > -1) filter_form.classList.remove("x-display-block");
							else filter_form.classList.add("x-display-block");
						}
						</script>
					</div>
					<!--
					<div class="x-box uk-margin-top x-pad-20 uk-hidden-small">
						<h2>New Event?</h2>
						<div class="uk-contrast">
							<a class="uk-button uk-button-success uk-width-1-1" href="<?php echo $root; ?>/dashboard?edit-event">Create Event</a>
							<a class="uk-button uk-button-white uk-width-1-1 uk-margin-top" href="<?php echo $root; ?>/dashboard?events">My Events</a>
						</div>
					</div>
					-->
				</div>
				<div class="uk-width-medium-2-3 uk-width-large-3-4">
					<div class="x-box x-pad-20 x-search-wrapper">
						<form id="search_query_form" class="uk-form x-search-bar" action="javascript:" method="post">
							<div class="uk-grid uk-grid-small">
								<div class="uk-width-4-5 uk-width-large-5-6">
									<div class="uk-width-1-1 uk-form-icon">
										<i class="uk-icon-search"></i>
										<input id="search_query" class="uk-width-1-1" type="text" placeholder="Search Services" value="<?php if (isset($search_query)) echo $search_query; ?>">
									</div>
								</div>
								<div class="uk-width-1-5 uk-width-large-1-6 uk-contrast">
									<button type="submit" class="uk-button uk-button-large uk-button-primary uk-width-1-1">
										<i class="uk-icon-search uk-hidden-large"></i>
										<span class="uk-hidden-small uk-visible-large">Search</span>
									</button>
								</div>
							</div>
							<div class="uk-grid uk-grid-small uk-form">
								<div class="uk-width-medium-1-3">
									<div class="uk-form-row">
										<label class="uk-form-label x-col-dark-green">Near Location</label>
										<div class="uk-form-controls x-drop-wrapper">
											<uk-locations class="uk-width-1-1" id="xx_location" fetch="<?php echo $root; ?>/autocomplete.php?location={{query}}" item-template="<i class='uk-icon-map-marker'></i> {{name}}" template="{{name}}" placeholder="Search City/Area" icon="map-marker" value="<?php if (isset($location_value)) echo $location_value; ?>"></uk-locations>
										</div>
									</div>
								</div>
								<div class="uk-width-medium-1-3">
									<div class="uk-form-row">
										<label class="uk-form-label x-col-dark-green">Availability On</label>
										<div class="uk-form-controls">
											<div class="uk-form-icon uk-form-icon-flip uk-width-1-1 xx_date">
												<i class="uk-icon-calendar"></i>
												<input id="xx_date" type="text" class="uk-width-1-1" placeholder="DD/MM/YYYY" data-uk-datepicker="{format:'DD/MM/YYYY', pos:'bottom'}" value="<?php if (isset($search_date)) echo $search_date; ?>">
											</div>
										</div>
									</div>
								</div>
								<div class="uk-width-medium-1-3">
									<div class="uk-form-row">
										<label class="uk-form-label x-col-dark-green">Rating</label>
										<div class="uk-form-controls">
											<select id="xx_rating" class="uk-width-1-1">
												<option <?php if (!(isset($search_rating) && $search_rating)) echo 'selected'; ?> value="">Any</option>
												<option <?php if (isset($search_rating) && $search_rating == 4.5) echo 'selected'; ?> value="4.5">Outstanding: 4.5+</option>
												<option <?php if (isset($search_rating) && $search_rating == 4) echo 'selected'; ?> value="4">Very Good: 4+</option>
												<option <?php if (isset($search_rating) && $search_rating == 3.5) echo 'selected'; ?> value="3.5">Good: 3.5+</option>
												<option <?php if (isset($search_rating) && $search_rating == 3) echo 'selected'; ?> value="3">Decent: 3+</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div class="x-search-content">
							<?php if (!count($search_results) && !count($featured_services) && !count($recent_services)){ ?>
								<div class="x-search-no-results uk-margin-top">
									<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
									<h3>No Services Available</h3>
									<p>Kindly check again later.</p>
									<a href="?reset" class="reset-search uk-button uk-button-white uk-margin-top x-min-100">Refresh</a>
								</div>
							<?php } if ($is_search){ ?>
								<?php if (empty($search_results)){ ?>
									<div class="x-search-no-results uk-margin-top">
										<img draggable="false" src="<?php echo $root; ?>/assets/img/dash/search-no.png" />
										<h3>No Results Found</h3>
										<p>Refine your search/filters to get different results.</p>
										<!--
										<a href="?reset" class="reset-search uk-button uk-button-white uk-margin-top x-min-100">Reset</a>
										-->
									</div>
								<?php } else { ?>
									<h2 class="x-search-title uk-margin-top">
										Search Results <small>(<?php echo count($search_results); ?>)</small>
										<!--
										<a href="?reset" class="reset-search uk-align-right uk-button uk-button-white uk-button-small">
											<span class="uk-visible-small">Reset</span>
											<span class="uk-hidden-small">Reset Search</span>
										</a>
										-->
									</h2>
									<ul class="<?php echo $results_grid_class; ?>" data-uk-grid-margin data-uk-grid-match>
										<?php foreach ($search_results as $service){ ?>
											<li><?php echo service_template($service); ?></li>
										<?php } ?>
									</ul>
								<?php }} if (count($featured_services)){ ?>
									<h2 class="x-search-title uk-margin-top">Featured Services <small>(<?php echo count($featured_services); ?>)</small></h2>
									<ul class="<?php echo $results_grid_class; ?>" data-uk-grid-margin data-uk-grid-match>
										<?php foreach ($featured_services as $service){ ?>
											<li><?php echo service_template($service); ?></li>
										<?php } ?>
									</ul>
								<?php } if (count($recent_services)){ ?>
									<h2 class="x-search-title uk-margin-top">Recommended Services <small>(<?php echo count($recent_services); ?>)</small></h2>
									<ul class="<?php echo $results_grid_class; ?>" data-uk-grid-margin data-uk-grid-match>
										<?php foreach ($recent_services as $service){ ?>
											<li><?php echo service_template($service); ?></li>
										<?php } ?>
									</ul>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- terms -->
		<?php include __DIR__ . "/parts/part-terms.php"; ?>
		<!-- footer -->
		<?php include __DIR__ . "/parts/part-footer.php"; ?>
		<!-- libraries -->
		<script type="text/javascript">
		window["ALL_SERVICES"] = <?php echo json_encode($all_services); ?>;
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCN73V5VTrs-MzakmhHJ2Pe47lxOc7B6PE&libraries=places"></script>
		<script src="<?php echo $root; ?>/assets/js/scripts.php?p=search"></script>
	</body>
	</html>
