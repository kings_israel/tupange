<?php
$page_title = "Tupange | Login";
$page_description = "Login";

//pre-processor
include __DIR__ . "/service/pg-login.php";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=login">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<div class="x-card x-auth-card">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-medium-1-2">
						<div class="x-auth-pad">
							<h1>Login</h1>
							<p>Quickly login using one of the methods provided. New users will automatically get a client account.</p>
							<div id="x-social-error"></div>
							<div class="x-social-auth uk-contrast">
								<a href="javascript:" class="x-btn-fb x-btn uk-button uk-button-default uk-width-1-1"><i class="uk-icon-facebook"></i> Login Using Facebook</a>
								<a href="javascript:" class="x-btn-tw x-btn uk-button uk-button-default uk-width-1-1 uk-margin-top"><i class="uk-icon-twitter"></i> Login Using Twitter</a>
								<a href="javascript:" class="x-btn-go x-btn uk-button uk-button-default uk-width-1-1 uk-margin-top"><i class="uk-icon-google"></i> Login Using Google</a>
							</div>
							<div class="uk-margin-top">
								<p class="x-agree"><strong>By logging in, you agree to our <a href="#modal-terms" data-uk-modal>terms of use</a> &amp; <a href="#modal-privacy" data-uk-modal>privacy policy</a>.</strong></p>
							</div>
						</div>
					</div>
					<div class="uk-width-medium-1-2 x-auth-card-right">
						<div class="x-auth-pad">
							<h2 class="uk-margin-bottom">Or Use Email/Username</h2>
							<form action="" method="post" class="uk-form uk-form-stacked">
								<?php if (isset($login_error)){ ?>
								<div class="uk-alert uk-alert-danger" data-uk-alert>
									<a href="" class="uk-alert-close uk-close"></a>
									<p><?php echo $login_error; ?></p>
								</div>
								<?php } ?>
								<div class="uk-form-row">
									<label class="uk-form-label" for="login_username">Email Address/Username</label>
									<div class="uk-form-controls">
										<input class="uk-width-1-1" type="text" id="login_username" name="login_username" value="<?php if (isset($login_username)) echo $login_username; ?>" autocomplete="username">
									</div>
								</div>
								<div class="uk-form-row">
									<label class="uk-form-label" for="login_password">Password</label>
									<div class="uk-form-controls">
										<input class="uk-width-1-1" type="password" id="login_password" name="login_password" value="<?php if (isset($login_password)) echo $login_password; ?>" autocomplete="password">
									</div>
								</div>
								<div class="uk-form-row">
									<div class="uk-grid uk-grid-small">
										<div class="uk-width-1-2">
											<label><input type="checkbox" name="remember_me" <?php if (isset($remember_me) && $remember_me) echo "checked=\"checked\";" ?>> Remember me</label>
										</div>
										<div class="uk-width-1-2 uk-text-right">
											<button type="submit" class="uk-button uk-button-success x-min-100">Login</button>
										</div>
									</div>
								</div>
								<div class="uk-form-row uk-text-center">
									<p><a href="<?php echo $root; ?>/forgot">Forgot password?</a></p>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<?php if (isset($_GET["redirect"])){ ?>
	<script type="text/javascript">
		window["redirect"] = encodeURIComponent('<?php echo str_replace("'", "%27", $_GET["redirect"]); ?>');
	</script>
	<?php } ?>
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=login"></script>
</body>
</html>
