<?php
$page_title = "Tupange | Forgot Password";
$page_description = "Forgot Password. Password recovery.";

//pre-processor
include __DIR__ . "/service/pg-forgot.php";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=forgot">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<div class="x-card x-auth-card">
				<h1>Forgot Password?</h1>
				<p>No worries, we can send you password recovery instructions. Kindly provide your account's email address.</p>
				<form action="" method="post" class="uk-form uk-form-stacked">
					<?php if (isset($forgot_error)){ ?>
					<div class="uk-alert uk-alert-danger" data-uk-alert>
						<a href="" class="uk-alert-close uk-close"></a>
						<p><?php echo $forgot_error; ?></p>
					</div>
					<?php } if (isset($forgot_success)){ ?>
					<div class="uk-alert uk-alert-success" data-uk-alert>
						<a href="" class="uk-alert-close uk-close"></a>
						<p><?php echo $forgot_success; ?></p>
					</div>
					<?php } ?>
					<div class="uk-form-row">
						<label class="uk-form-label" for="forgot_email">Email Address</label>
						<div class="uk-form-controls">
							<input class="uk-width-1-1" type="text" id="forgot_email" name="forgot_email" value="<?php if (isset($forgot_email)) echo $forgot_email; ?>" autocomplete="email">
						</div>
					</div>
					<div class="uk-form-row uk-text-right uk-contrast">
						<button type="submit" class="uk-button uk-button-success uk-width-1-1">Recover Password</button>
					</div>
					<div class="uk-form-row uk-text-center">
						<p><a href="<?php echo $root; ?>/login">Login Here</a></p>
					</div>
				</form>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p="></script>
</body>
</html>
