<?php
#include ns library
require_once __DIR__ . "/../includes.php";
use Naicode\Server\Funcs as fn1;

$route_title = fn1::strtocamel(str_replace("-", " ", $route_page));
$page_title = "Tupange | " . $route_title;
$page_description = "Page content is under construction.";
$page_tags = "";
$page_og_tags = false;
$page_twitter_tags = false;
$page_article_tags = false;
$page_interop_tags = false;
$page_fb_app_id = "";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=page">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<h1 class="uk-margin"><?php echo $route_title; ?></h1>
			<p>Page content is under construction. Check again soon.</p>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=page"></script>
</body>
</html>
