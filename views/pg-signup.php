<?php
$signup_account = 1;
if (isset($_GET["client"])) $signup_account = 1;
if (isset($_GET["vendor"])) $signup_account = 2;
$page_title = "Tupange | Get Started";
$page_description = "Create an account.";
if ($signup_account == 1){
	$page_title = "Tupange | Client Sign Up";
	$page_description = "Client sign up";
}
else if ($signup_account == 2){
	$page_title = "Tupange | Vendor Sign Up";
	$page_description = "Vendor sign up";
}

//pre-processor
include __DIR__ . "/service/pg-signup.php";

$redirect_url = isset($_GET["redirect"]) ? trim($_GET["redirect"]) : "";
$signup_url = $root . "/signup" . (strlen($redirect_url) ? "?redirect=" . urlencode($redirect_url) : "");
$client_url = $root . "/signup?client" . (strlen($redirect_url) ? "&redirect=" . urlencode($redirect_url) : "");
$vendor_url = $root . "/signup?vendor" . (strlen($redirect_url) ? "&redirect=" . urlencode($redirect_url) : "");
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=signup">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<?php if ($signup_account == null){ ?>
			<div class="x-card x-margin-auto x-signup-types">
				<div class="uk-grid uk-grid-collapse" data-uk-grid-margin data-uk-grid-match>
					<div class="uk-width-medium-1-2 x-type-client">
						<h1>Client</h1>
						<p>If you just want to create an account and start planning your events and services with access to all our planning tools</p>
						<div class="uk-text-right uk-contrast">
							<a href="<?php echo $client_url; ?>" class="uk-button uk-button-default">Create Account</a>
						</div>
					</div>
					<div class="uk-width-medium-1-2 x-type-vendor">
						<h1>Business</h1>
						<p>Are you a business looking to offer your services to our clients? Then this option is for you, click the button below to continue.</p>
						<div class="uk-text-right uk-contrast">
							<a href="<?php echo $vendor_url; ?>" class="uk-button uk-button-success">Vendor Account</a>
						</div>
					</div>
				</div>
			</div>
			<?php } else { ?>
			<div class="x-card x-auth-card">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-medium-1-2">
						<div class="x-auth-pad">
							<a class="x-auth-back" href="<?php echo $signup_url; ?>">
								<img src="<?php echo $root; ?>/assets/img/icons/back.png" />
							</a>
							<?php if ($signup_account == 1){ ?>
							<h1>Client Account</h1>
							<p>Thank you for joining us. We have a lot more waiting for you. Just choose an sign up method and continue.</p>
							<?php } else { ?>
							<h1>Business Account</h1>
							<p>Welcome to <strong>Tupange For Vendors</strong>. Choose an sign up method and start selling your services.</p>
							<?php } ?>
							<div id="x-social-error"></div>
							<div class="x-social-auth uk-contrast">
								<a href="javascript:" class="x-btn-fb x-btn uk-button uk-button-default uk-width-1-1"><i class="uk-icon-facebook"></i> Sign Up With Facebook</a>
								<a href="javascript:" class="x-btn-tw x-btn uk-button uk-button-default uk-width-1-1 uk-margin-top"><i class="uk-icon-twitter"></i> Sign Up With Twitter</a>
								<a href="javascript:" class="x-btn-go x-btn uk-button uk-button-default uk-width-1-1 uk-margin-top"><i class="uk-icon-google"></i> Sign Up With Google</a>
							</div>
							<div class="uk-margin-top">
								<p class="x-agree"><strong>By signing up, you agree to our <a href="#modal-terms" data-uk-modal>terms of use</a> &amp; <a href="#modal-privacy" data-uk-modal>privacy policy</a>.</strong></p>
							</div>
						</div>
					</div>
					<div class="uk-width-medium-1-2 x-auth-card-right">
						<div class="x-auth-pad">
							<h2 class="uk-margin-bottom">Or Register Email</h2>
							<?php if ($signup_account == 1){ ?>
							<form action="" method="post" class="uk-form uk-form-stacked">
								<input type="hidden" id="register_account" name="register_account" value="<?php echo $signup_account; ?>">
								<?php if (isset($register_error)){ ?>
								<div class="uk-alert uk-alert-danger" data-uk-alert>
									<a href="" class="uk-alert-close uk-close"></a>
									<p><?php echo $register_error; ?></p>
								</div>
								<?php } ?>
								<div class="uk-form-row">
									<label class="uk-form-label" for="register_names">Full Names</label>
									<div class="uk-form-controls">
										<input class="uk-width-1-1" type="text" onkeyup="this.value = camelCase(this.value)" id="register_names" name="register_names" value="<?php if (isset($register_names)) echo $register_names; ?>" autocomplete="names">
									</div>
								</div>
								<div class="uk-form-row">
									<label class="uk-form-label" for="register_email">Email Address</label>
									<div class="uk-form-controls">
										<input class="uk-width-1-1" type="text" id="register_email" name="register_email" value="<?php if (isset($register_email)) echo $register_email; ?>" autocomplete="email">
									</div>
								</div>
								<div class="uk-form-row">
									<label class="uk-form-label" for="register_password">Password</label>
									<div class="uk-form-controls">
										<input class="uk-width-1-1" type="password" id="register_password" name="register_password" value="<?php if (isset($register_password)) echo $register_password; ?>" autocomplete="new-password">
									</div>
								</div>
								<div class="uk-form-row">
									<label class="uk-form-label" for="register_confirm_password">Re-Enter Password</label>
									<div class="uk-form-controls">
										<input class="uk-width-1-1" type="password" id="register_confirm_password" name="register_confirm_password" value="<?php if (isset($register_confirm_password)) echo $register_confirm_password; ?>" autocomplete="new-password">
									</div>
								</div>
								<div class="uk-form-row uk-text-right uk-contrast">
									<button type="reset" class="uk-button">Reset</button>
									<button type="submit" class="uk-button uk-button-success x-min-150">Register</button>
								</div>
							</form>
							<?php } else { ?>
								<form action="" method="post" class="uk-form uk-form-stacked">
									<input type="hidden" id="register_account" name="register_account" value="<?php echo $signup_account; ?>">
									<?php if (isset($register_error)){ ?>
									<div class="uk-alert uk-alert-danger" data-uk-alert>
										<a href="" class="uk-alert-close uk-close"></a>
										<p><?php echo $register_error; ?></p>
									</div>
									<?php } ?>
									<div class="uk-form-row">
										<label class="uk-form-label" for="register_names">Full Names</label>
										<div class="uk-form-controls">
											<input class="uk-width-1-1" type="text" onkeyup="this.value = camelCase(this.value)" id="register_names" name="register_names" value="<?php if (isset($register_names)) echo $register_names; ?>" autocomplete="names">
										</div>
									</div>
									<div class="uk-form-row">
										<label class="uk-form-label" for="register_email">Email Address</label>
										<div class="uk-form-controls">
											<input class="uk-width-1-1" type="text" id="register_email" name="register_email" value="<?php if (isset($register_email)) echo $register_email; ?>" autocomplete="email">
										</div>
									</div>
									<div class="uk-form-row">
										<label class="uk-form-label" for="register_password">Password</label>
										<div class="uk-form-controls">
											<input class="uk-width-1-1" type="password" id="register_password" name="register_password" value="<?php if (isset($register_password)) echo $register_password; ?>" autocomplete="new-password">
										</div>
									</div>
									<div class="uk-form-row">
										<label class="uk-form-label" for="register_confirm_password">Re-Enter Password</label>
										<div class="uk-form-controls">
											<input class="uk-width-1-1" type="password" id="register_confirm_password" name="register_confirm_password" value="<?php if (isset($register_confirm_password)) echo $register_confirm_password; ?>" autocomplete="new-password">
										</div>
									</div>
									<div class="uk-form-row uk-text-right uk-contrast">
										<button type="reset" class="uk-button">Reset</button>
										<button type="submit" class="uk-button uk-button-success x-min-150">Register</button>
									</div>
								</form>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<?php if (strlen($redirect_url)){ ?>
	<script type="text/javascript">
		window["redirect"] = encodeURIComponent('<?php echo str_replace("'", "%27", $redirect_url); ?>');
	</script>
	<?php } ?>
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=signup"></script>
</body>
</html>
