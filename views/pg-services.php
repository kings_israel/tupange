<?php
//redirect view
if (!(isset($_GET["view"]) && in_array($_GET["view"], ["active", "paused", "deleted", "edit"]))){
	header("location: $root/services?view=active");
	exit();
}

//page variables
$load_vendor_data = true;
$load_vendor_services_stats = true;
switch ($_GET["view"]){
	case "active":
	$view_title = "Active Services";
	$page_title = "Tupange | My $view_title";
	$page_description = "View and manage active services";
	$load_vendor_services = 0;
	break;
	case "paused":
	$view_title = "Paused Services";
	$page_title = "Tupange | My $view_title";
	$page_description = "View and manage paused services";
	$load_vendor_services = -1;
	break;
	case "deleted":
	$view_title = "Deleted Services";
	$page_title = "Tupange | My $view_title";
	$page_description = "View and manage deleted services";
	$load_vendor_services = -2;
	break;
	case "edit":
	if (isset($_GET["s"]) && strlen($service_id = trim($_GET["s"]))){ //set service id
		$view_title = "Edit Service";
		$page_title = "Tupange | Edit Service";
		$page_description = "Vendor edit service details";
	}
	else {
		$view_title = "New Service";
		$page_title = "Tupange | New Service";
		$page_description = "Vendor create new service";
		unset($service_id);
	}
	break;
}

//requires session
include __DIR__ . "/service/session.php";

//allow vendors or admins only
if (!($session_userlevel == 2 && $session_status != 0 || $session_userlevel > 2)){
	header("location: $root/dashboard");
	exit();
}

//service edit pre processor
if ($_GET["view"] == "edit") include __DIR__ . "/service/pg-services-edit.php";
?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<?php include __DIR__ . "/parts/part-meta.php"; ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400">
	<link rel="stylesheet" href="<?php echo $root; ?>/assets/css/styles.php?p=services<?php if ($_GET["view"] == "edit") echo "-e"; ?>">
	<script src="<?php echo $root; ?>/assets/js/scripts.php?pre<?php if ($_GET["view"] == "edit") echo "=services-e"; ?>&min"></script>
</head>
<body>
	<!-- menu -->
	<?php include __DIR__ . "/parts/part-menu.php"; ?>
	<!-- page -->
	<section class="x-section-main">
		<div class="uk-container uk-container-center">
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-3 uk-width-large-1-4">
					<div class="x-box">
						<h2 class="x-box-title"> Menu <a id="menu-toggle" href="javascript:" onclick="toggleMenu()" class="uk-navbar-toggle uk-visible-small"></a></h2>
						<ul id="menu-toggle-content" class="uk-nav uk-nav-navbar uk-hidden-small">
							<li class="<?php if (isset($_GET["view"]) && $_GET["view"] == "active") echo "uk-active"; ?>">
								<a href="<?php echo $root; ?>/services/?view=active">
									<i class="uk-icon-star"></i> Active Services
									<?php if (isset($vendor_services_stats)){ ?><span class="x-badge-count"><?php echo $vendor_services_stats["active"]; ?></span><?php } ?>
								</a>
							</li>
							<li class="<?php if (isset($_GET["view"]) && $_GET["view"] == "paused") echo "uk-active"; ?>">
								<a href="<?php echo $root; ?>/services/?view=paused">
									<i class="uk-icon-pause"></i> Paused Services
									<?php if (isset($vendor_services_stats)){ ?><span class="x-badge-count"><?php echo $vendor_services_stats["paused"]; ?></span><?php } ?>
								</a>
							</li>
							<li class="<?php if (isset($_GET["view"]) && $_GET["view"] == "deleted") echo "uk-active"; ?>">
								<a href="<?php echo $root; ?>/services/?view=deleted">
									<i class="uk-icon-trash"></i> Deleted Services
									<?php if (isset($vendor_services_stats)){ ?><span class="x-badge-count"><?php echo $vendor_services_stats["deleted"]; ?></span><?php } ?>
								</a>
							</li>
						</ul>
						<script type="text/javascript">
						function toggleMenu(){
							let menu_content = document.getElementById("menu-toggle-content");
							if (menu_content.className.indexOf("x-display-block") > -1) menu_content.classList.remove("x-display-block");
							else menu_content.classList.add("x-display-block");
						}
						</script>
					</div>
					<div class="x-box uk-margin-top x-pad-20 uk-hidden-small">
						<h2>New Event?</h2>
						<div class="uk-contrast">
							<a class="uk-button uk-button-success uk-width-1-1 uk-margin-small-top" href="<?php echo $root; ?>/dashboard?edit-event">Create Event</a>
						</div>
					</div>
					<div class="x-box uk-margin-top x-pad-20 uk-hidden-small">
						<h2>Need Service?</h2>
						<div class="uk-contrast">
							<a class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-top" href="<?php echo $root; ?>/search?origin=request">Request Service</a>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-2-3 uk-width-large-3-4">
					<?php
					if (isset($_GET["view"])){
						if ($_GET["view"] == "active") include __DIR__ . "/parts/part-services-active.php";
						if ($_GET["view"] == "deleted") include __DIR__ . "/parts/part-services-deleted.php";
						if ($_GET["view"] == "paused") include __DIR__ . "/parts/part-services-paused.php";
						if ($_GET["view"] == "edit") include __DIR__ . "/parts/part-services-edit.php";
					}
					?>
				</div>
			</div>
		</div>
	</section>
	<!-- terms -->
	<?php include __DIR__ . "/parts/part-terms.php"; ?>
	<!-- footer -->
	<?php include __DIR__ . "/parts/part-footer.php"; ?>
	<!-- libraries -->
	<?php if ($_GET["view"] == "active"){ ?>
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=services-a"></script>
	<?php } if ($_GET["view"] == "deleted"){ ?>
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=services-d"></script>
	<?php } if ($_GET["view"] == "paused"){ ?>
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=services-p"></script>
	<?php } if ($_GET["view"] == "edit"){ ?>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCisnVFSnc5QVfU2Jm2W3oRLqMDrKwOEoM&libraries=places"></script>
	<script src="<?php echo $root; ?>/assets/js/scripts.php?p=services-e"></script>
	<?php } ?>
</body>
</html>
