<?php
//links
$links = "<br><small>Start <a href=\"./\">here</a> or <a href=\"javascript:history.back()\">go back</a>.</small>";

//get requested error page
if (isset($_GET["type"])){
	switch ($_GET["type"]){
		case "400":
		$error_code = "400";
		$error_title = "400 | Bad Request";
		$error_text = "<span>Bad Request!</span>" . $links;
		break;
		case "401":
		$error_code = "401";
		$error_title = "401 | Unauthorised Access";
		$error_text = "<span>Unauthorised Access!</span>" . $links;
		break;
		case "403":
		$error_code = "403";
		$error_title = "403 | Forbidden Access";
		$error_text = "<span>Forbidden Access!</span>" . $links;
		break;
		case "404":
		$error_code = "404";
		$error_title = "404 | Page Not Found";
		$error_text = "<span>Page Not Found!</span>" . $links;
		break;
		case "500":
		$error_code = "500";
		$error_title = "500 | Internal Server Error";
		$error_text = "<span>Internal Server Error!</span>" . $links;
		break;
	}
}

//defaults
if (!isset($error_code)) $error_code = "ERROR";
if (!isset($error_title)) $error_title = "ERROR | Unspecified Error";
if (!isset($error_text)) $error_text = "Unspecified Error!" . $links;

?>
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon-precomposed" href="assets/img/icon.png">
	<title><?php echo $error_title; ?></title>
	<style type="text/css">
	html, body {
		margin: 0;
		padding: 0;
		background: #fff;
	}
	.error-wrapper {
		display: flex;
		flex-direction: column;
		text-align: center;
		align-items: center;
		justify-content: center;
		height: 100vh;
		font-family: -apple-system, BlinkMacSystemFont, Roboto, "Segoe UI", "Fira Sans", Avenir, "Helvetica Neue", "Lucida Grande", sans-serif;
	}
	.error {
		padding: 10px;
		display: flex;
		flex-wrap: wrap;
		align-items: center;
		justify-content: center;
	}
	.error h1 {
		display: inline-block;
		border-right: 1px solid rgba(0, 0, 0, 0.3);
		margin: 0;
		margin-right: 20px;
		padding: 10px 23px 10px 0;
		font-size: 24px;
		font-weight: 700;
		vertical-align: top;
		color: #222;
		white-space: nowrap;
	}
	.error h2 {
		display: inline-block;
		text-align: left;
		line-height: 20px;
		vertical-align: middle;
		font-size: 14px;
		font-weight: normal;
		margin: 0;
		padding: 0;
		white-space: nowrap;
	}
	.error h2 span {
		font-weight: 500;
	}
	.error a {
		text-decoration: none;
		color: #2196F3;
	}
	.error a:focus,.error a:hover {
		text-decoration: underline;
	}
	.error a:active {
		color: #555;
		text-decoration: none;
	}
	</style>
</head>
<body>
	<div class="error-wrapper">
		<div class="error">
			<h1><?php echo $error_code; ?></h1>
			<h2><?php echo $error_text; ?></h2>
		</div>
	</div>
</body>
</html>
