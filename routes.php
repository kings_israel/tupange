<?php
const ROUTES = [
	//route pages
	"home" 			=> __DIR__ . "/views/pg-home.php",
	"signup" 		=> __DIR__ . "/views/pg-signup.php",
	"login" 		=> __DIR__ . "/views/pg-login.php",
	"forgot" 		=> __DIR__ . "/views/pg-forgot.php",
	"reset" 		=> __DIR__ . "/views/pg-reset.php",
	"verify" 		=> __DIR__ . "/views/pg-verify.php",
	"complete" 		=> __DIR__ . "/views/pg-complete.php",
	"services" 		=> __DIR__ . "/views/pg-services.php",
	"service" 		=> __DIR__ . "/views/pg-service.php",
	"search" 		=> __DIR__ . "/views/pg-search.php",
	"about" 		=> __DIR__ . "/views/pg-about.php",
	"edit-profile"  => __DIR__ . "/views/pg-profile.php",
	"e" 			=> __DIR__ . "/views/pg-rsvp.php",
	//incomplete

	"blog" 			=> __DIR__ . "/views/pg-blog.php",
	"support" 		=> __DIR__ . "/views/pg-support.php",
	"dashboard" 	=> __DIR__ . "/views/pg-dashboard.php",
	"messages" 		=> __DIR__ . "/views/pg-messages.php",
	"orders" 		=> __DIR__ . "/views/pg-orders.php",

	//placeholder
	"page"			=> __DIR__ . "/views/pg-page.php",

	//service routes
	"euser" 		=> __DIR__ . "/views/service/euser.php",
	"logout" 		=> __DIR__ . "/views/service/logout.php",
	"oauth" 		=> __DIR__ . "/views/service/oauth.php",
	"document" 		=> __DIR__ . "/views/service/document.php",
	"cart" 			=> __DIR__ . "/views/service/cart.php",
	"favorite" 		=> __DIR__ . "/views/service/favorite.php",
	"messenger" 	=> __DIR__ . "/views/service/messenger.php",
	"notifications" => __DIR__ . "/views/service/notifications.php",

	// Switch Profile
	"switch-profile" => __DIR__ . "/views/service/profile.php",
];
