<?php
//import ns library
require_once __DIR__ . "/includes.php";
use Naicode\Server\Funcs as fn1;

$data_categories = __DIR__ . "/data/categories.json";
$data_types = __DIR__ . "/data/types.json";
$data_locations = __DIR__ . "/data/locations.json";

if (isset($_GET["category"])){
	$categories = [];
	$search = fn1::toStrn($_GET["category"], true);
	$items = fn1::jsonParse(fn1::fileRead($data_categories, true), true, []);
	foreach ($items as $item){
		if ($search == "" || fn1::inStr($search, $item["name"], false)) $categories[] = $item;
	}
	header("Content-Type: application/json");
	echo fn1::jsonCreate($categories);
	exit();
}

if (isset($_GET["type"])){
	$types = [];
	$search = fn1::toStrn($_GET["type"], true);
	$items = fn1::jsonParse(fn1::fileRead($data_types, true), true, []);
	foreach ($items as $item){
		if ($search == "" || fn1::inStr($search, $item["name"], false)){
			if ($item['value'] == 0 && !isset($_GET['other'])) continue;
			$types[] = $item;
		}
	}
	header("Content-Type: application/json");
	echo fn1::jsonCreate($types);
	exit();
}

if (isset($_GET["location"])){
	$places = [];
	$search = fn1::toStrn($_GET["location"], true);
	$items = fn1::jsonParse(fn1::fileRead($data_locations, true), true, []);
	if (array_key_exists("data", $items)){
		$items = $items["data"];
		if (is_array($items) && count($items)){
			$items = $items[0];
			if (array_key_exists("children", $items)){
				$items = $items["children"];
				if (is_array($items) && count($items)){
					for ($i = 0; $i < count($items); $i ++){
						$child = $items[$i];
						$child_name = fn1::propval($child, "name");
						$child_type = fn1::propval($child, "type");
						$child_lat = fn1::propval($child, "latitude");
						$child_lon = fn1::propval($child, "longitude");
						$child_data = [
							"county" => "",
							"name" => $child_name,
							"type" => "COUNTY",
							"lat" => $child_lat,
							"lon" => $child_lon,
						];
						if ($search == "" || fn1::inStr($search, $child_name, false)) $places[] = $child_data;
						if (array_key_exists("children", $child)){
							$child_items = $child["children"];
							if (is_array($child_items) && count($child_items)){
								for ($x = 0; $x < count($child_items); $x ++){
									$child_item = $child_items[$x];
									$child_item_name = fn1::propval($child_item, "name");
									$child_item_type = fn1::propval($child_item, "type");
									$child_item_lat = fn1::propval($child_item, "latitude");
									$child_item_lon = fn1::propval($child_item, "longitude");
									$child_item_data = [
										"county" => $child_name,
										"name" => $child_item_name,
										"type" => "CITY",
										"lat" => $child_item_lat,
										"lon" => $child_item_lon,
									];
									if ($search == "" || fn1::inStr($search, $child_item_name, false)) $places[] = $child_item_data;
								}
							}
						}
					}
				}
			}
		}
	}
	header("Content-Type: application/json");
	echo fn1::jsonCreate($places);
	exit();
}

header("Content-Type: application/json");
echo fn1::jsonCreate([]);
exit();
