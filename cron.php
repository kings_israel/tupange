<?php
#Scheduled operations!
require_once __DIR__ . "/includes.php";
require_once __DIR__ . "/views/service/helper-event.php";
require_once __DIR__ . "/views/service/notifications.php";
require_once __DIR__ . "/views/service/messenger.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\NSDateTime;
use Naicode\Server\Database;
use Naicode\Server as s;

//constants
const LOG_MODE_ALL = 0;
const LOG_MODE_ERROR = 1;
const LOG_MODE_SUCCESS = 2;
const LOG_MODE_VERBOSE = 3;
const CRON_JOBS_LOG_MODE = LOG_MODE_ALL;
const CRON_JOBS_LOG_FILE = 'cron.log';

if (!defined('TASK_NOTIFY_DAILY')) define('TASK_NOTIFY_DAILY', 1);
if (!defined('TASK_NOTIFY_WEEKLY')) define('TASK_NOTIFY_WEEKLY', 2);
if (!defined('TASK_NOTIFY_MONTHLY')) define('TASK_NOTIFY_MONTHLY', 3);

//CRON PATH
if (isset($_GET['path'])){
	fn1::printr([
		'url' => NS_SITE . '/cron.php',
		'path' => __DIR__ . '/cron.php',
		'php' => PHP_BINARY,
	]);
	exit();
}

//CRON JOB: notify-task-due
if (isset($_GET['notify-task-due'])){
	$notified = [];
	$job = 'notify-task-due';
	if (($items = notify_task_due($notified)) === false){
		cron_log($job . ' [ERROR] ~ ' . $notify_task_due_error, LOG_MODE_ERROR);
		s::error(null, $notify_task_due_error);
	}
	if (count($notified)) cron_log($job . ' ~ ' . implode(', ', $notified), LOG_MODE_SUCCESS);
	else cron_log($job . ' ~ No new notifications', LOG_MODE_VERBOSE);
	s::success(count($notified) ? $notified : 0, $job);
}

//default response
$request = $_REQUEST;
unset($request['PHPSESSID']);
unset($request['xtoken']);
if (count($request)) s::error(null, 'Unsupported request!');

//RUN CRON JOB REQUESTS
$requests = [
	['query' => 'notify-task-due=1']
];
$cron_jobs = 0;
try {
	foreach ($requests as $request){
		$is_post = isset($request['post']) && $request['post'];
		$data = isset($request['data']) ? $request['data'] : null;
		$query = isset($request['query']) ? $request['query'] : null;

		$url_self = NS_SITE . '/cron.php';
		if ($query) $url_self .= '?' . $query;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url_self);

		if ($is_post){
			curl_setopt($ch, CURLOPT_POST, 1);
			if ($data) curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		curl_exec($ch);
		curl_close($ch);
		$cron_jobs ++;
	}
	cron_log('CRON JOBS ~ ' . $cron_jobs . ' Started!', LOG_MODE_VERBOSE);
	s::success($cron_jobs, 'Cron jobs started');
}
catch (Exception $e){
	$error = $e -> getMessage();
	cron_log('CRON JOBS [ERROR] ~ ' . $error, LOG_MODE_ERROR);
	s::error(null, $error);
}

#functions
function notify_task_due(&$notified=null){
	global $notify_task_due_error, $get_tuser_error, $get_event_item_error, $notification_clear_error, $notify_error;
	$db = null;
	try {
		$db = new Database();
		$task_complete = 2;
		$today = fn1::now('Y-m-d');
		$bind_query = "WHERE `person` <> '' AND `notify` <> '0' AND `date` <> '' AND `status` < '$task_complete' AND DATEDIFF(`date`, '$today') >= 0";
		$bind_data = null;
		$select_fields = ['*', "DATEDIFF(`date`, '$today') AS 'diff'", "IF(`time_notified` = '', -1, DATEDIFF(`time_notified`, '$today')) AS 'tdiff'"];
		if (!$result = $db -> select(TABLE_TASKS, $select_fields, $bind_query, $bind_data)) throw new Exception($db -> getErrorMessage());
		$notified = [];
		$task_events = [];
		if ($result instanceof mysqli_result){
			while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
				$row = xstrip($row);
				$notify = (int) $row['notify'];
				$diff = (int) $row['diff'];
				$tdiff = (int) $row['tdiff'];

				//check if can notify
				$do_notify = true;
				if ($do_notify && $notify === TASK_NOTIFY_WEEKLY && $tdiff < 7) $do_notify = false; //notify weekly - 7 days
				if ($do_notify && $notify === TASK_NOTIFY_MONTHLY && $tdiff < 30) $do_notify = false; //notify monthly - 30 days
				if ($do_notify && $notify === TASK_NOTIFY_DAILY && $tdiff < 1) $do_notify = false; //notify daily - 1 day
				if ($tdiff === -1) $do_notify = true; //notify if first time notify
				if ($diff === 1 && $tdiff >= 1) $do_notify = true; //notify if task due tomorrow
				if (!$do_notify) continue;

				//get task event
				$event_id = fn1::toStrn($row['event_id'], true);
				$event_data = null;
				if (!array_key_exists($event_id, $task_events)){
					if (!$event_data = get_event_item($event_id)) throw new Exception($get_event_item_error);
					$task_events[$event_id] = $event_data;
				}
				else $event_data = $task_events[$event_id];
				if (!fn1::hasKeys($event_data, 'id', 'uid')) throw new Exception('Invalid event data object');

				//get task user
				$user_id = fn1::toStrn($row['person'], true);
				if (!$tuser = get_tuser($user_id, $event_id, $event_data['uid'])) throw new Exception('Get user error: ' . $get_tuser_error);

				//notification
				$uid = $tuser['uid'];
				$task_id = fn1::toStrn($row['id'], true);
				$task = fn1::toStrn($row['task'], true);
				$tdate = fn1::toStrn($row['date'], true);
				$event_name = fn1::toStrn($event_data['name'], true);
				$date = format_task_date($tdate, false);
				$dtext = ($diff === 0 ? 'today' : ($diff === 1 ? 'tomorrow' : 'in ' . $diff . ' days')) . ' - ' . $date;
				$text = 'Your task "' . $task . '" for the event "' . $event_name . '" is due ' . $dtext;
				$html = '<h2>Hello ' . $tuser['names'] . ',</h2><p>Your task <strong>' . $task . '</strong> for the event <strong>' . $event_name . '</strong> is due ' . $dtext . '</p>';
				$action = NS_SITE . '/dashboard?event-checklist=' . urlencode($event_id) . '&task-assigned=' . urlencode($task_id);
				$ref = md5($task_id . '-' . $event_id . '-notify-task');

				//send notification
				if (!notification_clear($ref)) throw new Exception($notification_clear_error);
				if (!notify($uid, $text, $action, 'calendar', 0, $ref)) throw new Exception($notify_error);

				//send email notification
				$from_names = 'Tupange Event';
				$from_email = 'no-reply@' . NS_DOMAIN;
				$names = $tuser['names'];
				$email = $tuser['email'];
				$subject = 'Task Reminder - ' . $event_name;
				$message = $html;
				$button_link = $action;
				$button_text = 'Open Task';
				if (!email_message($from_names, $from_email, $names, $email, $subject, $message, $button_link, $button_text)) throw new Exception('There was an error sending email');

				//update task notified
				if (($affected_rows = update_time_notified($task_id, $user_id)) === false) throw new Exception($GLOBALS['update_time_notified_error']);

				//notified
				$notified[] = 'Notified: @' . $tuser['username'] . ' (' . $email . ') - ' . $text . ' ~ ' . $action;
			}
		}
		$db -> close();
		$db = null;
		return true;
	}
	catch (Exception $e){
		$notify_task_due_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function update_time_notified($task_id, $user_id){
	global $update_time_notified_error;
	$db = null;
	try {
		$db = new Database();
		$timestamp = fn1::now();
		$data = [
			'time_notified' => $timestamp,
		];
		if (!$db -> update(TABLE_TASKS, $data, 'WHERE `id` = ? AND `person` = ?', [$task_id, $user_id])) throw new Exception($db -> getErrorMessage());
		$affected_rows = (int) $db -> affected_rows;
		$db -> close();
		$db = null;
		return $affected_rows;
	}
	catch (Exception $e){
		$update_time_notified_error = $e -> getMessage();
		if ($db !== null) $db -> close();
		return false;
	}
}
function cron_log($str, $mode){
	if (CRON_JOBS_LOG_MODE !== LOG_MODE_ALL && $mode > CRON_JOBS_LOG_MODE) return;
	if (!($str = fn1::toStrn($str, true))) return;
	$file = fn1::logFile(CRON_JOBS_LOG_FILE);
	$log = fn1::now() . ': ' . $str;
	return fn1::fileWrite($file, $log, false, false);
}
