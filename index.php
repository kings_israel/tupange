<?php
#import ns library
require_once __DIR__ . "/includes.php";
require_once __DIR__ . "/routes.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server as s;

#init server to handle request
s::init();

#get request details (all requests)
if (!s::any("*", $request_params, $request_paths, $request_headers)) s::text("Request Failed!"); //very rare

#get route defauls
$root = NS_SITE;
$route_paths = [];
$route_child = "";
$route_page = "home";
if (count($request_paths = fn1::toArray($request_paths))){
	$route_page = strtolower($request_paths[0]);
	$route_paths = array_slice($request_paths, 1);
	if (count($route_paths)) $route_child = $route_paths[0];
}

//route include
$route_include = array_key_exists($route_page, ROUTES) ? ROUTES[$route_page] : ROUTES["page"];

#restore stored session
restoreSession();

#shared session variables
$placeholder_image = $root . "/assets/img/placeholder.jpg";
$default_avatar = $root . "/assets/img/user.png";
$session_uid = "";
$session_userlevel = 0;
$session_status = 0;
$session_description = "";
$session_avatar = $default_avatar;
$session_name = "";
if (sessionActive()){
	$session_uid = trim($_SESSION["uid"]);
	$session_userlevel = (int) $_SESSION["userlevel"];
	$session_status = (int) $_SESSION["status"];
	$session_description = trim($_SESSION["description"]);
	$session_avatar = isset($_SESSION["photo_url"]) && strlen($src = trim($_SESSION["photo_url"])) ? $src : $default_avatar;
	if (fn1::isFile(getUploads(true) . '/' . preg_replace('/^\/|\/$/s', '', $session_avatar))) $session_avatar = getUploads() . '/' . preg_replace('/^\/|\/$/s', '', $session_avatar);
	$session_name = fn1::strtocamel(fn1::toStrn($_SESSION["display_name"], true));
}

#include route page
include_once $route_include;
