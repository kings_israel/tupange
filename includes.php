<?php
#import ns library & globals
//if (!isset($_SERVER['HTTP_HOST'])) $_SERVER['HTTP_HOST'] = 'localhost';
if ($_SERVER["HTTP_HOST"] == "localhost"){
	require_once __DIR__ . "/globals_local.php";
	require_once __DIR__ . "/server/index.php";
}
else {
	require_once __DIR__ . "/globals_live.php";
	require_once __DIR__ . "/server/index.php";
}

#usage
use Naicode\Server\Lang as lg;
use Naicode\Server as s;
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\Main;
use Naicode\Server\Database;
use Naicode\Server\Mailer;
use Naicode\Server\Plugin\Audit;
use Naicode\Server\Plugin\Files;
use Naicode\Server\Plugin\EmailBuilder;
use Naicode\Server\Plugin\User;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;

#shared globals
const COOKIE_NAME = 'xtoken';
const SITE_VERSION = "1.0.2";
const GOOGLE_MAPS_API_KEY = "AIzaSyCN73V5VTrs-MzakmhHJ2Pe47lxOc7B6PE";

//tables ns_*
const TABLE_AUDIT = "ns_audit";
const TABLE_USERS = "ns_users";

//tables tb_*
const TABLE_ATTENDANCE = "tb_attendance";
const TABLE_BUDGET = "tb_budget";
const TABLE_BUDGET_TRANSACTIONS = "tb_budget_transactions";
const TABLE_CART = "tb_cart";
const TABLE_CHECKLIST_GROUPS = "tb_checklist_groups";
const TABLE_CHECKLIST_ITEMS = "tb_checklist_items";
const TABLE_EVENTS = "tb_events";
const TABLE_EVENT_GUESTS = "tb_event_guests";
const TABLE_FAVORITES = "tb_favorites";
const TABLE_GIFTS = "tb_gifts";
const TABLE_ORDERS = "tb_orders";
const TABLE_REGISTER = "tb_register";
const TABLE_REGISTER_TICKETS = "tb_register_tickets";
const TABLE_TEMP = "tb_temp";
const TABLE_TESTIMONIALS = "tb_testimonials";
const TABLE_VENDORS = "tb_vendors";
const TABLE_VENDOR_CATEGORIES = "tb_vendor_categories";
const TABLE_VENDOR_SERVICES = "tb_vendor_services";
const TABLE_VENDOR_SERVICE_PRICING = "tb_vendor_service_pricing";
const TABLE_MESSAGES = "tb_messages";
const TABLE_SERVICE_GALLERY = "tb_service_gallery";
const TABLE_REVIEWS = "tb_reviews";
const TABLE_PAYMENTS = "tb_payments";
const TABLE_TASKS = "tb_tasks";
const TABLE_EVENT_USERS = "tb_event_users";
const TABLE_NOTIFICATIONS = "tb_notifications";
const TABLE_TASK_COMMENTS = "tb_task_comments";
const TABLE_TASK_COMMENTS_SEEN = "tb_task_comments_seen";

#shared functions
function userFingerprint(){
	//fingerprint cookie
	$cookie_value = fn1::toStrx(fn1::propval($_COOKIE, COOKIE_NAME), true);
	if ($cookie_value == ''){
		$cookie_value = fn1::randomString(64);
		setcookie(COOKIE_NAME, $cookie_value, time() + (86400 * 30), '/');
	}

	//User ~ user fingerprint
	$user_ip = fn1::getIP();
	$user_useragent = fn1::getUserAgent();
	$user_fingerprint = md5($user_ip . $user_useragent . $cookie_value);

	//fn1::printr(['$user_fingerprint' => $user_fingerprint, '$cookie_value' => $cookie_value]); exit();
	return $user_fingerprint;
}
function getSessionToken(){
	//session start
	if (!isset($_SESSION)) session_start();
	if (isset($_SESSION["token"]) && !fn1::isEmpty($token = fn1::toStrx($_SESSION["token"], true))) return $token; //from session

	//from temp
	if (($temp_token = temp_get(userFingerprint(), "token")) === false) die ($GLOBALS['temp_get_error']);
	if (is_string($temp_token) && strlen($temp_token)) return $temp_token;
	return null;
}
function logoutSession(){
	//session start
	if (!isset($_SESSION)) session_start();

	//get session token
	$session_token = fn1::toStrx(getSessionToken(), true);
	if (strlen($session_token)){
		//logout session ~ User
		if (!User::logoutSession($session_token)) die (User::$last_error);

		//delete token from temp
		if (!temp_delete(null, null, $session_token)) die ($GLOBALS['temp_delete_error']);
	}

	//destroy fingerprint token
	unset($_COOKIE[COOKIE_NAME]);
    setcookie(COOKIE_NAME, null, -1, '/');

	//destroy session
	unset($_SESSION);
	session_destroy();
}
function restoreSession(){
	//session start
	if (!isset($_SESSION)) session_start();

	//get session token
	$session_token = fn1::toStrx(getSessionToken(), true);

	//login session token
	if (($user = User::getTokenUser($session_token)) !== false && $user instanceof User){
		$user -> token_fingerprint = userFingerprint(); //update fingerprint
		$user -> setSession(); //set session
		temp_save($user -> token_fingerprint, "token", $user -> token, 30); //temp token update
		return true;
	}
	return false;
}
function sessionActive(){
	if (!isset($_SESSION)) session_start();
	return isset($_SESSION["uid"]) && !fn1::isEmpty(fn1::toStrx($_SESSION["uid"], true)) && isset($_SESSION["userlevel"]);
}
function getUploads($get_uploads_folder=false){
	static $get_uploads_dir;
	static $get_uploads_url;
	if ($get_uploads_folder && $get_uploads_dir) return $get_uploads_dir;
	if (!$get_uploads_folder && $get_uploads_url) return $get_uploads_url;
	$uploads_folder = str_replace("\\", "/", NS_UPLOADS_FOLDER);
	if ($get_uploads_folder) return preg_replace('/\/\s*$/s', "", $uploads_folder);
	$site_dir = str_replace("\\", "/", NS_SITE_DIR);
	$uploads_root = str_replace($site_dir, "", $uploads_folder);
	$uploads_root = preg_replace('/^\s*\/|\/\s*$/s', "", $uploads_root);
	$path = preg_replace('/\/\s*$/s', "", NS_SITE . "/$uploads_root");
	if ($get_uploads_folder) $get_uploads_dir = $path;
	else $get_uploads_url = $path;
	return $path;
}
function xstrip($val, $htmlspecialchars=false, $stripslashes=true){
	if (fn1::isObject($val)) foreach($val as $key => $value) $val[$key] = xstrip($value, $htmlspecialchars);
	else if (!is_numeric($val) && is_string($val)){
		if ($stripslashes) $val = stripslashes($val);
		if ($htmlspecialchars) $val = htmlspecialchars($val);
	}
	return $val;
}
function minifyCSS($css){
	//remove comments
	$css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);

	//backup values within single or double quotes
	preg_match_all('/(\'[^\']*?\'|"[^"]*?")/ims', $css, $hit, PREG_PATTERN_ORDER);
 	for ($i = 0; $i < count($hit[1]); $i ++) $css = str_replace($hit[1][$i], '##########' . $i . '##########', $css);

	//remove traling semicolon of selector's last property
	$css = preg_replace('/;[\s\r\n\t]*?}[\s\r\n\t]*/ims', "}\r\n", $css);

	//remove any whitespace between semicolon and property-name
	$css = preg_replace('/;[\s\r\n\t]*?([\r\n]?[^\s\r\n\t])/ims', ';$1', $css);

	//remove any whitespace surrounding property-colon
	$css = preg_replace('/[\s\r\n\t]*:[\s\r\n\t]*?([^\s\r\n\t])/ims', ':$1', $css);

	//remove any whitespace surrounding selector-comma
	$css = preg_replace('/[\s\r\n\t]*,[\s\r\n\t]*?([^\s\r\n\t])/ims', ',$1', $css);

	//remove any whitespace surrounding opening parenthesis
	$css = preg_replace('/[\s\r\n\t]*{[\s\r\n\t]*?([^\s\r\n\t])/ims', '{$1', $css);

	//remove any whitespace between numbers and units
	$css = preg_replace('/([\d\.]+)[\s\r\n\t]+(px|em|pt|%)/ims', '$1$2', $css);

	//shorten zero-values
	$css = preg_replace('/([^\d\.]0)(px|em|pt|%)/ims', '$1', $css);

	//constrain multiple whitespaces
	$css = preg_replace('/\p{Zs}+/ims',' ', $css);

	//remove newlines
	$css = str_replace(array("\r\n", "\r", "\n"), '', $css);

	//restore backupped values within single or double quotes
	for ($i = 0; $i < count($hit[1]); $i ++) $css = str_replace('##########' . $i . '##########', $hit[1][$i], $css);

	//return minified css
	return $css;
}
function num_pad($num, $len=2, $pad="0", $left=true){
	$str_pad = $left ? STR_PAD_LEFT : STR_PAD_RIGHT;
	return str_pad($num, $len, $pad, $str_pad);
}
function num_commas($num, $places=2){
	return number_format(fn1::toNum($num), (int) $places);
}
function temp_save($ref, $key, $value, $valid_days=1){
	global $temp_save_error;
	$db = null;
	try {
		//get params
		$ref = fn1::toStrx($ref, true);
		$key = fn1::toStrx($key, true);
		$value = fn1::toStr($value, true, true);
		$valid_days = (int) $valid_days;
		if (!strlen($ref)) throw new Exception("Invalid temp reference");
		if (!strlen($key)) throw new Exception("Invalid temp key");
		if ($valid_days <= 0) throw new Exception("Invalid valid days value");

		//connection
		$db = new Database();

		//clean up expired
		$today = fn1::now('Y-m-d');
		$table = $db -> escapeString(TABLE_TEMP, true);
		if (!$db -> query("DELETE FROM `$table` WHERE `value` = '' OR ABS(DATEDIFF(`timestamp`, '$today')) > `valid_days`")) throw new Exception($db -> getErrorMessage());

		//update existing
		if (($existing = $db -> queryItem(TABLE_TEMP, null, "WHERE `ref` = ? AND `key` = ?", [$ref, $key])) === false) throw new Exception($db -> getErrorMessage());
		if ($existing && array_key_exists("_index", $existing)){
			//no empty value
			if (!strlen($value)){
				$db -> close(); //close connection
				if (!temp_delete($ref, $key)) throw new Exception($GLOBALS['temp_delete_error']);
				return true;
			}

			//update
			if (!$db -> update(TABLE_TEMP, ["value" => $value], "WHERE `_index` = ?", [$existing["_index"]])) throw new Exception($db -> getErrorMessage());
			$db -> close(); //close connection
			return true;
		}

		//no empty value
		if (!strlen($value)) return true;

		//create new
		$data = [
			"ref" => $ref,
			"key" => $key,
			"value" => $value,
			"valid_days" => (int) $valid_days,
		];
		if (!$db -> insert(TABLE_TEMP, $data)) throw new Exception($db -> getErrorMessage());
		$db -> close(); //close connection
		return true;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$temp_save_error = $e -> getMessage();
		return false;
	}
}
function temp_get($ref, $key, $return_value=true){
	global $temp_get_error;
	$db = null;
	try {
		//get params
		$ref = fn1::toStrx($ref, true);
		$key = fn1::toStrx($key, true);
		if (!strlen($ref)) throw new Exception("Invalid temp reference");
		if (!strlen($key)) throw new Exception("Invalid temp key");

		//connection
		$db = new Database();

		//clean up expired
		$today = fn1::now('Y-m-d');
		$table = $db -> escapeString(TABLE_TEMP, true);
		if (!$db -> query("DELETE FROM `$table` WHERE `value` = '' OR ABS(DATEDIFF(`timestamp`, '$today')) > `valid_days`")) throw new Exception($db -> getErrorMessage());

		//fetch temp entry
		if (($temp = $db -> queryItem(TABLE_TEMP, null, "WHERE `ref` = ? AND `key` = ?", [$ref, $key])) === false) throw new Exception($db -> getErrorMessage());

		$db -> close(); //close connection
		if ($temp && array_key_exists("value", $temp)){
			$temp = xstrip($temp);
			return $return_value ? $temp["value"] : $temp;
		}
		return null;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$temp_get_error = $e -> getMessage();
		return false;
	}
}
function temp_delete($ref=null, $key=null, $value=null){
	global $temp_delete_error;
	try {
		//connection
		$db = new Database();

		//build query
		$delete_where = [];
		if (!fn1::isEmpty($ref = fn1::toStrx($ref, true))) $delete_where[] = fn1::strbuild("`ref` = '%s'", $db -> escapeString($ref));
		if (!fn1::isEmpty($key = fn1::toStrx($key, true))) $delete_where[] = fn1::strbuild("`key` = '%s'", $db -> escapeString($key));
		if (!fn1::isEmpty($value = fn1::toStrn($value))) $delete_where[] = fn1::strbuild("`value` = '%s'", $db -> escapeString($value));
		if (fn1::isEmpty($delete_where)) throw new Exception("Invalid parameter values for this action");
		$table = $db -> escapeString(TABLE_TEMP, true);
		$delete_query = "DELETE FROM `$table` WHERE " . implode(" AND ", $delete_where);

		//run delete query
		if (!$db -> query($delete_query)) throw new Exception($db -> getErrorMessage());
		$db -> close(); //close connection
		return true;
	}
	catch (Exception $e){
		if ($db !== null) $db -> close();
		$temp_delete_error = $e -> getMessage();
		return false;
	}
}




























//eof
