<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190420
*	MAIN INCLUDE
*/

namespace Naicode\Server;
require_once __DIR__ . "/globals.php";
require_once __DIR__ . "/lang.php";
require_once __DIR__ . "/server.php";
require_once __DIR__ . "/funcs.php";
require_once __DIR__ . "/database.php";
require_once __DIR__ . "/mailer.php";
require_once __DIR__ . "/plugin/index.php";

#USE
use Naicode\Server\Lang as lg;
use Naicode\Server as s;
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\Main;
use Naicode\Server\Database;
use Naicode\Server\Mailer;

#USE Plugins
use Naicode\Server\Plugin\User;
use Naicode\Server\Plugin\Audit;
use Naicode\Server\Plugin\Files;
use Naicode\Server\Plugin\EmailBuilder;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
use Naicode\Server\Plugin\PDF;
use Naicode\Server\Plugin\PDFTable;
use Naicode\Server\Plugin\PDFTableReport;
use function Naicode\Server\Plugin\CreateQRCode;
use function Naicode\Server\Plugin\CreateBarcode;
