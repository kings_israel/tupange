<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190208
*	FUNCS ~ MAIN
*/

namespace Naicode\Server;
require_once __DIR__ . "/lang.php";

use Naicode\Server\Lang as lg;
use Naicode\Server\Funcs\Main;
use Naicode\Server\Funcs\NSDateTime;
use Naicode\Server\Funcs\NSDOMDocument;

class Funcs {
	const STOP_CRITICAL = 2;
	public static $last_error = "";
	public static $last_value = null;
	public static $case_match = true;
	public static $file_mimes = [
		"txt" => "text/plain",
		"htm" => "text/html",
		"html" => "text/html",
		"php" => "text/html",
		"css" => "text/css",
		"js" => "application/javascript",
		"json" => "application/json",
		"xml" => "application/xml",
		"swf" => "application/x-shockwave-flash",
		"flv" => "video/x-flv",
		"png" => "image/png",
		"jpe" => "image/jpeg",
		"jpeg" => "image/jpeg",
		"jpg" => "image/jpeg",
		"gif" => "image/gif",
		"bmp" => "image/bmp",
		"ico" => "image/vnd.microsoft.icon",
		"tiff" => "image/tiff",
		"tif" => "image/tiff",
		"svg" => "image/svg+xml",
		"svgz" => "image/svg+xml",
		"zip" => "application/zip",
		"rar" => "application/x-rar-compressed",
		"exe" => "application/x-msdownload",
		"msi" => "application/x-msdownload",
		"cab" => "application/vnd.ms-cab-compressed",
		"mp3" => "audio/mpeg",
		"qt" => "video/quicktime",
		"mov" => "video/quicktime",
		"pdf" => "application/pdf",
		"psd" => "image/vnd.adobe.photoshop",
		"ai" => "application/postscript",
		"eps" => "application/postscript",
		"ps" => "application/postscript",
		"doc" => "application/msword",
		"rtf" => "application/rtf",
		"xls" => "application/vnd.ms-excel",
		"ppt" => "application/vnd.ms-powerpoint",
		"odt" => "application/vnd.oasis.opendocument.text",
		"ods" => "application/vnd.oasis.opendocument.spreadshee",
		0 => "application/octet-stream",
	];
	public static function isObject($val, $include_arrays=true){
		return $include_arrays ? (is_object($val) || is_array($val)) : gettype($val) === "object";
	}
	public static function pregError($error=null){
		if ($error === null) $error = preg_last_error();
		$constants = get_defined_constants(true);
		$errors = [];
		foreach ($constants['pcre'] as $key => $value) if (self::regMatch('/_ERROR$/', $key)) $errors[$value] = $key;
		return isset($errors[$error]) ? $errors[$error] : null;
	}
	public static function jsonError($error=null){
		if ($error === null) $error = json_last_error();
		$constants = get_defined_constants(true);
		$errors = [];
		foreach ($constants["json"] as $key => $value) if (!strncmp($key, "JSON_ERROR_", 11)) $errors[$value] = $key;
		return isset($errors[$error]) ? $errors[$error] : null;
	}
	public static function isjson($val){
		return (self::jsonParse($val, false, false) === false);
	}
	public static function jsonCreate($val){
		return json_encode($val);
	}
	public static function jsonParse($val, $array=false, $default=null){
		self::$last_error = "";
		$data = json_decode($val, $array);
		if ($data === null && (($error = json_last_error()) !== JSON_ERROR_NONE)){
			self::$last_error = lg::str("FUNCS_ERROR_JSON_PARSE", self::jsonError($error));
			return $default;
		}
		return $data;
	}
	public static function toArray($val, $inset=false, $default=[], $callback=null, $callback_args=null){
		if (self::isEmpty($val)) $array = $default;
		elseif (self::isObject($val)) $array = (array) $val;
		elseif (is_string($val) && ($temp = self::jsonParse($val, true, false)) !== false) $array = $temp;
		elseif ($inset) $array = [$val];
		else $array = $default;
		if ($callback !== null){
			$args = [$array];
			if (!self::isEmpty($callback_args = self::toArray($callback_args))) $args = self::arraysMerge($args, $callback_args);
			if (is_array($temp = self::call($callback, $args))) $array = $temp;
		}
		return $array;
	}
	public static function toObject($val){
		return self::isObject($val, false) ? $val : self::jsonParse(self::jsonCreate($val));
	}
	public static function toNum($val, $default=0, $negatives=true, $min_limit=null, $max_limit=null, $limit_return_default=false){
		if (!is_numeric($default)) $default = 0;
		else $default = is_float($default) ? (float) $default : (int) $default;
		if (!($val !== null && is_numeric($val))) return $default;
		$val = is_float($val) ? (float) $val : (int) $val;
		if ($min_limit !== null && is_numeric($min_limit)){
			$min_limit = is_float($min_limit) ? (float) $min_limit : (int) $min_limit;
			if ($val < $min_limit) return $limit_return_default ? $default : $min_limit;
		}
		if ($max_limit !== null && is_numeric($max_limit)){
			$max_limit = is_float($max_limit) ? (float) $max_limit : (int) $max_limit;
			if ($val > $max_limit) return $limit_return_default ? $default : $max_limit;
		}
		if ($val < 0 && !$negatives) return $default;
		return $val;
	}
	public static function toStr($val, $trim=false, $stringify=false, $objects=true, $bools=true, $numbers=true){
		if (!is_string($val)){
			if ($numbers && is_numeric($val)) $val = "$val";
			else if ($bools && is_bool($val)) $val = $val ? "1" : "0";
			else if ($objects && self::isObject($val)) $val = $stringify ? self::jsonCreate($val) : "[" . gettype($val) . "]";
			else $val = "";
		}
		return $trim ? trim($val) : $val;
	}
	public static function toStrn($val, $trim=false){
		return self::toStr($val, $trim, false, false, false);
	}
	public static function toStrx($val, $trim=false){
		return self::toStr($val, $trim, false, false, false, false);
	}
	public static function trimVal($val, $remove_blanks=true, $recurse=false){
		if (is_string($val)) return trim($val);
		if (is_numeric($val) || is_bool($val) || $val === null) return $val;
		if (self::isObject($val)){
			$temp = [];
			$is_object = self::isObject($val, false);
			foreach ($val as $key => $value){
				if ($recurse && self::isObject($value)) $value = self::trimVal($value, $remove_blanks, $recurse);
				else $value = is_string($value) ? trim($value) : $value;
				if ($remove_blanks && self::isEmpty($value)) continue;
				$temp[$key] = $value;
			}
			return $is_object ? self::toObject($temp) : $temp;
		}
		return $val;
	}
	public static function isEmpty($val, $trim=false){
		if ($val === null) return true;
		if ($trim) $val = self::trimVal($val);
		if (is_string($val)) return !strlen($val);
		if (is_numeric($val) || is_bool($val)) return false;
		return empty($val);
	}
	public static function regMatch($search, $val, $offset=0, &$pos=-1, &$count=0, &$matches=null, $flag=PREG_OFFSET_CAPTURE){
		$pos = -1;
		$count = 0;
		self::$last_error = "";
		self::$last_value = null;
		$pattern = preg_match('/^\s*\/.+\/\s*[a-z\s]*$/i', ($search = self::toStr($search))) ? self::toStr($search, true, false, false) : '/' . $search . '/';
		$match_count = @preg_match_all($pattern, $val, $matches, $flag, (int) $offset);
		if ($match_count === false){
			if (($err = preg_last_error()) !== PREG_NO_ERROR) self::$last_error = "regMatch: " . self::pregError($err);
			return false;
		}
		if (!$match_count) return false;
		$matched_items = self::toArray($matches[0]);
		if (!count($matched_items)){
			self::$last_error = lg::str("FUNCS_ERROR_REGMATCH");
			return false;
		}
		$count = $match_count;
		$pos = $flag == PREG_OFFSET_CAPTURE ? (int) $matched_items[0][1] : -1;
		self::$last_value = $matched_items;
		return true;
	}
	public static function inStr($search, $val, $match_case=true, &$pos=-1, $offset=0){
		$pattern = self::regMatch('/^\s*\/.+\/\s*[a-z\s]*$/i', ($search = self::toStr($search))) ? self::toStr($search, true, false, false) : '/' . preg_quote($search, '/') . '/';
		if (!$match_case && !self::regMatch('/^\/.+\/[i]+$/', $pattern)) $pattern .= "i";
		return self::regMatch($pattern, $val, (int) $offset, $pos);
	}
	public static function replaceVal($search, $replace, $val, $match_case=true, &$count=null, $limit=-1){
		$pattern = self::regMatch('/^\s*\/.+\/\s*[a-z\s]*$/i', ($search = self::toStr($search))) ? self::toStr($search, true, false, false) : '/' . preg_quote($search, '/') . '/';
		if (!$match_case && !self::regMatch('/^\/.+\/[i]+$/', $pattern)) $pattern .= "i";
		self::$last_error = "";
		self::$last_value = null;
		$result = is_callable($replace) ? @preg_replace_callback($pattern, $replace, $val, (int) $limit, $count) : @preg_replace($pattern, $replace, $val, (int) $limit, $count);
		if ($result === null && ($err = preg_last_error()) !== PREG_NO_ERROR) self::$last_error = lg::str("FUNCS_ERROR_REPLACEVAL", self::pregError($err));
		self::$last_value = ["old" => $val, "new" => $result];
		return $result;
	}
	public static function valSplit($pattern, $val, $include_empty=true){
		$pattern = self::regMatch('/^\s*\/.+\/\s*[a-z\s]*$/i', ($pattern = self::toStr($pattern))) ? self::toStr($search, true, false, false) : '/' . preg_quote($pattern, '/') . '/';
		self::$last_error = "";
		self::$last_value = null;
		self::regMatch($pattern, $val, 0, $pos, $count, $matches, PREG_PATTERN_ORDER);
		$items = @preg_split($pattern, $val, -1, !$include_empty ? PREG_SPLIT_NO_EMPTY : 0);
		if ($items === false){
			if (($err = preg_last_error()) !== PREG_NO_ERROR) self::$last_error = lg::str("FUNCS_ERROR_VALSPLIT", self::pregError($err));
			return [];
		}
		return $items;
	}
	public static function valJoin($map, $val, $default_glue=""){
		self::$last_error = "";
		$val = self::toArray($val, true);
		if (self::isEmpty($map)) $map = $default_glue;
		if (self::isObject($map)){
			if (!is_array($map)){
				self::$last_error = lg::str("FUNCS_ERROR_VALJOIN");
				return null;
			}
			$m = 0;
			$buffer = "";
			$val_count = count($val);
			$map_count = count($map);
			for ($i = 0; $i < $val_count; $i ++){
				$part = $val[$i];
				$glue = $default_glue;
				if ($map_count){
					$glue = $map[$m];
					$m = $m < ($map_count - 1) ? $m + 1 : 0;
				}
				$buffer .= $i < ($val_count - 1) ? $part . $glue : $part;
			}
			return $buffer;
		}
		return implode($map, $val);
	}
	public static function call($callback=null){
		if ($callback !== null){
			$args = array_slice(func_get_args(), 1);
			if (count($args) == 1 && is_array($args[0])) $args = $args[0];
			return call_user_func_array($callback, $args);
		}
		return false;
	}
	public static function valKey($val, $key, $match_case=false, $return_key=true){
		self::$last_value = true;
		$val = self::toArray($val);
		if (array_key_exists($key, $val)) return $key;
		if (($i = self::arraySearch($key, ($keys = array_keys($val)), $match_case)) !== false) return $keys[$i];
		self::$last_value = false;
		return $return_key ? $key : null;
	}
	public static function inKeys($val, $array, $match_case=true){
		$key = self::valKey($array, $val, $match_case);
		return self::$last_value;
	}
	public static function inArray($val, $array, $match_case=true, $strict=true){
		$array = self::toArray($array);
		if ($match_case) return in_array($val, $array);
		return self::arraySearch($val, $array, $match_case, $strict) !== false;
	}
	public static function arraySearch($search, $haystack, $match_case=true, $strict=true){
		$haystack = self::toArray($haystack);
		if ($match_case) return array_search($search, $haystack, $strict);
		$search = is_string($search) || self::isObject($search) ? strtolower(self::toStr($search, false, true)) : $search;
		foreach ($haystack as $key => $value){
			$value = is_string($value) || self::isObject($value) ? strtolower(self::toStr($value, false, true)) : $value;
			if (($strict ? $search === $value : $search == $value)) return $key;
		}
		return false;
	}
	public static function arrayMerge($old, $new, $replace=true, $recurse=true){
		$temp = self::toArray($old);
		foreach (self::toArray($new) as $key => $value){
			$key = self::valKey($temp, $key);
			$key_exist = array_key_exists($key, $temp);
			if ($key_exist && !$replace) continue;
			if (is_array($value) && $recurse) $value = self::arrayMerge($key_exist ? $temp[$key] : [], $value, $replace, $recurse);
			$temp[$key] = $value;
		}
		return self::isObject($old, false) ? self::toObject($temp) : $temp;
	}
	public static function arraysMerge(){
		$items = [];
		foreach (func_get_args() as $item) $items = array_merge($items, self::toArray($item, false, []));
		return $items;
	}
	public static function arraysMergeTrim(){
		if (!is_array($items = self::call([__CLASS__, "arraysMerge"], func_get_args()))) return [];
		return self::trimVal($items);
	}
	public static function arrayFill($val=null, $len=1, $fill=null){
		$val = self::toArray($val);
		$len = ($len = self::toNum($len)) >= 0 ? $len : 0;
		if (($count = count($val)) >= $len) return $val;
		if (self::isEmpty($fills = self::toArray($fill, true, [], [__CLASS__, "trimVal"], [false, []]))) $fills = [""];
		$x = 0; $fills_count = count($fills);
		while (count($val) < $len){
			$fill = $fills[$x]; $x = $x == ($fills_count - 1) ? 0 : $x + 1;
			$val[] = $fill;
		}
		return $val;
	}
	public static function arraysDiff(){
		$diff = [];
		$temp = null;
		foreach (func_get_args() as $array){
			$array = self::toArray($array);
			if ($temp == null){
				$temp = $array;
				continue;
			}
			foreach ($array as $key => $value){
				if (array_key_exists($key, $temp)){
					$temp_value = $temp[$key];
					if (self::isObject($value) && self::isObject($temp_value)){
						$value_diff = self::arraysDiff($temp_value, $value);
						if (!self::isEmpty($value_diff)) $diff[$key] = $value_diff;
					}
					else {
						$val = self::toStr($value, false, true);
						$temp_val = self::toStr($temp_value, false, true);
						if ($val != $temp_val) $diff[$key] = $value;
					}
				}
				else $diff[$key] = $value;
			}
		}
		return $diff;
	}
	public static function arraysDiffVal($val){
		$diff = [];
		$temp = self::toArray($val);
		foreach (array_slice(func_get_args(), 1) as $array){
			$array = self::toArray($array);
			foreach ($array as $key => $value){
				if (!array_key_exists($key, $temp)) continue;
				$temp_value = $temp[$key];
				if (self::isObject($value) && self::isObject($temp_value)){
					$value_diff = self::arraysDiff($temp_value, $value);
					if (!self::isEmpty($value_diff)) $diff[$key] = $value_diff;
				}
				else {
					$val = self::toStr($value, false, true);
					$temp_val = self::toStr($temp_value, false, true);
					if ($val != $temp_val) $diff[$key] = $value;
				}
			}
		}
		return $diff;
	}
	public static function arraysCompare($old, $new, $match_keys=true){
		$to = [];
		$from = [];
		$old = self::toArray($old);
		$new = self::toArray($new);
		foreach ($old as $key => $value){
			if (array_key_exists($key, $new)){
				$new_value = $new[$key];
				if (is_array($value) && is_array($new_value)){
					$compare = self::arraysCompare($value, $new_value, $match_keys);
					if (is_array($compare)){
						$from[$key] = $compare["from"];
						$to[$key] = $compare["to"];
					}
				}
				elseif ($value != $new_value){
					$from[$key] = $value;
					$to[$key] = $new_value;
				}
			}
			elseif (!$match_keys){
				$from[$key] = $value;
				$to[$key] = null;
			}
		}
		if (!$match_keys){
			$compare_new = self::arraysCompare($new, $old, $match_keys);
			if (is_array($compare_new)){
				$from = self::arrayMerge($compare_new["to"]);
				$to = self::arrayMerge($compare_new["from"]);
			}
		}
		return !empty($from) ? ["from" => $from, "to" => $to] : null;
	}
	public static function propval($val){
		$is_object = self::isObject($val, false);
		$props = array_slice(func_get_args(), 1);
		if (count($props) == 1 && is_array($props[0])) $props = $props[0];
		if (count($props) && self::isObject($val)){
			$temp = self::toArray($val);
			foreach ($props as $prop){
				$keys = [];
				if (!!self::$case_match) $prop = self::valKey($temp, $prop);
				if (is_array($temp) && self::hasKeys($temp, $prop)) $temp = $temp[$prop];
				else return null;
			}
			return $is_object && self::isObject($temp) ? self::toObject($temp) : $temp;
		}
		return null;
	}
	public static function propset($val, $value){
		$props = array_slice(func_get_args(), 2);
		if (count($props) == 1 && is_array($props[0])) $props = $props[0];
		$is_object = self::isObject($val, false);
		$val = self::toArray($val);
		if (!!self::$case_match){
			$temp = $val;
			foreach ($props as $i => $prop){
				$prop = self::valKey($temp, $prop);
				if (!array_key_exists($prop, $temp)) break;
				$props[$i] = $prop;
				$temp = $temp[$prop];
			}
		}
		if ($props_count = count($props)){
			$new = $value;
			for ($i = ($props_count - 1); $i >= 0; $i --){
				$temp = [];
				$temp[$props[$i]] = $new;
				$new = $temp;
			}
			return self::arrayMerge($val, $new);
		}
		return $is_object ? self::toObject($val) : $val;
	}
	public static function objectXML($val){
		self::$last_error = "";
		if (self::isEmpty($val = self::toArray($val))) return null;
		$con = new NSDOMDocument("1.0", "utf-8");
		$con -> xmlStandalone = true;
		$con -> formatOutput = true;
		try {
			$con -> fromMixed($val);
			$con -> normalizeDocument();
			$xml = $con -> saveXML();
			return $xml;
		}
		catch (Exception $e){
			self::$last_error = lg::str("FUNCS_ERROR_OBJECTXML", $e -> getMessage());
			return null;
		}
	}
	public static function parseXMLDOM($node){
		$root = [];
		if ($node -> hasAttributes()) foreach($node -> attributes as $attr) $root["__attribute"][$attr -> name] = $attr -> value;
		if ($node -> hasChildNodes()){
			$children = $node -> childNodes;
			if ($children -> length == 1){
				$child = $children -> item(0);
				if ($child -> nodeType == XML_TEXT_NODE){
					$root["__value"] = self::trimVal($child -> nodeValue);
					return count($root) == 1 ? $root["__value"] : $root;
				}
			}
			$groups = [];
			foreach ($children as $child){
				if (!isset($root[$child -> nodeName]) && $child -> nodeType != XML_TEXT_NODE) $root[$child -> nodeName] = self::parseXMLDOM($child);
				else if (!isset($groups[$child -> nodeName])){
					$root[$child -> nodeName] = [isset($root[$child -> nodeName]) ? $root[$child -> nodeName] : ""];
					$groups[$child -> nodeName] = 1;
				}
				if ($child -> nodeType != XML_TEXT_NODE) $root[$child -> nodeName] = self::parseXMLDOM($child);
			}
		}
		return $root;
	}
	public static function parseHTML($val){
		$doc = new \DOMDocument();
		$doc -> loadHTML('<?xml encoding="UTF-8">' . $val);
		foreach ($doc -> childNodes as $item) if ($item -> nodeType == XML_PI_NODE) $doc -> removeChild($item); //remove hack
		$doc -> encoding = "UTF-8"; //insert proper
		return self::parseXMLDOM($doc);
	}
	public static function parseXML($val){
		$node = new \DOMDocument();
		if ($node -> loadXML($val) === false) return null;
		return self::parseXMLDOM($node);
	}
	public static function compressTags($val){
		$val = self::replaceVal('~>\s+<~', '><', $val);
		$val = self::replaceVal('/\s\s+/', ' ', $val);
		for ($i = 0; $i < 5; $i ++) $val = str_replace('  ', ' ', $val);
		$val = self::replaceVal('/<!--(.|\s)*?-->/', '', $val);
		return self::trimVal($val);
	}
	public static function stripHTML($val){
		$val = strip_tags(self::compressTags($val), "<h1><h2><h3><h4><h5><h6><p><a><ul><ol><li><table><tr><td>");
		$val = self::replaceVal('/<a.*?href\s*=\s*[\'"](.*?)[\'"].*?(?=>)>(.*?(?=<\/a>))<\/a>/is', function($matches){
			return "<p>[" . self::toStr(self::propval($matches, 2)) . " - " . self::toStr(self::propval($matches, 1)) . "](Copy-paste link in browser to open)</p>";
		}, $val);
		$val = self::replaceVal('/<\/(\s*)?td([^>]*)?><(\s*)?td[^>]*?>/is', "|", $val);
		$val = self::replaceVal('/<(\s*)?li[^>]*?>/is', "\t", $val);
		$val = self::replaceVal('/<[0-9a-z\s]+>/is', "", $val);
		$val = self::replaceVal('/<\/(\s*)?td([^>]*)?>/is', "", $val);
		$val = self::replaceVal('/<\/(\s*)?(h[0-9]|p)[^>]*?>/is', "\r\n\r\n", $val);
		$val = self::replaceVal('/<\/[0-9a-z\s]+>/is', "\r\n", $val);
		return self::trimVal($val);
	}
	public static function strtocamel($val, $trim=false, $split_regex=' '){ //$split_regex='/[^0-9a-z]/i'
		$val = self::toStr($val, $trim, false, false);
		if (!count($val_parts = self::valSplit($split_regex, $val))) return "";
		$words = [];
		$parts_map = self::$last_value;
		foreach ($val_parts as $part){
			if (self::inStr('/[^\\s]/', $part, true, $pos)){
				$temp = substr($part, $pos);
				$word = strtoupper(substr($part, $pos, 1)) . strtolower(substr($part, $pos + 1));
				if ($temp !== $part) $word = str_replace($temp, $word, $part);
			}
			else $word = $part;
			$words[] = $word;
		}
		return self::valJoin($parts_map, $words);
	}
	public static function isEmail($val){
		return filter_var(self::toStr($val, true), FILTER_VALIDATE_EMAIL) !== false;
	}
	public static function isSafaricomNumber($val) {
		$saf_regex = "^(?:254|\+254|0)?((?:(?:7(?:(?:[01249][0-9])|(?:5[789])|(?:6[89])))|(?:1(?:[1][0-5])))[0-9]{6})$";
		return self::regMatch($saf_regex, $val);
	}
	public static function isAirtelNumber($val) {
		$airtel_regex = "^(?:254|\+254|0)?((?:(?:7(?:(?:3[0-9])|(?:5[0-6])|(8[5-9])))|(?:1(?:[0][0-2])))[0-9]{6})$";
		return self::regMatch($airtel_regex, $val);
	}
	public static function isUrl($val){
		$regex = "((https?|ftp)\:\/\/)?"; // SCHEME
		$regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
		$regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
		$regex .= "(\:[0-9]{2,5})?"; // Port
		$regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
		$regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
		$regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor
		return self::regMatch("/^$regex$/i", $val);
	}
	public static function urlDomain($url){
		if (!self::isUrl($url = self::toStr($url, true))) return null;
		if (!self::regMatch('/^([^\.]*\.)?(.*)$/', self::replaceVal('/^.*\/\//', "", $url), 0, $p, $c, $matches, PREG_PATTERN_ORDER)) return null;
		if (count($matches) != 3) return null;
		$part0 = $matches[0][0];
		$part1 = $matches[1][0];
		$part2 = $matches[2][0];
		if (self::regMatch('/\.$/', $part1) && self::regMatch('/\./', $part2)) return $part2;
		if (self::regMatch('/\.$/', $part1) && !self::regMatch('/\./', $part2)) return $part0;
		return $part2;
	}
	public static function domain(){
		$host = $_SERVER["HTTP_HOST"];
		$domain = self::urlDomain($host);
		if (self::isEmpty($domain)) $domain = $host;
		return $domain;
	}
	public static function baseDir(){
		return getcwd();
	}
	public static function baseUrl(){
		return self::replaceVal('/\/$/', "", sprintf(
			"%s://%s%s",
			isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off" ? "https" : "http",
			$_SERVER["SERVER_NAME"],
			$_SERVER["REQUEST_URI"]
		));
	}
	public static function hasKeys($val){
		if (self::isEmpty($val = self::toArray($val))) return false;
		$props = self::trimVal(array_slice(func_get_args(), 1));
		if (count($props) == 1 && is_array($props[0])) $props = $props[0];
		if (!count($props)) return false;
		$keys = array_keys(self::toArray($val));
		foreach ($props as $key) if (!in_array($key, $keys)) return false;
		return true;
	}
	public static function hasAnyKeys($val){
		if (self::isEmpty($val = self::toArray($val))) return false;
		$props = self::trimVal(array_slice(func_get_args(), 1));
		if (count($props) == 1 && is_array($props[0])) $props = $props[0];
		if (!count($props)) return false;
		$keys = array_keys(self::toArray($val));
		foreach ($props as $key) if (in_array($key, $keys)) return true;
		return false;
	}
	public static function ihasKeys($val){
		$props = self::trimVal(array_slice(func_get_args(), 1));
		if (count($props) == 1 && is_array($props[0])) $props = $props[0];
		if (!count($props)) return false;
		$keys = self::jsonParse(strtolower(self::jsonCreate(array_keys(self::toArray($val)))), true, []);
		foreach ($props as $key) if (!in_array(strtolower($key), $keys)) return false;
		return true;
	}
	public static function filterEmail($email){
		if (self::isEmpty($email = strtolower(self::toStr($email, true)))) return $email;
		$rule = ["\r" => "", "\n" => "", "\t" => "", '"' => "", "," => "", "<" => "", ">" => ""];
		$email = strtr($email, $rule);
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);
		return $email;
	}
	public static function filterName($name, $trim=true, $camelcase=false){
		if (self::isEmpty($name = self::toStr($name, $trim))) return $name;
		if ($camelcase) $name = self::strtocamel($name);
		$rule = ["\r" => "", "\n" => "", "\t" => "", '"' => "'", "<" => "[", ">" => "]"];
		return strtr(filter_var($name, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES), $rule);
	}
	public static function filterOther($val){
		return filter_var($val, FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW);
	}
	public static function filterRequestValue($val){
		if (self::isObject($val)){
			$temp = [];
			$is_object = self::isObject($val, false);
			foreach ($val as $key => $value) $temp[self::filterName($key, true)] = self::filterRequestValue($value);
			return $is_object ? self::toObject($temp) : $temp;
		}
		return $val !== null && !is_bool($val) && in_array(strtolower(self::toStr($val = self::filterOther($val))), ["undefined", "null", ""]) ? "" : $val;
	}
	public static function strbuild($val=""){
		$val = self::toStrn($val);
		if (!self::regMatch('/%s/is', $val, 0, $p, $count)) return preg_replace('/%s/is', "", $val);
		$args = array_slice(func_get_args(), 1);
		if (count($args) == 1 && is_array($args[0])) $args = $args[0];
		if (count($args) < $count) $args = self::arrayFill($args, $count);
		$args = array_map(function($arg){ return self::toStr($arg, false, true); }, $args);
		array_unshift($args, $val);
		return self::call("sprintf", $args);
	}
	public static function prints(){
		$buffer = "";
		foreach (func_get_args() as $arg) $buffer .= ($buffer == "" ? "" : "\r\n") . (self::isObject($arg) ? trim(print_r($arg, true)) : print_r($arg, true));
		return $buffer;
	}
	public static function printr(){
		header("Content-Type: text/plain");
		return print(self::call([__CLASS__, "prints"], func_get_args()) . "\r\n");
	}
	public static function printl(){
		header("Content-Type: text/plain");
		return print(self::call([__CLASS__, "prints"], array_map(function($arg){ return self::toStr($arg, false, true); }, func_get_args())) . "\r\n");
	}
	public static function formatDate($date=null, $format="Y-m-d H:i:s"){
		return (new NSDateTime($date)) -> format($format);
	}
	public static function parseDate($date=null, $format="Y-m-d H:i:s"){
		return new NSDateTime($date, $format);
	}
	public static function parseFormatDate($date=null, $from_format="Y-m-d H:i:s", $to_format="Y-m-d H:i:s"){
		return (new NSDateTime($date, $from_format)) -> format($to_format);
	}
	public static function timestamp($format="Y-m-d H:i:s"){
		return self::formatDate(null, $format);
	}
	public static function now($format="Y-m-d H:i:s"){
		$timestamp = self::timestamp($format);
		return is_numeric($timestamp) ? self::toNum($timestamp) : $timestamp;
	}
	public static function serverHostname($host=null){
		$result = "";
		if (!empty($host)) $result = $host;
		elseif (isset($_SERVER) and array_key_exists("SERVER_NAME", $_SERVER)) $result = $_SERVER["SERVER_NAME"];
		elseif (function_exists("gethostname") && ($temp = gethostname()) !== false) $result = $temp;
		elseif (($temp = php_uname("n")) !== false) $result = $temp;
		if (!self::isValidHost($result)) return "localhost.localdomain";
		return $result;
	}
	public static function isValidHost($host){
		if (empty($host) || !is_string($host) || strlen($host) > 256) return false;
		if (trim($host, "[]") != $host) return (bool) filter_var(trim($host, "[]"), FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
		if (is_numeric(str_replace(".", "", $host))) return (bool) filter_var($host, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
		return (bool) filter_var('http://' . $host, FILTER_VALIDATE_URL);
	}
	public static function isDir($path, $is_readable=true){
		return @file_exists($path) && is_dir($path) && ($is_readable ? is_readable($path) : true);
	}
	public static function isFile($path, $is_readable=true){
		return @file_exists($path) && is_file($path) && ($is_readable ? is_readable($path) : true);
	}
	public static function dirItems($dir, $recurse=false, $size=false, $child=false){
		if (!(file_exists($dir = str_replace("\\", "/", $dir)) && is_dir($dir) && is_readable($dir))) return null;
		$items = [];
		if (!$child) self::$last_value = 0;
		$scan = scandir($dir);
		foreach ($scan as $item){
			if ($item == '.' || $item == '..') continue;
			$path = str_replace("\\", "/", $dir . "/" . $item);
			$items[] = $path;
			if (is_dir($path) && $recurse) $items = self::arraysMerge($items, self::dirItems($path, $recurse, $size, true));
			elseif (is_file($path) && $size) self::$last_value = self::toNum(self::$last_value) + filesize($path);
			elseif (!is_dir($path) && !is_file($path)) return null;
		}
		return $items;
	}
	public static function dirEmpty($dir){
		return ($items = self::dirItems($dir)) !== null ? count($items) == 0 : null;
	}
	public static function dirDelete($dir, $purge=false, $throw_exception=true){
		if (file_exists($dir) && is_dir($dir)) return self::delPath($dir, $purge, $throw_exception);
		return true;
	}
	public static function dirCreate($dir, $throw_exception=true){
		self::$last_error = null; self::$last_value = null;
		if (self::isEmpty(self::toStrn($dir, true))) return null;
		if (file_exists($dir) && is_dir($dir)) return true;
		$path = "";
		foreach (explode("/", str_replace("\\", "/", $dir)) as $part){
			$path .= strlen($path) ? "/" . $part : $part;
			if (!(file_exists($path) && is_dir($path))){
				if (!@mkdir($path)){
					$error = lg::str("FUNCS_ERROR_CREATE_FOLDER", $path);
					if ($throw_exception) throw new \Exception($error, self::STOP_CRITICAL);
					self::$last_error = $error;
					return false;
				}
			}
		}
		self::$last_value = $path;
		return true;
	}
	public static function delPath($path, $purge=false, $throw_exception=true, $main_path=null){
		self::$last_error = null; self::$last_value = null;
		if (self::isEmpty(self::toStrn(($path = str_replace("\\", "/", $path)), true))) return null;
		if (!file_exists($path)) return true;
		$root_dir = str_replace("\\", "/", $_SERVER["DOCUMENT_ROOT"]);
		if (is_file($path)){
			if (!@unlink($path)){
				$path = str_replace($root_dir, "~", $path);
				$error = lg::str("FUNCS_ERROR_DELETE_FILE", $path);
				if ($throw_exception) throw new \Exception($error, self::STOP_CRITICAL);
				self::$last_error = $error;
				return false;
			}
			self::$last_value = $path;
			return true;
		}
		else if (is_dir($path)){
			if (self::dirEmpty($path) === false){
				if ($purge){
					if (($items = self::dirItems($path)) === null){
						$path = str_replace($root_dir, "~", $path);
						$error = lg::str("FUNCS_ERROR_DELETE_FOLDER", $path . ($main_path !== null ? " [$main_path]" : ""));
						if ($throw_exception) throw new \Exception($error, self::STOP_CRITICAL);
						self::$last_error = $error;
						return false;
					}
					foreach ($items as $item) if (!self::delPath($item, $purge, $throw_exception, $path)) return false;
					if (!self::delPath($path, false, $throw_exception)) return false;
				}
				else {
					$path = str_replace($root_dir, "~", $path);
					$error = lg::str("FUNCS_ERROR_DELETE_FOLDER_EMPTY", $path . ($main_path !== null ? " [$main_path]" : ""));
					if ($throw_exception) throw new \Exception($error, self::STOP_CRITICAL);
					self::$last_error = $error;
					return false;
				}
			}
			else if (!@rmdir($path)){
				$path = str_replace($root_dir, "~", $path);
				$error = lg::str("FUNCS_ERROR_DELETE_FOLDER", $path . ($main_path !== null ? " [$main_path]" : ""));
				if ($throw_exception) throw new \Exception($error, self::STOP_CRITICAL);
				self::$last_error = $error;
				return false;
			}
			self::$last_value = $path;
			return true;
		}
		self::$last_error = lg::str("FUNCS_ERROR_DELETE_PATH", $path);
		return false;
	}
	public static function pathSize($path){
		self::$last_error = null; self::$last_value = null;
		if (!self::isEmpty(self::toStrn(($path = str_replace("\\", "/", $path)), true)) && file_exists($path)){
			if (is_file($path)) return filesize($path);
			else if (is_dir($path) && self::dirItems($path, true, true) !== null){
				$size = self::toNum(self::$last_value);
				self::$last_value = null;
				return $size;
			}
		}
		return null;
	}
	public static function pathInfo($path, $size=false){
		self::$last_error = null; self::$last_value = null;
		if (self::isEmpty(self::toStrn(($path = str_replace("\\", "/", $path)), true))) return null;
		if (self::isEmpty($parts = @pathinfo($path))) return null;
		$info = [
			"path" => $path,
			"dir" => self::propval($parts, "dirname"),
			"filename" => self::propval($parts, "basename"),
			"name" => self::propval($parts, "filename"),
			"ext" => self::propval($parts, "extension"),
		];
		if ($size) $info["size"] = self::pathSize($path);
		self::$last_value = $path;
		return $info;
	}
	public static function fileCopy($path, $destination){
		if (self::isFile($path)){
			if (!($dest_info = self::pathInfo($destination))) return $dest_info;
			if (!($dir_create = self::dirCreate($dest_info["dir"]))) return $dir_create;
			$dest = $dest_info["dir"] . "/" . $dest_info["filename"];
			return @copy($path, $dest);
		}
		return false;
	}
	public static function fileDelete($path, $throw_exception=true){
		if (file_exists($path) && is_file($path)) return self::delPath($path, false, $throw_exception);
		return true;
	}
	public static function fileWrite($path, $content=null, $overwrite=false, $throw_exception=true){
		self::$last_error = null; self::$last_value = null;
		if (self::isEmpty(self::toStrn(($path = str_replace("\\", "/", $path)), true))) return null;
		$root_dir = str_replace("\\", "/", $_SERVER["DOCUMENT_ROOT"]);
		if (($parts = self::pathInfo($path)) === null){
			$path = str_replace($root_dir, "~", $path);
			$error = lg::str("FUNCS_ERROR_FILE_WRITE_PATH", $path);
			if ($throw_exception) throw new \Exception($error, self::STOP_CRITICAL);
			self::$last_error = $error;
			return false;
		}
		if ($overwrite) self::delPath($path, false, $throw_exception);
		$dir = $parts["dir"];
		if (self::dirCreate($dir, $throw_exception) !== true){
			$dir = str_replace($root_dir, "~", $dir);
			$path = str_replace($root_dir, "~", $path);
			$error = lg::str("FUNCS_ERROR_FILE_WRITE_FOLDER", $path, $dir);
			if ($throw_exception) throw new \Exception($error, self::STOP_CRITICAL);
			self::$last_error = $error;
			return false;
		}
		$content = (file_exists($path) && is_file($path) ? PHP_EOL : "") . (self::isObject($content) ? print_r($content, true) : self::toStr($content, false, true));
		if (@file_put_contents($path, $content, ($overwrite ? LOCK_EX : FILE_APPEND | LOCK_EX)) === false){
			$path = str_replace($root_dir, "~", $path);
			$error = lg::str("FUNCS_ERROR_FILE_WRITE", $path);
			if ($throw_exception) throw new \Exception($error, self::STOP_CRITICAL);
			self::$last_error = $error;
			return false;
		}
		return true;
	}
	public static function fileRead($path, $trim=false){
		if (!(file_exists($path) && is_file($path) && is_readable($path))) return null;
		$handle = fopen($path, "r");
		$data = fread($handle, filesize($path));
		fclose($handle);
		return $trim ? trim($data) : $data;
	}
	public static function fileMimeType($path){
		if (function_exists("mime_content_type")) return mime_content_type($path);
		$ext = ($pathinfo = self::pathInfo($path)) !== null ? $pathinfo["ext"] : null;
		return self::extMimeType($ext);
	}
	public static function extMimeType($ext){
		$ext = strtolower(self::toStrn($ext, true));
		if (array_key_exists($ext, self::$file_mimes)) return self::$file_mimes[$ext];
		if (function_exists('finfo_open')){
            $finfo = @finfo_open(FILEINFO_MIME);
            $mime = @finfo_file($finfo, "filename.$ext");
            @finfo_close($finfo);
            return $mime;
        }
		return self::$file_mimes[0];
	}
	public static function dsvArray($val, $delimiter=",", $enclosure="\"", $escape="\"", $include_enclosing=false, $skip_empty_lines=false, $trim_delimiter=true){
		$val = str_replace(["\r\n", "\r"], "\n", self::toStr($val, false, false, false)); //normalize line breaks
		$val = str_replace(["+", $escape . $enclosure], ["!!P!!", "!!Q!!"], $val); //encode special characters
		$r = preg_quote($enclosure, "|");
		$val = @preg_replace_callback("|" . $r . "[^" . $r . "]*(\\n)[^" . $r . "]*" . $r . "|", function($match){
			return str_replace("\n", "!!N!!", $match[0]); //encode enclosed line breaks
		}, $val);
		$lines = preg_split('/\\n/s', preg_replace_callback('/' . $enclosure . '(.*?)' . $enclosure . '/s', function($field) use (&$include_enclosing){
			return urlencode(utf8_encode($field[$include_enclosing ? 0 : 1]));
		}, $val));
		$rows = [];
		foreach ($lines as $line){
			if (self::isEmpty($line = self::toStr($line, $trim_delimiter, false, false)) && $skip_empty_lines) continue;
			$fields = explode($delimiter, $line);
			$rows[] = array_map(function($field) use (&$escape, &$enclosure){
				return str_replace(["!!Q!!", "!!P!!", "!!N!!"], [$enclosure, "+", "\r\n"], utf8_decode(urldecode($field))); //restore encodings
			}, ($trim_delimiter ? array_map("trim", $fields) : $fields));
		}
		return $rows;
	}
	public static function csvArray($val, $skip_empty_lines=false, $trim_delimiter=true){
		return self::dsvArray($val, ",", "\"", "\"", false, $skip_empty_lines, $trim_delimiter);
	}
	public static function arrayCSV($val, $download=false, $filename="download.csv"){
		$data = self::toArray($val);
		if (self::isEmpty($data)) return null;
		$csvStr = function($str){
			$str = self::isObject($str) ? self::jsonCreate($str) : (string) $str;
			$str = str_replace('"', '""', $str);
			if (preg_match('/\s|,/s', $str)) $str = "\"$str\"";
			return $str;
		};
		$csv_rows = [];
		foreach ($data as $row){
			$csv_row = [];
			$row = self::toArray($row);
			if (self::isEmpty($row)) continue;
			foreach ($row as $key => $value) $csv_row[] = $csvStr($value);
			$csv_rows[] = implode(",", $csv_row);
		}
		$csv = implode("\r\n", $csv_rows);
		if (!strlen($filename = self::toStrn($filename, true))) $filename = "download.csv";
		if (!preg_match('/\.csv$/is', $filename)) $filename .= ".csv";
		if (!$download) return ["csv" => $csv, "filename" => $filename];
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . strlen($csv));
		echo $csv;
		exit();
	}
	public static function randomString($len=32, $lowercase=true, $uppercase=true, $numbers=true, $special_characters=false){
		if (($len = (int) $len) <= 0) $len = 32;
		if (!($lowercase || $uppercase || $numbers || $special_characters)){
			$lowercase = true;
			$uppercase = true;
			$numbers = true;
			$special_characters = false;
		}
		$chars = [];
		$abc = "abcdefghijklmnopqrstuvwxyz";
		if ($lowercase) $chars[] = $abc;
		if ($uppercase) $chars[] = strtoupper($abc);
		if ($numbers) $chars[] = "0123456789";
		if ($special_characters) $chars[] = "`~!@#$%^&*()_-+={[}]\\|:;\"''<,>.?/";
		$buffer = "";
		$chars = implode("", $chars);
		$chars_len = strlen($chars);
		while (strlen($buffer) < $len) $buffer .= substr($chars, rand(0, $chars_len - 1), 1);
		return $buffer;
	}
	public static function logFile($file=null){
		$base = str_replace("\\", "/", self::baseDir());
		if (self::isEmpty($file = self::toStrn($file, true))) $file = $base . "/logs.txt";
		else $file = $base . "/" . str_replace("//", "/", str_replace($base . "/", "", str_replace("\\", "/", $file)));
		return $file;
	}
	public static function log($content, $file=null, $overwrite=false, $throw_exception=true){
		return self::fileWrite(self::logFile($file), $content, $overwrite, $throw_exception);
	}
	public static function curlsReplace($val, $replacements, $replace_all=false){
		$val = self::toStrx($val);
		if (!self::isEmpty($replacements = self::toArray($replacements))) foreach ($replacements as $search => $replace) $val = self::replaceVal('/\\{\\{' . preg_quote(self::toStrn($search)) . '\\}\\}/is', self::toStrn($replace), $val);
		return $replace_all ? self::replaceVal('/\\{\\{[^\\{\\}]*\\}\\}/s', "", $val) : $val;
	}
	public static function strbreaks($val, $replace="\r\n", $trim=false){
		$val = str_replace("\r", "", self::toStrn($val, $trim));
		return self::toStrn(str_replace("\n", $replace, $val), $trim);
	}
	public static function strnobreak($val, $trim=true){
		return self::strbreaks($val, " ", $trim);
	}
	public static function defaultStr($val, $default="", $trim=true, $breaks=true){
		$val = $breaks ? self::toStrn($val, $trim) : self::strnobreak($val, $trim);
		return self::isEmpty($val) ? self::toStrn($default) : $val;
	}
	public static function defaultArray($val, $default=[], $trim=true){
		$val = $trim ? self::toArray($val, false, $default, [__CLASS__, "trimVal"]) : self::toArray($val, false, $default);
		return self::isEmpty($val) ? $default : $val;
	}
	public static function defaultObject($val, $default=null, $trim=true){
		return self::toObject(self::defaultArray($val, $default, $trim));
	}
	public static function hashstr($val, $salt="", $algorithm="md5"){
		$val = self::toStr($val, false, true);
		$salt = self::toStr($salt, false, true);
		$val = $salt . $val . $salt;
		$algorithm = strtolower(self::toStrx($algorithm, true));
		if (!in_array($algorithm, hash_algos())) $algorithm = "md5";
		return hash($algorithm, $val);
	}
	public static function getIP(){
		if (isset($_SERVER['HTTP_CLIENT_IP'])) return $_SERVER['HTTP_CLIENT_IP'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) return $_SERVER['HTTP_X_FORWARDED_FOR'];
        return $_SERVER['REMOTE_ADDR'];
	}
	public static function getUserAgent(){
		return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "Unknown useragent";
	}
	public static function getUserAgentDevice($useragent){
		if (self::isEmpty($useragent = self::toStrn($useragent, true))) return null;
		$platforms = [
			'/windows nt 10/i' => 'Windows 10',
			'/windows nt 6.3/i' => 'Windows 8.1',
			'/windows nt 6.2/i' => 'Windows 8',
			'/windows nt 6.1/i' => 'Windows 7',
			'/windows nt 6.0/i' => 'Windows Vista',
			'/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
			'/windows nt 5.1/i' => 'Windows XP',
			'/windows xp/i' => 'Windows XP',
			'/windows nt 5.0/i' => 'Windows 2000',
			'/windows me/i' => 'Windows ME',
			'/win98/i' => 'Windows 98',
			'/win95/i' => 'Windows 95',
			'/win16/i' => 'Windows 3.11',
			'/macintosh|mac os x/i' => 'Mac OS X',
			'/mac_powerpc/i' => 'Mac OS 9',
			'/linux/i' => 'Linux',
			'/ubuntu/i' => 'Ubuntu',
			'/iphone/i' => 'iPhone',
			'/ipod/i' => 'iPod',
			'/ipad/i' => 'iPad',
			'/android/i' => 'Android',
			'/blackberry/i' => 'BlackBerry',
			'/webos/i' => 'Mobile'
		];
		$browsers = [
			'/msie/i' => 'Internet Explorer',
			'/firefox/i' => 'Firefox',
			'/safari/i' => 'Safari',
			'/chrome/i' => 'Chrome',
			'/edge/i' => 'Edge',
			'/opera/i' => 'Opera',
			'/netscape/i' => 'Netscape',
			'/maxthon/i' => 'Maxthon',
			'/konqueror/i' => 'Konqueror',
			'/mobile/i' => 'Handheld Browser'
		];
		$platform = "Unknown Platform";
		foreach ($platforms as $regex => $value) if (self::regMatch($regex, $useragent)) $platform = $value;
		$browser = "Unknown Browser";
		foreach ($browsers as $regex => $value) if (self::regMatch($regex, $useragent)) $browser = $value;
		return ["platform" => $platform, "browser" => $browser, "useragent" => $useragent];
	}
	public static function output($val){
		static $output_header;
		if (!$output_header){
			$output_header = true;
			header("Content-Type: text/plain");
		}
		echo self::prints($val);
		ob_end_flush();
		ob_flush();
		flush();
		ob_start();
	}
	public static function minify($val){
		$keep = []; $x = 0;
		$val = self::strbreaks($val, "\r\n", true);
		$val = preg_replace_callback("/(?:(?:\"(?:\\\\\"|[^\"])+\")|(?:'(?:\\\'|[^'])+'))/is", function($matches) use (&$keep, &$x){
			$key = "!!~$x~!!"; $x ++;
			$match = $matches[0];
			$keep[$key] = $match;
			return $key;
		}, $val);
		$val = preg_replace('#\s*<!--(?!\[if\s).*?-->\s*|(?<!\>)\n+(?=\<[^!])#s', "", $val);
		$val = preg_replace('!^[ \t]*/\*.*?\*/[ \t]*[\r\n]!s', "", $val);
		$val = preg_replace('![ \t]*//.*[ \t]*[\r\n]!', "", $val);
		$val = str_replace(["\n", "\r"], "", $val);
		$val = preg_replace('!\s+!', " ", $val);
		foreach ($keep as $key => $value) $val = str_replace($key, $value, $val);
		return $val;
	}
	public static function compress($val){
		$pattern = '/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\')\/\/.*))/';
		$val = preg_replace($pattern, '', $val);
		$val = str_replace(array(PHP_EOL, "\t"), '', $val);
		$val = preg_replace('|\s\s+|', ' ', $val);
		return $val;
	}
	public static function base64Ext($base64_data){
		return explode(";", explode("/", $base64_data)[1])[0];
	}
	public static function isBinary($val){
		return preg_match('~[^\x20-\x7E\t\r\n]~', $val) > 0;
	}
	public static function isEven($num){
		return self::toNum($num) % 2 == 0;
	}
	public static function px2pt($px){
		//1 px = 0.75 point
		return self::toNum($px) * 0.75;
	}
	public static function pt2px($pt){
		//1 point = 1.333333 px
		return self::toNum($pt) * 1.333333;
	}
	public static function resizeRatio($old, $new){
		$old = self::toNum($old);
		$new = self::toNum($new);
		$diff = abs($new - $old);
		return $diff ? ($diff / $old) + ($new > $old ? 1 : 0) : 0;
	}
	public static function isBase64($val){
		$val = self::toStrx($val, true);
		if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $val)) return false;
		if (($decoded = base64_decode($val, true)) === false) return false;
		return base64_encode($decoded) === $val;
	}
	public static function getImage($img, $new_width=null, $new_height=null, $resize_ratio=null, $max_width=null, $max_height=null, $keep_aspect=true){
		$img = self::toStrx($img, true);
		if (!strlen($img)) return null;
		if (self::isBase64($img)){
			$img_parts = explode(",", $img, 2);
			if (count($img_parts) >= 2 && ($decoded = base64_decode($img_parts[1]))) $img = $decoded;
		}
		$image = null;
		$size_info = @getimagesize($img);
		if ($size_info === false){
			$size_info = @getimagesizefromstring($img);
			if (!$size_info) return false;
			$image = imagecreatefromstring($img);
		}
		else {
			if (!($img_data = @file_get_contents($img))) return false;
			$image = imagecreatefromstring($img_data);
		}
		list($width, $height, $type) = $size_info;
		$orig_width = $width;
		$orig_height = $height;
		if (!$image) return false;
		if ($new_width && !$new_height){
			$w = self::toNum($new_width);
			$h = $keep_aspect ? self::resizeRatio($width, $w) * $height : $height;
		}
		elseif ($new_height && !$new_width){
			$h = self::toNum($new_height);
			$w = $keep_aspect ? self::resizeRatio($height, $h) * $width : $width;
		}
		elseif ($resize_ratio && !$new_height && !$new_width){
			$resize_ratio = self::toNum($resize_ratio);
			$w = $resize_ratio * $width;
			$h = $resize_ratio * $height;
		}
		elseif ($new_width && $new_height){
			$w = self::toNum($new_width);
			$h = self::toNum($new_height);
		}
		$image_resized = isset($w) && isset($h);
		if ($image_resized){
			$width = $w;
			$height = $h;
		}
		if ($max_width && ($max_width = self::toNum($max_width)) < $width){
			$w = $max_width;
			$height = $keep_aspect ? self::resizeRatio($width, $w) * $height : $height;
			$width = $w;
			$image_resized = true;
		}
		if ($max_height && ($max_height = self::toNum($max_height)) < $height){
			$h = $max_height;
			$width = $keep_aspect ? self::resizeRatio($height, $h) * $width : $width;
			$height = $h;
			$image_resized = true;
		}
		if ($image_resized){
			if ($type === IMAGETYPE_PNG){
				$new_image = imagecreatetruecolor($width, $height);
				imagealphablending($new_image, false);
				imagesavealpha($new_image, true);
				imagecopyresampled($new_image, $image, 0, 0, 0, 0, $width, $width, $orig_width, $orig_height);
				$image = $new_image;
			}
			else $image = imagescale($image, $width, $height);
		}
		return $image;
	}
	public static function imageData($image, $func="imagejpeg"){
		if (!function_exists($func)) return false;
		ob_start();
		self::call($func, [$image]);
		$image_data = ob_get_contents();
		ob_end_clean();
		return $image_data;
	}
	public static function arrayRepeat($val, $limit_count){
		$val = self::toArray($val);
		$limit_count = (int) $limit_count;
		$temp = []; $x = 0; $n = count($val);
		if ($n){
			for ($i = 0; $i < $limit_count; $i ++){
				$temp[] = $val[$x];
				$x = ($x + 1) >= $n ? 0 : ($x + 1);
			}
		}
		return $temp;
	}
	public static function strplural($num, $text_single, $text_multiple){
		return abs(self::toNum($num)) == 1 ? $text_single : $text_multiple;
	}
	public static function getallheaders(){
		if (function_exists('getallheaders')) return getallheaders();
		$headers = [];
		foreach ($_SERVER as $name => $value){
			if (substr($name, 0, 5) == 'HTTP_') $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
		}
		return $headers;
	}
	//public static function temp(){}
}

namespace Naicode\Server\Funcs;
use Naicode\Server\Funcs as fn1;

if (!defined("NS_DEBUG_MODE")) define("NS_DEBUG_MODE", false);
if (!defined("NS_DEBUG_LEVEL")) define("NS_DEBUG_LEVEL", 2);
if (!defined("NS_DEBUG_LOG_FILE")) define("NS_DEBUG_LOG_FILE", "debug.log");
if (!defined("NS_DEBUG_OVERWRITE")) define("NS_DEBUG_OVERWRITE", false);

class Main {
	const STOP_MESSAGE = 0;
    const STOP_CONTINUE = 1;
    const STOP_CRITICAL = 2;
	const DEBUG_LEVEL_LOW = 0;
	const DEBUG_LEVEL_NORMAL = 1;
	const DEBUG_LEVEL_CRITICAL = 2;
	public static $debug_level = null;
	public static $debug_keep = false;
	public static $last_value = null;
	public static $last_error = "";
	public $error_msg = "";
	public $success_msg = "";
	public function __construct(){}
	public function error($msg, $return=false){
		$this -> error_msg = $msg;
		return $return;
	}
	public function success($msg, $return=true){
		$this -> success_msg = $msg;
		return $return;
	}
	public function debug($data, $name="DEFAULT", $level=0){
		if (!NS_DEBUG_MODE) return true;
		if (($level = fn1::toNum($level)) < NS_DEBUG_LEVEL) return false;
		if (fn1::isEmpty($data = fn1::trimVal($data))) return false;
		if (fn1::isEmpty($name = fn1::toStrn($name, true))) $name = "DEFAULT";
		$log_entry = fn1::strbuild("[%s (%s ~ %s)]: %s", fn1::now(), $level, $name, fn1::prints($data));
		$log_file = fn1::logFile(NS_DEBUG_LOG_FILE);
		return fn1::fileWrite($log_file, $log_entry, NS_DEBUG_OVERWRITE, true);
	}
	public function getErrorMessage(){
		return $this -> error_msg;
	}
	public function getSuccessMessage(){
		return $this -> success_msg;
	}
	public static function __error($msg, $return=false){
		self::$last_error = $msg;
		return $return;
	}
	public static function __success($msg, $return=true){
		self::$last_value = $msg;
		return $return;
	}
}

class NSDateTime extends \DateTime {
	public $format = "Y-m-d H:i:s";
	public function __construct($date=null, $format=null){
		parent::__construct();
		$this -> parse($date, $format);
	}
	public function setFormat($format){
		if (is_string($format) && strlen($format)) $this -> format = $format;
		return $this;
	}
	public function parse($date, $format=null){
		$this -> setFormat($format);
		if ($date instanceof NSDateTime || $date instanceof \DateTime) $this -> set($date);
		else {
			//$date = $date ? new \DateTime(date($this -> format, strtotime($date))) : new \DateTime();
			$date = $date ? \DateTime::createFromFormat($this -> format, $date) : new \DateTime();
			$this -> set($date);
		}
		return $this;
	}
	public function set($datetime){
		if ($datetime instanceof NSDateTime || $datetime instanceof \DateTime) $this -> setTimestamp($datetime -> getTimestamp());
		return $this;
	}
	public function addTime($days=0, $hours=0, $minutes=0, $seconds=0){
		$hours += $days * 24;
		$minutes += $hours * 60;
		$seconds += $minutes * 60;
		$timestamp = $this -> getTimestamp();
		$timestamp += (int) $seconds;
		$this -> setTimestamp($timestamp);
		return $this;
	}
	public function addMonths($months){
		list($y, $m, $d) = explode("-", $this -> format("Y-n-j"));
		$m += (int) $months;
		while ($m > 12){
			$m -= 12;
			$y ++;
		}
		$ld = date("t", strtotime("$y-$m-1"));
		if ($d > $ld) $d = $ld;
		$this -> setDate($y, $m, $d);
		return $this;
	}
	public function addYears($years){
		$yy = (int) $years;
		$yr = $years - $yy;
		if ($yr) $this -> addMonths($yr * 12);
		list($y, $m, $d) = explode("-", $this -> format("Y-n-j"));
		$y += $yy;
		$ld = date("t", strtotime("$y-$m-1"));
		if ($d > $ld) $d = $ld;
		$this -> setDate($y, $m, $d);
		return $this;
	}
	public function time_diff($date, $unit="D", $compare=false){
		$date = new self($date);
		$d = $this -> diff($date);
		$data = [
			"y" => (int) $d -> format("%y"),
			"m" => (int) $d -> format("%m"),
			"D" => (int) $d -> format("%a"),
			"d" => (int) $d -> format("%d"),
			"h" => (int) $d -> format("%h"),
			"i" => (int) $d -> format("%i"),
			"s" => (int) $d -> format("%s"),
		];
		if ($compare && $this -> getTimestamp() < $date -> getTimestamp()) foreach ($data as $key => $value) $data[$key] = - $value;
		if (array_key_exists($unit, $data)) return $data[$unit];
		else return $data;
	}
	public function copy(){
		$instance = new self();
		$instance -> set($this);
		return $instance;
	}
	public function toString($format=null){
		$this -> setFormat($format);
		return $this -> format($this -> format);
	}
}

class NSDOMDocument extends \DOMDocument {
	public function fromMixed($mixed, \DOMElement $domElement=null){
		$domElement = is_null($domElement) ? $this : $domElement;
		if (is_array($mixed)){
			foreach($mixed as $index => $mixedElement){
				if (is_int($index)){
					if ($index == 0) $node = $domElement;
					else {
						$node = $this -> createElement($domElement -> tagName);
						$domElement -> parentNode -> appendChild($node);
					}
				}
				else {
					$node = $this -> createElement($index);
					$domElement -> appendChild($node);
				}
				$this -> fromMixed($mixedElement, $node);
			}
		}
		else $domElement -> appendChild($this -> createTextNode($mixed));
	}
}
