<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190415
*	LANG
*/

namespace Naicode\Server;
require_once __DIR__ . "/funcs.php";
require_once __DIR__ . "/globals.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\Main;
use Naicode\Server\Funcs\Database\Resource;

class Lang {
	const LANGUAGES = ["eng"];
	public static $language = "eng";
	public static $config = [
		//Server
		"SERVER_ERROR_METHOD_SUPPORT" => ["eng" => "Request method \"%s\" is not one of the supported methods: %s"],
		"SERVER_ERROR_METHOD_MATCH" => ["eng" => "Request method \"%s\" does not match current request method: %s"],
		"SERVER_ERROR_INVALID_PARAMS" => ["eng" => "Invalid request listener params: %s"],
		"SERVER_ERROR_MISSING_PARAM" => ["eng" => "Required parameter \"%s\" not in current request parameters: %s"],
		"SERVER_ERROR_EXTRA_PARAM" => ["eng" => "Extra parameter \"%s\" in request parameters"],

		//Funcs
		"FUNCS_ERROR_JSON_PARSE" => ["eng" => "jsonParse: %s"],
		"FUNCS_ERROR_REGMATCH" => ["eng" => "regMatch: Error getting matched items"],
		"FUNCS_ERROR_REPLACEVAL" => ["eng" => "replaceVal: %s"],
		"FUNCS_ERROR_VALSPLIT" => ["eng" => "valSplit: %s"],
		"FUNCS_ERROR_VALJOIN" => ["eng" => "valJoin: Expected map to be an array"],
		"FUNCS_ERROR_OBJECTXML" => ["eng" => "objectXML (Exception): %s"],
		"FUNCS_ERROR_CREATE_FOLDER" => ["eng" => "Folder create failed: %s"],
		"FUNCS_ERROR_DELETE_FILE" => ["eng" => "File delete failed: %s"],
		"FUNCS_ERROR_DELETE_FOLDER_LIST" => ["eng" => "Folder delete failed! Unable to list contents: %s"],
		"FUNCS_ERROR_DELETE_FOLDER_EMPTY" => ["eng" => "Folder delete failed! Folder not empty: %s"],
		"FUNCS_ERROR_DELETE_FOLDER" => ["eng" => "Folder delete failed: %s"],
		"FUNCS_ERROR_DELETE_PATH" => ["eng" => "Unknown delete path type: %s"],
		"FUNCS_ERROR_FILE_WRITE" => ["eng" => "File Write Failed: %s"],
		"FUNCS_ERROR_FILE_WRITE_PATH" => ["eng" => "File Write Error! Path details failed: %s"],
		"FUNCS_ERROR_FILE_WRITE_FOLDER" => ["eng" => "File Write Error! Folder create failed (%s): %s"],
		"FUNCS_ERROR_INVALID_BASE64_DATA" => ["eng" => "Invalid base64 data"],

		//Mailer
		"MAILER_ATTACHMENT_GET_ERROR" => ["eng" => "Unable to get attachment path details: %s"],
		"MAILER_ATTACHMENT_MISSING_ERROR" => ["eng" => "Attachment file was not found or is unreadable for path: %s"],
		"MAILER_ATTACHMENT_SIZE_ERROR" => ["eng" => "Unable to read attachment file size for path: %s"],
		"MAILER_ATTACHMENT_MAX_SIZE_ERROR" => ["eng" => "Attachment file size (%d MBs) is greater than the allowed limit (%d MBs) for path: %s"],
		"MAILER_ATTACHMENT_NAME_MAX_SIZE_ERROR" => ["eng" => "Attachment size (%d MBs) is greater than the allowed limit (%d MBs) for name: %s"],
		"MAILER_ATTACHMENT_SIZE_ERROR" => ["eng" => "Unsupported mail address kind \"%s\""],
		"MAILER_EXCEPTION_FILE_OPEN" => ["eng" => "Unable to open file path: %s"],
		"MAILER_EXCEPTION_FILE_READ" => ["eng" => "Unable to read file path: %s"],
		"MAILER_ENCODING_UNKNOWN_ERROR" => ["eng" => "Unknown encoding: %s"],
		"MAILER_MIMEPRE" => ["eng" => "This is a multi-part message in MIME format."],
		"MAILER_EXCEPTION_MISSING_EXTENSION" => ["eng" => "Extension missing: %s"],
		"MAILER_EXCEPTION_SIGNING" => ["eng" => "Error signing message contents! %s"],
		"MAILER_NO_SENDER_ERROR" => ["eng" => "No sender address"],
		"MAILER_INVALID_SENDER_ERROR" => ["eng" => "Invalid sender address \"%s\""],
		"MAILER_X_HEADER_BUG_WARNING" => ["eng" => "Your version of PHP is affected by a bug that may result in corrupted messages. To fix it, switch to sending using SMTP, disable the mail.add_x_header option in  your php.ini, switch to MacOS or Linux, or upgrade your PHP to version 7.0.17+ or 7.1.3+."],
		"MAILER_EXCEPTION_NO_RECIPIENTS" => ["eng" => "No mail recipients"],
		"MAILER_EXCEPTION_NO_BODY" => ["eng" => "No message body"],
		"MAILER_EXCEPTION_PREPARE_BODY" => ["eng" => "Error preparing mail body"],
		"MAILER_EXCEPTION_PREPARE_HEADER" => ["eng" => "Error preparing mail header"],
		"MAILER_EXCEPTION_PREPARE_MAIL" => ["eng" => "Error preparing mail"],
		"MAILER_EXCEPTION_SENDMAIL_TO" => ["eng" => "Send Sendmail: No \"To\" recipients added"],
		"MAILER_EXCEPTION_SENDMAIL_EXECUTE" => ["eng" => "(%s) Failed to execute sendmail: %s"],
		"MAILER_SUCCESS_SENDMAIL" => ["eng" => "Sendmail sent!"],
		"MAILER_EXCEPTION_SMTP_TO" => ["eng" => "Send SMTP: No \"To\" recipients added"],
		"MAILER_SUCCESS_SMTP" => ["eng" => "SMTP Sent!"],
		"MAILER_EXCEPTION_MAIL_RECIPIENTS" => ["eng" => "Send Mail: No \"To\" recipients added"],
		"MAILER_EXCEPTION_MAIL_EXECUTE" => ["eng" => "Could not instantiate mail function"],
		"MAILER_SUCCESS_MAIL" => ["eng" => "Mail sent!"],

		//SMTP
		"SMTP_ERROR_COMMAND_CONNECTION" => ["eng" => "Called %s without connection"],
		"SMTP_ERROR_COMMAND_BREAKS" => ["eng" => "Command '%s' contained line breaks"],
		"SMTP_ERROR_COMMAND_FAILED" => ["eng" => "'%s' command failed!"],
		"SMTP_EXCEPTION_STARTTLS" => ["eng" => "STARTTLS Exception: %s"],
		"SMTP_EXCEPTION_AUTHENTICATION" => ["eng" => "Authentication Exception: %s"],
		"SMTP_ERROR_CONNECTION_FAILED" => ["eng" => "SMTP Connection failed!"],
		"SMTP_EXCEPTION_DATA" => ["eng" => "Data Exception: %s"],
		"SMTP_EXCEPTION_RECIPIENTS" => ["eng" => "Recipients Exception: %s"],
		"SMTP_ERROR_CONNECTION_FAILURE" => ["eng" => "Connection failed! %s (%s)"],
		"SMTP_ERROR_AUTHENTICATION_HELO" => ["eng" => "Authentication is not allowed before HELO/EHLO"],
		"SMTP_ERROR_AUTHENTICATION_STAGE" => ["eng" => "Authentication is not allowed at this stage"],
		"SMTP_ERROR_AUTHENTICATION_SUPPORT" => ["eng" => "No supported authentication methods found"],
		"SMTP_ERROR_AUTHENTICATION_NO_SUPPORT" => ["eng" => "The requested authentication method \"%s\" is not supported by the server"],
		"SMTP_ERROR_TURN" => ["eng" => "The SMTP TURN command is not implemented"],
		"SMTP_ERROR_NO_HELO" => ["eng" => "No HELO/EHLO was sent"],
		"SMTP_ERROR_HELO_HANDSHAKE" => ["eng" => "HELO handshake was used; No information about server extensions available"],

		//Config
		"CONFIG_ERROR_FILE_READ" => ["eng" => "Unable to read config file"],
		"CONFIG_ERROR_FILE_CORRUPT" => ["eng" => "Invalid or corrupt config file JSON data"],
		"CONFIG_ERROR_CURRENT_PATH" => ["eng" => "Invalid current config file write path: %s"],
		"CONFIG_ERROR_CURRENT_WRITE" => ["eng" => "Current config file write error: %s"],
		"CONFIG_ERROR_CURRENT_CORRUPT" => ["eng" => "Invalid or corrupt current config file JSON data: %s"],
		"CONFIG_ERROR_INVALID" => ["eng" => "Invalid or corrupt config data"],
		"CONFIG_ERROR_COLLECTION" => ["eng" => "Config collection \"%s\" is not configured"],
		"CONFIG_ERROR_COLLECTION_TABLE" => ["eng" => "Config collection \"%s\" is not properly configured"],
	];
	public static function get($key){
		$lang = self::$language;
		$key = fn1::valKey(self::$config, $key);
		if (!in_array($lang, self::LANGUAGES)) self::$language = ($lang = "eng");
		if (fn1::isEmpty($config = fn1::toArray(self::$config))) return "Lang config not defined!";
		if (!array_key_exists($key, $config)) return "Lang key \"$key\" does not exist!";
		if (fn1::isEmpty($item = fn1::toArray($config[$key]))) return "Lang key \"$key\" has not been configured!";
		if (!array_key_exists($lang, $item)) return "Lang key \"$key\" child \"$lang\" does not exist!";
		return fn1::toStrn(fn1::propval($item, $lang), true);
	}
	public static function str($key){
		return fn1::strbuild(self::get($key), array_slice(func_get_args(), 1));
	}
	public static function config($config){
		if (fn1::isEmpty($config = fn1::toArray($config))) return false;
		self::$config = fn1::arrayMerge(self::$config, $config);
		return true;
	}
}
