-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 01, 2021 at 09:38 AM
-- Server version: 10.2.21-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tupangec_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ns_audit`
--

CREATE TABLE `ns_audit` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `user` varchar(200) NOT NULL,
  `action` varchar(2000) NOT NULL,
  `identifier` varchar(2000) DEFAULT NULL,
  `edit_from` text DEFAULT NULL,
  `edit_to` text DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ns_audit`
--

INSERT INTO `ns_audit` (`_index`, `id`, `user`, `action`, `identifier`, `edit_from`, `edit_to`, `notes`, `timestamp`) VALUES
(1, '7D6ebInNiwvHdcScahibkaXzoBhKHvsH', 'SYSTEM', 'Create Account', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', '', '', 'provider: Twitter', '2019-05-16 00:23:20'),
(2, '8jzqaBRXi4aUo488dInGxaAPkmoVOZks', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', 'Update Account', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"DO15AhFjYIRm2MJ1Iy90Tt5OSBXRUmrAlFEvEhoVk2hITFCDOmkgrQFoMs90ewLZ\\\"}', 'provider: Twitter', '2019-05-16 00:23:20'),
(3, 'j2646W1Kqop9mW2sBKOkvPGBWn4xvkqh', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', 'Update Account', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', '{\\\"token\\\":\\\"DO15AhFjYIRm2MJ1Iy90Tt5OSBXRUmrAlFEvEhoVk2hITFCDOmkgrQFoMs90ewLZ\\\"}', '{\\\"token\\\":\\\"\\\"}', 'provider: Twitter', '2019-05-16 00:32:13'),
(4, 'NqWqwyBY3x0g61pRxo03hixbbLI3s7wQ', 'SYSTEM', 'Create Account', 'e8SeuiAue6ipWifjrlIqluK6H9qGzAOo', '', '', 'provider: Google', '2019-05-16 00:32:31'),
(5, 'PL6UgBBMM4YEpJRHn92aKIiuxbznNIIb', 'e8SeuiAue6ipWifjrlIqluK6H9qGzAOo', 'Update Account', 'e8SeuiAue6ipWifjrlIqluK6H9qGzAOo', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"Pj9O12Fa8RquP2sbz3v9mzv3reV3FeV1rcMNbmkmX1a9nvAf8SwGnp1qbKl1Flqz\\\"}', 'provider: Google', '2019-05-16 00:32:31'),
(6, 'OoX9KPCgde65S2MaxHor7whoil5pXqkZ', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', 'Update Account', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"jYBNFKoCN5GbOIAs64L3KrP8OHphjbRkGUSfKRJ8dNq3yNz5uiJMNaoOmaHveYKk\\\"}', 'provider: Twitter', '2019-05-16 01:15:38'),
(7, 'XmJV0upB6qD7AZDsH0Mn4Tsim4AVU5jN', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', 'Update Account', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', '{\\\"token\\\":\\\"jYBNFKoCN5GbOIAs64L3KrP8OHphjbRkGUSfKRJ8dNq3yNz5uiJMNaoOmaHveYKk\\\"}', '{\\\"token\\\":\\\"VXlg4Jcbk4WT7zfFO2XZ3NhTocveHGSSmpmyODMxoE8xhzOGMU1keFLSyCQdP97v\\\"}', 'provider: Twitter', '2019-05-17 20:54:14'),
(8, 'H4hagILgaBGU7c4IAN4SBILdWBzX3wQH', 'SYSTEM', 'Create Account', '9XwwU721LVHPfrfv4qGzh4Sh0ANw5lqu', '', '', 'provider: email', '2019-05-18 20:32:37'),
(9, '1J0ILhAfTOsoq0e9oWtdDxvj7DK0sY4T', '9XwwU721LVHPfrfv4qGzh4Sh0ANw5lqu', 'Update Account', '9XwwU721LVHPfrfv4qGzh4Sh0ANw5lqu', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"fHfw3Yj3ToYCDWINX1rbd1IdIGDNQlsvoZaz7j05Xk7hqlU6ghtu0fXnQmMJVYli\\\"}', 'provider: email', '2019-05-18 20:32:37'),
(10, 'fxJpXC6wRI8XCJm6skmqSQIhSSGtVhFl', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', 'Update Account', 'bHNyESOh3tMkVNAZBB1o3Qb4TCHjqQVB', '{\\\"token\\\":\\\"VXlg4Jcbk4WT7zfFO2XZ3NhTocveHGSSmpmyODMxoE8xhzOGMU1keFLSyCQdP97v\\\"}', '{\\\"token\\\":\\\"toMG2Ee2tpaBt0RuXqCLq80SWKDjb45rHFiz35X9xUiNhmXTSI3iAIrNsTx9OaQr\\\"}', 'provider: Twitter', '2019-05-20 16:07:35'),
(11, 'KW8jFUxKeOxvddkSjeUFFNOZcrCzEstv', 'SYSTEM', 'Create Account', 's8fhXVWkrlKw90UAFzh2LUu4vDiRNi7a', '', '', 'provider: Twitter', '2019-05-20 18:41:08'),
(12, '1XafT8dqUrUm5vVh5tJ75V06DyhQBGbD', 's8fhXVWkrlKw90UAFzh2LUu4vDiRNi7a', 'Update Account', 's8fhXVWkrlKw90UAFzh2LUu4vDiRNi7a', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"rUGmNVtdqDFtyMnLM5ePyLY9JLJW3liIzw42BGGouQhc7QAU8rY2yjRK3X9QUTDK\\\",\\\"token_ip\\\":\\\"197.177.39.124\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"444d88701954057978c7ad28c7483c62\\\"}', 'provider: Twitter', '2019-05-20 18:41:08'),
(13, 'ze4bYqiMLFqTb0Vk1965xrxEC7jHW62R', 'SYSTEM', 'Create Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '', '', 'provider: Google', '2019-05-20 18:46:12'),
(14, 'DEG76IENy0Xh2dfgyDnepx7eDTpBM8dh', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"UA7BuTpdkdNIoMd3yvMcW7DxKqFNGWHQJlG34uQXF6fqBtQJslUJzxx8cLvX5egL\\\",\\\"token_ip\\\":\\\"197.177.39.124\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"444d88701954057978c7ad28c7483c62\\\"}', 'provider: Google', '2019-05-20 18:46:12'),
(15, '1BvkuPv8mf2VGO7bgTRqg1NXqvVBi7aX', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"email_verified\\\":0,\\\"verification_code\\\":\\\"ptH8Z3Rum3Pk5F4zwUGVacg4lkLDBYoU\\\"}', '{\\\"email_verified\\\":1,\\\"verification_code\\\":\\\"\\\"}', 'provider: Google', '2019-05-20 18:47:16'),
(16, 'UPSvKJcPD5TQHFovyObY96alhh0Ry95n', 'SYSTEM', 'Create Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '', '', 'provider: Google', '2019-05-20 23:44:25'),
(17, 'vtLGdWPYoJKxr84T8i8Llt5gUeH4OFeU', 'SYSTEM', 'Create Account', 'QBmZmxDImVGt5T6zIIO85vlcPEgjfVQI', '', '', 'provider: Google', '2019-05-20 23:44:43'),
(18, 'bxRdsof4wZ2Ks7iJxemKv9DNy69oS63Y', 'QBmZmxDImVGt5T6zIIO85vlcPEgjfVQI', 'Update Account', 'QBmZmxDImVGt5T6zIIO85vlcPEgjfVQI', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"4sNp3dBkmUqE9Y3FuVnN36koRqj1QLbqI4A6LYOyoOlxUTvH0QSN5zDkIpAg2bcn\\\",\\\"token_ip\\\":\\\"66.249.93.55\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b73152e26ede07da2388685fb3150dce\\\"}', 'provider: Google', '2019-05-20 23:44:43'),
(19, 'IxKRZBTfDX8iwcyubA9Ss5n1SAGaK1EC', 'SYSTEM', 'Create Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '', '', 'provider: Google', '2019-05-21 08:18:30'),
(20, 'MNqXcUzCCsmI5O9oiPK2eKtVl5Kzsyha', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"7krOaUARYob3qbQC1CNOsex6aDvHLUnTFo5tzR4gIRbIk1fvMwRQBrvg3yTc0fHt\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SAMSUNG SM-N960F Build\\\\/PPR1.180610.011) AppleWebKit\\\\/537.36 (KHTML, like Gecko) SamsungBrowser\\\\/9.2 Chrome\\\\/67.0.3396.87 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b407651492fca5a3367c171302137d79\\\"}', 'provider: Google', '2019-05-21 08:18:30'),
(21, 'kMaonXzubMfWFaopkN2ynQHKnEoeqYdB', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"email_verified\\\":0,\\\"verification_code\\\":\\\"L25tmsSTo8CI3zBuQ7D5X38eVerOzJ2M\\\"}', '{\\\"email_verified\\\":1,\\\"verification_code\\\":\\\"\\\"}', 'provider: Google', '2019-05-21 08:19:35'),
(22, '1boCNRY6ZUA1dB3LPKvN66q9w5HKuUJ2', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"email_verified\\\":0,\\\"verification_code\\\":\\\"ejPaenWjgfh61N13cTdewgKC6y7Jhv21\\\"}', '{\\\"email_verified\\\":1,\\\"verification_code\\\":\\\"\\\"}', 'provider: Google', '2019-05-21 09:07:26'),
(23, 'BiTXxqM62UQmDhRWwU9hu360SoJ9XqeQ', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"grRrccxS9pra85cG4MZl4ZhNXySBlJOTEeOEyB9Txo1YhZeYM83ZQzntfSweu6oH\\\",\\\"token_ip\\\":\\\"197.237.128.235\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.131 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7237c7b579e1e8895181621f7b8ae255\\\"}', 'provider: Google', '2019-05-21 18:58:33'),
(24, 'sdoIJAnEUemreDq9mszqoGCKX5aSLvZw', 'SYSTEM', 'Create Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '', '', 'provider: Google', '2019-05-21 19:00:59'),
(25, '4sF7Ko2CCK87sH7WUh8wWSvmvQz0RTx6', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"Mo2HqSNj2DoOUVq9zwJKJDZxwmnD3hAqSgOQtkiF7J8Z741srkIUCUM8yNWbYnKN\\\",\\\"token_ip\\\":\\\"66.249.93.53\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.0.0; SM-A720F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/70.0.3538.110 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"819c54c1221944c1617f12a12340eae4\\\"}', 'provider: Google', '2019-05-21 19:02:02'),
(26, 'Wc757NYRNxmLCq2dtlFDspeojKCyWOtB', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"66.249.93.53\\\",\\\"token_fingerprint\\\":\\\"819c54c1221944c1617f12a12340eae4\\\"}', '{\\\"token\\\":\\\"Skecg0RdmifPWIiknblygcz10yM01IiM1BQ1RVX3PBymliOurxmv5YNSPnnV8iOX\\\",\\\"token_ip\\\":\\\"66.249.93.55\\\",\\\"token_fingerprint\\\":\\\"d12210e344031f395d1d7f0267f6d97f\\\"}', 'provider: Google', '2019-05-21 19:04:50'),
(27, '9X74QMQC6VEwa4NFIsFRhAkN4vbePBXL', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"j67hhAmc6hvBr8O44R1lmhFvUYP17QkUo6qFo8EKnjOlX1TJhyeRamvC5nWRJOXd\\\"}', 'provider: Google', '2019-05-21 19:11:08'),
(28, 'PwQpRNCJpix4PIa4IivaRTgqqfMfVt9Y', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"qZQ14A1CVMaGabPJTwUrGDYSUlFN2KmQ0tZDRoQzk0ADF86f18Pzs2Rde7SrBdTs\\\"}', 'provider: Google', '2019-05-21 19:11:08'),
(29, '17ib67VOwn8W7NsJcI2ATdgwccCeiF53', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"66.249.93.55\\\",\\\"token_fingerprint\\\":\\\"d12210e344031f395d1d7f0267f6d97f\\\"}', '{\\\"token\\\":\\\"kmX89uMoXfAmrDOF7UhgyJazxYD2M4H5wPbnA8PqONwG2lIisEt3Is52YdJw1wgz\\\",\\\"token_ip\\\":\\\"66.249.93.57\\\",\\\"token_fingerprint\\\":\\\"767e80ac840f25779f0fec755cb39da7\\\"}', 'provider: Google', '2019-05-21 19:12:01'),
(30, 'VRtco62R2osWmKiHBewX9ulYE6BRcOe1', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"7krOaUARYob3qbQC1CNOsex6aDvHLUnTFo5tzR4gIRbIk1fvMwRQBrvg3yTc0fHt\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SAMSUNG SM-N960F Build\\\\/PPR1.180610.011) AppleWebKit\\\\/537.36 (KHTML, like Gecko) SamsungBrowser\\\\/9.2 Chrome\\\\/67.0.3396.87 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b407651492fca5a3367c171302137d79\\\"}', '{\\\"token\\\":\\\"rwmze2aT40RRQ0WtmYgQRQUcKCGP7ZP8NaC3n7iI23rKHmq5oknv084XBMo6yu9m\\\",\\\"token_ip\\\":\\\"66.249.93.55\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"2fe939c08b086441df2f9b6d8edeb72c\\\"}', 'provider: Google', '2019-05-21 19:14:34'),
(31, 'K1Zlp280IynjGZCymnhtPi78Jgby1lRE', 's8fhXVWkrlKw90UAFzh2LUu4vDiRNi7a', 'Update Account', 's8fhXVWkrlKw90UAFzh2LUu4vDiRNi7a', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"NQ0SZ9jBU0f2JYrLhk2UOIdOZT3MYcuX1kW3KHY6cs0iuVdCmJvL1OMn9apxJLZx\\\"}', 'provider: Twitter', '2019-05-21 20:44:03'),
(32, 'OyzBJdpplXApcqEOX3WgZOq8DNPE3gMp', 'SYSTEM', 'Create Account', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', '', '', 'provider: Google', '2019-05-28 19:53:46'),
(33, 'uj4SVf9rIXqW07OC7kxe3bTYaAcKGZQO', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"reset_code\\\":null}', '{\\\"reset_code\\\":\\\"iCkQjObzgZ8wsleXgUPJQUxWNTzrYnQv\\\"}', 'provider: Google', '2019-05-29 12:22:56'),
(34, 'NJdCebzxxokYiPrFoLqcOTz8yD4RVCTo', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"hash\\\":\\\"\\\",\\\"reset_code\\\":\\\"iCkQjObzgZ8wsleXgUPJQUxWNTzrYnQv\\\"}', '{\\\"hash\\\":\\\"b435fb8b7673b505803691d30b6ddeb8\\\",\\\"reset_code\\\":\\\"\\\"}', 'provider: Google', '2019-05-29 12:23:22'),
(35, '0K7wv1QlnA9KyBudymo0uLGG5Hn52Vjv', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.177.39.124\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"444d88701954057978c7ad28c7483c62\\\"}', '{\\\"token\\\":\\\"qGkum6lIObv4RAWlNp278BOWE2JgibGVJFCj4WWWUtTR9JNNF9KtiBV23nhZ4Rw3\\\",\\\"token_ip\\\":\\\"197.182.58.16\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.169 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"634b20e094c529d7994f67805c2de336\\\"}', 'provider: Google', '2019-05-29 12:23:28'),
(36, 'eZURDl4gsHGffPvQjtoG6ULRUsBm9WkE', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"reset_code\\\":\\\"\\\"}', '{\\\"reset_code\\\":\\\"plAPaWJ9sx236Cda5jQHr9PrzMX15Zfg\\\"}', 'provider: Google', '2019-05-29 12:26:04'),
(37, '8H36TCLsdE9Oq9jFC8PjHgPGsHBWzMiR', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"MmMePf77FBKa2fnoN8JcS4izFNry93VZmOigj8N42uD86ibHQk1jQGAj4PJeXzLo\\\"}', 'provider: Google', '2019-05-29 12:26:24'),
(38, 'qRmWHGWdA2y2nFQaZBh3unBHKQmSPTCV', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"MmMePf77FBKa2fnoN8JcS4izFNry93VZmOigj8N42uD86ibHQk1jQGAj4PJeXzLo\\\"}', '{\\\"token\\\":\\\"zAmpTgfu5Smuiz9cttKqDI2O0Vfh0fwN7oUDiLpilqNLUeOZv9GynMDM0A81fO8g\\\"}', 'provider: Google', '2019-05-29 15:23:59'),
(39, '5c3rOlJNTndbQfV5HCx7ZghhSAB0Skc8', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"zAmpTgfu5Smuiz9cttKqDI2O0Vfh0fwN7oUDiLpilqNLUeOZv9GynMDM0A81fO8g\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.169 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"634b20e094c529d7994f67805c2de336\\\"}', '{\\\"token\\\":\\\"NolVFzOfs0cwdhaAaVJsWzRq15uDe3oL76O2CcG5tvTO6N2C1TrVLxQgnAoIr9RF\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"fe5dcb0642cd4a2d2fa6edd40df62544\\\"}', 'provider: Google', '2019-05-29 15:29:56'),
(40, 'fizGScuGyllBQKXcLub2fOBv8stIQkxi', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"NolVFzOfs0cwdhaAaVJsWzRq15uDe3oL76O2CcG5tvTO6N2C1TrVLxQgnAoIr9RF\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"fe5dcb0642cd4a2d2fa6edd40df62544\\\"}', '{\\\"token\\\":\\\"IDxTFxcp5VzKjbu44TtrsVcPwFWcFJ4yKpaGC2DGN8Bypcu0FmriRDsvomqE75TU\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.169 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"634b20e094c529d7994f67805c2de336\\\"}', 'provider: Google', '2019-05-29 20:28:59'),
(41, 'vJPiKfGSBBlkcaCixCCDl5wePfzYKadd', 'SYSTEM', 'Create Account', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', '', '', 'provider: Google', '2019-05-29 20:31:00'),
(42, 'ZiZuvwbzv8A2ETDcvEYH0GckKxo0eZHY', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'Update Account', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', '{\\\"status\\\":\\\"0\\\"}', '{\\\"status\\\":1}', 'provider: Google', '2019-05-29 20:32:34'),
(43, 't6Aq7iqovbO5S0rC9wTozZ2PHOWNhuwn', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"RwaYx2eyKvnwHuBPGXNrqfZg4fNYt8dlzif1VMFBXjL9YFsOHcZ2jpzbUzoxXRWz\\\"}', 'provider: Google', '2019-05-29 21:15:42'),
(44, 'CUN6r5WCJsqa5bO4DPn5iWEo52K9NIhU', 'SYSTEM', 'Create Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '', '', 'provider: Google', '2019-05-29 21:18:55'),
(45, 'PCi3VFaKQyiaj9YMLSVbjuhHkhDR5azT', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"status\\\":\\\"0\\\"}', '{\\\"status\\\":1}', 'provider: Google', '2019-05-29 21:20:44'),
(46, 'Etjja4jDY1yOddb5lWYPzCzOTCnr7MV4', 'SYSTEM', 'Create Account', 'iT3fjPuOfeJ4ett00tsn5BjqYN925kOd', '', '', 'provider: Google', '2019-05-29 21:51:19'),
(47, 'qeM1OpSbJ3IlM3Ax4NjJnARdA9GtgJlO', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.182.58.16\\\",\\\"token_fingerprint\\\":\\\"634b20e094c529d7994f67805c2de336\\\"}', '{\\\"token\\\":\\\"POemXrWwXPZuSEU0cHXTGFX34uSQ62y3aypoKYKkyMCgzfMY5XrvFPwJJCSC47N6\\\",\\\"token_ip\\\":\\\"105.55.220.225\\\",\\\"token_fingerprint\\\":\\\"9ca6de59918b6fa4f27ef97282e69740\\\"}', 'provider: Google', '2019-05-30 22:21:11'),
(48, 'g8LKcooaoZOFTBaSdYHHEAomYvfQ8QyV', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"105.55.220.225\\\",\\\"token_fingerprint\\\":\\\"9ca6de59918b6fa4f27ef97282e69740\\\"}', '{\\\"token\\\":\\\"DViImZZtMiqqxuIwR1M70VvRBP1wpd06VYssYCjNeWWIiKAMAecVxhLF5akq1nHG\\\",\\\"token_ip\\\":\\\"196.111.114.155\\\",\\\"token_fingerprint\\\":\\\"3e654acd81397f63820424c4e967914b\\\"}', 'provider: Google', '2019-06-01 03:19:54'),
(49, 'ojj3ei0hjcH5Z9JPMGkG5dIzWkcqFO4T', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.169 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"3e654acd81397f63820424c4e967914b\\\"}', '{\\\"token\\\":\\\"V11WI5qTPHBx2UxZPi3CyIjs6MTL9x6hE1ikFlOuFh7Huog6IzG7sIeGP3BpyZJM\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"10d5b4d2d177989b8a391a4491d5a098\\\"}', 'provider: Google', '2019-06-01 11:12:30'),
(50, 'hhuRy65pmkuIOsdmWuGlZ4s6llUYnjEG', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"10d5b4d2d177989b8a391a4491d5a098\\\"}', '{\\\"token\\\":\\\"JxUpZZd8ooLwWK4wtDLlleINJrsAXGw060vQwWJh8p5WCGXHb1vWIHOxlvRytKHQ\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.169 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"3e654acd81397f63820424c4e967914b\\\"}', 'provider: Google', '2019-06-01 12:19:42'),
(51, 'ndwP2YrSh1Cn0keNRXlAEARGxKjV3SNx', 'SYSTEM', 'Create Account', 'fAEryS4d8g6uIgdYpF6LSPTWeSIwossh', '', '', 'provider: Google', '2019-06-02 13:12:28'),
(52, 'eCKTaJKcUhfy8kvpNag5tyAhll3OPoIE', 'fAEryS4d8g6uIgdYpF6LSPTWeSIwossh', 'Update Account', 'fAEryS4d8g6uIgdYpF6LSPTWeSIwossh', '{\\\"status\\\":\\\"0\\\"}', '{\\\"status\\\":1}', 'provider: Google', '2019-06-02 13:12:50'),
(53, 'HRE2nTVBcmCfUvHPY0V6tgtls8hgUROm', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"status\\\":\\\"0\\\"}', '{\\\"status\\\":1}', 'provider: Google', '2019-06-03 09:33:51'),
(54, 'pL63aJbwdoYk9VVnZIeAGkeAzBLGGCgl', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"196.111.114.155\\\",\\\"token_fingerprint\\\":\\\"3e654acd81397f63820424c4e967914b\\\"}', '{\\\"token\\\":\\\"dooX5igtcYxseSN0zQalpgp7cB1pXCgyjoGdIluDrWYANLtl1SPJhkQAOfUiAwn5\\\",\\\"token_ip\\\":\\\"105.50.198.55\\\",\\\"token_fingerprint\\\":\\\"e6d47647f0997ea2b5c47b8ca07c5c88\\\"}', 'provider: Google', '2019-06-03 12:28:31'),
(55, 'g9mLI2GKUwA0FjIBzMOplRpOmkY3yUt8', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"105.50.198.55\\\",\\\"token_fingerprint\\\":\\\"e6d47647f0997ea2b5c47b8ca07c5c88\\\"}', '{\\\"token\\\":\\\"o0vhPrKDVSz4FIWzvQIbLo5ShCIvvQ22JN4eIuARZBq7uXntNEbsWpoEoa82rzZa\\\",\\\"token_ip\\\":\\\"41.80.200.113\\\",\\\"token_fingerprint\\\":\\\"8c296af851e62c52f12b25fd70e3fd14\\\"}', 'provider: Google', '2019-06-04 15:00:02'),
(56, 'bS6DMyLRLmAaANfUDpOQd9e0fBuoBIDa', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"o0vhPrKDVSz4FIWzvQIbLo5ShCIvvQ22JN4eIuARZBq7uXntNEbsWpoEoa82rzZa\\\",\\\"token_ip\\\":\\\"41.80.200.113\\\",\\\"token_fingerprint\\\":\\\"8c296af851e62c52f12b25fd70e3fd14\\\"}', '{\\\"token\\\":\\\"TdPYGs07BZupcMB4db7J6oz2IoH21fgaLhINX3hfxsYHL5blXOtpj0FkPXeBd3Xu\\\",\\\"token_ip\\\":\\\"197.237.128.235\\\",\\\"token_fingerprint\\\":\\\"a553842e9b60a965c36e355eab1e7257\\\"}', 'provider: Google', '2019-06-04 17:02:07'),
(57, 'BuPz4nYVPXcK7GnxoO0QDDZutxkF34PN', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"status\\\":\\\"0\\\"}', '{\\\"status\\\":1}', 'provider: Google', '2019-06-04 19:09:20'),
(58, 'ehhVTcrKyUOt5WxHCjBRlL7TqoUNIHnb', 'SYSTEM', 'Create Account', '7hS75ihbUhrswdWJzCsS7aMxdT34Td5U', '', '', 'provider: Twitter', '2019-06-04 20:05:24'),
(59, 'hjqLlEI4BEEEhpkq804nrROz9MyEuDxD', 'SYSTEM', 'Create Account', 'cEY2HYoVHiZQ5e3t1QLdaiITppTfZdq1', '', '', 'provider: email', '2019-06-04 20:05:41'),
(60, 'Z69B8T84hV49A691uo3Lbe2ALWVIhcN7', 'SYSTEM', 'Create Account', 'wuGzBFEBkiU99CPqAXeZwY5g1j8vw0Nk', '', '', 'provider: Twitter', '2019-06-04 20:05:53'),
(61, 'L9cws2qTnHcPHUlPNbZN4JlrHtYWHjoh', 'cEY2HYoVHiZQ5e3t1QLdaiITppTfZdq1', 'Update Account', 'cEY2HYoVHiZQ5e3t1QLdaiITppTfZdq1', '{\\\"status\\\":\\\"0\\\"}', '{\\\"status\\\":1}', 'provider: email', '2019-06-04 20:06:30'),
(62, 'Rps1Ug4WqSxdxYZqKCrwXutbqydRMwUQ', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.237.128.235\\\",\\\"token_fingerprint\\\":\\\"a553842e9b60a965c36e355eab1e7257\\\"}', '{\\\"token\\\":\\\"AXYbBbI6tTmw8CIWrr27wPvFPNaEkGCOEeQBPxcZbjW2wN9rYXFldvWtZ5sjuJuv\\\",\\\"token_ip\\\":\\\"105.165.75.247\\\",\\\"token_fingerprint\\\":\\\"c1dd73721c292bf0d443dd295943bd76\\\"}', 'provider: Google', '2019-06-06 11:49:44'),
(63, 'HuUhvW2wyj7LaBqvNdQPtCrkLcX69ejn', 'SYSTEM', 'Create Account', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', '', '', 'provider: email', '2019-06-06 14:56:58'),
(64, 'B8hZKAB8aZuZ3d6SrK0VYd0e3fVrnoit', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', 'Update Account', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', '{\\\"email_verified\\\":0,\\\"verification_code\\\":\\\"hbpVuUqObhxM62nNk3YIsRFpEdwHKKec\\\"}', '{\\\"email_verified\\\":1,\\\"verification_code\\\":\\\"\\\"}', 'provider: email', '2019-06-06 15:12:16'),
(65, 'aShy7f3ILXrXoA5ZLiAseE7xgbld28ay', 'SYSTEM', 'Create Account', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', '', '', 'provider: email', '2019-06-07 20:56:05'),
(66, 'SECHV7LHycXKp5EREsrKtYLgIzENxl84', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', 'Update Account', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', '{\\\"status\\\":\\\"0\\\"}', '{\\\"status\\\":1}', 'provider: email', '2019-06-07 21:00:22'),
(67, '4tm5L9L8FmAyMByFWbbIUNsRsBEgROUx', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"105.165.75.247\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.169 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"c1dd73721c292bf0d443dd295943bd76\\\"}', '{\\\"token\\\":\\\"LNxmfBjJQfOuLzhxJCzQqwTDhlgxPn7ZXwhWZvCelmSIssel4qjprtHJ9UuKN2Ed\\\",\\\"token_ip\\\":\\\"105.56.210.238\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"4b1c5e04b5cb65910686ff57c84bf79a\\\"}', 'provider: Google', '2019-06-08 20:26:35'),
(68, 'Sb11MuOa0LdUxNq2koTxJU9YH5kWFMSX', 'SYSTEM', 'Create Account', 'Yf4ccb5vyWplQy2UqpnCNXTeNcxSLaT2', '', '', 'provider: email', '2019-06-08 20:33:40'),
(69, 'KTKUtmTKAeTFON5GILSXbBRHReY5HrJd', 'Yf4ccb5vyWplQy2UqpnCNXTeNcxSLaT2', 'Update Account', 'Yf4ccb5vyWplQy2UqpnCNXTeNcxSLaT2', '{\\\"status\\\":\\\"0\\\"}', '{\\\"status\\\":1}', 'provider: email', '2019-06-08 20:34:50'),
(70, 'YtoJ1NdlUuxFWLaveVyayuPrhM5ungE5', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"105.56.210.238\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"4b1c5e04b5cb65910686ff57c84bf79a\\\"}', '{\\\"token\\\":\\\"XdvxQqjHEFi4u8FMQRTMd64dkC3KOE2JQkP00XmETjuUJ207liOgXHJKGvcqxdPW\\\",\\\"token_ip\\\":\\\"105.57.166.129\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.169 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b4afa0de2ed1a853ff1f208c60378b2a\\\"}', 'provider: Google', '2019-06-11 01:29:51'),
(71, 'PIKOy5MtUwr2DoKsePyoEyGwIJYyxJHZ', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', 'Update Account', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"6fzw8TTet7uZXzDaCuzRSn17l1kdZwK3KhXiTJb5i58EUJfNLbPpFz0eeXNDyhme\\\",\\\"token_ip\\\":\\\"197.237.128.235\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.169 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7a0247d314ab998251e5426d97569398\\\"}', 'provider: email', '2019-06-11 18:20:12'),
(72, 'FD0HScjsnB7rALxeQAPcAFkYVYoqrfMB', 'SYSTEM', 'Create Account', 'gnjfc5n1Q91Pjs5k8KgrbELhOgp88PKF', '', '', 'provider: Google', '2019-06-11 18:26:50'),
(73, 'XxvHlUsjjnHsDz9ghyheAHTRQquyelVX', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"XdvxQqjHEFi4u8FMQRTMd64dkC3KOE2JQkP00XmETjuUJ207liOgXHJKGvcqxdPW\\\",\\\"token_ip\\\":\\\"105.57.166.129\\\",\\\"token_fingerprint\\\":\\\"b4afa0de2ed1a853ff1f208c60378b2a\\\"}', '{\\\"token\\\":\\\"kgrQ0lKOIZNYhZDIb6BbBGOd7orqZxAKcGmwV1ARUyWqnwPgjt13BiP3NECuEnRt\\\",\\\"token_ip\\\":\\\"105.164.242.66\\\",\\\"token_fingerprint\\\":\\\"cb995a456cd6921bede527ca4411f5c4\\\"}', 'provider: Google', '2019-06-13 01:09:36'),
(74, 'k4uH3750oIqjhrWhvAHn7Zg1i50NfnKE', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"kgrQ0lKOIZNYhZDIb6BbBGOd7orqZxAKcGmwV1ARUyWqnwPgjt13BiP3NECuEnRt\\\"}', '{\\\"token\\\":\\\"HNvJJ2yBTo2FPqaSgAGRqXaIj226bORO2VkbOHVTGf3vYSoNtcwOWI5OOXZwgMb0\\\"}', 'provider: Google', '2019-06-13 02:44:46'),
(75, 'RtK1TXI1sSVUhTrNnyjmaLCO3VG0q9P8', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"HNvJJ2yBTo2FPqaSgAGRqXaIj226bORO2VkbOHVTGf3vYSoNtcwOWI5OOXZwgMb0\\\",\\\"token_ip\\\":\\\"105.164.242.66\\\",\\\"token_fingerprint\\\":\\\"cb995a456cd6921bede527ca4411f5c4\\\"}', '{\\\"token\\\":\\\"wqcGXHL0hOav1aqGsvHps331jjwwsRc8Qxo0mI2kA3nybDbKo2DVCIXBH5P7N7oa\\\",\\\"token_ip\\\":\\\"196.104.87.101\\\",\\\"token_fingerprint\\\":\\\"714d4773333e91642cdcd79f1a55721b\\\"}', 'provider: Google', '2019-06-14 13:35:35'),
(76, 'LEHfZ1N7sfc7iePqUjdaJ2O7VG52mo4U', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"wqcGXHL0hOav1aqGsvHps331jjwwsRc8Qxo0mI2kA3nybDbKo2DVCIXBH5P7N7oa\\\",\\\"token_ip\\\":\\\"196.104.87.101\\\",\\\"token_fingerprint\\\":\\\"714d4773333e91642cdcd79f1a55721b\\\"}', '{\\\"token\\\":\\\"hjFsB3JBheuQJ2ZI4yz2GYjJ5evHu5yRmknyDGdMVqjDFesM4GV0fsp5XSUfqc2A\\\",\\\"token_ip\\\":\\\"197.178.184.102\\\",\\\"token_fingerprint\\\":\\\"65792bc306e1c407c08c3e27ae69a2e5\\\"}', 'provider: Google', '2019-06-16 14:40:39'),
(77, '4K222p3TQ3bAeXQJhnERGh52MH7glZWJ', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"hjFsB3JBheuQJ2ZI4yz2GYjJ5evHu5yRmknyDGdMVqjDFesM4GV0fsp5XSUfqc2A\\\"}', '{\\\"token\\\":\\\"9KTX9QYZgI4eNtxEvO7AcmsNaIqB43cv86if3vAeos5t4FWrDczvZm4KRUwgGUYj\\\"}', 'provider: Google', '2019-06-17 01:26:27'),
(78, 'zhUoHTfkPKayUtxCPbSbxcIzfrg7ldt8', 'Yf4ccb5vyWplQy2UqpnCNXTeNcxSLaT2', 'Update Account', 'Yf4ccb5vyWplQy2UqpnCNXTeNcxSLaT2', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"ijcRpYfubEX6XtjlI0G6zQjWzeybBdOPygtMuSKcMlFCDzqXsVWNAkSgUAJdj1l9\\\",\\\"token_ip\\\":\\\"105.57.55.232\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"8306bcc97929f1899eee7b8a15b4e9fb\\\"}', 'provider: email', '2019-06-17 18:21:12'),
(79, 'rO7C8jhIXA9XfucxW6rasHyFDjuy4ueE', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', 'Update Account', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.237.128.235\\\",\\\"token_fingerprint\\\":\\\"7a0247d314ab998251e5426d97569398\\\"}', '{\\\"token\\\":\\\"M0ZUZT7umWIefOJKFeFNEY0ktJCFnDCL9tDpkkZYsbQ5yuKdFYuxF4IgzX9dQE4f\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_fingerprint\\\":\\\"98442b738359f2f17b7d202b881a685b\\\"}', 'provider: email', '2019-06-18 09:39:19'),
(80, '9uuuBRcinGCQOd2GLZxCQMs23URw270H', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.178.184.102\\\",\\\"token_fingerprint\\\":\\\"65792bc306e1c407c08c3e27ae69a2e5\\\"}', '{\\\"token\\\":\\\"zgNSMPM3HmGJRST13gRNkPYLdmgY7ekOoEqwGFp8FRZtfHhKHZUkT1PXanlqv5sW\\\",\\\"token_ip\\\":\\\"105.53.24.246\\\",\\\"token_fingerprint\\\":\\\"d69d53756f24c75067fdb1fd2bef8770\\\"}', 'provider: Google', '2019-06-18 13:38:38'),
(81, 'erEPVt9FniNhMG7gD88xP8veRxPFXXJi', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"105.53.24.246\\\",\\\"token_fingerprint\\\":\\\"d69d53756f24c75067fdb1fd2bef8770\\\"}', '{\\\"token\\\":\\\"owWuOvblEWIbnI7ro7HbeRohSLW6dYfrFcxttkNDQGmFiJkwcQQ0kT70OXkPQR8W\\\",\\\"token_ip\\\":\\\"105.51.85.141\\\",\\\"token_fingerprint\\\":\\\"600a043f0387c1242fe5869cc12f54fc\\\"}', 'provider: Google', '2019-06-19 01:19:34'),
(82, '6UCowm11fCWjrNUFvzpM9vv7VzBZD5oL', 'Yf4ccb5vyWplQy2UqpnCNXTeNcxSLaT2', 'Update Account', 'Yf4ccb5vyWplQy2UqpnCNXTeNcxSLaT2', '{\\\"token\\\":\\\"ijcRpYfubEX6XtjlI0G6zQjWzeybBdOPygtMuSKcMlFCDzqXsVWNAkSgUAJdj1l9\\\",\\\"token_ip\\\":\\\"105.57.55.232\\\",\\\"token_fingerprint\\\":\\\"8306bcc97929f1899eee7b8a15b4e9fb\\\"}', '{\\\"token\\\":\\\"t0NZkutxG3GIuLbomgSp2rb3X3kl6a65bFkL2LgpvCjO46f8EZSTaM2OvpynbEX1\\\",\\\"token_ip\\\":\\\"105.51.85.141\\\",\\\"token_fingerprint\\\":\\\"5bcddfe4bda4ec775b7c89a685bcdf58\\\"}', 'provider: email', '2019-06-19 01:56:15'),
(83, 'C0UK6FdAGRCVB7KuyqTdFZmDQ1XuHYAb', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"105.51.85.141\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.169 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"600a043f0387c1242fe5869cc12f54fc\\\"}', '{\\\"token\\\":\\\"cWm3SBX2G7NA5hdbOkfC5gU0P8jRT3Jlg5640fPVeAPhCbToCTQodaWhWzMi4rjz\\\",\\\"token_ip\\\":\\\"197.180.163.87\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b5b5fadbaafdd86fe3a42d2abaaebf1d\\\"}', 'provider: Google', '2019-06-22 14:38:52'),
(84, 'UfY91Czxd57iTZW3NBEYi7TF19liC1yj', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"ABxW4WqR0UxsQwkalVMifNyOrFtwCke6OkYiKrGED7M0W17PUlggXUMjdsGPZuNN\\\"}', 'provider: Google', '2019-06-23 03:58:56'),
(85, '2biISV9KJVWuVdAjDzgtu7ZTbpu8pnOj', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"ABxW4WqR0UxsQwkalVMifNyOrFtwCke6OkYiKrGED7M0W17PUlggXUMjdsGPZuNN\\\",\\\"token_ip\\\":\\\"197.180.163.87\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b5b5fadbaafdd86fe3a42d2abaaebf1d\\\"}', '{\\\"token\\\":\\\"S5nvQ1YZovHojLb5bTx69nBzhiIPpyNkd875jXGAaBdP9zEQPqRGGrQ2OFlR4bIF\\\",\\\"token_ip\\\":\\\"105.161.144.47\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"d1f99911e5343768de2700d882054d0e\\\"}', 'provider: Google', '2019-06-23 15:43:27'),
(86, 'H6X4i0My1VUjivsFuoX0Hf6Eba4f21Hm', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"S5nvQ1YZovHojLb5bTx69nBzhiIPpyNkd875jXGAaBdP9zEQPqRGGrQ2OFlR4bIF\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"d1f99911e5343768de2700d882054d0e\\\"}', '{\\\"token\\\":\\\"dFHwVgUZYqVvvLrQlUmY4pqp5mvlxnnT3vFX94roc3Bf5hgU7nT0cybHpPLeyPQT\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"09230f52787098f05193f1e027be7643\\\"}', 'provider: Google', '2019-06-23 19:36:26'),
(87, 'HHrsOXoQR0tKTooYLV3DtlhGEpKCSQ3C', 'SYSTEM', 'Create Account', 'H5FRGWKEXaivpKZMzlj2jU89OPR4vNrt', '', '', 'provider: email', '2019-06-25 19:33:15'),
(88, 'aZiAMfocYbdYh4YasgGAenMDTir5UZX6', 'SYSTEM', 'Create Account', '5S7WRHYAvLXpYTjq9ukhaKtWEsSQEl1d', '', '', 'provider: Google', '2019-06-27 15:28:00'),
(89, 'Od5PMfGh3bHEeHCLPoc2BLwNXAXe5QoD', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"dFHwVgUZYqVvvLrQlUmY4pqp5mvlxnnT3vFX94roc3Bf5hgU7nT0cybHpPLeyPQT\\\",\\\"token_ip\\\":\\\"105.161.144.47\\\",\\\"token_fingerprint\\\":\\\"09230f52787098f05193f1e027be7643\\\"}', '{\\\"token\\\":\\\"sXZ4GsLMFyypO6snipaukph23LNqyM3ULmZ1wrAe2Dpbt0XTqGUIxmYA7QX8qkfS\\\",\\\"token_ip\\\":\\\"105.60.39.227\\\",\\\"token_fingerprint\\\":\\\"7d873d17b951b6039561c48b0b9d3044\\\"}', 'provider: Google', '2019-07-01 18:27:53'),
(90, 'leMfT7vRKZBklxTZmEv1hbuzoaJINBYv', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"sXZ4GsLMFyypO6snipaukph23LNqyM3ULmZ1wrAe2Dpbt0XTqGUIxmYA7QX8qkfS\\\"}', '{\\\"token\\\":\\\"7HSajAxOYzZ6D5iQzUCmUZmPFUX6tw5x9Ch4Atq5Q3S9c9UG052v4Eu5hwYQ2qdk\\\"}', 'provider: Google', '2019-07-01 19:41:14'),
(91, 'Owx5hOWCpkZpHUhS2QEXYBZ7Hmpag2ZH', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'Update Account', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', '{\\\"reset_code\\\":null}', '{\\\"reset_code\\\":\\\"AwxksAGt3pknnnWTnohISWmcuc4jth19\\\"}', 'provider: Google', '2019-07-02 18:26:12'),
(92, 'AIN8WoJMXdT7Ha6HLQC7hL04XHfWg6kk', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'Update Account', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', '{\\\"hash\\\":\\\"\\\",\\\"reset_code\\\":\\\"AwxksAGt3pknnnWTnohISWmcuc4jth19\\\"}', '{\\\"hash\\\":\\\"b435fb8b7673b505803691d30b6ddeb8\\\",\\\"reset_code\\\":\\\"\\\"}', 'provider: Google', '2019-07-02 18:50:57'),
(93, 'ePyAkiFMdyb5V4oumxWtJ9xFK5ngSmax', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"7HSajAxOYzZ6D5iQzUCmUZmPFUX6tw5x9Ch4Atq5Q3S9c9UG052v4Eu5hwYQ2qdk\\\",\\\"token_ip\\\":\\\"105.60.39.227\\\",\\\"token_fingerprint\\\":\\\"7d873d17b951b6039561c48b0b9d3044\\\"}', '{\\\"token\\\":\\\"lwmanxm4Ht1H76OPJk6aqCWUmWIvC9A0seJFttBWJhE090vhnsGxvyy4vx48m54X\\\",\\\"token_ip\\\":\\\"41.81.219.176\\\",\\\"token_fingerprint\\\":\\\"8fbdaf0cb11b684f83e3b90aa59eb659\\\"}', 'provider: Google', '2019-07-03 17:04:57'),
(94, 'YPjZWGsk2zcGv1rIKocLHQ7ovUUKNaZH', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"lwmanxm4Ht1H76OPJk6aqCWUmWIvC9A0seJFttBWJhE090vhnsGxvyy4vx48m54X\\\"}', '{\\\"token\\\":\\\"rEMAjShiu19ckMdex2Ln4yLKq9fCPjccy2oRnvwswDRjCRs6vI4H1nylti6enyFR\\\"}', 'provider: Google', '2019-07-03 17:29:53'),
(95, 'BisSNKpYtzQuNNm5iyFwnRbUytLSmwv8', '5S7WRHYAvLXpYTjq9ukhaKtWEsSQEl1d', 'Update Account', '5S7WRHYAvLXpYTjq9ukhaKtWEsSQEl1d', '{\\\"email_verified\\\":0,\\\"verification_code\\\":\\\"cZuQvCdtVaprfXK3TkqqZE2uonNZ4NBb\\\"}', '{\\\"email_verified\\\":1,\\\"verification_code\\\":\\\"\\\"}', 'provider: Google', '2019-07-08 09:16:58'),
(96, 'HxMny7JVtgyUK7i8fZj1Lkb61ru31tqu', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"rEMAjShiu19ckMdex2Ln4yLKq9fCPjccy2oRnvwswDRjCRs6vI4H1nylti6enyFR\\\",\\\"token_ip\\\":\\\"41.81.219.176\\\",\\\"token_fingerprint\\\":\\\"8fbdaf0cb11b684f83e3b90aa59eb659\\\"}', '{\\\"token\\\":\\\"XJR8HCurtkev9wTUmxlpl4Oee3RBe7bcgaZ2LG87KZ9ZXkSJpYw96VKSsP811QVW\\\",\\\"token_ip\\\":\\\"41.80.154.103\\\",\\\"token_fingerprint\\\":\\\"dd52ee7f9ffbb07599f05d5c0d24cae0\\\"}', 'provider: Google', '2019-07-08 18:52:45'),
(97, 'zIfr94upePdQmnBStnqjFaGXQBvu0ObW', 'SYSTEM', 'Create Account', 'XEIwW6ocQcVaOrafe7YGGYGEEM5CxWjP', '', '', 'provider: Google', '2019-07-16 18:31:38'),
(98, 'YQs3fzf5pX5TFCGWvrBxgLH2LgZ8vqzp', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'XEIwW6ocQcVaOrafe7YGGYGEEM5CxWjP', '{\\\"email_verified\\\":0,\\\"verification_code\\\":\\\"JNHOf5tRvlG094EMK515kz8iU2NxIpFl\\\"}', '{\\\"email_verified\\\":1,\\\"verification_code\\\":\\\"\\\"}', 'provider: Google', '2019-07-16 19:01:16'),
(99, 'UJdmTyRYeVnI5dNXAsQSkKZj9wShlvcw', 'SYSTEM', 'Create Account', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '', '', 'provider: email', '2019-07-16 19:03:27'),
(100, 'b5O1Wn05dvrvsgqbunKZgIHYAs6kWBmv', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Update Account', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '{\\\"email_verified\\\":0,\\\"verification_code\\\":\\\"O6s5UBFGFRXflyW0d5e9zat73yfBVYLQ\\\"}', '{\\\"email_verified\\\":1,\\\"verification_code\\\":\\\"\\\"}', 'provider: email', '2019-07-16 19:04:48'),
(101, 'GyqtIOJICGxxn8B6CjHST27qxdXTnJGl', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Update Account', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"nlErOYyFcUF3DlE1IajsqEfeUTzVE9MNNtahOQJzukvAOMfTwHiHl09VhGN1bXbu\\\",\\\"token_ip\\\":\\\"41.212.112.15\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.142 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"13ceb5310f2987aaea9e6f435570c4ee\\\"}', 'provider: email', '2019-07-19 11:32:41'),
(102, 'yzKP2Hnmc098EY1fR8qm6hVjcTfCVODa', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"41.80.154.103\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"dd52ee7f9ffbb07599f05d5c0d24cae0\\\"}', '{\\\"token\\\":\\\"3kRZkhUid3MHk1YqrNTZgcZ8fEC4UHGJDLefAVGTNaHi48dWnnNbyVPzNkM3OPqU\\\",\\\"token_ip\\\":\\\"41.80.136.91\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.142 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"4d45ed81f6e775ae115ceee6f122dfc9\\\"}', 'provider: Google', '2019-07-27 01:45:34'),
(103, 'W8yuCpGAkE1l0Wkl8e1PexAsvoQabtlW', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"3kRZkhUid3MHk1YqrNTZgcZ8fEC4UHGJDLefAVGTNaHi48dWnnNbyVPzNkM3OPqU\\\",\\\"token_ip\\\":\\\"41.80.136.91\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.142 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"4d45ed81f6e775ae115ceee6f122dfc9\\\"}', '{\\\"token\\\":\\\"fVZjbla62Ddv6NFV4uEJEBxnuOHVY9YYvYiVBGHif3l1H9ejsLKDHOOf5DgfD0bm\\\",\\\"token_ip\\\":\\\"196.97.5.214\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.143 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"cc2ae02c5f5a78375c79229abc8a4da0\\\"}', 'provider: Google', '2019-07-28 05:02:49'),
(104, 'X9L84DIf3SQjMccW8Oj7HVDavKlfqkQL', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Update Account', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '{\\\"token\\\":\\\"nlErOYyFcUF3DlE1IajsqEfeUTzVE9MNNtahOQJzukvAOMfTwHiHl09VhGN1bXbu\\\",\\\"token_ip\\\":\\\"41.212.112.15\\\",\\\"token_fingerprint\\\":\\\"13ceb5310f2987aaea9e6f435570c4ee\\\"}', '{\\\"token\\\":\\\"WyG9cEWTA0IkG1ne8XiNiJQvVX0CJ5LkYHOfIiNsCvTqsv7QycrxABFbbzAOxbsh\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_fingerprint\\\":\\\"aa7e7185ea763e6fa8709076a2863547\\\"}', 'provider: email', '2019-07-29 09:21:05'),
(105, 'QVZU6NYQsyC7Iyvso4LB1875aEvmfmBY', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Update Account', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '{\\\"token\\\":\\\"WyG9cEWTA0IkG1ne8XiNiJQvVX0CJ5LkYHOfIiNsCvTqsv7QycrxABFbbzAOxbsh\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_fingerprint\\\":\\\"aa7e7185ea763e6fa8709076a2863547\\\"}', '{\\\"token\\\":\\\"dpPosecBsGBJLXpemM4Yu9SFJZ41I2bEshMHZTtCr5ZWTnPOtvLDzG9VMMRCxi4g\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"f0cbc9085a930f35263986b5ac78a116\\\"}', 'provider: email', '2019-07-30 18:25:06'),
(106, 'zH7EUmAKaaqAT0ojjeOzyrfQMtubAHez', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Update Account', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"wpjzeywbYZPODIryKaOcjCT67CFwPafBHp2vl1AKXxi1retWxEugMXoRPJY2GGiu\\\"}', 'provider: email', '2019-07-30 18:26:03'),
(107, 'B7Tghcfn4dKjr2uxRq2gV9SoZOfUM4ZW', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"fVZjbla62Ddv6NFV4uEJEBxnuOHVY9YYvYiVBGHif3l1H9ejsLKDHOOf5DgfD0bm\\\",\\\"token_ip\\\":\\\"196.97.5.214\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.143 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"cc2ae02c5f5a78375c79229abc8a4da0\\\"}', '{\\\"token\\\":\\\"KXOwcolkgxyXHneH1dxxR22bjNteLx0GsvnQNd0LYVblRv3P7SacSZ1QWeuTm508\\\",\\\"token_ip\\\":\\\"41.80.150.240\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.142 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"4a20a2d58d505c51fa9290cb40056353\\\"}', 'provider: Google', '2019-08-02 17:23:02'),
(108, '1a1ymEJEioalvaPB0mkTKR1iec2opJ10', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"KXOwcolkgxyXHneH1dxxR22bjNteLx0GsvnQNd0LYVblRv3P7SacSZ1QWeuTm508\\\",\\\"token_ip\\\":\\\"41.80.150.240\\\",\\\"token_fingerprint\\\":\\\"4a20a2d58d505c51fa9290cb40056353\\\"}', '{\\\"token\\\":\\\"FzuUChhAMwvAv07zup8yXncz6KfLMPVT8Jv0UurI4ZS8Q3VvG8i09FalSmPzRDu9\\\",\\\"token_ip\\\":\\\"105.166.3.243\\\",\\\"token_fingerprint\\\":\\\"7ad79de5f93cda2f67de8d81d4e81e36\\\"}', 'provider: Google', '2019-08-04 03:09:39'),
(109, 'A1sEoasZ61JZtonS28KWXbrHAnn9SOzy', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"FzuUChhAMwvAv07zup8yXncz6KfLMPVT8Jv0UurI4ZS8Q3VvG8i09FalSmPzRDu9\\\",\\\"token_ip\\\":\\\"105.166.3.243\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.142 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7ad79de5f93cda2f67de8d81d4e81e36\\\"}', '{\\\"token\\\":\\\"ovqwdt7E3uhwhrP6Ab4OwePnNo6PEiAIltK430BBEiZypaLBViZVhW3fDsspAB0n\\\",\\\"token_ip\\\":\\\"196.96.239.204\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.89 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"462f5ba52f6b6ae61ff9c9dea543a2bd\\\"}', 'provider: Google', '2019-08-04 12:38:55'),
(110, '6V4Qi5K5sHQwawdc3G1DSRdObleuqIxp', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"ovqwdt7E3uhwhrP6Ab4OwePnNo6PEiAIltK430BBEiZypaLBViZVhW3fDsspAB0n\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.89 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"462f5ba52f6b6ae61ff9c9dea543a2bd\\\"}', '{\\\"token\\\":\\\"7XxOU7luZ3lgVDDGUlQy6NOCZRBdhQ4AhEqsfKlfMMDfBVxC3Wxn7sNazpSiqIkD\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.142 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"ce2344cca3831b58877c410275012536\\\"}', 'provider: Google', '2019-08-04 22:32:45'),
(111, 'cPpzM0CNlOYkI2eq6JctrvLbMEhIgGjm', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Update Account', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '{\\\"token\\\":\\\"wpjzeywbYZPODIryKaOcjCT67CFwPafBHp2vl1AKXxi1retWxEugMXoRPJY2GGiu\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.142 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"f0cbc9085a930f35263986b5ac78a116\\\"}', '{\\\"token\\\":\\\"S3bNsrebP2NUFo8ghMFjsE7tn2e7DKRXFrqaE63OJLgp6gXZXR0wGR9OT20qk5Cp\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.89 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"f60f72994fb3ffe10bdab211f9c3e7f5\\\"}', 'provider: email', '2019-08-06 18:36:41'),
(112, 'af7G8XWGql3fHFVYEjJhevyD5nhPzmC5', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"7XxOU7luZ3lgVDDGUlQy6NOCZRBdhQ4AhEqsfKlfMMDfBVxC3Wxn7sNazpSiqIkD\\\",\\\"token_ip\\\":\\\"196.96.239.204\\\",\\\"token_fingerprint\\\":\\\"ce2344cca3831b58877c410275012536\\\"}', '{\\\"token\\\":\\\"w82B6bN9KU6NSC1koKerrgjzHlnT323Bd183gMM301SRkRuc0WjrABDaRqNXYnUO\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"a7e51c6e1055eaa28bf42b34744c8860\\\"}', 'provider: Google', '2019-08-06 18:43:26'),
(113, 'TrAAQ3EuLoZ0rDE1U6XPQQJIPhayZqoZ', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"w82B6bN9KU6NSC1koKerrgjzHlnT323Bd183gMM301SRkRuc0WjrABDaRqNXYnUO\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/75.0.3770.142 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"a7e51c6e1055eaa28bf42b34744c8860\\\"}', '{\\\"token\\\":\\\"kDF6W3QFV3W2hnSirInh5RFjfETwyQp3DVEGMPeW6KNiXE0ck7z8id0GSmQDiMXw\\\",\\\"token_ip\\\":\\\"41.80.127.20\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"c985cbf0796030ea5789c40af3800f02\\\"}', 'provider: Google', '2019-08-11 06:52:27'),
(114, 'GRBfvvM6H4qa3seW92J3mrPNbzIPglYd', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"kDF6W3QFV3W2hnSirInh5RFjfETwyQp3DVEGMPeW6KNiXE0ck7z8id0GSmQDiMXw\\\",\\\"token_ip\\\":\\\"41.80.127.20\\\",\\\"token_fingerprint\\\":\\\"c985cbf0796030ea5789c40af3800f02\\\"}', '{\\\"token\\\":\\\"Ick0Axf0I1yu1Ra0XblSdx9TEWrnt7aLLTkIwXKr22bulswOYKA4SJyp6s36gF9h\\\",\\\"token_ip\\\":\\\"41.90.196.159\\\",\\\"token_fingerprint\\\":\\\"6b9d97ffbdf0330f7485bec907bf2c86\\\"}', 'provider: Google', '2019-08-12 01:42:01');
INSERT INTO `ns_audit` (`_index`, `id`, `user`, `action`, `identifier`, `edit_from`, `edit_to`, `notes`, `timestamp`) VALUES
(115, '1vB0akdlRb4ytjQbbKCxSDtcJA4nhm58', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Update Account', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '{\\\"token\\\":\\\"S3bNsrebP2NUFo8ghMFjsE7tn2e7DKRXFrqaE63OJLgp6gXZXR0wGR9OT20qk5Cp\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.89 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"f60f72994fb3ffe10bdab211f9c3e7f5\\\"}', '{\\\"token\\\":\\\"937LEi4YfdIiPDsslBckOIayNZsWIYSH1NjQKCi0rQtmiEuL2aHzNeHHrcvKrbSt\\\",\\\"token_ip\\\":\\\"41.212.112.15\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"728e036233f0fc03655c44a9d6bd7556\\\"}', 'provider: email', '2019-08-13 13:22:24'),
(116, '5qJLsnlnB8jADDrLRbyYB8cmT8c5fv1e', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"Ick0Axf0I1yu1Ra0XblSdx9TEWrnt7aLLTkIwXKr22bulswOYKA4SJyp6s36gF9h\\\",\\\"token_ip\\\":\\\"41.90.196.159\\\",\\\"token_fingerprint\\\":\\\"6b9d97ffbdf0330f7485bec907bf2c86\\\"}', '{\\\"token\\\":\\\"SHAM346onF1hIl5gpk7edDTHr7w7JMoJ2FakgLspBQhkNApEmrVEU53I4xBbdU5v\\\",\\\"token_ip\\\":\\\"41.80.229.248\\\",\\\"token_fingerprint\\\":\\\"0934b90fe6d082a2793fb8dd07503c8b\\\"}', 'provider: Google', '2019-08-15 08:24:25'),
(117, '04wrcELvRMcK9AWwZMnG7XSpICeO4rbJ', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'Update Account', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"29KsohBajtwOYFegzgHL4CdjGK1ndnTc4D98eJXRlMqzMi1AyS5uxb49keBHNZIN\\\",\\\"token_ip\\\":\\\"41.80.229.248\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"0934b90fe6d082a2793fb8dd07503c8b\\\"}', 'provider: Google', '2019-08-15 08:25:28'),
(118, '4Zh1gFGOAhJlTcRpMkaAznrUDpOy4CQ9', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"0934b90fe6d082a2793fb8dd07503c8b\\\"}', '{\\\"token\\\":\\\"FFTanD2BDtzoToDpLwNybUT59m1rnQ2KS0m0Yyj4DWMfCItd7LsTfPC3IsnIyoaH\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.111 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"d495bb8fd7e460f02f6a048a07fa3d79\\\"}', 'provider: Google', '2019-08-15 09:22:04'),
(119, '2m53SBMqEUxFM6oBBFOK098xBEJ5Qmop', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Update Account', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '{\\\"token\\\":\\\"937LEi4YfdIiPDsslBckOIayNZsWIYSH1NjQKCi0rQtmiEuL2aHzNeHHrcvKrbSt\\\",\\\"token_ip\\\":\\\"41.212.112.15\\\",\\\"token_fingerprint\\\":\\\"728e036233f0fc03655c44a9d6bd7556\\\"}', '{\\\"token\\\":\\\"v1Gf5Yx7gHvBji6Ezw8bV5HmVXtBUlaGek2wrrj4SAZyoieoiK2MV846BG3odoBa\\\",\\\"token_ip\\\":\\\"212.49.95.37\\\",\\\"token_fingerprint\\\":\\\"182557aa23ad0fcb4bd033c6a9d21926\\\"}', 'provider: email', '2019-08-16 09:46:01'),
(120, 'if8AbRoGNXoxnjEiTdDXlhRfemTCpF7O', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"FFTanD2BDtzoToDpLwNybUT59m1rnQ2KS0m0Yyj4DWMfCItd7LsTfPC3IsnIyoaH\\\",\\\"token_ip\\\":\\\"41.80.229.248\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.111 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"d495bb8fd7e460f02f6a048a07fa3d79\\\"}', '{\\\"token\\\":\\\"b1W7Wb2xSplAwKUKzLjl4YEAAD4puY9yyTpAkUpBVBy2MeReCiugUKCwB17q1Io6\\\",\\\"token_ip\\\":\\\"41.90.192.207\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"a0330c58f64d8cbd016bb856ed3a5c6f\\\"}', 'provider: Google', '2019-08-16 16:30:08'),
(121, 'ha1hdHVqN1eYfOk2rlk6qCDhy9OjVmst', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"b1W7Wb2xSplAwKUKzLjl4YEAAD4puY9yyTpAkUpBVBy2MeReCiugUKCwB17q1Io6\\\",\\\"token_ip\\\":\\\"41.90.192.207\\\",\\\"token_fingerprint\\\":\\\"a0330c58f64d8cbd016bb856ed3a5c6f\\\"}', '{\\\"token\\\":\\\"TW9WOemVd9zz4oLJUZECfsSxRvlvr9LGneYeqeYXIxcHjbg2hdQBgWmKNsgp0aRU\\\",\\\"token_ip\\\":\\\"41.90.207.50\\\",\\\"token_fingerprint\\\":\\\"10e05bfbac25cc4e8af35afe2feb91c0\\\"}', 'provider: Google', '2019-08-16 22:42:55'),
(122, 'nJZgecOVoPmqyx3u44YUJ6kOoiuMvG6Y', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'Update Account', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', '{\\\"token\\\":\\\"29KsohBajtwOYFegzgHL4CdjGK1ndnTc4D98eJXRlMqzMi1AyS5uxb49keBHNZIN\\\",\\\"token_ip\\\":\\\"41.80.229.248\\\",\\\"token_fingerprint\\\":\\\"0934b90fe6d082a2793fb8dd07503c8b\\\"}', '{\\\"token\\\":\\\"qC8Fdjey0mgD7ly2zTH5g75cwwPb9MhN8eUVjC7ahUGCtrFXo76Jzla4zCN3MTzJ\\\",\\\"token_ip\\\":\\\"41.80.105.60\\\",\\\"token_fingerprint\\\":\\\"e52b582fe75ec335562f59e30b03e1cc\\\"}', 'provider: Google', '2019-08-17 11:34:21'),
(123, 'VIFZS5Hse0TorppVxmEk519t0dq0qk2t', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"66.249.93.55\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"2fe939c08b086441df2f9b6d8edeb72c\\\"}', '{\\\"token\\\":\\\"YDo0bRkGQaC13PJ8unWj3KOLn3MJGTxRqgh6JuNOhnwQ5yAtfJh7P18O6KcJqdCN\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.131 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"85f3af19f18540c892d799223e7543e1\\\"}', 'provider: Google', '2019-08-19 20:41:27'),
(124, 'drSjqS28oxtcMRSmV8R7EKcHB812kEUt', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Update Account', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '{\\\"token\\\":\\\"v1Gf5Yx7gHvBji6Ezw8bV5HmVXtBUlaGek2wrrj4SAZyoieoiK2MV846BG3odoBa\\\",\\\"token_ip\\\":\\\"212.49.95.37\\\",\\\"token_fingerprint\\\":\\\"182557aa23ad0fcb4bd033c6a9d21926\\\"}', '{\\\"token\\\":\\\"GsRMDPqLboAcgYoYzyRsUJbjPxvgEYSnoP8zXe4ELqSXcbYEKUPkL9cAJ9FFUymH\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"aa2d7287613f9d41ea175d07d2f297d4\\\"}', 'provider: email', '2019-08-20 18:55:30'),
(125, 'uwEAn3XKK0x4fWr7dQ5Mfm8TbkLhjPTW', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"66.249.93.57\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.0.0; SM-A720F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/70.0.3538.110 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"767e80ac840f25779f0fec755cb39da7\\\"}', '{\\\"token\\\":\\\"5ovQqivAoM1glJEYaWlTBhAhJixenpjauIRBSGkDhOsQKOfRwIItn8CT5JRuzVyi\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"ed79112e3489463ac25f750204dc8588\\\"}', 'provider: Google', '2019-08-20 19:10:13'),
(126, 'QglshnaDYkdNmCXznY7rWteOWMu7BTFZ', 'SYSTEM', 'Create Account', 'HcVOVsFTBspEuZTI5Tb5W1sFU5VFcRMj', '', '', 'provider: email', '2019-08-20 19:19:05'),
(127, 'I9jyTvqeBjwoBr18pT0wiBN1Cby1Wp8H', 'HcVOVsFTBspEuZTI5Tb5W1sFU5VFcRMj', 'Update Account', 'HcVOVsFTBspEuZTI5Tb5W1sFU5VFcRMj', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"iJ8RASwVK3dEccEQdg3GbuThASyMyH0LpOTo846M3E5sOHk7nHDpqouHfIHwl3ry\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; WOW64; Trident\\\\/7.0; rv:11.0) like Gecko\\\",\\\"token_fingerprint\\\":\\\"85f7f61487c976f9a64dc17bd195180d\\\"}', 'provider: email', '2019-08-20 19:19:05'),
(128, 'is8u8xi3VfAirAMzo4xcrNKgwLGxcl3O', 'SYSTEM', 'Create Account', 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', '', '', 'provider: email', '2019-08-20 19:20:49'),
(129, '1gdhdlTX97WObJFd1VSBrIAGoY8Nbaad', 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', 'Update Account', 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"zGbaZNIi4YDgwW3E3pzX9JkZfHCgFizEJ4gugferKqlA47FgaIMPZq6U9PgVJFAs\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"aa2d7287613f9d41ea175d07d2f297d4\\\"}', 'provider: email', '2019-08-20 19:20:49'),
(130, 'RGJko1HgXr4IZWBia54ExuH8fu0Z0Viw', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"YDo0bRkGQaC13PJ8unWj3KOLn3MJGTxRqgh6JuNOhnwQ5yAtfJh7P18O6KcJqdCN\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.131 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"85f3af19f18540c892d799223e7543e1\\\"}', '{\\\"token\\\":\\\"WAnRd3WmI0E0nqYHmIztnhhUC3ne1stpTcYgTiibDGnfhmmXXXHjsVaUu8gRPRX9\\\",\\\"token_ip\\\":\\\"41.90.4.104\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.111 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"18f65cdc9d6a92673bbf21ce60c11781\\\"}', 'provider: Google', '2019-08-20 19:21:39'),
(131, 'V31f4qVvh3ida0iYdkDyqNSKpZ7sCamb', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"5ovQqivAoM1glJEYaWlTBhAhJixenpjauIRBSGkDhOsQKOfRwIItn8CT5JRuzVyi\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"ed79112e3489463ac25f750204dc8588\\\"}', '{\\\"token\\\":\\\"iDl70Wa7jZCV6GsfXNwQrk8xUqFG9KMFKj0toMzWYQvuHxVloGtfBNttZoGE1b4T\\\",\\\"token_ip\\\":\\\"154.155.241.187\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.0.0; SM-A720F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/70.0.3538.110 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"0daa42a0ad80cecf537f31bacc4aeb33\\\"}', 'provider: Google', '2019-08-20 19:22:07'),
(132, 'hAjnIBzmpREvA6naVqLnvBGxH6SacV0a', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"iDl70Wa7jZCV6GsfXNwQrk8xUqFG9KMFKj0toMzWYQvuHxVloGtfBNttZoGE1b4T\\\",\\\"token_ip\\\":\\\"154.155.241.187\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.0.0; SM-A720F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/70.0.3538.110 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"0daa42a0ad80cecf537f31bacc4aeb33\\\"}', '{\\\"token\\\":\\\"eHzwvsBzPO4Kskr6bvJFc6tUgJm6YbB0NarFeo7UIVWxT40Lizt9CJzcnNrJuNJD\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"ed79112e3489463ac25f750204dc8588\\\"}', 'provider: Google', '2019-08-20 19:33:34'),
(133, 'TSN1yMiN9afsnvAuTLSTPzXuTnVeMxkn', 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', 'Update Account', 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', '{\\\"token\\\":\\\"zGbaZNIi4YDgwW3E3pzX9JkZfHCgFizEJ4gugferKqlA47FgaIMPZq6U9PgVJFAs\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"aa2d7287613f9d41ea175d07d2f297d4\\\"}', '{\\\"token\\\":\\\"aZIASKfeuRSnirsOm1Y5Vv5nEZblnQFQqiXaHOLla0VoKz3vs16Ca4YYrAHsCnN3\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.111 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"affc64dd29bfbd42befae8046d79467a\\\"}', 'provider: email', '2019-08-21 17:23:46'),
(134, 'kf8TGaQYi3s0ZUGiwjRYJS19HkVNFJMC', 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', 'Update Account', 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', '{\\\"token\\\":\\\"aZIASKfeuRSnirsOm1Y5Vv5nEZblnQFQqiXaHOLla0VoKz3vs16Ca4YYrAHsCnN3\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_fingerprint\\\":\\\"affc64dd29bfbd42befae8046d79467a\\\"}', '{\\\"token\\\":\\\"NGg7l0HAWzpwyVdsBkDRNkkdVj3zjDD2zaSpHaSXQ0PhwJdxzsuJ4tvXWns9C7oM\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_fingerprint\\\":\\\"5d54a024b2bb38c59a46a4451690145b\\\"}', 'provider: email', '2019-08-25 12:40:50'),
(135, 'Sa3NPOY4um81lFAYJ2RAortWvPMFc4ea', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"TW9WOemVd9zz4oLJUZECfsSxRvlvr9LGneYeqeYXIxcHjbg2hdQBgWmKNsgp0aRU\\\",\\\"token_ip\\\":\\\"41.90.207.50\\\",\\\"token_fingerprint\\\":\\\"10e05bfbac25cc4e8af35afe2feb91c0\\\"}', '{\\\"token\\\":\\\"mVfhhvBJvVDtYlnwZDFyTKMsHdFqeShDsu65JCFPzYaQ77aD42pEi2iy3g1M56ZQ\\\",\\\"token_ip\\\":\\\"41.80.249.249\\\",\\\"token_fingerprint\\\":\\\"bc307dd5c2e400b2219c6575a081cf1e\\\"}', 'provider: Google', '2019-08-27 11:35:31'),
(136, 'xb0ooV8T55jjxJzXT5but8ifkjIly4W8', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"WAnRd3WmI0E0nqYHmIztnhhUC3ne1stpTcYgTiibDGnfhmmXXXHjsVaUu8gRPRX9\\\",\\\"token_ip\\\":\\\"41.90.4.104\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.111 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"18f65cdc9d6a92673bbf21ce60c11781\\\"}', '{\\\"token\\\":\\\"Ycg5ECuSfRdgBCbWljgRHgdDhzdjPx1QLOwrKHK6tGdeayGAusxRhe2JD8rXuny5\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.131 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"8e542b958d1a474787d3709fe085d189\\\"}', 'provider: Google', '2019-08-27 18:20:30'),
(137, 'oN7zuLrI154PTqOH0IdfZ6MZnRhh3mKF', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', 'Update Account', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.169 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"98442b738359f2f17b7d202b881a685b\\\"}', '{\\\"token\\\":\\\"yUgLKyrypOlA7FMPWLxfdp3oP1zVWSjfLgD7qo2pyDPaU3AW1jUu4PhIOeikSv28\\\",\\\"token_ip\\\":\\\"197.178.205.206\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.111 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"6712f095ac62b5491790bb749ba96fb4\\\"}', 'provider: email', '2019-08-28 22:16:14'),
(138, 'GRjpTrZu8XRubVvIwdCO7yRdb9CDUD0Y', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.237.128.235\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.131 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7237c7b579e1e8895181621f7b8ae255\\\"}', '{\\\"token\\\":\\\"fFLmCjz3HXt2QLiQYwAimpuidUnXXqasgkGuYCFIYyB4RBrGyQLvYj201m4KsQxa\\\",\\\"token_ip\\\":\\\"196.108.206.118\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"cd194d5c4dcf0865f02708faf7ebe0a4\\\"}', 'provider: Google', '2019-08-31 19:08:52'),
(139, 'K224prl4mN98bGGbcgyJ7tUeIp9lP7Yc', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"Ycg5ECuSfRdgBCbWljgRHgdDhzdjPx1QLOwrKHK6tGdeayGAusxRhe2JD8rXuny5\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"8e542b958d1a474787d3709fe085d189\\\"}', '{\\\"token\\\":\\\"JUkighbA4HfYEGLXdba6X5tRiuPROLeduqo2HkYNMtDo4MtnYzCuVrTlRjSlEzyd\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_fingerprint\\\":\\\"85f3af19f18540c892d799223e7543e1\\\"}', 'provider: Google', '2019-09-08 19:46:18'),
(140, 'skusUA34FQv2qyAsLoH1XOG7spC644Xo', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"mVfhhvBJvVDtYlnwZDFyTKMsHdFqeShDsu65JCFPzYaQ77aD42pEi2iy3g1M56ZQ\\\",\\\"token_ip\\\":\\\"41.80.249.249\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"bc307dd5c2e400b2219c6575a081cf1e\\\"}', '{\\\"token\\\":\\\"zP3enY3vqzaobYg7KgjMnqcSgPlDzocVE83wGcfLlV5OefnXmAvjGSJ4VsbxhoHN\\\",\\\"token_ip\\\":\\\"41.90.191.175\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"378977aed56a00f184a0acd5760f0e10\\\"}', 'provider: Google', '2019-09-08 23:27:20'),
(141, 'fkLPJS33Gm6oJsTdkXdHycw7Jin62aM5', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"zP3enY3vqzaobYg7KgjMnqcSgPlDzocVE83wGcfLlV5OefnXmAvjGSJ4VsbxhoHN\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"378977aed56a00f184a0acd5760f0e10\\\"}', '{\\\"token\\\":\\\"VA7uueszLCiHNfT7sk9RJFmoRQS4dUXTIkdd5jIhJOP7c4hCaSJhVWWQT6zJeAVR\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"37a60c540f92aa5c5ffb04737279e7a0\\\"}', 'provider: Google', '2019-09-09 01:14:46'),
(142, 'NYU2VkCNMo5lIiY5Uqqf6cvEacuMwqQD', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"VA7uueszLCiHNfT7sk9RJFmoRQS4dUXTIkdd5jIhJOP7c4hCaSJhVWWQT6zJeAVR\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"37a60c540f92aa5c5ffb04737279e7a0\\\"}', '{\\\"token\\\":\\\"EnjupGV0ORuZ10wSW4kWHNTpJH9JamDZuXzKLOANJaWMUCFDzIeWIGGCAFNIfCW2\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"378977aed56a00f184a0acd5760f0e10\\\"}', 'provider: Google', '2019-09-10 10:57:42'),
(143, 'ifrmJ1Hjx2I9ubPBE6fvxshAOrPRAiZP', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"EnjupGV0ORuZ10wSW4kWHNTpJH9JamDZuXzKLOANJaWMUCFDzIeWIGGCAFNIfCW2\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"378977aed56a00f184a0acd5760f0e10\\\"}', '{\\\"token\\\":\\\"cmGKnA1sFZ5Kh99xdtHTC6W1ChVdKdKhef5qnyka7pL5RpG1WexMqfb6RM7r8u2H\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"37a60c540f92aa5c5ffb04737279e7a0\\\"}', 'provider: Google', '2019-09-11 01:51:16'),
(144, 'r3cZ2LNTnVHLc49CspMc4u14Qf86sKSH', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"cmGKnA1sFZ5Kh99xdtHTC6W1ChVdKdKhef5qnyka7pL5RpG1WexMqfb6RM7r8u2H\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"37a60c540f92aa5c5ffb04737279e7a0\\\"}', '{\\\"token\\\":\\\"CONHbGqgqxeOGW3dFaTRij09zoqmp3PSe6t15fHLEMjSc9sRIod1CewBGecmPbL6\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"378977aed56a00f184a0acd5760f0e10\\\"}', 'provider: Google', '2019-09-11 02:26:12'),
(145, 'QiQ6kAQ8eswnQBj7zR1U1mDOhpEHKb5E', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"CONHbGqgqxeOGW3dFaTRij09zoqmp3PSe6t15fHLEMjSc9sRIod1CewBGecmPbL6\\\",\\\"token_ip\\\":\\\"41.90.191.175\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"378977aed56a00f184a0acd5760f0e10\\\"}', '{\\\"token\\\":\\\"BFV6r4wX34m9r0UT3TeOtyWClH9PxRTNXxnfRhT1KjcQaauSJJDlCevvn1XdF8gO\\\",\\\"token_ip\\\":\\\"41.80.104.87\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7433aa1ef325c4f030e99708fc3e0cff\\\"}', 'provider: Google', '2019-09-17 14:37:03'),
(146, '8v9YlHbFhLxWQWvlNOLD4vxSb8pDA3UO', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"fFLmCjz3HXt2QLiQYwAimpuidUnXXqasgkGuYCFIYyB4RBrGyQLvYj201m4KsQxa\\\",\\\"token_ip\\\":\\\"196.108.206.118\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"cd194d5c4dcf0865f02708faf7ebe0a4\\\"}', '{\\\"token\\\":\\\"qadsu4gCK7GeWyRMjFlZKh2XtcB8Nv5yW5rux7sVkI8ufatJ63DCoIjLWi57xhqV\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"ef30543b61505ea327cb33b124382ab8\\\"}', 'provider: Google', '2019-09-17 18:50:41'),
(147, 'caeiZvmANh7BWdjoFlC87EsSUgZI0BiE', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', 'Update Account', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"lE61fhlNC5VXbI8R6LLCehemVpyQ11roMtqeei02D0wzd2qVFYO53bQOu4VorYb1\\\",\\\"token_ip\\\":\\\"41.90.5.195\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.75 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"5a972292a3147e74d711e8e0b67acceb\\\"}', 'provider: Google', '2019-09-17 19:32:30'),
(148, 'K2dofgWA29H7LVHsQtu15aXBzYQFh41C', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', 'Update Account', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"LgB9dlZuvpn1cHGiNar35oNkfoBIzryXPHnnoRwl0FeQNYzFe5lOw8D6o8cRnzmg\\\"}', 'provider: Google', '2019-09-17 19:35:22'),
(149, 'avCGjDyj5qtQ1MwX8G24nWBCQKiFw89T', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', 'Update Account', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"41.90.5.195\\\",\\\"token_fingerprint\\\":\\\"5a972292a3147e74d711e8e0b67acceb\\\"}', '{\\\"token\\\":\\\"TYKKIAWh1MCK2oIIxER2HipRiigz0UQqL8Rw3vVVLFBaK5g2PCsaj67rAi9gKigo\\\",\\\"token_ip\\\":\\\"41.90.4.184\\\",\\\"token_fingerprint\\\":\\\"cbe04d4d1da379a4c7e5efa5780163fd\\\"}', 'provider: Google', '2019-09-17 19:36:10'),
(150, 'fq1s1CeBEINkYu8SHdMJm4fiK7DQJsJO', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"eHzwvsBzPO4Kskr6bvJFc6tUgJm6YbB0NarFeo7UIVWxT40Lizt9CJzcnNrJuNJD\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"ed79112e3489463ac25f750204dc8588\\\"}', '{\\\"token\\\":\\\"LCIhzcdfk4QMC2I9boHGlfswQhhJ3539ErAgyP3fx7upW0CPuEmKjIEZdTbTisDn\\\",\\\"token_ip\\\":\\\"154.76.232.28\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.0.0; SM-A720F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/70.0.3538.110 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"eecf3fb46ea041b43a94dcc4aaf5339f\\\"}', 'provider: Google', '2019-09-17 19:36:38'),
(151, 'KHdoD2FDnjx3amBSk8x9ISoeGpUiBOyv', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"JUkighbA4HfYEGLXdba6X5tRiuPROLeduqo2HkYNMtDo4MtnYzCuVrTlRjSlEzyd\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.131 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"85f3af19f18540c892d799223e7543e1\\\"}', '{\\\"token\\\":\\\"ahw2gIU7ZAejxHZzHmw62bPBx0kWNvB3aKsMRYXO8sd9aEYMSBkhXAi3s8gBVW8J\\\",\\\"token_ip\\\":\\\"41.90.4.117\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.75 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7824482f2e0738968fef914f3d1efed3\\\"}', 'provider: Google', '2019-09-17 19:37:19'),
(152, 'RuGYx8QY0ySPPN2tB1gpd81qYJ1LZzmv', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"qadsu4gCK7GeWyRMjFlZKh2XtcB8Nv5yW5rux7sVkI8ufatJ63DCoIjLWi57xhqV\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"ef30543b61505ea327cb33b124382ab8\\\"}', '{\\\"token\\\":\\\"tZi1Ws0Jq8YvVI19VQazVOZzfBZCxCzt4FdYB5K0V2oATgGcvbUcykkg7jsqa56r\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_fingerprint\\\":\\\"da6f900d4cba5dacdc23c629b469239e\\\"}', 'provider: Google', '2019-09-19 07:59:44'),
(153, 'zPbUI6JuqePl9NfiOcwOQlcHamNZen3B', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"BFV6r4wX34m9r0UT3TeOtyWClH9PxRTNXxnfRhT1KjcQaauSJJDlCevvn1XdF8gO\\\",\\\"token_ip\\\":\\\"41.80.104.87\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7433aa1ef325c4f030e99708fc3e0cff\\\"}', '{\\\"token\\\":\\\"h9S1MdQGkH9KTs9qNNAgxKp6nNgHBxgW7gVSKNSKWqJx0lwGhL0jCB8KPJIm2VUE\\\",\\\"token_ip\\\":\\\"41.80.130.114\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"9c1a0aa084caa1eaab9af1df126319d3\\\"}', 'provider: Google', '2019-09-19 21:39:52'),
(154, 'jDokEMh5grupCD5hswu9s5wn95vCAlHg', 'SYSTEM', 'Create Account', '35DrzmfG2VxxM7NC06gH6HZymJgyRhsD', '', '', 'provider: Google', '2019-09-20 10:58:50'),
(155, 'CXnTY5ZDEjEpmikVOSeqDKwA1oc1KZ6Q', '35DrzmfG2VxxM7NC06gH6HZymJgyRhsD', 'Update Account', '35DrzmfG2VxxM7NC06gH6HZymJgyRhsD', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"f6vYp5CFqJl157A5wU5bWDYLzzaCdEAXHvxcJrsyV87snjuB4kK4rzwTubm89JA2\\\",\\\"token_ip\\\":\\\"197.237.251.249\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit\\\\/605.1.15 (KHTML, like Gecko) Version\\\\/12.1.2 Mobile\\\\/15E148 Safari\\\\/604.1\\\",\\\"token_fingerprint\\\":\\\"9b9518078fbd7a6823ea6f5ff8a19a69\\\"}', 'provider: Google', '2019-09-20 10:58:52'),
(156, '9bIUoM9bHr0rs2e7o3Hh4cUJAOouPegi', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', 'Update Account', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', '{\\\"reset_code\\\":null}', '{\\\"reset_code\\\":\\\"xhk2V38xhfTDwZjPUCUmzqo1TQWIkwcB\\\"}', 'provider: email', '2019-09-20 12:25:13'),
(157, 'bRYPzEH0LNECuOLBTNUB1Qa2ULarvoPq', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', 'Update Account', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', '{\\\"hash\\\":\\\"ca0baa4451d41cb1841d02eb933b6119\\\",\\\"reset_code\\\":\\\"xhk2V38xhfTDwZjPUCUmzqo1TQWIkwcB\\\"}', '{\\\"hash\\\":\\\"c80fc01f0f5d34a8454fa72552b389cc\\\",\\\"reset_code\\\":\\\"\\\"}', 'provider: email', '2019-09-20 12:25:47'),
(158, 'AbZZ0nAAMuGvtt0tICXWYzJH7BQdcTmn', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', 'Update Account', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"qPHZHX5n637qDYFXjsR5P2zjOarF2tkuWHbN9TMgLUL3E8zcB79Ib8pvu3yKnUFY\\\",\\\"token_ip\\\":\\\"197.237.251.249\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"99034fc73d425a888f0bd6fb26915bfd\\\"}', 'provider: email', '2019-09-20 12:26:35'),
(159, 'DREXYxo18WluY9fh6tnykqAuMRHlKfJ1', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"tZi1Ws0Jq8YvVI19VQazVOZzfBZCxCzt4FdYB5K0V2oATgGcvbUcykkg7jsqa56r\\\"}', '{\\\"token\\\":\\\"AeRQTf1axPJE8xBRAYZSBd4VGifa988n0VoFjJ0P6nOxjmJJ2fqaFHY6Q5a9HKvo\\\"}', 'provider: Google', '2019-09-20 23:29:49'),
(160, 'giK1zKgdhJCH7QF64mFo1gGtN0NMuhj0', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'Update Account', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', '{\\\"token\\\":\\\"qC8Fdjey0mgD7ly2zTH5g75cwwPb9MhN8eUVjC7ahUGCtrFXo76Jzla4zCN3MTzJ\\\",\\\"token_ip\\\":\\\"41.80.105.60\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.100 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"e52b582fe75ec335562f59e30b03e1cc\\\"}', '{\\\"token\\\":\\\"jMEsgV7mMcfnaCfXA3N3V89h53YuiCrTJrO0PMzrxeYUNToSpMhD1fzf8XXdNp3p\\\",\\\"token_ip\\\":\\\"41.80.130.114\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"9c1a0aa084caa1eaab9af1df126319d3\\\"}', 'provider: Google', '2019-09-21 07:41:56'),
(161, 'ZQ5h5RZR2vjkjbYFPOubaPs1ZAP9MItX', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"AeRQTf1axPJE8xBRAYZSBd4VGifa988n0VoFjJ0P6nOxjmJJ2fqaFHY6Q5a9HKvo\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/76.0.3809.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"da6f900d4cba5dacdc23c629b469239e\\\"}', '{\\\"token\\\":\\\"zEQ7YVe7y2TXADK0riNQfv5NkxUYsrzN02Z8S7KJGdY7uyrsHd5L7yRMCv4vNjX2\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.92 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"330695f98b88df9d1cf179112a573ed1\\\"}', 'provider: Google', '2019-09-22 23:27:44'),
(162, 'bXBciNZsXzFy3jSV7emp5gTNmmegnvQK', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"ahw2gIU7ZAejxHZzHmw62bPBx0kWNvB3aKsMRYXO8sd9aEYMSBkhXAi3s8gBVW8J\\\",\\\"token_ip\\\":\\\"41.90.4.117\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.75 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7824482f2e0738968fef914f3d1efed3\\\"}', '{\\\"token\\\":\\\"dEXkCCndDPyJZIQQmVBmii8SqYLByfMjmQwwX0JRtxbrXrguaj6sTyGACRvqSs55\\\",\\\"token_ip\\\":\\\"41.90.7.85\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"786f5cf14bfcab41d0b3994f6d5dfb15\\\"}', 'provider: Google', '2019-09-24 18:16:23'),
(163, 'sJr60hBchgw5tQ52k6YmZLy3gXtPs901', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"LCIhzcdfk4QMC2I9boHGlfswQhhJ3539ErAgyP3fx7upW0CPuEmKjIEZdTbTisDn\\\",\\\"token_ip\\\":\\\"154.76.232.28\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.0.0; SM-A720F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/70.0.3538.110 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"eecf3fb46ea041b43a94dcc4aaf5339f\\\"}', '{\\\"token\\\":\\\"8XMAOZB4hYHY9Ms6zgSn7FUYLNLnYyESynXUqGRfeacBgcbCEzaJrj8UWzkuPqOF\\\",\\\"token_ip\\\":\\\"41.90.7.88\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"f3a7303be3ffcbde8d642b4ca2755a8a\\\"}', 'provider: Google', '2019-09-24 18:42:44'),
(164, 'PQmZyxEob1HkjTNqDuZ92GeWBIZhAuD9', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"zEQ7YVe7y2TXADK0riNQfv5NkxUYsrzN02Z8S7KJGdY7uyrsHd5L7yRMCv4vNjX2\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.92 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"330695f98b88df9d1cf179112a573ed1\\\"}', '{\\\"token\\\":\\\"qnwQPVwIAhlRlNhZulGMEtp7DtoWmJsF6UJzsGkPtFqyXi8yVPvlkahu7SXq3Kc9\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b1d09f0fb813b228dd69458e02c23211\\\"}', 'provider: Google', '2019-09-24 18:48:02'),
(165, '9EGjEsCBz6wcMTIJE0waMSZuJoXFrm9t', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"41.90.7.85\\\",\\\"token_fingerprint\\\":\\\"786f5cf14bfcab41d0b3994f6d5dfb15\\\"}', '{\\\"token\\\":\\\"YG793mXOnYj8aPMCat4SedWYXi7D9102ncY4nwdIx3bPaQE0az7Jua5Icet2FcqU\\\",\\\"token_ip\\\":\\\"41.90.6.0\\\",\\\"token_fingerprint\\\":\\\"5a0b28bf4af13d191b88254f7b381e26\\\"}', 'provider: Google', '2019-09-24 19:39:47'),
(166, 'XC7fRwboIVjsugRY1flk32j92ngiYS90', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', 'Update Account', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', '{\\\"reset_code\\\":\\\"\\\"}', '{\\\"reset_code\\\":\\\"mgwMP3Bf1f7uU7LFDVE6mnya4YaqueM7\\\"}', 'provider: email', '2019-09-24 21:35:50'),
(167, 'LhdkbsSgFmZTMboiu37EvdOOkjRCbw0a', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"qnwQPVwIAhlRlNhZulGMEtp7DtoWmJsF6UJzsGkPtFqyXi8yVPvlkahu7SXq3Kc9\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b1d09f0fb813b228dd69458e02c23211\\\"}', '{\\\"token\\\":\\\"WnLXHqZGnY7h1YIUp1Dn0VcXAnU2BI561jmJYGpjiTejyz2SJCgFV13Nv7E4zv4i\\\",\\\"token_ip\\\":\\\"196.110.181.49\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.92 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"231c9a5961760775109f254003be28f5\\\"}', 'provider: Google', '2019-09-28 18:41:50'),
(168, 'B8MnKJrUgczmH35AAWDcO3S5oc3hBQGx', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"WnLXHqZGnY7h1YIUp1Dn0VcXAnU2BI561jmJYGpjiTejyz2SJCgFV13Nv7E4zv4i\\\",\\\"token_ip\\\":\\\"196.110.181.49\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.92 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"231c9a5961760775109f254003be28f5\\\"}', '{\\\"token\\\":\\\"bIcoPs7EzEtwRwFIbkjvmXBAU4ktggPhfNM2oJcOqTkgzhFpOoivMBzATEnSZzHk\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b4fedc91e809081bc42d6b29c5e06bdd\\\"}', 'provider: Google', '2019-09-30 10:22:13'),
(169, 'GFtPLJ75W4Zbp1VdSYN2cRCUQpIooWWK', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"h9S1MdQGkH9KTs9qNNAgxKp6nNgHBxgW7gVSKNSKWqJx0lwGhL0jCB8KPJIm2VUE\\\",\\\"token_ip\\\":\\\"41.80.130.114\\\",\\\"token_fingerprint\\\":\\\"9c1a0aa084caa1eaab9af1df126319d3\\\"}', '{\\\"token\\\":\\\"fu1Aw7XegaLZBM6JC8IqYJlz8JBChiev9X5EpYKWkqFRNjQTofCwQOQlGdlZeQj8\\\",\\\"token_ip\\\":\\\"41.80.238.142\\\",\\\"token_fingerprint\\\":\\\"0bb1b327e480b5e79d054272673a4d0b\\\"}', 'provider: Google', '2019-10-01 09:22:56'),
(170, 'tTyQy7jZ0Fr9C05T2JkXOufqPPIFNX2i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"bIcoPs7EzEtwRwFIbkjvmXBAU4ktggPhfNM2oJcOqTkgzhFpOoivMBzATEnSZzHk\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_fingerprint\\\":\\\"b4fedc91e809081bc42d6b29c5e06bdd\\\"}', '{\\\"token\\\":\\\"9nV5iCD6ng97ifqz28xHSDZrVwURYOqD3ZeqrceuSZRM05dgalGTDq8m2WqY0Azd\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"b1d09f0fb813b228dd69458e02c23211\\\"}', 'provider: Google', '2019-10-01 18:41:37'),
(171, 'igZYH5AmVwaC4exCSvGPIKn0ya8WYk1C', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', 'Update Account', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"41.90.4.184\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.75 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"cbe04d4d1da379a4c7e5efa5780163fd\\\"}', '{\\\"token\\\":\\\"rw5U5OiovLDjlDx7C04lHSuerFGCQi9w4WQdwusWVk32JlBCiNanF3LNTSU4AjSy\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"cf6ea73303a571c1f5783e8c3b991edf\\\"}', 'provider: Google', '2019-10-01 18:52:54'),
(172, 'B5GuH0F7WBK1YR6nXK9XgG0gdiCQTUzg', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"9nV5iCD6ng97ifqz28xHSDZrVwURYOqD3ZeqrceuSZRM05dgalGTDq8m2WqY0Azd\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b1d09f0fb813b228dd69458e02c23211\\\"}', '{\\\"token\\\":\\\"4pmxFL349DqMpJhWmLXbycKWfljXiZjiu5RuliZonRJCU5eQx9SxGjiegkhMSoql\\\",\\\"token_ip\\\":\\\"105.59.5.27\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.92 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"366155eefb577a9249b8e0fb62dfbf6b\\\"}', 'provider: Google', '2019-10-01 19:45:43'),
(173, 'hTBBeJnKGyd6MVknBr8K7w2Qqc8XMqPr', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"4pmxFL349DqMpJhWmLXbycKWfljXiZjiu5RuliZonRJCU5eQx9SxGjiegkhMSoql\\\",\\\"token_ip\\\":\\\"105.59.5.27\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-G955U) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.92 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"366155eefb577a9249b8e0fb62dfbf6b\\\"}', '{\\\"token\\\":\\\"UiC2jTSjTT8GR32zdfCnwmABwEtDC9FtdNRtDWnReJgWYlDPp8aSyMarjjLadGCJ\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"86369f87ae4780efc65b94dfe65ce961\\\"}', 'provider: Google', '2019-10-02 09:09:56'),
(174, 'q4kbqj8mulHILkuSFPn6MEdMZJwww6nk', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', 'Update Account', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', '{\\\"token\\\":\\\"rw5U5OiovLDjlDx7C04lHSuerFGCQi9w4WQdwusWVk32JlBCiNanF3LNTSU4AjSy\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"cf6ea73303a571c1f5783e8c3b991edf\\\"}', '{\\\"token\\\":\\\"aB0Kckl7UWuCyu1ENGWg48rnmNggglwSOTXSflFJaYlsQgSuhb1QKgDAvZEkrEix\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_fingerprint\\\":\\\"22ff1eef84242474215726aae3e9b41d\\\"}', 'provider: Google', '2019-10-02 18:10:59'),
(175, 'Qt38UpXP3FXyoZZtwFtSOZeDLMPhOkcb', 'SYSTEM', 'Create Account', 'VXliwISX9SyZ23tlbn5Qf1t6Tg0uJmAR', '', '', 'provider: Google', '2019-10-02 18:13:14'),
(176, 'umb7V1XwsemdpTroB7583RaGBygwmhoF', 'VXliwISX9SyZ23tlbn5Qf1t6Tg0uJmAR', 'Update Account', 'VXliwISX9SyZ23tlbn5Qf1t6Tg0uJmAR', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"4fWpS7iniKZ4FBeNd1uiTveFehlfoa4qWq7ITZsZSuUSWdXEfuHCwsReTvKoP1sG\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"22ff1eef84242474215726aae3e9b41d\\\"}', 'provider: Google', '2019-10-02 18:13:15'),
(177, 'Go7gefdi8Fa2OPzMTKWnmiGa6ikSpd3T', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"YG793mXOnYj8aPMCat4SedWYXi7D9102ncY4nwdIx3bPaQE0az7Jua5Icet2FcqU\\\",\\\"token_ip\\\":\\\"41.90.6.0\\\",\\\"token_fingerprint\\\":\\\"5a0b28bf4af13d191b88254f7b381e26\\\"}', '{\\\"token\\\":\\\"Me9scWtfEN4Lm8g6VOPo5vEssM8yb0do86cGhHpiWag7oemIJklPQ6tTE9vxmoF2\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_fingerprint\\\":\\\"22ff1eef84242474215726aae3e9b41d\\\"}', 'provider: Google', '2019-10-02 18:16:26'),
(178, 'wNHkHEOPKKljnifMWPVQLdIf2AaMxWj8', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"Me9scWtfEN4Lm8g6VOPo5vEssM8yb0do86cGhHpiWag7oemIJklPQ6tTE9vxmoF2\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_fingerprint\\\":\\\"22ff1eef84242474215726aae3e9b41d\\\"}', '{\\\"token\\\":\\\"rHQN8rjoy1DmR7hSlCgwfPk8Hw6S2rS0H1NCNBpdAK9eFOOkDS1rJ7erwLpujsL8\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_fingerprint\\\":\\\"ef13d506def163ad425ec9bfca4ef782\\\"}', 'provider: Google', '2019-10-05 09:35:35'),
(179, 'Yh7EUDaKy0Fs9mGFev7RkaJ0I1zKiIXw', 'SYSTEM', 'Create Account', 'igoQnHOXrlyhwTt1AnSOLUkFP9n789DI', '', '', 'provider: email', '2019-10-05 11:11:21'),
(180, 'v2I9quG4JGOotZ32hkycY5rt8lcnNrO3', 'igoQnHOXrlyhwTt1AnSOLUkFP9n789DI', 'Update Account', 'igoQnHOXrlyhwTt1AnSOLUkFP9n789DI', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"RjkpOxfEWfnqw9s2PhiELTiTvKDrMrq3tQGUDfUPNtEFqc1g70cJBaQfh3rnXgIO\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"86369f87ae4780efc65b94dfe65ce961\\\"}', 'provider: email', '2019-10-05 11:11:21'),
(181, 'lsRXYrumBUtAr47wCl9aJuloR5ws3AGJ', 'SYSTEM', 'Create Account', 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', '', '', 'provider: email', '2019-10-05 11:11:41'),
(182, 'n9wNeKmkzNX9jIDIoEeDXn4ZxaTDVMeb', 'igoQnHOXrlyhwTt1AnSOLUkFP9n789DI', 'Update Account', 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"UiCUODJIoneIDZd5ncGr326ttYgZmdE4N1HsjD7u8tmX6Kfm0t858txBKqRaptOV\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"86369f87ae4780efc65b94dfe65ce961\\\"}', 'provider: email', '2019-10-05 11:11:41'),
(183, 'z1Vh4nRMTyZUL6LaVcfUKsTx878Hup0X', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"86369f87ae4780efc65b94dfe65ce961\\\"}', '{\\\"token\\\":\\\"1sZl4n8Bs0qO2io38oxEgLAxVXQ7CKfHExfCNUOOQByOFAEYnyoW1lBoFDQr2Fpb\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.92 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"2ea1e94a24c73bc64bacb4006a6487d6\\\"}', 'provider: Google', '2019-10-05 16:10:58'),
(184, 'RqqnlEYYLrD0xw6vtaMmWcp2IjkxGAmH', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"1sZl4n8Bs0qO2io38oxEgLAxVXQ7CKfHExfCNUOOQByOFAEYnyoW1lBoFDQr2Fpb\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.92 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"2ea1e94a24c73bc64bacb4006a6487d6\\\"}', '{\\\"token\\\":\\\"jvulU2MdtD03Cfe7mqzmnqzCCKyipmkKtSdU0CAVHf7qQYfW8d1cBL1ldDXqRpc7\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b4fedc91e809081bc42d6b29c5e06bdd\\\"}', 'provider: Google', '2019-10-06 19:17:50'),
(185, 'ERBXShNzGDIbgTPdxPHpXuJtFP5K7d4t', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"jvulU2MdtD03Cfe7mqzmnqzCCKyipmkKtSdU0CAVHf7qQYfW8d1cBL1ldDXqRpc7\\\",\\\"token_ip\\\":\\\"197.232.41.80\\\",\\\"token_fingerprint\\\":\\\"b4fedc91e809081bc42d6b29c5e06bdd\\\"}', '{\\\"token\\\":\\\"0vW005NE9ULWBmpUzmDnC8CSH0KW6oBUaLrHerrDWmeKoNI9n0CAC1syGBFUdCGF\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_fingerprint\\\":\\\"86369f87ae4780efc65b94dfe65ce961\\\"}', 'provider: Google', '2019-10-08 11:30:07'),
(186, 'tpbRXpyGhqti0X8MPpaJNWWDYa3Yxjkk', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"RsjRXLRaqAosu3h7kDPCFM66bxc8BlekE1X1ZJMkQdFlYgvnBFSpYs2l3BbjfvQV\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"89629a611709a0a6228d46fcb3adad3a\\\"}', 'provider: Google', '2019-10-12 22:40:03'),
(187, 'woNDyTjiOpxKGwAfW3Z7oKnh9NSC7mDx', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"mCZCqdvpUGcAHiMhLtoS78eI1OV7LpzkhtiPjsKffIjVCfoURTRLW3ArWytWvq7b\\\"}', 'provider: Google', '2019-10-12 22:41:04'),
(188, 'sYHKH1qNLxj2nuz97NeE1lK0gMS3x9vw', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"QIoWifhF0PLnIPyk8j3dffMgHRO5I6Ssg1A4QepNWzAszyZs1Gx7XOC7lNHo1roc\\\"}', 'provider: Google', '2019-10-12 23:00:36'),
(189, '7w8t72rGuak7pYbuylXGpsVLt9RsEWbM', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"RwTnWEQv2LtAk1Jj865b18SS68m1DS2Ziirj7YbY8FhGabHFmwNaushGwYNDpF9j\\\"}', 'provider: Google', '2019-10-12 23:02:43'),
(190, 'VrmlQ1gN9dNe3V24ek4QUmWLFQXlCmNI', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"gUiys8jHwrM2bmd70Kk0wU7Rg7kmgZZFp1wGOrlWtqLw1IWxCJPAKlEdYs26s9EG\\\"}', 'provider: Google', '2019-10-12 23:24:59');
INSERT INTO `ns_audit` (`_index`, `id`, `user`, `action`, `identifier`, `edit_from`, `edit_to`, `notes`, `timestamp`) VALUES
(191, 'QiBqXqzwJQlvsjF8leeUZ23q7s09LDTP', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"4jICPm49yygqsGUQ4kr2s6DCZANgBqd9upJCscF0DxwWYN1PcmMzVw6cCWsVjsIj\\\"}', 'provider: Google', '2019-10-14 11:58:09'),
(192, 'L8Axpvmt2OD3KvSg70A2h2NJyFHueDKl', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"fUnHWWWx3aN9D41jvub9VhNKkjsgk1gSUvorwPcgzz8EqwK7iCQUmMD7RHuxSu4w\\\"}', 'provider: Google', '2019-10-14 15:22:28'),
(193, '7o4Oo2nTv3BE8EGzFpvdEZzpJtvR7qDe', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"9maEdTtvg15ovKul3Mf1JthnvBn5dnXE3ekZyNvw1zj7iICuQ54sjcHNwGgiz88x\\\"}', 'provider: Google', '2019-10-14 17:09:40'),
(194, 'H7va1omTeTGwdDxIWsUGxyXFrGP1EMvk', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"I3tGNFpf9Vu6JWo6k9howSZT7AGxeGRGSfWjhQH0xF0KdX1WT1VkC42d9gaqLR0V\\\"}', 'provider: Google', '2019-10-14 17:15:27'),
(195, 'DlneVcjBDC18A7FXmYHwOzOyzNmfiqiz', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', 'Update Account', 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', '{\\\"token\\\":\\\"\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"22ff1eef84242474215726aae3e9b41d\\\"}', '{\\\"token\\\":\\\"KuJRMRYwwzp53dyuqVwMinYKyyoQc7Bl0KgjgNRC5x0LuKkFKwr09Tfb2YJUytCw\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.120 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"17915016d64bb3dafd9a535e1e486887\\\"}', 'provider: Google', '2019-10-14 17:28:43'),
(196, 'SgMzR17KCbHuFgJmPEkZJiEgOYESsNoJ', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"ef13d506def163ad425ec9bfca4ef782\\\"}', '{\\\"token\\\":\\\"p1f5da4J4NgskgJ1PESeDfKy4OVdFPnjMYo55XLZBDVoROZGGGL4KfMiTKRNxX5n\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.120 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"17915016d64bb3dafd9a535e1e486887\\\"}', 'provider: Google', '2019-10-14 17:34:42'),
(197, 'nBluUYeEg2I1wsWFB7c1MsQIfxFJHhvl', 'SYSTEM', 'Create Account', 'rUemiRfRkPmwyIRNTN73NF6WstWILaD5', '', '', 'provider: email', '2019-10-15 15:03:06'),
(198, 'bTVz4oiYDIRdocnDabwGHEj6W33P4KHv', 'rUemiRfRkPmwyIRNTN73NF6WstWILaD5', 'Update Account', 'rUemiRfRkPmwyIRNTN73NF6WstWILaD5', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"9qZQnsbF8qjTsSEYkg8F0gXD3VcD2iYVsaKyP1CtNIS1G8lkUz3QD7DMUFvsat23\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"89629a611709a0a6228d46fcb3adad3a\\\"}', 'provider: email', '2019-10-15 15:03:06'),
(199, 'kshi7Z6IenfBWVprA9qlGSRUm85GioBa', 'rUemiRfRkPmwyIRNTN73NF6WstWILaD5', 'Update Account', 'rUemiRfRkPmwyIRNTN73NF6WstWILaD5', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"j6yDYcqfLbmerWZmAXaVTnIVgA5qmqsyBCLTlGRervdbiy0hG9NnuzwIqXqFaIl9\\\"}', 'provider: email', '2019-10-15 17:39:46'),
(200, 'KEFvfJD8JE3MHUTtmQDC66p0yDMpya8B', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"0vW005NE9ULWBmpUzmDnC8CSH0KW6oBUaLrHerrDWmeKoNI9n0CAC1syGBFUdCGF\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_fingerprint\\\":\\\"86369f87ae4780efc65b94dfe65ce961\\\"}', '{\\\"token\\\":\\\"tHqJCVVKxn8GlFzFN4dYMdQhIcrjVEtWYDas6srpEbsrWYgkkUN4kkO1mhImmzBn\\\",\\\"token_ip\\\":\\\"154.156.129.49\\\",\\\"token_fingerprint\\\":\\\"03aa0955e2b75f58b22d4af68a806299\\\"}', 'provider: Google', '2019-10-15 18:57:17'),
(201, '1EqOMOYuMb7ghaNOJzUCIAcco6yRAxrt', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"8XMAOZB4hYHY9Ms6zgSn7FUYLNLnYyESynXUqGRfeacBgcbCEzaJrj8UWzkuPqOF\\\",\\\"token_ip\\\":\\\"41.90.7.88\\\",\\\"token_fingerprint\\\":\\\"f3a7303be3ffcbde8d642b4ca2755a8a\\\"}', '{\\\"token\\\":\\\"Gha2Z4YESey2p9sRJGnu2BN4RgKzkLe0nlgyQ90vswAg9U3WXs3owILuHE3Osu98\\\",\\\"token_ip\\\":\\\"154.156.129.49\\\",\\\"token_fingerprint\\\":\\\"f9487e24a050b3a9bdcb13b4d50bce6d\\\"}', 'provider: Google', '2019-10-15 19:02:40'),
(202, '2jrvSMAkn955oeacltdGWzrsdiHo7Hvn', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"de65NxnyvVlKqA3o4aiww0aCjWSDWjdul0cw3mykItWni28V92WzxLezjIUnrA3T\\\"}', 'provider: Google', '2019-10-15 19:25:13'),
(203, 'mej5Inuml4mXEgBChheIsOoUnbpLFkPV', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"p1f5da4J4NgskgJ1PESeDfKy4OVdFPnjMYo55XLZBDVoROZGGGL4KfMiTKRNxX5n\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_fingerprint\\\":\\\"17915016d64bb3dafd9a535e1e486887\\\"}', '{\\\"token\\\":\\\"mHZPPobW3ba62CflM7REaI7yVaY1pA2BYPOmm6kEoYXZdT9wnFbgKZyVarUGIpfF\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"e049727127671f2d6414a1b6c130aba5\\\"}', 'provider: Google', '2019-10-15 20:12:09'),
(204, 'amFdosjp4gI4UWw1S6voAgFsVAeR9kEU', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"e049727127671f2d6414a1b6c130aba5\\\"}', '{\\\"token\\\":\\\"u1P2qFfBPZkkp6x3SKqGLLdZeJN74T2SxOg9qouKwLSQZr4vHoNqndmBVetj0ccr\\\",\\\"token_ip\\\":\\\"41.90.4.129\\\",\\\"token_fingerprint\\\":\\\"fde542ae11ae1b34c42d45d3172042ba\\\"}', 'provider: Google', '2019-10-22 18:44:59'),
(205, 'MKUgrUcXoTkwe8wo2IZ6YPqoJlmZUN0X', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"jAa1AuUSna2lpozz1hTnLnEkQKsrEjDQtHvSIsJF0nFH4i2FefiPCSc9oEmW5H8a\\\"}', 'provider: Google', '2019-10-22 18:46:01'),
(206, 'iM3wV5zWFnjWHAOa5a2v1Zlo1k6S4iYw', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"rkSkxpuMCJGslj2EJaliBySrcELqmHpIla2mfIPN48vBIU5TSA1Buw3ybEbSpo93\\\"}', 'provider: Google', '2019-10-22 18:50:32'),
(207, 'PS24hlM2J5ojxuXHOt92R1LHORZDa6OS', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"de65NxnyvVlKqA3o4aiww0aCjWSDWjdul0cw3mykItWni28V92WzxLezjIUnrA3T\\\",\\\"token_ip\\\":\\\"154.156.129.49\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"f9487e24a050b3a9bdcb13b4d50bce6d\\\"}', '{\\\"token\\\":\\\"A8JMqfgFkuyZT3pAqowQaE5MuxvdXdE3VdDmw6wC7ZQ3sHyBjhWL2y6e471RTAIs\\\",\\\"token_ip\\\":\\\"41.90.5.200\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.120 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"69d3d23e52858a33139f9e55812437c0\\\"}', 'provider: Google', '2019-10-22 18:57:18'),
(208, 'cUjJzOeLHf0jItaEKIsY2kMwCDOBtaMO', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"fu1Aw7XegaLZBM6JC8IqYJlz8JBChiev9X5EpYKWkqFRNjQTofCwQOQlGdlZeQj8\\\",\\\"token_ip\\\":\\\"41.80.238.142\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"0bb1b327e480b5e79d054272673a4d0b\\\"}', '{\\\"token\\\":\\\"gVqfhsOlf2oaXR4xIheKz5hMo94t4zqvjLNmpiAkt0WBa5nBIGouyRHQt7Nl9tL7\\\",\\\"token_ip\\\":\\\"196.110.114.23\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.120 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"9d7fa1ab5b61bb6a1cf529b34525f85d\\\"}', 'provider: Google', '2019-10-22 19:00:37'),
(209, '90su6avtUkaMcU0xn419ddGrSwjJzGBh', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"gVqfhsOlf2oaXR4xIheKz5hMo94t4zqvjLNmpiAkt0WBa5nBIGouyRHQt7Nl9tL7\\\",\\\"token_ip\\\":\\\"196.110.114.23\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.120 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"9d7fa1ab5b61bb6a1cf529b34525f85d\\\"}', '{\\\"token\\\":\\\"3o3TiQiuuTadDCKpnbpRDTdAPtr1rfpo3WoDDkPJl0xyMsyjqmN1MZHUJuq3mxio\\\",\\\"token_ip\\\":\\\"105.160.126.205\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.70 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"bbc2e8561511e8098bdbb7df9c857037\\\"}', 'provider: Google', '2019-10-25 06:07:32'),
(210, 'sZSDFxLs0dESrNWSgB9MYW1VtOo6t8dY', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"89629a611709a0a6228d46fcb3adad3a\\\"}', '{\\\"token\\\":\\\"gk7QX7VLxwRqdstQbPmS3mzX7CNjskKTBMBov2XCcX3W4BvybOyIGZdqWDc77VFZ\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.120 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"12060533d4b448f3173001544b505bdb\\\"}', 'provider: Google', '2019-10-25 10:39:33'),
(211, 'PJxGIn0BYPbTLK1SL3qlYlCtHQeARbqm', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"gk7QX7VLxwRqdstQbPmS3mzX7CNjskKTBMBov2XCcX3W4BvybOyIGZdqWDc77VFZ\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_fingerprint\\\":\\\"12060533d4b448f3173001544b505bdb\\\"}', '{\\\"token\\\":\\\"U0dtkDoCM5qOln3W1cAyxvEsLGXv5sFxpRdcDWT5fhMF5uKLzcKlDKw4poaRnJxs\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_fingerprint\\\":\\\"dc8d615caaf928ffe4b6b3c7ce37344d\\\"}', 'provider: Google', '2019-10-26 09:31:50'),
(212, 'Ya2IZRPPC91X8dDLw0vPuSm3Ndrtr2Of', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"rkSkxpuMCJGslj2EJaliBySrcELqmHpIla2mfIPN48vBIU5TSA1Buw3ybEbSpo93\\\",\\\"token_ip\\\":\\\"41.90.4.129\\\",\\\"token_fingerprint\\\":\\\"fde542ae11ae1b34c42d45d3172042ba\\\"}', '{\\\"token\\\":\\\"tclWTGcHiBLFxeFpAHAvgBonYraH4QTpflR50gwBIFoAVsNZEO273WdPjCKw8s7Z\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_fingerprint\\\":\\\"348375673feaa5038927960958dd5064\\\"}', 'provider: Google', '2019-10-26 10:58:01'),
(213, 'MelnPL5MzXLpkimK05DPGOBXXEXH7ObJ', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"tHqJCVVKxn8GlFzFN4dYMdQhIcrjVEtWYDas6srpEbsrWYgkkUN4kkO1mhImmzBn\\\",\\\"token_ip\\\":\\\"154.156.129.49\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"03aa0955e2b75f58b22d4af68a806299\\\"}', '{\\\"token\\\":\\\"s2IEWi4LLyCi0ukOvaVXDmxrnsYE4mwGEk67MbeiUgygt3jMwq0UtnQBrg75PMTf\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.70 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"63b321c00a02315e73b285905430bb76\\\"}', 'provider: Google', '2019-10-26 11:04:50'),
(214, 'BWxCEvYIYMnuRG14egvOJq3bfGwBUyV0', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"A8JMqfgFkuyZT3pAqowQaE5MuxvdXdE3VdDmw6wC7ZQ3sHyBjhWL2y6e471RTAIs\\\",\\\"token_ip\\\":\\\"41.90.5.200\\\",\\\"token_fingerprint\\\":\\\"69d3d23e52858a33139f9e55812437c0\\\"}', '{\\\"token\\\":\\\"HqRZrDFIi5pzqEnHjo2bLMeW5oXQzB9SFZJQDEhfyYGOM5vXmMwK8BAtjK9IwAJQ\\\",\\\"token_ip\\\":\\\"105.230.127.115\\\",\\\"token_fingerprint\\\":\\\"dcaa2a55a0cb55c43567192fbc254fba\\\"}', 'provider: Google', '2019-10-26 15:11:40'),
(215, '3UghS5jkuqquOyRAaslrqlOH7XAHnfvZ', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"3o3TiQiuuTadDCKpnbpRDTdAPtr1rfpo3WoDDkPJl0xyMsyjqmN1MZHUJuq3mxio\\\",\\\"token_ip\\\":\\\"105.160.126.205\\\",\\\"token_fingerprint\\\":\\\"bbc2e8561511e8098bdbb7df9c857037\\\"}', '{\\\"token\\\":\\\"rW7EvsslVGaex3L9Iw0LRm4LQAgJYMC6TLSlV68r5iKNgf5aDTaAmXt6otXobJf0\\\",\\\"token_ip\\\":\\\"197.237.240.217\\\",\\\"token_fingerprint\\\":\\\"5c29642fb1db6960066e851aae06ba92\\\"}', 'provider: Google', '2019-10-26 15:12:37'),
(216, 'B4nWKMSsIaq8qiMzwUnlh7ixd35rHLVb', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"rW7EvsslVGaex3L9Iw0LRm4LQAgJYMC6TLSlV68r5iKNgf5aDTaAmXt6otXobJf0\\\",\\\"token_ip\\\":\\\"197.237.240.217\\\",\\\"token_fingerprint\\\":\\\"5c29642fb1db6960066e851aae06ba92\\\"}', '{\\\"token\\\":\\\"Q0wkjPxBuj6IHY9hGM6ESjcFpCJPI71Ao2FnoDSnZzLGg8TMt5Pdel13CnlzNCNZ\\\",\\\"token_ip\\\":\\\"105.49.105.161\\\",\\\"token_fingerprint\\\":\\\"1893e899f2ca78f0d5abede5b4ba20fc\\\"}', 'provider: Google', '2019-10-29 16:59:49'),
(217, 'uKST9zOhm235w9NseAzFbCYSWCSFcoCg', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"Q0wkjPxBuj6IHY9hGM6ESjcFpCJPI71Ao2FnoDSnZzLGg8TMt5Pdel13CnlzNCNZ\\\",\\\"token_ip\\\":\\\"105.49.105.161\\\",\\\"token_fingerprint\\\":\\\"1893e899f2ca78f0d5abede5b4ba20fc\\\"}', '{\\\"token\\\":\\\"xihUKgsj6FSC56i91mnbXFipI4Y7UYcbuHyHehPjxWI614HMI3lVfgS3nYRUEobb\\\",\\\"token_ip\\\":\\\"197.248.47.186\\\",\\\"token_fingerprint\\\":\\\"d62cc0db73ed32800cd3276cdfa426ae\\\"}', 'provider: Google', '2019-10-29 18:43:35'),
(218, 'mqhm0Sg44786G7OkUdWJ7bV7mmztoBZb', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"xihUKgsj6FSC56i91mnbXFipI4Y7UYcbuHyHehPjxWI614HMI3lVfgS3nYRUEobb\\\",\\\"token_ip\\\":\\\"197.248.47.186\\\",\\\"token_fingerprint\\\":\\\"d62cc0db73ed32800cd3276cdfa426ae\\\"}', '{\\\"token\\\":\\\"3eqksZ5ct2PjCsCAZWI4mjiLFADQsimZZNOpQGjlc1UDUmMp5NZ3rexVP04QVRW3\\\",\\\"token_ip\\\":\\\"197.237.240.217\\\",\\\"token_fingerprint\\\":\\\"5c29642fb1db6960066e851aae06ba92\\\"}', 'provider: Google', '2019-10-30 18:26:27'),
(219, 'dyRAN82MVifvevZGrEFX2YmRSZwwikuP', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"nkXZHUymOp2BM3ZeHzZO7eUqNb5RZvXvLHIRGS36lHAkel0fAWHUDhp5RJBxvGT5\\\"}', 'provider: Google', '2019-10-30 18:33:14'),
(220, 'XJkYsLQQISCZ8gxQCMdgw2FIcpHQgCag', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"U0dtkDoCM5qOln3W1cAyxvEsLGXv5sFxpRdcDWT5fhMF5uKLzcKlDKw4poaRnJxs\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_fingerprint\\\":\\\"dc8d615caaf928ffe4b6b3c7ce37344d\\\"}', '{\\\"token\\\":\\\"qLJwqI818iznA5zGR5C6nQNU5Uh76Jm5mhCEGK5jSWIQpmXP1Le8hU3S5AG61264\\\",\\\"token_ip\\\":\\\"105.49.153.230\\\",\\\"token_fingerprint\\\":\\\"659070f2e45c390e2b6529761bf971cc\\\"}', 'provider: Google', '2019-10-30 18:33:53'),
(221, 'wJNuypBaMgR6Gt8vL8EKixopoBCRPNOf', 's8fhXVWkrlKw90UAFzh2LUu4vDiRNi7a', 'Update Account', 's8fhXVWkrlKw90UAFzh2LUu4vDiRNi7a', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.177.39.124\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/74.0.3729.157 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"444d88701954057978c7ad28c7483c62\\\"}', '{\\\"token\\\":\\\"TjVVW6pIrwzvDACF0B3fNNQ4wBTVYimXLXKeN8ECwXhlxY6eD47vTp0T2Pq4IvbP\\\",\\\"token_ip\\\":\\\"105.49.105.161\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.70 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"1893e899f2ca78f0d5abede5b4ba20fc\\\"}', 'provider: Twitter', '2019-10-30 18:47:58'),
(222, 'MFhwiXX4Jb4KbJSJR7y7sb0KzgFA3v8v', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.237.240.217\\\",\\\"token_fingerprint\\\":\\\"5c29642fb1db6960066e851aae06ba92\\\"}', '{\\\"token\\\":\\\"2Q45uKfGNQs0q6ZsmRRm1ay2mfqdGoCKg27RnypXPse1lvZIy7vwopMfMwoVlAWJ\\\",\\\"token_ip\\\":\\\"105.49.105.161\\\",\\\"token_fingerprint\\\":\\\"1893e899f2ca78f0d5abede5b4ba20fc\\\"}', 'provider: Google', '2019-10-31 00:31:44'),
(223, '4DHgZScxlcGopgPXk5H1SsxTjb5IKEW7', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"s2IEWi4LLyCi0ukOvaVXDmxrnsYE4mwGEk67MbeiUgygt3jMwq0UtnQBrg75PMTf\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.70 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"63b321c00a02315e73b285905430bb76\\\"}', '{\\\"token\\\":\\\"J2sVeMw44cGLCLw59E5MLEk55JspG9SQu5MunrQAxW6RhQcfEeDqkvJcgkj7bXQ9\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.62 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b053eb177f2841a4fa802717e29c738a\\\"}', 'provider: Google', '2019-10-31 18:05:28'),
(224, 'ZPHjFmlHGS5nd2Vh7hoJUodtICuoxDei', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"2Q45uKfGNQs0q6ZsmRRm1ay2mfqdGoCKg27RnypXPse1lvZIy7vwopMfMwoVlAWJ\\\",\\\"token_ip\\\":\\\"105.49.105.161\\\",\\\"token_fingerprint\\\":\\\"1893e899f2ca78f0d5abede5b4ba20fc\\\"}', '{\\\"token\\\":\\\"N7KCkBo10VLa6ieGCawXtLOwqdIrhIkFUWFlRbqWNPPy8UIu3x33szL1GCCWkScs\\\",\\\"token_ip\\\":\\\"41.81.47.168\\\",\\\"token_fingerprint\\\":\\\"afa1fe526b02d7b33defd84522dca5f9\\\"}', 'provider: Google', '2019-11-02 07:01:32'),
(225, 'puLwXoYzlR0FqUbpghvngWnkIPPmgsCL', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"qLJwqI818iznA5zGR5C6nQNU5Uh76Jm5mhCEGK5jSWIQpmXP1Le8hU3S5AG61264\\\",\\\"token_ip\\\":\\\"105.49.153.230\\\",\\\"token_fingerprint\\\":\\\"659070f2e45c390e2b6529761bf971cc\\\"}', '{\\\"token\\\":\\\"QiYVCcQLIgmi6E1CMrCNUzOPUQWLNhzn6YkLHgs5dJJtJLrYTyPSmobratQJhjN6\\\",\\\"token_ip\\\":\\\"154.156.186.77\\\",\\\"token_fingerprint\\\":\\\"d52f29d09048f6b9a3b29fbafc31b96b\\\"}', 'provider: Google', '2019-11-04 22:00:29'),
(226, 'kljhCABuYFeO9XBuZJ1CSGmbQTIbW10T', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"N7KCkBo10VLa6ieGCawXtLOwqdIrhIkFUWFlRbqWNPPy8UIu3x33szL1GCCWkScs\\\",\\\"token_ip\\\":\\\"41.81.47.168\\\",\\\"token_fingerprint\\\":\\\"afa1fe526b02d7b33defd84522dca5f9\\\"}', '{\\\"token\\\":\\\"7lyx04xYuj1Jq61BDH2iQGXXruHsVHRx0pzUnFzzkxUojYBswNktG8A6wFzWSNSl\\\",\\\"token_ip\\\":\\\"105.164.185.111\\\",\\\"token_fingerprint\\\":\\\"39b28f55d38b993d91030c1d9b20cd48\\\"}', 'provider: Google', '2019-11-05 07:19:34'),
(227, 'nfDs5CHruNUWrPbSAFyscP6vNRBcviPB', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"7lyx04xYuj1Jq61BDH2iQGXXruHsVHRx0pzUnFzzkxUojYBswNktG8A6wFzWSNSl\\\",\\\"token_ip\\\":\\\"105.164.185.111\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.70 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"39b28f55d38b993d91030c1d9b20cd48\\\"}', '{\\\"token\\\":\\\"Cu4rdtUSEvmjW0B0PBPeBg0XQ58h7N8r6xxlwW5N0BxZDoxhohXvS6ZADOE9l0MB\\\",\\\"token_ip\\\":\\\"197.237.240.217\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"2eef76ee560fcb20f7a2338df89598db\\\"}', 'provider: Google', '2019-11-06 13:35:06'),
(228, 'LVRzMK4DQyvHSDxhAImlZLvPnTbNyrur', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"tclWTGcHiBLFxeFpAHAvgBonYraH4QTpflR50gwBIFoAVsNZEO273WdPjCKw8s7Z\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.120 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"348375673feaa5038927960958dd5064\\\"}', '{\\\"token\\\":\\\"1jFqtoHSJVd8JwBzgsAlnSq67usarMuhSQTFDONOP1VUCHvtVw8HO8Kc9lnk0Kel\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.70 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"40a5b05d8fcfa447bc95217fcd9ae0a9\\\"}', 'provider: Google', '2019-11-08 08:28:12'),
(229, 'tlE1AAIDN64kThK0P34hBLBmaGPuzHfu', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"J2sVeMw44cGLCLw59E5MLEk55JspG9SQu5MunrQAxW6RhQcfEeDqkvJcgkj7bXQ9\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.62 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b053eb177f2841a4fa802717e29c738a\\\"}', '{\\\"token\\\":\\\"clK09wx6BZsUP6z8ItPusRmpBFXK5N0cnQhOTVqgMNuCmaiysOrmCJ7PyGbakE5b\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"af1a4debc4ef817dcea8ae99df8d321d\\\"}', 'provider: Google', '2019-11-09 09:03:06'),
(230, 'ML8Kz7LH1JYKlXY98o6gAEmGRBHRGmuC', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"1jFqtoHSJVd8JwBzgsAlnSq67usarMuhSQTFDONOP1VUCHvtVw8HO8Kc9lnk0Kel\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_fingerprint\\\":\\\"40a5b05d8fcfa447bc95217fcd9ae0a9\\\"}', '{\\\"token\\\":\\\"5CVeF9GEI8pMEHqQ5KKuBOXo2756nVhcxYRytIMWGMieZOmC73S6nnee1CXN3Xrj\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_fingerprint\\\":\\\"8cca140b265d9ab03c2cdfc8c356297e\\\"}', 'provider: Google', '2019-11-09 09:07:52'),
(231, 'sjuk2Qy84G57azZuAwrIpU9WgnYJ0zXC', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"HqRZrDFIi5pzqEnHjo2bLMeW5oXQzB9SFZJQDEhfyYGOM5vXmMwK8BAtjK9IwAJQ\\\",\\\"token_ip\\\":\\\"105.230.127.115\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.120 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"dcaa2a55a0cb55c43567192fbc254fba\\\"}', '{\\\"token\\\":\\\"6VPoHZM4vMX83FtBZvP0esAULmoETtcWYk6VkWFJ7ig0AB8mjKDaFgZ8cfmwDLaY\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.97 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7088ba0def984cb91aea3abde94bc9cd\\\"}', 'provider: Google', '2019-11-09 09:17:59'),
(232, 'lOxc9CAFZFIo1Ts2mTNy3VpT028AV9n3', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"clK09wx6BZsUP6z8ItPusRmpBFXK5N0cnQhOTVqgMNuCmaiysOrmCJ7PyGbakE5b\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"af1a4debc4ef817dcea8ae99df8d321d\\\"}', '{\\\"token\\\":\\\"jy6D1EaKaV4yBwNkVxezEBwW2fONMq8k73U9xNMdWjUYYW8mu4Kk615Xi5UE0WOl\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.90 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"99b3c2a4fb1e81a2a0ce21e338b68924\\\"}', 'provider: Google', '2019-11-09 09:35:44'),
(233, 'MJrlysiAky7wCHDm2RM7tLBinWMKuLaa', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"QiYVCcQLIgmi6E1CMrCNUzOPUQWLNhzn6YkLHgs5dJJtJLrYTyPSmobratQJhjN6\\\",\\\"token_ip\\\":\\\"154.156.186.77\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.120 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"d52f29d09048f6b9a3b29fbafc31b96b\\\"}', '{\\\"token\\\":\\\"EY2xcKvyUsh0aXIEuJnQq3axRbNpRlEGlBZn8Lwbt1lJmYOkaDBz1KAifhp67R85\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"206e52273a2081459c998d9bdc97eed9\\\"}', 'provider: Google', '2019-11-09 09:36:01'),
(234, 'fF4xoD1IIXqBRE95eZFSylGAhQblaqm8', 'SYSTEM', 'Create Account', 'PYBYvuf80nYCwn4Wd42QWnwRsWFhjqpS', '', '', 'provider: email', '2019-11-09 10:02:35'),
(235, 'PDyDgqgjb59O4K0p3ZiLokCvphsaVDEw', 'PYBYvuf80nYCwn4Wd42QWnwRsWFhjqpS', 'Update Account', 'PYBYvuf80nYCwn4Wd42QWnwRsWFhjqpS', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"OrddurteVkkE4ax4vQsCsHAonpga1G5iZdAx5UN2pSqSRibWmvooF2ODUwATvbjx\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.97 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7088ba0def984cb91aea3abde94bc9cd\\\"}', 'provider: email', '2019-11-09 10:02:35'),
(236, 'Ioc5W1x6uMtwrtb7zVTJv4M0GGr8dhN1', 'fAEryS4d8g6uIgdYpF6LSPTWeSIwossh', 'Update Account', 'fAEryS4d8g6uIgdYpF6LSPTWeSIwossh', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"i4EpftT9SAIcheV9wFZ2LhTZWAtMtOpHTjEQmRVmWWb76z9sWK8pzcB6D5mf8esU\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"af1a4debc4ef817dcea8ae99df8d321d\\\"}', 'provider: Google', '2019-11-09 10:07:30'),
(237, 'EM51VqmvfoD460H9mlEmIUAZprRDn55l', 'SYSTEM', 'Create Account', 'EE755TUD404ZFcCOfGIJcrhTJ9Wp1dl8', '', '', 'provider: email', '2019-11-09 10:11:18'),
(238, 'CGmyUIJ3tjWWZ0KYgys28rgt8xxiqstO', 'EE755TUD404ZFcCOfGIJcrhTJ9Wp1dl8', 'Update Account', 'EE755TUD404ZFcCOfGIJcrhTJ9Wp1dl8', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"G8fEnIqQJQsSBjXTEO8Lxm0y6bE4nrEWPPakGhDm6jyTSgeYRBUD0nFP8s82hg7y\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"206e52273a2081459c998d9bdc97eed9\\\"}', 'provider: email', '2019-11-09 10:11:18'),
(239, 'zFNNMklg6QI6ZxwbyL1fn6S8ArayEva8', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"4VLeyqpcoRLYSq6W3ZghsjKsvJIsAMFLbB5JuvQ0h6pz8Lu9T59HPVsidqQKggXy\\\"}', 'provider: Google', '2019-11-09 10:12:17'),
(240, 'DQrZZnEZ0enXlniEfA9t3ra62mE3uiIy', 'fAEryS4d8g6uIgdYpF6LSPTWeSIwossh', 'Update Account', 'fAEryS4d8g6uIgdYpF6LSPTWeSIwossh', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"J7HZQAHRUtDHbTxYWlUgPzfC7mmKKfrPjbO41vUXzzffdLzdjfxf1m1NMOOiadwE\\\"}', 'provider: Google', '2019-11-09 10:50:47'),
(241, 'QJDd5ErHdPrqTifMT47eMRHxp54zb8XI', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"jy6D1EaKaV4yBwNkVxezEBwW2fONMq8k73U9xNMdWjUYYW8mu4Kk615Xi5UE0WOl\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.90 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"99b3c2a4fb1e81a2a0ce21e338b68924\\\"}', '{\\\"token\\\":\\\"k6r6q20qjo6wc9auMNMRJcKXgifEFxqbdRJQHUUyI1KrKxUfGfzhcceWPRZAU7lM\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"af1a4debc4ef817dcea8ae99df8d321d\\\"}', 'provider: Google', '2019-11-09 10:53:26'),
(242, 'QJxr51uDAhU4GijAIqZudhT3sDmv9lxP', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"ixrdlU78i86I2lcMcDoT73N8JthIWHUiebnRp5PucKubQhhUPiWuH8EltFHDmA9z\\\"}', 'provider: Google', '2019-11-09 12:28:43'),
(243, 'lB3QJ1oIB68aQXXOw3kjToUj08jQoV5P', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"Cu4rdtUSEvmjW0B0PBPeBg0XQ58h7N8r6xxlwW5N0BxZDoxhohXvS6ZADOE9l0MB\\\",\\\"token_ip\\\":\\\"197.237.240.217\\\",\\\"token_fingerprint\\\":\\\"2eef76ee560fcb20f7a2338df89598db\\\"}', '{\\\"token\\\":\\\"6LVzBNRjYNqLysPlL6OFbDHJNWmvpHg09g5aiUkOMhanj8Q3B3ReTT4TUcOBGGke\\\",\\\"token_ip\\\":\\\"41.81.75.137\\\",\\\"token_fingerprint\\\":\\\"cfab949f2431fd341afa027cc721f277\\\"}', 'provider: Google', '2019-11-12 08:30:44'),
(244, '453nSgzXaOXkLUHuxZ5U4mIbdlqXuvCb', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"ixrdlU78i86I2lcMcDoT73N8JthIWHUiebnRp5PucKubQhhUPiWuH8EltFHDmA9z\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_fingerprint\\\":\\\"206e52273a2081459c998d9bdc97eed9\\\"}', '{\\\"token\\\":\\\"j3v2QfNm3Dl1zNbFkHMUZgY7J94oga4NGQwQvTJxyuw2I0fOWaq9BuZduWUfbXxr\\\",\\\"token_ip\\\":\\\"154.156.250.123\\\",\\\"token_fingerprint\\\":\\\"385ffab5bcdb5c09e315ea1f162b801d\\\"}', 'provider: Google', '2019-11-12 10:36:47'),
(245, 'SxIK0JAaiyV9ij6XqSJC6AsHWPM5GEA1', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"af1a4debc4ef817dcea8ae99df8d321d\\\"}', '{\\\"token\\\":\\\"lsTXlCCMKMzZmeEHwhdSgNxeZWMrgXqgqrS1kUSZVGHsc7O5DzGE2rU5kgJ33rxE\\\",\\\"token_ip\\\":\\\"154.152.245.63\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"53bde14ecfb0a28a9483c7d5db2a22eb\\\"}', 'provider: Google', '2019-11-13 12:02:50'),
(246, 's0YGho7cExTdV1ya1bXHEkHhDDvt4eqz', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"j3v2QfNm3Dl1zNbFkHMUZgY7J94oga4NGQwQvTJxyuw2I0fOWaq9BuZduWUfbXxr\\\",\\\"token_ip\\\":\\\"154.156.250.123\\\",\\\"token_fingerprint\\\":\\\"385ffab5bcdb5c09e315ea1f162b801d\\\"}', '{\\\"token\\\":\\\"uHU1muT1A5umPzAw5ZHlayNueUkJzVUeul2uthY9gmtLTokZdfvdG9XzEaaaSkky\\\",\\\"token_ip\\\":\\\"154.76.39.47\\\",\\\"token_fingerprint\\\":\\\"b1b595420647aff30c0857bdc880f24e\\\"}', 'provider: Google', '2019-11-13 15:24:55'),
(247, '8hBNefrjBz1F58YzaGPvmlH2Y3YNLMpG', 'SYSTEM', 'Create Account', '0TxVWeJoWOWP8j0CdWaBQaT7tcHJI7l0', '', '', 'provider: Facebook', '2019-11-25 01:32:48'),
(248, 'sdfgC7fCeZNbWuPsKioEUoPd45e4wzc1', 'SYSTEM', 'Create Account', 'D60rgto6HO1y1WuQZG3khhHomh2uxMcL', '', '', 'provider: Facebook', '2019-11-25 01:32:54'),
(249, 'LmWxeGUtLTyXji3YlwBOAO8bs1LirJzk', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'Update Account', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', '{\\\"token\\\":\\\"jMEsgV7mMcfnaCfXA3N3V89h53YuiCrTJrO0PMzrxeYUNToSpMhD1fzf8XXdNp3p\\\",\\\"token_ip\\\":\\\"41.80.130.114\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/77.0.3865.90 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"9c1a0aa084caa1eaab9af1df126319d3\\\"}', '{\\\"token\\\":\\\"ZZwPxnbPNovk7MUYmkggrkZqC9NvwBBZlMgRi7KsSKMWNU7reUG9nZQSgrNjR29P\\\",\\\"token_ip\\\":\\\"197.237.17.121\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"acad62e376e483cad2e65144c57e2571\\\"}', 'provider: Google', '2019-11-25 01:33:04'),
(250, '7m4dzlotsX1AuTkRz39Br9pq92Bd3qMB', 'SYSTEM', 'Create Account', 'r7CbOAhAmui3jN3sNejrWpi3salqTAfc', '', '', 'provider: Facebook', '2019-11-26 12:47:26'),
(251, 'wuFVG5jgOOPNoJisBQkF5tztX44Y5NKj', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"6LVzBNRjYNqLysPlL6OFbDHJNWmvpHg09g5aiUkOMhanj8Q3B3ReTT4TUcOBGGke\\\",\\\"token_ip\\\":\\\"41.81.75.137\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"cfab949f2431fd341afa027cc721f277\\\"}', '{\\\"token\\\":\\\"e5xOggHZ80iMDvmXiaAwaGCNnUxybrOQcUUwRcvmAm78fFeeazr3hGnS1GdtUDaC\\\",\\\"token_ip\\\":\\\"196.97.38.130\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"84cd8e5b27f8500f6efa9fc0207d1190\\\"}', 'provider: Google', '2019-11-26 12:47:35'),
(252, '8zKHcJ2wuY7wwLcKNAussGUZTcUKH4D6', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"e5xOggHZ80iMDvmXiaAwaGCNnUxybrOQcUUwRcvmAm78fFeeazr3hGnS1GdtUDaC\\\",\\\"token_ip\\\":\\\"196.97.38.130\\\",\\\"token_fingerprint\\\":\\\"84cd8e5b27f8500f6efa9fc0207d1190\\\"}', '{\\\"token\\\":\\\"ldKxPyTV4MvfaYdFgyy8v4ECIJ2ZdAzltkfD6EM2jU6oZT0VwDV1m2Jb9hgl7eOq\\\",\\\"token_ip\\\":\\\"41.212.108.210\\\",\\\"token_fingerprint\\\":\\\"ae6f5d4ddf98ee25be930d0d9b1aff4d\\\"}', 'provider: Google', '2019-11-29 09:27:40'),
(253, 'BXhidN6pkR1fpvCAFm7BiHJa7GLiEnot', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"ldKxPyTV4MvfaYdFgyy8v4ECIJ2ZdAzltkfD6EM2jU6oZT0VwDV1m2Jb9hgl7eOq\\\",\\\"token_ip\\\":\\\"41.212.108.210\\\",\\\"token_fingerprint\\\":\\\"ae6f5d4ddf98ee25be930d0d9b1aff4d\\\"}', '{\\\"token\\\":\\\"hyksFUtBzOyDF4dwrgh79gEP0avrVnIOQeqhNrjHPuhQ8sozM4aYZ0p1krpnuI6N\\\",\\\"token_ip\\\":\\\"41.81.65.26\\\",\\\"token_fingerprint\\\":\\\"d8dcccb2cb91d602340278d34fd95234\\\"}', 'provider: Google', '2019-12-01 10:26:17'),
(254, 'l6nfmpTIkVIhBXIpU36gagSKwj3zGiaG', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"hyksFUtBzOyDF4dwrgh79gEP0avrVnIOQeqhNrjHPuhQ8sozM4aYZ0p1krpnuI6N\\\",\\\"token_ip\\\":\\\"41.81.65.26\\\",\\\"token_fingerprint\\\":\\\"d8dcccb2cb91d602340278d34fd95234\\\"}', '{\\\"token\\\":\\\"9TKi8hcHnH20SgYIGgKRQDibiivgCGTa8QUeTkfToT5KzlZXItSJ0Jwx7c7m8Beg\\\",\\\"token_ip\\\":\\\"41.212.108.210\\\",\\\"token_fingerprint\\\":\\\"ae6f5d4ddf98ee25be930d0d9b1aff4d\\\"}', 'provider: Google', '2019-12-03 05:27:55'),
(255, 'pihzoz7Bnl5YyNKA7027P0JxAJQAAGCm', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"lsTXlCCMKMzZmeEHwhdSgNxeZWMrgXqgqrS1kUSZVGHsc7O5DzGE2rU5kgJ33rxE\\\",\\\"token_ip\\\":\\\"154.152.245.63\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"53bde14ecfb0a28a9483c7d5db2a22eb\\\"}', '{\\\"token\\\":\\\"vy4ZBIcon3Ir9k4SYb0WmbmfKxn8AS5lrsITYDNYX3JHV0RWe97p8a04qxm9QtXm\\\",\\\"token_ip\\\":\\\"196.26.223.42\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"dc72cd80cf64170cc85b355c2faba354\\\"}', 'provider: Google', '2019-12-03 08:33:52'),
(256, 'd52KGy7Fh5BgNBsNNqlRQT0xR3Fys2lH', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"4VLeyqpcoRLYSq6W3ZghsjKsvJIsAMFLbB5JuvQ0h6pz8Lu9T59HPVsidqQKggXy\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.70 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"8cca140b265d9ab03c2cdfc8c356297e\\\"}', '{\\\"token\\\":\\\"TinuZU1gWdrPeuUi7oNrzVCAuaR3Nu8zOAk0sdbZHq96tIbAy6fSQq5DlomzLq8p\\\",\\\"token_ip\\\":\\\"196.201.218.20\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7cbd6c306ede2db4d6b73971ae1fdadc\\\"}', 'provider: Google', '2019-12-05 18:41:03'),
(257, 'xBMqAv8G9DzWmtYpbIoMWTvZHFWVZntr', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"vy4ZBIcon3Ir9k4SYb0WmbmfKxn8AS5lrsITYDNYX3JHV0RWe97p8a04qxm9QtXm\\\",\\\"token_ip\\\":\\\"196.26.223.42\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"dc72cd80cf64170cc85b355c2faba354\\\"}', '{\\\"token\\\":\\\"ejqjHTbWYwBqFONi3dm2GvfdkjUqudoycVWhRcoIKoOP8G92G5pO8APy9dp6Wv3R\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"c8162bcc462268df3ef612efd3dafcee\\\"}', 'provider: Google', '2019-12-07 00:07:53'),
(258, 'bMXhx6k6khG2w0igL6psCvMB97gn9nuU', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"ejqjHTbWYwBqFONi3dm2GvfdkjUqudoycVWhRcoIKoOP8G92G5pO8APy9dp6Wv3R\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"c8162bcc462268df3ef612efd3dafcee\\\"}', '{\\\"token\\\":\\\"sAMDbqi6pMOZyCQKvC0LMPFCbRNMxgtzST1vKeGlm4wfQeuTYaT1wQTC3M5BSvP9\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"25866cd457b4db132094fd28992c5ee8\\\"}', 'provider: Google', '2019-12-10 12:43:18'),
(259, '2hTnSVuq3K8F5T3f60722qHlNPSIz1D0', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"sAMDbqi6pMOZyCQKvC0LMPFCbRNMxgtzST1vKeGlm4wfQeuTYaT1wQTC3M5BSvP9\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"25866cd457b4db132094fd28992c5ee8\\\"}', '{\\\"token\\\":\\\"V5aSUpe3gko0LymOxdaVCSryf6qpH5UrPa8Kk5PLizJewwWN8k3QrQjQ2RclwVtx\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"c8162bcc462268df3ef612efd3dafcee\\\"}', 'provider: Google', '2019-12-13 19:37:32'),
(260, 'ffPsuSMuDoT8tpEE3a6WXSYmWOjfZqSm', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"9TKi8hcHnH20SgYIGgKRQDibiivgCGTa8QUeTkfToT5KzlZXItSJ0Jwx7c7m8Beg\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"ae6f5d4ddf98ee25be930d0d9b1aff4d\\\"}', '{\\\"token\\\":\\\"5BznrfnF8AoiXsGdeqblpS3gSWy0beAZ8rdDNP653Lu2Q7368NXHkDFbrlDGdXdn\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.88 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"f8b8257d018ddfa94ef3936ca20351cd\\\"}', 'provider: Google', '2019-12-23 20:52:02'),
(261, 'GIUKwxo9cC122IaSGQxCAsCQH9wdL3oW', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"uHU1muT1A5umPzAw5ZHlayNueUkJzVUeul2uthY9gmtLTokZdfvdG9XzEaaaSkky\\\",\\\"token_ip\\\":\\\"154.76.39.47\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.87 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b1b595420647aff30c0857bdc880f24e\\\"}', '{\\\"token\\\":\\\"Qa0ATESVodY6Bj8Q49AAx1210MDyfQjWwVj81tOyb7gNS63msTgHGltQyjo1kIcT\\\",\\\"token_ip\\\":\\\"102.7.246.71\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"84879cc6dedc3c10860d53484e1a20f0\\\"}', 'provider: Google', '2019-12-24 06:56:33'),
(262, 'Rj78l9mmh5zB6Hu7hsT06dVAK6vhl1H7', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"V5aSUpe3gko0LymOxdaVCSryf6qpH5UrPa8Kk5PLizJewwWN8k3QrQjQ2RclwVtx\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"c8162bcc462268df3ef612efd3dafcee\\\"}', '{\\\"token\\\":\\\"PQj6qasFzywDdpggkCeKe0Ey7pKeIIrlVVbeht4z8usP8WBvQ81WYYBrWKihSJww\\\",\\\"token_ip\\\":\\\"154.152.42.222\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.93 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"e270140c2f36c5aad9af87ce003aed46\\\"}', 'provider: Google', '2019-12-28 13:32:11'),
(263, 'FhUJ6jeO6pONAbdnSww6IkfnCYiBSKJF', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"TinuZU1gWdrPeuUi7oNrzVCAuaR3Nu8zOAk0sdbZHq96tIbAy6fSQq5DlomzLq8p\\\",\\\"token_ip\\\":\\\"196.201.218.20\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7cbd6c306ede2db4d6b73971ae1fdadc\\\"}', '{\\\"token\\\":\\\"QmnHjQ5pyWFoQqZSR7wUk1ygmP95I7gZu5H7CzcQpIxvJTtv69pc71RkRrP0GOMa\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.117 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"0827fbd2370d420b25b8271f6dfdde21\\\"}', 'provider: Google', '2020-01-11 16:16:57'),
(264, 'OwaHTEcgFMsujsChZIkMquGOk7gLELYU', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"PQj6qasFzywDdpggkCeKe0Ey7pKeIIrlVVbeht4z8usP8WBvQ81WYYBrWKihSJww\\\",\\\"token_ip\\\":\\\"154.152.42.222\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.93 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"e270140c2f36c5aad9af87ce003aed46\\\"}', '{\\\"token\\\":\\\"IB4YUAU6XDBOYn8qwh2TfbJR4dMJZd755DXHAVJdYYGW40QUP1e6Y5yZWmflOPX4\\\",\\\"token_ip\\\":\\\"154.153.134.114\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.117 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"6338724bd99444316f1b58545aec014d\\\"}', 'provider: Google', '2020-01-13 18:26:14'),
(265, 'ZAKIpwCs0OD9zLHIIRfVFEqsFju0Scg0', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"6VPoHZM4vMX83FtBZvP0esAULmoETtcWYk6VkWFJ7ig0AB8mjKDaFgZ8cfmwDLaY\\\",\\\"token_ip\\\":\\\"197.232.43.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.97 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7088ba0def984cb91aea3abde94bc9cd\\\"}', '{\\\"token\\\":\\\"6gZXpq2nVZ7pjxCHM72Fh96xO2dacQJQxpZauKbDgamcuLtxI6vQSF4ZkrVkQl18\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.117 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"dd9b6b4e228dc1d4674f7477a7d67d2b\\\"}', 'provider: Google', '2020-01-13 18:32:27'),
(266, 'OUHZBvJFLKLZLugyUG3ErMD6qB3BA2xC', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"Qa0ATESVodY6Bj8Q49AAx1210MDyfQjWwVj81tOyb7gNS63msTgHGltQyjo1kIcT\\\",\\\"token_ip\\\":\\\"102.7.246.71\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/78.0.3904.108 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"84879cc6dedc3c10860d53484e1a20f0\\\"}', '{\\\"token\\\":\\\"YDLCQceqbnJ7bk62MH8wwbF4LwbPRa2YFykpxt9CJIxSYLelZ4P12ml5Mcr9cJ7y\\\",\\\"token_ip\\\":\\\"154.159.82.193\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.88 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b1b6a769d9e721a95829ed9b200595d9\\\"}', 'provider: Google', '2020-01-15 17:25:27'),
(267, 'BC5zyRuIQ5cj5S1RBTLTk7SOTK1oUiYB', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"YDLCQceqbnJ7bk62MH8wwbF4LwbPRa2YFykpxt9CJIxSYLelZ4P12ml5Mcr9cJ7y\\\",\\\"token_ip\\\":\\\"154.159.82.193\\\",\\\"token_fingerprint\\\":\\\"b1b6a769d9e721a95829ed9b200595d9\\\"}', '{\\\"token\\\":\\\"94UNAZEWZ75COJ3yfxCpUUuiMwYtCgUF8ZJeYshbySouycgWfuit8nxdMyWiAhGo\\\",\\\"token_ip\\\":\\\"105.231.94.97\\\",\\\"token_fingerprint\\\":\\\"6d823970c876b2f66c1d8a66fe6508db\\\"}', 'provider: Google', '2020-01-16 20:42:47'),
(268, 'f3j917zFDHFnyNzohzbQJW2YAA5tHeil', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"94UNAZEWZ75COJ3yfxCpUUuiMwYtCgUF8ZJeYshbySouycgWfuit8nxdMyWiAhGo\\\",\\\"token_ip\\\":\\\"105.231.94.97\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.88 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"6d823970c876b2f66c1d8a66fe6508db\\\"}', '{\\\"token\\\":\\\"XIP5TbcCpdV6h7y3v7whGMea68EJDj9gwHYy84P4eMQTuI4YPSlQ2d494ZQavOcG\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.117 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"a770ef3f1aaff0536586359b8d6ef8f4\\\"}', 'provider: Google', '2020-01-21 18:11:50'),
(269, 'rTAJWYa6VBNuLCvkic5epgNGuDzQp1qW', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"QmnHjQ5pyWFoQqZSR7wUk1ygmP95I7gZu5H7CzcQpIxvJTtv69pc71RkRrP0GOMa\\\",\\\"token_ip\\\":\\\"212.49.69.83\\\",\\\"token_fingerprint\\\":\\\"0827fbd2370d420b25b8271f6dfdde21\\\"}', '{\\\"token\\\":\\\"owrh6aIDCK5FTfwDuV9gx5zxYtJ6n36TRJ9xI81skAIKot8UyRnzSkobRIm5IKEx\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"9b0e53769a092d0937a2629d352a710c\\\"}', 'provider: Google', '2020-01-21 18:17:03');
INSERT INTO `ns_audit` (`_index`, `id`, `user`, `action`, `identifier`, `edit_from`, `edit_to`, `notes`, `timestamp`) VALUES
(270, 'ga1ihhY8pYGW0P9zQ87dG2ORDjo5Goms', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"6gZXpq2nVZ7pjxCHM72Fh96xO2dacQJQxpZauKbDgamcuLtxI6vQSF4ZkrVkQl18\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.117 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"dd9b6b4e228dc1d4674f7477a7d67d2b\\\"}', '{\\\"token\\\":\\\"V9C3XBmWcPdWcxGRDHdpjn5OzU2QtDvlyv9uh2o3IGdPSonto4ILp8Y6Y44lpQPE\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.130 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"89d30c6cc87842257779e90d8487f1c2\\\"}', 'provider: Google', '2020-01-21 18:44:32'),
(271, 'wZgfPfYg1669kWw56UwfD7z0OuzfORDu', 'SYSTEM', 'Create Account', '40RNorzfIWiKKdTeqH5HXzcqTWxdZAIL', '', '', 'provider: email', '2020-01-21 19:02:44'),
(272, 'LNCYOzjwqv8dl6yD6h3PLK68guG12C71', '40RNorzfIWiKKdTeqH5HXzcqTWxdZAIL', 'Update Account', '40RNorzfIWiKKdTeqH5HXzcqTWxdZAIL', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"JudB5xKZpoG5bORhAd2gnk4Nv5o6EGRShCWnOqCbpmea7awgXOr00c8QP3EQPZu6\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.117 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"a770ef3f1aaff0536586359b8d6ef8f4\\\"}', 'provider: email', '2020-01-21 19:02:44'),
(273, 'sCrru8NJpPjWTveiTOizHuCda2iyNBDr', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"a770ef3f1aaff0536586359b8d6ef8f4\\\"}', '{\\\"token\\\":\\\"5mlojvmkPPN4RLAnwv23i2bldmkJZhPfnyYQEari7BIqkmP8b6TVQAezIP3Gb8Dt\\\",\\\"token_ip\\\":\\\"105.230.122.182\\\",\\\"token_fingerprint\\\":\\\"54490a5f8ae381649a89904b37c6f039\\\"}', 'provider: Google', '2020-01-23 05:44:42'),
(274, 'ti7jnjnuclEbV8PjP75vv7T1jW2EyFVK', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"5mlojvmkPPN4RLAnwv23i2bldmkJZhPfnyYQEari7BIqkmP8b6TVQAezIP3Gb8Dt\\\",\\\"token_ip\\\":\\\"105.230.122.182\\\",\\\"token_fingerprint\\\":\\\"54490a5f8ae381649a89904b37c6f039\\\"}', '{\\\"token\\\":\\\"KJmjWO5O0hitKuh60QEDSgeBITaWRk970wwFGys1ufwA43wORywlD4ZdoHnexUX2\\\",\\\"token_ip\\\":\\\"154.156.202.170\\\",\\\"token_fingerprint\\\":\\\"88237173b506d2c6c49d83b475f90dc4\\\"}', 'provider: Google', '2020-01-25 22:55:45'),
(275, 'eBUaAHOpOnvRgidwmo3u3pFWAmaDkgaP', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"owrh6aIDCK5FTfwDuV9gx5zxYtJ6n36TRJ9xI81skAIKot8UyRnzSkobRIm5IKEx\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_fingerprint\\\":\\\"9b0e53769a092d0937a2629d352a710c\\\"}', '{\\\"token\\\":\\\"aWRWgMGd5tfZWdEptHuuP99rnuxA6eEqoitLmUCwFbn4Ao2nalAoJMSytakUIkq2\\\",\\\"token_ip\\\":\\\"41.90.5.99\\\",\\\"token_fingerprint\\\":\\\"d66fd80e3a5981d6e66671af058ddd51\\\"}', 'provider: Google', '2020-02-12 12:34:23'),
(276, 'v1wLXqUR0lmDotT6Q9tpP0Is3TgDcDmg', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"5BznrfnF8AoiXsGdeqblpS3gSWy0beAZ8rdDNP653Lu2Q7368NXHkDFbrlDGdXdn\\\",\\\"token_ip\\\":\\\"41.212.108.210\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.88 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"f8b8257d018ddfa94ef3936ca20351cd\\\"}', '{\\\"token\\\":\\\"9z9wTyc80nTDHtfUGrNd5PWsJcR8nKGpY3P7AdcV4Qqje5byI4ClrsNbDGR7h6W5\\\",\\\"token_ip\\\":\\\"197.237.22.154\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.116 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"0464a80f8e94ba0d541232966d5011af\\\"}', 'provider: Google', '2020-02-24 16:18:21'),
(277, 'cE4kZIcG7iBjjaMuG996YLRd488LMie2', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"IB4YUAU6XDBOYn8qwh2TfbJR4dMJZd755DXHAVJdYYGW40QUP1e6Y5yZWmflOPX4\\\",\\\"token_ip\\\":\\\"154.153.134.114\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.117 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"6338724bd99444316f1b58545aec014d\\\"}', '{\\\"token\\\":\\\"pGrZNTs4uJ1nyyM6LLKeb16L6kloPj3fyk5z4hGtyo1LPWviZZG7RBwgqo0Rz78B\\\",\\\"token_ip\\\":\\\"41.80.126.54\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.132 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"f4e03ad87f9a66661a2a1177041d3271\\\"}', 'provider: Google', '2020-03-06 09:29:25'),
(278, 'UzOk0RsI5tVQ9Vbe9jENPqPimB4BkYjV', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"aWRWgMGd5tfZWdEptHuuP99rnuxA6eEqoitLmUCwFbn4Ao2nalAoJMSytakUIkq2\\\",\\\"token_ip\\\":\\\"41.90.5.99\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.117 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"d66fd80e3a5981d6e66671af058ddd51\\\"}', '{\\\"token\\\":\\\"LpmtdPv9RrFOe8t1yWYImrtd3u5DntRmEEa8OtkY0hgGeNfRqN0Y9DWtSx24FitW\\\",\\\"token_ip\\\":\\\"41.80.73.102\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.122 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7b3648857b95512332f927f95e2fb643\\\"}', 'provider: Google', '2020-03-06 09:29:26'),
(279, '0TZck6cnawgYj5yaE4phRy11tNL0cjZ8', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"pGrZNTs4uJ1nyyM6LLKeb16L6kloPj3fyk5z4hGtyo1LPWviZZG7RBwgqo0Rz78B\\\",\\\"token_ip\\\":\\\"41.80.126.54\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 9; SM-A705FN) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.132 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"f4e03ad87f9a66661a2a1177041d3271\\\"}', '{\\\"token\\\":\\\"I8j9ll7lOUWjZLPQp2brwXx82clOKslXxvSGlkUEL07hQV3wClESRXJwXwJ3TJnY\\\",\\\"token_ip\\\":\\\"154.152.165.48\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"51b7ba3d3324227ad3e7777c7e02a71e\\\"}', 'provider: Google', '2020-03-10 18:36:55'),
(280, 'nf6dMv7EDnCjkqfLBZPNxsfq2lpCs1Ji', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"KJmjWO5O0hitKuh60QEDSgeBITaWRk970wwFGys1ufwA43wORywlD4ZdoHnexUX2\\\",\\\"token_ip\\\":\\\"154.156.202.170\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.117 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"88237173b506d2c6c49d83b475f90dc4\\\"}', '{\\\"token\\\":\\\"OAKq5AIobTL1JOhtpxjTJuLuE9wGAOys6zDOuGHyTsGwaoDQEN4eP5MmOMODoqgf\\\",\\\"token_ip\\\":\\\"154.157.214.125\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b68d44ce211efeed2619611d546ee187\\\"}', 'provider: Google', '2020-03-12 22:35:39'),
(281, '7zvA5grDyCpWt2JtAZwJga2PukyPu4r0', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"OAKq5AIobTL1JOhtpxjTJuLuE9wGAOys6zDOuGHyTsGwaoDQEN4eP5MmOMODoqgf\\\",\\\"token_ip\\\":\\\"154.157.214.125\\\",\\\"token_fingerprint\\\":\\\"b68d44ce211efeed2619611d546ee187\\\"}', '{\\\"token\\\":\\\"OrQSZ4qjWfUhBPN3EuWvUWq5MUU2RPnSrP79VueaKNgx1EhO5XowvJVj3kzlCQK1\\\",\\\"token_ip\\\":\\\"154.153.129.240\\\",\\\"token_fingerprint\\\":\\\"b21c316addece73424058ef4fe9390fe\\\"}', 'provider: Google', '2020-03-15 11:41:32'),
(282, 'tjj6aYKqNr6qar8rLpYDRVPTwz4OtAZo', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"OrQSZ4qjWfUhBPN3EuWvUWq5MUU2RPnSrP79VueaKNgx1EhO5XowvJVj3kzlCQK1\\\",\\\"token_ip\\\":\\\"154.153.129.240\\\",\\\"token_fingerprint\\\":\\\"b21c316addece73424058ef4fe9390fe\\\"}', '{\\\"token\\\":\\\"uyDZ2P163iCK8mf3kJKpPPI7cPl0sI0h0oqNVUPWb0RMbUvGOwZuiTVLF9TNVtZc\\\",\\\"token_ip\\\":\\\"154.76.32.241\\\",\\\"token_fingerprint\\\":\\\"e45bb8ab78673a35e1740812f9b844ef\\\"}', 'provider: Google', '2020-03-15 20:08:19'),
(283, 'nLEwk0PS0Vvb5onVOVjoV5omqOwnqVWZ', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"9z9wTyc80nTDHtfUGrNd5PWsJcR8nKGpY3P7AdcV4Qqje5byI4ClrsNbDGR7h6W5\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.116 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"0464a80f8e94ba0d541232966d5011af\\\"}', '{\\\"token\\\":\\\"u78cL9FD3GJObqKEdPn8pYQymbw3v3a9EObXRNLiTgnIRRF2WbbJDXOnrVVcBlLr\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.149 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"9f7f4c2e5ccb5a9ae18bc0656f7a0603\\\"}', 'provider: Google', '2020-03-25 14:02:11'),
(284, 'vJBAiLV5AGfpRI15prthtMk9YISwTvhF', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"LpmtdPv9RrFOe8t1yWYImrtd3u5DntRmEEa8OtkY0hgGeNfRqN0Y9DWtSx24FitW\\\",\\\"token_ip\\\":\\\"41.80.73.102\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.122 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"7b3648857b95512332f927f95e2fb643\\\"}', '{\\\"token\\\":\\\"8m2jPqCegGEoYKbKIkBA3ZImmOZrHNiulBn4DlT5Wfrkqn6hzKl14C3MTmGeuE5S\\\",\\\"token_ip\\\":\\\"41.80.70.118\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.149 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"9980a6fa322160a0b554fb1bd53d7fe1\\\"}', 'provider: Google', '2020-03-27 20:24:54'),
(285, 'vjcO5xPf3UDWMRJPS7ZG7oy8emoqw2Fb', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"u78cL9FD3GJObqKEdPn8pYQymbw3v3a9EObXRNLiTgnIRRF2WbbJDXOnrVVcBlLr\\\",\\\"token_ip\\\":\\\"197.237.22.154\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.149 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"9f7f4c2e5ccb5a9ae18bc0656f7a0603\\\"}', '{\\\"token\\\":\\\"r61taqgOKxs42OznL54EUBGOSD9EL6v0GexSLPyNd1AASww7JXruT8ap6AO2xCKn\\\",\\\"token_ip\\\":\\\"41.80.96.182\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko\\\\/20100101 Firefox\\\\/75.0\\\",\\\"token_fingerprint\\\":\\\"48727664343f9e184bb8b73b32e2abf7\\\"}', 'provider: Google', '2020-04-26 17:05:18'),
(286, 'AYvP8OFhf7DWtuFcifqz3h9adWFSVZPt', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"I8j9ll7lOUWjZLPQp2brwXx82clOKslXxvSGlkUEL07hQV3wClESRXJwXwJ3TJnY\\\",\\\"token_ip\\\":\\\"154.152.165.48\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"51b7ba3d3324227ad3e7777c7e02a71e\\\"}', '{\\\"token\\\":\\\"F72Mg6QkeN7ina6TCaI26hQ423CEnyc5Gzx5SAb5MMv7jIenr62YcOnvUGQaQJ8C\\\",\\\"token_ip\\\":\\\"41.80.110.3\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko\\\\/20100101 Firefox\\\\/73.0\\\",\\\"token_fingerprint\\\":\\\"3c79aecf2855fc0aae5002db5066700b\\\"}', 'provider: Google', '2020-05-04 09:50:17'),
(287, 'WoXna42f4eX7mRS0ymK9nB6Coxf9bePL', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"8m2jPqCegGEoYKbKIkBA3ZImmOZrHNiulBn4DlT5Wfrkqn6hzKl14C3MTmGeuE5S\\\",\\\"token_ip\\\":\\\"41.80.70.118\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.149 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"9980a6fa322160a0b554fb1bd53d7fe1\\\"}', '{\\\"token\\\":\\\"BegThfItrrMV5x07Ql2pWuigImCjlVsJUqdRjCq0cvul9CHJA4gkRicevueOwzIE\\\",\\\"token_ip\\\":\\\"41.80.5.216\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 10; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/81.0.4044.117 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b9d640a8f41168255229969ccf6afc81\\\"}', 'provider: Google', '2020-05-04 09:56:36'),
(288, '68zYnCwXZ71bLIk3238Lu28I0YAn8Ppz', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"BegThfItrrMV5x07Ql2pWuigImCjlVsJUqdRjCq0cvul9CHJA4gkRicevueOwzIE\\\",\\\"token_ip\\\":\\\"41.80.5.216\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 10; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/81.0.4044.117 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"b9d640a8f41168255229969ccf6afc81\\\"}', '{\\\"token\\\":\\\"96TkximjT8TjMafBudrftmwyCH9yULZUU7rjrYwc0y3WjF4vDoZn7bHzvmN6VivY\\\",\\\"token_ip\\\":\\\"41.80.93.18\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 10; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/83.0.4103.106 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"5f5e3f3dcbf66f3bdcb8b6458a05140c\\\"}', 'provider: Google', '2020-06-25 18:46:43'),
(289, '7HiPrpg00QXYDRhD3E2YTNPoKUuC8gh4', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"r61taqgOKxs42OznL54EUBGOSD9EL6v0GexSLPyNd1AASww7JXruT8ap6AO2xCKn\\\",\\\"token_ip\\\":\\\"41.80.96.182\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko\\\\/20100101 Firefox\\\\/75.0\\\",\\\"token_fingerprint\\\":\\\"48727664343f9e184bb8b73b32e2abf7\\\"}', '{\\\"token\\\":\\\"j8O2eTgnmN31hdEWGRVeKh5AdTJygSPUjRuSV5ZggDHSYitjJOJdXjyXwCIGRWzr\\\",\\\"token_ip\\\":\\\"41.212.65.31\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (X11; Linux x86_64; rv:68.0) Gecko\\\\/20100101 Firefox\\\\/68.0\\\",\\\"token_fingerprint\\\":\\\"04dcd98293d0df68e86ba31a873dc87e\\\"}', 'provider: Google', '2020-07-18 07:25:36'),
(290, 'gVfcM7BAn3pjZvP4iewP7hN9srAv0MtD', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"96TkximjT8TjMafBudrftmwyCH9yULZUU7rjrYwc0y3WjF4vDoZn7bHzvmN6VivY\\\",\\\"token_ip\\\":\\\"41.80.93.18\\\",\\\"token_fingerprint\\\":\\\"5f5e3f3dcbf66f3bdcb8b6458a05140c\\\"}', '{\\\"token\\\":\\\"t5FAEL1R9MnIAKy43Dl3YtCzAXDvjIFze9zDKQxaBYPySy5AJ5cJLtM2rVvr3Gj1\\\",\\\"token_ip\\\":\\\"41.90.6.64\\\",\\\"token_fingerprint\\\":\\\"872ffa4081fd5680759072be6b8aa32b\\\"}', 'provider: Google', '2020-07-26 18:11:10'),
(291, 'HkNmKX6CJ6u0IaegNJxgrvj2B9IfNI2N', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"F72Mg6QkeN7ina6TCaI26hQ423CEnyc5Gzx5SAb5MMv7jIenr62YcOnvUGQaQJ8C\\\",\\\"token_ip\\\":\\\"41.80.110.3\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko\\\\/20100101 Firefox\\\\/73.0\\\",\\\"token_fingerprint\\\":\\\"3c79aecf2855fc0aae5002db5066700b\\\"}', '{\\\"token\\\":\\\"pmomIj46T8gyluKqRnHLxJEgKyo1xkL5vz8N3BMelxltQYCxcAgMNYVSMoMu7tgN\\\",\\\"token_ip\\\":\\\"102.0.213.189\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 10; Redmi Note 9S) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/86.0.4240.99 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"a877c4b1d1063b494405a81927c5947c\\\"}', 'provider: Google', '2020-10-27 18:19:49'),
(292, '1uySJW7k6nsZuPz1NRnZkoml7eByUMEY', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"XGaItOxe5uqwxs4svLQAC65MXofer2TuAMiLxvZ9nyBlqd7MwjvT1xHFtrtycHDv\\\"}', 'provider: Google', '2020-10-27 20:48:11'),
(293, 'TgSifiYLav5ywmNNQkGa5wll1HsTYEM4', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"41.212.65.31\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (X11; Linux x86_64; rv:68.0) Gecko\\\\/20100101 Firefox\\\\/68.0\\\",\\\"token_fingerprint\\\":\\\"04dcd98293d0df68e86ba31a873dc87e\\\"}', '{\\\"token\\\":\\\"yGhL041V77C4FBpDrrm0hCn4b7uqx0HHGX4FCICOE0hwBUVPD63LhFoG1gsDTdob\\\",\\\"token_ip\\\":\\\"41.212.116.214\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/86.0.4240.198 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"45975940bf59999faaf411ff08bf6314\\\"}', 'provider: Google', '2020-11-20 08:37:59'),
(294, 'L5NHhu1mvo19krOHqtSE8J1BrbAsv4pE', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"yGhL041V77C4FBpDrrm0hCn4b7uqx0HHGX4FCICOE0hwBUVPD63LhFoG1gsDTdob\\\",\\\"token_ip\\\":\\\"41.212.116.214\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/86.0.4240.198 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"45975940bf59999faaf411ff08bf6314\\\"}', '{\\\"token\\\":\\\"wlDcF5Eo0oErrYHmAejAUu86ZOy6pvKJHy3jX3uwDvQv52BC8IS96cTyvl8FVox3\\\",\\\"token_ip\\\":\\\"41.80.125.150\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/87.0.4280.66 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"461f058696ab2fc7494c209a37856842\\\"}', 'provider: Google', '2020-11-29 17:29:31'),
(295, 'ValcTYgiMHUVXuUnqxBPkQWEd7pkgA5i', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Update Account', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '{\\\"token\\\":\\\"t5FAEL1R9MnIAKy43Dl3YtCzAXDvjIFze9zDKQxaBYPySy5AJ5cJLtM2rVvr3Gj1\\\",\\\"token_ip\\\":\\\"41.90.6.64\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 10; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/83.0.4103.106 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"872ffa4081fd5680759072be6b8aa32b\\\"}', '{\\\"token\\\":\\\"yxsa7fmUHh8nkKKtYNDbMznuzFjEQffK110jAqRX5BIzvIbytBf2mMlYmwVIxSOV\\\",\\\"token_ip\\\":\\\"105.160.93.156\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/87.0.4280.67 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"349b04e652293b85cfda9b7a536c65fb\\\"}', 'provider: Google', '2020-12-02 07:42:12'),
(296, 'ncD8UAaPtNGduPrVEWC2I2GVol8ogzdn', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"102.0.213.189\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 10; Redmi Note 9S) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/86.0.4240.99 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"a877c4b1d1063b494405a81927c5947c\\\"}', '{\\\"token\\\":\\\"vXFJfis5MsYUZ3N08qHSD0dOuR1j713naKIhvmYa76lnyOGxaLLkWsiMDugDMqOE\\\",\\\"token_ip\\\":\\\"102.2.46.19\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/86.0.4240.198 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"e95b424420515c52dc5b921aa2a32c04\\\"}', 'provider: Google', '2020-12-02 10:35:29'),
(297, 'ei5hAZY3VZrTVFP5orvcE1cSyHFCgRIo', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"vXFJfis5MsYUZ3N08qHSD0dOuR1j713naKIhvmYa76lnyOGxaLLkWsiMDugDMqOE\\\",\\\"token_ip\\\":\\\"102.2.46.19\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/86.0.4240.198 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"e95b424420515c52dc5b921aa2a32c04\\\"}', '{\\\"token\\\":\\\"2BtYDY8lnDwvLxTnxrpAsB34UL2gp7mERvV0kDtOjuusDtvX0hi71RoUldulu9bl\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 10; Redmi Note 9S) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/87.0.4280.66 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"0ae4a5ae11b3f486ded02fa86a2eae46\\\"}', 'provider: Google', '2020-12-04 18:52:41'),
(298, 'Ay2DyxkZwEKBJnoTcKxb4uVFfYvUjpQA', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"wlDcF5Eo0oErrYHmAejAUu86ZOy6pvKJHy3jX3uwDvQv52BC8IS96cTyvl8FVox3\\\",\\\"token_ip\\\":\\\"41.80.125.150\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/87.0.4280.66 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"461f058696ab2fc7494c209a37856842\\\"}', '{\\\"token\\\":\\\"P1224spMAtV9A1aYh5k2ZbsvYmM9sgodqTbYLhpZLjlzWJJ0oNdYeqaTnih3dfVg\\\",\\\"token_ip\\\":\\\"105.161.27.91\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/87.0.4280.101 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"ccdf80febbf118720a2ea95359edd169\\\"}', 'provider: Google', '2020-12-19 09:50:29'),
(299, '99j6XAfbySZUzSbmOumjUspN1aciqnWb', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Update Account', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '{\\\"token\\\":\\\"P1224spMAtV9A1aYh5k2ZbsvYmM9sgodqTbYLhpZLjlzWJJ0oNdYeqaTnih3dfVg\\\",\\\"token_ip\\\":\\\"105.161.27.91\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/87.0.4280.101 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"ccdf80febbf118720a2ea95359edd169\\\"}', '{\\\"token\\\":\\\"cvaIXqBI4g7cbCRQti3cVm8lc2TrKKKe4DB1oynwaROzEv4Q2lPWUrF3GEQZA6e0\\\",\\\"token_ip\\\":\\\"105.162.50.157\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (X11; Linux x86_64; rv:78.0) Gecko\\\\/20100101 Firefox\\\\/78.0\\\",\\\"token_fingerprint\\\":\\\"1b49a9d367ba6cbdf343d27fb146af17\\\"}', 'provider: Google', '2021-02-13 21:57:11'),
(300, 'dq6DjAl5n3uhtA5uu2081P9yu1SFeMQV', 'gnjfc5n1Q91Pjs5k8KgrbELhOgp88PKF', 'Update Account', 'gnjfc5n1Q91Pjs5k8KgrbELhOgp88PKF', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"e1xyf0VD3r69XRVs6g4ZOY1Y3SFMCWZOfLOSwb2H2j4iAnXphK5bbKw1toduQDXG\\\",\\\"token_ip\\\":\\\"41.80.97.166\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/91.0.4472.77 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"a10baeac182f582351e9678c39284530\\\"}', 'provider: Google', '2021-06-10 14:18:43'),
(301, 'AtkuMithdpRNGWtPXY6ezMCzlFnK50Rl', 'XEIwW6ocQcVaOrafe7YGGYGEEM5CxWjP', 'Update Account', 'XEIwW6ocQcVaOrafe7YGGYGEEM5CxWjP', '{\\\"token\\\":\\\"\\\",\\\"token_ip\\\":\\\"\\\",\\\"token_useragent\\\":\\\"\\\",\\\"token_fingerprint\\\":\\\"\\\"}', '{\\\"token\\\":\\\"NahDbfYPseDtrRjFuw6yiCunwXpXjthPpWK1x2tVXO7VGdsdRAiNOSGzekeIS7rF\\\",\\\"token_ip\\\":\\\"154.155.12.28\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/91.0.4472.77 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"aef4e7b748356b0606e5988a7ca7943a\\\"}', 'provider: Google', '2021-06-10 14:18:51'),
(302, 'aKwbWCqhfnuzPfrzR6M2rFFWr9VzIY8z', 'gnjfc5n1Q91Pjs5k8KgrbELhOgp88PKF', 'Update Account', 'gnjfc5n1Q91Pjs5k8KgrbELhOgp88PKF', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"u4MGHG7F4Pd1tWVy5Flh3UdE1nm3NGYVXVt6qCd3rgeezeNDDMRLh7eJQTc88fne\\\"}', 'provider: Google', '2021-06-10 14:20:30'),
(303, 'KVeUec4FLgHOGIAqQZQN4tXGAzD8d18t', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Update Account', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '{\\\"token\\\":\\\"uyDZ2P163iCK8mf3kJKpPPI7cPl0sI0h0oqNVUPWb0RMbUvGOwZuiTVLF9TNVtZc\\\",\\\"token_ip\\\":\\\"154.76.32.241\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/80.0.3987.132 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"e45bb8ab78673a35e1740812f9b844ef\\\"}', '{\\\"token\\\":\\\"PHPh5a1Ty0xoO7UQX5onNXrn8J5jPlLghbCsI5YWxs46Qm7BvAUhEPmRNluZSd9i\\\",\\\"token_ip\\\":\\\"154.155.12.28\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/91.0.4472.77 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"dd2170709289630c303a6baf64c3e5c4\\\"}', 'provider: Google', '2021-06-10 14:21:56'),
(304, 'e2SLqD4Nf1fu9Ghl4pUqpKR4pDzLsPp1', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Update Account', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '{\\\"token\\\":\\\"V9C3XBmWcPdWcxGRDHdpjn5OzU2QtDvlyv9uh2o3IGdPSonto4ILp8Y6Y44lpQPE\\\",\\\"token_ip\\\":\\\"196.207.128.143\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/79.0.3945.130 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"89d30c6cc87842257779e90d8487f1c2\\\"}', '{\\\"token\\\":\\\"V8KVnEcTylxTsksUa9pp5USUNnCHDblmyuU59euMm7KX7038GheNhg0dXYtLxeWM\\\",\\\"token_ip\\\":\\\"212.49.95.37\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/91.0.4472.77 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"e852ebf2f292461e493c36338a645610\\\"}', 'provider: Google', '2021-06-10 14:22:41'),
(305, 'dLlw0Mc5Rg0djFrljEl5bNmrx9I2J6hQ', 'XEIwW6ocQcVaOrafe7YGGYGEEM5CxWjP', 'Update Account', 'XEIwW6ocQcVaOrafe7YGGYGEEM5CxWjP', '{\\\"token\\\":\\\"\\\"}', '{\\\"token\\\":\\\"QOxE5PKbYLQCxF6Q1hp3HBf6kkbu4nVV132s5DOl3TOua7fq4Obw47av10bmbkQm\\\"}', 'provider: Google', '2021-06-10 14:24:27'),
(306, 'YzXQepiFr7AaOfvYY8XPdUDuQcv9axlx', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Update Account', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '{\\\"token\\\":\\\"2BtYDY8lnDwvLxTnxrpAsB34UL2gp7mERvV0kDtOjuusDtvX0hi71RoUldulu9bl\\\",\\\"token_ip\\\":\\\"197.232.244.41\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 10; Redmi Note 9S) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/87.0.4280.66 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"0ae4a5ae11b3f486ded02fa86a2eae46\\\"}', '{\\\"token\\\":\\\"OsglWIR3AUf6nx08f0tozZzu0wl8rmonmRELjaMqyBXwtRP7F8jiPq7bGp1wPfKj\\\",\\\"token_ip\\\":\\\"41.80.97.166\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/91.0.4472.77 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"a10baeac182f582351e9678c39284530\\\"}', 'provider: Google', '2021-06-10 14:33:19'),
(307, 'WAEKe0p9lvq6KaVZ4TZzEazk43UnYisZ', 'XEIwW6ocQcVaOrafe7YGGYGEEM5CxWjP', 'Update Account', 'XEIwW6ocQcVaOrafe7YGGYGEEM5CxWjP', '{\\\"token\\\":\\\"QOxE5PKbYLQCxF6Q1hp3HBf6kkbu4nVV132s5DOl3TOua7fq4Obw47av10bmbkQm\\\",\\\"token_ip\\\":\\\"154.155.12.28\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/91.0.4472.77 Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"aef4e7b748356b0606e5988a7ca7943a\\\"}', '{\\\"token\\\":\\\"MjocrDL0bLizVtKo0TB1fNMYKWPoNO8EdUsqHgTXx90C7RqIvssSXtQUSedJRfNN\\\",\\\"token_ip\\\":\\\"105.160.100.159\\\",\\\"token_useragent\\\":\\\"Mozilla\\\\/5.0 (Linux; Android 10; SM-N960F) AppleWebKit\\\\/537.36 (KHTML, like Gecko) Chrome\\\\/91.0.4472.101 Mobile Safari\\\\/537.36\\\",\\\"token_fingerprint\\\":\\\"cbbe9e8cbb18aa865f0719a0c94aa5d6\\\"}', 'provider: Google', '2021-06-18 11:15:27');

-- --------------------------------------------------------

--
-- Table structure for table `ns_users`
--

CREATE TABLE `ns_users` (
  `_index` int(20) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `status` varchar(200) DEFAULT NULL,
  `hash` varchar(200) NOT NULL,
  `token` varchar(200) DEFAULT NULL,
  `token_ip` varchar(200) DEFAULT NULL,
  `token_useragent` text DEFAULT NULL,
  `token_fingerprint` varchar(200) DEFAULT NULL,
  `provider` varchar(200) DEFAULT NULL,
  `userlevel` tinyint(2) NOT NULL,
  `identifier` varchar(200) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `profile_url` varchar(200) DEFAULT NULL,
  `photo_url` text DEFAULT NULL,
  `display_name` varchar(200) NOT NULL,
  `description` text DEFAULT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `gender` varchar(200) DEFAULT NULL,
  `language` varchar(200) DEFAULT NULL,
  `age` varchar(200) DEFAULT NULL,
  `birth_day` varchar(200) DEFAULT NULL,
  `birth_month` varchar(200) DEFAULT NULL,
  `birth_year` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `email_verified` tinyint(2) DEFAULT NULL,
  `verification_code` varchar(200) DEFAULT NULL,
  `reset_code` varchar(200) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `region` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `zip` varchar(200) DEFAULT NULL,
  `other` text DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ns_users`
--

INSERT INTO `ns_users` (`_index`, `uid`, `status`, `hash`, `token`, `token_ip`, `token_useragent`, `token_fingerprint`, `provider`, `userlevel`, `identifier`, `website`, `profile_url`, `photo_url`, `display_name`, `description`, `first_name`, `last_name`, `gender`, `language`, `age`, `birth_day`, `birth_month`, `birth_year`, `username`, `email`, `email_verified`, `verification_code`, `reset_code`, `phone`, `address`, `country`, `region`, `city`, `zip`, `other`, `timestamp`) VALUES
(22, '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', '1', '2b5b393b0300c77777f0e92c7dfa14fa', '', '196.207.128.143', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'aa2d7287613f9d41ea175d07d2f297d4', 'email', 2, NULL, NULL, NULL, NULL, 'Martin Njoroge', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'info@blueconsulting.co.ke', 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-16 19:03:27'),
(25, '35DrzmfG2VxxM7NC06gH6HZymJgyRhsD', '0', '', 'f6vYp5CFqJl157A5wU5bWDYLzzaCdEAXHvxcJrsyV87snjuB4kK4rzwTubm89JA2', '197.237.251.249', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', '9b9518078fbd7a6823ea6f5ff8a19a69', 'Google', 1, 'id_106766802100967886762', NULL, NULL, 'avatar-4o05jP4MAy.png', 'Diana Dee', NULL, 'Diana', 'Dee', NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'facebookchebet@gmail.com', 0, '9rmGJ8Yyd1k0jYQIpLltwppT4dfTYXEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5LkdsdUpCNWVwTXRPREN1MUhrS3NoeEVYb1ZiRTRzT2h6a01SN1pKaUs5QTc2bUx0THBRM2p6TnZYendseUdfTnRCcmN0Z2xoSENXUEViSFl6M0ZsUEJLNkU4Q2tTeXVMX0tLcHotQ3NDY05zSnRXMDZnOFBKdXZXUmhEZDYiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcLy1EZ2ktek9kSHA5RW1aNGxOQzBKSTdFUGxtendDNzkyVGU1bGZjSmxvQTAiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTY4OTY5OTI0fQ==', '2019-09-20 10:58:50'),
(32, '40RNorzfIWiKKdTeqH5HXzcqTWxdZAIL', '1', '3ada11046c9148bc1af9a1c8a802e0cd', 'JudB5xKZpoG5bORhAd2gnk4Nv5o6EGRShCWnOqCbpmea7awgXOr00c8QP3EQPZu6', '196.207.128.143', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'a770ef3f1aaff0536586359b8d6ef8f4', 'email', 2, NULL, NULL, NULL, NULL, 'Tupange', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6luis.dalmonec@tampabayluxuryagent.com', 0, 'TkZM0k4c8Do97wQ0Bp9A0S5oNIFizMbj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-21 19:02:44'),
(20, '5S7WRHYAvLXpYTjq9ukhaKtWEsSQEl1d', '1', '', '', '', '', '', 'Google', 2, 'id_108682896443653742941', NULL, NULL, 'avatar-aiBM1rQse3.png', 'Oliver Queen', NULL, 'Oliver', 'Queen', NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'queenoliver821@gmail.com', 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5LkdsczBCd2R0SUM1RGpLNDFJeHRZSkFMMnJvd0lGZWotU2pKNFF1QmdpVHdkT2puZTg5V2duUUVBWE9HRGtncnJ0X3FQM1V6a2ktTF9RMm5HNFlJN1NWMlN6cEE4SXZ1eUVWb3hJLXJmQWVocFZTdGN1VzcwR1d0X0RqVTQiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcL1hJbzBiTXk0a1ZnY0Z2Rjlhc0Z3WXcwa3ozcHFUZVo4LUJhY1JKNWV5bVkiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTYxNjQyMDc1fQ==', '2019-06-27 15:28:00'),
(12, '7hS75ihbUhrswdWJzCsS7aMxdT34Td5U', '0', '', '', '', '', '', 'Twitter', 1, 'id_95722930', NULL, NULL, 'http://pbs.twimg.com/profile_images/566857590/madness_tee.jpg', 'madnesscrew', NULL, 'madnesscrew usa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'madnesscrewusa@gmail.com', 0, 'rDcunvl2ESmijQwgSWWOh387kfRl5MJC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiI5NTcyMjkzMC1XY3hGQ3FEQ1l3UFc3cVowYzIyODNySDk1SU9iOHk5SkpGazhLbDV4VCIsImFjY2Vzc190b2tlbl9zZWNyZXQiOiJmbVluckk1WVJVOFc1ak1FZW1qb21sY0c3RXJOTERHQ2hqdXNrU002a1AwQ1MifQ==', '2019-06-04 20:05:24'),
(13, 'cEY2HYoVHiZQ5e3t1QLdaiITppTfZdq1', '1', '6299d01ff9b6938a4dd45635b0552c7c', '', '', '', '', 'email', 2, NULL, NULL, NULL, NULL, 'Francis', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fngari@resolution.co.ke', 0, '9HWGBRDLADtNTwVsDeFU2ERAKm07cny9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-04 20:05:41'),
(2, 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '0', 'b435fb8b7673b505803691d30b6ddeb8', 'cvaIXqBI4g7cbCRQti3cVm8lc2TrKKKe4DB1oynwaROzEv4Q2lPWUrF3GEQZA6e0', '105.162.50.157', 'Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0', '1b49a9d367ba6cbdf343d27fb146af17', 'Google', 1, 'id_107661531060544420421', NULL, NULL, 'avatar-F99dlgsFfD.jpg', 'Martin Thuku', NULL, 'Martin', 'Thuku', NULL, 'en-GB', NULL, NULL, NULL, NULL, NULL, 'xthukuh@gmail.com', 1, '', 'plAPaWJ9sx236Cda5jQHr9PrzMX15Zfg', NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsd1BCenN6c0lkQzA5VTVtOGpYS3gta1cxc3VaUXZpSUxDV1VPM1M2N2huRXQ5NHJPTmhLVWh6ZEdVamZXNTUzMlBNMzV2M28tVnVwbEtNUVFQS2VCNU9TWjJmaGxjcTY0R1V0Y3pUVTU1VjRiNmdEcjYtcFBwa3p3Y3IyZyIsInRva2VuX3R5cGUiOiJCZWFyZXIiLCJleHBpcmVzX2luIjozNTk5LCJleHBpcmVzX2F0IjoxNTU4MzcwNzYzfQ==', '2019-05-20 18:46:12'),
(31, 'EE755TUD404ZFcCOfGIJcrhTJ9Wp1dl8', '0', 'fb85786da44f93fb62ee12a7b12e9e84', '', '197.232.43.41', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36', '206e52273a2081459c998d9bdc97eed9', 'email', 1, NULL, NULL, NULL, NULL, 'Linda Njesh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cheriwamuturi@yahoo.com', 0, 'e2u1Cu4lqk3E1WgkBj303rgPGhCQ921f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09 10:11:18'),
(11, 'fAEryS4d8g6uIgdYpF6LSPTWeSIwossh', '1', '', '', '197.232.43.41', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36', 'af1a4debc4ef817dcea8ae99df8d321d', 'Google', 2, 'id_103368736732846277777', NULL, NULL, 'avatar-benw680k6A.jpg', 'First Last', NULL, 'First', 'Last', NULL, 'en-GB', NULL, NULL, NULL, NULL, NULL, 'carbonblue45@gmail.com', 0, 'AX4fSXLnVuefKZjrbeqvUmvbGrMQYEGT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsc2NCNU9NdEhjU2YwWDdQTS1NTDRUTFJjN1RGOXNLcGw4aEFkbnJlalBWWDEzQXJmLW9OREJSdnhLV0MzOWQzMThBcEZnVU1DN3I5dkViNl9vNU9xWlpVS1Uza2tWck5oMGR4MW40bER3aGZiVU5sWjNBX2w1RjAyTm8iLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcL1VmU2pXZGFjd29nQkhNdHBLSW1kNWZoY3dKNGRMTEdTWkZaN0RKQm1PWWMiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTU5NDczOTQzfQ==', '2019-06-02 13:12:28'),
(18, 'gnjfc5n1Q91Pjs5k8KgrbELhOgp88PKF', '0', '', '', '41.80.97.166', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'a10baeac182f582351e9678c39284530', 'Google', 1, 'id_110617376232088298215', NULL, NULL, 'avatar-vJbldOglcP.png', 'Limo Sadalla', NULL, 'Limo', 'Sadalla', NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'limo2009@gmail.com', 0, 'z3QLmxTr3mnv9DOBOtcZdl7Wyl1RK1NE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsc2xCNkJEMEt3SWJSaHVwbUkzc1NlQ0ZhbjRTMzVGa052ZXUwVnVhX1NCaXJ5VnhuczlrREpDMVdZajI1ZENzbTRTNlNnNjEtaWREa0xwNHdHLWtWVF9Bc2FrVzBwMF8tem9VeEFMRkxaSTQ5LUFQSDhUdFEwamtCa2QiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcLzBVbzBSX3BCQlc0ZWVuY3pDX1BGeF83QXQ4OEY3TmNYeGM3ODItRjgzVEEiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTYwMjcwNDA0fQ==', '2019-06-11 18:26:50'),
(19, 'H5FRGWKEXaivpKZMzlj2jU89OPR4vNrt', '1', '6299d01ff9b6938a4dd45635b0552c7c', '', '', '', '', 'email', 2, NULL, NULL, NULL, NULL, 'Blue', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'carbon@blueconsulting.co.ke', 0, '5JEKjcCcmIzQp2JIO2nZAGLP8sZpQQN7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-25 19:33:15'),
(23, 'HcVOVsFTBspEuZTI5Tb5W1sFU5VFcRMj', '0', 'a2628d9a72cee2c9fc86f00f08ea291e', 'iJ8RASwVK3dEccEQdg3GbuThASyMyH0LpOTo846M3E5sOHk7nHDpqouHfIHwl3ry', '196.207.128.143', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko', '85f7f61487c976f9a64dc17bd195180d', 'email', 2, NULL, NULL, NULL, NULL, 'Frajoe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fngari@gmail.com', 0, 'QZtqucVb7VDVUock5Sn6EPbQdmmJNd6b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-20 19:19:05'),
(27, 'igoQnHOXrlyhwTt1AnSOLUkFP9n789DI', '0', 'c2f782b95ca310492a77ed1430086f20', 'RjkpOxfEWfnqw9s2PhiELTiTvKDrMrq3tQGUDfUPNtEFqc1g70cJBaQfh3rnXgIO', '197.232.43.41', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', '86369f87ae4780efc65b94dfe65ce961', 'email', 1, NULL, NULL, NULL, NULL, 'Mike', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mikinyass@yahoo.com', 0, 'h8lgL5PPdVs1AvDpcs76GhC9phaGcRAl', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-05 11:11:21'),
(10, 'iT3fjPuOfeJ4ett00tsn5BjqYN925kOd', '0', '', '', '', '', '', 'Google', 1, 'id_105928252543838372318', NULL, NULL, 'https://lh3.googleusercontent.com/-wivIIqx_hyI/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdoYvK3XVGI81mSTG8MHIWPQ4GL5g/mo/photo.jpg', 'Finance Department', NULL, 'Finance', 'Department', NULL, 'en-GB', NULL, NULL, NULL, NULL, NULL, 'finance@blueconsulting.co.ke', 0, 'DoG7eZHFnJ3Ck1eEWAJRnvRWIWwvM5DA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsc1lCN1V3ZTFVbWxKYWRjS0FWblU1dnd2dXBjR1E5c0IzTnVFQXVBeG5OdDFBRW9vOEFaTVh3R2s0TVhXNjl3OVdaam9EYk1Wb3hYTFBVOEYxZEJLUXlhckRkWWRpM3E2aFBsYVRHc3lucjZ3NWJrd2oxNk5ubWRwcHMiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcLy02ZV9ERFp5Mzk2TEpTUnZmaVpUdGtvbDRHM0Y1cjR0MzNENHl1VVhKWm1CU2o5ZmJaSV9sTmtSWG5PN1FfSEoiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTU5MTU5NDcyfQ==', '2019-05-29 21:51:19'),
(9, 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '1', '', 'PHPh5a1Ty0xoO7UQX5onNXrn8J5jPlLghbCsI5YWxs46Qm7BvAUhEPmRNluZSd9i', '154.155.12.28', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'dd2170709289630c303a6baf64c3e5c4', 'Google', 2, 'id_104113690813375597939', NULL, NULL, 'avatar-NOxPUyRGBW.jpg', 'Linda Maina', NULL, 'Linda', 'Maina', NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'cheriwamuturi@gmail.com', 0, 'CYrMg7Bvw1eLKNUX1uPt1R9V5xOsxtMC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsc1lCLWJRLUhSQ3hteXBVU1BTMWtreVlxN3V1dlRSMXBwNVliX2NNM1BlY3hPbVFPT1Vqb2hFYkM0NUNRdkxRSVVoLWNybnF5R01KODVqWTY0ZDgwNWNaalp0SFZrRHFJaS1IMnFuUXY4TnBqdGRuUDFCSHdYTEdsd1IiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcL2FObTZiLWlkWG9EWHA2Qlp4emlUWVpmUXMzWjYycHhhUDJwemNYZGlQSUp3eHdfcVhfbUQ2aE5qdVF6dng3QjEiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTU5MTU3NTI5fQ==', '2019-05-29 21:18:55'),
(16, 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', '1', '6eea04b2842c76812d5c9d83525e90ff', 'yUgLKyrypOlA7FMPWLxfdp3oP1zVWSjfLgD7qo2pyDPaU3AW1jUu4PhIOeikSv28', '197.178.205.206', 'Mozilla/5.0 (Linux; Android 9; SM-G955U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', '6712f095ac62b5491790bb749ba96fb4', 'email', 2, NULL, NULL, NULL, NULL, 'Kigen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mboganakadhalika@gmail.com', 0, 'w6eywHvqP4Kwc4zY1E4C226tKiTZMEgD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-07 20:56:05'),
(8, 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', '1', 'b435fb8b7673b505803691d30b6ddeb8', 'ZZwPxnbPNovk7MUYmkggrkZqC9NvwBBZlMgRi7KsSKMWNU7reUG9nZQSgrNjR29P', '197.237.17.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 'acad62e376e483cad2e65144c57e2571', 'Google', 2, 'id_113867287079359531668', NULL, NULL, 'avatar-ejlfXU3D0B.png', 'martin thuku', NULL, 'martin', 'thuku', NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'mxdodger@gmail.com', 0, '9SfBLo7ByKiPNJACjz7kMlbjlTy0Haeh', '', NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsc1lCMDRMRnhQQTNnaUpyVktFS2pxQVRSV0J6VGZMUVA3Y2pxQXhtZlFEUUZQa3VsWnlMZDFJTkRMZjh1MEwxQlZmZEFnZFFXX20teVFEdFotOUF4aVd3T25scTNTOFlwU2FHZVJjcWNZN2M2QW5tMVpVRjRQenYyYlMiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwiZXhwaXJlc19pbiI6MzU5OSwiZXhwaXJlc19hdCI6MTU1OTE1NDY1M30=', '2019-05-29 20:31:00'),
(3, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '0', '', 'OsglWIR3AUf6nx08f0tozZzu0wl8rmonmRELjaMqyBXwtRP7F8jiPq7bGp1wPfKj', '41.80.97.166', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'a10baeac182f582351e9678c39284530', 'Google', 1, 'id_105151374265662149206', NULL, NULL, 'avatar-J3xJnwpDuD.png', 'Limo Sadalla', NULL, 'Limo', 'Sadalla', 'male', 'en', NULL, NULL, NULL, NULL, NULL, 'lsadalla@blueconsulting.co.ke', 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsc1BCLVlsZnRxWGZaajhyX0pYb1I5VVhrX1gwSGRSdG8tYTA1cHpqNFNkdmpuNjVBdzVlSjNOVTBQUlptekhKZ19zdThrUHBnc3JIQWpERDlCOVVlZWFwd05Xd0lJN2gtTFhlQkxFTFF6R0V5a3lKOUY2UElfQWtGSksiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcL3d0SGNUcWlsX0hqSzBDUjJmaTJUQ2I1dV8teE5TOVRxZ25jYjJZWFd0UjQiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTU4Mzg4NjU4fQ==', '2019-05-20 23:44:25'),
(30, 'PYBYvuf80nYCwn4Wd42QWnwRsWFhjqpS', '0', 'a2628d9a72cee2c9fc86f00f08ea291e', 'OrddurteVkkE4ax4vQsCsHAonpga1G5iZdAx5UN2pSqSRibWmvooF2ODUwATvbjx', '197.232.43.41', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', '7088ba0def984cb91aea3abde94bc9cd', 'email', 1, NULL, NULL, NULL, NULL, 'Vuda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'vurzakurta@enayu.com', 0, 'WGa1T5sfXgNxrkktbrqKKmhvz8PIHvec', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-09 10:02:35'),
(28, 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', '0', 'c2f782b95ca310492a77ed1430086f20', '', '197.232.43.41', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', '86369f87ae4780efc65b94dfe65ce961', 'email', 1, NULL, NULL, NULL, NULL, 'Mike', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mikkinyass@yahoo.com', 0, 'Z1peHoNZtvDQUsElEmBwLQ04hpE13k74', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-05 11:11:41'),
(4, 'QBmZmxDImVGt5T6zIIO85vlcPEgjfVQI', '0', '', '', '66.249.93.55', 'Mozilla/5.0 (Linux; Android 9; SM-G955U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Mobile Safari/537.36', 'b73152e26ede07da2388685fb3150dce', 'Google', 1, 'id_100894781064141152194', NULL, NULL, 'https://lh4.googleusercontent.com/-rdcnM4qIbPg/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rfT2gEOwfGpcxCYFKknqJBn6sL8Og/mo/photo.jpg', 'events 360', NULL, 'events', '360', NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'events360kenya@gmail.com', 0, '4RUHUMxW4R9w1GbPLCVAHvWyM8AIZDUd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsc1BCN0MxcWduQWlZNV9reXliVkczNXZwX2NkVHFPSnZpUl9wUFhiNVV5SHRJMnQ0a1hlc1R2WmZZV2lmaHdfNEdGTTJBa05hdmJBcnhxSzZzNmtLV1NFZkV5R1ltUWNaVExlOGR4emVYanQ3YkhqTWExaVVvVkgtSmoiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcL3Vpcl95NlZIV0xNcFd4MTM2clhIdkhnbzVSNmxrWklTWmtGRVZwdjVFLTBKNFFueWxGVzBNVGFPcDFXWkdoN0EiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTU4Mzg4Njc4fQ==', '2019-05-20 23:44:43'),
(29, 'rUemiRfRkPmwyIRNTN73NF6WstWILaD5', '0', '6d7856f2ae8eada9e12e894fb6dbdff6', 'j6yDYcqfLbmerWZmAXaVTnIVgA5qmqsyBCLTlGRervdbiy0hG9NnuzwIqXqFaIl9', '212.49.69.83', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', '89629a611709a0a6228d46fcb3adad3a', 'email', 1, NULL, NULL, NULL, NULL, 'Njesh Mn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'linda.maina@icloud.com', 0, 'aMQHagLasmBE174GukBx3FW7oaJV00OV', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-15 15:03:06'),
(1, 's8fhXVWkrlKw90UAFzh2LUu4vDiRNi7a', '0', '', '', '105.49.105.161', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36', '1893e899f2ca78f0d5abede5b4ba20fc', 'Twitter', 1, 'id_1122827394455363585', NULL, NULL, '', 'tupangeofficial', 'Occasions 360 Events & Services. Create and manage your event and search for services', 'Occasions 360', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'o360@naicode.com', 0, 'AnQzNT7SzAxcBtYoNiKR1BfkKDaNHPOy', NULL, NULL, NULL, NULL, 'Nairobi, Kenya', NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiIxMTIyODI3Mzk0NDU1MzYzNTg1LTNZU0tudm1vSWJTS291OTdjcVdheUV6NlVQcjRyYSIsImFjY2Vzc190b2tlbl9zZWNyZXQiOiJxamo3bmlYbGJLZnJRekVtMXpXZ0ZONUV3RlI4Ym8zTHZuclFMSzdxdDBZWnkifQ==', '2019-05-20 18:41:08'),
(24, 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', '0', '6299d01ff9b6938a4dd45635b0552c7c', 'NGg7l0HAWzpwyVdsBkDRNkkdVj3zjDD2zaSpHaSXQ0PhwJdxzsuJ4tvXWns9C7oM', '197.232.41.80', 'Mozilla/5.0 (Linux; Android 9; SM-G955U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', '5d54a024b2bb38c59a46a4451690145b', 'email', 1, NULL, NULL, NULL, NULL, 'Limo Sadalla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'limo@gmail.com', 0, 'pdf9LoX0G9NhHiEWAnvsyyrfthvuYDRU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-20 19:20:49'),
(15, 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', '0', 'c80fc01f0f5d34a8454fa72552b389cc', 'qPHZHX5n637qDYFXjsR5P2zjOarF2tkuWHbN9TMgLUL3E8zcB79Ib8pvu3yKnUFY', '197.237.251.249', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', '99034fc73d425a888f0bd6fb26915bfd', 'email', 1, NULL, NULL, NULL, NULL, 'Diana Cheserem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dianacheserem@gmail.com', 1, '', 'mgwMP3Bf1f7uU7LFDVE6mnya4YaqueM7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-06 14:56:58'),
(26, 'VXliwISX9SyZ23tlbn5Qf1t6Tg0uJmAR', '0', '', '', '212.49.69.83', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', '22ff1eef84242474215726aae3e9b41d', 'Google', 1, 'id_106853142483390015862', NULL, NULL, 'avatar-g4bDR1IAzD.jpg', 'madnesscrewkitchens', NULL, 'madnesscrewkitchens', NULL, NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'onecallevents254@gmail.com', 0, 'Vj7CR9pVIAu8n2MxddoHmr5xxTjYeGr1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5LklsLVVCLThfdmdlVVd3UkpwX1UxZlRWRFBjd1VoTURrTEc0Ql9xWGNVeG8xQ0Z2N3FXbU9jX051X0Fjc3BDX29JQ3lfbEpMWkR0RjRjQ29jOFFGc3BCdS14WERGTzhEMkxHNUVaa1BwSUU3Z1JyUWFHamlkeXgxbVhoNjVtalozZkEiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcL19nWEpmeklUbE5Wel9NeFo5YTE1U1g1QmRXOEk0clpLRVpTYVQzVlV2eTdMaDU5djdlV3FLT1BWYU02aHlMY2IiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTcwMDMyNzg5fQ==', '2019-10-02 18:13:14'),
(14, 'wuGzBFEBkiU99CPqAXeZwY5g1j8vw0Nk', '0', '', '', '', '', '', 'Twitter', 1, 'id_613203388', NULL, NULL, 'http://pbs.twimg.com/profile_images/2325230539/Lion_hunting.jpg', 'Hoyeiya', 'Putting it in action and seeing it through to the end is my passion.', 'Ef Em', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hoyeiya@yahoo.com', 0, 'YG8QZZVxBbuQ8IPenn9fb7bIVhTBN6Ss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiI2MTMyMDMzODgtT2JjWHE3WHA2OWlKWndhN1FCdUo1WEJPamNqT01rWDR3YzlNVEZaMyIsImFjY2Vzc190b2tlbl9zZWNyZXQiOiJ6R05TbHdqRFFoWXBoYUlVd2RSajJUYWg1bFo5ZE53bVppZW96OGh5SkxkbHIifQ==', '2019-06-04 20:05:53'),
(21, 'XEIwW6ocQcVaOrafe7YGGYGEEM5CxWjP', '0', '', 'MjocrDL0bLizVtKo0TB1fNMYKWPoNO8EdUsqHgTXx90C7RqIvssSXtQUSedJRfNN', '105.160.100.159', 'Mozilla/5.0 (Linux; Android 10; SM-N960F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Mobile Safari/537.36', 'cbbe9e8cbb18aa865f0719a0c94aa5d6', 'Google', 1, 'id_104752091421026298833', NULL, NULL, 'avatar-BdJEyEHYCZ.png', 'Tupange Events', NULL, 'Tupange', 'Events', NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'tupangeevents@gmail.com', 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5LkdsdEhCNU0zWU9JTFYyVUpBdTlWQXp3QXFDYzN2dl9oOTFLSVB0SElfejhqaFBMaUxoQXJQMWRkNnUwakc5MFhrRDROcGhxWENJT1RhNUxoMkVzLWZBSXJ1N2pOWkQ1Q2kwVlY4Vk9lTzhKMEZDMGtfM3l1ZmY2WjFQb3giLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcLzN1LWV4UVJGYnlwdVJFbHdxWEU3aUJPQ0R2c3RRaHZGX053OHRDdGtOWkFrLVpjNTh5ci1mWjBwVDZpaHRLT2YiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTYzMjk0NjkzfQ==', '2019-07-16 18:31:38'),
(5, 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '1', '', 'yxsa7fmUHh8nkKKtYNDbMznuzFjEQffK110jAqRX5BIzvIbytBf2mMlYmwVIxSOV', '105.160.93.156', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36', '349b04e652293b85cfda9b7a536c65fb', 'Google', 2, 'id_101370340585437414634', NULL, NULL, 'avatar-Mf5U4zcWa3.jpg', 'OneCallEvents Kenya', NULL, 'OneCallEvents', 'Kenya', NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'onecalleventskenya@gmail.com', 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsc1FCekJaMF81aVFNWlpvcWdMV1dMbjFFTkYwTC0wd3hJZTMzdXRfMXYwa2V5MTVCWEpFYVFJWklUQnBSNzBYdFNtQ1JiQkVJZE91OVlWc1lEeTlDSW1yd25XVGFvOW1Uak8yVHdyYTdWLWVWaEdhWnJ5UGtrMERvWnoiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcL0JoNW5rSXcteWxWN25QZFppWW9CS1V2OTVWUUtzRVZydGpnM3c5dmg2Y00iLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTU4NDE5NTA1fQ==', '2019-05-21 08:18:30'),
(7, 'Y8LAHX9E46ChnhIApULGQKBXY6DdUtJv', '0', '', '', '212.49.69.83', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', '17915016d64bb3dafd9a535e1e486887', 'Google', 2, 'id_118130755728673170911', NULL, NULL, 'avatar-WxUGpQVhw4.png', 'Micheal Nganga', NULL, 'Micheal', 'Nganga', NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'Miken@golfsasa.com', 0, 'Tbuv7vsktK6TyLkFNmFbegcJfrkyZRAO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsc1hCNlVyRVJlb1B0MlZiU0t1NFZlYk5VczZEZWZPUi1scVVLUERUVFhUY0IzSTBwTk9VOHhWR2tvQkJxeGdGS1hIblpUczN6dW5Idl93ZWRBUWVDMTk4OGFaZlhreGFWM2J5clE3NkhKSmY0UFN2dVJBUlc2aG41Q0kiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcL0hDSUQtSUtVbzNyTVJTVXdUWDM0V1VRSjNJRG1vZHFWbVZXQWt5WmZ3anJHZmozQWx6ZTRlYjk0YmtNMTkxWnAiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTU5MDY2MDIwfQ==', '2019-05-28 19:53:46'),
(17, 'Yf4ccb5vyWplQy2UqpnCNXTeNcxSLaT2', '1', 'b435fb8b7673b505803691d30b6ddeb8', 't0NZkutxG3GIuLbomgSp2rb3X3kl6a65bFkL2LgpvCjO46f8EZSTaM2OvpynbEX1', '105.51.85.141', 'Mozilla/5.0 (Linux; Android 8.1.0; DUB-LX1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Mobile Safari/537.36', '5bcddfe4bda4ec775b7c89a685bcdf58', 'email', 2, NULL, NULL, NULL, NULL, 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'temp2019@yopmail.com', 0, 'sq632X6aRToKyGquOmPLzZUqTCfzZIrJ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-08 20:33:40'),
(6, 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '1', '', '', '212.49.95.37', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', 'e852ebf2f292461e493c36338a645610', 'Google', 2, 'id_106407046921999763720', NULL, NULL, '', 'Francis Ngari', NULL, 'Francis', 'Ngari', NULL, 'en', NULL, NULL, NULL, NULL, NULL, 'fngari@googlemail.com', 0, 'tHZi8mBEaGzumgsG1XdzsCqf0tKlJIKl', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhY2Nlc3NfdG9rZW4iOiJ5YTI5Lkdsc1FCM25yWnc3YUtoYlZ1YmF6UXZ4REFTUDRBRVN5SUxhUzJqSnRjUGE2dkhGMHIweFA5Z1kzazJDdXE4QUJOQnBsRlk3d2tFS1RfR2E2YzhVMWRmMlBzUTUzQVk0bjNaMnpvbmhnLTFYb21HT19LRmFocmRFUzY3ZXQiLCJ0b2tlbl90eXBlIjoiQmVhcmVyIiwicmVmcmVzaF90b2tlbiI6IjFcLy1fdlQxanlRQ1pVdkc0UWh0WE5NVGFUOWd1Q0ZwNHZBUkRHclIwSUJERlpoREMwdjJNbXdteVFsdVAtZmQ3QXQiLCJleHBpcmVzX2luIjozNjAwLCJleHBpcmVzX2F0IjoxNTU4NDU4MDUwfQ==', '2019-05-21 19:00:59');

-- --------------------------------------------------------

--
-- Table structure for table `tb_attendance`
--

CREATE TABLE `tb_attendance` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `barcode` varchar(200) NOT NULL,
  `names` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `direction` varchar(200) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_attendance`
--

INSERT INTO `tb_attendance` (`_index`, `id`, `event_id`, `type`, `barcode`, `names`, `phone`, `email`, `direction`, `timestamp`) VALUES
(1, '4SErVnm6gh7D4FijoN5nlugTkTqyQWVC', 'FLOekhHX0v', 'INVITE', 'D45S', 'Martin Thuku', '', '', 'IN', '2019-09-11 01:53:54'),
(2, 'npiyjPmpGB7nKzxbIzp4yNe1VpvLx1YP', 'FLOekhHX0v', 'INVITE', 'D45S', 'Martin Thuku', '', '', 'IN', '2019-09-17 14:39:18');

-- --------------------------------------------------------

--
-- Table structure for table `tb_budget`
--

CREATE TABLE `tb_budget` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(1000) NOT NULL DEFAULT '',
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_budget`
--

INSERT INTO `tb_budget` (`_index`, `id`, `event_id`, `title`, `description`, `timestamp`) VALUES
(1, 'RtxC6Qcqy5pFB5ppxq9h2hByw04kBQ0u', 'FLOekhHX0v', 'Test', '', '2019-09-11 01:55:12'),
(2, 'WVswNptdedyplSWGqa1wUISwkx3ZM9eg', 'GdRk51MIWl', 'Transport', '', '2019-10-22 19:15:24'),
(3, 'fJCoF55ATdPuEi3rtA3UziuHyfU5oBmT', '02yMkEIbxS', 'MIKES', 'Test', '2019-09-20 23:34:56'),
(6, 'GoTSrAMR3C98aE0la0m5rCgTRWYLDhLB', 'ITHIUHCE9z', 'TOTAL BUDGET', '100K', '2019-09-24 20:18:42'),
(7, 'Jp0XW66gSprxa6QPtHpNUIZn26dRzC5L', '7IHj6mIEcG', 'Trial', 'XXX', '2019-10-15 15:14:07'),
(8, 'CjhGB4RRyZcrCuAKESyLY2xikUQPERCY', 'suzBez9d5C', 'Transport', 'For Transport', '2019-10-15 20:11:50'),
(9, '10nap8N8VdVvhxWs0S9zjrIKELqKxKn6', 'suzBez9d5C', 'Entertainment', 'For Fun', '2019-10-15 20:12:24'),
(10, 'oLdzjiXm17KNpOPNGdwXoK0EfFCsdO2z', 'GdRk51MIWl', 'Food', '', '2019-10-22 19:15:13'),
(11, '9ScbLDSCG4WaPobGaHwkHeFvJtvr6HHD', 'GdRk51MIWl', 'Services', '', '2019-10-22 19:15:31'),
(12, 'j1p9JvOiZjx3RmnA3VqqurjxgzTtKYI9', 'N4oqpbOqwI', 'Hdhdh', 'Hdhdh', '2019-12-28 13:33:26'),
(13, 'ti2XNBAodyj446fyc538CVAgWDeAJtum', 'N4oqpbOqwI', 'Graduation', '', '2020-01-13 20:00:47'),
(14, '1YPlQ1CCyE5xYU6kAlnLwIM2H0dz66DG', 'V7XCbQzYVv', 'Trial', '', '2020-01-16 21:09:22'),
(15, 'TrgJQCxjFjGZDFH7ZqtmyHUKpjqyZwF5', 'V7XCbQzYVv', 'Xtrial', 'Does this work', '2020-01-17 05:36:10'),
(16, 'WCQwjyufkksCPjWAbVCeQSt0905KFNnO', '0BfvxO3yyD', 'Birthday Budget', '', '2020-03-15 20:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `tb_budget_transactions`
--

CREATE TABLE `tb_budget_transactions` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `budget_id` varchar(200) NOT NULL,
  `type` tinyint(2) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(1000) NOT NULL DEFAULT '',
  `amount` double(20,2) NOT NULL DEFAULT 0.00,
  `reference` varchar(200) NOT NULL DEFAULT '',
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_budget_transactions`
--

INSERT INTO `tb_budget_transactions` (`_index`, `id`, `event_id`, `budget_id`, `type`, `date`, `title`, `description`, `amount`, `reference`, `timestamp`) VALUES
(1, '3JHIFJSLurAS4jYY6d54GTorwRFqfvzE', 'FLOekhHX0v', 'RtxC6Qcqy5pFB5ppxq9h2hByw04kBQ0u', 1, '2019-09-04', 'Cash At Hand', '', 20000.00, '', '2019-09-11 01:55:38'),
(2, 'GUZhsPhIypa0Aj1nOJoAHFuYHXlFZm0V', 'FLOekhHX0v', 'RtxC6Qcqy5pFB5ppxq9h2hByw04kBQ0u', 0, '2019-09-05', 'Vehicle Booking', '', 4000.00, '', '2019-09-11 01:55:57'),
(3, 'Ysfd8ZaGCckShd3KREmISdcnq7YqoAI5', '02yMkEIbxS', 'fJCoF55ATdPuEi3rtA3UziuHyfU5oBmT', 0, '2019-09-21', 'Cakes', 'Mr', 10000.00, 'adsf', '2019-09-20 23:35:20'),
(4, '3KAdnN3OEDBacmXZFB5CFKXvhsQSrszB', 'ITHIUHCE9z', 'GoTSrAMR3C98aE0la0m5rCgTRWYLDhLB', 0, '2019-09-25', 'Cakes', 'SDF', 333.00, 'adsf', '2019-09-24 20:17:35'),
(5, 'qanjyReJhKSdXSs0nF5ATgorQjJ5iFp0', '7IHj6mIEcG', 'Jp0XW66gSprxa6QPtHpNUIZn26dRzC5L', 0, '2019-10-19', 'Cake', '', 60000.00, '', '2019-10-15 15:14:50'),
(6, 'yjmgyQccurkupiMwo4SeQKruewgQAnoZ', 'suzBez9d5C', '10nap8N8VdVvhxWs0S9zjrIKELqKxKn6', 0, '2019-10-16', 'Merc', '', 50000.00, '', '2019-10-15 20:13:11'),
(7, 'UWOCP2qS6jGgLOUqSkDmBsXakbE0ozBs', 'suzBez9d5C', '10nap8N8VdVvhxWs0S9zjrIKELqKxKn6', 0, '2019-10-24', 'Speakers', 'JBL for Sound', 90000.00, '', '2019-10-15 20:13:49'),
(8, 'DtQMd3OJom5ygBNZaUbJhoHV1FsciH9S', 'GdRk51MIWl', 'WVswNptdedyplSWGqa1wUISwkx3ZM9eg', 1, '2019-10-22', 'Topup', '', 30000.00, '', '2019-10-22 19:14:46'),
(9, 't053XastRkl1hI5z8OkcaZbMOGlZiORP', 'N4oqpbOqwI', 'ti2XNBAodyj446fyc538CVAgWDeAJtum', 1, '2020-01-24', 'Wsdf', 'Sdfsdf', 1000000.00, 'asdf', '2020-01-13 20:01:05'),
(10, 'LieHXinFFz4W6DEg9y6H2YyVzDXkx3te', 'N4oqpbOqwI', 'ti2XNBAodyj446fyc538CVAgWDeAJtum', 0, '2020-01-18', 'Transport', 'TAXI', 499999.00, 'DFGDFG', '2020-01-13 20:01:35'),
(11, '0UttHlT6TooYSAYt5CQHmL2IjStyUIUY', 'V7XCbQzYVv', 'TrgJQCxjFjGZDFH7ZqtmyHUKpjqyZwF5', 0, '2020-01-25', 'Cake', '', 50.00, '', '2020-01-17 05:36:45'),
(12, 'd3EKPIm1ogcDR2gsp2RvyhjWkQGWIMw0', 'V7XCbQzYVv', 'TrgJQCxjFjGZDFH7ZqtmyHUKpjqyZwF5', 0, '2020-01-31', 'CAr', '', 100.00, '', '2020-01-17 05:38:07');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cart`
--

CREATE TABLE `tb_cart` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `service_id` varchar(200) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_cart`
--

INSERT INTO `tb_cart` (`_index`, `id`, `uid`, `service_id`, `timestamp`) VALUES
(4, 'NVtHic8NP0yaHFiBeIaGsnvIEbsKwXTq', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Y0864JpMoHg0tTA2V7BrsYO9wsTZLChG', '2019-10-15 19:07:43');

-- --------------------------------------------------------

--
-- Table structure for table `tb_checklist_groups`
--

CREATE TABLE `tb_checklist_groups` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_checklist_groups`
--

INSERT INTO `tb_checklist_groups` (`_index`, `id`, `event_id`, `title`, `description`, `timestamp`) VALUES
(3, 'nUVMlLOQYOsS7SiV69MVKTs6Ad568T87', '02yMkEIbxS', 'Jkj', '', '2019-09-20 23:36:53'),
(5, 'hmuknJYBYVT5bn8Jhi2YdcnbMw1oQipO', 'suzBez9d5C', 'Yengs', 'Good People', '2019-09-24 19:19:40'),
(6, 'cIZIgWeVUOqy1jJd9huMyHz2vNt848ia', 'N4oqpbOqwI', 'Graduation Committee', '', '2019-09-30 10:43:57'),
(7, '8q5AIVvyr0gIszD0UEm6eIDFZ3Z2Q5WM', 'ITHIUHCE9z', 'TESE', 'Ssdfsdf', '2019-10-01 18:44:01'),
(11, '9FmULph7vtx4hR7CGoeRolApNkdOiAqY', '7IHj6mIEcG', 'To do', 'Trial', '2019-10-15 15:21:41'),
(12, '2lxFpV0pfinPM58N15iY3RaCO8uC9FVE', '7IHj6mIEcG', 'Meet up', 'Xxx', '2019-10-15 15:22:05'),
(13, 'gnkgPWbbB9diyI4wL0W1bRCBMQGAO5Fv', 'GdRk51MIWl', 'Hgjhg', 'Jhvhj', '2019-10-22 21:02:26'),
(14, '4qeU8md5Ms7S5xOqlxQiorxUqYDV9YcX', 'FLOekhHX0v', 'Test', '', '2019-10-26 18:06:13');

-- --------------------------------------------------------

--
-- Table structure for table `tb_checklist_items`
--

CREATE TABLE `tb_checklist_items` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `group_id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `text` text NOT NULL,
  `due_date` date DEFAULT NULL,
  `checked` tinyint(2) NOT NULL DEFAULT 0,
  `checked_timestamp` datetime DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_checklist_items`
--

INSERT INTO `tb_checklist_items` (`_index`, `id`, `group_id`, `event_id`, `text`, `due_date`, `checked`, `checked_timestamp`, `timestamp`) VALUES
(8, 'y9d2Pq9GAfuSIQHKN3ahTylqu4ZsI8Yt', 'nUVMlLOQYOsS7SiV69MVKTs6Ad568T87', '02yMkEIbxS', 'Jk', '2019-09-22', 0, '2019-10-26 11:07:15', '2019-09-20 23:37:04'),
(9, '3gOlcWxrJ9rPtZcwPJnIk1rCLkFcPmA5', 'hmuknJYBYVT5bn8Jhi2YdcnbMw1oQipO', 'suzBez9d5C', 'Photo', '2019-09-28', 0, '2019-09-24 19:20:10', '2019-09-24 19:20:10'),
(10, 'ZZ5z1ONKPeOQZWhcffhF0YvUmMAyzYEO', 'hmuknJYBYVT5bn8Jhi2YdcnbMw1oQipO', 'suzBez9d5C', 'Cake', '2019-09-28', 0, '2019-09-24 19:20:44', '2019-09-24 19:20:44'),
(11, 'mx7ZeCNl9ejNGk6EpBY1In2E1rWsfcl2', 'cIZIgWeVUOqy1jJd9huMyHz2vNt848ia', 'N4oqpbOqwI', 'Jhj', '2019-10-04', 0, '2019-09-30 10:45:15', '2019-09-30 10:45:15'),
(12, '8sWY2urJw25s3zN8Fjc5mttNV4uEVEJk', 'cIZIgWeVUOqy1jJd9huMyHz2vNt848ia', 'N4oqpbOqwI', 'Buy a cake', '0000-00-00', 0, '2019-09-30 10:45:26', '2019-09-30 10:45:26'),
(13, 'mjKbHPZGzqEuIbYj3wLO0W2uAdCfVXO3', '8q5AIVvyr0gIszD0UEm6eIDFZ3Z2Q5WM', 'ITHIUHCE9z', 'Sdd', '2019-10-10', 0, '2019-10-01 18:44:09', '2019-10-01 18:44:09'),
(16, 'FmoWPjMLkXBNAyNwO641TUrRh1eOeEMz', '9FmULph7vtx4hR7CGoeRolApNkdOiAqY', '7IHj6mIEcG', 'Trial', '2019-10-19', 0, '2019-10-15 15:21:52', '2019-10-15 15:21:52'),
(17, 'dny6hKjph9QSpUKcTRCVYCEyec6yS2HQ', '2lxFpV0pfinPM58N15iY3RaCO8uC9FVE', '7IHj6mIEcG', 'Ndhd', '2019-10-27', 1, '2019-10-15 15:27:06', '2019-10-15 15:22:15'),
(18, '7cUH3KmwUhcGGFnJACFIjYKBBEoYjoPT', '2lxFpV0pfinPM58N15iY3RaCO8uC9FVE', '7IHj6mIEcG', 'Dnf', '2019-10-20', 1, '2019-10-15 15:27:08', '2019-10-15 15:27:00'),
(19, 'ROlW4G0GpnfWP4dhnZPv7QheFnFoITdg', '4qeU8md5Ms7S5xOqlxQiorxUqYDV9YcX', 'FLOekhHX0v', 'Hello', '0000-00-00', 0, '2019-10-26 18:07:24', '2019-10-26 18:06:20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_events`
--

CREATE TABLE `tb_events` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `type_name` varchar(200) NOT NULL,
  `type_value` int(20) NOT NULL,
  `type_other` varchar(200) NOT NULL DEFAULT '',
  `start_timestamp` datetime NOT NULL,
  `end_timestamp` datetime NOT NULL,
  `description` text DEFAULT NULL,
  `location` tinyint(1) NOT NULL DEFAULT 0,
  `location_name` varchar(200) DEFAULT NULL,
  `location_type` varchar(200) DEFAULT NULL,
  `location_lat` varchar(200) DEFAULT NULL,
  `location_lon` varchar(200) DEFAULT NULL,
  `location_map` tinyint(1) NOT NULL DEFAULT 0,
  `location_map_lat` varchar(200) DEFAULT NULL,
  `location_map_lon` varchar(200) DEFAULT NULL,
  `location_map_type` varchar(200) DEFAULT NULL,
  `location_map_name` varchar(200) DEFAULT NULL,
  `poster` varchar(200) DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `settings` text DEFAULT NULL,
  `guest_groups` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_events`
--

INSERT INTO `tb_events` (`_index`, `id`, `uid`, `name`, `type_name`, `type_value`, `type_other`, `start_timestamp`, `end_timestamp`, `description`, `location`, `location_name`, `location_type`, `location_lat`, `location_lon`, `location_map`, `location_map_lat`, `location_map_lon`, `location_map_type`, `location_map_name`, `poster`, `timestamp`, `status`, `settings`, `guest_groups`) VALUES
(1, 'FLOekhHX0v', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Test', 'Wedding', 1, '', '2019-07-01 18:29:00', '2019-07-01 18:29:00', '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-07-01 18:29:35', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(2, '0QeRarrl3a', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Limos baby shower', 'Baby Shower', 2, '', '2019-07-02 18:30:00', '2019-07-02 19:00:00', '', 1, 'Nairobi', 'COUNTY', '-1.27615', '36.839269', 0, NULL, NULL, NULL, NULL, '2603NuNbsm.jpg', '2019-07-02 18:36:04', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(3, 'hcudO2aNQz', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Franjoe', 'Birthday Party (Adult)', 18, '', '2019-07-10 18:30:00', '2019-07-31 20:00:00', 'Night Party', 0, NULL, NULL, NULL, NULL, 1, '-1.2959325', '36.8067524', 'CURRENT', 'Current Location', '4mMpnUGZMg.jpg', '2019-07-16 18:30:51', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":1,\\\"guests_expected\\\":30,\\\"guests_max\\\":50,\\\"scan_mode\\\":1}', '[]'),
(5, '02yMkEIbxS', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Mike\\\'s 3rd Wedding Event', 'Wedding', 1, '', '2019-07-31 19:26:00', '2019-11-14 19:26:00', 'Wedo time', 1, 'Nairobi', 'COUNTY', '-1.27615', '36.839269', 1, '-1.2754635247116', '36.854718523926', 'DRAG', 'Custom', '8P762kOu4t.jpg', '2019-10-26 15:34:34', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":1,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(6, 'gDQdrhMBqG', '5S7WRHYAvLXpYTjq9ukhaKtWEsSQEl1d', 'PaRTy', 'First Communion', 19, '', '2019-07-11 01:00:00', '2019-07-11 01:00:00', '', 1, 'Nairobi', 'COUNTY', '-1.27615', '36.839269', 0, NULL, NULL, NULL, NULL, 'b9P3xStCau.jpeg', '2019-07-08 09:24:41', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":1,\\\"table\\\":0,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":0,\\\"guests_expected\\\":20,\\\"guests_max\\\":200,\\\"scan_mode\\\":1}', '{\\\"vw5qme0fridjxunlct7\\\":{\\\"id\\\":\\\"vw5qme0fridjxunlct7\\\",\\\"name\\\":\\\"VIP\\\",\\\"limit\\\":5,\\\"description\\\":\\\"Very important people\\\"}}'),
(7, 'TXXQ5jc5vs', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'Test', 'Custom', 0, '', '2019-07-09 06:17:00', '2019-07-09 06:17:00', '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'yLu9p8HlJj.jpg', '2019-07-09 06:17:53', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(8, 'COa5T1FiBF', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Open house day', 'Custom', 0, '', '2019-07-20 18:42:00', '2019-07-21 18:42:00', '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'sqDlRZFZob.png', '2019-07-16 18:49:05', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(9, 'FWPPyx3cIn', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Ketepa party', 'Custom', 0, '', '2019-07-31 18:20:00', '2019-08-02 18:20:00', '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'NZylQV5usf.JPG', '2019-07-30 18:23:36', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(10, '5ZGufM3xNS', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Mike wedding', 'Wedding', 1, '', '2019-08-29 18:36:00', '2019-08-29 18:36:00', '', 1, 'Current Location', 'CURRENT', '-1.296121', '36.8064935', 0, NULL, NULL, NULL, NULL, 'M6pIfX5fss.jpg', '2019-08-06 18:38:08', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(11, 'NvdgZSjpfu', 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', 'LIMO\\\'S BIG WEDDING', 'Wedding', 1, '', '2019-08-31 19:27:00', '2019-08-31 23:30:00', 'Biggest day of my life', 1, 'Mombasa', 'COUNTY', '-4.035931', '39.662767', 0, NULL, NULL, NULL, NULL, 'nB4nJfLiRg.jpg', '2019-08-20 19:31:03', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":200,\\\"guests_max\\\":250,\\\"scan_mode\\\":1}', '{\\\"fv16ivw3ubsjzk1n8ac\\\":{\\\"id\\\":\\\"fv16ivw3ubsjzk1n8ac\\\",\\\"name\\\":\\\"Executive Committee\\\",\\\"limit\\\":30,\\\"description\\\":\\\"Fundraisers\\\"},\\\"x10ahy0p2sjzk1nsw8\\\":{\\\"id\\\":\\\"x10ahy0p2sjzk1nsw8\\\",\\\"name\\\":\\\"Brides Maids\\\",\\\"limit\\\":10,\\\"description\\\":\\\"\\\"},\\\"yndlm1hhtzejzk1o3w7\\\":{\\\"id\\\":\\\"yndlm1hhtzejzk1o3w7\\\",\\\"name\\\":\\\"Grooms men\\\",\\\"limit\\\":10,\\\"description\\\":\\\"\\\"}}'),
(12, 'GdRk51MIWl', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Bbay K Bday', 'Custom', 0, '', '2020-04-27 00:00:00', '2020-04-27 02:00:00', '', 1, 'Nairobi', 'COUNTY', '-1.27615', '36.839269', 0, NULL, NULL, NULL, NULL, NULL, '2019-10-15 20:21:38', 1, '{\\\"last_name\\\":0,\\\"phone\\\":0,\\\"email\\\":0,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":0,\\\"rsvp\\\":0,\\\"reviews\\\":0,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(13, 'm6NgsROCZx', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', 'Dee', 'Bar Mitzvah', 3, '', '2019-09-01 02:00:00', '2019-09-01 02:00:00', '1123r4b', 1, 'Mombasa', 'COUNTY', '-4.035931', '39.662767', 1, '-4.035931', '39.662767', 'COUNTY', 'Mombasa', 'wFKZkHGvP4.jpg', '2019-08-28 22:23:50', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":1,\\\"guests_expected\\\":50,\\\"guests_max\\\":80,\\\"scan_mode\\\":1}', '[]'),
(14, 'KlY5n4TdXX', '35DrzmfG2VxxM7NC06gH6HZymJgyRhsD', 'Dee', 'Wedding', 1, '', '2019-10-06 11:01:00', '2019-10-06 11:01:00', 'Wedding', 1, 'Nairobi', 'COUNTY', '-1.27615', '36.839269', 0, NULL, NULL, NULL, NULL, 'XkOSKhaKPc.jpeg', '2019-09-20 11:06:13', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":100,\\\"guests_max\\\":120,\\\"scan_mode\\\":1}', '[]'),
(15, '9TtdqhirAQ', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', 'Diana', 'Wedding', 1, '', '2019-10-23 10:30:00', '2019-10-23 16:30:00', 'Diana\\\'s wedding.', 1, 'Nairobi', 'COUNTY', '-1.27615', '36.839269', 1, '-1.2784278426751', '36.803340911865', 'CLICK', 'Custom', NULL, '2019-09-20 13:04:01', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":1,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":80,\\\"guests_max\\\":100,\\\"scan_mode\\\":1}', '{\\\"j0nnt2sn4adk0rygamf\\\":{\\\"id\\\":\\\"j0nnt2sn4adk0rygamf\\\",\\\"name\\\":\\\"Family\\\",\\\"limit\\\":20,\\\"description\\\":\\\"Close family members\\\"},\\\"ccrgefopw5mk0rygqfe\\\":{\\\"id\\\":\\\"ccrgefopw5mk0rygqfe\\\",\\\"name\\\":\\\"Friends\\\",\\\"limit\\\":30,\\\"description\\\":\\\"Close friends to bride and groom\\\"}}'),
(16, 'ITHIUHCE9z', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'BLUE GOLF EVENT', 'Golf Event', 30, '', '2019-10-03 00:00:00', '2019-10-03 22:30:00', 'GOlf & Nyama and networking', 1, 'Karen', 'CITY', '-1.312354', '36.709661', 1, '-1.3412345', '36.7151861', 'PLACE', 'Karen Country Club', 'ZNcL2ZCFgg.jpg', '2019-09-24 19:07:48', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":100,\\\"guests_max\\\":100,\\\"scan_mode\\\":1}', '[]'),
(17, 'suzBez9d5C', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Inauguration of HE F.NGARI', 'Opening Ceremony', 34, '', '2019-10-20 00:00:00', '2019-10-20 00:00:00', 'INAUGURATION OF F.NGARI AS PRESIDENT', 1, 'Nairobi', 'COUNTY', '-1.27615', '36.839269', 1, '-1.27615', '36.839269', 'COUNTY', 'Nairobi', 'GICVNLlFfq.jpg', '2019-10-15 19:27:30', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":80,\\\"guests_max\\\":100,\\\"scan_mode\\\":1}', '[]'),
(18, 'N4oqpbOqwI', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Diana\\\'s graduation party', 'Bar Mitzvah', 3, '', '2020-10-06 10:37:00', '2020-10-06 10:37:00', 'Diana\\\'s Graduation party. Venue Langata.', 0, 'Karani House, Eastleigh Second Ave, Nairobi, Kenya', 'DRAG', '0', '0', 1, '-1.2718071238727', '36.849923435132', 'DRAG', 'Karani House, Eastleigh Second Ave, Nairobi, Kenya', NULL, '2020-03-10 18:39:33', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":10,\\\"guests_max\\\":15,\\\"scan_mode\\\":1}', '[]'),
(19, 'kCHMIsV33L', 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', 'Julius Birthday', 'Birthday Party (Kids)', 17, '', '2019-10-12 11:25:00', '2019-10-12 19:00:00', '', 1, 'Kiambu', 'COUNTY', '-1.08135', '36.785095', 1, '-1.0624705530328', '36.780975126953', 'DRAG', 'Custom', '0CbGS7XjgV.jpg', '2019-10-08 11:26:45', 1, '{\\\"last_name\\\":1,\\\"phone\\\":0,\\\"email\\\":0,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(23, '7IHj6mIEcG', 'rUemiRfRkPmwyIRNTN73NF6WstWILaD5', 'Test', 'Custom', 0, '', '2019-10-27 15:08:00', '2019-10-27 15:08:00', 'Parte', 1, 'Nairobi', 'COUNTY', '-1.27615', '36.839269', 0, NULL, NULL, NULL, NULL, NULL, '2019-10-15 15:11:24', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(24, 'kT71fAEiFz', 'rUemiRfRkPmwyIRNTN73NF6WstWILaD5', 'K party', 'Baby Shower', 2, '', '2019-10-20 17:05:00', '2019-10-20 21:00:00', '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-10-15 17:06:17', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(25, 'dyFjhdEqyC', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'MASHUJAA', 'Christening', 11, '', '2019-10-31 18:57:00', '2019-11-01 18:57:00', 'MASHUJAA DAY', 1, 'Kasarani', 'CITY', '-1.221802', '36.908058', 1, '42.6977082', '23.3218675', 'PLACE', 'Sofia', NULL, '2019-10-22 20:44:24', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(26, 'LYcPuI1yLX', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Xx', 'Wedding', 1, '', '2019-11-01 11:00:00', '2019-11-01 11:00:00', '', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'VKl6z0HLDg.jpeg', '2019-10-26 11:03:10', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":0,\\\"company\\\":0,\\\"table\\\":0,\\\"diet\\\":0,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":0,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":0,\\\"invite_facebook\\\":0,\\\"invite_twitter\\\":0,\\\"pass_question\\\":0,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(27, 'aQ5qdJiNfi', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'KK Dedication', 'Other', 0, 'Dedication', '2019-10-30 18:44:00', '2019-10-30 20:30:00', 'Come one come all', 0, 'Kilimani', 'PLACE', '0', '0', 1, '-1.2892786', '36.7869311', 'PLACE', 'Kilimani', NULL, '2019-10-30 20:24:21', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":10,\\\"guests_max\\\":15,\\\"scan_mode\\\":1}', '[]'),
(28, 'V7XCbQzYVv', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Test 1', 'Bar Mitzvah', 3, '', '2020-01-26 16:42:00', '2020-01-26 20:00:00', '', 0, 'Nairobi', 'DEFAULT', '0', '0', 1, '-1.2835201', '36.8218568', 'DEFAULT', 'Nairobi', NULL, '2019-11-05 16:43:15', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(29, 'kH6rzBHgZL', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Pepea Launch 1', 'Product Launch', 35, '', '2020-01-24 09:00:00', '2020-01-31 09:00:00', 'Pepea Studios. Uberizing Photo print', 0, 'Viva Lounge', 'PLACE', '0', '0', 1, '-1.2758847', '36.7888939', 'PLACE', 'Viva Lounge', 'event_poster_NqkVbeiyFd.png', '2020-01-13 18:37:21', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":560,\\\"guests_max\\\":60,\\\"scan_mode\\\":1}', '[]'),
(30, '0BfvxO3yyD', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'KK Birthday', 'Birthday Party (Kids)', 17, '', '2020-04-25 13:00:00', '2020-04-25 18:00:00', 'Come celebrate baby K\\\'s first birthday', 0, 'TRM - Thika Road Mall', 'PLACE', '0', '0', 1, '-1.2198906', '36.889213', 'PLACE', 'TRM - Thika Road Mall', 'event_poster_7jTp1TopQ9.png', '2020-03-13 00:07:28', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":20,\\\"guests_max\\\":20,\\\"scan_mode\\\":1}', '[]'),
(31, 'gR9LJ566YQ', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'KK Birthday', 'Other', 0, '1st bday', '2020-04-25 00:08:00', '2020-04-25 00:08:00', 'Come celebrate baby K\\\'s first birthday', 0, 'TRM - Thika Road Mall', 'PLACE', '0', '0', 1, '-1.2198906', '36.889213', 'PLACE', 'TRM - Thika Road Mall', 'event_poster_e7E8K69MPn.png', '2020-03-13 00:11:06', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":20,\\\"guests_max\\\":20,\\\"scan_mode\\\":1}', '[]'),
(32, 'bk1cvYJOSZ', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Test', 'Wedding', 1, '', '2020-04-26 17:08:00', '2020-04-26 17:08:00', '', 0, 'Nairobi', 'DEFAULT', '0', '0', 1, '-1.2835201', '36.8218568', 'DEFAULT', 'Nairobi', NULL, '2020-04-26 17:08:39', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":0,\\\"guests_max\\\":0,\\\"scan_mode\\\":1}', '[]'),
(33, '4DW8YTpC69', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Birthday Party', 'Birthday Party (Adult)', 18, '', '2021-06-24 14:33:00', '2021-06-24 14:33:00', '18th birthday', 0, 'Karen Country Club', 'PLACE', '0', '0', 1, '-1.341192', '36.715186', 'PLACE', 'Karen Country Club', NULL, '2021-06-10 14:35:10', 1, '{\\\"last_name\\\":1,\\\"phone\\\":1,\\\"email\\\":1,\\\"address\\\":1,\\\"company\\\":1,\\\"table\\\":1,\\\"diet\\\":1,\\\"type\\\":1,\\\"rsvp\\\":1,\\\"reviews\\\":1,\\\"extend\\\":1,\\\"invite_email\\\":1,\\\"invite_sms\\\":1,\\\"invite_whatsapp\\\":1,\\\"invite_facebook\\\":1,\\\"invite_twitter\\\":1,\\\"pass_question\\\":1,\\\"guests_expected\\\":20,\\\"guests_max\\\":30,\\\"scan_mode\\\":1}', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `tb_event_guests`
--

CREATE TABLE `tb_event_guests` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `barcode` varchar(200) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `group_id` varchar(200) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `company` varchar(200) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `diet` varchar(1000) DEFAULT NULL,
  `table_no` varchar(200) DEFAULT NULL,
  `extend_inv` tinyint(1) DEFAULT NULL,
  `expected_guests` int(20) NOT NULL DEFAULT 1,
  `inv_phone` varchar(200) DEFAULT NULL,
  `inv_email` varchar(200) DEFAULT NULL,
  `inv_message` varchar(1000) DEFAULT NULL,
  `inv_question` varchar(200) DEFAULT NULL,
  `inv_answer` varchar(200) DEFAULT NULL,
  `inv_edits` tinyint(1) NOT NULL DEFAULT 0,
  `status_sms` varchar(200) DEFAULT NULL,
  `status_email` varchar(200) DEFAULT NULL,
  `status_whatsapp` varchar(200) DEFAULT NULL,
  `status_fb` varchar(200) DEFAULT NULL,
  `status_twitter` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `time_created` datetime NOT NULL,
  `time_invited` datetime DEFAULT NULL,
  `time_confirmed` datetime DEFAULT NULL,
  `time_modified` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_event_guests`
--

INSERT INTO `tb_event_guests` (`_index`, `id`, `event_id`, `uid`, `barcode`, `first_name`, `last_name`, `type`, `group_id`, `phone`, `email`, `company`, `address`, `diet`, `table_no`, `extend_inv`, `expected_guests`, `inv_phone`, `inv_email`, `inv_message`, `inv_question`, `inv_answer`, `inv_edits`, `status_sms`, `status_email`, `status_whatsapp`, `status_fb`, `status_twitter`, `status`, `time_created`, `time_invited`, `time_confirmed`, `time_modified`) VALUES
(1, 'QHCw2qrSBW', 'TXXQ5jc5vs', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'JXKC', 'Martin', 'Thuku', 0, '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 0, '2019-07-15 04:24:25', NULL, NULL, '2019-07-15 04:24:25'),
(2, '3YS4EzliTc', 'hcudO2aNQz', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'RKR5', 'Limo', 'Sadalla', 0, '', '071146150', 'limosadalla@blueconsulting.co.ke', '', '', '', '', 0, 1, '254711466150', 'carbonblue45@gmail.com', '', '', '', 0, '', '', '', '', '', 0, '2019-07-16 18:56:37', NULL, NULL, '2019-07-16 18:56:37'),
(3, 'DXiIPzxlNH', 'hcudO2aNQz', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'UYG3', 'Francis', 'Ngari', 0, '', '0788846834', 'fngari@gmail.com', '', '', '', '', 0, 1, '254788846834', 'fngari@gmail.com', '', '', '', 0, '', '', '', '', '', 0, '2019-07-16 18:57:54', NULL, NULL, '2019-07-16 18:57:54'),
(4, 'NKMjKXnQ9w', 'COa5T1FiBF', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'LAE0', 'Michael', 'Nganga', 0, '', '0701029545', 'mikinyas@yahoo.com', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 0, '2019-07-16 18:58:50', NULL, NULL, '2019-07-16 18:58:50'),
(5, 'Qe51oWECBi', 'FLOekhHX0v', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'D45S', 'Martin', 'Thuku', 0, '', '', '', '', '', '', '', 0, 1, '', 'xthukuh@gmail.com', '', '', '', 0, '', 'SENT', '', '', '', 1, '2019-07-27 01:45:58', '2019-09-19 21:42:04', NULL, '2019-09-19 21:41:47'),
(6, 'c4YmqpjfUo', 'FLOekhHX0v', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'AWLK', 'Jane', 'Doe', 0, '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 0, '2019-07-27 01:46:07', NULL, NULL, '2019-07-27 01:46:07'),
(7, 'DZ1ICa3IYA', 'FLOekhHX0v', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '7MOE', 'John', 'Doe', 0, '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 0, '2019-07-27 01:46:19', NULL, NULL, '2019-07-27 01:46:19'),
(9, 'NXO0XmaJGZ', 'FWPPyx3cIn', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'IW6X', 'Michael', 'Nganga', 0, '', '', 'mikinyas@yahoo.com', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 0, '2019-07-30 18:24:25', NULL, NULL, '2019-07-30 19:12:34'),
(10, 'TmUWzTgwlS', '5ZGufM3xNS', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'MOMG', 'Michael', 'Nganga', 2, '', '0711466150', 'lsadalla@blueconsulting.co.ke', '', '', '', '', 0, 1, '254711466150', 'lsadalla@blueconsulting.co.ke', 'Wazi', '', '', 1, '', '', '', '', '', 0, '2019-08-06 18:39:45', NULL, NULL, '2019-08-06 18:39:45'),
(11, '4vJGhBnrcy', 'NvdgZSjpfu', 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', 'FW23', 'Mike', 'Nganga', 2, 'yndlm1hhtzejzk1o3w7', '0701029545', 'mikinyas@yahoo.com', 'FF', '33', 'NO Beer', '1', 1, 2, '254701029545', 'mikinyas@yahoo.com', 'Welcome to my party!', 'What is my fav color?', 'Yellow', 1, '', '', '', '', '', 0, '2019-08-20 19:39:05', NULL, NULL, '2019-08-20 19:39:05'),
(12, 'D3qa678ShM', 'GdRk51MIWl', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'W70D', 'Sadalla', '', 0, '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 1, '2019-08-27 19:17:21', '2019-08-27 19:17:21', NULL, '2019-08-27 19:17:21'),
(13, 'OWSgydQM3e', 'GdRk51MIWl', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'WQPK', 'Ngari', '', 0, '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 1, '2019-08-27 19:17:29', '2019-08-27 19:17:29', NULL, '2019-08-27 19:17:29'),
(14, 'kQxoaRrmIu', 'GdRk51MIWl', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'YWI7', 'Njesh', '', 0, '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 0, '2019-09-12 09:05:02', NULL, NULL, '2019-09-12 09:05:02'),
(15, 'YoGfH99Aon', 'GdRk51MIWl', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Y4DI', 'Limo', '', 0, '', '', '', '', '', '', '', 0, 1, '254701029545', 'mikinyas@yahoo.com', '', '', '', 0, '', 'SENT', '', '', '', 1, '2019-09-24 18:22:53', '2019-09-24 18:23:22', NULL, '2019-09-24 18:22:53'),
(16, 'IXC1bKxPB2', 'GdRk51MIWl', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'BDNV', 'Njeshmemen', '', 0, '', '', '', '', '', '', '', 0, 1, '254708830324', 'cheriwamuturi@gmail.com', 'Dress code is jeans and boots', '', '', 0, '', 'SENT', '', '', '', 1, '2019-09-24 18:29:38', '2019-10-22 19:40:18', NULL, '2019-09-24 18:29:38'),
(17, 'UW1DJ3NeXx', 'ITHIUHCE9z', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '0H2A', 'Michael', 'Kinyanjui', 2, '', '0700776655', 'mikinyas@yahoo.com', '', '', 'No beerNo milk', '', 1, 12, '254700776655', 'mikinyas@yahoo.com', 'Welcome', 'Limo\\\'s birthday', 'Sept', 1, '', 'SENT', '', '', '', 1, '2019-09-24 19:10:06', '2019-09-24 19:13:51', NULL, '2019-09-24 19:10:06'),
(18, 'wdUZqFA6Ev', 'ITHIUHCE9z', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '16UY', 'Francis', 'Ngari', 0, '', '0788846834', 'fngari@gmail.com', '', '', 'NO MILK', '', 1, 1, '254788846834', 'fngari@gmail.com', '', '', '', 0, '', 'SENT', '', '', '', 1, '2019-09-24 19:11:14', '2019-09-24 19:13:29', NULL, '2019-09-24 19:11:14'),
(19, '7kqwqI0hE0', 'suzBez9d5C', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'D1QA', 'Limo', 'Sandi', 0, '', '0711466150', 'lsadalla@blueconsulting.co.ke', '', '', '', '', 0, 1, '254711466150', 'lsadalla@blueconsulting.co.ke', 'Hawayu', 'Where', 'Majuu', 1, '', 'SENT', '', '', '', 1, '2019-09-24 19:24:35', '2019-09-24 19:28:28', NULL, '2019-09-24 19:24:35'),
(20, 'UnCQ0VNGsL', 'suzBez9d5C', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '5H4S', 'Michael', 'Kinyas', 0, '', '0700776655', 'mikinyas@yahoo.com', '', '', '', '', 0, 1, '254700776655', 'mikinyas@yahoo.com', 'Wam;ambez', 'Wamlambez', 'Wanyonyez', 1, '', 'SENT', '', '', '', 1, '2019-09-24 19:26:40', '2019-09-24 19:28:04', NULL, '2019-09-24 19:26:40'),
(21, 'P8fF5nDdxf', 'suzBez9d5C', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'QH9S', 'Francis', 'Ngari', 0, '', '0788846834', 'fngari@gmail.com', '', '', '', '', 0, 1, '254788846834', 'fngari@gmail.com', 'Testing', 'Kingston', 'Jamaica', 1, '', 'SENT', '', '', '', 1, '2019-09-24 19:52:41', '2019-09-24 19:54:01', NULL, '2019-09-24 19:52:41'),
(22, 'n1RnKOcENr', 'N4oqpbOqwI', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Y2J5', 'Diana', '', 0, '', '', '', '', '', '', '', 0, 1, '254700043911', '', '', '', '', 0, '', '', '', '', '', 0, '2019-09-30 10:48:21', NULL, NULL, '2019-09-30 10:48:21'),
(23, 'BU8390VfiJ', 'kCHMIsV33L', 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', 'WJNI', 'Limo', 'Sadalla', 0, '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 0, '2019-10-08 11:27:14', NULL, NULL, '2019-10-08 11:27:14'),
(24, 'UEAJgsRjN1', '7IHj6mIEcG', 'rUemiRfRkPmwyIRNTN73NF6WstWILaD5', 'L9VT', 'Kenzo', 'K', 1, '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', 1, '', '', '', '', '', 0, '2019-10-15 15:41:40', NULL, NULL, '2019-10-15 15:41:40'),
(25, '9HphO4AAJ1', 'kH6rzBHgZL', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'SVQ0', 'Limo', 'Kibinot', 1, '', '0711466150', 'lsadalla@blueconsulting.co.ke', 'Blue Consulting', 'Woodlands Business Park, 6th Floor, Off Kimbere Road', 'White Meat Only', '54', 1, 10, '254711466150', 'lsadalla@blueconsulting.co.ke', 'Blue Gate', 'Where do you Live', 'San Francisco', 0, '', 'SENT', '', '', '', 2, '2019-11-09 13:27:02', '2020-01-13 18:49:19', '2020-01-13 18:50:55', '2019-11-09 13:32:30'),
(26, 'CrHBuufBtu', 'QyAVdptXmJ', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'DL1S', 'Bee', 'Mn', 0, '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 0, '2019-11-13 20:52:51', NULL, NULL, '2019-11-13 20:52:51'),
(27, 'aV5zss8Dkk', 'N4oqpbOqwI', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'YAQ3', 'Mike', 'M', 2, '', '93939393', 'mikinyas@yahoo.com', '', '', '', '', 0, 1, '', 'mikinyas@yahoo.com', '', '', '', 0, '', 'SENT', '', '', '', 2, '2020-01-13 18:43:02', '2020-01-13 18:50:22', '2020-01-13 18:51:37', '2020-01-13 18:50:15'),
(28, 'Nu9cekxOgR', 'N4oqpbOqwI', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'WMHU', 'Francis', 'Ngari', 0, '', '89383483', 'fngari@gmail.com', 'RIL', '9393', 'Meat', '33', 0, 6, '254711466150', 'fngari@gmail.com', '', '', '', 0, '', 'SENT', '', '', '', 2, '2020-01-13 18:48:03', '2020-01-13 18:49:58', '2020-01-13 18:51:02', '2020-01-13 18:48:03'),
(29, 'T0ydMYYohG', 'kH6rzBHgZL', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'ZRTR', 'Limo', 'Sadalla', 0, '', '0788846834', 'lsadalla@blueconsulting.co.ke', 'Blue Consulting', '', '', '', 0, 1, '254788846834', 'lsadalla@blueconsulting.co.ke', 'Karibu Pepea Launch. Carry your camera', 'Which car are you buying?', 'A8', 1, '', '', '', '', '', 1, '2020-01-13 18:48:48', '2020-01-13 18:48:48', NULL, '2020-01-13 18:48:48'),
(30, 'kosVivGYWo', 'bk1cvYJOSZ', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'M6DV', 'Kimemia', 'Mundu', 0, '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', 0, '', '', '', '', '', 0, '2020-04-26 17:09:36', NULL, NULL, '2020-04-26 17:09:36');

-- --------------------------------------------------------

--
-- Table structure for table `tb_event_users`
--

CREATE TABLE `tb_event_users` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `names` varchar(200) NOT NULL,
  `role` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_event_users`
--

INSERT INTO `tb_event_users` (`_index`, `id`, `event_id`, `uid`, `email`, `username`, `names`, `role`, `status`, `timestamp`) VALUES
(1, 'vTzVEFBZDB0pu8tfzZKQHcUDyl5t19lE', 'QyAVdptXmJ', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'xthukuh@gmail.com', 'omar', 'Martin', 0, 1, '2019-11-06 13:35:07'),
(2, 'NAic58L7jwXVeoBulrVbgpSErA8nG9ta', 'QyAVdptXmJ', '', 'linda.maina@icloud.com', 'njesh', 'Linda', 1, 0, '2019-11-05 11:57:16'),
(3, 'MlmSBOZDK6arDcNtmSnwxpwZ4lrfeWhQ', 'V7XCbQzYVv', '', 'linda.maina@icloud.com', 'njesh', 'Linda', 0, 0, '2019-11-05 16:43:41'),
(4, 'uVgW5hsIhhmXDujrN5Gf0IcqdVyvhVva', 'V7XCbQzYVv', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'cheriwamuturi@gmail.com', 'che', 'Njeri', 1, 1, '2019-11-05 16:52:57'),
(5, 'NCY2FSjbCjLVWvazBQTOAcnMJHQDvjDp', 'kH6rzBHgZL', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'mikinyas@yahoo.com', 'mganga', 'Michael', 1, 1, '2019-11-09 09:48:58'),
(6, 'DtLqrNMbhVEYsMkFctGjTxkVqUIx0lV7', 'kH6rzBHgZL', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'lsadalla@blueconsulting.co.ke', 'carbonblue', 'Limo Sadalla', 2, 1, '2019-11-09 09:50:30'),
(7, 'XpfTLY6ugJbBueWO9xqtgl4M45NVYC0m', 'kH6rzBHgZL', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'tupangelimited@yahoo.com', 'tupangeltd', 'Tupange Limited', 0, 1, '2019-11-09 09:55:17'),
(8, '2li0i6TpFbl0HsMITMlKxPtxiifQdlTP', 'kH6rzBHgZL', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'vurzakurta@enayu.com', 'vuda', 'Vuda', 2, 1, '2019-11-09 10:00:12'),
(9, 'OsFUbajFZcJSVt2K82Vogd2q2SXrSK7c', 'kH6rzBHgZL', 'EE755TUD404ZFcCOfGIJcrhTJ9Wp1dl8', 'cheriwamuturi@yahoo.com', 'njesh', 'Linda Njeri', 1, 1, '2019-11-09 10:11:18'),
(10, 'Tsq4S8Pr3nPv3DRJMVVkSDj5vySjHCkx', 'kH6rzBHgZL', '', 'mikinyas@yahoo.com', 'mgangas', 'Kagogo', 2, 0, '2019-11-09 10:44:34'),
(12, 'slX2dfbT5lpTDUjpbG3G0fIHE0eUgQsR', 'N4oqpbOqwI', '', 'dianacheserem@gmail.com', 'dee', 'Diana Cheserem', 0, 0, '2019-12-03 10:10:51'),
(13, 'q1s4HPrmyno85vzbu4uoFfIyJhcznDiy', 'N4oqpbOqwI', '', 'mikinyas@yahoo.com', 'mikinyas', 'Michael', 0, 0, '2020-01-13 19:08:12'),
(14, 'C4WYtOVVJ9fEPQAqagNsWgD2OhQ5PupK', 'N4oqpbOqwI', '', 'fngari@gmail.com', 'fngari', 'Francis Ngrari', 0, 0, '2020-01-13 19:10:08'),
(15, '9limlfve1Jta2W8n0sX9Au8dlAkxQ3tz', 'N4oqpbOqwI', '', 'carbonblue45@gmail.com', 'carbon', 'Limo', 0, 0, '2020-01-13 19:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `tb_favorites`
--

CREATE TABLE `tb_favorites` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `service_id` varchar(200) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_favorites`
--

INSERT INTO `tb_favorites` (`_index`, `id`, `uid`, `service_id`, `timestamp`) VALUES
(4, 'gYPnmttifxbqtTyJhJlZUGLuNCCy9KRj', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'ZPYWTyfEQZFsoCa2ItX8CYrsoxBLRiei', '2019-08-16 16:30:28'),
(5, 'tAjCoY4M41AfSky4MqMCCVH7QIZhl4nb', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Q2MyzMua9FzCqfA0PDzT0jO4jPJF7pw8', '2019-08-16 16:30:35'),
(7, 'omJ4swaiaGlqrCj03Zn9uwO1uaCMKSLg', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', '2019-09-20 23:31:17'),
(9, 'xadSKsaseuvSdhMaYPp186MSWP3cdHY3', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', '2019-09-24 19:44:36'),
(10, 'eTE11HXgJ62EKnzIc8if2K5yjHHw6pYE', 'rUemiRfRkPmwyIRNTN73NF6WstWILaD5', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', '2019-10-15 17:31:43'),
(11, 'XdlYroiKmsukXkEVTnS91xsCNP00YLcn', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Y0864JpMoHg0tTA2V7BrsYO9wsTZLChG', '2019-10-26 12:46:33'),
(12, 'kzu1bSZ9ta7jts9Xx2nQyNF1YgsOy2wW', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', '2019-10-26 12:46:35'),
(14, '10ok2q3wuleAYnTe2gM6YePzETe9IlCo', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'ZPYWTyfEQZFsoCa2ItX8CYrsoxBLRiei', '2019-10-26 13:19:01');

-- --------------------------------------------------------

--
-- Table structure for table `tb_gifts`
--

CREATE TABLE `tb_gifts` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(1000) NOT NULL DEFAULT '',
  `value` double(20,2) NOT NULL DEFAULT 0.00,
  `received_date` date NOT NULL,
  `received_by` varchar(200) NOT NULL,
  `received_from` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `notes` text NOT NULL,
  `image` longtext NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_gifts`
--

INSERT INTO `tb_gifts` (`_index`, `id`, `event_id`, `title`, `description`, `value`, `received_date`, `received_by`, `received_from`, `phone`, `email`, `notes`, `image`, `timestamp`) VALUES
(1, 'Xv7mFxu52QyeAWusAb0L3NVPjT2FFUhN', '7IHj6mIEcG', 'My Gift', 'Wishlist', 10000.00, '2019-10-12', 'Me', 'You', '', '', '', '', '2019-10-15 16:18:59');

-- --------------------------------------------------------

--
-- Table structure for table `tb_messages`
--

CREATE TABLE `tb_messages` (
  `_index` int(11) NOT NULL,
  `id` varchar(200) NOT NULL,
  `sender` varchar(200) NOT NULL,
  `recipient` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `attachments` longtext NOT NULL,
  `time_sent` datetime NOT NULL DEFAULT current_timestamp(),
  `time_read` varchar(200) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_messages`
--

INSERT INTO `tb_messages` (`_index`, `id`, `sender`, `recipient`, `message`, `attachments`, `time_sent`, `time_read`, `status`, `timestamp`) VALUES
(1, 'V4ops5PtNmzIFMjaknrs31g2scOUuno3', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Testing message', '[]', '2019-09-19 21:40:40', '', 1, '2019-09-24 18:16:33'),
(2, '6SgWuwBX9OFCjr0Kq2l23H92B5LgwLzO', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Hi', '[]', '2019-09-20 23:31:26', '', 1, '2019-09-24 18:17:09'),
(3, 'lplmhqMFJESoxrRFSceexgGKW8JL7SKi', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Did you get tnis?', '[]', '2019-09-20 23:32:44', '', 1, '2019-09-24 18:17:09'),
(4, 'j2fyWnWpMyOyVhZBbrqjhhtXMkTsV52F', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'Hello world!', '[]', '2019-09-21 07:42:45', '', 1, '2019-09-21 07:42:56'),
(5, 'lBqI1SYOe7BYPieFGCSNWkbvk1pEDwq7', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Hello world!', '[]', '2019-09-21 07:43:04', '', 1, '2019-09-21 07:43:07'),
(6, 'B5w4Fbcwg6AK0lLcbmp05fRRIf5TVqf0', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', '', '[{\\\"type\\\":\\\"order\\\",\\\"name\\\":\\\"Order: Zuri Flowers 20,000.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"0O5bNTOhy3tcrVauPJirEAdjy3j5V4at\\\",\\\"ref\\\":\\\"REF001\\\",\\\"service\\\":\\\"YpKbve8t66PZB6Ms4482pRarHfQgCOED\\\",\\\"title\\\":\\\"Zuri Flowers\\\",\\\"category\\\":\\\"Flowers\\\",\\\"agreement\\\":\\\"Testing order\\\",\\\"image\\\":\\\"https:\\\\/\\\\/tupange.naicode.com\\\\/uploads\\\\/jYdqv74bJx.jpeg\\\",\\\"price\\\":20000}}]', '2019-09-21 07:43:34', '', 1, '2019-09-21 07:43:37'),
(7, 'Iw6eKY14K4Pb85jDTOeOBnLI2AzuGvDF', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'ORDER ACCEPTED: AN2YA REF: REF001 Zuri Flowers 20,000.00/=', '[]', '2019-09-21 07:43:43', '', 1, '2019-09-21 07:43:45'),
(8, 'JfZmEet00ISKLd8F2aRBD2NXevfMAr9n', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Hi limo. Unfortunately we can\\\'t offer our services since we are fully booked.  At your next wedding, we got you', '[]', '2019-09-24 18:18:00', '', 1, '2019-09-24 18:51:30'),
(9, 'lepTLYhesuANxXoXYsm65BNy2GYdvjLd', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Hi. Please send me a quote', '[]', '2019-09-24 18:45:28', '', 1, '2019-09-24 18:46:08'),
(10, 'DC6t8WLNpKkpqUcI4hszORWwL82xA0FD', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'My Thank you for the order. We take high quality photos.', '[]', '2019-09-24 18:46:49', '', 1, '2019-09-24 18:47:28'),
(11, 'J8AovaLZji7wvv2iLOXoKtXde4NHUI5a', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '', '[{\\\"type\\\":\\\"quote\\\",\\\"name\\\":\\\"Quote: FRD3H - Pepea Studio 50,000.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"FftbUsqkN2e85O7sDDFfneKyKAzPJCdy\\\",\\\"ref\\\":\\\"ttyy\\\",\\\"order\\\":\\\"FRD3H\\\",\\\"title\\\":\\\"Pepea Studio\\\",\\\"agreement\\\":\\\"\\\",\\\"price\\\":50000}}]', '2019-09-24 18:50:24', '', 1, '2019-09-24 18:50:37'),
(12, '9Bn0Adl1L3m3rIYGOAi3px1atQMbRM3v', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'QUOTATION ACCEPTED: FRD3H REF: ttyy Pepea Studio 50,000.00/=', '[]', '2019-09-24 18:50:50', '', 1, '2019-09-24 18:51:05'),
(13, 'mBshebDsNiNO2GwBD4m8bRPNh1M1RUzz', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'FUCK OFF I WANT MY MONEY BACK', '[]', '2019-09-24 18:51:47', '', 1, '2019-09-24 18:52:49'),
(14, 'sy9BISsTPJhlhSCC15FFn9pZ2cz7ZQH5', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Hahahaha', '[]', '2019-09-24 18:52:58', '', 1, '2019-09-24 18:52:58'),
(15, 'ZdQxMeM5qVqI19uzhK4gnvMGim407OJi', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '', '[{\\\"type\\\":\\\"image\\\",\\\"name\\\":\\\"Screenshot 2019-09-23 at 17.41.36.png\\\",\\\"data\\\":\\\"\\\\/messages\\\\/attachment\\\\/f7MFeNRa8F9thLhy9xQLDM1F0cQcXeIy.png\\\"}]', '2019-09-24 18:54:58', '', 1, '2019-09-24 19:12:13'),
(16, 'VfDoXs4XbUeMTnwVVV2d3X3ugsowlyBj', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '', '[{\\\"type\\\":\\\"quote\\\",\\\"name\\\":\\\"Quote: ON7MW - Budget Bride Car Hire 75,000.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"Zr8EufgzxAdODD974bw4jbsIwdk6A3xu\\\",\\\"ref\\\":\\\"\\\",\\\"order\\\":\\\"ON7MW\\\",\\\"title\\\":\\\"Budget Bride Car Hire\\\",\\\"agreement\\\":\\\"\\\",\\\"price\\\":75000}}]', '2019-09-24 19:25:15', '', 1, '2019-09-24 19:25:28'),
(17, 'a1VLDRbugCBKvwMrRJfY3EP8gyQIwkyY', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'THIS IS ABOVE MY BUDGET!! TIMES are hard. I can only afford 40K mwisho. Take or leave', '[]', '2019-09-24 19:26:04', '', 1, '2019-09-24 19:26:06'),
(18, 'Ub8P49YUYNShVq7YUkKIXkqOKEacUc37', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'We have a deal but utaweka mafuta. 45k', '[]', '2019-09-24 19:26:34', '', 1, '2019-09-24 19:26:35'),
(19, 'MWrUFLT4tyatqMykhnQkvqBCmqU4kHHO', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Sawa. Tuma Quote 45K no VAT!', '[]', '2019-09-24 19:26:53', '', 1, '2019-09-24 19:26:55'),
(20, 'VQ7AYHBIZg6LSoIXPEsF50CNNb6nFUia', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Deal', '[]', '2019-09-24 19:28:24', '', 1, '2019-09-24 19:28:24'),
(21, 'r4qYCjETxRPRr5AjLhqiVao3Kpzn9uSg', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '', '[{\\\"type\\\":\\\"quote\\\",\\\"name\\\":\\\"Quote: ON7MW - Budget Bride Car Hire 45,000.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"fejNP4eo3U2Z5eIQ9txNYgeFaJz2owV9\\\",\\\"ref\\\":\\\"\\\",\\\"order\\\":\\\"ON7MW\\\",\\\"title\\\":\\\"Budget Bride Car Hire\\\",\\\"agreement\\\":\\\"VAT included\\\",\\\"price\\\":45000}}]', '2019-09-24 19:28:55', '', 1, '2019-09-24 19:28:58'),
(22, '1XCoDrtx0a52xCihl0mMw2JCCvrvqxIB', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'QUOTATION ACCEPTED: ON7MW Budget Bride Car Hire 45,000.00/=', '[]', '2019-09-24 19:29:12', '', 1, '2019-09-24 19:29:16'),
(23, 'n4ghIpbvtaV3U9VFFXy0Set2eBUygSnb', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'I have not received a payment notification', '[]', '2019-09-24 19:31:02', '', 1, '2019-09-24 19:32:18'),
(24, 'RtOBNyOvk6gvlU0iJonuCrxWmNb6qQlB', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '', '[{\\\"type\\\":\\\"quote\\\",\\\"name\\\":\\\"Quote: OAYWI - DJ Service 50,000.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"UiliTJEMoHmItWEooU1aQnqEiXmZaUQl\\\",\\\"ref\\\":\\\"Wamnyoyez\\\",\\\"order\\\":\\\"OAYWI\\\",\\\"title\\\":\\\"DJ Service\\\",\\\"agreement\\\":\\\"100% Payment\\\",\\\"price\\\":50000}}]', '2019-09-24 19:32:29', '', 1, '2019-09-24 19:32:44'),
(25, 'CV7YdxakIW9sZemdIVyXcSmKUu1EandO', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'NO DEALmwisho 30K. Take or leave', '[]', '2019-09-24 19:33:02', '', 1, '2019-09-24 19:35:39'),
(26, 'uoVcDYIEmDbgM7NYEw8NH0rz0flLAsvh', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', '50% now and 50% after event', '[]', '2019-09-24 19:33:16', '', 1, '2019-09-24 19:35:39'),
(27, 'gOIlymMMuGGIr2507YBxSGRi1z7xzRLw', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'We guarantee high quality music with JBL sound systems, Decibel noise measurement.', '[]', '2019-09-24 19:36:59', '', 1, '2019-09-24 19:38:56'),
(28, 'xEVOor1ik7wpPZCF8SoJj4ZVzENRNCJu', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Yo. At honeymoon....', '[]', '2019-09-24 19:55:57', '', 1, '2019-09-24 19:56:05'),
(29, 'twL2Sgjgac2sjUwtQrlUZypXpCeZAebM', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '', '[{\\\"type\\\":\\\"quote\\\",\\\"name\\\":\\\"Quote: 3NTG9 - Honeymoon Safaris 45,000.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"HT8cHuljxw9JM3NCOtveZ2IzpTlGNXgQ\\\",\\\"ref\\\":\\\"\\\",\\\"order\\\":\\\"3NTG9\\\",\\\"title\\\":\\\"Honeymoon Safaris\\\",\\\"agreement\\\":\\\"\\\",\\\"price\\\":45000}}]', '2019-09-24 19:57:04', '', 1, '2019-09-24 19:57:05'),
(30, 'siP22SYnzvSIMuW6KZioyzgAJl50hDqM', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'No', '[]', '2019-09-24 19:57:25', '', 1, '2019-09-24 19:57:30'),
(31, 'j7QwMs3UktMssVn49gmaGiQDpUxNJSff', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'QUOTATION ACCEPTED: 3NTG9 Honeymoon Safaris 45,000.00/=', '[]', '2019-09-24 19:58:00', '', 1, '2019-09-24 19:58:05'),
(32, 'MfJOVPmNYtc5nY1LWlMJBhxsAFf4et6q', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'QUOTATION ACCEPTED: OAYWI REF: Wamnyoyez DJ Service 50,000.00/=', '[]', '2019-09-24 19:59:22', '', 1, '2019-09-24 20:02:37'),
(33, 'GL9U6Lx7QVtk7KzhnpYE0TyJAdJX1M3p', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'Yf4ccb5vyWplQy2UqpnCNXTeNcxSLaT2', 'Send me a quote please.', '[]', '2019-10-01 09:23:26', '', 0, '2019-10-01 09:23:26'),
(34, 'j5ZInLQSIkxFSygklcwsUlnPRmBxvwCl', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Hi', '[]', '2019-10-26 15:05:43', '', 1, '2019-10-26 15:05:47'),
(35, '31DVAVuvOVJrvc8pi5othe6g1yzv4vQm', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Hi', '[]', '2019-10-26 15:05:50', '', 1, '2019-10-26 15:05:54'),
(36, 'f8jZ4nYyRp5CMWk28550XmcKCBKKQjhW', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '', '[{\\\"type\\\":\\\"quote\\\",\\\"name\\\":\\\"Quote: 8GIV0 - Honeymoon Safaris 100,000.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"Fcgk5gx4m9SEPeBxgdbT88Nt1kGRclFP\\\",\\\"ref\\\":\\\"\\\",\\\"order\\\":\\\"8GIV0\\\",\\\"title\\\":\\\"Honeymoon Safaris\\\",\\\"agreement\\\":\\\"\\\",\\\"price\\\":100000}}]', '2019-10-26 15:06:43', '', 1, '2019-10-26 15:06:44'),
(37, 'aNajYmbEZkgNODGv6MdOiwfXc9O8uwqk', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'QUOTATION ACCEPTED: 8GIV0 Honeymoon Safaris 100,000.00/=', '[]', '2019-10-26 15:06:48', '', 1, '2019-10-26 15:06:48'),
(38, 'W0yb9CYf3cOstLCb8B8K5zrUf9pKKUqI', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '35DrzmfG2VxxM7NC06gH6HZymJgyRhsD', '', '[{\\\"type\\\":\\\"quote\\\",\\\"name\\\":\\\"Quote: W40JY - Nairobi cakes 10,000.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"KX24eCUZ9Ip6wZpkWgNBKW9AUpOKQ7Ip\\\",\\\"ref\\\":\\\"\\\",\\\"order\\\":\\\"W40JY\\\",\\\"title\\\":\\\"Nairobi cakes\\\",\\\"agreement\\\":\\\"\\\",\\\"price\\\":10000}}]', '2019-10-26 15:10:40', '', 0, '2019-10-26 15:10:40'),
(39, 't8IX1CUxE0uN0BuWhsoSsmwYkKgMwU35', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'It wasnt', '[]', '2019-10-26 15:14:39', '', 1, '2019-10-26 15:32:51'),
(40, 'J4DuplWWQud0RWb76WpfSyvdc8qrFwzn', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '', '[{\\\"type\\\":\\\"order\\\",\\\"name\\\":\\\"Order: Nairobi cakes 0.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"isCvTebI5JivGptf8HNqpadsJx6AueTW\\\",\\\"ref\\\":\\\"\\\",\\\"service\\\":\\\"fg418A6EBPLjntqfUGtoZCa03xy0gulC\\\",\\\"title\\\":\\\"Nairobi cakes\\\",\\\"category\\\":\\\"Cakes\\\",\\\"agreement\\\":\\\"\\\",\\\"image\\\":\\\"https:\\\\/\\\\/tupange.naicode.com\\\\/uploads\\\\/4wLtqwr7FL.jpeg\\\",\\\"price\\\":0}}]', '2019-10-26 15:17:08', '', 1, '2019-10-26 15:32:51'),
(41, 'th58JbDy1iSJ8A4m94TN9IWv0pRg0CKp', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'ORDER ACCEPTED: VXXE9 Nairobi cakes 0.00/=', '[]', '2019-10-26 15:33:17', '', 1, '2019-10-26 15:47:05'),
(42, 'bL5dffBvqYc6ZycXCGwlSW0EearJAvhB', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', '', '[{\\\"type\\\":\\\"image\\\",\\\"name\\\":\\\"image-asset.png\\\",\\\"data\\\":\\\"\\\\/messages\\\\/attachment\\\\/jt4ljj0u9hWadJIUN2Yp8D4u16ne6YAi.png\\\"}]', '2019-12-03 08:37:02', '', 0, '2019-12-03 08:37:02'),
(43, 'R0u2GcfcnRsxv2rieSTgzNYu2p8oeMcn', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', '', '[{\\\"type\\\":\\\"quote\\\",\\\"name\\\":\\\"Quote: HUVBI - Nairobi cakes 12,000.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"OaVHtlFDGvqApA3u3dQZeklVdBDHC4Nu\\\",\\\"ref\\\":\\\"\\\",\\\"order\\\":\\\"HUVBI\\\",\\\"title\\\":\\\"Nairobi cakes\\\",\\\"agreement\\\":\\\"book now and get a discount\\\",\\\"price\\\":12000}}]', '2020-01-21 18:27:40', '', 1, '2020-01-21 18:28:46'),
(44, '4vDgXklkJn05RLS414ljofOnpAlrJDBG', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'QUOTATION ACCEPTED: HUVBI Nairobi cakes 12,000.00/=', '[]', '2020-01-21 18:28:51', '', 1, '2020-01-21 18:28:55'),
(45, 'A1uQX1qWLmMFFmKNLqYuORxzCWw1dcKk', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'HiLINDA', '[]', '2020-03-10 18:37:45', '', 1, '2020-03-12 22:36:11'),
(46, 'E76kNqOWUDazobDDBkfqt4Z847hMp11v', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Hey', '[]', '2020-03-10 18:38:21', '', 1, '2020-03-12 22:36:11'),
(47, 'sHwqJxh5glsXJKGG7dSnBDn0TXUrdkc4', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Thanks for your custom order.', '[]', '2020-03-13 01:36:27', '', 1, '2020-05-04 09:51:02'),
(48, 'DYR4DVqws18KY40y5IDo2kbonT11lL98', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Much appreciated!', '[]', '2020-05-04 09:51:14', '', 1, '2021-06-10 14:41:30'),
(49, 'nMReRMGbDXQ6GS1tPZfVQDaj2lV4EWyn', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Bosswhen is my cake arriving', '[]', '2020-05-04 09:51:29', '', 1, '2020-05-04 10:00:30'),
(50, 'P8yc7wlzWrXE2IYtzDLLHBqFO5LRl7vc', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'Due to Covid.... Your order will be late', '[]', '2020-05-04 10:00:04', '', 1, '2021-06-10 14:41:22'),
(51, 'Irlhzk8NxpNI2U1pBvZjaMUG0sqVvnfZ', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Your order is under quarantine', '[]', '2020-05-04 10:00:55', '', 1, '2020-05-04 10:00:57'),
(52, 'gP2dje8usbd3rdOYf9Z1R5ST46Pa8wBz', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'Hi. Do you still want to eat some cake?', '[]', '2020-06-25 18:47:26', '', 1, '2020-12-02 10:35:37'),
(53, 'MdZVOym41jmD3t5QKHm30LDziSEc2BMU', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '', '[{\\\"type\\\":\\\"quote\\\",\\\"name\\\":\\\"Quote: PQRLS - Pretty Flowers 6,000.00\\\\/=\\\",\\\"data\\\":{\\\"id\\\":\\\"IGlSUI7xaCEOscphiQ6njI0SisPJchE1\\\",\\\"ref\\\":\\\"\\\",\\\"order\\\":\\\"PQRLS\\\",\\\"title\\\":\\\"Pretty Flowers\\\",\\\"agreement\\\":\\\"\\\",\\\"price\\\":6000}}]', '2021-06-10 14:45:53', '', 1, '2021-06-10 14:46:34'),
(54, '0oOHcQNNPYwxUN7yYgLkKbZYNnjxYgUO', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'QUOTATION ACCEPTED: PQRLS Pretty Flowers 6,000.00/=', '[]', '2021-06-10 14:46:45', '', 1, '2021-06-10 14:46:50');

-- --------------------------------------------------------

--
-- Table structure for table `tb_notifications`
--

CREATE TABLE `tb_notifications` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `type` tinyint(2) NOT NULL,
  `text` text NOT NULL,
  `icon` varchar(200) NOT NULL DEFAULT '',
  `action` text NOT NULL,
  `ref` varchar(200) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_notifications`
--

INSERT INTO `tb_notifications` (`_index`, `id`, `uid`, `type`, `text`, `icon`, `action`, `ref`, `status`, `timestamp`) VALUES
(2, '7HfamHN6N96OAcbXhAcaTxZyHOWOpvqX', 'vubnh9cdsYOhxSFIZe4fXZ02QoiGoygO', 0, 'You have been invited to collaborate on the event \\\"Diana\\\\\\\'s graduation party\\\"', 'users', 'https://demo.tupange.com/euser/N4oqpbOqwI/slX2dfbT5lpTDUjpbG3G0fIHE0eUgQsR', '', 0, '2019-12-03 10:10:52'),
(8, 'U47db2zwi7U19X6ilnI18M1Oi1hFwuZ3', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 0, 'Your task \\\"Transport\\\" for the event \\\"Test 1\\\" is due today - 26/12/2019', 'calendar', 'https://demo.tupange.com/dashboard?event-checklist=V7XCbQzYVv&task-assigned=jmCUlkvNrL', '83273804e097799187860d11ebbd7fe2', 0, '2019-12-26 00:00:04'),
(12, 'GPJj5mBBJxtaDEN3GocLRDaJn9OICnsx', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 0, 'A client has requested a quotation for service \\\"DJ Service\\\". Kindly contact the client to send them a custom quotation.', 'shopping-basket', 'https://demo.tupange.com/orders/?order=VRDP8', 'f0a6d16e595186f999af50de6dc0965a', 0, '2020-01-13 18:35:12'),
(13, 'lq7RJdIpaS487nKLDNHKjXcol1nhYdI4', 'HcVOVsFTBspEuZTI5Tb5W1sFU5VFcRMj', 0, 'You have been invited to collaborate on the event \\\"Diana\\\\\\\'s graduation party\\\"', 'users', 'https://demo.tupange.com/euser/N4oqpbOqwI/C4WYtOVVJ9fEPQAqagNsWgD2OhQ5PupK', '', 0, '2020-01-13 19:10:09'),
(14, 'h7uSpwaZxHtxAhR0ic1CnlQVACEedmDl', 'fAEryS4d8g6uIgdYpF6LSPTWeSIwossh', 0, 'You have been invited to collaborate on the event \\\"Diana\\\\\\\'s graduation party\\\"', 'users', 'https://demo.tupange.com/euser/N4oqpbOqwI/9limlfve1Jta2W8n0sX9Au8dlAkxQ3tz', '', 0, '2020-01-13 19:11:37'),
(21, '3nZvfMY1OJEZlLtaQRIth2xdRIkBfVDQ', '40RNorzfIWiKKdTeqH5HXzcqTWxdZAIL', 0, 'A client has requested a quotation for service \\\"Elle\\\'s Transport\\\". Kindly contact the client to send them a custom quotation.', 'shopping-basket', 'https://demo.tupange.com/orders/?order=OLUPZ', 'dca93bbbc4c7baf912b3dd94ee48a061', 0, '2020-03-10 18:37:16'),
(22, 'OAWdZYuuXOQ7PMLobG08V8KHmOrugK0J', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 0, 'A client has requested a quotation for service \\\"DJ Service\\\". Kindly contact the client to send them a custom quotation.', 'shopping-basket', 'https://demo.tupange.com/orders/?order=YNVU8', 'c5985549787b122018aebab92beb11ab', 0, '2020-03-10 18:37:16'),
(24, 'TxlpwFBbpJnTJxbNi9gWe80bsUEAiYX7', '40RNorzfIWiKKdTeqH5HXzcqTWxdZAIL', 0, 'A client has requested a quotation for service \\\"Elle\\\'s Transport\\\". Kindly contact the client to send them a custom quotation.', 'shopping-basket', 'https://demo.tupange.com/orders/?order=4V6IO', '4fbcdfb6eaba1839bee533e939972fe8', 0, '2020-07-26 18:11:44'),
(25, 'VjU6GLElzinY6TW6q3wPunc50s08OF09', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 0, 'A client has requested a quotation for service \\\"Zuri Flowers\\\". Kindly contact the client to send them a custom quotation.', 'shopping-basket', 'https://demo.tupange.com/orders/?order=3IKWL', 'b9d3f3716efa5eb08ff29ba76f5f78a4', 0, '2020-12-02 07:43:14'),
(27, 'vBA68bfPSsXW9ZusS7uCx6OvVCjYWcgx', '40RNorzfIWiKKdTeqH5HXzcqTWxdZAIL', 0, 'A client has requested a quotation for service \\\"Elle\\\'s Transport\\\". Kindly contact the client to send them a custom quotation.', 'shopping-basket', 'https://demo.tupange.com/orders/?order=FOCDP', 'b97d49819690e482b63fc76a13d1e84c', 0, '2021-06-10 14:37:57'),
(28, 'HXAQFxMmhVz6CEJDLisN7wtftuyeu0wp', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 0, 'A client has requested a quotation for service \\\"DJ Service\\\". Kindly contact the client to send them a custom quotation.', 'shopping-basket', 'https://demo.tupange.com/orders/?order=5426G', '05da138c2d7a1d4b55a3b83d99c797c5', 0, '2021-06-10 14:37:57'),
(29, 'dR5eh0VfZlETurytr4TdyenIf4oX26cA', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 0, 'A client has requested a quotation for service \\\"Trial Service\\\". Kindly contact the client to send them a custom quotation.', 'shopping-basket', 'https://demo.tupange.com/orders/?order=0UWHT', '36f6e19ff88d08b4225e15d4b4860f16', 0, '2021-06-10 14:37:57'),
(30, 'O6yeKXZsz0J62ZqYn6BD0uz1BC7qlDd6', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 0, 'QUOTATION ACCEPTED: PQRLS Pretty Flowers 6,000.00/=', 'cart-plus', 'https://demo.tupange.com/orders/?order=PQRLS', '67a5fcae0084ec894f7ea2d2bc2ac334', 0, '2021-06-10 14:46:45'),
(45, 'qfAiqGZdTk1ysDiOUBfIniJtySP0ztQL', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 0, 'Your task \\\"Order DJ\\\" for the event \\\"Birthday Party\\\" is due today - 17/06/2021', 'calendar', 'https://demo.tupange.com/dashboard?event-checklist=4DW8YTpC69&task-assigned=Nu2NXgZKEa', '8047eac8a7e730150d7fe902bc3acd89', 0, '2021-06-17 00:00:14'),
(46, '3jy3m9f6aUgFYX21Td6AwsdbnKzMU2jy', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 0, 'Your task \\\"Buy flowers\\\" for the event \\\"Birthday Party\\\" is due today - 17/06/2021', 'calendar', 'https://demo.tupange.com/dashboard?event-checklist=4DW8YTpC69&task-assigned=z962SZifIH', 'da51b4c7e9b61960db60428bcb56a5f2', 0, '2021-06-17 00:00:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_orders`
--

CREATE TABLE `tb_orders` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `type` tinyint(2) NOT NULL DEFAULT 0,
  `uid` varchar(200) NOT NULL,
  `ref` varchar(200) NOT NULL,
  `ref_id` varchar(200) NOT NULL DEFAULT '',
  `event_id` varchar(200) NOT NULL,
  `time_added` datetime NOT NULL,
  `time_ordered` datetime NOT NULL DEFAULT current_timestamp(),
  `pricing_id` varchar(200) NOT NULL,
  `pricing_price` double(20,2) NOT NULL,
  `pricing_title` varchar(200) NOT NULL,
  `pricing_description` text NOT NULL,
  `pricing_package` text NOT NULL,
  `pricing_timestamp` varchar(200) NOT NULL DEFAULT '',
  `service_id` varchar(200) NOT NULL,
  `service_title` varchar(200) NOT NULL,
  `service_category` varchar(200) NOT NULL,
  `service_timestamp` datetime NOT NULL,
  `vendor_id` varchar(200) NOT NULL,
  `vendor_company` varchar(200) NOT NULL,
  `vendor_timestamp` datetime NOT NULL,
  `message` text NOT NULL,
  `notes` text NOT NULL,
  `status` tinyint(2) NOT NULL,
  `status_reason` varchar(200) NOT NULL DEFAULT '',
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `overdue` tinyint(1) NOT NULL DEFAULT 0,
  `overdue_timestamp` varchar(200) NOT NULL DEFAULT '',
  `seen_user` varchar(200) NOT NULL DEFAULT '',
  `seen_vendor` varchar(200) NOT NULL DEFAULT '',
  `quote_id` varchar(200) NOT NULL DEFAULT '',
  `quote_ref` varchar(200) NOT NULL DEFAULT '',
  `payment_id` varchar(200) NOT NULL DEFAULT '',
  `payment_ref` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_orders`
--

INSERT INTO `tb_orders` (`_index`, `id`, `type`, `uid`, `ref`, `ref_id`, `event_id`, `time_added`, `time_ordered`, `pricing_id`, `pricing_price`, `pricing_title`, `pricing_description`, `pricing_package`, `pricing_timestamp`, `service_id`, `service_title`, `service_category`, `service_timestamp`, `vendor_id`, `vendor_company`, `vendor_timestamp`, `message`, `notes`, `status`, `status_reason`, `timestamp`, `overdue`, `overdue_timestamp`, `seen_user`, `seen_vendor`, `quote_id`, `quote_ref`, `payment_id`, `payment_ref`) VALUES
(4, 'A1GKP', 0, '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'JER3B', '', '5ZGufM3xNS', '2019-08-16 09:46:07', '2019-08-20 19:01:20', '', 0.00, '', '', '', '0000-00-00 00:00:00', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-20 19:01:20', 0, '', '2019-08-20 19:01:29', '', '', '', '', ''),
(5, 'FN806', 0, '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'JER3B', '', '5ZGufM3xNS', '2019-08-16 09:46:09', '2019-08-20 19:01:20', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'Y0864JpMoHg0tTA2V7BrsYO9wsTZLChG', 'Test Service', 'Photography', '2019-06-19 01:57:33', 'wzKQgdphjIziQ8Urykz0l7VycNEF7dkm', 'Test', '2019-06-08 20:34:50', '', '', 0, '', '2019-08-20 19:01:20', 0, '', '2019-08-20 19:01:29', '', '', '', '', ''),
(6, '2HJ1E', 0, '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'JER3B', '', '5ZGufM3xNS', '2019-08-16 09:48:04', '2019-08-20 19:01:20', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'ZPYWTyfEQZFsoCa2ItX8CYrsoxBLRiei', 'Tupange Caterers', 'Catering', '2019-07-02 18:30:50', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-20 19:01:20', 0, '', '2019-08-20 19:01:29', '', '', '', '', ''),
(7, 'FY06R', 0, 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'SIROJ', '', 'hcudO2aNQz', '2019-08-20 19:33:40', '2019-08-20 19:36:57', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-20 19:36:57', 0, '', '2019-08-20 19:37:26', '2019-09-24 18:43:48', '', '', '', ''),
(8, 'XU4OP', 0, 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'SIROJ', '', 'hcudO2aNQz', '2019-08-20 19:33:47', '2019-08-20 19:36:57', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'uIz9iyGWPh36TUTknfAenndjcEw2BW1E', 'Budget Bride Car Hire', 'Transportation', '2019-08-19 20:51:41', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'covid', '2020-12-02 07:45:30', 0, '', '2019-08-20 19:37:26', '', '', '', '', ''),
(9, 'QRFU6', 0, 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'SIROJ', '', 'hcudO2aNQz', '2019-08-20 19:33:52', '2019-08-20 19:36:57', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'ZPYWTyfEQZFsoCa2ItX8CYrsoxBLRiei', 'Tupange Caterers', 'Catering', '2019-07-02 18:30:50', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-20 19:36:57', 0, '', '2019-08-20 19:37:26', '2019-09-24 18:43:48', '', '', '', ''),
(10, '5RSZC', 0, 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'SIROJ', '', 'hcudO2aNQz', '2019-08-20 19:33:59', '2019-08-20 19:36:57', '', 0.00, '', '', '', '0000-00-00 00:00:00', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-20 19:36:57', 0, '', '2019-08-20 19:37:26', '2019-09-24 18:43:48', '', '', '', ''),
(11, 'B7SGX', 0, 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'SIROJ', '', 'hcudO2aNQz', '2019-08-20 19:34:04', '2019-08-20 19:36:57', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', 'Honeymoon Safaris', 'Travel Agents', '2019-06-23 13:19:16', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', 1, '', '2019-11-09 09:09:07', 0, '', '2019-08-20 19:37:26', '', '', '', '', ''),
(12, 'AL752', 0, 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', '85LPK', '', 'NvdgZSjpfu', '2019-08-20 20:02:39', '2019-08-20 20:04:32', '', 0.00, '', '', '', '0000-00-00 00:00:00', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'Makeki Mathwiti bakery', 'Cakes', '2019-08-20 19:26:43', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', 'tst', '', -1, 'covid', '2020-12-02 07:45:38', 0, '', '2019-08-20 20:04:40', '', '', '', '', ''),
(13, '2VXA3', 0, 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', '85LPK', '', 'NvdgZSjpfu', '2019-08-20 20:02:42', '2019-08-20 20:04:32', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', 'test', '', 0, '', '2019-08-20 20:04:32', 0, '', '2019-08-20 20:04:40', '', '', '', '', ''),
(14, 'KB79H', 0, 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', '6MDUI', '', 'NvdgZSjpfu', '2019-08-25 12:40:56', '2019-08-25 12:41:25', '', 0.00, '', '', '', '0000-00-00 00:00:00', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'Makeki Mathwiti bakery', 'Cakes', '2019-08-20 19:26:43', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'covid', '2020-12-02 07:45:14', 0, '', '2019-08-25 12:41:32', '', '', '', '', ''),
(15, 'GXJEW', 0, 'UWhAcdzivIqMyUrCNokqJrIxS8ODIdpH', '6MDUI', '', 'NvdgZSjpfu', '2019-08-25 12:40:59', '2019-08-25 12:41:25', '', 0.00, '', '', '', '0000-00-00 00:00:00', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-25 12:41:25', 0, '', '2019-08-25 12:41:32', '', '', '', '', ''),
(16, 'MUAWP', 0, 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'Z641P', '', '', '2019-08-27 19:13:22', '2019-08-27 19:14:09', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-27 19:14:09', 0, '', '2019-08-27 19:18:17', '', '', '', '', ''),
(17, 'AVSF1', 0, 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'ECGMC', '', 'GdRk51MIWl', '2019-08-27 19:19:17', '2019-08-27 19:19:37', '', 0.00, '', '', '', '0000-00-00 00:00:00', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-27 19:19:37', 0, '', '2019-08-27 19:19:43', '2019-09-24 18:43:07', '', '', '', ''),
(18, 'DKRWZ', 0, 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', '64PUO', '', '', '2019-08-28 22:16:18', '2019-08-28 22:17:08', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-28 22:17:08', 0, '', '2019-08-28 22:17:13', '2019-09-24 18:43:07', '', '', '', ''),
(19, 'G9AQ2', 0, 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', '64PUO', '', '', '2019-08-28 22:16:21', '2019-08-28 22:17:08', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'uIz9iyGWPh36TUTknfAenndjcEw2BW1E', 'Budget Bride Car Hire', 'Transportation', '2019-08-19 20:51:41', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', 1, '', '2019-10-26 15:12:54', 0, '', '2019-08-28 22:17:13', '', '', '', '', ''),
(20, 'WY0YV', 0, 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', '64PUO', '', '', '2019-08-28 22:16:23', '2019-08-28 22:17:08', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'ZPYWTyfEQZFsoCa2ItX8CYrsoxBLRiei', 'Tupange Caterers', 'Catering', '2019-07-02 18:30:50', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-28 22:17:08', 0, '', '2019-08-28 22:17:13', '2019-09-24 18:43:07', '', '', '', ''),
(21, '95F3B', 0, 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', '64PUO', '', '', '2019-08-28 22:16:25', '2019-08-28 22:17:08', '', 0.00, '', '', '', '0000-00-00 00:00:00', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-28 22:17:08', 0, '', '2019-08-28 22:17:13', '2019-09-24 18:43:07', '', '', '', ''),
(22, 'OAYWI', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'RHTGN', '', '', '2019-08-31 19:09:47', '2019-08-31 19:10:10', 'QUOTATION', 50000.00, 'Quotation', '100% Payment', '', '2019-09-24 19:59:22', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-08-31 19:10:10', 0, '', '2019-08-31 19:10:17', '2019-09-24 18:43:07', 'UiliTJEMoHmItWEooU1aQnqEiXmZaUQl', 'Wamnyoyez', '', ''),
(23, 'KR1BA', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'RHTGN', '', '', '2019-08-31 19:09:53', '2019-08-31 19:10:10', '', 0.00, '', '', '', '0000-00-00 00:00:00', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'Makeki Mathwiti bakery', 'Cakes', '2019-08-20 19:26:43', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'covid', '2020-12-02 07:45:20', 0, '', '2019-08-31 19:10:17', '2019-09-24 18:20:40', '', '', '', ''),
(24, 'EG46X', 0, 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'BR3ZG', '', '', '2019-09-08 19:46:32', '2019-09-08 19:46:55', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', -1, 'kkk', '2019-09-24 20:04:34', 0, '', '2019-09-08 19:47:01', '2019-09-24 18:43:07', '', '', '', ''),
(25, 'HI7Q5', 0, 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'RRZ0L', '', 'FLOekhHX0v', '2019-08-16 16:30:44', '2019-09-11 01:54:24', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'Y0864JpMoHg0tTA2V7BrsYO9wsTZLChG', 'Test Service', 'Photography', '2019-06-19 01:57:33', 'wzKQgdphjIziQ8Urykz0l7VycNEF7dkm', 'Test', '2019-06-08 20:34:50', '', '', 0, '', '2019-09-11 01:54:24', 0, '', '2019-09-11 01:54:39', '', '', '', '', ''),
(26, 'T4NZS', 0, 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'A2LJ1', '', '', '2019-09-12 09:02:52', '2019-09-12 09:03:51', '', 0.00, '', '', '', '0000-00-00 00:00:00', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', 'please send me a quote for your pricing.', '', -1, 'jj', '2019-09-24 20:04:03', 0, '', '2019-09-12 09:04:02', '2019-09-24 18:43:07', '', '', '', ''),
(27, '7YQH0', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'IMFC2', '', '02yMkEIbxS', '2019-09-17 19:34:03', '2019-09-17 19:34:54', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'ZPYWTyfEQZFsoCa2ItX8CYrsoxBLRiei', 'Tupange Caterers', 'Catering', '2019-07-02 18:30:50', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', -1, 'jj', '2019-09-24 20:04:14', 0, '', '2019-09-17 19:34:59', '2019-09-24 18:43:07', '', '', '', ''),
(28, 'SKYP8', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'IMFC2', '', '02yMkEIbxS', '2019-09-17 19:34:28', '2019-09-17 19:34:54', '', 0.00, '', '', '', '0000-00-00 00:00:00', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'Makeki Mathwiti bakery', 'Cakes', '2019-08-20 19:26:43', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', 1, '', '2019-09-24 18:19:37', 0, '', '2019-09-17 19:34:59', '2019-09-24 18:19:09', '', '', '', ''),
(29, 'E33RG', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'IMFC2', '', '02yMkEIbxS', '2019-09-17 19:34:31', '2019-09-17 19:34:54', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'Q2MyzMua9FzCqfA0PDzT0jO4jPJF7pw8', 'Yummy Food', 'Catering', '2019-06-19 07:45:27', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', 1, '', '2019-09-24 18:19:44', 0, '', '2019-09-17 19:34:59', '2019-09-24 18:19:09', '', '', '', ''),
(30, '4CGOG', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'IMFC2', '', '02yMkEIbxS', '2019-09-17 19:34:33', '2019-09-17 19:34:54', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'we are fully booked', '2019-09-24 18:20:00', 0, '', '2019-09-17 19:34:59', '2019-09-24 18:19:09', '', '', '', ''),
(31, 'ML6M4', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'SM06X', '', '', '2019-09-17 19:40:17', '2019-09-17 19:41:15', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', 1, '', '2019-10-05 09:36:17', 0, '', '2019-09-17 19:41:19', '2019-09-24 18:19:09', '', '', '', ''),
(32, 'TBB6A', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'SM06X', '', '', '2019-09-17 19:40:18', '2019-09-17 19:41:15', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'Q2MyzMua9FzCqfA0PDzT0jO4jPJF7pw8', 'Yummy Food', 'Catering', '2019-06-19 07:45:27', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'covid', '2020-12-02 07:44:53', 0, '', '2019-09-17 19:41:19', '2019-09-24 18:19:09', '', '', '', ''),
(33, '5ZJQ6', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'SM06X', '', '', '2019-09-17 19:40:22', '2019-09-17 19:41:15', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', 'Honeymoon Safaris', 'Travel Agents', '2019-06-23 13:19:16', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'covid', '2020-12-02 07:44:49', 0, '', '2019-09-17 19:41:19', '2019-09-24 18:19:09', '', '', '', ''),
(34, 'G1443', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'SM06X', '', '', '2019-09-17 19:40:22', '2019-09-17 19:41:15', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'uIz9iyGWPh36TUTknfAenndjcEw2BW1E', 'Budget Bride Car Hire', 'Transportation', '2019-08-19 20:51:41', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'covid', '2020-12-02 07:44:46', 0, '', '2019-09-17 19:41:19', '2019-09-24 18:19:09', '', '', '', ''),
(35, 'K5QYJ', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'SM06X', '', '', '2019-09-17 19:40:26', '2019-09-17 19:41:15', '', 0.00, '', '', '', '0000-00-00 00:00:00', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'Makeki Mathwiti bakery', 'Cakes', '2019-08-20 19:26:43', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'covid', '2020-12-02 07:44:42', 0, '', '2019-09-24 18:48:20', '2019-09-24 18:19:09', '', '', '', ''),
(36, 'W40JY', 0, '35DrzmfG2VxxM7NC06gH6HZymJgyRhsD', 'GP6DR', '', '', '2019-09-20 10:59:03', '2019-09-20 11:00:40', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'covid', '2020-12-02 07:44:38', 0, '', '2019-09-20 11:00:52', '2019-09-24 18:19:09', '', '', '', ''),
(37, '3F84W', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'TC43N', '', '02yMkEIbxS', '2019-09-20 23:29:56', '2019-09-20 23:30:19', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'covid', '2020-12-02 07:44:34', 0, '', '2019-09-24 18:48:20', '2019-09-24 18:19:09', '', '', '', ''),
(38, 'S99TO', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'TC43N', '', '02yMkEIbxS', '2019-09-20 23:30:00', '2019-09-20 23:30:19', '', 0.00, '', '', '', '0000-00-00 00:00:00', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', -1, 'tt', '2019-09-24 20:03:45', 0, '', '2019-09-24 18:48:20', '2019-09-24 18:43:07', '', '', '', ''),
(39, 'AN2YA', 0, 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'REF001', '0O5bNTOhy3tcrVauPJirEAdjy3j5V4at', '', '2019-09-21 07:43:43', '2019-09-21 07:43:43', 'CUSTOM', 20000.00, 'Custom Order', 'Testing order', '', '2019-09-21 07:43:43', 'YpKbve8t66PZB6Ms4482pRarHfQgCOED', 'Zuri Flowers', 'Flowers', '2019-06-27 14:56:49', 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'Test', '2019-05-29 20:32:34', 'ORDER ACCEPTED: AN2YA REF: REF001 Zuri Flowers 20,000.00/=', '', 0, '', '2019-09-21 07:43:43', 0, '', '2019-09-21 07:47:16', '2019-09-21 07:44:33', '', '', '', ''),
(40, 'BPYOM', 0, 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '4NH79', '', '', '2019-09-24 18:31:22', '2019-09-24 18:31:33', '', 0.00, '', '', '', '', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 1, '', '2019-09-24 20:03:30', 0, '', '2019-09-24 18:46:27', '2019-09-24 18:43:07', '', '', '', ''),
(41, 'FRD3H', 0, 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', '456HX', '', '', '2019-09-24 18:44:57', '2019-09-24 18:48:07', 'QUOTATION', 50000.00, 'Quotation', '', '', '2019-09-24 18:50:50', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 1, '', '2019-09-24 19:33:48', 0, '', '2019-09-24 18:48:15', '2019-09-24 18:48:28', 'FftbUsqkN2e85O7sDDFfneKyKAzPJCdy', 'ttyy', '', ''),
(42, 'Z4UDK', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'FYVQO', '', 'ITHIUHCE9z', '2019-09-24 18:56:04', '2019-09-24 19:18:57', '', 0.00, '', '', '', '', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', 'I WANT THIS ASAP', '', 1, '', '2019-09-24 20:03:28', 0, '', '2019-09-24 19:19:12', '2019-09-24 19:29:53', '', '', '', ''),
(43, 'K6N81', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'FYVQO', '', 'ITHIUHCE9z', '2019-09-24 18:56:22', '2019-09-24 19:18:57', '', 0.00, '', '', '', '', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', 'I WANT THIS ASAP', '', -1, 'covid', '2020-12-02 07:44:30', 0, '', '2019-09-24 19:19:12', '2019-09-24 19:23:27', '', '', '', ''),
(44, 'ZTCRU', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'FYVQO', '', 'ITHIUHCE9z', '2019-09-24 19:15:28', '2019-09-24 19:18:57', '', 0.00, '', '', '', '', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'Makeki Mathwiti bakery', 'Cakes', '2019-08-20 19:26:43', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', 'I WANT THIS ASAP', '', -1, 'covid', '2020-12-02 07:44:23', 0, '', '2019-09-24 19:19:12', '2019-09-24 19:23:27', '', '', '', ''),
(45, '8PN76', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'FYVQO', '', 'ITHIUHCE9z', '2019-09-24 19:16:46', '2019-09-24 19:18:57', '', 0.00, '', '', '', '', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', 'I WANT THIS ASAP', '', 1, '', '2019-09-24 20:03:22', 0, '', '2019-09-24 19:19:12', '2019-09-24 19:29:53', '', '', '', ''),
(46, 'ON7MW', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'UBH4G', '', 'ITHIUHCE9z', '2019-09-24 19:20:56', '2019-09-24 19:21:23', 'QUOTATION', 45000.00, 'Quotation', 'VAT included', '', '2019-09-24 19:29:12', 'uIz9iyGWPh36TUTknfAenndjcEw2BW1E', 'Budget Bride Car Hire', 'Transportation', '2019-09-24 19:20:39', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', 4, '', '2019-10-02 09:10:25', 0, '', '2019-09-24 19:21:52', '2019-09-24 19:23:27', 'fejNP4eo3U2Z5eIQ9txNYgeFaJz2owV9', '', '', ''),
(47, 'YOSVX', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'LBJ7R', '', 'ITHIUHCE9z', '2019-09-24 19:37:21', '2019-09-24 19:37:40', '', 0.00, '', '', '', '', 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', 'Honeymoon Safaris', 'Travel Agents', '2019-06-23 13:19:16', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', 'NEW QUOTE', '', -1, 'wewe unachocha', '2019-09-24 19:41:57', 0, '', '2019-09-24 19:37:47', '2019-09-24 19:39:58', '', '', '', ''),
(48, '3NTG9', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'RGE1J', '', 'ITHIUHCE9z', '2019-09-24 19:44:56', '2019-09-24 19:45:11', 'QUOTATION', 45000.00, 'Quotation', '', '', '2019-09-24 19:58:00', 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', 'Honeymoon Safaris', 'Travel Agents', '2019-06-23 13:19:16', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'covid', '2020-12-02 07:44:17', 0, '', '2019-09-24 19:45:17', '2019-09-24 19:55:26', 'HT8cHuljxw9JM3NCOtveZ2IzpTlGNXgQ', '', '', ''),
(49, '97PRF', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'E56LE', '', '', '2019-09-30 10:23:26', '2019-09-30 10:25:07', '', 0.00, '', '', '', '', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', 'Budget 5000, all day event', '', 0, '', '2019-09-30 10:25:07', 0, '', '2019-09-30 10:25:47', '', '', '', '', ''),
(50, '9Q2Q0', 0, 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', 'YMU5Q', '', '', '2019-10-05 11:43:14', '2019-10-05 12:13:01', '', 0.00, '', '', '', '', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', 'adsf', '', 0, '', '2019-10-05 12:13:01', 0, '', '2019-10-05 12:13:09', '', '', '', '', ''),
(51, 'SNKP4', 0, 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', 'YMU5Q', '', '', '2019-10-05 11:43:18', '2019-10-05 12:13:01', '', 0.00, '', '', '', '', 'Y0864JpMoHg0tTA2V7BrsYO9wsTZLChG', 'Test Service', 'Photography', '2019-06-19 01:57:33', 'wzKQgdphjIziQ8Urykz0l7VycNEF7dkm', 'Test', '2019-06-08 20:34:50', 'asdf', '', 0, '', '2019-10-05 12:13:01', 0, '', '2019-10-05 12:13:09', '', '', '', '', ''),
(52, 'LA2FN', 0, 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', 'YMU5Q', '', '', '2019-10-05 11:43:22', '2019-10-05 12:13:01', '', 0.00, '', '', '', '', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', 'asdf', '', 0, '', '2019-10-05 12:13:01', 0, '', '2019-10-05 12:13:09', '2019-11-09 11:18:28', '', '', '', ''),
(53, 'F02CJ', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'V5A6T', '', '02yMkEIbxS', '2019-10-05 16:17:21', '2019-10-06 19:19:22', '', 0.00, '', '', '', '', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-10-06 19:19:22', 0, '', '2019-10-06 19:19:28', '2019-11-09 11:18:28', '', '', '', ''),
(54, 'K346B', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'V5A6T', '', '02yMkEIbxS', '2019-10-06 19:18:55', '2019-10-06 19:19:22', '', 0.00, '', '', '', '', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'xe', '2020-12-02 07:44:12', 0, '', '2019-10-06 19:19:28', '2019-10-14 17:35:45', '', '', '', ''),
(55, 'X4YSH', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'V5A6T', '', '02yMkEIbxS', '2019-10-06 19:18:55', '2019-10-06 19:19:22', '', 0.00, '', '', '', '', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-10-06 19:19:22', 0, '', '2019-10-06 19:19:28', '2019-11-09 11:18:28', '', '', '', ''),
(56, 'U26ZQ', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'V5A6T', '', '02yMkEIbxS', '2019-10-06 19:18:56', '2019-10-06 19:19:22', '', 0.00, '', '', '', '', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'Makeki Mathwiti bakery', 'Cakes', '2019-08-20 19:26:43', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'cv', '2020-12-02 07:44:06', 0, '', '2019-10-06 19:19:28', '2019-10-14 17:35:45', '', '', '', ''),
(57, 'X6SO4', 0, 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', 'K3HYA', '', 'kCHMIsV33L', '2019-10-08 11:24:51', '2019-10-08 11:28:43', '', 0.00, '', '', '', '', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-10-08 11:28:43', 0, '', '2019-10-08 11:28:51', '2019-11-09 11:18:28', '', '', '', ''),
(58, '5U272', 0, 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', 'K3HYA', '', 'kCHMIsV33L', '2019-10-08 11:28:25', '2019-10-08 11:28:43', '', 0.00, '', '', '', '', 'ZPYWTyfEQZFsoCa2ItX8CYrsoxBLRiei', 'Tupange Caterers', 'Catering', '2019-07-02 18:30:50', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-10-08 11:28:43', 0, '', '2019-10-08 11:28:51', '2019-11-09 11:18:28', '', '', '', ''),
(59, '12QWL', 0, 'Q2jCI7NtaSIk1qPQ61jN7emaTDCNIy78', 'K3HYA', '', 'kCHMIsV33L', '2019-10-08 11:28:26', '2019-10-08 11:28:43', '', 0.00, '', '', '', '', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-10-08 11:28:43', 0, '', '2019-10-08 11:28:51', '2019-11-09 11:18:28', '', '', '', ''),
(60, 'Z7TVF', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'SKRA2', '', 'ITHIUHCE9z', '2019-10-09 15:22:01', '2019-10-09 15:22:19', '', 0.00, '', '', '', '', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'vcd', '2020-12-02 07:44:03', 0, '', '2019-10-09 15:22:25', '2019-10-14 17:35:45', '', '', '', ''),
(61, 'U4NMB', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'SKRA2', '', 'ITHIUHCE9z', '2019-10-09 15:22:02', '2019-10-09 15:22:19', '', 0.00, '', '', '', '', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-10-09 15:22:19', 0, '', '2019-10-09 15:22:25', '2019-11-09 11:18:28', '', '', '', ''),
(62, '28IFZ', 0, 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'UH2YW', '', 'QyAVdptXmJ', '2019-10-14 12:36:48', '2019-10-14 12:41:55', '', 0.00, '', '', '', '', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', 'form ni gani?', '', 0, '', '2019-10-14 12:41:55', 0, '', '2019-10-14 12:42:14', '2019-11-09 11:18:28', '', '', '', ''),
(63, '3OK4U', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'JHEDH', '', '02yMkEIbxS', '2019-10-26 11:58:06', '2019-10-26 15:03:36', '', 0.00, '', '', '', '', 'Y0864JpMoHg0tTA2V7BrsYO9wsTZLChG', 'Test Service', 'Photography', '2019-06-19 01:57:33', 'wzKQgdphjIziQ8Urykz0l7VycNEF7dkm', 'Test', '2019-06-08 20:34:50', '', '', 0, '', '2019-10-26 15:03:36', 0, '', '2019-10-26 15:03:42', '', '', '', '', ''),
(64, 'WHFJP', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'JHEDH', '', '02yMkEIbxS', '2019-10-26 12:22:15', '2019-10-26 15:03:36', '', 0.00, '', '', '', '', 'S1ikxcj0Zn1ulfDS5JJIlxs9YOjaWMjO', 'Trial Service', 'Other Services', '2019-10-26 09:41:15', '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', 'mem en', '2019-05-29 21:20:44', '', '', -1, 'no reason', '2020-03-15 12:40:48', 0, '', '2019-10-26 15:03:42', '2019-11-05 17:26:35', '', '', '', ''),
(65, '6DIIA', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'JHEDH', '', '02yMkEIbxS', '2019-10-26 12:22:21', '2019-10-26 15:03:36', '', 0.00, '', '', '', '', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, '6DIIA(Nairobi cakes)', '2019-10-26 15:24:32', 0, '', '2019-10-26 15:03:42', '2019-10-26 15:04:39', '', '', '', ''),
(66, '1CWX6', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'JHEDH', '', '02yMkEIbxS', '2019-10-26 13:19:17', '2019-10-26 15:03:36', '', 0.00, '', '', '', '', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-08-20 19:27:21', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-10-26 15:03:36', 0, '', '2019-10-26 15:03:42', '2019-11-09 11:18:28', '', '', '', ''),
(68, '74PE2', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'JHEDH', '', '02yMkEIbxS', '2019-10-26 13:44:52', '2019-10-26 15:03:36', '', 0.00, '', '', '', '', '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'Pepea Studio', 'Photography', '2019-07-02 18:27:34', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-10-26 15:03:36', 0, '', '2019-10-26 15:03:42', '2019-11-09 11:18:28', '', '', '', ''),
(69, '8GIV0', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'JHEDH', '', '02yMkEIbxS', '2019-10-26 15:03:19', '2019-10-26 15:03:36', 'QUOTATION', 100000.00, 'Quotation', '', '', '2019-10-26 15:06:48', 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', 'Honeymoon Safaris', 'Travel Agents', '2019-06-23 13:19:16', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', 3, '', '2019-10-26 15:14:05', 1, '2019-10-26 15:13:35', '2019-10-26 15:03:42', '2019-10-26 15:04:39', 'Fcgk5gx4m9SEPeBxgdbT88Nt1kGRclFP', '', 'p4OBNDS5nP1UiQ3pHHM2wxuq1YZgNLjD', 'DEMO_JOT42GC9QZ'),
(70, 'VXXE9', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '', 'isCvTebI5JivGptf8HNqpadsJx6AueTW', '', '2019-10-26 15:33:17', '2019-10-26 15:33:17', 'CUSTOM', 0.00, 'Custom Order', '', '', '2019-10-26 15:33:17', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', 'ORDER ACCEPTED: VXXE9 Nairobi cakes 0.00/=', '', -1, 'sd', '2020-12-02 07:44:01', 0, '', '2019-10-26 15:35:12', '2019-10-26 15:33:30', '', '', '', ''),
(71, '3OD08', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'IRUSB', '', '02yMkEIbxS', '2019-10-26 15:33:37', '2019-10-26 15:43:19', '', 0.00, '', '', '', '', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', 'ieoiroweirowiero', '', -1, '3OD08 (Nairobi cakes)', '2019-11-09 09:08:40', 0, '', '2019-10-26 15:45:47', '2019-10-26 15:44:16', '', '', '', ''),
(72, '1W76Y', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '6R5IG', '', '02yMkEIbxS', '2019-11-09 11:39:02', '2019-11-09 11:40:22', '', 0.00, '', '', '', '', 'S1ikxcj0Zn1ulfDS5JJIlxs9YOjaWMjO', 'Trial Service', 'Other Services', '2019-10-26 09:41:15', '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', 'mem en', '2019-05-29 21:20:44', '', '', -1, 'cant', '2020-01-25 22:59:11', 0, '', '2019-11-09 11:40:29', '2019-11-12 10:36:51', '', '', '', ''),
(73, 'UMI3K', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '6R5IG', '', '02yMkEIbxS', '2019-11-09 11:39:03', '2019-11-09 11:40:22', '', 0.00, '', '', '', '', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'dw', '2020-12-02 07:43:56', 0, '', '2019-11-09 11:40:29', '2020-01-11 16:27:41', '', '', '', ''),
(74, '2UO53', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '6R5IG', '', '02yMkEIbxS', '2019-11-09 11:39:05', '2019-11-09 11:40:22', '', 0.00, '', '', '', '', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'Makeki Mathwiti bakery', 'Cakes', '2019-08-20 19:26:43', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 's', '2020-12-02 07:43:53', 0, '', '2019-11-09 11:40:29', '2020-01-11 16:27:41', '', '', '', ''),
(75, 'GABY9', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '6R5IG', '', '02yMkEIbxS', '2019-11-09 11:39:08', '2019-11-09 11:40:22', '', 0.00, '', '', '', '', 'ZPYWTyfEQZFsoCa2ItX8CYrsoxBLRiei', 'Tupange Caterers', 'Catering', '2019-07-02 18:30:50', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2019-11-09 11:40:22', 0, '', '2019-11-09 11:40:29', '2020-01-13 18:34:14', '', '', '', ''),
(76, '70Y26', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '6R5IG', '', '02yMkEIbxS', '2019-11-09 11:39:17', '2019-11-09 11:40:22', '', 0.00, '', '', '', '', 'YpKbve8t66PZB6Ms4482pRarHfQgCOED', 'Zuri Flowers', 'Flowers', '2019-06-27 14:56:49', 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'Test', '2019-05-29 20:32:34', '', '', 0, '', '2019-11-09 11:40:22', 0, '', '2019-11-09 11:40:29', '', '', '', '', ''),
(77, 'GXKEC', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '6R5IG', '', '02yMkEIbxS', '2019-11-09 11:39:18', '2019-11-09 11:40:22', '', 0.00, '', '', '', '', 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', 'Honeymoon Safaris', 'Travel Agents', '2019-06-23 13:19:16', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'hauna doh', '2020-01-21 18:18:43', 0, '', '2019-11-09 11:40:29', '2020-01-11 16:27:41', '', '', '', ''),
(78, 'R9KRA', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '6R5IG', '', '02yMkEIbxS', '2019-11-09 11:39:20', '2019-11-09 11:40:22', '', 0.00, '', '', '', '', 'Q2MyzMua9FzCqfA0PDzT0jO4jPJF7pw8', 'Yummy Food', 'Catering', '2019-06-19 07:45:27', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'c', '2020-12-02 07:43:50', 0, '', '2019-11-09 11:40:29', '2020-01-11 16:27:41', '', '', '', ''),
(79, '60AT2', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'E0L8N', '', 'N4oqpbOqwI', '2019-11-09 13:33:55', '2019-12-03 08:36:27', 'YqacDiObbNVznBPew1jtTTcjtyouDjsO', 10000.00, 'Epic cake', 'Best cakes in town', '[]', '2019-09-17 12:39:41', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'Makeki Mathwiti bakery', 'Cakes', '2019-08-20 19:26:43', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', '', '', -1, 'sd', '2020-12-02 07:43:47', 0, '', '2019-12-03 08:36:33', '2020-01-11 16:27:41', '', '', '', ''),
(80, 'T3YKU', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'E0L8N', '', 'N4oqpbOqwI', '2019-12-03 08:34:52', '2019-12-03 08:36:27', 't8zVueaTnKDLIxPcWmbP8wDOjjvolOi4', 1000.00, 'Basic', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus sapien, malesuada non ipsum at, auctor dictum urna.', '[\\\"Package Item 1\\\",\\\"Package Item 2\\\",\\\"Package Item 3\\\"]', '2019-08-15 01:26:22', 'YpKbve8t66PZB6Ms4482pRarHfQgCOED', 'Zuri Flowers', 'Flowers', '2019-06-27 14:56:49', 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'Test', '2019-05-29 20:32:34', '', '', 0, '', '2019-12-03 08:36:27', 0, '', '2019-12-03 08:36:33', '', '', '', '', ''),
(81, 'VRDP8', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'PYNI4', '', 'ITHIUHCE9z', '2019-12-03 10:21:15', '2020-01-13 18:35:12', '', 0.00, '', '', '', '', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-10-26 15:21:08', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2020-01-13 18:35:12', 0, '', '2020-01-13 18:35:19', '', '', '', '', ''),
(82, 'HUVBI', 0, 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'HV850', '', 'V7XCbQzYVv', '2020-01-21 18:19:46', '2020-01-21 18:21:53', 'QUOTATION', 12000.00, 'Quotation', 'book now and get a discount', '', '2020-01-21 18:28:51', 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'Nairobi cakes', 'Cakes', '2019-09-09 08:52:44', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'onecall', '2019-06-03 09:33:51', 'how much for 5 tier cake', '', -1, 'sf', '2020-12-02 07:43:44', 0, '', '2020-01-21 18:28:55', '2020-01-21 18:29:29', 'OaVHtlFDGvqApA3u3dQZeklVdBDHC4Nu', '', '', ''),
(84, 'OLUPZ', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'XE6QT', '', 'ITHIUHCE9z', '2020-03-06 09:29:31', '2020-03-10 18:37:16', '', 0.00, '', '', '', '', 'xyjibdKro8vD22paWa4NSBWVMuxwcwZd', 'Elle\\\'s Transport', 'Lighting & Decor', '2020-01-21 19:47:01', '5SnRru8hEy0F45FgsbJMymL5XHZd3NrD', 'Tupange', '2020-01-21 19:04:45', '', '', 0, '', '2020-03-10 18:37:16', 0, '', '2020-03-10 18:37:26', '', '', '', '', ''),
(85, 'YNVU8', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'dca93bbbc4c7baf912b3dd94ee48a061', '', 'ITHIUHCE9z', '2020-03-06 09:29:32', '2020-03-10 18:37:16', '', 0.00, '', '', '', '', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-10-26 15:21:08', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2020-03-10 18:37:16', 0, '', '2020-03-10 18:37:26', '', '', '', '', ''),
(86, 'TKJL5', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'c5985549787b122018aebab92beb11ab', '', 'ITHIUHCE9z', '2020-03-06 09:29:35', '2020-03-10 18:37:16', '', 0.00, '', '', '', '', 'S1ikxcj0Zn1ulfDS5JJIlxs9YOjaWMjO', 'Trial Service', 'Other Services', '2019-10-26 09:41:15', '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', 'mem en', '2019-05-29 21:20:44', '', '', 1, '', '2020-03-15 12:40:25', 0, '', '2020-03-10 18:37:26', '2020-03-13 00:49:46', '', '', '', ''),
(87, '8UXWA', 0, 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'IOQKL', '', 'bk1cvYJOSZ', '2020-04-26 17:29:06', '2020-04-26 17:29:31', 't8zVueaTnKDLIxPcWmbP8wDOjjvolOi4', 1000.00, 'Basic', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus sapien, malesuada non ipsum at, auctor dictum urna.', '[\\\"Package Item 1\\\",\\\"Package Item 2\\\",\\\"Package Item 3\\\"]', '2019-08-15 01:26:22', 'YpKbve8t66PZB6Ms4482pRarHfQgCOED', 'Zuri Flowers', 'Flowers', '2019-06-27 14:56:49', 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'Test', '2019-05-29 20:32:34', '', '', 0, '', '2020-04-26 17:29:31', 0, '', '2020-04-26 17:29:54', '', '', '', '', ''),
(88, '4V6IO', 0, 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'SR462', '', '', '2020-07-26 18:11:21', '2020-07-26 18:11:44', '', 0.00, '', '', '', '', 'xyjibdKro8vD22paWa4NSBWVMuxwcwZd', 'Elle\\\'s Transport', 'Lighting & Decor', '2020-01-21 19:47:01', '5SnRru8hEy0F45FgsbJMymL5XHZd3NrD', 'Tupange', '2020-01-21 19:04:45', '', '', 0, '', '2020-07-26 18:11:44', 0, '', '2020-07-26 18:11:52', '', '', '', '', ''),
(89, '3IKWL', 0, 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'AOLZT', '', '', '2020-12-02 07:42:29', '2020-12-02 07:43:14', '', 0.00, '', '', '', '', 'YpKbve8t66PZB6Ms4482pRarHfQgCOED', 'Zuri Flowers', 'Flowers', '2019-06-27 14:56:49', 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'Test', '2019-05-29 20:32:34', 'Do you sell purple flowers?', '', 0, '', '2020-12-02 07:43:14', 0, '', '', '', '', '', '', ''),
(90, 'PQRLS', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'TR2TH', '', '4DW8YTpC69', '2021-06-10 14:36:47', '2021-06-10 14:37:57', 'QUOTATION', 6000.00, 'Quotation', '', '', '2021-06-10 14:46:45', 'U2c7n84KmiFA04NYAWpt5pkpxFQQfxoT', 'Pretty Flowers', 'Flowers', '2021-06-10 14:29:43', '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', 'mem en', '2019-05-29 21:20:44', 'I need flowers for birthday my budget is 5000', '', 1, '', '2021-06-10 14:43:04', 0, '', '2021-06-10 14:38:06', '2021-06-10 14:39:09', 'IGlSUI7xaCEOscphiQ6njI0SisPJchE1', '', '', ''),
(91, 'FOCDP', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '0ec911bccc3ecf778e5dbac6a8b8116f', '', '4DW8YTpC69', '2021-06-10 14:36:50', '2021-06-10 14:37:57', '', 0.00, '', '', '', '', 'xyjibdKro8vD22paWa4NSBWVMuxwcwZd', 'Elle\\\'s Transport', 'Lighting & Decor', '2020-01-21 19:47:01', '5SnRru8hEy0F45FgsbJMymL5XHZd3NrD', 'Tupange', '2020-01-21 19:04:45', '', '', 0, '', '2021-06-10 14:37:57', 0, '', '2021-06-10 14:38:06', '', '', '', '', ''),
(92, '5426G', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'b97d49819690e482b63fc76a13d1e84c', '', '4DW8YTpC69', '2021-06-10 14:36:52', '2021-06-10 14:37:57', '', 0.00, '', '', '', '', 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'DJ Service', 'DJs', '2019-10-26 15:21:08', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Oloi Tik Tok', '2019-06-04 19:09:20', '', '', 0, '', '2021-06-10 14:37:57', 0, '', '2021-06-10 14:38:06', '', '', '', '', ''),
(93, '0UWHT', 0, 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', '05da138c2d7a1d4b55a3b83d99c797c5', '', '4DW8YTpC69', '2021-06-10 14:36:52', '2021-06-10 14:37:57', '', 0.00, '', '', '', '', 'S1ikxcj0Zn1ulfDS5JJIlxs9YOjaWMjO', 'Trial Service', 'Other Services', '2019-10-26 09:41:15', '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', 'mem en', '2019-05-29 21:20:44', '', '', 1, '', '2021-06-10 14:43:23', 0, '', '2021-06-10 14:38:06', '2021-06-10 14:39:09', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_payments`
--

CREATE TABLE `tb_payments` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `ref` varchar(200) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `amount` double(20,2) NOT NULL,
  `data` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_payments`
--

INSERT INTO `tb_payments` (`_index`, `id`, `uid`, `ref`, `order_id`, `amount`, `data`, `timestamp`) VALUES
(1, 'p4OBNDS5nP1UiQ3pHHM2wxuq1YZgNLjD', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'DEMO_JOT42GC9QZ', '8GIV0', 100000.00, '{\\\"names\\\":\\\"Limo\\\",\\\"number\\\":\\\"4500000000000000\\\",\\\"exp_month\\\":10,\\\"exp_year\\\":22,\\\"cvc\\\":\\\"123\\\"}', '2019-10-26 15:13:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_register`
--

CREATE TABLE `tb_register` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `barcode` varchar(200) NOT NULL,
  `names` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `company` varchar(200) NOT NULL,
  `ticket_title` varchar(200) NOT NULL,
  `ticket_price` double(20,2) NOT NULL,
  `guests` int(20) NOT NULL,
  `amount` double(20,2) NOT NULL,
  `notes` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `sent` varchar(200) NOT NULL DEFAULT '',
  `time_created` datetime NOT NULL,
  `time_modified` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_register`
--

INSERT INTO `tb_register` (`_index`, `id`, `event_id`, `barcode`, `names`, `phone`, `email`, `address`, `company`, `ticket_title`, `ticket_price`, `guests`, `amount`, `notes`, `status`, `sent`, `time_created`, `time_modified`) VALUES
(1, 'vBcRQz0xCRztnxeic2jUbofx0YeDhmDj', 'FLOekhHX0v', '6CKGAYWQUE', 'John Doe', '', '', '', '', 'VIP', 1000.00, 2, 2000.00, '', 0, '', '2019-09-09 01:18:00', '2019-09-09 01:18:00'),
(2, 'XoyYTI9Rnyb1i83Ljq1uG7jDewAf8h5H', 'FLOekhHX0v', 'IJDFJTVD5T', 'Jane Doe', '', '', '', '', 'VVIP', 5000.00, 1, 5000.00, '', 0, '', '2019-09-09 01:18:10', '2019-09-09 01:18:10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_register_tickets`
--

CREATE TABLE `tb_register_tickets` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(1000) NOT NULL DEFAULT '',
  `price` double(20,2) NOT NULL DEFAULT 0.00,
  `guests_limit` int(20) NOT NULL DEFAULT 0,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_register_tickets`
--

INSERT INTO `tb_register_tickets` (`_index`, `id`, `event_id`, `title`, `description`, `price`, `guests_limit`, `timestamp`) VALUES
(1, 'ND0G9l7I05SZypeO6ew6GXupcLQHQu4g', 'FLOekhHX0v', 'VIP', 'VIP Ticket', 1000.00, 200, '2019-09-09 01:16:22'),
(2, 'aIjJ3Iw675u5xBebxNueqUN1f3w49stN', 'FLOekhHX0v', 'VVIP', 'VVIP Ticket', 5000.00, 100, '2019-09-09 01:17:42'),
(3, 'QRyEAU71QhlNmn6jCFZMEz4rNdH5noMn', 'ITHIUHCE9z', 'VVIP', 'VIP TICKETS', 3000.00, 1000, '2019-09-24 20:25:03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_reviews`
--

CREATE TABLE `tb_reviews` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `service_id` varchar(200) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `barcode` varchar(200) NOT NULL DEFAULT '',
  `uid` varchar(200) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `rating` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_reviews`
--

INSERT INTO `tb_reviews` (`_index`, `id`, `service_id`, `order_id`, `event_id`, `barcode`, `uid`, `avatar`, `name`, `type`, `rating`, `comment`, `timestamp`) VALUES
(1, '6RUA1DmRm6Z05G012ex7VLy50n7bar4O', 'uIz9iyGWPh36TUTknfAenndjcEw2BW1E', 'ON7MW', 'ITHIUHCE9z', '', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'avatar-J3xJnwpDuD.png', 'Limo Sadalla', 1, 1, 'VERY TERRIBLE. WILL NOT RECOMMEND. SAVE YOUR MONEY', '2019-10-02 09:10:44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_service_gallery`
--

CREATE TABLE `tb_service_gallery` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `service_id` varchar(200) NOT NULL,
  `caption` varchar(200) NOT NULL DEFAULT '',
  `image` longtext NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tasks`
--

CREATE TABLE `tb_tasks` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `task` varchar(200) NOT NULL,
  `person` varchar(200) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT 0,
  `time_notified` varchar(200) NOT NULL DEFAULT '',
  `assign_emailed` varchar(200) NOT NULL DEFAULT '',
  `date` varchar(200) NOT NULL,
  `user_id` varchar(200) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_tasks`
--

INSERT INTO `tb_tasks` (`_index`, `id`, `event_id`, `task`, `person`, `notify`, `time_notified`, `assign_emailed`, `date`, `user_id`, `status`, `timestamp`) VALUES
(1, 'mxOBX2CGqM', 'FLOekhHX0v', 'Get Transport', '', 0, '', '', '2019-10-17', '', 1, '2019-10-31 01:01:57'),
(3, 'apo9cnGzOk', 'FLOekhHX0v', 'Another task', '', 0, '', '', '', '', 0, '2019-10-31 00:53:36'),
(5, '0bJh6YtALy', 'LYcPuI1yLX', 'transportt', '', 0, '', '', '', '', 1, '2019-11-04 22:01:53'),
(6, 'GTq9h6E1Pz', 'LYcPuI1yLX', 'try', '', 0, '', '', '', '', 1, '2019-11-04 22:02:13'),
(7, 'JCGJsO8Pps', 'LYcPuI1yLX', 'enter', '', 0, '', '', '2019-12-12', '', 1, '2019-11-04 22:02:51'),
(9, 'BDYkGMNoT0', 'kH6rzBHgZL', 'Call attendant', '', 0, '', '', '2019-11-30', '', 0, '2019-11-09 12:15:21'),
(11, 'RgwK0pS2t5', 'kH6rzBHgZL', 'Buy Dress', 'Limo Dallas', 0, '', '', '2019-11-16', '', 0, '2019-11-09 12:14:54'),
(12, 'TKPj9DiEdR', 'kH6rzBHgZL', 'Platform', 'Mike', 0, '', '', '2019-11-13', '', 0, '2019-11-09 12:13:38'),
(13, 'EYhYIo23NK', 'kH6rzBHgZL', 'Speech', 'Franjo', 0, '', '', '2019-12-01', '', 0, '2019-11-09 12:13:39'),
(14, 'cGSqcDSErj', 'QyAVdptXmJ', 'Test task', 'owner', 0, '', '', '2019-11-29', 'vTzVEFBZDB0pu8tfzZKQHcUDyl5t19lE', 0, '2019-11-26 12:48:07'),
(15, 'u1CEK2HPi8', 'N4oqpbOqwI', 'cake', 'owner', 1, '2019-12-07 00:00:04', 'owner', '2019-12-18', 'owner', 3, '2019-12-07 00:08:05'),
(16, 'jmCUlkvNrL', 'V7XCbQzYVv', 'Transport', 'uVgW5hsIhhmXDujrN5Gf0IcqdVyvhVva', 1, '2019-12-26 00:00:05', 'uVgW5hsIhhmXDujrN5Gf0IcqdVyvhVva', '2019-12-26', 'owner', 1, '2019-12-25 04:29:59'),
(17, 'l7JsrLGfys', 'V7XCbQzYVv', 'Dress', 'owner', 1, '', 'owner', '2019-12-26', 'owner', 3, '2019-12-25 04:37:08'),
(18, 'AXF23O1e33', 'V7XCbQzYVv', 'Cake', '', 2, '', '', '2019-12-27', 'owner', 0, '2019-12-25 04:34:29'),
(19, 'm1OFTIkf2p', 'GdRk51MIWl', 'transport', 'owner', 2, '2020-01-16 00:00:04', 'owner', '2020-01-16', 'owner', 0, '2020-01-11 16:20:15'),
(20, 'wcw1e4Unru', '0BfvxO3yyD', 'Cake', '', 0, '', '', '2020-03-25', 'owner', 3, '2020-03-13 01:06:28'),
(21, 'VQqdlT2WZN', '0BfvxO3yyD', 'balloons', 'owner', 1, '', 'owner', '2020-03-21', 'owner', 2, '2020-03-15 12:31:56'),
(22, 'lfQKqY4hnb', '0BfvxO3yyD', 'Transport', '', 0, '', '', '2020-03-28', 'owner', 2, '2020-03-15 12:33:27'),
(23, 'CZNYOgZizE', '0BfvxO3yyD', 'decor', '', 0, '', '', '2020-03-19', 'owner', 1, '2020-03-15 12:33:03'),
(24, 'VR17ZhfuPc', 'bk1cvYJOSZ', 'sdfsdf', 'owner', 1, '', 'owner', '2020-04-09', 'owner', 1, '2020-07-18 07:29:52'),
(25, 'Nu2NXgZKEa', '4DW8YTpC69', 'Order DJ', 'owner', 1, '2021-06-17 00:00:15', 'owner', '2021-06-17', 'owner', 0, '2021-06-10 14:48:40'),
(26, 'z962SZifIH', '4DW8YTpC69', 'Buy flowers', 'owner', 1, '2021-06-17 00:00:17', 'owner', '2021-06-17', 'owner', 0, '2021-06-10 14:48:56');

-- --------------------------------------------------------

--
-- Table structure for table `tb_task_comments`
--

CREATE TABLE `tb_task_comments` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `task_id` varchar(200) NOT NULL,
  `event_id` varchar(200) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `user_id` varchar(200) NOT NULL,
  `comment` text NOT NULL,
  `tagged` text NOT NULL,
  `file_name` varchar(200) NOT NULL DEFAULT '',
  `file_src` varchar(200) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `seen` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_task_comments`
--

INSERT INTO `tb_task_comments` (`_index`, `id`, `task_id`, `event_id`, `uid`, `user_id`, `comment`, `tagged`, `file_name`, `file_src`, `status`, `timestamp`, `seen`) VALUES
(1, 'VMA8H29TADGR2gO4ERxbzrF7twW2bulb', 'cGSqcDSErj', 'QyAVdptXmJ', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ', 'vTzVEFBZDB0pu8tfzZKQHcUDyl5t19lE', 'This is a test comment', '[]', 'lorem.txt', 'tcfiles/QyAVdptXmJ-VMA8H29TADGR2gO4ERxbzrF7twW2bulb.txt.tc', 1, '2019-11-26 12:49:57', 'DF1s3cCr1QCu1Dhv3f2PwhjSRDavWBpZ'),
(2, 'jRpvcj8CHs0538fuJcVXmbM6IsrdScmv', 'jmCUlkvNrL', 'V7XCbQzYVv', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'owner', '@che where are we on this?', '[]', '', '', 1, '2019-12-25 04:32:43', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ'),
(3, 'Gjcp0DW2g8qfnOanqnTv1nUTPNEfmebf', 'l7JsrLGfys', 'V7XCbQzYVv', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'owner', 'good job', '[]', '', '', 1, '2019-12-25 04:33:01', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ'),
(4, 'XdcItmUGPGWBfkfM6uxexQn8ZtCexwos', 'VQqdlT2WZN', '0BfvxO3yyD', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'owner', 'good job', '[]', '', '', 1, '2020-03-13 01:09:31', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ'),
(5, 'QXNsYya6vs43QzZVLcuximb4cmQAfkX4', 'Nu2NXgZKEa', '4DW8YTpC69', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U', 'owner', 'test', '[]', '', '', 1, '2021-06-10 14:49:17', 'NqYndkYpnDk9CYStLL8TXXocDL1PlQ6U');

-- --------------------------------------------------------

--
-- Table structure for table `tb_temp`
--

CREATE TABLE `tb_temp` (
  `_index` int(20) NOT NULL,
  `ref` varchar(200) NOT NULL,
  `key` varchar(200) NOT NULL,
  `value` text DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `valid_days` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_testimonials`
--

CREATE TABLE `tb_testimonials` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `uid` varchar(200) DEFAULT NULL,
  `names` varchar(200) NOT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `testimony` varchar(300) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_testimonials`
--

INSERT INTO `tb_testimonials` (`_index`, `id`, `uid`, `names`, `avatar`, `testimony`, `timestamp`) VALUES
(1, 'RUnGXBW3tE', 'SYSTEM', 'John Doe', 'assets/img/home/avatar-1.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus sapien, malesuada non ipsum at, auctor dictum urna. Donec at ex eu nisi efficitur gravida sit amet tempus eros.', '2019-06-14 15:40:40'),
(2, 'VwhbDo0tZ0', 'SYSTEM', 'Jane Doe', 'assets/img/home/avatar-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus sapien, malesuada non ipsum at, auctor dictum urna. Donec at ex eu nisi efficitur gravida sit amet tempus eros.', '2019-06-14 15:40:40'),
(3, 'CmNUdjyGNF', 'SYSTEM', 'John Smith', 'assets/img/home/avatar-3.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus sapien, malesuada non ipsum at, auctor dictum urna. Donec at ex eu nisi efficitur gravida sit amet tempus eros.', '2019-06-14 15:40:40');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendors`
--

CREATE TABLE `tb_vendors` (
  `_index` int(11) NOT NULL,
  `id` varchar(200) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `company_name` varchar(200) NOT NULL,
  `company_email` varchar(200) NOT NULL,
  `company_description` text NOT NULL,
  `location_lat` double NOT NULL,
  `location_lon` double NOT NULL,
  `location_name` varchar(200) NOT NULL,
  `location_type` varchar(200) NOT NULL,
  `location_map_lat` double NOT NULL,
  `location_map_lon` double NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_vendors`
--

INSERT INTO `tb_vendors` (`_index`, `id`, `uid`, `company_name`, `company_email`, `company_description`, `location_lat`, `location_lon`, `location_name`, `location_type`, `location_map_lat`, `location_map_lon`, `timestamp`) VALUES
(1, 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'M4yf5gvGCijsAoMi54rExNlgG5wgqfLK', 'Test', 'test@example.com', 'Hello world!', -1, 36, 'Roysambu', 'CITY', -1, 36, '2019-05-29 20:32:34'),
(2, '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', 'iw6p0YI083rNl1jXLGHmYQVT2msL7yEJ', 'mem en', 'cheriwamuturi@gmail.com', 'testing', -1, 36, 'Nairobi', 'COUNTY', -1, 36, '2019-05-29 21:20:44'),
(3, '7FSl80vbuiisY8I9LPjj4MogBa9N0gYz', 'fAEryS4d8g6uIgdYpF6LSPTWeSIwossh', 'Blue Consulting', 'lsadalla@blueconsulting.co.ke', '', -1, 36, 'Current Location', 'CURRENT', 0, 0, '2019-06-02 13:12:50'),
(4, 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'xRjvBAB1h4P4ATBycRCw1k8pwwEOlp7i', 'onecall', 'onecalleventskenya@gmail.com', '', -1, 36, 'Current Location', 'CURRENT', -1, 36, '2019-06-03 09:33:51'),
(5, 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'z1LJLrhYhMFqOWCMVGFAee7aBbSHEec5', 'Oloi Tik Tok', 'fngari@gmail.com', 'We provide Photo Services. We have a team  of 10 photographers who are highly skilled and trained. They have high spec Cameras with over 2 inch sensor sizes.We have a team of 5 DJ\\\'s Each with an ultra modern deck', -1, 36, 'Nairobi', 'COUNTY', 0, 0, '2019-06-04 19:09:20'),
(6, 'shnMwlc5DhYZnxcHXnXxTtaPXNicWmI0', 'cEY2HYoVHiZQ5e3t1QLdaiITppTfZdq1', 'Blue Consulting', 'lsadalla@blueconsulting.co.ke', '', -1, 36, 'Current Location', 'CURRENT', 0, 0, '2019-06-04 20:06:30'),
(7, 'cjRB5SIbPy7EtW7Zzjh0tbJEKaORmzwy', 'LF976yAwqi2REXnBP59WHKrFB3fzNwHG', 'Mboga', 'Jejehe@fg.hj', 'Gh', -1, 36, 'Nairobi', 'COUNTY', -1, 36, '2019-06-07 21:00:22'),
(8, 'wzKQgdphjIziQ8Urykz0l7VycNEF7dkm', 'Yf4ccb5vyWplQy2UqpnCNXTeNcxSLaT2', 'Test', 'test@example.com', 'Hello world', -1.27615, 36.839269, 'Nairobi', 'COUNTY', -1.27615, 36.839269, '2019-06-08 20:34:50'),
(9, 'TCMFlwVLqDZh0vObKwnrtess3VSHbB52', 'H5FRGWKEXaivpKZMzlj2jU89OPR4vNrt', 'Blue Consulting', 'lsadalla@blueconsulting.co.ke', 'asdf', -1.27615, 36.839269, 'Nairobi', 'COUNTY', -1.27615, 36.839269, '2019-06-25 19:34:45'),
(10, 'LGNYA5iPJP68UZEGF5f4oyu5RvV4UlOG', '5S7WRHYAvLXpYTjq9ukhaKtWEsSQEl1d', 'Test Company', 'company@email.com', '', -1.27615, 36.839269, 'Nairobi', 'COUNTY', -1.2786585, 36.75066229999993, '2019-07-08 09:18:38'),
(11, 'KHyA5r3wMHM4fHODiBk6YZsd4vEyvzAI', '0zVi4WHbKF71nQ4N9SsyxRkAZk8amupg', 'Blue Consulting', 'lsadalla@blueconsulting.co.ke', 'Blue', -1.27615, 36.839269, 'Nairobi', 'COUNTY', -1.2613907410855, 36.804593401856, '2019-07-16 19:03:57'),
(12, '5SnRru8hEy0F45FgsbJMymL5XHZd3NrD', '40RNorzfIWiKKdTeqH5HXzcqTWxdZAIL', 'Tupange', '6luis.dalmonec@tampabayluxuryagent.com', 'dj  and photo', -1.2959826, 36.8067953, 'Current Location', 'CURRENT', -1.2959826, 36.8067953, '2020-01-21 19:04:45');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendor_categories`
--

CREATE TABLE `tb_vendor_categories` (
  `_index` int(11) NOT NULL,
  `vendor_id` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `value` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_vendor_categories`
--

INSERT INTO `tb_vendor_categories` (`_index`, `vendor_id`, `name`, `value`) VALUES
(1, 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'Photography', '1'),
(2, 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'Beauty & Health', '3'),
(3, 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'DJs', '2'),
(4, 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'Wedding Planning', '4'),
(5, '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', 'DJs', '2'),
(6, '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', 'Videography', '7'),
(7, '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', 'Photography', '1'),
(8, '7FSl80vbuiisY8I9LPjj4MogBa9N0gYz', 'Photography', '1'),
(9, '7FSl80vbuiisY8I9LPjj4MogBa9N0gYz', 'DJs', '2'),
(10, '7FSl80vbuiisY8I9LPjj4MogBa9N0gYz', 'Beauty & Health', '3'),
(11, 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'Photography', '1'),
(12, 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'Catering', '5'),
(13, 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Photography', '1'),
(14, 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'DJs', '2'),
(15, 'shnMwlc5DhYZnxcHXnXxTtaPXNicWmI0', 'Beauty & Health', '3'),
(16, 'shnMwlc5DhYZnxcHXnXxTtaPXNicWmI0', 'Catering', '5'),
(17, 'shnMwlc5DhYZnxcHXnXxTtaPXNicWmI0', 'Videography', '7'),
(18, 'cjRB5SIbPy7EtW7Zzjh0tbJEKaORmzwy', 'DJs', '2'),
(19, 'cjRB5SIbPy7EtW7Zzjh0tbJEKaORmzwy', 'Transportation', '14'),
(20, 'wzKQgdphjIziQ8Urykz0l7VycNEF7dkm', 'Photography', '1'),
(21, 'wzKQgdphjIziQ8Urykz0l7VycNEF7dkm', 'Wedding Planning', '4'),
(22, 'wzKQgdphjIziQ8Urykz0l7VycNEF7dkm', 'Catering', '5'),
(23, 'TCMFlwVLqDZh0vObKwnrtess3VSHbB52', 'DJs', '2'),
(24, 'LGNYA5iPJP68UZEGF5f4oyu5RvV4UlOG', 'Photography', '1'),
(25, 'LGNYA5iPJP68UZEGF5f4oyu5RvV4UlOG', 'Flowers', '6'),
(26, 'LGNYA5iPJP68UZEGF5f4oyu5RvV4UlOG', 'Event Rentals', '9'),
(27, 'LGNYA5iPJP68UZEGF5f4oyu5RvV4UlOG', 'Catering', '5'),
(28, 'LGNYA5iPJP68UZEGF5f4oyu5RvV4UlOG', 'Dress & Attire', '12'),
(29, 'LGNYA5iPJP68UZEGF5f4oyu5RvV4UlOG', 'Cakes', '13'),
(30, 'LGNYA5iPJP68UZEGF5f4oyu5RvV4UlOG', 'Transportation', '14'),
(31, 'LGNYA5iPJP68UZEGF5f4oyu5RvV4UlOG', 'Ceremony Music', '15'),
(32, 'KHyA5r3wMHM4fHODiBk6YZsd4vEyvzAI', 'Photography', '1'),
(33, 'KHyA5r3wMHM4fHODiBk6YZsd4vEyvzAI', 'Catering', '5'),
(34, '5SnRru8hEy0F45FgsbJMymL5XHZd3NrD', 'Photography', '1'),
(35, '5SnRru8hEy0F45FgsbJMymL5XHZd3NrD', 'Wedding Planning', '4'),
(36, '5SnRru8hEy0F45FgsbJMymL5XHZd3NrD', 'Videography', '7');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendor_services`
--

CREATE TABLE `tb_vendor_services` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `vendor_id` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `category_value` tinyint(2) NOT NULL,
  `location_name` varchar(200) NOT NULL,
  `location_lat` double NOT NULL,
  `location_lon` double NOT NULL,
  `location_map_lat` double NOT NULL,
  `location_map_lon` double NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `sponsored` tinyint(1) NOT NULL DEFAULT 0,
  `paused` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_vendor_services`
--

INSERT INTO `tb_vendor_services` (`_index`, `id`, `vendor_id`, `image`, `title`, `description`, `category_name`, `category_value`, `location_name`, `location_lat`, `location_lon`, `location_map_lat`, `location_map_lon`, `timestamp`, `featured`, `sponsored`, `paused`, `deleted`) VALUES
(2, 'YpKbve8t66PZB6Ms4482pRarHfQgCOED', 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 'jYdqv74bJx.jpeg', 'Zuri Flowers', 'Beautiful flowers for any occasion. Contact us and we deliver', 'Flowers', 6, 'Current Location', -1.2845056, 36.831232, -1.2845056, 36.831232, '2019-06-27 14:56:49', 0, 0, 0, 0),
(4, 'RopbqqpZwj98k0WW2hGZ879AlVmlX7gg', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', '3K5s34O23M.jpeg', 'Test', 'Test', 'Photography', 1, 'Nairobi', -1.27615, 36.839269, -1.27615, 36.839269, '2019-06-04 19:13:02', 0, 0, 1, 0),
(7, 'Y0864JpMoHg0tTA2V7BrsYO9wsTZLChG', 'wzKQgdphjIziQ8Urykz0l7VycNEF7dkm', 'ruKl8GlU9L.jpg', 'Test Service', 'This service has been created for demo purposes.', 'Photography', 1, 'Nairobi', -1.27615, 36.839269, -1.27615, 36.839269, '2019-06-19 01:57:33', 0, 0, 0, 0),
(8, 'uIz9iyGWPh36TUTknfAenndjcEw2BW1E', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', '6sppCab9RT.jpeg', 'Budget Bride Car Hire', 'Budget bride car hire. We are the best at what we do.', 'Transportation', 14, 'Nairobi', -1.27615, 36.839269, -1.0875428, 35.8770642, '2019-09-24 19:22:12', 0, 0, 1, 0),
(9, 'fg418A6EBPLjntqfUGtoZCa03xy0gulC', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', '4wLtqwr7FL.jpeg', 'Nairobi cakes', 'Cakes cakes and more cakes. Order from us and enjoy', 'Cakes', 13, 'Nakuru', -0.378619, 35.926952, -0.378619, 35.926952, '2019-09-09 08:52:44', 0, 0, 0, 0),
(10, 'Q2MyzMua9FzCqfA0PDzT0jO4jPJF7pw8', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'NdXfIYqj8f.jpg', 'Yummy Food', 'Best catering service in Nairobi and its environs', 'Catering', 5, 'Nairobi', -1.27615, 36.839269, -1.27615, 36.839269, '2019-06-19 07:45:27', 0, 0, 0, 0),
(11, 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'bUT8pz2aUB.jpg', 'Honeymoon Safaris', 'Now that your wedding is over, let us help you relax by providing with a trip that will crown it all.', 'Travel Agents', 18, 'Kiambu', -1.08135, 36.785095, -1.08135, 36.785095, '2019-06-23 13:19:16', 0, 0, 0, 0),
(12, '9oe5rwmH3T5hcPrgdnpgoAovQ92sU9UH', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'LrbJXJCAIL.jpg', 'Pepea Studio', 'We take Photos and print them on high quality paper with high quality ink, giving you a long lasting product.', 'Photography', 1, 'Nairobi', -1.27615, 36.839269, -1.22939, 36.78595480000001, '2019-07-02 18:27:34', 0, 0, 0, 0),
(13, 'ZPYWTyfEQZFsoCa2ItX8CYrsoxBLRiei', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', '33wJSFHhqG.jpeg', 'Tupange Caterers', 'We provide all services in catering. Cakes, Food menu of your choice,', 'Catering', 5, 'Limuru Central', -1.108855, 36.623122, -1.108594, 36.62002050000001, '2019-07-02 18:30:50', 0, 0, 0, 0),
(14, '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 'OhCIe4F8D2.jpg', 'Makeki Mathwiti bakery', 'Whatever the occasion is, you can\\\'t go wrong with a cake.', 'Cakes', 13, 'Limuru Central', -1.108855, 36.623122, -1.108855, 36.623122, '2019-08-20 19:26:43', 0, 0, 0, 0),
(15, 'HQfkeGFDNEic160IPtQZRx1xy6Zm5X8l', 'FZQqFDtkVmGPf3BXRWzibhDAXgIJQV5Q', 'Mg98FL9xFw.jpg', 'DJ Service', 'Sound systems, microphones, Speakers, Dj Services. We theme events with your Genre of choice.From Reggae riddims to rhythm and blues', 'DJs', 2, 'Nairobi', -1.27615, 36.839269, -1.2994605, 36.7906854, '2019-10-26 15:21:08', 0, 0, 0, 0),
(16, 'S1ikxcj0Zn1ulfDS5JJIlxs9YOjaWMjO', '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', 'UnnFFaeXnt.JPG', 'Trial Service', 'Chap chap. We offer exclusive services that can be accessed anywhere.', 'Other Services', 0, 'Nairobi', -1.27615, 36.839269, -1.27615, 36.839269, '2019-10-26 09:41:15', 0, 0, 0, 0),
(17, 'xyjibdKro8vD22paWa4NSBWVMuxwcwZd', '5SnRru8hEy0F45FgsbJMymL5XHZd3NrD', 'B15SGgtXyS.JPG', 'Elle\\\'s Transport', 'Fhtjh f hgndnhyd4tr  rht4yawn57uy57uj6546543543ryhsu5hrsbtE', 'Lighting & Decor', 16, 'Nairobi', -1.27615, 36.839269, -1.27615, 36.839269, '2020-01-21 19:47:01', 0, 0, 0, 0),
(18, 'U2c7n84KmiFA04NYAWpt5pkpxFQQfxoT', '2JVDvoqk1EVxpSZDQMBPzaqyXFltB2cv', '5g1BWjSWuo.png', 'Pretty Flowers', 'Flower service for people who like pretty things', 'Flowers', 6, 'Nairobi', -1.27615, 36.839269, -1.2920659, 36.8219462, '2021-06-10 14:29:43', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendor_service_pricing`
--

CREATE TABLE `tb_vendor_service_pricing` (
  `_index` int(20) NOT NULL,
  `id` varchar(200) NOT NULL,
  `service_id` varchar(200) NOT NULL,
  `vendor_id` varchar(200) NOT NULL,
  `price` double NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `package` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_vendor_service_pricing`
--

INSERT INTO `tb_vendor_service_pricing` (`_index`, `id`, `service_id`, `vendor_id`, `price`, `title`, `description`, `package`, `timestamp`) VALUES
(1, 't8zVueaTnKDLIxPcWmbP8wDOjjvolOi4', 'YpKbve8t66PZB6Ms4482pRarHfQgCOED', 'FZZxFy7AmQ7Yxm1PoabydGJxKKn3aQ6J', 1000, 'Basic', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tellus sapien, malesuada non ipsum at, auctor dictum urna.', '[\\\"Package Item 1\\\",\\\"Package Item 2\\\",\\\"Package Item 3\\\"]', '2019-08-15 01:26:22'),
(2, 'YqacDiObbNVznBPew1jtTTcjtyouDjsO', '987eX40vqO0OjGU1ZI4MBK8Dxind3wD6', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 10000, 'Epic cake', 'Best cakes in town', '[]', '2019-09-17 12:39:41'),
(3, '9WYi5lAHOXz1yjwhLqvs8aPM8bjtwgRN', 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 50000, 'HoneyMoon Safaris', 'Best after wedding safaris', '[]', '2019-10-05 02:38:03'),
(4, 'pTnfX9EF2IEaFCZKGshfiPA4bIQ2c32p', 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 20000, 'Ya Regular', 'Package that works across all wallets', '[]', '2019-10-26 07:45:34'),
(5, 'nCi0xtHgjFLTr3p6mzglyak6duFnLnQt', 'cs4EZ7le3Mk1y82orES3NhwnhEwna4FK', 'TDc4ibMRboeMcG7n9iLwTG6bTscH9qKT', 5, 'Custom', 'Customize as per your needs', '[]', '2019-10-26 07:46:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ns_audit`
--
ALTER TABLE `ns_audit`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ns_users`
--
ALTER TABLE `ns_users`
  ADD PRIMARY KEY (`uid`),
  ADD KEY `_index` (`_index`);

--
-- Indexes for table `tb_attendance`
--
ALTER TABLE `tb_attendance`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_budget`
--
ALTER TABLE `tb_budget`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_budget_transactions`
--
ALTER TABLE `tb_budget_transactions`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_cart`
--
ALTER TABLE `tb_cart`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_checklist_groups`
--
ALTER TABLE `tb_checklist_groups`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_checklist_items`
--
ALTER TABLE `tb_checklist_items`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_events`
--
ALTER TABLE `tb_events`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_event_guests`
--
ALTER TABLE `tb_event_guests`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_event_users`
--
ALTER TABLE `tb_event_users`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_favorites`
--
ALTER TABLE `tb_favorites`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_gifts`
--
ALTER TABLE `tb_gifts`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_messages`
--
ALTER TABLE `tb_messages`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_notifications`
--
ALTER TABLE `tb_notifications`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_orders`
--
ALTER TABLE `tb_orders`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_payments`
--
ALTER TABLE `tb_payments`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_register`
--
ALTER TABLE `tb_register`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_register_tickets`
--
ALTER TABLE `tb_register_tickets`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_reviews`
--
ALTER TABLE `tb_reviews`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_service_gallery`
--
ALTER TABLE `tb_service_gallery`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_tasks`
--
ALTER TABLE `tb_tasks`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_task_comments`
--
ALTER TABLE `tb_task_comments`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_temp`
--
ALTER TABLE `tb_temp`
  ADD PRIMARY KEY (`_index`);

--
-- Indexes for table `tb_testimonials`
--
ALTER TABLE `tb_testimonials`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_vendors`
--
ALTER TABLE `tb_vendors`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_vendor_categories`
--
ALTER TABLE `tb_vendor_categories`
  ADD PRIMARY KEY (`_index`);

--
-- Indexes for table `tb_vendor_services`
--
ALTER TABLE `tb_vendor_services`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_vendor_service_pricing`
--
ALTER TABLE `tb_vendor_service_pricing`
  ADD PRIMARY KEY (`_index`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ns_audit`
--
ALTER TABLE `ns_audit`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=308;

--
-- AUTO_INCREMENT for table `ns_users`
--
ALTER TABLE `ns_users`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tb_attendance`
--
ALTER TABLE `tb_attendance`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_budget`
--
ALTER TABLE `tb_budget`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_budget_transactions`
--
ALTER TABLE `tb_budget_transactions`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_cart`
--
ALTER TABLE `tb_cart`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `tb_checklist_groups`
--
ALTER TABLE `tb_checklist_groups`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tb_checklist_items`
--
ALTER TABLE `tb_checklist_items`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tb_events`
--
ALTER TABLE `tb_events`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tb_event_guests`
--
ALTER TABLE `tb_event_guests`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_event_users`
--
ALTER TABLE `tb_event_users`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tb_favorites`
--
ALTER TABLE `tb_favorites`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_gifts`
--
ALTER TABLE `tb_gifts`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_messages`
--
ALTER TABLE `tb_messages`
  MODIFY `_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tb_notifications`
--
ALTER TABLE `tb_notifications`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tb_orders`
--
ALTER TABLE `tb_orders`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `tb_payments`
--
ALTER TABLE `tb_payments`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_register`
--
ALTER TABLE `tb_register`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_register_tickets`
--
ALTER TABLE `tb_register_tickets`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_reviews`
--
ALTER TABLE `tb_reviews`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_service_gallery`
--
ALTER TABLE `tb_service_gallery`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_tasks`
--
ALTER TABLE `tb_tasks`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tb_task_comments`
--
ALTER TABLE `tb_task_comments`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_temp`
--
ALTER TABLE `tb_temp`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT for table `tb_testimonials`
--
ALTER TABLE `tb_testimonials`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_vendors`
--
ALTER TABLE `tb_vendors`
  MODIFY `_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_vendor_categories`
--
ALTER TABLE `tb_vendor_categories`
  MODIFY `_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tb_vendor_services`
--
ALTER TABLE `tb_vendor_services`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tb_vendor_service_pricing`
--
ALTER TABLE `tb_vendor_service_pricing`
  MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
