<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190726
*	CREATE BARCODE
*	https://github.com/picqer/php-barcode-generator
*/

namespace Naicode\Server\Plugin;
require_once __DIR__ . "/../funcs.php";
require_once __DIR__ . "/php-barcode/index.php";
use Naicode\Server\Funcs as fn1;
use Picqer\Barcode\BarcodeGeneratorPNG;

//handle qr code request
if (isset($_GET["barcode"])){
	$text = $_GET["barcode"];
	$width = isset($_GET["width"]) && is_numeric($_GET["width"]) ? $_GET["width"] : null;
	$height = isset($_GET["height"]) && is_numeric($_GET["height"]) ? $_GET["height"] : null;
	CreateBarcode($text, $width, $height);
}

//generate QR code
function CreateBarcode($text="Default", $width=null, $height=null, $output=true, $type=null){
	//generate barcode
	$text = fn1::toStrn($text, true);
	$width = ($width = fn1::toNum($width)) > 0 ? $width : 0;
	$height = ($height = fn1::toNum($height)) > 0 ? $height : 0;

	$generator = new BarcodeGeneratorPNG();
	if (!$type) $type = $generator::TYPE_CODE_128;
	$barcode = $generator -> getBarcode($text, $type);

	if ($width && $height){
		$temp = fn1::getImage($barcode, $width, $height);
		if ($temp) $temp = fn1::imageData($temp, "imagepng");
		if ($temp) $barcode = $temp;
	}

	if (!$output) return $barcode;
	header("Content-Type: image/png");
	echo $barcode;
	exit();
}
