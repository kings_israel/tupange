<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190808
*	HTTP REQUEST
*/

namespace Naicode\Server\Plugin;
require_once __DIR__ . "/../lang.php";
require_once __DIR__ . "/../funcs.php";
use Naicode\Server\Lang as lg;
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\Main;

//lang config
lg::config([
	"REQUEST_INVALID_URL" => ["eng" => "Invalid request url \"%s\""],
	"REQUEST_MAX_REDIRECTS" => ["eng" => "Reached redirections limit \"%s\""],
]);

//main class
class Request extends Main {
	const REQUEST_METHODS = ["HEAD", "GET", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE"];
	public $is_json = false;
	public $follow_redirects = true;
	public $max_redirects = 0;
	public $max_timeout = 30;
	public $url_chain = [];
	private $url;
	private $curl;
	private $data;
	private $method;
	private $query_array;
	private $data_format;
	private $content_type;
	private $curl_options = [];
	private $status = null;
	private $result = null;
	private $response = null;
	private $request_headers = [];
	private $response_headers = [];
	public function __construct($url=null, $method=null, $data=null, $is_json=null){
		if ($url !== null) $this -> setUrl($url);
		if ($method !== null) $this -> setMethod($method);
		if ($data !== null) $this -> setData($data);
		if ($is_json !== null) $this -> is_json = !!$is_json;
	}
	public function setUrl($url, $query_params=null){
		$url = fn1::toStrn($url, true);
		if (!strlen($url) || !fn1::isUrl($url)) return $this;
		$url_query = parse_url($url, PHP_URL_QUERY);
		$url = trim(preg_replace('/\?\s*$/s', "", str_replace($url_query, "", $url)));
		$query_array = self::queryStringArray($url_query);
		$query_params_array = self::queryStringArray($query_params);
		if (count($query_params_array)) $query_array = fn1::arrayMerge($query_array, $query_params_array);
		$this -> url = $url;
		$this -> query_array = $query_array;
		return $this;
	}
	public function setData($data, $is_json=null){
		$this -> data = $data;
		if ($is_json !== null) $this -> is_json = !!$is_json;
		return $this;
	}
	public function setMethod($method){
		$this -> method = $method;
		return $this;
	}
	public function setHeader($header, $value){
		$header = fn1::toStrn($header, true);
		$value = fn1::toStrn($value, true);
		if (strlen($header)){
			$key = fn1::valKey($this -> request_headers, $header);
			if (array_key_exists($key, $this -> request_headers)) unset($this -> request_headers[$key]);
			$this -> request_headers[$header] = $value;
		}
		return $this;
	}
	public function setOptions($curl_options){
		$curl_options = fn1::toArray($curl_options);
		foreach ($curl_options as $key => $value) $this -> curl_options[$key] = $value;
		return $this;
	}
	public function getUrl(){
		$url = fn1::toStrn($this -> url, true);
		if (!strlen($url) || !fn1::isUrl($url)) return $this -> error(lg::str("REQUEST_INVALID_URL", $url));
		return $url;
	}
	public function getMethod(){
		$method = strtoupper(fn1::toStrx($this -> method, true));
		if (!strlen($method) || in_array($method, self::REQUEST_METHODS)) $method = "GET";
		return $method;
	}
	public function getData(){
		return $this -> data;
	}
	public function getQuery($get_query_string=true, $include_GET_data=false){
		$query_array = fn1::toArray($this -> query_array);
		if ($include_GET_data && $this -> getMethod() == "GET"){
			$temp_array = $this -> getDataQuery(false);
			if (count($temp_array)) $query_array = fn1::arrayMerge($query_array, $temp_array);
		}
		return $get_query_string ? http_build_query($query_array) : $query_array;
	}
	public function getDataQuery($get_query_string=true, $include_query_string=false){
		$temp_array = [];
		if ($include_query_string) $temp_array = fn1::toArray($this -> query_array);
		if (!fn1::isEmpty($data = $this -> getData())){
			if (fn1::isObject($data)) $temp_array = count($temp_array) ? fn1::arrayMerge($temp_array, fn1::toArray($data)) : fn1::toArray($data);
			else {
				$temp = fn1::jsonParse($data, true, []);
				if (!count($temp)) $temp = self::queryStringArray(fn1::toStrn($data, true));
				if (count($temp)) $temp_array = count($temp_array) ? fn1::arrayMerge($temp_array, $temp) : $temp;
			}
		}
		return $get_query_string ? http_build_query($temp_array) : $temp_array;
	}
	public function getDataJSON($include_query_string=false, $empty_object=false){
		$temp_array = [];
		if ($include_query_string) $temp_array = $this -> getQuery(false);
		if (!fn1::isEmpty($data = $this -> getData())){
			if (fn1::isObject($data)) $temp_array = count($temp_array) ? fn1::arrayMerge($temp_array, fn1::toArray($data)) : fn1::toArray($data);
			else {
				$temp = fn1::jsonParse($data, true, []);
				if (!count($temp)) $temp = self::queryStringArray(fn1::toStrn($data, true));
				if (count($temp)) $temp_array = count($temp_array) ? fn1::arrayMerge($temp_array, $temp) : $temp;
			}
		}
		return count($temp_array) || $empty_object ? fn1::jsonCreate($temp_array) : "";
	}
	public function getOptions(){
		//default options
		$options = [
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_VERBOSE => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		];
		$max_redirects = (int) $this -> max_redirects;
		if ($max_redirects > 0) $options[CURLOPT_MAXREDIRS] = $max_redirects;
		$max_timeout = (int) $this -> max_timeout;
		if ($max_timeout > 0) $options[CURLOPT_TIMEOUT] = $max_timeout;

		//user defined options
		$curl_options = fn1::toArray($this -> curl_options);
		foreach ($curl_options as $key => $value) $options[$key] = $value;

		//required options
		$required = [
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => true,
			CURLINFO_HEADER_OUT => true,
			CURLOPT_CUSTOMREQUEST => $this -> getMethod(),
		];
		if (!$this -> follow_redirects) $required[CURLOPT_FOLLOWLOCATION] = false;
		foreach ($required as $key => $value) $options[$key] = $value;
		return $options;
	}
	public function getStatus(){
		return (int) $this -> status;
	}
	public function getResult(){
		return $this -> result;
	}
	public function getResponse(){
		return $this -> response;
	}
	public function getRequestHeaders(){
		return $this -> request_headers;
	}
	public function getRequestHeader($header){
		$headers = $this -> getRequestHeaders();
		$key = fn1::valKey($headers, $header, false, false);
		return $key && array_key_exists($key, $headers) ? $headers[$key] : null;
	}
	public function getResponseHeaders(){
		return $this -> response_headers;
	}
	public function getResponseHeader($header){
		$headers = $this -> getResponseHeaders();
		$key = fn1::valKey($headers, $header, false, false);
		return $key && array_key_exists($key, $headers) ? $headers[$key] : null;
	}
	public function fetch(){
		$this -> status = null;
		$this -> result = null;
		$this -> response_headers = null;
		$this -> request_headers = null;
		$this -> response = null;
		try {
			//request url
			if (($url = $this -> getUrl()) === false) return false;
			$query_params = fn1::toStrn($this -> getQuery(true, true), true);
			if (strlen($query_params)) $url .= "?" . $query_params;

			//request method
			$method = $this -> getMethod();

			//request headers - content type
			$request_headers = fn1::toArray($this -> request_headers);
			$content_type = null;
			$key = fn1::valKey($request_headers, "content-type", false, false);
			if ($key && array_key_exists($key, $request_headers)){
				unset($request_headers[$key]);
				$content_type = strlen($content_type = fn1::toStrn($request_headers[$key], true)) ? $content_type : null;
			}
			if (!$content_type && $this -> is_json && $method == "POST") $content_type = "application/json";
			if ($content_type) $request_headers["Content-Type"] = $content_type;

			//headers
			$headers = [];
			if (count($request_headers)) foreach ($request_headers as $key => $value) $headers[] = fn1::strbuild("%s: %s", $key, $value);

			//curl options
			$curl_options = $this -> getOptions();
			if ($method == "POST"){
				$curl_options[CURLOPT_POST] = true;
				$curl_options[CURLOPT_POSTFIELDS] = $this -> is_json ? $this -> getDataJSON() : $this -> getDataQuery();
			}

			//curl request loop
			$new_url = $url;
			$max_redirects = (int) $this -> max_redirects;
			$check_redirects = $max_redirects > 0;
			$temp_time = time();
			$this -> url_chain = [];
			do {
				//check max redirects
				if ($check_redirects && $max_redirects < 1) throw new \Exception(lg::str("REQUEST_MAX_REDIRECTS", (int) $this -> max_redirects));

				//init curl
				$curl = curl_init();

				//set url
				curl_setopt($curl, CURLOPT_URL, $new_url);
				$this -> url_chain[time() - $temp_time] = $new_url;

				//set request headers
				if (count($headers)) curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

				//set options
				if (count($curl_options)) curl_setopt_array($curl, $curl_options);

				//get response
				$result = curl_exec($curl);
				$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

				//redirects
				if (in_array($code, [301, 302, 303, 307])){
					preg_match('/Location:(.*?)\n/is', $result, $matches);
					$new_url = trim(array_pop($matches));
					curl_close($curl);
					$max_redirects --;
				}
				else {
					$status = $code;
					$header_sent = curl_getinfo($curl, CURLINFO_HEADER_OUT);
					$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
					$response_header = substr($result, 0, $header_size);
					$response_text = substr($result, $header_size);

					//set response
					$this -> status = $code;
					$this -> result = $result;
					$this -> response_headers = self::headerArray($response_header);
					$this -> request_headers = self::headerArray($header_sent);
					$this -> response = $response_text;

					//close curl
					curl_close($curl);
					$code = 0;
				}
			}
			while ($code);
			return $this -> success($this -> getResponse());
		}
		catch (\Exception $e){
			return $this -> error($e -> getMessage());
		}
	}
	public static function headerArray($header_text){
		$headers = [];
		$header_text = trim(str_replace("\r", "", $header_text));
		foreach (explode("\n", $header_text) as $i => $line){
			if ($i == 0) $headers["http_code"] = $line;
			else {
				list($key, $value) = explode(":", $line);
				$headers[trim($key)] = trim($value);
			}
		}
		return $headers;
	}
	public static function queryStringArray($query_string){
		$temp = fn1::toStrn($query_string, true);
		if (!strlen($temp)) return [];
		$temp = preg_replace('/^\?/s', "", $temp);
		parse_str($temp, $array);
		return isset($array) && count($array) ? $array : [];
	}
}


//Bitly shorten link helper
function bitly_shorten_link($link, $access_token="c7f772610c80b4ebf65044804e992b42e1bc8147", $api_endpoint="https://api-ssl.bitly.com/v3/shorten"){
	global $bitly_shorten_link_error;
	$request = new Request($api_endpoint, "GET", ["access_token" => $access_token, "longUrl" => $link]);
	if (!$request -> fetch()){
		$bitly_shorten_link_error = $request -> getErrorMessage();
		return false;
	}
	$result = $request -> getResponse();
	$response = fn1::jsonParse($result, true, []);
	$status_code = (int) fn1::propval($response, "status_code");
	if ($status_code !== 200){
		$status_text = fn1::toStrn(fn1::propval($response, "status_txt"), true);
		$bitly_shorten_link_error = strlen($status_text) ? $status_text : "Error in response: " . $result;
		return false;
	}
	$short_url = fn1::toStrn(fn1::propval($response, "data", "url"), true);
	if (!strlen($short_url)){
		$bitly_shorten_link_error = "Unable to get shortened link";
		return false;
	}
	return $short_url;
}
