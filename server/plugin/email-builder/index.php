<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190503
*	EMAIL SERVICE
*/

namespace Naicode\Server\Plugin;
require_once __DIR__ . "/../../funcs.php";
include_once __DIR__ . "/template.php";
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Plugin\EmailBuilder\Block;

class EmailBuilder {
	public $html = "";
	private $blocks = [];
	private $title = "Email Message";
	private $background_color = "#222";
	private $custom_style = "";
	public function __construct($title="Email Message", $background_color="#222", $custom_style=""){
		$this -> blocks = [];
		$this -> title = fn1::defaultStr($title, "Email Message", true, false);
		$this -> background_color = fn1::defaultStr($background_color, "#222", true, false);
		$this -> custom_style = fn1::strnobreak($custom_style);
		$this -> html = "";
	}
	public function pre($text){
		$html = fn1::curlsReplace(fn1::toStrx(EMAIL_TEMPLATE_PREVIEW_TEXT, true), ["content" => fn1::toStrn($text, true)]);
		$this -> blocks[] = $html;
		return $this;
	}
	public function container($custom_style=""){
		$block = new Block(true, $custom_style);
		$this -> blocks[] = $block;
		return $block;
	}
	public function fullBleed($custom_style="", $background_color="#709f2b", $color="#fff"){
		$block = new Block(true, $custom_style, true, $background_color, $color);
		$this -> blocks[] = $block;
		return $block;
	}
	public function build($compress=false){
		$items = [];
		if (is_array($this -> blocks)) foreach ($this -> blocks as $item) $items[] = is_object($item) && method_exists($item, "build") ? $item -> build() : (string) $item;
		$content = implode("", $items);
		$html = fn1::curlsReplace(fn1::toStrx(EMAIL_TEMPLATE_HTML, true), [
			"title" => $this -> title,
			"custom_style" => $this -> custom_style,
			"background_color" => $this -> background_color,
			"content" => $content,
		]);
		if ($compress) $html = fn1::minify($html);
		$this -> html = $html;
		return $html;
	}
}

namespace Naicode\Server\Plugin\EmailBuilder;
use Naicode\Server\Funcs as fn1;

class Block {
	private $contents = [];
	private $color = "#fff";
	private $container = false;
	private $full_bleed = false;
	private $custom_style = "";
	private $background_color = "#709f2b";
	public function __construct($container=false, $custom_style="", $full_bleed=false, $background_color="#709f2b", $color="#fff"){
		$this -> container = (bool) $container;
		$this -> full_bleed = (bool) $full_bleed;
		$this -> custom_style = fn1::strnobreak($custom_style);
		$this -> background_color = fn1::defaultStr($background_color, "#709f2b", true, false);
		$this -> color = fn1::defaultStr($color, "#fff", true, false);
	}
	public function html($content){
		$this -> contents[] = fn1::toStrn($content, true);
		return $this;
	}
	public function link($href, $text, $custom_style=""){
		$html = fn1::curlsReplace("<a class=\"a-link\" href=\"{{href}}\" target=\"_blank\" style=\"{{custom_style}}\">{{content}}</a>", [
			"href" => fn1::defaultStr($href, "#", true, false),
			"custom_style" => fn1::strnobreak($custom_style),
			"content" => fn1::defaultStr($text, "Link", true, false),
		]);
		$this -> contents[] = $html;
		return $this;
	}
	public function button($href, $text, $background_color="#222", $border_color="#000", $color="#fff", $custom_style=""){
		$html = fn1::curlsReplace(fn1::toStrx(EMAIL_TEMPLATE_BUTTON, true), [
			"href" => fn1::defaultStr($href, "#", true, false),
			"background_color" => fn1::defaultStr($background_color, "#222", true, false),
			"border_color" => fn1::defaultStr($border_color, "#000", true, false),
			"color" => fn1::defaultStr($color, "#fff", true, false),
			"custom_style" => fn1::strnobreak($custom_style),
			"content" => fn1::defaultStr($text, "Button", true, false),
		]);
		$this -> contents[] = $html;
		return $this;
	}
	public function buttonBlock($href, $text, $align="center", $background_color="#222", $border_color="#000", $color="#fff", $custom_style=""){
		$html = fn1::curlsReplace(fn1::toStrx(EMAIL_TEMPLATE_BUTTON_BLOCK, true), [
			"align" => fn1::defaultStr($align, "center", true, false),
			"background_color" => fn1::defaultStr($background_color, "#222", true, false),
			"href" => fn1::defaultStr($href, "#", true, false),
			"border_color" => fn1::defaultStr($border_color, "#000", true, false),
			"color" => fn1::defaultStr($color, "#fff", true, false),
			"custom_style" => fn1::strnobreak($custom_style),
			"content" => fn1::defaultStr($text, "Button", true, false),
		]);
		$this -> contents[] = $html;
		return $this;
	}
	public function p($content, $custom_style=""){
		$html = fn1::curlsReplace("<p style=\"margin: 0;{{custom_style}}\">{{content}}</p>", [
			"custom_style" => fn1::strnobreak($custom_style),
			"content" => fn1::toStrn($content, true),
		]);
		$this -> contents[] = $html;
		return $this;
	}
	public function h1($content, $color="#333333", $custom_style=""){
		$html = fn1::curlsReplace("<h1 style=\"margin: 0 0 10px 0; font-family: sans-serif; font-size: 25px; line-height: 30px; color: {{color}}; font-weight: normal;{{custom_style}}\">{{content}}</h1>", [
			"color" => fn1::defaultStr($color, "#333333", true, false),
			"custom_style" => fn1::strnobreak($custom_style),
			"content" => fn1::toStrn($content, true),
		]);
		$this -> contents[] = $html;
		return $this;
	}
	public function h2($content, $color="#333333", $custom_style=""){
		$html = fn1::curlsReplace("<h2 style=\"margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; line-height: 22px; color: {{color}}; font-weight: bold;{{custom_style}}\">{{content}}</h2>", [
			"color" => fn1::defaultStr($color, "#333333", true, false),
			"custom_style" => fn1::strnobreak($custom_style),
			"content" => fn1::toStrn($content, true),
		]);
		$this -> contents[] = $html;
		return $this;
	}
	public function listItems($content_array, $ordered=false, $custom_style="", $custom_style_item=""){
		if ($count = count($content_array = fn1::toArray($content_array))){
			$items = [];
			$last = $count - 1;
			for ($i = 0; $i < $count; $i ++){
				$class = $i == 0 ? "list-item-first" : ($i == $last ? "list-item-last" : "");
				$style = ($i == $last ? "margin: 0 0 0 30px;" : "margin: 0 0 10px 30px;") . fn1::strnobreak($custom_style_item);
				$items[] = "<li" . ($class == "" ? "" : " class=\"$class\"") . " style=\"$style\">" . fn1::toStrn($content_array[$i], true) . "</li>";
			}
			$tag = $ordered ? "ol" : "ul";
			$html = "<$tag style=\"padding: 0; margin: 0 0 10px 0; list-style-type: disc;" . fn1::strnobreak($custom_style) . "\">" . implode("", $items) . "</$tag>";
			$this -> contents[] = $html;
		}
		return $this;
	}
	public function image($src, $alt_text="Loading image...", $width="", $height="", $background_color="#dddddd", $color="#555555", $custom_style="", $gimg=false){
		$html = "<img src=\"{{src}}\" alt=\"{{alt_text}}\" width=\"{{width}}\" height=\"{{height}}\" border=\"0\" style=\"background: {{background_color}}; font-family: sans-serif; font-size: 15px; line-height: 15px; color: {{color}};{{custom_style}}\"" . ($gimg ? " class=\"g-img\"" : "") . "/>";
		$html = fn1::curlsReplace($html, [
			"src" => fn1::strnobreak($src),
			"alt_text" => fn1::defaultStr($alt_text, "Loading image...", true),
			"width" => is_numeric($width) ? $width : "",
			"height" => is_numeric($height) ? $height : "",
			"background_color" => fn1::defaultStr($background_color, "#dddddd", true, false),
			"color" => fn1::defaultStr($color, "#555555", true, false),
			"custom_style" => fn1::strnobreak($custom_style),
		]);
		$this -> contents[] = $html;
		return $this;
	}
	public function image_200($src, $alt_text="Loading image...", $background_color="#dddddd", $color="#555555", $custom_style=""){
		$custom_style = "width: 100%; max-width: 200px;" . fn1::strnobreak($custom_style);
		return $this -> image($src, $alt_text, "200", "", $background_color, $color, $custom_style);
	}
	public function image_200x50($src, $alt_text="Loading image...", $background_color="#dddddd", $color="#555555", $custom_style=""){
		$custom_style = "height: auto;" . fn1::strnobreak($custom_style);
		return $this -> image($src, $alt_text, "200", "50", $background_color, $color, $custom_style);
	}
	public function image_600($src, $alt_text="Loading image...", $background_color="#dddddd", $color="#555555", $custom_style=""){
		$custom_style = "width: 100%; max-width: 600px; height: auto;margin: auto; display: block;" . fn1::strnobreak($custom_style);
		return $this -> image($src, $alt_text, "600", "", $background_color, $color, $custom_style, true);
	}
	public function grid($custom_style="", $align=""){
		$grid = new Grid($custom_style, $align);
		$this -> contents[] = $grid;
		return $grid;
	}
	public function grid_auto($custom_style=""){
		return $this -> grid($custom_style);
	}
	public function grid_center($custom_style=""){
		return $this -> grid($custom_style, "center");
	}
	public function build(){
		$items = [];
		if (is_array($this -> contents)) foreach ($this -> contents as $item) $items[] = is_object($item) && method_exists($item, "build") ? $item -> build() : (string) $item;
		$content = implode("", $items);
		if ($this -> container){
			if ($this -> full_bleed){
				$content = fn1::curlsReplace(fn1::toStrx(EMAIL_TEMPLATE_CONTAINER_BLEED, true), [
					"background_color" => $this -> background_color,
					"color" => $this -> color,
					"content" => $content,
				]);
			}
			else $content = fn1::curlsReplace(fn1::toStrx(EMAIL_TEMPLATE_CONTAINER, true), ["content" => $content]);
		}
		return $content;
	}
}

class Grid {
	private $align;
	private $custom_style;
	private $contents = [];
	public function __construct($custom_style="", $align=""){
		$this -> align = fn1::strnobreak($align);
		$this -> custom_style = fn1::strnobreak($custom_style);
	}
	public function row(){
		$row = new Row();
		$this -> contents[] = $row;
		return $row;
	}
	public function build(){
		$style = "margin: auto;" . fn1::toStrx($this -> custom_style);
		$html = "<table role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" style=\"$style\"";
		if (!fn1::isEmpty($align = fn1::strnobreak($this -> align))) $align .= " align=\"$align\"";
		$items = [];
		if (is_array($this -> contents)) foreach ($this -> contents as $item) $items[] = is_object($item) && method_exists($item, "build") ? $item -> build() : (string) $item;
		$html .= ">" . implode("", $items) . "</table>";
		return $html;
	}
}

class Row {
	private $contents = [];
	public function __construct(){}
	public function column($custom_style="", $valign="", $width="", $height="", $aria_hidden=""){
		$column = new Column($custom_style, $valign, $width, $height, $aria_hidden);
		$this -> contents[] = $column;
		return $column;
	}
	public function col($custom_style="", $valign="", $width="", $height="", $aria_hidden=""){
		return $this -> column($custom_style, $valign, $width, $height, $aria_hidden);
	}
	public function col_white($custom_style=""){
		return $this -> col("background-color: #ffffff;" . fn1::strnobreak($custom_style));
	}
	public function col_pad_white($custom_style=""){
		return $this -> col("padding: 0 10px 40px 10px;background-color: #ffffff;" . fn1::strnobreak($custom_style));
	}
	public function col_pad($custom_style=""){
		return $this -> col("padding: 0 20px;" . fn1::strnobreak($custom_style));
	}
	public function col_pad_center($custom_style=""){
		return $this -> col("padding: 20px 0; text-align: center;" . fn1::strnobreak($custom_style));
	}
	public function col_text_15($custom_style=""){
		return $this -> col("padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;" . fn1::strnobreak($custom_style));
	}
	public function col_text_12($custom_style=""){
		return $this -> col("padding: 20px; font-family: sans-serif; font-size: 12px; line-height: 15px; text-align: center; color: #888888;" . fn1::strnobreak($custom_style));
	}
	public function col_50($custom_style=""){
		return $this -> col($custom_style, "top", "50%");
	}
	public function col_50_center($custom_style=""){
		return $this -> col("text-align: center; padding: 0 10px;" . fn1::strnobreak($custom_style));
	}
	public function col_50_text($custom_style=""){
		return $this -> col("text-align: left; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 10px 10px 0;" . fn1::strnobreak($custom_style));
	}
	public function col_spacer($custom_style=""){
		return $this -> col("font-size: 0px; line-height: 0px;" . fn1::strnobreak($custom_style), "", "", "40", "true");
	}
	public function build(){
		$items = [];
		if (is_array($this -> contents)) foreach ($this -> contents as $item) $items[] = is_object($item) && method_exists($item, "build") ? $item -> build() : (string) $item;
		return "<tr>" . implode("", $items) . "</tr>";
	}
}

class Column extends Block {
	private $attributes = [];
	public function __construct($custom_style="", $valign="", $width="", $height="", $aria_hidden=""){
		parent::__construct();
		$this -> attributes = [];
		if (!fn1::isEmpty($valign = fn1::strnobreak($valign))) $this -> attributes[] = "valign=\"$valign\"";
		if (is_numeric($width = fn1::toStrn($width, true))) $this -> attributes[] = "width=\"$width\"";
		if (is_numeric($height = fn1::toStrn($height, true))) $this -> attributes[] = "height=\"$height\"";
		if ($aria_hidden !== null){
			if (is_bool($aria_hidden)) $aria_hidden = $aria_hidden ? "true" : "false";
			if (!fn1::isEmpty($aria_hidden = fn1::strnobreak($aria_hidden))) $this -> attributes[] = "aria-hidden=\"$aria_hidden\"";
		}
		if (!fn1::isEmpty($custom_style = fn1::strnobreak($custom_style))) $this -> attributes[] = "style=\"$custom_style\"";
	}
	public function build(){
		$content = parent::build();
		$attributes = is_array($this -> attributes) && count($this -> attributes) ? implode(" ", $this -> attributes) : "";
		return "<td" . ($attributes == "" ? "" : " $attributes") . ">" . $content . "</td>";
	}
}
