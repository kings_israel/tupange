<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190503
*	AUDIT LOGS
*/

namespace Naicode\Server\Plugin;
require_once __DIR__ . "/../lang.php";
require_once __DIR__ . "/../funcs.php";
require_once __DIR__ . "/../globals.php";
require_once __DIR__ . "/../database.php";
use Naicode\Server\Lang as lg;
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\Main;
use Naicode\Server\Database as db;

#defaults
if (!defined("NS_TABLE_AUDIT")) define("NS_TABLE_AUDIT", "ns_audit");
lg::config([
	"AUDIT_ERROR_NO_USER" => ["eng" => "Audit log user is not defined!"],
	"AUDIT_ERROR_NO_ACTION" => ["eng" => "Audit log action is not defined!"],
]);

class Audit extends Main {
	const SYSTEM_USER = "SYSTEM";
	public $id = null;
	public $user = null;
	public $action = null;
	public $identifier = null;
	public $edit_from = null;
	public $edit_to = null;
	public $notes = null;
	public $timestamp = null;
	public function __construct($user=null, $action=null, $identifier=null, $edit_from=null, $edit_to=null, $notes=null, $timestamp=null){
		parent::__construct();
		$this -> user = fn1::toStrn($user, true);
		$this -> action = fn1::toStr($action, true, true);
		$this -> identifier = fn1::toStr($identifier, true, true);
		$this -> edit_from = fn1::toStr($edit_from, true, true);
		$this -> edit_to = fn1::toStr($edit_to, true, true);
		$this -> notes = fn1::toStr($notes, true, true);
		$this -> timestamp = fn1::toStrn($timestamp, true);
	}
	public function save($user=null, $action=null, $identifier=null, $edit_from=null, $edit_to=null, $notes=null, $timestamp=null){
		$id = !fn1::isEmpty($id = fn1::toStrn($this -> id, true)) ? $id : "";
		$user = !fn1::isEmpty($user = fn1::toStrn($user, true)) ? $user : (!fn1::isEmpty($user = fn1::toStrn($this -> user, true)) ? $user : "");
		$action = !fn1::isEmpty($action = fn1::toStr($action, true, true)) ? $action : (!fn1::isEmpty($action = fn1::toStr($this -> action, true, true)) ? $action : "");
		$identifier = !fn1::isEmpty($identifier = fn1::toStr($identifier, true, true)) ? $identifier : (!fn1::isEmpty($identifier = fn1::toStr($this -> identifier, true, true)) ? $identifier : "");
		$edit_from = !fn1::isEmpty($edit_from = fn1::toStr($edit_from, true, true)) ? $edit_from : (!fn1::isEmpty($edit_from = fn1::toStr($this -> edit_from, true, true)) ? $edit_from : "");
		$edit_to = !fn1::isEmpty($edit_to = fn1::toStr($edit_to, true, true)) ? $edit_to : (!fn1::isEmpty($edit_to = fn1::toStr($this -> edit_to, true, true)) ? $edit_to : "");
		$notes = !fn1::isEmpty($notes = fn1::toStr($notes, true, true)) ? $notes : (!fn1::isEmpty($notes = fn1::toStr($this -> notes, true, true)) ? $notes : "");
		$timestamp = !fn1::isEmpty($timestamp = fn1::toStrn($timestamp, true)) ? $timestamp : (!fn1::isEmpty($timestamp = fn1::toStrn($this -> timestamp, true)) ? $timestamp : "");
		if ($user == "") return $this -> error(lg::get("AUDIT_ERROR_NO_USER"));
		if ($action == "") return $this -> error(lg::get("AUDIT_ERROR_NO_ACTION"));
		if ($timestamp == "") $timestamp = fn1::now();
		$data = [
			"id" => $id,
			"user" => $user,
			"action" => $action,
			"identifier" => $identifier,
			"edit_from" => $edit_from,
			"edit_to" => $edit_to,
			"notes" => $notes,
			"timestamp" => $timestamp,
		];
		try {
			$db = new db();
			if (fn1::isEmpty($id)){
				if (($id = $db -> createToken(NS_TABLE_AUDIT, "id")) === false){
					$error = $db -> getErrorMessage(); $db -> close();
					throw new \Exception($error, self::STOP_CRITICAL);
				}
				$data["id"] = $id;
				if (!$db -> insert(NS_TABLE_AUDIT, $data)){
					$error = $db -> getErrorMessage(); $db -> close();
					throw new \Exception($error, self::STOP_CRITICAL);
				}
			}
			elseif (!$db -> update(NS_TABLE_AUDIT, $data, "`id` = ?", [$id])){
				$error = $db -> getErrorMessage(); $db -> close();
				throw new \Exception($error, self::STOP_CRITICAL);
			}
			self::$last_value = $db -> affected_rows;
			$db -> close();
			return $this -> success($data);
		}
		catch (\Exception $e){
			return $this -> error($e -> getMessage());
		}
	}
	public function id($id){
		if (fn1::isEmpty($id = fn1::toStrn($id, true))) return $this;
		$this -> id = $id;
		return $this;
	}
	public function set($key, $value){
		if ($key == "id") $this -> id = fn1::toStrn($value, true);
		if ($key == "user") $this -> user = fn1::toStr($value, true, true);
		if ($key == "action") $this -> action = fn1::toStr($value, true, true);
		if ($key == "identifier") $this -> identifier = fn1::toStr($value, true, true);
		if ($key == "edit_from") $this -> edit_from = fn1::toStr($value, true, true);
		if ($key == "edit_to") $this -> edit_to = fn1::toStr($value, true, true);
		if ($key == "notes") $this -> notes = fn1::toStr($value, true, true);
		if ($key == "timestamp") $this -> timestamp = fn1::toStrn($value, true);
		return $this;
	}
	public function setData($data){
		if (fn1::isEmpty($data = fn1::toArray($data))) return $this;
		if (array_key_exists("id", $data)) $this -> id = fn1::toStrn($data["id"], true);
		if (array_key_exists("user", $data)) $this -> user = fn1::toStrn($data["user"], true);
		if (array_key_exists("action", $data)) $this -> action = fn1::toStr($data["action"], true, true);
		if (array_key_exists("identifier", $data)) $this -> identifier = fn1::toStr($data["identifier"], true, true);
		if (array_key_exists("edit_from", $data)) $this -> edit_from = fn1::toStr($data["edit_from"], true, true);
		if (array_key_exists("edit_to", $data)) $this -> edit_to = fn1::toStr($data["edit_to"], true, true);
		if (array_key_exists("notes", $data)) $this -> notes = fn1::toStr($data["notes"], true, true);
		if (array_key_exists("timestamp", $data)) $this -> timestamp = fn1::toStrn($data["timestamp"], true);
		return $this;
	}
	public static function log($user=null, $action=null, $identifier=null, $edit_from=null, $edit_to=null, $notes=null, $timestamp=null, $id=null){
		$audit = new static($user, $action, $identifier, $edit_from, $edit_to, $notes, $timestamp);
		$audit -> id($id);
		if (!$audit -> save()) return self::__error($audit -> getErrorMessage());
		self::$last_error = self::$last_value;
		return self::__success($audit -> getSuccessMessage());
	}
	public static function syslog($action=null, $identifier=null, $edit_from=null, $edit_to=null, $notes=null, $timestamp=null, $id=null){
		return self::log(self::SYSTEM_USER, $action, $identifier, $edit_from, $edit_to, $notes, $timestamp, $id);
	}
	public static function fetch($id=null, $user=null, $limit=1000, $offset=0, $desc=true, $row_index=false){
		try {
			$limit = fn1::toNum($limit);
			$offset = fn1::toNum($offset);
			$bind_query = "";
			$bind_values = [];
			if (!fn1::isEmpty($id = fn1::toStrn($id, true))){
				$bind_query .= "WHERE `id` = ? ";
				$bind_values[] = $id;
			}
			if (!fn1::isEmpty($user = fn1::toStrn($user, true))){
				$bind_query .= ($bind_query == "" ? "WHERE" : "AND") . " `user` = ? ";
				$bind_values[] = $user;
			}
			$bind_query .= "ORDER BY `_index` " . ($desc ? "DESC" : "ASC");
			if ($limit > 0){
				if ($offset > 0){
					$bind_query .= " LIMIT ?,?";
					$bind_values = array_merge($bind_values, [$offset, $limit]);
				}
				else {
					$bind_query .= " LIMIT ?";
					$bind_values = array_merge($bind_values, [$limit]);
				}
			}
			$db = new db();
			if (($result = $db -> select(NS_TABLE_AUDIT, null, $bind_query, $bind_values)) === false){
				$error = $db -> getErrorMessage();
				$db -> close();
				throw new \Exception($error, self::STOP_CRITICAL);
			}
			$items = [];
			while ($row = (array) $result -> fetch_array(MYSQLI_ASSOC)){
				if (!$row_index) unset($row["_index"]);
				$items[] = $row;
			}
			$db -> close();
			return self::__success($items, $items);
		}
		catch (\Exception $e){
			return self::__error($e -> getMessage());
		}
	}
	public static function fetchLog($id=null, $user=null, $row_index=false){
		if (($items = self::fetch($id, $user, 1, 0, true, $row_index)) === false) return false;
		if (!fn1::isEmpty($items = fn1::trimVal(fn1::toArray($items)))) return $items[0];
		return null;
	}
	public static function delete($id=null, $user=null, $limit=0, $offset=0, $desc=true){
		try {
			$limit = fn1::toNum($limit);
			$offset = fn1::toNum($offset);
			$bind_query = "";
			$bind_values = [];
			if (!fn1::isEmpty($id = fn1::toStrn($id, true))){
				$bind_query .= "WHERE `id` = ? ";
				$bind_values[] = $id;
			}
			if (!fn1::isEmpty($user = fn1::toStrn($user, true))){
				$bind_query .= ($bind_query == "" ? "WHERE" : "AND") . " `user` = ? ";
				$bind_values[] = $user;
			}
			$bind_query .= "ORDER BY `_index` " . ($desc ? "DESC" : "ASC");
			if ($limit > 0){
				if ($offset > 0){
					$bind_query .= " LIMIT ?,?";
					$bind_values = array_merge($bind_values, [$offset, $limit]);
				}
				else {
					$bind_query .= " LIMIT ?";
					$bind_values = array_merge($bind_values, [$limit]);
				}
			}
			$db = new db();
			if (($result = $db -> delete(NS_TABLE_AUDIT, "id", $bind_query, $bind_values)) === false){
				$error = $db -> getErrorMessage();
				$db -> close();
				throw new \Exception($error, self::STOP_CRITICAL);
			}
			$affected_rows = $db -> affected_rows;
			$db -> close();
			return self::__success($affected_rows);
		}
		catch (\Exception $e){
			return self::__error($e -> getMessage());
		}
	}
}


/*	NS_TABLE_AUDIT TABLE CREATE SQL:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
CREATE TABLE `ns_audit` (
	`_index` int(20) NOT NULL,
	`id` varchar(200) NOT NULL,
	`user` varchar(200) NOT NULL,
	`action` varchar(2000) NOT NULL,
	`identifier` varchar(2000) DEFAULT NULL,
	`edit_from` text,
	`edit_to` text,
	`notes` text,
	`timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `ns_audit` ADD PRIMARY KEY (`_index`), ADD UNIQUE KEY `id` (`id`);
ALTER TABLE `ns_audit` MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT;
COMMIT;
*/
