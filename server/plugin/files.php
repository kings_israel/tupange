<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190601
*	FILES
*/

namespace Naicode\Server\Plugin;
require_once __DIR__ . "/../lang.php";
require_once __DIR__ . "/../funcs.php";
require_once __DIR__ . "/../globals.php";
use Naicode\Server\Lang as lg;
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\Main;


if (!defined("NS_SITE_DIR")) define("NS_SITE_DIR", NS_SERVER_ROOT . "/");
if (!defined("NS_SITE")) define("NS_SITE", "http://localhost/");

if (!defined("NS_UPLOADS_FOLDER")) define("NS_UPLOADS_FOLDER", NS_SITE_DIR . "/uploads");
if (!defined("NS_UPLOADS_TEMPLATE")) define("NS_UPLOADS_TEMPLATE", "{{uploads}}/{{random_string}}.{{ext}}");
if (!defined("NS_UPLOADS_MAX_SIZE")) define("NS_UPLOADS_MAX_SIZE", 5);

lg::config([
	"FILES_SESSION_ERROR" => ["eng" => "Invalid session"],
	"FILES_NO_NAME" => ["eng" => "Uploads name has not been set"],
	"FILES_NO_FOLDER" => ["eng" => "Uploads folder has not been set"],
	"FILES_NO_FILES" => ["eng" => "No files in request"],
	"FILES_PATHINFO_ERROR" => ["eng" => "Failed to get path details for \"%s\""],
	"FILES_EXT_ERROR" => ["eng" => "File extension \"%s\" is not accepted for \"%s\""],
	"FILES_MIME_ERROR" => ["eng" => "File mime type \"%s\" is not accepted for \"%s\""],
	"FILES_SIZE_ERROR" => ["eng" => "File size \"%sMB\" is larger than the allowed size of \"%sMB\" for \"%s\""],
	"FILES_UPLOAD_ERR_NO_FILE" => ["eng" => "Upload Error: No file sent for \"%s\""],
	"FILES_UPLOAD_ERR_SIZE" => ["eng" => "Upload Error: File exceeded size limit for \"%s\""],
	"FILES_UPLOAD_ERR_UNKNOWN" => ["eng" => "Upload Error: Unknown upload error for \"%s\""],
	"FILES_UPLOAD_DIR_ERROR" => ["eng" => "Failed to get folder for \"%s\""],
	"FILES_UPLOAD_MOVE_ERROR" => ["eng" => "Failed to upload file \"%s\""],
	"FILES_INVALID_BASE64" => ["eng" => "Invalid base64 data"],
	"FILES_PATH_ERROR" => ["eng" => "Invalid file path \"%s\""],
	"FILE_ERROR_IMAGE_SUPPORT" => ["eng" => "File extension \"%s\" is not a supported image file"],
	"FILE_ERROR_IMAGE_RESIZE" => ["eng" => "Image resize error"],
]);

class Files extends Main {
	const SIZE_KB = 1024;
	const SIZE_MB = 1048576;
	const SIZE_GB = 1073741824;
	const SIZE_TB = 1099511627776;
	public $name = "files";
	public $uploads_folder = NS_UPLOADS_FOLDER;
	public $uploads_template = NS_UPLOADS_TEMPLATE;
	public $allowed_extensions = [];
	public $allowed_mime_types = [];
	public $uploads_max_size = NS_UPLOADS_MAX_SIZE;
	public $uploaded = [];
	public function __construct($name="files"){
		parent::__construct();
		$this -> name = fn1::toStrn($name, true);
	}
	public function setAllowedExtensions($extensions){
		$allowed_extensions = fn1::toArray($this -> allowed_extensions);
		$extensions = fn1::toArray($extensions);
		$allowed_extensions = array_unique(fn1::trimVal(fn1::arraysMerge($allowed_extensions, $extensions)));
		$this -> allowed_extensions = $allowed_extensions;
		return $this;
	}
	public function setAllowedMimeTypes($mime_types){
		$allowed_mime_types = fn1::toArray($this -> allowed_mime_types);
		$mime_types = fn1::toArray($mime_types);
		$allowed_mime_types = array_unique(fn1::trimVal(fn1::arraysMerge($allowed_mime_types, $mime_types)));
		$this -> allowed_mime_types = $allowed_mime_types;
		return $this;
	}
	public function setMaxLimit($limit){
		$this -> uploads_max_size = fn1::toNum($limit);
		return $this;
	}
	public function setTemplate($template){
		$this -> uploads_template = fn1::toStrx($template, true);
		return $this;
	}
	public function setUploadsFolder($dir){
		$this -> uploads_folder = fn1::toStrx($dir, true);
		return $this;
	}
	private function getName(){
		$name = fn1::toStrn($this -> name, true);
		if (fn1::isEmpty($name)) $name = "files";
		return $name;
	}
	private function getUploadsFolder(){
		$uploads_folder = fn1::toStrx($this -> uploads_folder, true);
		if (fn1::isEmpty($uploads_folder)) $uploads_folder = NS_UPLOADS_FOLDER;
		$uploads_folder = str_replace("\\", "/", $uploads_folder);
		$uploads_folder = preg_replace('/\/\s*$/', "", $uploads_folder);
		return $uploads_folder;
	}
	private function getUploadTemplate(){
		$uploads_template = fn1::toStrx($this -> uploads_template, true);
		if (fn1::isEmpty($uploads_template)) $uploads_template = NS_UPLOADS_TEMPLATE;
		return $uploads_template;
	}
	private function getUploadPath($ext="", $name=""){
		$uploads_folder = $this -> getUploadsFolder();
		$uploads_template = $this -> getUploadTemplate();
		if ($name == "") $name = "file_" . fn1::randomString(10);
		$path = $uploads_template;
		$path = str_replace("{{uploads}}", $uploads_folder, $path);
		$path = str_replace("{{random_string}}", fn1::randomString(10), $path);
		$path = str_replace("{{random_number}}", rand(1000000,9999999), $path);
		$path = str_replace("{{ext}}", $ext, $path);
		$path = str_replace("{{name}}", $name, $path);
		if (!$path_info = fn1::pathInfo($path)) return $this -> error(lg::str("FILES_PATHINFO_ERROR", $path));
		$path_dir = $path_info["dir"];
		if (!fn1::dirCreate($path_dir, false)) return $this -> error(lg::str("FILES_UPLOAD_DIR_ERROR", $path_dir));
		return $path;
	}
	private function getSRC($path){
		$uploads_folder = $this -> getUploadsFolder();
		return str_replace($uploads_folder . "/", "", $path);
	}
	private function validateFile($file_name, $size_bytes, $ext, $error=null){
		$allowed_extensions = fn1::toArray($this -> allowed_extensions);
		$allowed_mime_types = fn1::toArray($this -> allowed_mime_types);
		$uploads_max_size = fn1::toNum($this -> uploads_max_size);
		$size_mb = round(fn1::toNum($size_bytes) / self::SIZE_MB, 2);
		if ($uploads_max_size > 0 && $size_mb > $uploads_max_size) return $this -> error(lg::str("FILES_SIZE_ERROR", $size_mb, $uploads_max_size, $file_name));
		if (!fn1::isEmpty($allowed_extensions)){
			if (!fn1::inArray($ext, $allowed_extensions, false)) return $this -> error(lg::str("FILES_EXT_ERROR", $ext, $file_name));
		}
		if (!fn1::isEmpty($allowed_mime_types)){
			$mime = fn1::extMimeType($ext);
			if (!fn1::inArray($mime, $allowed_mime_types, false)) return $this -> error(lg::str("FILES_MIME_ERROR", $mime, $file_name));
		}
		if ($error !== null){
			switch ($error){
				case UPLOAD_ERR_OK:
					break;
				case UPLOAD_ERR_NO_FILE:
					return $this -> error(lg::str("FILES_UPLOAD_ERR_NO_FILE", $file_name));
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					return $this -> error(lg::str("FILES_UPLOAD_ERR_SIZE", $file_name));
				default:
					return $this -> error(lg::str("FILES_UPLOAD_ERR_UNKNOWN", $file_name));
			}
		}
		return true;
	}
	public function fileUpload(){
		$this -> uploaded = [];
		$name = $this -> getName();
		if (!isset($_FILES[$name])) return $this -> success($this -> uploaded);
		$uploads = [];
		$file = $_FILES[$name];
		if (!$path_info = fn1::pathInfo(($file_name = $file["name"]))) return $this -> error(lg::str("FILES_PATHINFO_ERROR", $file_name));
		if (!$this -> validateFile($file_name, fn1::toNum($file["size"]), ($ext = $path_info["ext"]), (isset($file["error"]) ? $file["error"] : null))) return false;
		$uploads[] = [
			"filename" => $path_info["filename"],
			"name" => $path_info["name"],
			"tmp" => $file["tmp_name"],
			"ext" => $ext,
		];
		foreach($uploads as $upload){
			if (!$path = $this -> getUploadPath($upload["ext"], $upload["name"])) return false;
			if (!@move_uploaded_file($upload["tmp"], $path)) return $this -> error(lg::str("FILES_UPLOAD_MOVE_ERROR", $upload["filename"]));
			$upload["path"] = $path;
			$upload["src"] = $this -> getSRC($path);
			$this -> uploaded[] = $upload;
		}
		return $this -> success($this -> uploaded);
	}
	public function filesUpload(){
		$this -> uploaded = [];
		$name = $this -> getName();
		if (!isset($_FILES[$name])) return $this -> success($this -> uploaded);
		$uploads = [];
		foreach ($_FILES[$name] as $file){
			if (!$path_info = fn1::pathInfo(($file_name = $file["name"]))) return $this -> error(lg::str("FILES_PATHINFO_ERROR", $file_name));
			if (!$this -> validateFile($file_name, fn1::toNum($file["size"]), ($ext = $path_info["ext"]), (isset($file["error"]) ? $file["error"] : null))) return false;
			$uploads[] = [
				"filename" => $path_info["filename"],
				"name" => $path_info["name"],
				"tmp" => $file["tmp_name"],
				"ext" => $ext,
			];
		}
		foreach($uploads as $upload){
			if (!$path = $this -> getUploadPath($upload["ext"], $upload["name"])) return false;
			if (!@move_uploaded_file($upload["tmp"], $path)) return $this -> error(lg::str("FILES_UPLOAD_MOVE_ERROR", $upload["filename"]));
			$upload["path"] = $path;
			$upload["src"] = $this -> getSRC($path);
			$this -> uploaded[] = $upload;
		}
		return $this -> success($this -> uploaded);
	}
	public function base64Upload($base64_data, $filename=null){
		$data = explode(",", $base64_data);
		if (count($data) <= 1) return $this -> error(lg::get("FILES_INVALID_BASE64"));
		$ext = fn1::base64Ext($base64_data);
		$name = "file_" . fn1::randomString(10);
		if ($filename !== null && $path_info = fn1::pathInfo($filename)) $name = $path_info["name"];
		else $filename = "$name.$ext";
		$data = base64_decode($data[1]);
		if (!$this -> validateFile($filename, strlen($data), $ext)) return false;
		if (!$path = $this -> getUploadPath($ext, $name)) return false;
		$fo = fopen($path, "wb");
		fwrite($fo, $data);
		fclose($fo);
		$this -> uploaded[] = [
			"path" => $path,
			"src" => $this -> getSRC($path),
			"filename" => $filename,
			"name" => $name,
			"tmp" => "",
			"ext" => $ext,
		];
		return $this -> success($this -> uploaded);
	}
	public function imagesUpload($max_width=null){
		$this -> allowed_extensions = ["gif", "jpeg", "jpg", "png"];
		$this -> uploaded = [];
		$name = $this -> getName();
		if (!isset($_FILES[$name])) return $this -> success($this -> uploaded);
		$uploads = [];
		foreach ($_FILES[$name] as $file){
			if (!$path_info = fn1::pathInfo(($file_name = $file["name"]))) return $this -> error(lg::str("FILES_PATHINFO_ERROR", $file_name));
			if (!$this -> validateFile($file_name, fn1::toNum($file["size"]), ($ext = $path_info["ext"]), (isset($file["error"]) ? $file["error"] : null))) return false;
			$uploads[] = [
				"filename" => $path_info["filename"],
				"name" => $path_info["name"],
				"tmp" => $file["tmp_name"],
				"ext" => $ext,
			];
		}
		foreach($uploads as $upload){
			if (!$path = $this -> getUploadPath($upload["ext"], $upload["name"])) return false;
			if (!@move_uploaded_file($upload["tmp"], $path)) return $this -> error(lg::str("FILES_UPLOAD_MOVE_ERROR", $upload["filename"]));
			if ($max_width !== null && !$this -> resizeImage($path, $max_width)) return false;
			$upload["path"] = $path;
			$upload["src"] = $this -> getSRC($path);
			$this -> uploaded[] = $upload;
		}
		return $this -> success($this -> uploaded);
	}
	public function imageUploadBase64($base64_data, $filename=null, $max_width=null){
		$this -> allowed_extensions = ["gif", "jpeg", "jpg", "png"];
		$data = explode(",", $base64_data);
		if (count($data) <= 1) return $this -> error(lg::get("FILES_INVALID_BASE64"));
		$ext = fn1::base64Ext($base64_data);
		$name = "file_" . fn1::randomString(10);
		if ($filename !== null && $path_info = fn1::pathInfo($filename)) $name = $path_info["name"];
		else $filename = "$name.$ext";
		$data = base64_decode($data[1]);
		if (!$this -> validateFile($filename, strlen($data), $ext)) return false;
		if (!$path = $this -> getUploadPath($ext, $name)) return false;
		$fo = fopen($path, "wb");
		fwrite($fo, $data);
		fclose($fo);
		if ($max_width !== null && !$this -> resizeImage($path, $max_width)) return false;
		$this -> uploaded[] = [
			"path" => $path,
			"src" => $this -> getSRC($path),
			"filename" => $filename,
			"name" => $name,
			"tmp" => "",
			"ext" => $ext,
		];
		return $this -> success($this -> uploaded);
	}
	public function resizeImage($path, $new_width){
		if (!(fn1::isFile($path) && $path_info = fn1::pathInfo($path))) return $this -> error(lg::str("FILES_PATH_ERROR", $path));
		$image = fn1::getImage($path, null, null, null, $new_width);
		if (!$image) return $this -> error(lg::str("FILE_ERROR_IMAGE_RESIZE"));
		if (file_exists($path) && is_file($path)) unlink($path);
		imagejpeg($image, $path);
		imagedestroy($image);
		return true;
	}
}
