<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190510
*	USER
*/

namespace Naicode\Server\Plugin;
require_once __DIR__ . "/../lang.php";
require_once __DIR__ . "/../funcs.php";
require_once __DIR__ . "/../globals.php";
require_once __DIR__ . "/../database.php";
use Naicode\Server\Lang as lg;
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\Main;
use Naicode\Server\Mailer;
use Naicode\Server\Database as db;
use Naicode\Server\Plugin\Audit as audit;

if (!defined("NS_DOMAIN")) define("NS_DOMAIN", "example.com");
if (!defined("NS_SITE")) define("NS_SITE", "http://localhost/server");
if (!defined("NS_TABLE_USERS")) define("NS_TABLE_USERS", "ns_users");
if (!defined("NS_USER_PASSWORD_HASH")) define("NS_USER_PASSWORD_HASH", "mE9r1Gbl14SWen4K4tDvSrLeSJc8hPhuR3SkUByvDRzlvHb");
if (!defined("NS_USER_PASSWORD_MIN")) define("NS_USER_PASSWORD_MIN", 5);
if (!defined("NS_USER_USERLEVEL_MIN")) define("NS_USER_USERLEVEL_MIN", 1);
if (!defined("NS_USER_USERLEVEL_ADMIN")) define("NS_USER_USERLEVEL_ADMIN", 5);
if (!defined("NS_USER_EMAIL_FROM")) define("NS_USER_EMAIL_FROM", "info@" . NS_DOMAIN);
if (!defined("NS_USER_EMAIL_FROM_NAMES")) define("NS_USER_EMAIL_FROM_NAMES", "Accounts");
if (!defined("NS_USER_VERIFY_SUBJECT")) define("NS_USER_VERIFY_SUBJECT", "Verify Email");
if (!defined("NS_USER_VERIFY_HEADING")) define("NS_USER_VERIFY_HEADING", "Welcome {{names}}!");
if (!defined("NS_USER_VERIFY_MESSAGE")) define("NS_USER_VERIFY_MESSAGE", "Your account has been created successfully! Click the button below to verify your email address.");
if (!defined("NS_USER_VERIFY_LINK_TEXT")) define("NS_USER_VERIFY_LINK_TEXT", "VERIFY");
if (!defined("NS_USER_VERIFY_LINK")) define("NS_USER_VERIFY_LINK", NS_SITE . "/auth/verify/{{code}}");
if (!defined("NS_USER_RESET_SUBJECT")) define("NS_USER_RESET_SUBJECT", "Recover Password");
if (!defined("NS_USER_RESET_HEADING")) define("NS_USER_RESET_HEADING", "Hello {{names}}!");
if (!defined("NS_USER_RESET_MESSAGE")) define("NS_USER_RESET_MESSAGE", "A request was made to recover the password for your account. Click on the button below to set up a new password. If this was not requested by you, let us know by replying to this message.");
if (!defined("NS_USER_RESET_LINK_TEXT")) define("NS_USER_RESET_LINK_TEXT", "RESET PASSWORD");
if (!defined("NS_USER_RESET_LINK")) define("NS_USER_RESET_LINK", NS_SITE . "/auth/reset/{{code}}");
if (!defined("NS_MAIL_FOOTER_TEMPLATE")) define("NS_MAIL_FOOTER_TEMPLATE", 'Naicode<br><span class="unstyle-auto-detected-links">123 Fake Street, Nairobi, Kenya<br>(123) 456-7890</span>');
if (!defined("NS_MAIL_UNSUBSCRIBE_TEMPLATE")) define("NS_MAIL_UNSUBSCRIBE_TEMPLATE", '<br><br><unsubscribe style="color: #888888; text-decoration: underline;">unsubscribe</unsubscribe>');
if (!defined("NS_MAIL_TEMPLATE_LOGO")) define("NS_MAIL_TEMPLATE_LOGO", NS_SITE . '/assets/img/logo.png');

lg::config([
	"AUTH_USER_ERROR_PROVIDER" => ["eng" => "Invalid account provider \"%s\""],
	"AUTH_USER_ERROR_PROVIDER_EMAIL" => ["eng" => "Email provider not supported"],
	"AUTH_USER_ERROR_PASSWORD" => ["eng" => "Password must have " . NS_USER_PASSWORD_MIN . " or more characters"],
	"AUTH_USER_ERROR_PASSWORD_MATCH" => ["eng" => "Passwords do not match!"],
	"AUTH_USER_ERROR_IDENTIFIER" => ["eng" => "Invalid User Account ID"],
	"AUTH_USER_ERROR_NAMES" => ["eng" => "Please provide valid names"],
	"AUTH_USER_ERROR_EMAIL" => ["eng" => "Invalid email address \"%s\""],
	"AUTH_USER_ERROR_EMAIL_EXISTS" => ["eng" => "Email address \"%s\" is already in use"],
	"AUTH_USER_ERROR_IDENTIFIER_EXIST" => ["eng" => "User Account ID \"%s\" is already in use"],
	"AUTH_USER_ERROR_USERNAME_SHORT" => ["eng" => "Username \"%s\" is too short"],
	"AUTH_USER_ERROR_USERNAME_RESERVED" => ["eng" => "Username \"%s\" is not available"],
	"AUTH_USER_ERROR_USERNAME_EXIST" => ["eng" => "Username \"%s\" is already in use"],
	"AUDIT_ACTION_USER_CREATE" => ["eng" => "Create Account"],
	"AUTH_USER_ERROR_ACCOUNT_NOT_FOUND" => ["eng" => "User account \"%s\" does not extst"],
	"AUDIT_ACTION_USER_UPDATE" => ["eng" => "Update Account"],
	"AUTH_USER_ERROR_PERMIT_USERLEVEL" => ["eng" => "Userlevel \"%s\" is not allowed for such actions"],
	"AUTH_USER_ERROR_PERMIT_SESSION" => ["eng" => "Action requires a valid session"],
	"AUTH_USER_VERIFY_CODE" => ["eng" => "Invalid email verification code"],
	"AUTH_USER_RESET_CODE" => ["eng" => "Invalid password reset code"],
	"AUTH_USER_VERIFY_INVALID" => ["eng" => "Invalid or expired verification code"],
	"AUTH_USER_RESET_INVALID" => ["eng" => "Invalid or expired password reset code"],
	"AUTH_USER_ERROR_CREDENTIALS" => ["eng" => "Invalid login credentials"],
	"AUTH_USER_ERROR_LOGIN_USERNAME" => ["eng" => "Please enter valid login username or email"],
	"AUTH_USER_ERROR_LOGIN_PASSWORD" => ["eng" => "Please enter valid login password"],
	"AUTH_USER_ERROR_IDENTIFIER_NOT_FOUND" => ["eng" => "Provider \"%s\" Account ID \"%s\" does not exist"],
	"AUTH_USER_ERROR_UID" => ["eng" => "Invalid User ID"],
	"AUTH_USER_ERROR_UID_NOT_FOUND" => ["eng" => "Account User ID \"%s\" does not exist"],
	"AUTH_USER_ERROR_TOKEN" => ["eng" => "Invalid session token"],
	"AUTH_USER_ERROR_TOKEN_NOT_FOUND" => ["eng" => "Invalid or expired session token"],
]);

//$_SESSION["uid"]
//$_SESSION["userlevel"]
class User extends Main {
	const PROVIDERS = ["email", "twitter", "facebook", "google"];
	public $uid = "";
	public $status = "";
	public $token = "";
	public $token_ip = "";
	public $token_useragent = "";
	public $token_fingerprint = "";
	public $provider = "email";
	public $userlevel = NS_USER_USERLEVEL_MIN;
	public $identifier = "";
	public $website = "";
	public $profile_url = "";
	public $photo_url = "";
	public $display_name = "";
	public $description = "";
	public $first_name = "";
	public $last_name = "";
	public $gender = "";
	public $language = "";
	public $age = "";
	public $birth_day = "";
	public $birth_month = "";
	public $birth_year = "";
	public $username = "";
	public $email = "";
	public $email_verified = null;
	public $verification_code = null;
	public $reset_code = null;
	public $phone = "";
	public $address = "";
	public $country = "";
	public $region = "";
	public $city = "";
	public $zip = "";
	public $other = "";
	public $timestamp = "";
	public function __construct($data=null){
		parent::__construct();
		if (!fn1::isEmpty($data = fn1::toArray($data))) $this -> set($data);
	}
	public function set($data=null){
		if (fn1::isEmpty($data = fn1::trimVal(fn1::toArray($data)))) return $this;
		if (array_key_exists("uid", $data)) $this -> uid = fn1::defaultStr($data["uid"], "", true, false);
		if (array_key_exists("status", $data)) $this -> status = fn1::defaultStr($data["status"], "", true, false);
		if (array_key_exists("token", $data)) $this -> token = fn1::defaultStr($data["token"], "", true, false);
		if (array_key_exists("token_ip", $data)) $this -> token_ip = fn1::defaultStr($data["token_ip"], "", true, false);
		if (array_key_exists("token_useragent", $data)) $this -> token_useragent = fn1::defaultStr($data["token_useragent"], "", true, false);
		if (array_key_exists("token_fingerprint", $data)) $this -> token_fingerprint = fn1::defaultStr($data["token_fingerprint"], "", true, false);
		if (array_key_exists("provider", $data)) $this -> provider = fn1::defaultStr($data["provider"], "email", true, false);
		if (array_key_exists("userlevel", $data)) $this -> userlevel = (($userlevel = fn1::toNum($data["userlevel"])) > 0 ? $userlevel : NS_USER_USERLEVEL_MIN);
		if (array_key_exists("identifier", $data)) $this -> identifier = fn1::defaultStr($data["identifier"], "", true, false);
		if (array_key_exists("website", $data)) $this -> website = fn1::defaultStr($data["website"], "", true, false);
		if (array_key_exists("profile_url", $data)) $this -> profile_url = fn1::defaultStr($data["profile_url"], "", true, false);
		if (array_key_exists("photo_url", $data)) $this -> photo_url = fn1::defaultStr($data["photo_url"], "", true, false);
		if (array_key_exists("display_name", $data)) $this -> display_name = fn1::defaultStr($data["display_name"], "", true, false);
		if (array_key_exists("description", $data)) $this -> description = fn1::defaultStr($data["description"], "", true, false);
		if (array_key_exists("first_name", $data)) $this -> first_name = fn1::defaultStr($data["first_name"], "", true, false);
		if (array_key_exists("last_name", $data)) $this -> last_name = fn1::defaultStr($data["last_name"], "", true, false);
		if (array_key_exists("gender", $data)) $this -> gender = fn1::defaultStr($data["gender"], "", true, false);
		if (array_key_exists("language", $data)) $this -> language = fn1::defaultStr($data["language"], "", true, false);
		if (array_key_exists("age", $data)) $this -> age = fn1::defaultStr($data["age"], "", true, false);
		if (array_key_exists("birth_day", $data)) $this -> birth_day = fn1::defaultStr($data["birth_day"], "", true, false);
		if (array_key_exists("birth_month", $data)) $this -> birth_month = fn1::defaultStr($data["birth_month"], "", true, false);
		if (array_key_exists("birth_year", $data)) $this -> birth_year = fn1::defaultStr($data["birth_year"], "", true, false);
		if (array_key_exists("username", $data)) $this -> username = fn1::defaultStr($data["username"], "", true, false);
		if (array_key_exists("email", $data)) $this -> email = fn1::defaultStr($data["email"], "", true, false);
		if (array_key_exists("email_verified", $data)) $this -> email_verified = fn1::defaultStr($data["email_verified"], "", true, false);
		if (array_key_exists("verification_code", $data)) $this -> verification_code = fn1::defaultStr($data["verification_code"], "", true, false);
		if (array_key_exists("reset_code", $data)) $this -> reset = fn1::defaultStr($data["reset_code"], "", true, false);
		if (array_key_exists("phone", $data)) $this -> phone = fn1::defaultStr($data["phone"], "", true, false);
		if (array_key_exists("address", $data)) $this -> address = fn1::defaultStr($data["address"], "", true, false);
		if (array_key_exists("country", $data)) $this -> country = fn1::defaultStr($data["country"], "", true, false);
		if (array_key_exists("region", $data)) $this -> region = fn1::defaultStr($data["region"], "", true, false);
		if (array_key_exists("city", $data)) $this -> city = fn1::defaultStr($data["city"], "", true, false);
		if (array_key_exists("zip", $data)) $this -> zip = fn1::defaultStr($data["zip"], "", true, false);
		if (array_key_exists("other", $data)) $this -> other = fn1::defaultStr($data["other"], "");
		if (array_key_exists("timestamp", $data)) $this -> timestamp = fn1::defaultStr($data["timestamp"], "", true, false);
		return $this;
	}
	public function get(){
		$data = [];
		if (!fn1::isEmpty($uid = fn1::toStrn($this -> uid, true))) $data["uid"] = $uid;
		if (($status = $this -> status) !== null) $data["status"] = fn1::toNum($status);
		if (($token = $this -> token) !== null) $data["token"] = fn1::toStrx($token, true);
		if (($token_ip = $this -> token_ip) !== null) $data["token_ip"] = fn1::toStrx($token_ip, true);
		if (($token_useragent = $this -> token_useragent) !== null) $data["token_useragent"] = fn1::toStrx($token_useragent, true);
		if (($token_fingerprint = $this -> token_fingerprint) !== null) $data["token_fingerprint"] = fn1::toStrx($token_fingerprint, true);
		if (!fn1::isEmpty($provider = fn1::toStrn($this -> provider, true))) $data["provider"] = $provider;
		if (($userlevel = fn1::toNum($this -> userlevel)) >= NS_USER_USERLEVEL_MIN) $data["userlevel"] = $userlevel;
		if (!fn1::isEmpty($identifier = fn1::toStrn($this -> identifier, true))) $data["identifier"] = $identifier;
		if (!fn1::isEmpty($website = fn1::toStrn($this -> website, true))) $data["website"] = $website;
		if (!fn1::isEmpty($profile_url = fn1::toStrn($this -> profile_url, true))) $data["profile_url"] = $profile_url;
		if (!fn1::isEmpty($photo_url = fn1::toStrn($this -> photo_url, true))) $data["photo_url"] = $photo_url;
		if (!fn1::isEmpty($display_name = fn1::toStrn($this -> display_name, true))) $data["display_name"] = $display_name;
		if (!fn1::isEmpty($description = fn1::toStrn($this -> description, true))) $data["description"] = $description;
		if (!fn1::isEmpty($first_name = fn1::toStrn($this -> first_name, true))) $data["first_name"] = $first_name;
		if (!fn1::isEmpty($last_name = fn1::toStrn($this -> last_name, true))) $data["last_name"] = $last_name;
		if (!fn1::isEmpty($gender = fn1::toStrn($this -> gender, true))) $data["gender"] = $gender;
		if (!fn1::isEmpty($language = fn1::toStrn($this -> language, true))) $data["language"] = $language;
		if (!fn1::isEmpty($age = fn1::toStrn($this -> age, true))) $data["age"] = $age;
		if (!fn1::isEmpty($birth_day = fn1::toStrn($this -> birth_day, true))) $data["birth_day"] = $birth_day;
		if (!fn1::isEmpty($birth_month = fn1::toStrn($this -> birth_month, true))) $data["birth_month"] = $birth_month;
		if (!fn1::isEmpty($birth_year = fn1::toStrn($this -> birth_year, true))) $data["birth_year"] = $birth_year;
		if (!fn1::isEmpty($username = fn1::toStrn($this -> username, true))) $data["username"] = $username;
		if (!fn1::isEmpty($email = fn1::toStrn($this -> email, true))) $data["email"] = $email;
		if (($email_verified = $this -> email_verified) !== null) $data["email_verified"] = fn1::toNum($email_verified);
		if (($verification_code = $this -> verification_code) !== null) $data["verification_code"] = $verification_code;
		if (($reset_code = $this -> reset_code) !== null) $data["reset_code"] = $reset_code;
		if (!fn1::isEmpty($phone = fn1::toStrn($this -> phone, true))) $data["phone"] = $phone;
		if (!fn1::isEmpty($address = fn1::toStrn($this -> address, true))) $data["address"] = $address;
		if (!fn1::isEmpty($country = fn1::toStrn($this -> country, true))) $data["country"] = $country;
		if (!fn1::isEmpty($region = fn1::toStrn($this -> region, true))) $data["region"] = $region;
		if (!fn1::isEmpty($city = fn1::toStrn($this -> city, true))) $data["city"] = $city;
		if (!fn1::isEmpty($zip = fn1::toStrn($this -> zip, true))) $data["zip"] = $zip;
		if (!fn1::isEmpty($other = fn1::toStrn($this -> other, true))) $data["other"] = $other;
		if (!fn1::isEmpty($timestamp = fn1::toStrn($this -> timestamp, true))) $data["timestamp"] = $timestamp;
		return $data;
	}
	public function save($password=null, $confirm_password=null, $system_user=false){
		$data = $this -> get();
		$uid = fn1::toStrn(fn1::propval($data, "uid"), true);
		$provider = fn1::toStrn(fn1::propval($data, "provider"), true);
		$email = fn1::toStrn(fn1::propval($data, "email"), true);
		$username = fn1::toStrn(fn1::propval($data, "username"), true);
		$identifier = fn1::toStrn(fn1::propval($data, "identifier"), true);
		$display_name = fn1::toStrn(fn1::propval($data, "display_name"), true);
		$userlevel = fn1::toStrn(fn1::propval($data, "userlevel"), true);
		$password = fn1::toStrn($password);
		$confirm_password = fn1::toStrn($confirm_password);
		$db = null;
		try {
			if (!in_array(strtolower($provider), self::PROVIDERS)) throw new \Exception(lg::str("AUTH_USER_ERROR_PROVIDER", $provider), self::STOP_CRITICAL);
			if ($provider == "email"){
				if (!fn1::isEmpty($password) && strlen($password) < NS_USER_PASSWORD_MIN) throw new \Exception(lg::get("AUTH_USER_ERROR_PASSWORD"), self::STOP_CRITICAL);
				if (!fn1::isEmpty($confirm_password) && $confirm_password != $password) throw new \Exception(lg::get("AUTH_USER_ERROR_PASSWORD_MATCH"), self::STOP_CRITICAL);
			}
			$db = new db();
			if (fn1::isEmpty($uid)){
				if ($provider == "email" && fn1::isEmpty($password)) throw new \Exception(lg::get("AUTH_USER_ERROR_PASSWORD"), self::STOP_CRITICAL);
				elseif ($provider != "email" && fn1::isEmpty($identifier)) throw new \Exception(lg::get("AUTH_USER_ERROR_IDENTIFIER"), self::STOP_CRITICAL);
				if (fn1::isEmpty($display_name)) throw new \Exception(lg::get("AUTH_USER_ERROR_NAMES"), self::STOP_CRITICAL);
				if (!fn1::isEmail($email)) throw new \Exception(lg::str("AUTH_USER_ERROR_EMAIL", $email), self::STOP_CRITICAL);
				if ($db -> queryExists(NS_TABLE_USERS, "`email` = ?", [$email])) throw new \Exception(lg::str("AUTH_USER_ERROR_EMAIL_EXISTS", $email), self::STOP_CRITICAL);
				if (!fn1::isEmpty($identifier) && $db -> queryExists(NS_TABLE_USERS, "`identifier` = ?", [$identifier])) throw new \Exception(lg::str("AUTH_USER_ERROR_IDENTIFIER_EXIST", $identifier), self::STOP_CRITICAL);
				if (!fn1::isEmpty($username)){
					if (in_array(strtolower($username), ["admin", "root", "super", "user", "domain"])) throw new \Exception(lg::str("AUTH_USER_ERROR_USERNAME_RESERVED", $username), self::STOP_CRITICAL);
					if (strlen($username) < 4) throw new \Exception(lg::str("AUTH_USER_ERROR_USERNAME_SHORT", $username), self::STOP_CRITICAL);
					if ($db -> queryExists(NS_TABLE_USERS, "`username` = ?", [$username])) throw new \Exception(lg::str("AUTH_USER_ERROR_USERNAME_EXIST", $username), self::STOP_CRITICAL);
				}
				if (($verification_code = $db -> createToken(NS_TABLE_USERS, "verification_code")) === false) throw new \Exception($db -> err, $db -> errno);
				// if (!self::sendVerificationCode($email, $display_name, $verification_code)) throw new \Exception(self::$last_error, self::STOP_CRITICAL);
				if (($uid = $db -> createToken(NS_TABLE_USERS, "uid")) === false) throw new \Exception($db -> err, $db -> errno);
				if (!fn1::isEmpty($password)) $data["hash"] = fn1::hashstr($password, NS_USER_PASSWORD_HASH);
				$data["uid"] = $uid;
				$data["timestamp"] = fn1::now();
				$data["email_verified"] = 0;
				$data["verification_code"] = $verification_code;
				if (!audit::sysLog(lg::get("AUDIT_ACTION_USER_CREATE"), $uid, null, null, "provider: " . $provider)) throw new \Exception(audit::$last_error, self::STOP_CRITICAL);
				if (!$db -> insert(NS_TABLE_USERS, $data)) throw new \Exception($db -> err, $db -> errno);
				$db -> close();
				$this -> set($data);
				return $this -> success($data);
			}
			else {
				if (!$system_user && !self::permit($uid)) throw new \Exception("[0]".self::$last_error, self::STOP_CRITICAL);
				$email_change = false;
				if (($user = $db -> queryItem(NS_TABLE_USERS, null, "`uid` = ?", [$uid])) === false) throw new \Exception($db -> err, $db -> errno);
				if ($user === null) throw new \Exception(lg::str("AUTH_USER_ERROR_ACCOUNT_NOT_FOUND", $uid), self::STOP_CRITICAL);
				if (!fn1::isEmpty($userlevel) && (int) $userlevel != (int) $user["userlevel"]){
					if (!self::permit()) throw new \Exception("[1]" . self::$last_error, self::STOP_CRITICAL);
				}
				if ($email != $user["email"]){
					if ($db -> queryExists(NS_TABLE_USERS, "`email` = ?", [$email])) throw new \Exception(lg::str("AUTH_USER_ERROR_EMAIL_EXISTS", $email), self::STOP_CRITICAL);
					$email_change = true;
				}
				if ($identifier != $user["identifier"] && !fn1::isEmpty($identifier) && $db -> queryExists(NS_TABLE_USERS, "`identifier` = ?", [$identifier])) throw new \Exception(lg::str("AUTH_USER_ERROR_IDENTIFIER_EXIST", $identifier), self::STOP_CRITICAL);
				if ($username != $user["username"] && !fn1::isEmpty($username)){
					if (in_array(strtolower($username), ["admin", "root", "super", "user", "domain"])) throw new \Exception(lg::str("AUTH_USER_ERROR_USERNAME_RESERVED", $username), self::STOP_CRITICAL);
					if (strlen($username) < 4) throw new \Exception(lg::str("AUTH_USER_ERROR_USERNAME_SHORT", $username), self::STOP_CRITICAL);
					if ($db -> queryExists(NS_TABLE_USERS, "`username` = ?", [$username])) throw new \Exception(lg::str("AUTH_USER_ERROR_USERNAME_EXIST", $username), self::STOP_CRITICAL);
				}
				if ($email_change){
					if (($verification_code = $db -> createToken(NS_TABLE_USERS, "verification_code")) === false) throw new \Exception($db -> err, $db -> errno);
					if (!self::sendVerificationCode($email, $user["display_name"], $verification_code)) throw new \Exception(self::$last_error, self::STOP_CRITICAL);
					$data["email_verified"] = 0;
					$data["verification_code"] = $verification_code;
				}
				if (!fn1::isEmpty($password)) $data["hash"] = fn1::hashstr($password, NS_USER_PASSWORD_HASH);
				$session_uid = isset($_SESSION["uid"]) ? fn1::toStrn($_SESSION["uid"]) : $uid;
				$compare = fn1::arraysCompare($user, $data);
				if (is_array($compare) && !audit::log($session_uid, lg::get("AUDIT_ACTION_USER_UPDATE"), $uid, $compare["from"], $compare["to"], "provider: " . $provider)) throw new \Exception(audit::$last_error, self::STOP_CRITICAL);
				if (!$db -> update(NS_TABLE_USERS, $data, "`uid` = ?", [$uid])) throw new \Exception($db -> err, $db -> errno);
				$db -> close();
				$data = fn1::arrayMerge($user, $data, true, false);
				$this -> set($data);
				return $this -> success($data);
			}
		}
		catch (\Exception $e){
			if ($db instanceof Database) $db -> close();
			return $this -> error($e -> getMessage());
		}
	}
	public function setSession(){
		if (!isset($_SESSION)) session_start();
		$_SESSION = [];
		$_SESSION["uid"] = $this -> uid;
		$_SESSION["status"] = $this -> status;
		$_SESSION["token"] = $this -> token;
		$_SESSION["provider"] = $this -> provider;
		$_SESSION["userlevel"] = (int) $this -> userlevel;
		$_SESSION["identifier"] = $this -> identifier;
		$_SESSION["website"] = $this -> website;
		$_SESSION["profile_url"] = $this -> profile_url;
		$_SESSION["photo_url"] = $this -> photo_url;
		$_SESSION["display_name"] = $this -> display_name;
		$_SESSION["description"] = $this -> description;
		$_SESSION["first_name"] = $this -> first_name;
		$_SESSION["last_name"] = $this -> last_name;
		$_SESSION["gender"] = $this -> gender;
		$_SESSION["language"] = $this -> language;
		$_SESSION["age"] = $this -> age;
		$_SESSION["birth_day"] = $this -> birth_day;
		$_SESSION["birth_month"] = $this -> birth_month;
		$_SESSION["birth_year"] = $this -> birth_year;
		$_SESSION["username"] = $this -> username;
		$_SESSION["email"] = $this -> email;
		$_SESSION["email_verified"] = $this -> email_verified;
		$_SESSION["verification_code"] = $this -> verification_code;
		$_SESSION["reset_code"] = $this -> reset_code;
		$_SESSION["phone"] = $this -> phone;
		$_SESSION["address"] = $this -> address;
		$_SESSION["country"] = $this -> country;
		$_SESSION["region"] = $this -> region;
		$_SESSION["city"] = $this -> city;
		$_SESSION["zip"] = $this -> zip;
		$_SESSION["other"] = $this -> other;
		$_SESSION["timestamp"] = $this -> timestamp;
		return $_SESSION;
	}
	public static function permit($allow_uid=null, $session_check=true, $userlevels=null){
		if ($userlevels === null) $userlevels = [NS_USER_USERLEVEL_ADMIN];
		else $userlevels = fn1::toArray($userlevels, true);
		if (isset($_SESSION["uid"]) && isset($_SESSION["userlevel"])){
			$allow_uid = fn1::toStrn($allow_uid, true);
			$session_uid = fn1::toStrn($_SESSION["uid"], true);
			$session_userlevel = fn1::toNum($_SESSION["userlevel"]);
			if ($session_uid == $allow_uid) return true;
			if (!in_array($session_userlevel, $userlevels)) return self::__error(lg::str("AUTH_USER_ERROR_PERMIT_USERLEVEL", $session_userlevel));
		}
		elseif ($session_check) return self::__error(lg::get("AUTH_USER_ERROR_PERMIT_SESSION"));
		return true;
	}
	public static function sendVerificationCode($email, $names, $code){
		if (fn1::isEmpty($names = fn1::toStrn($names, true))) return self::__error(lg::get("AUTH_USER_ERROR_NAMES"));
		if (fn1::isEmpty($code = fn1::toStrn($code, true))) return self::__error(lg::get("AUTH_USER_VERIFY_CODE"));
		if (!fn1::isEmail($email = fn1::toStrn($email, true))) return self::__error(lg::str("AUTH_USER_ERROR_EMAIL", $email));
		$link = fn1::curlsReplace(NS_USER_VERIFY_LINK, ["code" => $code]);
		$subject = NS_USER_VERIFY_SUBJECT;
		$message = NS_USER_VERIFY_MESSAGE;
		$heading = fn1::curlsReplace(NS_USER_VERIFY_HEADING, ["names" => $names]);
		$message = self::getEmailTemplate($subject, $heading, $message, $link, NS_USER_VERIFY_LINK_TEXT);
		try {
			$mailer = new Mailer(true);
			$mailer -> setFrom(NS_USER_EMAIL_FROM, NS_USER_EMAIL_FROM_NAMES);
			$mailer -> addReplyTo(NS_USER_EMAIL_FROM, NS_USER_EMAIL_FROM_NAMES);
			$mailer -> addTo($email, $names);
			$mailer -> setSubject($subject);
			$mailer -> setBody($message);
			if (!$mailer -> send()) throw new \Exception($mailer -> getErrorMessage(), self::STOP_CRITICAL);
			return self::__success($mailer -> getSuccessMessage());
		}
		catch (\Exception $e){
			return self::__error($e -> getMessage());
		}
	}
	public static function verifyEmail($code){
		if (fn1::isEmpty($code = fn1::toStrn($code, true))) return self::__error(lg::get("AUTH_USER_VERIFY_CODE"));
		$db = null;
		try {
			$db = new db();
			if (($user_data = $db -> queryItem(NS_TABLE_USERS, null, "`verification_code` = ?", [$code])) === false) throw new \Exception($db -> err, $db -> errno);
			$db  -> close();
			if ($user_data === null) throw new \Exception(lg::get("AUTH_USER_VERIFY_INVALID"), self::STOP_CRITICAL);
			$user = new static($user_data);
			$user -> verification_code = "";
			$user -> email_verified = "1";
			if (!$user -> save(null, null, true)) throw new \Exception($user -> getErrorMessage(), self::STOP_CRITICAL);
			return self::__success($user -> get());
		}
		catch (\Exception $e){
			if ($db instanceof Database) $db -> close();
			return self::__error($e -> getMessage());
		}
	}
	public static function recoverPassword($email){
		if (!fn1::isEmail($email = fn1::toStrn($email, true))) return self::__error(lg::str("AUTH_USER_ERROR_EMAIL", $email));
		$db = null;
		try {
			$db = new db();
			if (($user_data = $db -> queryItem(NS_TABLE_USERS, null, "`email` = ?", [$email])) === false) throw new \Exception($db -> err, $db -> errno);
			if ($user_data === null) throw new \Exception(lg::str("AUTH_USER_ERROR_ACCOUNT_NOT_FOUND", $email), self::STOP_CRITICAL);
			$user = new static($user_data);
			$display_name = $user -> display_name;
			if (($reset_code = $db -> createToken(NS_TABLE_USERS, "reset_code")) === false) throw new \Exception($db -> err, $db -> errno);
			$db -> close();
			$user -> reset_code = $reset_code;
			if (!self::sendResetCode($email, $display_name, $reset_code)) throw new \Exception(self::$last_error, self::STOP_CRITICAL);
			if (!$user -> save(null, null, true)) throw new \Exception($user -> getErrorMessage(), self::STOP_CRITICAL);
			return self::__success($user -> get());
		}
		catch (\Exception $e){
			if ($db instanceof Database) $db -> close();
			return self::__error($e -> getMessage());
		}
	}
	public static function sendResetCode($email, $names, $code){
		if (fn1::isEmpty($names = fn1::toStrn($names, true))) return self::__error(lg::get("AUTH_USER_ERROR_NAMES"));
		if (fn1::isEmpty($code = fn1::toStrn($code, true))) return self::__error(lg::get("AUTH_USER_RESET_CODE"));
		if (!fn1::isEmail($email = fn1::toStrn($email, true))) return self::__error(lg::str("AUTH_USER_ERROR_EMAIL", $email));
		$link = fn1::curlsReplace(NS_USER_RESET_LINK, ["code" => $code]);
		$subject = NS_USER_RESET_SUBJECT;
		$message = NS_USER_RESET_MESSAGE;
		$heading = fn1::curlsReplace(NS_USER_RESET_HEADING, ["names" => $names]);
		$message = self::getEmailTemplate($subject, $heading, $message, $link, NS_USER_RESET_LINK_TEXT);
		try {
			$mailer = new Mailer(true);
			$mailer -> setFrom(NS_USER_EMAIL_FROM, NS_USER_EMAIL_FROM_NAMES);
			$mailer -> addReplyTo(NS_USER_EMAIL_FROM, NS_USER_EMAIL_FROM_NAMES);
			$mailer -> addTo($email, $names);
			$mailer -> setSubject($subject);
			$mailer -> setBody($message);
			if (!$mailer -> send()) throw new \Exception($mailer -> getErrorMessage(), self::STOP_CRITICAL);
			return self::__success($mailer -> getSuccessMessage());
		}
		catch (\Exception $e){
			return self::__error($e -> getMessage());
		}
	}
	public static function resetPassword($code, $password, $confirm_password=null){
		if (fn1::isEmpty($code = fn1::toStrn($code, true))) return self::__error(lg::get("AUTH_USER_RESET_CODE"));
		if (fn1::isEmpty($password = fn1::toStrn($password))) return self::__error(lg::get("AUTH_USER_ERROR_PASSWORD"));
		$db = null;
		try {
			$db = new db();
			if (($user_data = $db -> queryItem(NS_TABLE_USERS, null, "`reset_code` = ?", [$code])) === false) throw new \Exception($db -> err, $db -> errno);
			$db  -> close();
			if ($user_data === null) throw new \Exception(lg::get("AUTH_USER_RESET_INVALID"), self::STOP_CRITICAL);
			$user = new static($user_data);
			$user -> reset_code = "";
			if (!$user -> save($password, $confirm_password, true)) throw new \Exception($user -> getErrorMessage(), self::STOP_CRITICAL);
			return self::__success($user -> get());
		}
		catch (\Exception $e){
			if ($db instanceof Database) $db -> close();
			return self::__error($e -> getMessage());
		}
	}
	public static function getPasswordUser($username, $password){
		if (fn1::isEmpty($username = fn1::toStrn($username, true))) return self::__error(lg::get("AUTH_USER_ERROR_LOGIN_USERNAME"));
		if (fn1::isEmpty($password = fn1::toStrn($password))) return self::__error(lg::get("AUTH_USER_ERROR_LOGIN_PASSWORD"));
		$db = null;
		try {
			$db = new db();
			$hash = fn1::hashstr($password, NS_USER_PASSWORD_HASH);
			if (($user_data = $db -> queryItem(NS_TABLE_USERS, null, "(`email` = ? OR `username` = ?) AND `hash` = ?", [$username, $username, $hash])) === false) throw new \Exception($db -> err, $db -> errno);
			$db  -> close();
			if ($user_data === null) throw new \Exception(lg::get("AUTH_USER_ERROR_CREDENTIALS"), self::STOP_CRITICAL);
			return new static($user_data);
		}
		catch (\Exception $e){
			if ($db instanceof Database) $db -> close();
			return self::__error($e -> getMessage());
		}
	}
	public static function createSessionToken($uid){
		if (!($user = self::getUser($uid, true))) return false;
		$db = null;
		try {
			$db = new db();
			if (($token = $db -> createToken(NS_TABLE_USERS, "token", 64)) === false) throw new \Exception($db -> err, $db -> errno);
			$user -> token = $token;
			$user -> token_ip = fn1::getIP();
			$user -> token_useragent = fn1::getUserAgent();
			$user -> token_fingerprint = md5($user -> token_ip . $user -> token_useragent);
			if (!$user -> save(null, null, true)) throw new \Exception($user -> getErrorMessage());
			return $user;
		}
		catch (\Exception $e){
			if ($db instanceof Database) $db -> close();
			return self::__error($e -> getMessage());
		}
	}
	public static function logoutSession($token){
		if (fn1::isEmpty($token = fn1::toStrx($token, true))) return true;
		$db = null;
		try {
			$db = new db();
			if (!$db -> update(NS_TABLE_USERS, ["token" => ""], "token = ?", [$token])) throw new \Exception($db -> err, $db -> errno);
			$db -> close();
			return true;
		}
		catch (\Exception $e){
			if ($db instanceof Database) $db -> close();
			return self::__error($e -> getMessage());
		}
	}
	public static function getIdentifierUser($identifier, $provider){
		if (fn1::isEmpty($identifier = fn1::toStrn($identifier, true))) return self::__error(lg::get("AUTH_USER_ERROR_IDENTIFIER"));
		if (!in_array(($provider = strtolower(fn1::toStrx($provider))), self::PROVIDERS)) return self::__error(lg::str("AUTH_USER_ERROR_PROVIDER", $provider));
		if ($provider == "email") return self::__error(lg::get("AUTH_USER_ERROR_PROVIDER_EMAIL"));
		$db = null;
		try {
			$db = new db();
			if (($user_data = $db -> queryItem(NS_TABLE_USERS, null, "`provider` = ? AND `identifier` = ?", [$provider, $identifier])) === false) throw new \Exception($db -> err, $db -> errno);
			$db  -> close();
			if ($user_data === null) return null;
			return new static($user_data);
		}
		catch (\Exception $e){
			if ($db instanceof Database) $db -> close();
			return self::__error($e -> getMessage());
		}
	}
	public static function getUser($uid, $system_user=false){
		if (fn1::isEmpty($uid = fn1::toStrn($uid, true))) return self::__error(lg::get("AUTH_USER_ERROR_UID"));
		$db = null;
		try {
			if (!$system_user && !self::permit($uid)) throw new \Exception(self::$last_error, self::STOP_CRITICAL);
			$db = new db();
			if (($user_data = $db -> queryItem(NS_TABLE_USERS, null, "`uid` = ?", [$uid])) === false) throw new \Exception($db -> err, $db -> errno);
			$db  -> close();
			if ($user_data === null) throw new \Exception(lg::str("AUTH_USER_ERROR_UID_NOT_FOUND", $uid), self::STOP_CRITICAL);
			return new static($user_data);
		}
		catch (\Exception $e){
			if ($db instanceof Database) $db -> close();
			return self::__error($e -> getMessage());
		}
	}
	public static function getTokenUser($token){
		if (fn1::isEmpty($token = fn1::toStrx($token, true))) return self::__error(lg::get("AUTH_USER_ERROR_TOKEN"));
		$db = null;
		try {
			$db = new db();
			if (($user_data = $db -> queryItem(NS_TABLE_USERS, null, "`token` = ?", [$token])) === false) throw new \Exception($db -> err, $db -> errno);
			$db  -> close();
			if ($user_data === null) throw new \Exception(lg::get("AUTH_USER_ERROR_TOKEN_NOT_FOUND"), self::STOP_CRITICAL);
			return new static($user_data);
		}
		catch (\Exception $e){
			if ($db instanceof Database) $db -> close();
			return self::__error($e -> getMessage());
		}
	}
	public static function getEmailTemplate($title, $message_title, $message_text, $button_href, $button_text){
		$builder = new EmailBuilder("{{title}}", "#ddd");
		$builder -> pre(fn1::stripHTML($message_text));
		$container = $builder -> container();
		$body = $container -> grid(null, "center");
		$body -> row() -> col_spacer();
		if (NS_MAIL_TEMPLATE_LOGO != '') $body -> row() -> col_pad_center("background-color:#fff;") -> image_200x50(NS_MAIL_TEMPLATE_LOGO, "Logo", "#fff");
		$body -> row() -> col("font-size: 0px; line-height: 0px;", "", "", "20", "true");
		$content = $body -> row() -> col_white() -> grid();
		$content_body = $content -> row() -> col_text_15();
		$content_body -> h1($message_title);
		$content_body -> p($message_text) -> html("<br>");
		$content_body -> buttonBlock($button_href, $button_text) -> html("<br>");
		$footer = $container -> grid(null, "center");
		$footer = $footer -> row() -> col_text_12();
		if (NS_MAIL_FOOTER_TEMPLATE != '') $footer -> html(NS_MAIL_FOOTER_TEMPLATE);
		if (NS_MAIL_UNSUBSCRIBE_TEMPLATE != '') $footer -> html(NS_MAIL_UNSUBSCRIBE_TEMPLATE);
		$email_html = $builder -> build(true);
		return $email_html;
	}
}

/* NS_TABLE_AUDIT TABLE CREATE SQL:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
CREATE TABLE `ns_users` (
	`_index` int(20) NOT NULL,
	`uid` varchar(200) NOT NULL,
	`status` varchar(200) DEFAULT NULL,
	`token` varchar(200) DEFAULT NULL,
	`hash` varchar(200) NOT NULL,
	`provider` varchar(200) DEFAULT NULL,
	`userlevel` tinyint(2) NOT NULL,
	`identifier` varchar(200) DEFAULT NULL,
	`website` varchar(200) DEFAULT NULL,
	`profile_url` varchar(200) DEFAULT NULL,
	`photo_url` text,
	`display_name` varchar(200) NOT NULL,
	`description` text,
	`first_name` varchar(200) DEFAULT NULL,
	`last_name` varchar(200) DEFAULT NULL,
	`gender` varchar(200) DEFAULT NULL,
	`language` varchar(200) DEFAULT NULL,
	`age` varchar(200) DEFAULT NULL,
	`birth_day` varchar(200) DEFAULT NULL,
	`birth_month` varchar(200) DEFAULT NULL,
	`birth_year` varchar(200) DEFAULT NULL,
	`username` varchar(200) DEFAULT NULL,
	`email` varchar(200) NOT NULL,
	`email_verified` tinyint(2) DEFAULT NULL,
	`verification_code` varchar(200) DEFAULT NULL,
	`reset_code` varchar(200) DEFAULT NULL,
	`phone` varchar(200) DEFAULT NULL,
	`address` varchar(200) DEFAULT NULL,
	`country` varchar(200) DEFAULT NULL,
	`region` varchar(200) DEFAULT NULL,
	`city` varchar(200) DEFAULT NULL,
	`zip` varchar(200) DEFAULT NULL,
	`other` text,
	`timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `ns_users` ADD PRIMARY KEY (`uid`), ADD KEY `_index` (`_index`);
ALTER TABLE `ns_users` MODIFY `_index` int(20) NOT NULL AUTO_INCREMENT;
COMMIT;
*/
