<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190726
*	CREATE QR CODE
*/

namespace Naicode\Server\Plugin;
require_once __DIR__ . "/../funcs.php";
use Naicode\Server\Funcs as fn1;

//handle qr code request
if (isset($_GET["qr_code"])){
	$text = $_GET["qr_code"];
	$size = isset($_GET["size"]) && is_numeric($_GET["size"]) ? $_GET["size"] : null;
	$margin = isset($_GET["margin"]) && is_numeric($_GET["margin"]) ? $_GET["margin"] : null;
	CreateQRCode($text, $size, $margin);
}

//generate QR code
function CreateQRCode($text="Default", $size=null, $margin=null, $output=true){
	//include library (http://phpqrcode.sourceforge.net/)
	include __DIR__ . "/lib-phpqrcode/qrlib.php";

	//generate QR code
	$text = fn1::toStrn($text, true);
	$size = ($size = fn1::toNum($size)) > 0 ? $size : 3;
	$margin = ($margin = fn1::toNum($margin)) > 0 ? $margin : 4;
	ob_start();
	\QRcode::png($text, false, 0, $size, $margin);
	$image_data = ob_get_contents();
	ob_end_clean();

	//output png image
	if ($output){
		header("Content-Type: image/png");
		echo $image_data;
		exit();
	}
	return $image_data;
}
