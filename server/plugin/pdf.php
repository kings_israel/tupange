<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190719
*	FPDF - Helper
*/

namespace Naicode\Server\Plugin;
require_once __DIR__ . "/fpdf/fpdf.php";
require_once __DIR__ . "/../funcs.php";
use Naicode\Server\Funcs as fn1;
use FPDF;

//PDF class
class PDF extends FPDF {
	const PAGE_SIZE = "A4";
	const PAGE_ROTATION = 0;
	const PAGE_ORIENTATION = "P";
	const ALIAS_NB_PAGE = "{p}";
	const ALIAS_NB_PAGES = "{nb}";
	const FOOTER_TEMPLATE = "Page {p}/{nb}";
	const FONT = "Helvetica";
	const FONT_SIZE = 9;
	const LINE_WIDTH = 0.1;
	const CELL_HEIGHT = 5;
	const CELL_WIDTH_MIN = 5;
	const CELL_WIDTH_MAX = 50;
	const DRAW_COLOR = [230, 230, 230];
	const FONT_COLOR = [68, 68, 68];
	const BACKGROUND_COLOR = [255, 255, 255];
	public $page_size = self::PAGE_SIZE;
	public $page_rotation = self::PAGE_ROTATION;
	public $page_orientation = self::PAGE_ORIENTATION;
	public $alias_nb_page = self::ALIAS_NB_PAGE;
	public $alias_nb_pages = self::ALIAS_NB_PAGES;
	public $footer_template = self::FOOTER_TEMPLATE;
	public $footer_font = self::FONT;
	public $footer_font_size = self::FONT_SIZE;
	public $footer_cell_height = self::CELL_HEIGHT;
	public $font = self::FONT;
	public $font_size = self::FONT_SIZE;
	public $line_width = self::LINE_WIDTH;
	public $cell_height = self::CELL_HEIGHT;
	public $cell_width_min = self::CELL_WIDTH_MIN;
	public $cell_width_max = self::CELL_WIDTH_MAX;
	public $draw_color = self::DRAW_COLOR;
	public $font_color = self::FONT_COLOR;
	public $background_color = self::BACKGROUND_COLOR;
	public $header_title = null;
	public $header_title_left = null;
	public $header_subtitle = null;
	public $header_logo_left = null;
	public $header_logo_center = null;
	public $header_logo_right = null;
	public $page_added = false;
	public $create_header = true;
	public $create_footer = true;
	private $add_link_items = [];
	private $header_items = [];
	public function __construct($page_orientation=null, $page_size=null, $page_rotation=null){
		parent::__construct();
		if ($page_size !== null) $this -> page_size = self::pageSize($page_size);
		if ($page_rotation !== null) $this -> page_rotation = self::pageRotation($page_rotation);
		if ($page_orientation !== null) $this -> page_orientation = self::pageOrientation($page_orientation);
	}
	public function init(){
		$this -> alias_nb_page = strlen($alias_nb_page = fn1::toStrx($this -> alias_nb_page, true)) ? $alias_nb_page : self::ALIAS_NB_PAGE;
		$this -> alias_nb_pages = strlen($alias_nb_pages = fn1::toStrx($this -> alias_nb_pages, true)) ? $alias_nb_pages : self::ALIAS_NB_PAGES;
		if ($this -> footer_template !== null) $this -> footer_template = strlen($footer_template = fn1::toStrx($this -> footer_template, true)) ? $footer_template : self::FOOTER_TEMPLATE;
		$this -> footer_font = strlen($footer_font = fn1::toStrx($this -> footer_font, true)) ? $footer_font : self::FONT;
		$this -> footer_font_size = ($footer_font_size = fn1::toNum($this -> footer_font_size)) > 0 ? $footer_font_size : self::FONT_SIZE;
		$this -> footer_cell_height = ($footer_cell_height = fn1::toNum($this -> footer_cell_height)) > 0 ? $footer_cell_height : self::CELL_HEIGHT;
		$this -> font = strlen($font = fn1::toStrx($this -> font, true)) ? $font : self::FONT;
		$this -> font_size = ($font_size = fn1::toNum($this -> font_size)) > 0 ? $font_size : self::FONT_SIZE;
		$this -> line_width = ($line_width = fn1::toNum($this -> line_width)) > 0 ? $line_width : self::LINE_WIDTH;
		$this -> cell_height = ($cell_height = fn1::toNum($this -> cell_height)) > 0 ? $cell_height : self::CELL_HEIGHT;
		$this -> cell_width_min = ($cell_width_min = fn1::toNum($this -> cell_width_min)) > 0 ? $cell_width_min : self::CELL_WIDTH_MIN;
		$this -> cell_width_max = ($cell_width_max = fn1::toNum($this -> cell_width_max)) > 0 ? $cell_width_max : self::CELL_WIDTH_MAX;
		$this -> draw_color = self::isRGB($draw_color = $this -> draw_color) ? $draw_color : self::DRAW_COLOR;
		$this -> font_color = self::isRGB($font_color = $this -> font_color) ? $font_color : self::FONT_COLOR;
		$this -> background_color = self::isRGB($background_color = $this -> background_color) ? $background_color : self::BACKGROUND_COLOR;
	}
	public function addPage($page_orientation=null, $page_size=null, $page_rotation=null, $create_header=true){
		$this -> init();
		if ($page_size === null) $page_size = $this -> page_size;
		if ($page_rotation === null) $page_rotation = $this -> page_rotation;
		if ($page_orientation === null) $page_orientation = $this -> page_orientation;
		if (!$this -> page_added){
			$this -> AliasNbPages($this -> alias_nb_pages);
		}
		$this -> create_header = !!$create_header;
		$page_orientation = self::pageOrientation($page_orientation);
		$page_size = self::pageSize($page_size);
		$page_rotation = self::pageRotation($page_rotation);
		parent::AddPage($page_orientation, $page_size, $page_rotation);
		$this -> SetLineWidth($this -> line_width);
		$this -> setFont($this -> font, "", $this -> font_size);
		$this -> setRGB("SetFillColor", $this -> background_color);
		$this -> setRGB("SetTextColor", $this -> font_color);
		$this -> setRGB("SetDrawColor", $this -> draw_color);
		$this -> page_added = true;
	}
	public function setDefaultHeaderItems($title=null, $subtitle=null, $logo=null, $logo_width=40, $logo_page_align="L", $logo_page_margin=10){
		$items = [];
		if (!fn1::isEmpty($logo = fn1::toStrx($logo, true))){
			$logo_width = ($logo_width = fn1::toNum($logo_width)) ? $logo_width : 40;
			$items[] = self::itemImage($logo, 10, 6, $logo_width, null, null, null, $logo_page_align, $logo_page_margin);
		}
		if (!fn1::isEmpty($title = fn1::toStrx($title, true))){
			$items[] = self::itemFont($this -> font, 14, "B");
			$items[] = self::itemCell($title, 0, 5, 0, 0, "C");
		}
		if (!fn1::isEmpty($subtitle = fn1::toStrx($subtitle, true))){
			$items[] = self::itemLn();
			$items[] = self::itemFont($this -> font, 9);
			$items[] = self::itemCell($subtitle, 0, 5, 0, 0, "C");
		}
		$this -> header_items = $items;
		return $this;
	}
	public function addHeaderItem($item, $clear=false){
		if ($clear) $this -> header_items = [];
		if (self::isItem($item)) $this -> header_items[] = $item;
		return $this;
	}
	public function header(){
		if (!$this -> create_header || !count($this -> header_items)) return;
		foreach ($this -> header_items as $item) $this -> setItem($item);
	}
	public function footer(){
		if (!$this -> create_footer || $this -> footer_template === null) return;
		$this -> init();
		$this -> SetY(-15);
		$this -> SetFont($this -> footer_font, "", $this -> footer_font_size);
		$template = str_replace($this -> alias_nb_page, $this -> PageNo(), $this -> footer_template);
		$this -> Cell(0, $this -> footer_cell_height, $template, 0, 0, "C");
	}
	public function table($data=null){
		//$this -> addPage();
		return new PDFTable($data, $this);
	}
	public function setItem($item){
		if (!self::isItem($item)) return;
		switch ($item["item"]){
			case "ADD_LINK":
			if (!fn1::isEmpty($key = fn1::toStrn(fn1::propval($item, "key"), true))) $this -> add_link_items[$key] = $this -> AddLink();
			break;

			case "IMAGE":
			$image = fn1::toStrx(fn1::propval($item, "image"), true);
			$x = is_numeric($x = fn1::propval($item, "x")) ? fn1::toNum($x) : null;
			$y = is_numeric($y = fn1::propval($item, "y")) ? fn1::toNum($y) : null;
			$w = fn1::toNum(fn1::propval($item, "w"));
			$h = fn1::toNum(fn1::propval($item, "h"));
			$type = strlen($type = fn1::toStrx(fn1::propval($item, "type"), true)) ? $type : null;
			$link = array_key_exists(($link = fn1::propval($item, "link")), $this -> add_link_items) ? $this -> add_link_items[$link] : null;
			$page_align = self::isAlign($page_align = fn1::propval($item, "page_align")) ? $page_align : null;
			if ($page_align !== null){
				list($image_width, $image_height) = @getimagesize($image);
				if ($image_width && $image_height){
					$image_width = fn1::px2pt($image_width);
					$image_height = fn1::px2pt($image_height);
					if ($w && !$h){
						$image_height = $image_height * fn1::resizeRatio($image_width, $w);
						$image_width = $w;
					}
					elseif (!$w && $h){
						$image_width = $image_width * fn1::resizeRatio($image_height, $h);
						$image_height = $h;
					}
					elseif ($w && $h){
						$image_width = $w;
						$image_height = $h;
					}
					$page_align_margin = is_numeric($page_align_margin = fn1::propval($item, "page_align_margin")) ? fn1::toNum($page_align_margin) : 0;
					if ($page_align == "L") $x = $page_align_margin;
					else {
						$page_width = $pdf -> GetPageWidth();
						if ($image_width >= $page_width){
							$resized_width = $page_width - ($page_align_margin * 2);
							$image_height = $image_height * fn1::resizeRatio($image_width, $resized_width);
							$image_width = $resized_width;
						}
						if ($page_align == "C") $x = ($page_width / 2) - ($image_width / 2);
						elseif ($page_align == "R") $x = $page_width - ($image_width + $page_align_margin);
					}
				}
			}
			$this -> Image($image, $x, $y, $w, $h, $type, $link);
			break;

			case "FONT":
			$family = fn1::toStrx(fn1::propval($item, "family"), true);
			$size = is_numeric($size = fn1::propval($item, "size")) ? fn1::toNum($size) : 0;
			$style = self::isFontStyle($style = fn1::toStrx(fn1::propval($item, "style"), true)) ? $style : "";
			$this -> SetFont($family, $style, $size);
			break;

			case "CELL":
			$w = is_numeric($w = fn1::propval($item, "w")) ? fn1::toNum($w) : 0;
			$h = is_numeric($h = fn1::propval($item, "h")) ? fn1::toNum($h) : 0;
			$text = fn1::toStrx(fn1::propval($item, "text"), true);
			$border = self::isBorder($border = fn1::propval($item, "border")) ? $border : 0;
			$ln = in_array(($ln = (int) fn1::propval($item, "ln")), [0, 1, 2]) ? $ln : 0;
			$align = self::isAlign($align = fn1::propval($item, "align")) ? $align : "L";
			$fill = !!fn1::propval($item, "fill");
			$link = array_key_exists(($link = fn1::propval($item, "link")), $this -> add_link_items) ? $this -> add_link_items[$link] : null;
			$this -> Cell($w, $h, $text, $border, $ln, $align, $fill, $link);
			break;

			case "MULTICELL":
			$w = is_numeric($w = fn1::propval($item, "w")) ? fn1::toNum($w) : 0;
			$h = is_numeric($h = fn1::propval($item, "h")) ? fn1::toNum($h) : 0;
			$text = fn1::toStrx(fn1::propval($item, "text"), true);
			$border = self::isBorder($border = fn1::propval($item, "border")) ? $border : 0;
			$align = self::isAlign($align = fn1::propval($item, "align"), true) ? $align : "L";
			$fill = !!fn1::propval($item, "fill");
			$this -> MultiCell($w, $h, $text, $border, $align, $fill);
			break;

			case "LINE":
			$x = is_numeric($x = fn1::propval($item, "x")) ? fn1::toNum($x) : 0;
			$y = is_numeric($y = fn1::propval($item, "y")) ? fn1::toNum($y) : 0;
			$x2 = is_numeric($x2 = fn1::propval($item, "x2")) ? fn1::toNum($x2) : 0;
			$y2 = is_numeric($y2 = fn1::propval($item, "y2")) ? fn1::toNum($y2) : 0;
			$this -> Line($x, $y, $x2, $y2);
			break;

			case "LN":
			$h = is_numeric($h = fn1::propval($item, "h")) ? fn1::toNum($h) : null;
			$this -> Ln($h);
			break;
		}
	}
	public function setRGB($method, $rgb_array){
		if (method_exists($this, $method) && self::isRGB($rgb_array)){
			list($r, $g, $b) = $rgb_array;
			fn1::call([$this, $method], $r, $g, $b);
		}
	}
	public function setRGBColor($rgb_array_fill=null, $rgb_array_text=null, $rgb_array_draw=null){
		if (self::isRGB($rgb_array_fill)) $this -> setRGB("SetFillColor", $rgb_array_fill);
		if (self::isRGB($rgb_array_text)) $this -> setRGB("SetTextColor", $rgb_array_text);
		if (self::isRGB($rgb_array_draw)) $this -> setRGB("SetDrawColor", $rgb_array_draw);
	}
	public function mCell($txt, $w, $lines, $cell_height, $border=0, $align="L", $fill=false){
		if (!self::isBorder($border)) $border = 0;
		if (!self::isAlign($align, true)) $align = "L";
		$w = fn1::toNum($w);
		$lines = ($lines = fn1::toNum($lines)) > 0 ? $lines : 1;
		$cell_height = fn1::toNum($cell_height);
		$h = fn1::toNum($lines * $cell_height);
		$txt = fn1::toStrn($txt);
		$x = (int) $this -> GetX();
		$y = (int) $this -> GetY();
		$this -> Cell($w, $h, "", $border, 0, "L", !!$fill);
		$this -> SetXY($x, $y);
		$this -> MultiCell($w, $cell_height, $txt, 0, $align, false);
		$this -> SetXY($x + $w, $y);
		return $h;
	}
	public function getCurrentFont(){
		$current_font = &$this -> CurrentFont;
		return $current_font;
	}
	public function get($prop){
		$prop = fn1::toStrx($prop, true);
		$value = null;
		if (strlen($prop) && property_exists($this, $prop)) $value = &$this -> {$prop};
		return $value;
	}
	public function getString($is_utf8=false){
		return $this -> Output("S", "doc.pdf", $is_utf8);
	}
	public static function pageRotation($page_rotation){
		return ($page_rotation = fn1::toNum($page_rotation)) % 90 == 0 ? $page_rotation : self::PAGE_ROTATION;
	}
	public static function pageOrientation($page_orientation){
		return in_array($page_orientation, ["L", "P"]) ? $page_orientation : self::PAGE_ORIENTATION;
	}
	public static function pageSize($page_size){
		if (!in_array($page_size, ["A3", "A4", "A5", "Letter", "Legal"])){
			if (is_array($page_size) && count($page_size) == 2 && is_numeric($page_size[0]) && is_numeric($page_size[1])) $page_size = [fn1::toNum($page_size[0]), fn1::toNum($page_size[1])];
			else $page_size = self::PAGE_SIZE;
		}
		return $page_size;
	}
	public static function isFontStyle($val){
		return is_string($val) && (!strlen($val) || strlen($val) && !preg_match('/[^BIU]/s', $val));
	}
	public static function isBorder($val){
		return in_array($val, [0, 1]) || is_string($val) && strlen($val) && !preg_match('/[^LTRB]/s', $val);
	}
	public static function isAlign($val, $justify=false){
		$aligns = ["L", "C", "R"];
		if ($justify) $aligns[] = "J";
		return is_string($val) && in_array($val, $aligns);
	}
	public static function isRGB($val){
		return is_array($val) && count($val) == 3 && is_numeric($val[0]) && is_numeric($val[1]) && is_numeric($val[2]);
	}
	public static function isItem($val){
		return is_array($val) && count($val) > 1 && array_key_exists("item", $val);
	}
	public static function itemAddLink($key){
		return ["item" => "ADD_LINK", "key" => $key];
	}
	public static function itemImage($image, $x=null, $y=null, $width=null, $height=null, $type=null, $link=null, $page_align=null, $page_align_margin=10){
		return [
			"item" => "IMAGE",
			"image" => $image,
			"x" => $x,
			"y" => $y,
			"w" => $width,
			"h" => $height,
			"type" => $type,
			"link" => $link,
			"page_align" => $page_align,
			"page_align_margin" => $page_align_margin
		];
	}
	public static function itemFont($family=null, $size=null, $style=null){
		return ["item" => "FONT", "family" => $family, "size" => $size, "style" => $style];
	}
	public static function itemCell($text="", $width=0, $height=0, $border=0, $ln=0, $align="L", $fill=false, $link=null){
		return ["item" => "CELL", "w" => $width, "h" => $height, "text" => $text, "border" => $border, "ln" => $ln, "align" => $align, "fill" => $fill, "link" => $link];
	}
	public static function itemMultiCell($text, $width, $height, $border=0, $align="L", $fill=false){
		return ["item" => "MULTICELL", "w" => $width, "h" => $height, "text" => $text, "border" => $border, "align" => $align, "fill" => $fill];
	}
	public static function itemLn($height=null){
		return ["item" => "LN", "h" => $height];
	}
}

//PDFTable class
class PDFTable {
	public $draw_color_odd = null;
	public $draw_color_even = null;
	public $draw_color_header = [211, 227, 255];
	public $font_color_odd = null;
	public $font_color_even = null;
	public $font_color_header = null;
	public $background_color_odd = null;
	public $background_color_even = [247, 247, 247];
	public $background_color_header = [224, 235, 255];
	public $fill_row = true;
	public $fill_header = true;
	public $line_width_row = null;
	public $line_width_header = null;
	public $table_ln_space = 10;
	public $border_row = "LTRB"; //"LR"
	public $border_header = 1;
	public $align_row = "L";
	public $align_header = "C";
	public $font_row = null;
	public $font_header = null;
	public $font_size_row = null;
	public $font_size_header = null;
	public $line_width = null;
	public $cell_height_row = null;
	public $cell_height_header = null;
	public $cell_width_min = null;
	public $cell_width_max = null;
	public $has_columns = true;
	public $include_missing = true;
	public $wrap_column_cells = false;
	public $wrap_row_cells = true;
	public $column_widths = [];
	public $aligns_header = [];
	public $aligns_row = [];
	public $data = [];
	private $pdf = null;
	private $rows = [];
	private $columns = [];
	public function __construct($data=null, $pdf=null, $page_orientation=null, $page_size=null, $page_rotation=null){
		if ($pdf && $pdf instanceof PDF) $this -> pdf = $pdf;
		else $pdf = new PDF($page_orientation, $page_size, $page_rotation);
		if (!fn1::isEmpty($data = fn1::toArray($data))) $this -> data = $data;
	}
	public function set($prop, $value){
		$prop = fn1::toStrx($prop, true);
		if (strlen($prop) && property_exists($this, $prop)) $this -> {$prop} = $value;
		return $this;
	}
	public function get($prop){
		$prop = fn1::toStrx($prop, true);
		$value = null;
		if (strlen($prop) && property_exists($this, $prop)) $value = &$this -> {$prop};
		return $value;
	}
	public function pdf(){
		return $this -> pdf;
	}
	public function create(){
		$pdf = $this -> pdf;
		$data = fn1::toArray($this -> data);
		if (!($pdf instanceof PDF && count($data))) return $this;

		//init pdf
		if (!$pdf -> page_added) $pdf -> addPage();
		else $pdf -> init();

		//get vars
		$draw_color_odd = PDF::isRGB($draw_color_odd = $this -> draw_color_odd) ? $draw_color_odd : $pdf -> draw_color;
		$draw_color_even = PDF::isRGB($draw_color_even = $this -> draw_color_even) ? $draw_color_even : $pdf -> draw_color;
		$draw_color_header = PDF::isRGB($draw_color_header = $this -> draw_color_header) ? $draw_color_header : $pdf -> draw_color;
		$font_color_odd = PDF::isRGB($font_color_odd = $this -> font_color_odd) ? $font_color_odd : $pdf -> font_color;
		$font_color_even = PDF::isRGB($font_color_even = $this -> font_color_even) ? $font_color_even : $pdf -> font_color;
		$font_color_header = PDF::isRGB($font_color_header = $this -> font_color_header) ? $font_color_header : $pdf -> font_color;
		$background_color_odd = PDF::isRGB($background_color_odd = $this -> background_color_odd) ? $background_color_odd : $pdf -> background_color;
		$background_color_even = PDF::isRGB($background_color_even = $this -> background_color_even) ? $background_color_even : $pdf -> background_color;
		$background_color_header = PDF::isRGB($background_color_header = $this -> background_color_header) ? $background_color_header : $pdf -> background_color;
		$line_width_row = (($line_width_row = $this -> line_width_row) !== null) ? fn1::toNum($line_width_row) : $pdf -> line_width;
		$line_width_header = (($line_width_header = $this -> line_width_header) !== null) ? fn1::toNum($line_width_header) : $pdf -> line_width;
		$font_row = strlen($font_row = fn1::toStrx($this -> font_row, true)) ? $font_row : $pdf -> font;
		$font_header = strlen($font_header = fn1::toStrx($this -> font_header, true)) ? $font_header : $pdf -> font;
		$font_size_row = (($font_size_row = $this -> font_size_row) !== null) ? fn1::toNum($font_size_row) : $pdf -> font_size;
		$font_size_header = (($font_size_header = $this -> font_size_header) !== null) ? fn1::toNum($font_size_header) : $pdf -> font_size;
		$line_width = (($line_width = $this -> line_width) !== null) ? fn1::toNum($line_width) : $pdf -> line_width;
		$cell_height_row = (($cell_height_row = $this -> cell_height_row) !== null) ? fn1::toNum($cell_height_row) : $pdf -> cell_height;
		$cell_height_header = (($cell_height_header = $this -> cell_height_header) !== null) ? fn1::toNum($cell_height_header) : $pdf -> cell_height;
		$cell_width_min = (($cell_width_min = $this -> cell_width_min) !== null) ? fn1::toNum($cell_width_min) : $pdf -> cell_width_min;
		$cell_width_max = (($cell_width_max = $this -> cell_width_max) !== null) ? fn1::toNum($cell_width_max) : $pdf -> cell_width_max;
		$border_header = PDF::isBorder($border_header = $this -> border_header) ? $border_header : 1;
		$border_row = PDF::isBorder($border_row = $this -> border_row) ? $border_row : "LR";
		$align_row = PDF::isAlign($align_row = $this -> align_row) ? $align_row : "L";
		$align_header = PDF::isAlign($align_header = $this -> align_header) ? $align_header : "C";
		$fill_row = !!$this -> fill_row;
		$fill_header = !!$this -> fill_header;
		$wrap_column_cells = !!$this -> wrap_column_cells;
		$wrap_row_cells = !!$this -> wrap_row_cells;
		$include_missing = !!$this -> include_missing;
		$table_ln_space = fn1::toNum($this -> table_ln_space);
		$column_widths = fn1::toArray($this -> column_widths);
		$aligns_header = fn1::toArray($this -> aligns_header);
		$aligns_row = fn1::toArray($this -> aligns_row);

		//table vars
		$temp_cols = [];
		$temp_rows = [];
		$temp_widths = [];

		//table data
		$columns_data = $this -> has_columns && !fn1::isEmpty($temp = fn1::toArray($data[0])) ? $temp : [];
		$rows_data = count($data) > 1 ? array_slice($data, 1) : [];

		//calc columns
		if (count($columns_data)){
			$pdf -> setFont($font_header, "B", $font_size_header);
			for ($i = 0; $i < count($columns_data); $i ++){
				$txt = $columns_data[$i];
				$width_min = $cell_width_min;
				$width_max = $cell_width_max;
				if (isset($column_widths[$i])){
					$width_min = fn1::toNum($column_widths[$i]);
					$width_max = $width_min;
				}
				$cell_data = self::cellData($pdf, $txt, $width_min, $width_max);
				if (!$cell_data) continue;
				$temp_widths[$i] = $cell_data["w"];
				$temp_cols[$i] = $cell_data;
			}
		}

		//calc rows
		$update_cols = false;
		if (count($rows_data)){
			$pdf -> setFont($font_row, "", $font_size_row);
			for ($r = 0; $r < count($rows_data); $r ++){
				$row_data = $rows_data[$r];
				if (count($row_data)){
					$temp_row = [];
					for ($i = 0; $i < count($row_data); $i ++){
						$txt = $row_data[$i];
						$width_min = $cell_width_min;
						$width_max = $cell_width_max;
						if (isset($column_widths[$i])){
							$width_min = fn1::toNum($column_widths[$i]);
							$width_max = $width_min;
						}
						elseif (isset($temp_widths[$i])) $width_min = $temp_widths[$i];
						$cell_data = self::cellData($pdf, $txt, $width_min, $width_max);
						if (!$cell_data) continue;
						if (count($temp_cols) && !isset($temp_cols[$i]) && $include_missing){
							$temp_cols[$i] = ["txt" => "", "w" => $cell_data["w"]];
							if (!$update_cols) $update_cols = true;
						}
						if ($temp_widths[$i] < $cell_data["w"]){
							$temp_widths[$i] = $cell_data["w"];
							if (!$update_cols) $update_cols = true;
						}
						$temp_row[$i] = $cell_data;
					}
					if (count($temp_row)) $temp_rows[] = $temp_row;
				}
			}
		}

		//calc columns update
		if ($update_cols && count($temp_cols)){
			$temp = [];
			$pdf -> setFont($font_header, "B", $font_size_header);
			for ($i = 0; $i < count($temp_cols); $i ++){
				$txt = $temp_cols[$i]["txt"];
				$width_min = $cell_width_min;
				$width_max = $cell_width_max;
				if (isset($temp_widths[$i])){
					$width_min = $temp_widths[$i];
					$width_max = $width_min;
				}
				elseif (isset($column_widths[$i])){
					$width_min = fn1::toNum($column_widths[$i]);
					$width_max = $width_min;
				}
				$cell_data = self::cellData($pdf, $txt, $width_min, $width_max);
				if (!$cell_data) continue;
				$temp[$i] = $cell_data;
			}
			$temp_cols = $temp;
		}

		//calc table wraps
		$temp_pdf_width = ceil($pdf -> GetPageWidth() - 20);
		$temp_wrap = [];
		$temp_wraps = [];
		$temp_wrap_width = 0;
		for ($i = 0; $i < count($temp_widths); $i ++){
			if (($temp_wrap_width + $temp_widths[$i]) > $temp_pdf_width){
				$temp_wraps[] = $temp_wrap;
				$temp_wrap_width = 0;
				$temp_wrap = [];
			}
			$temp_wrap[$i] = $temp_widths[$i];
			$temp_wrap_width += $temp_widths[$i];
		}
		if (count($temp_wrap)) $temp_wraps[] = $temp_wrap;

		//create table with wraps
		for ($w = 0; $w < count($temp_wraps); $w ++){
			$temp_wrap = $temp_wraps[$w];
			$pdf -> Ln(10);
			if ($w){
				$pdf -> SetFont($font_header, "B", $font_size_header);
				$pdf -> setRGB("SetTextColor", [187, 187, 187]);
				$pdf -> Write(5, "Columns Cont...");
				$pdf -> Ln();
			}

			//create columns
			if (count($temp_cols)){
				$pdf -> SetLineWidth($line_width_header);
				$pdf -> SetFont($font_header, "B", $font_size_header);
				$pdf -> setRGBColor($background_color_header, $font_color_header, $draw_color_header);
				$nb = 0;
				foreach ($temp_wrap as $i => $width){
					if (isset($temp_cols[$i]) && array_key_exists("txt", ($item = $temp_cols[$i]))){
						$txt = $item["txt"];
						$nb = max($nb, self::NbLines($pdf, $txt, $width));
					}
				}
				$row_height = $cell_height_header * $nb;
				self::checkPageBreak($pdf, $row_height);
				foreach ($temp_wrap as $i => $width){
					if (isset($temp_cols[$i]) && array_key_exists("txt", ($item = $temp_cols[$i]))){
						$txt = $item["txt"];
						$align = $align_header;
						if (isset($aligns_header[$i]) && PDF::isAlign($aligns_header[$i])) $align = $aligns_header[$i];
						$pdf -> mCell($txt, $width, $nb, $cell_height_header, $border_header, $align, $fill_header);
					}
				}
				$pdf -> Ln($row_height);
			}

			//create rows
			if (count($temp_rows)){
				$pdf -> SetLineWidth($line_width_row);
				$pdf -> SetFont($font_row, "", $font_size_row);
				foreach ($temp_rows as $r => $temp_row){
					if (count($temp_row)){
						$color_bg = fn1::isEven($r + 1) ? $background_color_even : $background_color_odd;
						$color_font = fn1::isEven($r + 1) ? $font_color_even : $font_color_odd;
						$color_draw = fn1::isEven($r + 1) ? $draw_color_even : $draw_color_odd;
						$pdf -> setRGBColor($color_bg, $color_font, $color_draw);
						$nb = 0;
						foreach ($temp_wrap as $i => $width){
							if (isset($temp_row[$i]) && array_key_exists("txt", ($item = $temp_row[$i]))){
								$txt = $item["txt"];
								$nb = max($nb, self::NbLines($pdf, $txt, $width));
							}
						}
						$row_height = $cell_height_row * $nb;
						self::checkPageBreak($pdf, $row_height);
						foreach ($temp_wrap as $i => $width){
							if (isset($temp_row[$i]) && array_key_exists("txt", ($item = $temp_row[$i]))){
								$txt = $item["txt"];
								$align = $align_row;
								if (isset($aligns_row[$i]) && PDF::isAlign($aligns_row[$i])) $align = $aligns_row[$i];
								$pdf -> mCell($txt, $width, $nb, $cell_height_row, $border_row, $align, $fill_row);
							}
						}
						$pdf -> Ln($row_height);
					}
				}
			}
		}

		//return self
		return $this;
	}
	public static function checkPageBreak($pdf, $h){
		$h = fn1::toNum($h);
		if ($pdf instanceof PDF && $pdf -> GetY() + $h > $pdf -> get("PageBreakTrigger")) $pdf -> addPage(null, null, 0, false);
	}
	public static function NbLines($pdf, $txt, $w){
		$w = fn1::toNum($w);
		$txt = fn1::toStrn($txt);
		if ($pdf instanceof PDF){
			$cw = $pdf -> get("CurrentFont")["cw"];
			if ($w == 0) $w = $pdf -> get("w") - $pdf -> get("rMargin") - $pdf -> get("x");
			$wmax = ($w - 2 * $pdf -> get("cMargin")) * 1000 / $pdf -> get("FontSize");
			$s = str_replace("\r", "", $txt);
			$nb = strlen($s);
			if ($nb > 0 && $s[$nb - 1] == "\n") $nb --;
			$sep = -1;
			$i = 0; $j = 0; $l = 0; $nl = 1;
			while ($i < $nb){
				$c = $s[$i];
				if ($c == "\n"){
					$i ++;
					$sep = -1;
					$j = $i;
					$l = 0;
					$nl ++;
					continue;
				}
				if ($c == " ") $sep = $i;
				$l += $cw[$c];
				if ($l > $wmax){
					if ($sep == -1){
						if ($i == $j) $i ++;
					}
					else $i = $sep + 1;
					$sep = -1;
					$j = $i;
					$l = 0;
					$nl ++;
				}
				else $i ++;
			}
			return $nl;
		}
		return 0;
	}
	public static function cellData($pdf, $txt, $width_min, $width_max){
		if ($pdf instanceof PDF){
			$txt = fn1::toStrn($txt);
			$width_min = fn1::toNum($width_min);
			$width_max = fn1::toNum($width_max);
			$width = (int) ceil($pdf -> GetStringWidth($txt)) + 5;
			if ($width_min && $width < $width_min) $width = $width_min;
			elseif ($width_max && $width > $width_max) $width = $width_max;
			return ["txt" => $txt, "w" => $width];
		}
		return null;
	}
}
