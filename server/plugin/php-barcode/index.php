<?php
#load library
require_once __DIR__ . "/Exceptions/BarcodeException.php";
require_once __DIR__ . "/Exceptions/InvalidCharacterException.php";
require_once __DIR__ . "/Exceptions/InvalidCheckDigitException.php";
require_once __DIR__ . "/Exceptions/InvalidFormatException.php";
require_once __DIR__ . "/Exceptions/InvalidLengthException.php";
require_once __DIR__ . "/Exceptions/UnknownTypeException.php";
require_once __DIR__ . "/BarcodeGenerator.php";
require_once __DIR__ . "/BarcodeGeneratorHTML.php";
require_once __DIR__ . "/BarcodeGeneratorJPG.php";
require_once __DIR__ . "/BarcodeGeneratorPNG.php";
require_once __DIR__ . "/BarcodeGeneratorSVG.php";
