<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190420
*	PLUGINS INCLUDE
*/

namespace Naicode\Server\Plugin;
require_once __DIR__ . "/user.php";
require_once __DIR__ . "/audit.php";
require_once __DIR__ . "/files.php";
require_once __DIR__ . "/email-builder/index.php";
require_once __DIR__ . "/hybridauth/autoload.php";
require_once __DIR__ . "/pdf.php";
require_once __DIR__ . "/qr.php";
require_once __DIR__ . "/barcode.php";
require_once __DIR__ . "/request.php";

#USE EXAMPLES
use Naicode\Server\Plugin\User;
use Naicode\Server\Plugin\Audit;
use Naicode\Server\Plugin\Files;
use Naicode\Server\Plugin\EmailBuilder;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
use Naicode\Server\Plugin\PDF;
use Naicode\Server\Plugin\PDFTable;
use Naicode\Server\Plugin\PDFTableReport;
use function Naicode\Server\Plugin\CreateQRCode;
use function Naicode\Server\Plugin\CreateBarcode;
use Naicode\Server\Plugin\Request;
use function Naicode\Server\Plugin\bitly_shorten_link;
