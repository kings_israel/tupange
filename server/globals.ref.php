<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190330
*	DEFAULT GLOBALS DEFINITIONS (Reference)
*/

#host
if (!defined("NS_SERVER_ROOT")) define("NS_SERVER_ROOT", "/opt/lampp/htdocs/projects/_tupange");
if (!defined("NS_ROOT_DIR")) define("NS_ROOT_DIR", NS_SERVER_ROOT . "/");
if (!defined("NS_SITE_DIR")) define("NS_SITE_DIR", NS_SERVER_ROOT . "/");
if (!defined("NS_BASE")) define("NS_BASE", "http://localhost/");
if (!defined("NS_SITE")) define("NS_SITE", "http://localhost/");
if (!defined("NS_DOMAIN")) define("NS_DOMAIN", "example.com");
if (!defined("NS_HOSTNAME")) define("NS_HOSTNAME", "www.example.com");

#server
if (!defined("NS_ALLOW_HEADERS")) define("NS_ALLOW_HEADERS", [
	"Access-Control-Allow-Origin: *",
	"Access-Control-Allow-Methods: HEAD, GET, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE",
	"Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description",
]);
if (!defined("NS_TIMEZONE")) define("NS_TIMEZONE", "Africa/Nairobi");
if (!defined("NS_ERROR_REPORTING")) define("NS_ERROR_REPORTING", E_ALL & ~E_DEPRECATED);
if (!defined("NS_RESPONSE_MODE")) define("NS_RESPONSE_MODE", "json");

#main debug
if (!defined("NS_DEBUG_MODE")) define("NS_DEBUG_MODE", false);
if (!defined("NS_DEBUG_LEVEL")) define("NS_DEBUG_LEVEL", 2);
if (!defined("NS_DEBUG_LOG_FILE")) define("NS_DEBUG_LOG_FILE", "debug.log");
if (!defined("NS_DEBUG_OVERWRITE")) define("NS_DEBUG_OVERWRITE", false);

#mailer
if (!defined("NS_MAIL_FROM")) define("NS_MAIL_FROM", "no-reply@example.com");
if (!defined("NS_MAIL_FROM_NAME")) define("NS_MAIL_FROM_NAME", "Mailer");

#SMTP
if (!defined("NS_SMTP_ENABLED")) define("NS_SMTP_ENABLED", false);
if (!defined("NS_SMTP_HOST")) define("NS_SMTP_HOST", "");
if (!defined("NS_SMTP_PORT")) define("NS_SMTP_PORT", 25);
if (!defined("NS_SMTP_AUTH")) define("NS_SMTP_AUTH", true);
if (!defined("NS_SMTP_AUTH_TYPE")) define("NS_SMTP_AUTH_TYPE", "LOGIN");
if (!defined("NS_SMTP_USERNAME")) define("NS_SMTP_USERNAME", "");
if (!defined("NS_SMTP_PASSWORD")) define("NS_SMTP_PASSWORD", "");
if (!defined("NS_SMTP_SECURITY")) define("NS_SMTP_SECURITY", "ssl");
if (!defined("NS_SMTP_TIMEOUT_SEC")) define("NS_SMTP_TIMEOUT_SEC", 30);
if (!defined("NS_SMTP_TIMELIMIT_SEC")) define("NS_SMTP_TIMELIMIT_SEC", 120);

#database
if (!defined("NS_DATABASE_HOST")) define("NS_DATABASE_HOST", "localhost");
if (!defined("NS_DATABASE_USERNAME")) define("NS_DATABASE_USERNAME", "root");
if (!defined("NS_DATABASE_PASSWORD")) define("NS_DATABASE_PASSWORD", "local");
if (!defined("NS_DATABASE_NAME")) define("NS_DATABASE_NAME", "tupange");
if (!defined("NS_DATABASE_PORT")) define("NS_DATABASE_PORT", 3306);

#audit
if (!defined("NS_TABLE_AUDIT")) define("NS_TABLE_AUDIT", "ns_audit");

#user
if (!defined("NS_TABLE_USERS")) define("NS_TABLE_USERS", "ns_users");
if (!defined("NS_USER_PASSWORD_HASH")) define("NS_USER_PASSWORD_HASH", "mE9r1Gbl14SWen4K4tDvSrLeSJc8hPhuR3SkUByvDRzlvHb");
if (!defined("NS_USER_PASSWORD_MIN")) define("NS_USER_PASSWORD_MIN", 5);
if (!defined("NS_USER_USERLEVEL_MIN")) define("NS_USER_USERLEVEL_MIN", 1);
if (!defined("NS_USER_USERLEVEL_ADMIN")) define("NS_USER_USERLEVEL_ADMIN", 5);
if (!defined("NS_USER_EMAIL_FROM")) define("NS_USER_EMAIL_FROM", "info@" . NS_DOMAIN);
if (!defined("NS_USER_EMAIL_FROM_NAMES")) define("NS_USER_EMAIL_FROM_NAMES", "Accounts");
if (!defined("NS_USER_VERIFY_SUBJECT")) define("NS_USER_VERIFY_SUBJECT", "Verify Email");
if (!defined("NS_USER_VERIFY_HEADING")) define("NS_USER_VERIFY_HEADING", "Welcome {{names}}!");
if (!defined("NS_USER_VERIFY_MESSAGE")) define("NS_USER_VERIFY_MESSAGE", "Your account has been created successfully! Click the button below to verify your email address.");
if (!defined("NS_USER_VERIFY_LINK_TEXT")) define("NS_USER_VERIFY_LINK_TEXT", "VERIFY");
if (!defined("NS_USER_VERIFY_LINK")) define("NS_USER_VERIFY_LINK", NS_SITE . "/auth/verify/{{code}}");
if (!defined("NS_USER_RESET_SUBJECT")) define("NS_USER_RESET_SUBJECT", "Recover Password");
if (!defined("NS_USER_RESET_HEADING")) define("NS_USER_RESET_HEADING", "Hello {{names}}!");
if (!defined("NS_USER_RESET_MESSAGE")) define("NS_USER_RESET_MESSAGE", "A request was made to recover the password for your account. Click on the button below to set up a new password. If this was not requested by you, let us know by replying to this message.");
if (!defined("NS_USER_RESET_LINK_TEXT")) define("NS_USER_VERIFY_LINK_TEXT", "RESET PASSWORD");
if (!defined("NS_USER_RESET_LINK")) define("NS_USER_RESET_LINK", NS_SITE . "/auth/reset/{{code}}");

#files
if (!defined("NS_UPLOADS_FOLDER")) define("NS_UPLOADS_FOLDER", NS_SITE_DIR . "/uplaods");
if (!defined("NS_UPLOADS_TEMPLATE")) define("NS_UPLOADS_TEMPLATE", "{{uploads}}/{{random_string}}.{{ext}}");
if (!defined("NS_UPLOADS_MAX_SIZE")) define("NS_UPLOADS_MAX_SIZE", 5);
