<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190208
*	FUNCS ~ MAIN
*/

namespace Naicode\Server;
require_once __DIR__ . "/lang.php";
require_once __DIR__ . "/funcs.php";
require_once __DIR__ . "/globals.php";
use Naicode\Server\Lang as lg;
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\Main;

#defaults
if (!defined("NS_DOMAIN")) define("NS_DOMAIN", "example.com");
if (!defined("NS_HOSTNAME")) define("NS_HOSTNAME", "www.example.com");
if (!defined("NS_MAIL_FROM")) define("NS_MAIL_FROM", "no-reply@" . NS_DOMAIN);
if (!defined("NS_MAIL_FROM_NAME")) define("NS_MAIL_FROM_NAME", "Mailer");
if (!defined("NS_SMTP_ENABLED")) define("NS_SMTP_ENABLED", false);
if (!defined("NS_SMTP_HOST")) define("NS_SMTP_HOST", "");
if (!defined("NS_SMTP_PORT")) define("NS_SMTP_PORT", 25);
if (!defined("NS_SMTP_AUTH")) define("NS_SMTP_AUTH", true);
if (!defined("NS_SMTP_AUTH_TYPE")) define("NS_SMTP_AUTH_TYPE", "LOGIN");
if (!defined("NS_SMTP_USERNAME")) define("NS_SMTP_USERNAME", "");
if (!defined("NS_SMTP_PASSWORD")) define("NS_SMTP_PASSWORD", "");
if (!defined("NS_SMTP_SECURITY")) define("NS_SMTP_SECURITY", "ssl");
if (!defined("NS_SMTP_TIMEOUT_SEC")) define("NS_SMTP_TIMEOUT_SEC", 30);
if (!defined("NS_SMTP_TIMELIMIT_SEC")) define("NS_SMTP_TIMELIMIT_SEC", 120);

//Mailer PORTED From: https://github.com/PHPMailer/PHPMailer/blob/master/src/PHPMailer.php
class Mailer extends Main {
	const CHARSET_UTF8 = "utf-8";
	const CHARSET_ISO88591 = "iso-8859-1";
	const CONTENT_TYPE_PLAINTEXT = "text/plain";
    const CONTENT_TYPE_TEXT_CALENDAR = "text/calendar";
    const CONTENT_TYPE_TEXT_HTML = "text/html";
    const CONTENT_TYPE_MULTIPART_ALTERNATIVE = "multipart/alternative";
    const CONTENT_TYPE_MULTIPART_MIXED = "multipart/mixed";
    const CONTENT_TYPE_MULTIPART_RELATED = "multipart/related";
    const ENCODING_7BIT = "7bit";
    const ENCODING_8BIT = "8bit";
    const ENCODING_BASE64 = "base64";
    const ENCODING_BINARY = "binary";
    const ENCODING_QUOTED_PRINTABLE = 'quoted-printable';
	const MAX_LINE_LENGTH = 998;
    const STD_LINE_LENGTH = 76;
	static protected $br = PHP_EOL;
	public static $validator = "php";
	private $smtp = null;
	private $message_type = "";
	public $mail_header = "";
	public $mime_header = "";
	public $mime_body = "";
	private $errs = [];
	private $boundary = [];
	private $hasError = false;
	private $all_recipients = [];
	public $to = [];
	public $single_to = false;
	public $single_to_array = [];
	public $cc = [];
	public $bcc = [];
	public $replyTo = [];
	public $attachments = [];
	public $queue_recipients = [];
	public $queue_replyTo = [];
	public $queue_addresses = [];
	public $charSet = self::CHARSET_ISO88591;
	public $encoding = self::ENCODING_8BIT;
	public $contentType = self::CONTENT_TYPE_PLAINTEXT;
	public $confirmReadingTo = "";
	public $from = [NS_MAIL_FROM, NS_MAIL_FROM_NAME];
	public $hostname = NS_HOSTNAME;
	public $customHeader = null;
	public $priority = null;
	public $xmailer = "";
	public $id = "";
	public $uid = "";
	public $date = "";
	public $Ical = "";
	public $subject = "";
	public $body = "";
	public $altbody = "";
	public $isHTML = false;
	public $mailer = (!!NS_SMTP_ENABLED ? "smtp" : "mail");
	public $wordwrap = 0;
	public $allowEmpty = false;
	public $attachment_max_size = 5;
	public $smtp_host = NS_SMTP_HOST;
	public $smtp_port = NS_SMTP_PORT;
	public $smtp_auth = NS_SMTP_AUTH;
	public $smtp_auth_type = NS_SMTP_AUTH_TYPE;
	public $smtp_username = NS_SMTP_USERNAME;
	public $smtp_password = NS_SMTP_PASSWORD;
	public $smtp_secure = NS_SMTP_SECURITY;
	public $smtp_timeout = NS_SMTP_TIMEOUT_SEC;
	public $smtp_timelimit = NS_SMTP_TIMELIMIT_SEC;
	public $sign_key_file = "";
	public $sign_cert_file = "";
	public $sign_extracerts_file = "";
	public $sign_key_pass = "";
	public $lastMessageID = "";
	public $DKIM_selector = "";
    public $DKIM_identity = "";
    public $DKIM_passphrase = "";
    public $DKIM_domain = "";
    public $DKIM_copyHeaderFields = true;
    public $DKIM_extraHeaders = [];
    public $DKIM_private = "";
    public $DKIM_private_string = "";
	public $sendmail_path = "/usr/sbin/sendmail";
	public $useSendMailOptions = true;
	public function __construct($isHTML=true){
		parent::__construct();
		$this -> isHTML = !!$isHTML;
	}
	public function __destruct(){
		if ($this -> smtp !== null) $this -> smtp -> smtpClose();
		$this -> smtp = null;
	}
	public function isHTML($isHTML=true){
		$this -> isHTML = !!$isHTML;
		return $this;
	}
	public function setMailer($mailer="mail"){
		if (fn1::isEmpty($mailer = strtolower(fn1::toStrn($mailer, true))) || !in_array($mailer, ["sendmail", "qmail", "smtp", "mail"])) $mailer = "mail";
		$this -> mailer = $mailer;
		return $this;
	}
	public function setSender($address, $name=null){
		return $this -> setFrom($address, $name);
	}
	public function setFrom($address, $name=null){
		if ($this -> hasError) return $this;
		$this -> setAddress("from", $address, $name);
		return $this;
	}
	public function addTo($address, $name=null){
		if ($this -> hasError) return $this;
		$this -> setAddress("to", $address, $name);
		return $this;
	}
	public function addCC($address, $name=null){
		if ($this -> hasError) return $this;
		$this -> setAddress("cc", $address, $name);
		return $this;
	}
	public function addBCC($address, $name=null){
		if ($this -> hasError) return $this;
		$this -> setAddress("bcc", $address, $name);
		return $this;
	}
	public function addReplyTo($address, $name=null){
		if ($this -> hasError) return $this;
		$this -> setAddress("replyto", $address, $name);
		return $this;
	}
	public function queueAddresses($addresses){
		if ($this -> hasError) return $this;
		if (fn1::isEmpty($addresses = fn1::toArray($addresses, true))) return $this;
		$kinds = ["to", "cc", "bcc", "replyto", "reply-to"];
		foreach ($addresses as $key => $value){
			$name = null;
			$address = null;
			$kind = strtolower(fn1::toStrn($key, true));
			if (!in_array($kind, $kinds)) $kind = "to";
			if (is_array($value)){
				if (!count($value)) continue;
				else if (count($value) == 1) $address = $value[0];
				elseif (count($value) == 2){
					$address = $value[0];
					$name = $value[1];
				}
				elseif (count($value) > 2){
					$kind = $value[0];
					$address = $value[1];
					$name = $value[2];
				}
			}
			else $address = fn1::toStrn($value, true);
			$kind = strtolower(fn1::toStrn($kind, true));
			if (!in_array($kind, $kinds)) continue;
			if (!$this -> setAddress($kind, $address, $name)) break;
		}
		return $this;
	}
	public function queueRecipients($addresses){
		if ($this -> hasError) return $this;
		if (fn1::isEmpty($addresses = fn1::toArray($addresses, true))) return $this;
		$kinds = ["to", "cc", "bcc"];
		foreach ($addresses as $key => $value){
			$name = null;
			$address = null;
			$kind = strtolower(fn1::toStrn($key, true));
			if (!in_array($kind, $kinds)) $kind = "to";
			if (is_array($value)){
				if (!count($value)) continue;
				else if (count($value) == 1) $address = $value[0];
				elseif (count($value) == 2){
					$address = $value[0];
					$name = $value[1];
				}
				elseif (count($value) > 2){
					$kind = $value[0];
					$address = $value[1];
					$name = $value[2];
				}
			}
			else $address = fn1::toStrn($value, true);
			$kind = strtolower(fn1::toStrn($kind, true));
			if (!in_array($kind, $kinds)) continue;
			if (!$this -> setAddress($kind, $address, $name)) break;
		}
		return $this;
	}
	public function queueReplyTo($addresses){
		if ($this -> hasError) return $this;
		if (fn1::isEmpty($addresses = fn1::toArray($addresses, true))) return $this;
		$kinds = ["replyto", "reply-to"];
		foreach ($addresses as $key => $value){
			$name = null;
			$address = null;
			$kind = strtolower(fn1::toStrn($key, true));
			if (!in_array($kind, $kinds)) $kind = "replyto";
			if (is_array($value)){
				if (!count($value)) continue;
				else if (count($value) == 1) $address = $value[0];
				elseif (count($value) == 2){
					$address = $value[0];
					$name = $value[1];
				}
				elseif (count($value) > 2){
					$kind = $value[0];
					$address = $value[1];
					$name = $value[2];
				}
			}
			else $address = fn1::toStrn($value, true);
			$kind = strtolower(fn1::toStrn($kind, true));
			if (!in_array($kind, $kinds)) continue;
			if (!$this -> setAddress($kind, $address, $name)) break;
		}
		return $this;
	}
	public function setSubject($subject){
		if ($this -> hasError) return $this;
		$this -> subject = fn1::toStrn($subject, true);
		return $this;
	}
	public function setBody($body){
		if ($this -> hasError) return $this;
		$this -> body = fn1::toStr($body, true, true);
		return $this;
	}
	public function setMessage($body){
		return $this -> setBody($body);
	}
	public function addAttachment($path, $name=null, $disposition="attachment", $encoding=null){
		if ($this -> hasError) return $this;
		if ($encoding === null) $encoding = self::ENCODING_BASE64;
		if (!fn1::isEmpty($disposition = fn1::toStrn($disposition, true)) || in_array($disposition, ["attachment", "inline"])) $disposition = "attachment";
		if (($info = fn1::pathInfo($path, true)) === null){
			$this -> err(lg::str("MAILER_ATTACHMENT_GET_ERROR", $path));
			return $this;
		}
		$path = $info["path"];
		if (!(file_exists($path) && is_file($path) && is_readable($path))){
			$this -> err(lg::str("MAILER_ATTACHMENT_MISSING_ERROR", $path));
			return $this;
		}
		if (($size = $info["size"]) === null){
			$this -> err(lg::str("MAILER_ATTACHMENT_SIZE_ERROR", $path));
			return $this;
		}
		$size_mb = round(fn1::toNum($size) / (1024 ** 2));
		$size_max = fn1::toNum($this -> attachment_max_size);
		if ($size_mb > $size_max){
			$this -> err(lg::str("MAILER_ATTACHMENT_MAX_SIZE_ERROR", $size_mb, $size_max, $path));
			return $this;
		}
		$filename = $info["filename"];
		$name = fn1::isEmpty($name = fn1::toStrn($name)) ? $filename : $name;
		if (!fn1::isEmpty($ext = fn1::toStrn($info["ext"], true)) && strpos($name, ".$ext") === false) $name .= ".$ext";
		$mimeType = fn1::fileMimeType($path);
		$attachment = [
			0 => $path,
			1 => $filename,
			2 => $name,
			3 => $encoding,
			4 => $mimeType,
			5 => false,
			6 => $disposition,
			7 => $name,
		];
		$key = md5($path . 0 . $encoding . $disposition);
		if (!array_key_exists($key, $this -> attachments)) $this -> attachments[$key] = $attachment;
		return $this;
	}
	public function addAttachmentContent($content, $name=null, $mimeType="text/plain", $disposition="attachment", $isString=false, $cid=0, $encoding=null){
		if ($this -> hasError) return $this;
		if ($encoding === null) $encoding = self::ENCODING_BASE64;
		$name = fn1::toStrn($name, true);
		$mimeType = strtolower(fn1::toStrn($mimeType, true));
		if (!in_array($mimeType, fn1::$file_mimes)) $mimeType = "text/plain";
		if (fn1::isEmpty($disposition = fn1::toStrn($disposition, true)) || in_array($disposition, ["attachment", "inline"])) $disposition = "attachment";
		if (fn1::isEmpty($name = fn1::toStrn($name, true))){
			$ext = "";
			if (is_string($ext = array_search($mimeType, fn1::$file_mimes, true))) $ext = "." . $ext;
			$name = "Attachment" . $ext; $i = 0;
			$key = md5($name . (!!$isString ? 1 : 0) . $encoding . $disposition);
			while (array_key_exists($key, $this -> attachments)){
				$i ++; $name = "Attachment($i)" . $ext;
				$key = md5($name . (!!$isString ? 1 : 0) . $encoding . $disposition);
			}
		}
		$size = strlen($content);
		$size_mb = round($size / (1024 ** 2));
		$size_max = fn1::toNum($this -> attachment_max_size);
		if ($size_mb > $size_max){
			$this -> err(lg::str("MAILER_ATTACHMENT_NAME_MAX_SIZE_ERROR", $size_mb, $size_max, $name));
			return $this;
		}
		$attachment = [
			0 => $content,
			1 => $name,
			2 => $name,
			3 => $encoding,
			4 => $mimeType,
			5 => !!$isString,
			6 => $disposition,
			7 => $cid,
		];
		$key = md5($name . (!!$isString ? 1 : 0) . $encoding . $disposition);
		if (!array_key_exists($key, $this -> attachments)) $this -> attachments[$key] = $attachment;
		return $this;
	}
	public function send(){
		if ($this -> hasError) return false;
		try {
			if (!$this -> preSend()) return false;
			return $this -> postSend();
		}
		catch (\Exception $e){
			return $this -> err($e -> getMessage());
		}
	}
	private function err($msg){
		$this -> hasError = true;
		$this -> errs[] = $msg;
		$error = implode(";\r\n", $this -> errs) . ";";
		return $this -> error($error);
	}
	private function setAddress($kind, $address, $name=null){
		if ($this -> hasError) return false;
		if (!$this -> validateAddress($address = strtolower(fn1::toStrn($address, true)))) return $this -> err("Invalid mail address \"$address\"");
		$name = trim(preg_replace('/[\r\n]+/', "", fn1::toStrn($name, true)));
		$kind = strtolower(fn1::toStrn($kind, true));
		$data = [$address, $name];
		switch ($kind){
			case "to":
			if (!is_array($this -> to)) $this -> to = [];
			if (!array_key_exists($address, $this -> to)) $this -> to[$address] = $data;
			break;
			case "cc":
			if (!is_array($this -> cc)) $this -> cc = [];
			if (!array_key_exists($address, $this -> cc)) $this -> cc[$address] = $data;
			break;
			case "bcc":
			if (!is_array($this -> bcc)) $this -> bcc = [];
			if (!array_key_exists($address, $this -> bcc)) $this -> bcc[$address] = $data;
			break;
			case "replyto":
			case "reply-to":
			if (!is_array($this -> replyTo)) $this -> replyTo = [];
			if (!array_key_exists($address, $this -> replyTo)) $this -> replyTo[$address] = $data;
			break;
			case "from":
			case "sender":
			$this -> from = $data;
			break;
			default:
			return $this -> err(lg::str("MAILER_ADDRESS_KIND_ERROR", $kind));
		}
		return $data;
	}
	private function generateId(){
		$len = 32;
		if (function_exists("random_bytes")) $bytes = random_bytes($len);
		elseif (function_exists("openssl_random_pseudo_bytes")) $bytes = openssl_random_pseudo_bytes($len);
		else $bytes = hash("sha256", uniqid((string) mt_rand(), true, true));
		return str_replace(["=", "+", "/"], "", base64_encode(hash("sha256", $bytes, true)));
	}
	private function wrapText($text, $length, $qp_mode=false){
		$soft_break = $qp_mode ? sprintf(" =%s", self::$br) : self::$br;
		$is_utf8 = strtolower(fn1::toStrn($this -> charSet)) === self::CHARSET_UTF8;
		$blen = strlen(self::$br);
		$clen = $blen;
		$text = self::normalizeBreaks($text);
		if (substr($text, -$blen) == self::$br) $text = substr($text, 0, -$blen);
		$lines = explode(self::$br, $text);
		$text = "";
		foreach ($lines as $line){
			$words = explode(" ", $line);
			$buffer = "";
			$first_word = true;
			foreach ($words as $word){
				if ($qp_mode && (strlen($word) > $length)){
					$space_left = $length - strlen($buffer) - $clen;
					if (!$first_word){
						if ($space_left > 20){
							$len = $space_left;
							if ($is_utf8) $len = $this -> utf8CharBoundary($word, $len);
							elseif (substr($word, $len - 1, 1) == "=") $len --;
							elseif (substr($word, $len - 2, 1) == "=") $len -= 2;
							$part = substr($word, 0, $len);
							$word = substr($word, $len);
							$buffer .= " " . $part;
							$text .= $buffer . sprintf("=%s", self::$br);
						}
						else $text .= $buffer . $soft_break;
						$buffer = "";
					}
					while (strlen($word) > 0){
						if ($length <= 0) break;
						$len = $length;
						if ($is_utf8) $len = $this -> utf8CharBoundary($word, $len);
						elseif (substr($word, $len - 1, 1) == "=") $len --;
						elseif (substr($word, $len - 2, 1) == "=") $len -= 2;
						$part = substr($word, 0, $len);
						$word = substr($word, $len);
						if (strlen($word) > 0) $text .= $part . sprintf("=%s", self::$br);
						else $buffer = $part;
					}
				}
				else {
					$buffer_o = $buffer;
					if (!$first_word) $buffer .= " ";
					$buffer .= $word;
					if (strlen($buffer) > $length && $buffer_o != ""){
						$text .= $buffer_o . $soft_break;
						$buffer .= $word;
					}
				}
				$first_word = false;
			}
			$text .= $buffer . self::$br;
		}
		return $text;
	}
	private function utf8CharBoundary($encoded, $maxlen){
		$found = false;
		$lookback = 3;
		while (!$found){
			$last_chunk = substr($encoded, $maxlen - $lookback, $lookback);
			if (($encoded_pos = strpos($last_chunk, "=")) !== false){
				$hex = substr($encoded, $maxlen - $lookback + $encoded_pos + 1, 2);
				$dec = hexdec($hex);
				if ($dec < 128){
					if ($encoded_pos > 0) $maxlen -= $lookback - $encoded_pos;
					$found = true;
				}
				elseif ($dec >= 192){
					$maxlen -= $lookback - $encoded_pos;
					$found = true;
				}
				elseif ($dec < 192) $lookback += 3;
			}
			else $found = true;
		}
		return $maxlen;
	}
	private function getMessageType(){
		if ($this -> message_type != "") return $this -> message_type;
		$type = [];
		if ($this -> hasAlternative()) $type[] = "alt";
		if ($this -> hasInline()) $type[] = "inline";
		if ($this -> hasAttachment()) $type[] = "attach";
		if (($type = implode("_", $type)) == "") $type = "plain";
		return ($this -> message_type = $type);
	}
	private function hasAlternative(){
		return !empty($this -> altbody);
	}
	private function hasInline(){
		foreach ($this -> attachments as $key => $attachment) if ($attachment[6] == "inline") return true;
		return false;
	}
	private function hasAttachment(){
		foreach ($this -> attachments as $key => $attachment) if ($attachment[6] == "attachment") return true;
		return false;
	}
	private function has8bitChars($text){
		return (bool) preg_match('/[\x80-\xFF]/', $text);
	}
	private function hasMultiBytes($text){
        if (function_exists('mb_strlen')) return strlen($text) > mb_strlen($text, $this -> charSet);
        return false;
    }
	private function headerLine($name, $value){
		return $name . ": " . $value . self::$br;
	}
	private function textLine($value){
		return $value . self::$br;
	}
	private function getMailMIME(){
		$result = "";
		$multipart = true;
		switch ($this -> getMessageType()){
			case "inline":
			$result .= $this -> headerLine("Content-Type", self::CONTENT_TYPE_MULTIPART_RELATED . ";");
			$result .= $this -> textLine("\tboundary=\"" . $this -> boundary[1] . "\"");
			break;
			case "attach":
			case "inline_attach":
			case "alt_attach":
			case "alt_inline_attach":
			$result .= $this -> headerLine("Content-Type", self::CONTENT_TYPE_MULTIPART_MIXED . ";");
			$result .= $this -> textLine("\tboundary=\"" . $this -> boundary[1] . "\"");
			break;
			case "alt":
			case "alt_inline":
			$result .= $this -> headerLine("Content-Type", self::CONTENT_TYPE_MULTIPART_ALTERNATIVE . ";");
			$result .= $this -> textLine("\tboundary=\"" . $this -> boundary[1] . "\"");
			break;
			default:
			$result .= $this -> textLine("Content-Type: " . $this -> contentType . "; charset=" . $this -> charSet);
			$multipart = false;
			break;
		}
		if ($this -> encoding != self::ENCODING_7BIT){
			if ($multipart){
				if ($this -> encoding == self::ENCODING_8BIT) $result .= $this -> headerLine("Content-Transfer-Encoding", $this -> encoding);
			}
			else $result .= $this -> headerLine("Content-Transfer-Encoding", $this -> encoding);
		}
		if ($this -> mailer != "mail") $result .= self::$br;
		return $result;
	}
	private function setWordWrap(){
		if ($this -> wordwrap < 1) return $this;
		switch ($this -> getMessageType()){
			case "alt":
            case "alt_inline":
            case "alt_attach":
            case "alt_inline_attach":
			$this -> altbody = $this -> wrapText($this -> altbody, $this -> wordwrap);
			break;
			default:
			$this -> body = $this -> wrapText($this -> body, $this -> wordwrap);
			break;
		}
		return $this;
	}
	private function getBoundary($boundary, $charSet="", $contentType="", $encoding=""){
		$result = "";
		if ($charSet == "") $charSet = $this -> charSet;
		if ($contentType == "") $contentType = $this -> contentType;
		if ($encoding == "") $encoding = $this -> encoding;
		$result .= $this -> textLine("--" . $boundary);
		$result .= sprintf("Content-Type: %s; charset=%s", $contentType, $charSet);
		$result .= self::$br;
		if ($encoding != self::ENCODING_7BIT) $result .= $this -> headerLine("Content-Transfer-Encoding", $encoding);
		$result .= self::$br;
		return $result;
	}
	private function endBoundary($boundary){
		return self::$br . "--" . $boundary . "--" . self::$br;
	}
	private function encodeQP($str){
		return self::normalizeBreaks(quoted_printable_encode($str));
	}
	private function encodeQ($str, $position="text"){
		$pattern = "";
		$encoded = str_replace(["\r", "\n"], "", $str);
		switch (strtolower($position)){
			case "phrase":
			$pattern = "^A-Za-z0-9!*+\/ -";
			break;
			case "comment":
			$pattern = '\(\)"';
			case "text":
			default:
			$pattern = '\000-\011\013\014\016-\037\075\077\137\177-\377' . $pattern;
			break;
		}
		$matches = [];
		if (preg_match_all("/[{$pattern}]/", $encoded, $matches)){
			$eqkey = array_search("=", $matches[0]);
			if ($eqkey !== false){
				unset($matches[0][$eqkey]);
				array_unshift($matches[0], "=");
			}
			foreach (array_unique($matches[0]) as $char) $encoded = str_replace($char, "=" . sprintf("%02X", ord($char)), $encoded);
		}
		return str_replace(" ", "_", $encoded);
	}
	private function base64EncodeWrapMB($str, $linebreak=null){
		$start = "=?" . $this -> charSet . "?B?";
		$end = "?=";
		$encoded = "";
		if ($linebreak === null) $linebreak = self::$br;
		$mb_length = mb_strlen($str, $this -> charSet);
		$length = 75 - strlen($start) - strlen($end);
		$ratio = $mb_length / strlen($str);
		$avg_length = floor($length * $ratio * .75);
		for ($i = 0; $i < $mb_length; $i += $offset){
			$lookback = 0;
			do {
				$offset = $avg_length - $lookback;
				$chunk = mb_substr($str, $i, $offset, $this -> charSet);
				$chunk = base64_encode($chunk);
				$lookback ++;
			}
			while (strlen($chunk) > $length);
			$encoded .= $chunk . $linebreak;
		}
		return substr($encoded, 0, -strlen($linebreak));
	}
	private function secureHeader($str){
		return trim(str_replace(["\r", "\n"], "", $str));
	}
	private function encodeHeader($str, $position="text"){
		$matchcount = 0;
		switch (strtolower($position)){
			case "phrase":
			if (!preg_match('/[\200-\377]/', $str)){
				$encoded = addcslashes($str, "\0..\37\177\\\"");
				if ($encoded == $str && !preg_match('/[^A-Za-z0-9!#$%&\'*+\/=?^_`{|}~ -]/', $str)) return $encoded;
				return "\"$encoded\"";
			}
			$matchcount = preg_match_all('/[^\040\041\043-\133\135-\176]/', $str, $matches);
			break;
			case "comment":
			$matchcount = preg_match_all('/[()"]/', $str, $matches);
			break;
			case "text":
			default:
			$matchcount += preg_match_all('/[\000-\010\013\014\016-\037\177-\377]/', $str, $matches);
			break;
		}
		$lengthsub = $this -> mailer == "mail" ? 13 : 0;
		$maxlen = self::STD_LINE_LENGTH - $lengthsub;
		if ($matchcount > strlen($str) / 3){
			$encoding = "B";
			$maxlen = self::STD_LINE_LENGTH - $lengthsub - 8 - strlen($this -> charSet);
			if ($this -> hasMultiBytes($str)) $encoded = $this -> base64EncodeWrapMB($str, "\n");
			else {
				$encoded = base64_encode($str);
				$maxlen -= $maxlen % 4;
				$encoded = trim(chunk_split($encoded, $maxlen, "\n"));
			}
			$encoded = preg_replace('/^(.*)$/m', " =?" . $this -> charSet . "?$encoding?\\1?=", $encoded);
		}
		elseif ($matchcount > 0){
			$encoding = "Q";
			$maxlen = self::STD_LINE_LENGTH - $lengthsub - 8 - strlen($this -> charSet);
			$encoded = $this -> encodeQ($str, $position);
			$encoded = $this -> wrapText($encoded, $maxlen, true);
			$encoded = str_replace("=" . self::$br, "\n", trim($encoded));
			$encoded = preg_replace('/^(.*)$/m', " =?" . $this -> charSet . "?$encoding?\\1?=", $encoded);
		}
		elseif (strlen($str) > $maxlen){
			$encoded = trim($this -> wrapText($str, $maxlen, false));
			if ($str == $encoded) $encoded = trim(chunk_split($str, self::STD_LINE_LENGTH, self::$br));
			$encoded = str_replace(self::$br, "\n", trim($encoded));
			$encoded = preg_replace('/^(.*)$/m', " \\1", $encoded);
		}
		else return $str;
		return trim(self::normalizeBreaks($encoded));
	}
	private function encodeFile($path, $encoding=null){
		if ($encoding === null) $encoding = self::ENCODING_BASE64;
		try {
			if (!self::isPermittedPath($path) || !file_exists($path)) throw new \Exception(lg::str("MAILER_EXCEPTION_FILE_OPEN", $path), self::STOP_CONTINUE);
			if (($file_buffer = file_get_contents($path)) === false) throw new \Exception(lg::str("MAILER_EXCEPTION_FILE_READ", $path), self::STOP_CONTINUE);
			$file_buffer = $this -> encodeString($file_buffer, $encoding);
			return $file_buffer;
		}
		catch (\Exception $e){
			$this -> err($e -> getMessage());
			return "";
		}
	}
	private function encodeString($str, $encoding=null){
		if ($encoding === null) $encoding = self::ENCODING_BASE64;
		$encoded = "";
		switch (strtolower($encoding)){
			case self::ENCODING_BASE64:
			$encoded = chunk_split(base64_encode($str), self::STD_LINE_LENGTH, self::$br);
			break;
			case self::ENCODING_7BIT:
			case self::ENCODING_8BIT:
			$encoded = self::normalizeBreaks($str);
			if (substr($encoded, -(strlen(self::$br))) != self::$br) $encoded .= self::$br;
			break;
			case self::ENCODING_BINARY:
			$encoded = $str;
			break;
			case self::ENCODING_QUOTED_PRINTABLE:
			$encoded = $this -> encodeQP($str);
			break;
			default:
			$this -> err(lg::str("MAILER_ENCODING_UNKNOWN_ERROR", $encoding));
			break;
		}
		return $encoded;
	}
	private function attachAll($disposition_type, $boundary){
		$mime = [];
		$cid_unique = [];
		foreach ($this -> attachments as $key => $attachment){
			if (($disposition = $attachment[6]) == $disposition_type){
				$path = "";
				$string = "";
				$bstring = $attachment[5];
				if ($bstring) $string = $attachment[0];
				else $path = $attachment[0];
				$name = $attachment[2];
				$encoding = $attachment[3];
				$mimeType = $attachment[4];
				$cid = $attachment[7];
				if ($disposition == "inline" && array_key_exists($cid, $cid_unique)) continue;
				$cid_unique[$cid] = true;
				$mime[] = sprintf("--%s%s", $boundary, self::$br);
				if (!empty($name)) $mime[] = sprintf('Content-Type: %s; name="%s"%s', $mimeType, $this -> encodeHeader($this -> secureHeader($name)), self::$br);
				else $mime[] = sprintf("Content-Type: %s%s", $mimeType, self::$br);
				if ($encoding != self::ENCODING_7BIT) $mime[] = sprintf("Content-Transfer-Encoding: %s%s", $encoding, self::$br);
				if (!empty($cid)) $mime[] = sprintf("Content-ID: <%s>%s", $cid, self::$br);
				if (!empty($disposition)){
					$encoded_name = $this -> encodeHeader($this -> secureHeader($name));
					if (preg_match('/[ \(\)<>@,;:\\"\/\[\]\?=]/', $encoded_name)) $mime[] = sprintf('Content-Disposition: %s; filename="%s"%s', $disposition, $encoded_name, self::$br . self::$br);
					else {
						if (!empty($encoded_name)) $mime[] = sprintf("Content-Disposition: %s; filename=%s%s", $disposition, $encoded_name, self::$br . self::$br);
						else $mime[] = sprintf("Content-Disposition: %s%s", $disposition, self::$br . self::$br);
					}
				}
				else $mime[] = self::$br;
				if ($bstring) $mime[] = $this -> encodeString($string, $encoding);
				else $mime[] = $this -> encodeFile($path, $encoding);
				if ($this -> hasError) return "";
				$mime[] = self::$br;
			}
		}
		$mime[] = sprintf("--%s--%s", $boundary, self::$br);
		return implode("", $mime);
	}
	private function createBody(){
		if ($this -> hasError) return false;
		$body = "";
		$this -> uid = $this -> generateId();
		$this -> boundary[1] = "b1_" . $this -> uid;
		$this -> boundary[2] = "b2_" . $this -> uid;
		$this -> boundary[3] = "b3_" . $this -> uid;
		if ($this -> sign_key_file) $body .= $this -> getMailMIME() . self::$br;
		$this -> setWordWrap();
		$bodyEncoding = $this -> encoding;
		$bodyCharSet = $this -> charSet;
		if ($bodyEncoding == self::ENCODING_8BIT && !$this -> has8bitChars($this -> body)){
			$bodyEncoding = self::ENCODING_7BIT;
			$bodyCharSet = "us-ascii";
		}
		if ($this -> encoding != self::ENCODING_BASE64 && self::hasLineLongerThanMax($this -> body)) $bodyEncoding = self::ENCODING_QUOTED_PRINTABLE;
		$altBodyEncoding = $this -> encoding;
		$altBodyCharSet = $this -> charSet;
		if ($altBodyEncoding == self::ENCODING_8BIT && !$this -> has8bitChars($this -> altbody)){
			$altBodyEncoding = self::ENCODING_7BIT;
			$altBodyCharSet = "us-ascii";
		}
		if ($altBodyEncoding != self::ENCODING_BASE64 && self::hasLineLongerThanMax($this -> altbody)) $altBodyEncoding = self::ENCODING_QUOTED_PRINTABLE;
		$mimepre = lg::str("MAILER_MIMEPRE") . self::$br;
		switch ($this -> getMessageType()){
			case "inline":
			$body .= $mimepre;
			$body .= $this -> getBoundary($this -> boundary[1], $bodyCharSet, "", $bodyEncoding);
			$body .= $this -> encodeString($this -> body, $bodyEncoding);
			$body .= self::$br;
			$body .= $this -> attachAll("inline", $this -> boundary[1]);
			break;
			case "attach":
			$body .= $mimepre;
			$body .= $this -> getBoundary($this -> boundary[1], $bodyCharSet, "", $bodyEncoding);
			$body .= $this -> encodeString($this -> body, $bodyEncoding);
			$body .= self::$br;
			$body .= $this -> attachAll("attachment", $this -> boundary[1]);
			break;
			case "inline_attach":
			$body .= $mimepre;
			$body .= $this -> textLine("--" . $this -> boundary[1]);
			$body .= $this -> headerLine("Content-Type", self::CONTENT_TYPE_MULTIPART_RELATED . ";");
			$body .= $this -> textLine("\tboundary\"" . $this -> boundary[2] . '"');
			$body .= self::$br;
			$body .= $this -> getBoundary($this -> boundary[2], $bodyCharSet, "", $bodyEncoding);
			$body .= $this -> encodeString($this -> body, $bodyEncoding);
			$body .= self::$br;
			$body .= $this -> attachAll("inline", $this -> boundary[2]);
			$body .= self::$br;
			$body .= $this -> attachAll("attachment", $this -> boundary[1]);
			break;
			case "alt":
			$body .= $mimepre;
			$body .= $this -> getBoundary($this -> boundary[1], $altBodyCharSet, self::CONTENT_TYPE_PLAINTEXT, $altBodyEncoding);
			$body .= $this -> encodeString($this -> altbody, $altBodyEncoding);
			$body .= self::$br;
			$body .= $this -> getBoundary($this -> boundary[1], $bodyCharSet, self::CONTENT_TYPE_TEXT_HTML, $bodyEncoding);
			$body .= $this -> encodeString($this -> body, $bodyEncoding);
			$body .= self::$br;
			if (!empty($this -> Ical)){
				$body .= $this -> getBoundary($this -> boundary[1], "", self::CONTENT_TYPE_TEXT_CALENDAR . "; method=REQUEST", "");
				$body .= $this -> encodeString($this -> Ical, $this -> encoding);
				$body .= self::$br;
			}
			$body .= $this -> endBoundary($this -> boundary[1]);
			break;
			case "alt_inline":
			$body .= $mimepre;
			$body .= $this -> getBoundary($this -> boundary[1], $altBodyCharSet, self::CONTENT_TYPE_PLAINTEXT, $altBodyEncoding);
			$body .= $this -> encodeString($this -> altbody, $altBodyEncoding);
			$body .= self::$br;
			$body .= $this -> textLine("--" . $this -> boundary[1]);
			$body .= $this -> headerLine("Content-Type", self::CONTENT_TYPE_MULTIPART_RELATED . ";");
			$body .= $this -> textLine("\tboundary=\"" . $this -> boundary[2] . '"');
			$body .= self::$br;
			$body .= $this -> getBoundary($this -> boundary[2], $bodyCharSet, self::CONTENT_TYPE_TEXT_HTML, $bodyEncoding);
			$body .= $this -> encodeString($this -> body, $bodyEncoding);
			$body .= self::$br;
			$body .= $this -> attachAll("inline", $this -> boundary[2]);
			$body .= self::$br;
			$body .= $this -> endBoundary($this -> boundary[1]);
			break;
			case "alt_attach":
			$body .= $mimepre;
			$body .= $this -> textLine("--" . $this -> boundary[1]);
			$body .= $this -> headerLine("Content-Type", self::CONTENT_TYPE_MULTIPART_ALTERNATIVE . ";");
			$body .= $this -> textLine("\tboundary=\"" . $this -> boundary[2] . '"');
			$body .= self::$br;
			$body .= $this -> getBoundary($this -> boundary[2], $altBodyCharSet, self::CONTENT_TYPE_PLAINTEXT, $altBodyEncoding);
			$body .= $this -> encodeString($this -> altbody, $altBodyEncoding);
			$body .= self::$br;
			$body .= $this -> getBoundary($this -> boundary[2], $bodyCharSet, self::CONTENT_TYPE_TEXT_HTML, $bodyEncoding);
			$body .= $this -> encodeString($this -> body, $bodyEncoding);
			$body .= self::$br;
			if (!empty($this -> Ical)){
				$body .= $this -> getBoundary($this -> boundary[2], "", self::CONTENT_TYPE_TEXT_CALENDAR . "; method=REQUEST", "");
				$body .= $this -> encodeString($this -> Ical, $this -> encoding);
			}
			$body .= $this -> endBoundary($this -> boundary[2]);
			$body .= self::$br;
			$body .= $this -> attachAll("attachment", $this -> boundary[1]);
			break;
			case "alt_inline_attach":
			$body .= $mimepre;
			$body .= $this -> textLine("--" . $this -> boundary[1]);
			$body .= $this -> headerLine("Content-Type", self::CONTENT_TYPE_MULTIPART_ALTERNATIVE . ";");
			$body .= $this -> textLine("\tboundary=\"" . $this -> boundary[2] . '"');
			$body .= self::$br;
			$body .= $this -> getBoundary($this -> boundary[2], $altBodyCharSet, self::CONTENT_TYPE_PLAINTEXT, $altBodyEncoding);
			$body .= $this -> encodeString($this -> altbody, $altBodyEncoding);
			$body .= self::$br;
			$body .= $this -> textLine("--" . $this -> boundary[2]);
			$body .= $this -> headerLine("Content-Type", self::CONTENT_TYPE_MULTIPART_RELATED . ";");
			$body .= $this -> textLine("\tboundary=\"" . $this -> boundary[3] . '"');
			$body .= self::$br;
			$body .= $this -> getBoundary($this -> boundary[3], $bodyCharSet, self::CONTENT_TYPE_TEXT_HTML, $bodyEncoding);
			$body .= $this -> encodeString($this -> body, $bodyEncoding);
			$body .= self::$br;
			$body .= $this -> attachAll("inline", $this -> boundary[3]);
			$body .= self::$br;
			$body .= $this -> endBoundary($this -> boundary[2]);
			$body .= self::$br;
			$body .= $this -> attachAll("attachment", $this -> boundary[1]);
			break;
			default:
			$this -> encoding = $bodyEncoding;
			$body .= $this -> encodeString($this -> body, $this -> encoding);
			break;
		}
		if ($this -> hasError){
			$body = null;
			return false;
		}
		if ($this -> sign_key_file){
			try {
				if (!defined("PKCS7_TEXT")) throw new \Exception(lg::str("MAILER_EXCEPTION_MISSING_EXTENSION", "openssl"));
				$file = fopen("php://temp", "rb+");
				$signed = fopen("php://temp", "rb+");
				fwrite($file, $body);
				if (empty($this -> sign_extracerts_file)){
					$sign = @openssl_pkcs7_sign(
						$file,
						$signed,
						"file://" . realpath($this -> sign_cert_file),
						["file://" . realpath($this -> sign_key_file), $this -> sign_key_pass],
						[]
					);
				}
				else {
					$sign = @openssl_pkcs7_sign(
						$file,
						$signed,
						"file://" . realpath($this -> sign_cert_file),
						["file://" . realpath($this -> sign_key_file), $this -> sign_key_pass],
						[],
						PKCS7_DETACHED,
						$this -> sign_extracerts_file
					);
				}
				fclose($file);
				if ($sign){
					$body = file_get_contents($signed);
					fclose($signed);
					$parts = explode("\n\n", $body, 2);
					$this -> mime_header .= $parts[0] . self::$br . self::$br;
					$body = $parts[1];
				}
				else {
					fclose($signed);
					throw new \Exception(lg::str("MAILER_EXCEPTION_SIGNING", openssl_error_string()));
				}
			}
			catch (\Exception $e){
				$body = null;
				return $this -> err($e -> getMessage());
			}
		}
		return $body;
	}
	private function validateAddress($address, $patternselect=null){
		if ($patternselect === null) $patternselect = self::$validator;
		if (is_callable($patternselect)) return call_user_func($patternselect, $address);
		if (strpos($address, "\n") !== false || strpos($address, "\r") !== false) return false;
		switch ($patternselect){
			case "pcre":
			case "pcre8":
			return (bool) preg_match(
				'/^(?!(?>(?1)"?(?>\\\[ -~]|[^"])"?(?1)){255,})(?!(?>(?1)"?(?>\\\[ -~]|[^"])"?(?1)){65,}@)' .
				'((?>(?>(?>((?>(?>(?>\x0D\x0A)?[\t ])+|(?>[\t ]*\x0D\x0A)?[\t ]+)?)(\((?>(?2)' .
				'(?>[\x01-\x08\x0B\x0C\x0E-\'*-\[\]-\x7F]|\\\[\x00-\x7F]|(?3)))*(?2)\)))+(?2))|(?2))?)' .
				'([!#-\'*+\/-9=?^-~-]+|"(?>(?2)(?>[\x01-\x08\x0B\x0C\x0E-!#-\[\]-\x7F]|\\\[\x00-\x7F]))*' .
				'(?2)")(?>(?1)\.(?1)(?4))*(?1)@(?!(?1)[a-z0-9-]{64,})(?1)(?>([a-z0-9](?>[a-z0-9-]*[a-z0-9])?)' .
				'(?>(?1)\.(?!(?1)[a-z0-9-]{64,})(?1)(?5)){0,126}|\[(?:(?>IPv6:(?>([a-f0-9]{1,4})(?>:(?6)){7}' .
				'|(?!(?:.*[a-f0-9][:\]]){8,})((?6)(?>:(?6)){0,6})?::(?7)?))|(?>(?>IPv6:(?>(?6)(?>:(?6)){5}:' .
				'|(?!(?:.*[a-f0-9]:){6,})(?8)?::(?>((?6)(?>:(?6)){0,4}):)?))?(25[0-5]|2[0-4][0-9]|1[0-9]{2}' .
				'|[1-9]?[0-9])(?>\.(?9)){3}))\])(?1)$/isD',
				$address
			);
			case "html5":
			return (bool) preg_match(
				'/^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}' .
				'[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/sD',
				$address
			);
			case "php":
			default:
			return (bool) filter_var($address, FILTER_VALIDATE_EMAIL);
		}
	}
	private function punyencodeAddress($address){
		if (($pos = strrpos($address, "@")) !== false && self::idnSupported() && !empty($this -> charSet)){
			$domain = substr($address, $pos + 1);
			if ($this -> has8bitChars($domain) && @mb_check_encoding($domain, $this -> charSet)){
				$domain = mb_convert_encoding($domain, "UTF-8", $this -> charSet);
				$error_code = 0;
				if (($puny_code = idn_to_ascii($domain, $error_code, INTL_IDNA_VARIANT_UTS46)) !== false) return substr($address, 0, $pos) . $puny_code;
			}
		}
		return $address;
	}
	private function addrAppend($type, $addr){
		$addresses = [];
		foreach (fn1::toArray($addr, true) as $address) $addresses[] = $this -> addrFormat($address);
		return $type . ": " . implode(", ", $addresses) . self::$br;
	}
	private function addrFormat($addr){
		if (count($addr) > 1 && empty($addr[1])) return $this -> secureHeader($addr[0]);
		return $this -> encodeHeader($this -> secureHeader($addr[1]), "phrase") . "<" . $this -> secureHeader($addr[0]) . ">";
	}
	private function createHeader(){
		if ($this -> hasError) return false;
		$result = "";
		$result .= $this -> headerLine("Date", $this -> date == "" ? self::rfcDate() : $this -> date);
		if ($this -> single_to){
			if ($this -> mailer != "mail" && count($this -> to)){
				foreach ($this -> to as $to_addr) $this -> single_to_array[] = $this -> addrFormat($to_addr);
			}
		}
		else {
			if (count($this -> to)){
				if ($this -> mailer != "mail") $result .= $this -> addrAppend("To", $this -> to);
			}
			elseif (!count($this -> cc)) $result .= $this -> headerLine("To", "undisclosed-recipients:;");
		}
		if (fn1::isEmpty($from = fn1::toArray($this -> from, true))) return $this -> err(lg::str("MAILER_NO_SENDER_ERROR"));
		if (!$this -> validateAddress($from_address = $from[0])) return $this -> err(lg::str("MAILER_INVALID_SENDER_ERROR", $from_address));
		$from_name = count($from) > 1 ? fn1::toStrn($from[1], true) : "";
		$this -> from = [$from_address, $from_name];
		$result .= $this -> addrAppend("From", [$this -> from]);
		if (count($this -> cc)) $result .= $this -> addrAppend("Cc", $this -> cc);
		if (in_array($this -> mailer, ["sendmail", "qmail", "mail"]) && count($this -> bcc)) $result .= $this -> addrAppend("Bcc", $this -> bcc);
		if (count($this -> replyTo)) $result .= $this -> addrAppend("Reply-To", $this -> replyTo);
		if ($this -> mailer != "mail") $result .= $this -> headerLine("Subject", $this -> encodeHeader($this -> secureHeader($this -> subject)));
		if ($this -> id != "" && preg_match('/^<.*@.*>$/', $this -> id)) $this -> lastMessageID = $this -> id;
		else $this -> lastMessageID = sprintf("<%s@%s>", $this -> uid, fn1::serverHostname($this -> hostname));
		$result .= $this -> headerLine("Message-ID", $this -> lastMessageID);
		if ($this -> priority !== null) $result .= $this -> headerLine("X-Priority", $this -> priority);
		if ($this -> xmailer == "") $result .= $this -> headerLine("X-Mailer", "NS-Mailer-PHP/" . phpversion());
		else if (!fn1::isEmpty($xmailer = fn1::toStrn($this -> xmailer, true))) $result .= $this -> headerLine("X-Mailer", $mailer);
		if ($this -> confirmReadingTo != "" && $this -> validateAddress($confirmReadingTo = $this -> confirmReadingTo)) $result .= $this -> headerLine("Disposition-Notification-To", "<" . $confirmReadingTo . ">");
		if (!fn1::isEmpty($customHeader = fn1::toArray($this -> customHeader)) && count($customHeader)){
			foreach ($customHeader as $header){
				if (!fn1::isEmpty($header = fn1::toArray($header)) && count($header) > 1) $result .= $this -> headerLine(fn1::toStrn($header[0], true), $this -> encodeHeader(fn1::toStrn($header[1], true)));
			}
		}
		if (!$this -> sign_key_file){
			$result .= $this -> headerLine("MIME-Version", "1.0");
			$result .= $this -> getMailMIME();
		}
		return $result;
	}
	public function preSend(){
		if ($this -> hasError) return false;
		self::$br = PHP_EOL;
		if (stripos(PHP_OS, "WIN") === 0){
			self::$br = "\r\n";
			if ($this -> mailer == "mail" && ini_get("mail.add_x_header") === 1 && ((version_compare(PHP_VERSION, "7.0.0", ">=") && version_compare(PHP_VERSION, "7.0.17", "<")) || (version_compare(PHP_VERSION, "7.1.0", ">=") && version_compare(PHP_VERSION, "7.1.3", "<")))){
				$warning = lg::str("MAILER_X_HEADER_BUG_WARNING");
				trigger_error($warning, E_USER_WARNING);
			}
		}
		try {
			$this -> mail_header = "";
			$this -> queueAddresses($this -> queue_addresses) -> queueRecipients($this -> queue_recipients) -> queueReplyTo($this -> queue_replyTo);
			if ((count($this -> to) + count($this -> cc) + count($this -> bcc)) < 1) throw new \Exception(lg::str("MAILER_EXCEPTION_NO_RECIPIENTS"), self::STOP_CRITICAL);
			if ($this -> isHTML) $this -> altbody = fn1::stripHTML($this -> body);
			if ($this -> hasAlternative()) $this -> contentType = self::CONTENT_TYPE_MULTIPART_ALTERNATIVE;
			$message_type = $this -> getMessageType();
			if (!$this -> allowEmpty && fn1::isEmpty($this -> body)) throw new \Exception(lg::str("MAILER_EXCEPTION_NO_BODY"), self::STOP_CRITICAL);
			$this -> subject = fn1::toStrn($this -> subject, true);
			$this -> mime_header = "";
			if (!$this -> mime_body = $this -> createBody()) throw new \Exception(lg::str("MAILER_EXCEPTION_PREPARE_BODY"), self::STOP_CRITICAL);
			$temp_header = $this -> mime_header;
			if (!$this -> mime_header = $this -> createHeader()) throw new \Exception(lg::str("MAILER_EXCEPTION_PREPARE_HEADER"), self::STOP_CRITICAL);
			$this -> mime_header .= $temp_header;
			if ($this -> mailer == "mail"){
				if (count($this -> to)) $this -> mail_header .= $this -> addrAppend("To", $this -> to);
				else $this -> mail_header .= $this -> headerLine("To", "undisclosed-recipients:;");
				$this -> mail_header .= $this -> headerLine("Subject", $this -> encodeHeader($this -> secureHeader($this -> subject)));
			}
			if (!empty($this -> DKIM_domain) && !empty($this -> DKIM_selector) && (!empty($this -> DKIM_private_string) || (!empty($this -> DKIM_private) && self::isPermittedPath($this -> DKIM_private) && file_exists($this -> DKIM_private)))){
				$header_dkim = $this -> DKIM_Add($this -> mime_header, $this -> encodeHeader($this -> secureHeader($this -> subject)), $this -> mime_body);
				$this -> mime_header = rtrim($this -> mime_header, "\r\n ") . self::$br . self::normalizeBreaks($header_dkim) . self::$br;
			}
			if ($this -> hasError) throw new \Exception(lg::str("MAILER_EXCEPTION_PREPARE_MAIL"), self::STOP_CRITICAL);
			return true;
		}
		catch (\Exception $e){
			return $this -> err($e -> getMessage());
		}
	}
	private function postSend(){
		if ($this -> hasError) return false;
		try {
			switch ($this -> mailer){
				case "sendmail":
				case "qmail":
				return $this -> sendmail_Send($this -> mime_header, $this -> mime_body);
				case "smtp":
				return $this -> smtp_Send($this -> mime_header, $this -> mime_body);
				case "mail":
				return $this -> mail_Send($this -> mime_header, $this -> mime_body);
				default:
				$sendMethod = $this -> mailer . "_Send";
				if (method_exists($this, $sendMethod)) return $this -> $sendMethod($this -> mime_header, $this -> mime_body);
				return $this -> mail_Send($this -> mime_header, $this -> mime_body);
			}
		}
		catch (\Exception $e){
			return $this -> err($e -> getMessage());
		}
	}
	private function sendmail_Send($header, $body){
		if ($this -> hasError) return false;
		$sender = $this -> from[0];
		if (!empty($sender) && self::isShellSafe($sender)){
			if ($this -> mailer == "qmail") $sendmailFmt = "%s -f%s";
			else $sendmailFmt = "%s -oi -f%s -t";
		}
		else {
			if ($this -> mailer == "qmail") $sendmailFmt = "%s";
			else $sendmailFmt = "%s -oi -t";
		}
		$sendmail = sprintf($sendmailFmt, escapeshellcmd($this -> sendmail_path), $sender);
		if ($this -> single_to){
			if (!count($this -> single_to_array)) throw new \Exception(lg::str("MAILER_EXCEPTION_SENDMAIL_TO"), self::STOP_CRITICAL);
			foreach ($this -> single_to_array as $to_addr){
				$mail = @popen($sendmail, "w");
				if (!$mail) throw new \Exception(lg::str("MAILER_EXCEPTION_SENDMAIL_EXECUTE", "001", $this -> sendmail_path), self::STOP_CRITICAL);
				fwrite($mail, "To: " . $to_addr . "\n");
				fwrite($mail, $header);
				fwrite($mail, $body);
				$result = pclose($mail);
				if ($result !== 0) throw new \Exception(lg::str("MAILER_EXCEPTION_SENDMAIL_EXECUTE", "002", $this -> sendmail_path), self::STOP_CRITICAL);
			}
		}
		else {
			$mail = @popen($sendmail, "w");
			if (!$mail) throw new \Exception(lg::str("MAILER_EXCEPTION_SENDMAIL_EXECUTE", "010", $this -> sendmail_path), self::STOP_CRITICAL);
			fwrite($mail, $header);
			fwrite($mail, $body);
			$result = pclose($mail);
			if ($result !== 0) throw new \Exception(lg::str("MAILER_EXCEPTION_SENDMAIL_EXECUTE", "011", $this -> sendmail_path), self::STOP_CRITICAL);
		}
		return $this -> success(lg::str("MAILER_SUCCESS_SENDMAIL"));
	}
	private function smtp_Send($header, $body){
		if ($this -> hasError) return false;
		if (!count($this -> to)) throw new \Exception(lg::str("MAILER_EXCEPTION_SMTP_TO"), self::STOP_CRITICAL);
		$this -> smtp = new SMTP($this -> smtp_host, $this -> smtp_port, $this -> smtp_username, $this -> smtp_password, $this -> smtp_secure, $this -> smtp_auth, $this -> smtp_auth_type);
		$this -> smtp -> subject = $this -> subject;
		$this -> smtp -> from = $this -> from[0];
		$this -> smtp -> to = $this -> to;
		$this -> smtp -> cc = $this -> cc;
		$this -> smtp -> bcc = $this -> bcc;
		try {
			if (!$this -> smtp -> smtpSend($header, $body)) return $this -> err($this -> smtp -> getErrorMessage());
			return $this -> success(lg::str("MAILER_SUCCESS_SMTP"));
		}
		catch (\Exception $e){
			return $this -> err($e -> getMessage());
		}
	}
	private function mail_Send($header, $body){
		if ($this -> hasError) return false;
		if (!count($this -> to)) throw new \Exception(lg::str("MAILER_EXCEPTION_MAIL_RECIPIENTS"), self::STOP_CRITICAL);
		$to_arr = [];
		foreach ($this -> to as $to_addr){
			$to_arr[] = $this -> addrFormat($to_addr);
		}
		$to = implode(", ", $to_arr);
		$sender = $this -> from[0];
		$params = null;
		if (!empty($sender) && $this -> validateAddress($sender)){
			if (self::isShellSafe($sender)) $params = sprintf("-f%s", $sender);
		}
		if (!empty($sender) && $this -> validateAddress($sender)){
			$old_from = ini_get("sendmail_from");
            ini_set("sendmail_from", $sender);
		}
		$result = false;
		if ($this -> single_to && count($to_arr) > 1){
			foreach ($to_arr as $to_addr){
				$result = $this -> mailPassthru($to_addr, $this -> subject, $body, $header, $params);
			}
		}
		else {
			$result = $this -> mailPassthru($to, $this -> subject, $body, $header, $params);
		}
		if (isset($old_from)) ini_set("sendmail_from", $old_from);
		if (!$result) throw new \Exception(lg::str("MAILER_EXCEPTION_MAIL_EXECUTE"), self::STOP_CRITICAL);
		return $this -> success(lg::str("MAILER_SUCCESS_MAIL"));
	}
	private function mailPassthru($to, $subject, $body, $header, $params){
		if (ini_get("mbstring.func_overload") & 1) $subject = $this -> secureHeader($subject);
		else $subject = $this -> encodeHeader($this -> secureHeader($subject));
		if (!$this -> useSendMailOptions || $params == null) $result = @mail($to, $subject, $body, $header);
		else $result = @mail($to, $subject, $body, $header, $params);
		return $result;
	}
	private function DKIM_Add($headers_line, $subject, $body){
		$DKIMsignatureType = "rsa-sha256";
		$DKIMcanonicalization = "relaxed/simple";
		$DKIMquery = "dns/txt";
		$DKIMtime = time();
		$subject_header = "Subject: $subject";
		$headers = explode(self::$br, $headers_line);
		$from_header = "";
		$to_header = "";
		$date_header = "";
		$current = "";
		$copiedHeaderFields = "";
		$foundExtraHeaders = [];
		$extraHeaderKeys = "";
		$extraHeaderValues = "";
		$extraCopyHeaderFields = "";
		foreach ($headers as $header){
			if (strpos($header, "From:") === 0){
				$from_header = $header;
                $current = "from_header";
			}
			elseif (strpos($header, "To:") === 0){
                $to_header = $header;
                $current = "to_header";
            }
			elseif (strpos($header, "Date:") === 0){
                $date_header = $header;
                $current = "date_header";
            }
			elseif (!empty($this -> DKIM_extraHeaders)){
				foreach ($this -> DKIM_extraHeaders as $extraHeader){
					if (strpos($header, $extraHeader . ":") === 0){
						$headerValue = $header;
						if (!fn1::isEmpty($customHeaders = fn1::toArray($this -> customHeader)) && count($customHeaders)){
							foreach ($customHeaders as $customHeader){
								if (!fn1::isEmpty($customHeader = fn1::toArray($customHeader)) && count($customHeader) > 1){
									if ($customHeader[0] === $extraHeader){
										$headerValue = fn1::toStrn($customHeader[0], true) . $this -> encodeHeader(fn1::toStrn($customHeader[1], true));
										break;
									}
								}
							}
						}
						$foundExtraHeaders[$extraHeader] = $headerValue;
						$current = "";
						break;
					}
				}
			}
			else {
				if (!empty($$current) && strpos($header, " =?") === 0) $$current .= $header;
				else $current = "";
			}
		}
		foreach ($foundExtraHeaders as $key => $value){
			$extraHeaderKeys .= ":" . $key;
			$extraHeaderValues .= $value . "\r\n";
			if ($this -> DKIM_copyHeaderFields) $extraCopyHeaderFields .= "\t|" . str_replace("|", "=7C", $this -> DKIM_QP($value)) . ";\r\n";
		}
		if ($this -> DKIM_copyHeaderFields){
			$from = str_replace("|", "=7C", $this -> DKIM_QP($from_header));
            $to = str_replace("|", "=7C", $this -> DKIM_QP($to_header));
            $date = str_replace("|", "=7C", $this -> DKIM_QP($date_header));
            $subject = str_replace("|", "=7C", $this -> DKIM_QP($subject_header));
            $copiedHeaderFields = "\tz=$from\r\n" . "\t|$to\r\n" . "\t|$date\r\n" . "\t|$subject;\r\n" . $extraCopyHeaderFields;
		}
		$body = $this -> DKIM_BodyC($body);
		$DKIMlen = strlen($body);
        $DKIMb64 = base64_encode(pack('H*', hash('sha256', $body)));
		if ($this -> DKIM_identity == "") $ident = "";
		else $ident = " i=" . $this -> DKIM_identity . ";";
		$dkimhdrs = "DKIM-Signature: v=1; a="
		. $DKIMsignatureType . "; q="
		. $DKIMquery . "; l="
		. $DKIMlen . "; s="
		. $this -> DKIM_selector . ";\r\n"
		. "\tt=" . $DKIMtime . '; c=' . $DKIMcanonicalization . ";\r\n"
		. "\th=From:To:Date:Subject" . $extraHeaderKeys . ";\r\n"
		. "\td=" . $this->DKIM_domain . ';' . $ident . "\r\n"
		. $copiedHeaderFields
		. "\tbh=" . $DKIMb64 . ";\r\n"
		. "\tb=";
		$toSign = $this -> DKIM_HeaderC($from_header . "\r\n" . $to_header . "\r\n" . $date_header . "\r\n" . $subject_header . "\r\n" . $extraHeaderValues . $dkimhdrs);
		$signed = $this -> DKIM_Sign($toSign);
		return self::normalizeBreaks($dkimhdrs . $signed) . self::$br;
	}
	private function DKIM_QP($txt){
		$line = "";
        $len = strlen($txt);
        for ($i = 0; $i < $len; $i ++){
            $ord = ord($txt[$i]);
            if (((0x21 <= $ord) and ($ord <= 0x3A)) or $ord == 0x3C or ((0x3E <= $ord) and ($ord <= 0x7E))) $line .= $txt[$i];
			else $line .= "=" . sprintf("%02X", $ord);
        }
        return $line;
	}
	private function DKIM_Sign($signHeader){
        if (!defined('PKCS7_TEXT')) throw new \Exception(lg::str("MAILER_EXCEPTION_MISSING_EXTENSION", "openssl"), self::STOP_CRITICAL);
        $privKeyStr = !empty($this -> DKIM_private_string) ? $this -> DKIM_private_string : file_get_contents($this -> DKIM_private);
        if ($this -> DKIM_passphrase != "") $privKey = openssl_pkey_get_private($privKeyStr, $this -> DKIM_passphrase);
        else $privKey = openssl_pkey_get_private($privKeyStr);
        if (openssl_sign($signHeader, $signature, $privKey, "sha256WithRSAEncryption")){
            openssl_pkey_free($privKey);
            return base64_encode($signature);
        }
        openssl_pkey_free($privKey);
        return "";
    }
	private function DKIM_HeaderC($signHeader){
        $signHeader = preg_replace('/\r\n[ \t]+/', " ", $signHeader);
        $lines = explode("\r\n", $signHeader);
        foreach ($lines as $key => $line){
            if (strpos($line, ":") === false) continue;
            list($heading, $value) = explode(":", $line, 2);
            $heading = strtolower($heading);
            $value = preg_replace('/[ \t]{2,}/', " ", $value);
            $lines[$key] = trim($heading, " \t") . ":" . trim($value, " \t");
        }
        return implode("\r\n", $lines);
    }
	private function DKIM_BodyC($body){
        if (empty($body)) return "\r\n";
        $body = self::normalizeBreaks($body, "\r\n");
        return rtrim($body, "\r\n") . "\r\n";
    }
	public static function normalizeBreaks($text, $br=null){
		$br = $br !== null ? $br : self::$br;
		$text = str_replace(["\r\n", "\r"], "\n", $text);
		if ("\n" !== $br) $text = str_replace("\n", $br, $text);
		return $text;
	}
	public static function hasLineLongerThanMax($text){
		return (bool) preg_match('/^(.{' . (self::MAX_LINE_LENGTH + strlen(self::$br)) . ',})/m', $text);
	}
	static protected function isShellSafe($string){
		if (escapeshellcmd($string) !== $string || !in_array(escapeshellarg($string), ["'$string'", "\"$string\""])) return false;
		$length = strlen($string);
		for ($i = 0; $i < $length; $i ++){
			$c = $string[$i];
			if (!ctype_alnum($c) && strpos("@_-.", $c) === false) return false;
		}
		return true;
	}
	static protected function isPermittedPath($path){
        return !preg_match('#^[a-z]+://#i', $path);
    }
	public static function idnSupported(){
        return function_exists("idn_to_ascii") && function_exists("mb_convert_encoding");
    }
	public static function rfcDate(){
        date_default_timezone_set(@date_default_timezone_get());
        return date('D, j M Y H:i:s O');
    }
}

//SMTP PORTED From: https://github.com/PHPMailer/PHPMailer/blob/master/src/SMTP.php
class SMTP extends Main {
	public $from;
	public $to = [];
	public $cc = [];
	public $bcc = [];
	public $replyTo = [];
	public $all_recipients = [];
	public $options = [];
	public $subject = "";
	public $dsn = "";
	public $helo = "";
	public $host = "";
	public $port = 25;
	public $secure = "";
	public $username = "";
	public $password = "";
	public $authtype = "";
	public $hostname = "";
	public $action_function = "";
	public $SMTPKeepAlive = false;
	public $SMTPAutoTLS = true;
	public $SMTPAuth = false;
	public $connection;
	public $timeout = 300;
	public $timelimit = 300;
	public $do_verp = false;
	const BR = "\r\n";
	const DEFAULT_PORT = 25;
	const MAX_LINE_LENGTH = 998;
	protected $server_caps = null;
	protected $hello_reply = null;
	protected $last_reply = "";
	protected $last_smtp_transaction_id;
	protected $smtp_transaction_id_patterns = [
        'exim' => '/[\d]{3} OK id=(.*)/',
        'sendmail' => '/[\d]{3} 2.0.0 (.*) Message/',
        'postfix' => '/[\d]{3} 2.0.0 Ok: queued as (.*)/',
        'Microsoft_ESMTP' => '/[0-9]{3} 2.[\d].0 (.*)@(?:.*) Queued mail for delivery/',
        'Amazon_SES' => '/[\d]{3} Ok (.*)/',
        'SendGrid' => '/[\d]{3} Ok: queued as (.*)/',
        'CampaignMonitor' => '/[\d]{3} 2.0.0 OK:([a-zA-Z\d]{48})/',
    ];
	public function __construct($host="", $port=25, $username="", $password="", $secure="", $auth=false, $authtype=""){
		parent::__construct();
		$this -> host = fn1::toStrn($host, true);
		$this -> port = fn1::toNum($port);
		$this -> username = fn1::toStrn($username, true);
		$this -> password = fn1::toStrn($password);
		$this -> secure = strtolower(fn1::toStrn($secure, true));
		$this -> SMTPAuth = (bool) $auth;
		$this -> authtype = fn1::toStrn($authtype, true);
		if (is_numeric(NS_SMTP_TIMEOUT_SEC) && NS_SMTP_TIMEOUT_SEC > 0) $this -> timeout = NS_SMTP_TIMEOUT_SEC;
		if (is_numeric(NS_SMTP_TIMELIMIT_SEC) && NS_SMTP_TIMELIMIT_SEC > 0) $this -> timelimit = NS_SMTP_TIMEOUT_SEC;
	}
	public function __destruct(){
		$this -> smtpClose();
	}
	protected function getLines(){
		if (!is_resource($this -> connection)) return "";
		$data = "";
		$endtime = 0;
		stream_set_timeout($this -> connection, $this -> timeout);
		if ($this -> timelimit > 0) $endtime = time() + $this -> timelimit;
		$selR = [$this -> connection];
		$selW = null;
		while (is_resource($this -> connection) && !feof($this -> connection)){
			if (!stream_select($selR, $selW, $selW, $this -> timelimit)){
				$this -> debug("SMTP -> getLines(): time-out(" . $this -> timeout . ")", "SMTP", self::DEBUG_LEVEL_NORMAL);
				break;
			}
			$str = @fgets($this -> connection, 515);
			$this -> debug("SMTP INBOUND: " . trim($str), "SMTP", self::DEBUG_LEVEL_NORMAL);
			$data .= $str;
			if (!isset($str[3]) || (isset($str[3]) && $str[3] == " ")) break;
			$info = stream_get_meta_data($this -> connection);
			if ($info["timed_out"]){
				$this -> debug("SMTP -> getLines(): time-out(" . $this -> timeout . ")", "SMTP", self::DEBUG_LEVEL_NORMAL);
				break;
			}
			if ($endtime && time() > $endtime){
				$this -> debug("SMTP -> getLines(): timelimit reached (" . $this -> timelimit . ")", "SMTP", self::DEBUG_LEVEL_NORMAL);
				break;
			}
		}
		return $data;
	}
	protected function command($command, $command_string, $expect){
		if (!$this -> isConnected()) return $this -> error(lg::str("SMTP_ERROR_COMMAND_CONNECTION", $command));
		if (strpos($command_string, "\n") !== false || strpos($command_string, "\r") !== false) return $this -> error(lg::str("SMTP_ERROR_COMMAND_BREAKS", $command));
		$this -> clientSend($command_string . self::BR, $command);
		$this -> last_reply = $this -> getLines();
		$matches = [];
		if (preg_match('/^([0-9]{3})[ -](?:([0-9]\\.[0-9]\\.[0-9]{1,2}) )?/', $this -> last_reply, $matches)){
			$code = $matches[1];
			$code_ex = count($matches) > 2 ? $matches[2] : null;
			$detail = preg_replace("/{$code}[ -]" . ($code_ex ? str_replace(".", "\\.", $code_ex) . " " : "") . "/m", "", $this -> last_reply);
		}
		else {
			$code = substr($this -> last_reply, 0, 3);
			$code_ex = null;
			$detail = substr($this -> last_reply, 4);
		}
		$this -> debug("SERVER -> CLIENT: " . $this -> last_reply, "SMTP", self::DEBUG_LEVEL_NORMAL);
		if (!in_array($code, (array) $expect)){
			$this -> debug("SMTP ERROR: " . $this -> last_reply, "SMTP", self::DEBUG_LEVEL_CRITICAL);
			$error_data = [
				"error" => lg::str("SMTP_ERROR_COMMAND_FAILED", $command),
				"detail" => $detail,
				"smtp_code" => $code,
				"smtp_code_ex" => $code_ex,
				"expect" => $expect
			];
			$this -> debug($error_data, "SMTP", self::DEBUG_LEVEL_CRITICAL);
			return $this -> error(lg::str("SMTP_ERROR_COMMAND_FAILED", $command));
		}
		self::$last_error = "";
		$this -> error_msg = "";
		return true;
	}
	protected function hmac($data, $key){
		if (function_exists("hash_hmac")) return hash_hmac("md5", $data, $key);
		$bytelen = 64; //md5
		if (strlen($key) > $bytelen) $key = pack("H*", md5($key));
		$key = str_pad($key, $bytelen, chr(0x00));
		$ipad = str_pad("", $bytelen, chr(0x36));
		$opad = str_pad("", $bytelen, chr(0x5c));
		$k_ipad = $key ^ $ipad;
		$k_opad = $key ^ $opad;
		return md5($k_opad . pack("H*", md5($k_ipad . $data)));
	}
	protected function sendHello($hello, $host){
		$noerror = $this -> command($hello, $hello . " " . $host, 250);
		$this -> hello_reply = $this -> last_reply;
		if ($noerror) $this -> parseHelloFields($hello);
		else $this -> server_caps = null;
		return $noerror;
	}
	protected function parseHelloFields($type){
		$this -> server_caps = null;
		$lines = explode("\n", $this -> hello_reply);
		foreach ($lines as $n => $s){
			$s = trim(substr($s, 4));
			if (empty($s)) continue;
			$fields = explode(" ", $s);
			if (!empty($fields)){
				if (!$n){
					$name = $type;
					$fields = $fields[0];
				}
				else {
					$name = array_shift($fields);
					switch ($name){
						case "SIZE":
						$fields = ($fields ? $fields[0] : 0);
						break;
						case "AUTH":
						if (!is_array($fields)) $fields = [];
						break;
						default:
						$fields = true;
					}
				}
				$this -> server_caps[$name] = $fields;
			}
		}
	}
	protected function recordLastTransactionID(){
		$reply = $this -> last_reply;
		if (empty($reply)) $this -> last_smtp_transaction_id = null;
		else {
			$this -> last_smtp_transaction_id = false;
			foreach ($this -> smtp_transaction_id_patterns as $smtp_transaction_id_pattern){
				if (preg_match($smtp_transaction_id_pattern, $reply, $matches)){
					$this -> last_smtp_transaction_id = trim($matches[1]);
					break;
				}
			}
		}
		return $this -> last_smtp_transaction_id;
	}
	protected function serverHostname(){
		$result = "";
		if (!empty($this -> hostname)) $result = $this -> hostname;
		else if (isset($_SERVER) && array_key_exists("SERVER_NAME", $_SERVER)) $result = $_SERVER["SERVER_NAME"];
		else if (function_exists("gethostname") && gethostname() !== false) $result = gethostname();
		else if (php_uname("n") !== false) $result = php_uname("n");
		return fn1::isValidHost($result) ? $result : "localhost.localdomain";
	}
	public function errorHandler($errno, $detail="", $smtp_code="", $smtp_code_ex=""){
		$error = [
			"errno" => $errno,
			"detail" => $detail,
			"smtp_code" => $smtp_code,
			"smtp_code_ex" => $smtp_code_ex
		];
		$this -> debug($error, "SMTP", self::DEBUG_LEVEL_CRITICAL);
		return $this -> error($error);
	}
	public function getAllRecipients(){
		$this -> all_recipients = [];
		foreach ($this -> to as $address) $this -> all_recipients[] = $address;
		foreach ($this -> cc as $address) $this -> all_recipients[] = $address;
		foreach ($this -> bcc as $address) $this -> all_recipients[] = $address;
		return $this -> all_recipients;
	}
	public function smtpConnect($options=null){
		if ($this -> isConnected()) return true;
		if ($options === null) $options = $this -> options;
		$hosts = explode(";", $this -> host);
		foreach ($hosts as $hostentry){
			if (!preg_match('/^((ssl|tls):\/\/)*([a-zA-Z0-9\.-]*|\[[a-fA-F0-9:]+\]):?([0-9]*)$/', trim($hostentry), $hostinfo)){
				$this -> debug("Invalid Host: " . $host_entry, "SMTP", self::DEBUG_LEVEL_NORMAL);
				continue;
			}
			if (!fn1::isValidHost($hostinfo[3])){
				$this -> debug("Invalid Host: " . $host_entry, "SMTP", self::DEBUG_LEVEL_NORMAL);
				continue;
			}
			$prefix = "";
			$secure = strtolower(trim($this -> secure));
			$tls = $secure == "tls";
			if (strtolower($hostinfo[2]) == "ssl" || ($hostinfo[2] == "" && $secure == "ssl")){
				$prefix = "ssl://";
				$secure = "ssl";
				$tls = false;
			}
			else if (strtolower($hostinfo[2]) == "tls"){
				$tls = true;
				$secure = "tls";
			}
			$sslext = defined("OPENSSL_ALGO_SHA256");
			if ($secure === "tls" || $secure === "ssl"){
				if (!$sslext) throw new \Exception(lg::str("MAILER_EXCEPTION_MISSING_EXTENSION", "openssl"), self::STOP_CRITICAL);
			}
			$host = $hostinfo[3];
			$port = fn1::toNum($this -> port);
			$tport = fn1::toNum($hostinfo[4]);
			if ($tport > 0 && $tport < 65536) $port = $tport;
			if ($this -> connect($prefix . $host, $port, $this -> timeout, $options)){
				try {
					$helo = $this -> helo ? $this -> helo : $this -> serverHostname();
					$this -> hello($helo);
					if ($this -> SMTPAutoTLS && $sslext && $secure != "ssl" && $this -> getServerExt("STARTTLS")) $tls = true;
					if ($tls){
						if (!$this -> startTLS()) throw new \Exception(lg::str("SMTP_EXCEPTION_STARTTLS", $this -> error_msg));
						$this -> hello($helo);
					}
					if ($this -> SMTPAuth && !$this -> authenticate($this -> username, $this -> password, $this -> authtype)) throw new \Exception(lg::str("SMTP_EXCEPTION_AUTHENTICATION", $this -> error_msg));
					$this -> debug("SMTPConnect (smtpConnect): connected", "SMTP", self::DEBUG_LEVEL_NORMAL);
					return true;
				}
				catch (\Exception $e){
					$this -> debug($e -> getMessage(), "SMTP", self::DEBUG_LEVEL_CRITICAL);
					$this -> quit();
					return $this -> error($e -> getMessage());
				}
			}
		}
		$this -> close();
		$this -> debug("SMTPConnect (smtpConnect): failed", "SMTP", self::DEBUG_LEVEL_CRITICAL);
		return $this -> error(lg::str("SMTP_ERROR_CONNECTION_FAILED"));
	}
	public function smtpClose(){
		if ($this -> isConnected()){
			$this -> quit();
			$this -> close();
		}
	}
	public function smtpSend($header, $body){
		if (!$this -> smtpConnect($this -> options)) throw new \Exception($this -> error_msg, self::STOP_CRITICAL);
		if (!$this -> mail($this -> from)) throw new \Exception($this -> error_msg, self::STOP_CRITICAL);
		$bad_rcpt = [];
		$callbacks = [];
		$all_recipients = $this -> getAllRecipients();
		foreach ([$this -> to, $this -> cc, $this -> bcc] as $togroup){
			foreach ($togroup as $to){
				if (!$this -> recipient($to[0], $this -> dsn)){
					$bad_rcpt[] = ["to" => $to[0], "error" => $this -> error_msg];
					$isSent = false;
				}
				else $isSent = true;
				$callbacks[] = ["issent" => $isSent, "to" => $to[0]];
			}
		}
		if ((count($all_recipients) > count($bad_rcpt)) && !$this -> data($header . $body)) throw new \Exception(lg::str("SMTP_EXCEPTION_DATA", $this -> error_msg));
		$smtp_transaction_id = $this -> getLastTransactionID();
		if ($this -> SMTPKeepAlive) $this -> reset();
		else {
			$this -> quit();
			$this -> close();
		}
		foreach ($callbacks as $cb) $this -> doCallback($cb["issent"], [$cb["to"]], [], [], $this -> subject, $body, $this -> from, ["smtp_transaction_id" => $smtp_transaction_id]);
		if (count($bad_rcpt) > 0){
			$errstr = "";
			foreach ($bad_rcpt as $bad) $errstr .= $bad["to"] . ": " . $bad["error"];
			$this -> debug("SMTPSend (smtpSend): $errstr", "SMTP", self::DEBUG_LEVEL_CRITICAL);
			throw new \Exception(lg::str("SMTP_EXCEPTION_RECIPIENTS", $errstr), self::STOP_CONTINUE);
		}
		$this -> debug("SMTPSend (smtpSend): successful", "SMTP", self::DEBUG_LEVEL_NORMAL);
		return true;
	}
	public function connect($host, $port=null, $timeout=null, $options=[]){
		static $streamok;
		if ($this -> isConnected()) return true;
		if ($streamok === null) $streamok = function_exists("stream_socket_client");
		if ($options === null) $options = $this -> options;
		$port = (($port = fn1::toNum($port)) > 0) ? $port : self::DEFAULT_PORT;
		$timeout = (($timeout = fn1::toNum($timeout)) > 0) ? $timeout : $this -> timeout;
		$this -> debug("SMTP connect $host:$port, timeout=$timeout, options=" . (count($options) > 0 ? var_export($options, true) : "[]"), "SMTP", self::DEBUG_LEVEL_NORMAL);
		$errno = 0;
		$errstr = "";
		if ($streamok){
			$socket_context = stream_context_create($options);
			set_error_handler([$this, "errorHandler"]);
			$this -> connection = @stream_socket_client($host . ":" . $port, $errno, $errstr, $timeout, STREAM_CLIENT_CONNECT, $socket_context);
			restore_error_handler();
		}
		else {
			$this -> debug("Connection stream_socket_client unavailable using fsockopen", "SMTP", self::DEBUG_LEVEL_NORMAL);
			set_error_handler([$this, "errorHandler"]);
			$this -> connection = @fsockopen($host, $port, $errno, $errstr, $timeout);
			restore_error_handler();
		}
		if (!is_resource($this -> connection)) return $this -> error(lg::str("SMTP_ERROR_CONNECTION_FAILURE", $errstr, $errno));
		$this -> debug("Connection: opened", "SMTP", self::DEBUG_LEVEL_NORMAL);
		if (substr(PHP_OS, 0, 3) != "WIN"){
			$max = ini_get("max_execution_time");
			if (0 != $max && $timeout > $max) @set_time_limit($timeout);
			stream_set_timeout($this -> connection, $timeout, 0);
		}
		$announce = $this -> getLines();
		$this -> debug("SERVER -> CLIENT: " . $announce, "SMTP", self::DEBUG_LEVEL_NORMAL);
		return $this -> success($announce);
	}
	public function isConnected(){
		if (is_resource($this -> connection)){
			$sock_status = stream_get_meta_data($this -> connection);
			if ($sock_status["eof"]){
				$this -> debug("SMTP NOTICE: EOF caught while checking connection", "SMTP", self::DEBUG_LEVEL_CRITICAL);
				$this -> close();
				return false;
			}
			return true;
		}
		return false;
	}
	public function close(){
		$this -> server_caps = null;
		$this -> hello_reply = null;
		if (is_resource($this -> connection)){
			fclose($this -> connection);
			$this -> connection = null;
			$this -> debug("Connection: closed", "SMTP", self::DEBUG_LEVEL_NORMAL);
		}
	}
	public function quit($close_on_error=true){
		$noerror = $this -> command("QUIT", "QUIT", 221);
		$err = self::$last_error;
		if ($noerror || $close_on_error){
			$this -> close();
			self::$last_error = $err;
		}
		return $noerror;
	}
	public function startTLS(){
		if (!$this -> command("STARTTLS", "STARTTLS", 220)) return false;
		$crypto_method = STREAM_CRYPTO_METHOD_TLS_CLIENT;
		if (defined("STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT")){
			$crypto_method |= STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT;
            $crypto_method |= STREAM_CRYPTO_METHOD_TLSv1_1_CLIENT;
		}
		set_error_handler([$this, "errorHandler"]);
		$crypto_ok = stream_socket_enable_crypto($this -> connection, true, $crypto_method);
		restore_error_handler();
		return (bool) $crypto_ok;
	}
	public function authenticate($username, $password, $authtype=null){
		if (!$this -> server_caps) return $this -> error(lg::str("SMTP_ERROR_AUTHENTICATION_HELO"));
		if (array_key_exists("EHLO", $this -> server_caps)){
			if (!array_key_exists("AUTH", $this -> server_caps)) return $this -> error(lg::str("SMTP_ERROR_AUTHENTICATION_STAGE"));
			$this -> debug("Auth method requested: " . ($authtype ? $authtype : "UNSPECIFIED"), "SMTP", self::DEBUG_LEVEL_NORMAL);
			$this -> debug("Auth methods available on the server: " . implode(",", $this -> server_caps["AUTH"]), "SMTP", self::DEBUG_LEVEL_LOW);
			if ($authtype !== null && !in_array($authtype, $this -> server_caps["AUTH"])){
				$this -> debug("Requested auth method not available: " . $authtype, "SMTP", self::DEBUG_LEVEL_CRITICAL);
				$authtype = null;
			}
			if (empty($authtype)){
				foreach (["CRAM-MD5", "LOGIN", "PLAIN", "XOAUTH2"] as $method){
                    if (in_array($method, $this -> server_caps["AUTH"])){
                        $authtype = $method;
                        break;
                    }
                }
				if (empty($authtype)) return $this -> error(lg::str("SMTP_ERROR_AUTHENTICATION_SUPPORT"));
				$this -> debug("Auth method selected: " . $authtype, "SMTP", self::DEBUG_LEVEL_CRITICAL);
			}
			if (!in_array($authtype, $this -> server_caps["AUTH"])) return $this -> error(lg::str("SMTP_ERROR_AUTHENTICATION_NO_SUPPORT", $authtype));
		}
		else if (empty($authtype)) $authtype = "LOGIN";
		switch ($authtype){
			case "PLAIN":
			if (!$this -> command("AUTH", "AUTH PLAIN", 334)) return false;
			if (!$this -> command("User & Password", base64_encode("\0" . $username . "\0" . $password), 235)) return false;
			break;
			case "LOGIN":
			if (!$this -> command("AUTH", "AUTH LOGIN", 334)) return false;
			if (!$this -> command("Username", base64_encode($username), 334)) return false;
			if (!$this -> command("Password", base64_encode($password), 235)) return false;
			break;
			case "CRAM-MD5":
			if (!$this -> command("AUTH CRAM-MD5", "AUTH CRAM-MD5", 334)) return false;
			$challenge = base64_encode(substr($this -> last_reply, 4));
			$response = $username . " " . $this -> hmac($challenge, $password);
			return $this -> command("Username", base64_encode($response), 235);
			break;
			default:
			return $this -> error(lg::str("SMTP_ERROR_AUTHENTICATION_NO_SUPPORT", $authtype));
		}
		return true;
	}
	public function clientSend($data, $command=""){
		if (in_array($command, ["User & Password", "Username", "Password"], true)) $this -> debug("CLIENT -> SERVER: <credentials hidden>", "SMTP", self::DEBUG_LEVEL_NORMAL);
		else $this -> debug("CLIENT -> SERVER: " . $data, "SMTP", self::DEBUG_LEVEL_NORMAL);
		set_error_handler([$this, "errorHandler"]);
		$result = fwrite($this -> connection, $data);
		restore_error_handler();
		return $result;
	}
	public function data($data){
		if (!$this -> command("DATA", "DATA", 354)) return false;
		$lines = explode("\n", str_replace(["\r\n", "\r"], "\n", $data));
		$field = substr($lines[0], 0, strpos($lines[0], ":"));
		$in_headers = false;
		if (!empty($field) && strpos($field, " ") === false) $in_headers = true;
		foreach ($lines as $line){
			$lines_out = [];
			if ($in_headers && $line == "") $in_headers = false;
			while (isset($line[self::MAX_LINE_LENGTH])){
				$pos = strrpos(substr($line, 0, self::MAX_LINE_LENGTH), " ");
				if (!$pos){
					$pos = self::MAX_LINE_LENGTH - 1;
					$lines_out[] = substr($line, 0, $pos);
					$line = substr($line, $pos);
				}
				else {
					$lines_out[] = substr($line, 0, $pos);
					$line = substr($line, $pos + 1);
				}
				if ($in_headers) $line = "\t" . $line;
			}
			$lines_out[] = $line;
			foreach ($lines_out as $line_out){
				if (!empty($line_out) && $line_out[0] == ".") $line_out = "." . $line_out;
				$this -> clientSend($line_out . self::BR, "DATA");
			}
		}
		$savetimelimit = $this -> timelimit;
		$this -> timelimit = $this -> timelimit * 2;
		$result = $this -> command("DATA END", ".", 250);
		$this -> recordLastTransactionID();
		$this -> timelimit = $savetimelimit;
		return $result;
	}
	public function hello($host=""){
		return $this -> sendHello("EHLO", $host) || $this -> sendHello("HELO", $host);
	}
	public function mail($from){
		$useVerp = ($this -> do_verp ? "XVERP" : "");
		return $this -> command("MAIL FROM", "MAIL FROM:<" . $from . ">" . $useVerp, 250);
	}
	public function recipient($address, $dsn=""){
		if (empty($dsn)) $rcpt = "RCPT TO:<" . $address . ">";
		else {
			$dsn = strtoupper($dsn);
			$notify = [];
			if (strpos($dsn, "NEVER") !== false) $notify[] = "NEVER";
			else {
				foreach (["SUCCESS", "FAILURE", "DELAY"] as $value){
					if (strpos($dsn, $value) !== false) $notify[] = $value;
				}
			}
			$rcpt = "RCPT TO:<" . $address . "> NOTIFY=" . implode(",", $notify);
		}
		return $this -> command("RCPT TO", $rcpt, [250, 251]);
	}
	public function reset(){
		return $this -> command("RSET", "RSET", 250);
	}
	public function sendAndMail($from){
		return $this -> command("SAML", "SAML FROM:$from", 250);
	}
	public function verify($name){
		return $this -> command("VRFY", "VRFY $name", [250, 251]);
	}
	public function noop(){
		return $this -> command("NOOP", "NOOP", 250);
	}
	public function turn(){
		$error = lg::str("SMTP_ERROR_TURN");
		$this -> debug("SMTP NOTICE: " . $error, "SMTP", self::DEBUG_LEVEL_LOW);
		return $this -> error($error);
	}
	public function getServerExtList(){
		return $this -> server_caps;
	}
	public function getServerExt($name){
		if (!$this -> server_caps) return $this -> error(lg::str("SMTP_ERROR_NO_HELO"), null);
		if (!array_key_exists($name, $this -> server_caps)){
			if ("HELO" == $name) return $this -> server_caps["EHLO"];
			if ("EHLO" == $name || array_key_exists("EHLO", $this -> server_caps)) return false;
			return $this -> error(lg::str("SMTP_ERROR_HELO_HANDSHAKE"), null);
		}
		return $this -> server_caps[$name];
	}
	public function getLastReply(){
		return $this -> last_reply;
	}
	public function setVerp($enabled=false){
		$this -> do_verp = (bool) $enabled;
	}
	public function getVerp(){
		return $this -> do_verp;
	}
	public function setTimeout($timeout=0){
		$this -> timeout = fn1::toNum($timeout);
	}
	public function getTimeout(){
		return $this -> timeout;
	}
	public function getLastTransactionID(){
		return $this -> last_smtp_transaction_id;
	}
	public function doCallback($isSent, $to, $cc, $bcc, $subject, $body, $from, $extra){
		if (!empty($this -> action_function) && is_callable($this -> action_function)){
			call_user_func($this -> action_function, $isSent, $to, $cc, $bcc, $subject, $body, $from, $extra);
		}
	}
}
