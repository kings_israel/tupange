<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190330
*	OVERRODE DEFAULT GLOBAL DEFINITIONS (see default values on: globals.ref.php)
*/
#host
if (!defined("NS_SERVER_ROOT")) define("NS_SERVER_ROOT", "/opt/lampp/htdocs/projects/_tupange");
if (!defined("NS_ROOT_DIR")) define("NS_ROOT_DIR", NS_SERVER_ROOT . "/");
if (!defined("NS_SITE_DIR")) define("NS_SITE_DIR", NS_SERVER_ROOT . "/");
if (!defined("NS_BASE")) define("NS_BASE", "http://tupange.site");
if (!defined("NS_SITE")) define("NS_SITE", "http://tupange.site");
if (!defined("NS_DOMAIN")) define("NS_DOMAIN", "tupange.site");
if (!defined("NS_HOSTNAME")) define("NS_HOSTNAME", "tupange.site");

#debug
if (!defined("NS_DEBUG_MODE")) define("NS_DEBUG_MODE", true);

#mailer
if (!defined("NS_MAIL_FROM")) define("NS_MAIL_FROM", "mail@tupange.com");
if (!defined("NS_MAIL_FROM_NAME")) define("NS_MAIL_FROM_NAME", "Tupange");

#SMTP
if (!defined("NS_SMTP_ENABLED")) define("NS_SMTP_ENABLED", true);
if (!defined("NS_SMTP_HOST")) define("NS_SMTP_HOST", "mail.tupange.com");
if (!defined("NS_SMTP_PORT")) define("NS_SMTP_PORT", 465);
if (!defined("NS_SMTP_USERNAME")) define("NS_SMTP_USERNAME", "mail@tupange.com");
if (!defined("NS_SMTP_PASSWORD")) define("NS_SMTP_PASSWORD", "LeH78O,-8BUs");

#database
if (!defined("NS_DATABASE_PASSWORD")) define("NS_DATABASE_PASSWORD", "root");

#user
if (!defined("NS_USER_VERIFY_LINK")) define("NS_USER_VERIFY_LINK", NS_SITE . "/auth/verify/{{code}}");
if (!defined("NS_USER_RESET_LINK")) define("NS_USER_RESET_LINK", NS_SITE . "/auth/reset/{{code}}");

#files
if (!defined("NS_UPLOADS_FOLDER")) define("NS_UPLOADS_FOLDER", NS_SITE_DIR . "/uploads");
if (!defined("NS_UPLOADS_TEMPLATE")) define("NS_UPLOADS_TEMPLATE", "{{uploads}}/{{random_string}}.{{ext}}");
if (!defined("NS_UPLOADS_MAX_SIZE")) define("NS_UPLOADS_MAX_SIZE", 5);
