<?php
/**
*	NAICODE SERVER (C) 2019 NAICODE SYSTEMS AND PLATFORMS LTD
*	VERSION 1.0 ~ @martin 20190208
*	SERVER
*/

namespace Naicode;
require_once __DIR__ . "/lang.php";
require_once __DIR__ . "/funcs.php";
require_once __DIR__ . "/globals.php";
use Naicode\Server\Lang as lg;
use Naicode\Server\Funcs as fn1;
use Naicode\Server\Funcs\Main;

if (!defined("NS_ALLOW_HEADERS")) define("NS_ALLOW_HEADERS", [
	"Access-Control-Allow-Origin: *",
	"Access-Control-Allow-Methods: HEAD, GET, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE",
	"Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description",
]);
if (!defined("NS_TIMEZONE")) define("NS_TIMEZONE", "Africa/Nairobi");
if (!defined("NS_ERROR_REPORTING")) define("NS_ERROR_REPORTING", E_ALL & ~E_DEPRECATED);
if (!defined("NS_RESPONSE_MODE")) define("NS_RESPONSE_MODE", "json");

class Server {
	public static $last_error = null;
	public static function init(){
		if (!fn1::isEmpty($allow_headers = fn1::toArray(NS_ALLOW_HEADERS))){
			foreach ($allow_headers as $header) if (!fn1::isEmpty($header = fn1::toStrx($header, true)) && fn1::regMatch('/^(.+):(.+)$/', $header)) header($header);
		}
		if (!fn1::isEmpty($timezone = fn1::toStrn(NS_TIMEZONE, true))) date_default_timezone_set($timezone);
		if (!fn1::isEmpty($error_reporting = NS_ERROR_REPORTING)) error_reporting($error_reporting);
		//on options
		if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") self::html();
		if (isset($_GET["__serverinfo"])) self::text($_SERVER);
		if (isset($_GET["__randstr"])) self::text(fn1::randomString($_GET["__randstr"]));
		if (isset($_GET["__phpinfo"])){ echo phpinfo(); exit(); }
	}
	public static function responseMode($mode=null){
		$modes = ["json", "xml", "html", "text"];
		if (in_array($mode = strtolower(fn1::toStr($mode, true)), $modes)) return $mode;
		if (in_array(($mode = strtolower(fn1::toStrn(NS_RESPONSE_MODE, true))), $modes)) return $mode;
		return "json";
	}
	public static function response($data=null, $message=null, $type=null, $mode=null){
		$mode = self::responseMode($mode);
		switch ($mode){
			case "json":
			case "xml":
			$response = [];
			if (!fn1::isEmpty($type = fn1::toStr($type, true, true))) $response["type"] = $type;
			if (!fn1::isEmpty($message = fn1::trimVal($message, false))) $response["message"] = $message;
			if (!fn1::isEmpty($data = fn1::trimVal($data, false))) $response["data"] = $data;
			if (fn1::isEmpty($response)) $response = null;
			$response = ["response" => $response];
			header("Content-Type: application/" . $mode);
			echo $mode == "json" ? fn1::jsonCreate($response) : fn1::objectXML($response);
			break;
			case "html":
			header("Content-Type: text/html");
			$response = "<div";
			if (!fn1::isEmpty($type = fn1::strbreaks($type, " ", true))) $response .= " ns-type=\"$type\"";
			$response .= ">";
			if (!fn1::isEmpty($message = fn1::strbreaks(fn1::prints($message), "<br>", true))) $response .= "<div class=\"ns-message\">$message</div>";
			if (!fn1::isEmpty($data = fn1::strbreaks(fn1::prints($data), "<br>", true))) $response .= "<div class=\"ns-data\">$data</div>";
			$response .= "</div>";
			echo $response;
			break;
			case "text":
			default:
			$response = "";
			if (!fn1::isEmpty($message = fn1::trimVal(fn1::prints($message)))) $response .= $message;
			if (!fn1::isEmpty($data = fn1::trimVal(fn1::prints($data)))) $response .= ($response == "" ? "" : "\r\n") . $data;
			if ($mode == "text"){
				header("Content-Type: text/plain");
				if (!fn1::isEmpty($type = fn1::toStr($type, true, true))) $response = $type . ($response == "" ? "" : "\r\n") . $response;
			}
			else {
				$filename = !fn1::isEmpty($type = fn1::strbreaks($type, " ", true)) ? (fn1::regMatch('/^(.+)\.(.+)$/', $type) ? $type : $type . ".txt") : "download.txt";
				header("Content-Type: application/octet-stream; name=\"$filename\"");
				header("Content-Disposition: inline;");
				header("Content-Type: text/plain");
			}
			echo $response;
		}
		exit();
	}
	public static function error($data, $message=null, $mode=null){
		self::response($data, $message, "error", $mode);
	}
	public static function success($data, $message=null, $mode=null){
		self::response($data, $message, "success", $mode);
	}
	public static function text($data=null){
		self::response($data, null, null, "text");
	}
	public static function html($data=null){
		self::response($data, null, null, "html");
	}
	public static function on($params=null, $method="GET", &$data=null, &$path=null, &$headers=null){
		self::$last_error = null;
		$request_method = strtoupper($_SERVER["REQUEST_METHOD"]);
		if (fn1::isEmpty($method = strtoupper(fn1::toStrn($request_method, true)))) $method = $request_method;
		elseif (!in_array($method, ($supported_methods = ["HEAD", "GET", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE"]))){
			self::$last_error = lg::str("SERVER_ERROR_METHOD_SUPPORT", $method, implode(", ", $supported_methods));
			return false;
		}
		elseif ($method !== $request_method){
			self::$last_error = lg::str("SERVER_ERROR_METHOD_MATCH", $method, $request_method);
			return false;
		}
		$request = fn1::toArray($_REQUEST);
		if (!fn1::isEmpty($input = fn1::toStrx(@file_get_contents("php://input"), true))) $request = fn1::arrayMerge($request, fn1::jsonParse($input, true, []), false, true);
		if (!fn1::isEmpty($params = fn1::trimVal($params))){
			if (!fn1::isObject($params)){
				if (count($csv_array = fn1::csvArray($params, true))) $params = fn1::trimVal($csv_array[0]);
				else {
					self::$last_error = lg::str("SERVER_ERROR_INVALID_PARAMS", fn1::toStr($params, true, true));
					return false;
				}
			}
			else $params = (array) $params;
		}
		else $params = [];
		$request_params = array_keys($request);
		$temp_params = $params;
		$params = [];
		$params_any = false;
		foreach ($temp_params as $param){
			$param = fn1::filterName($param, true);
			if (fn1::isEmpty($param)) continue;
			if (!$params_any && fn1::inStr('/\s*\*\s*$/', $param)) $params_any = true;
			$param = fn1::replaceVal('/\s*\*\s*$/', "", $param);
			if (fn1::isEmpty($param)) continue;
			$is_optional = fn1::inStr('/\s*\?\s*$/', $param);
			$param = fn1::replaceVal('/\s*\?\s*$/', "", $param);
			if (fn1::isEmpty($param)) continue;
			if (!fn1::inArray($param, $request_params, false) && !$is_optional){
				self::$last_error = lg::str("SERVER_ERROR_MISSING_PARAM", $param, implode(", ", $request_params));
				return false;
			}
			$params[] = $param;
		}
		$data = [];
		$params_count = count($params);
		foreach ($request as $request_param => $value){
			if ($params_count){
				$param_index = fn1::arraySearch($request_param, $params, false);
				if ($param_index === false){
					if (!$params_any){
						self::$last_error = lg::str("SERVER_ERROR_EXTRA_PARAM", $request_param);
						return false;
					}
				}
				else $request_param = $params[$param_index];
			}
			if (fn1::isEmpty($param = fn1::filterName($request_param))) continue;
			$data[$param] = fn1::filterRequestValue($value);
		}
		$path = trim(substr(@$_SERVER['PATH_INFO'], 1));
		if (strlen($path)) $path = explode("/", $path);
		else $path = [];
		$headers = fn1::getallheaders();
		return true;
	}
	public static function get($params=null, &$data=null, &$path=null, &$headers=null){
		return self::on($params, "GET", $data, $path, $headers);
	}
	public static function post($params=null, &$data=null, &$path=null, &$headers=null){
		return self::on($params, "POST", $data, $path, $headers);
	}
	public static function any($params=null, &$data=null, &$path=null, &$headers=null){
		return self::on($params, null, $data, $path, $headers);
	}
}
